package ce1002.s101502026;

import java.util.Scanner; //import the header file

public class Q1
{
	public static void toFahrenheit() //this is the function to transform Celcius to Fahrenheit
	{
		Scanner scanner=new Scanner(System.in);
		double c;
		System.out.println("Please input the temperature: ");
		c=scanner.nextInt();
		System.out.println(c+" in Celcius is equal to "+(c*9/5+32)+" in Fahrenheit.");
		System.out.println();
	}
	
	public static void toCelcius() //this is the function to transform Fahrenheit to Celcius
	{
		Scanner scanner=new Scanner(System.in);
		double f;
		System.out.println("Please input the temperature: ");
		f=scanner.nextInt();
		System.out.println(f+" in Fahrenheit is equal to "+((f-32)*5/9)+" in Celcius.");
		System.out.println();		
	}
	
	public static void main(String[] args)
	{
		while(true)
		{
			int x; //x is the options the user choose
			System.out.println("Please choose the method you want to use:\n1.toFahrenheit\n2.toCelcius\n3.Exit" );
			Scanner scanner=new Scanner(System.in);
			x=scanner.nextInt();
			if(x==1)
			{
				toFahrenheit();
				continue;
			}
			if(x==2)
			{
				toCelcius();
				continue;				
			}
			if(x==3)
				break;
		}
		System.out.println("Good Bye");
	}
}
