package ce1002.s101502513;

import java.util.Scanner; //function call

public class Q1 {
	public static void main(String[] args) {		
		int x;
		double y, temp;
		Scanner input = new Scanner(System.in);
		
		//input repetitively
		do {
		System.out.println("Please choose the method you want to use:");
		System.out.println("1.toFahrenheit");
		System.out.println("2.toCelcius");
		System.out.println("3.Exit");
		x = input.nextInt(); //input
		
		if( x == 1 ) {
			System.out.println("Please input the temperature: ");
			y = input.nextDouble(); //input
			temp = y * 9 / 5 + 32; //transform temperature in Celcius into temperature in Fahrenheit
			System.out.println( y + " in Celcius is equal to " + temp + " in Fahrenheit.");
		}	
		else if( x == 2 ) {
			System.out.println("Please input the temperature: ");
			y = input.nextDouble(); //input
			temp = (y - 32) * 5 / 9; //transform temperature in Fahrenheit into temperature in Celcius
			System.out.println( y + " in Fahrenheit is equal to " + temp + " in Celcius.");
		}	
		else if( x == 3 )
			System.out.println("Good Bye");
		
		System.out.println();
		} while( x == 1 || x == 2 );
	}
}
