package ce1002.s101502501;
import java.util.Scanner;
public class Q1 {
	
	public static void main(String[] args)
	{
			Scanner cin = new Scanner(System.in);
			
			System.out.println("Please choose the method you want to use:");
			System.out.println("1.toFahrenheit");
			System.out.println("2.toCelcius");
			System.out.println("3.Exit");
			int number = cin.nextInt();

			while(number!=3)//輸入不等於3時進入
			{
				if(number == 1)//如果輸入是1執行下面程式
				{
					System.out.println("Please input the temperature:");
					float sir1 = cin.nextInt();//輸入攝氏溫度
					float hua1;
					hua1 = (sir1)*9/5 + 32;//轉換成華氏
					System.out.println( sir1 + " in Celcius is equal to " + hua1 + " in Fahrenheit.");
					System.out.println();				
				}
				else if (number == 2)//如果輸入是2執行下面程式
				{
					System.out.println("Please input the temperature:");
					float hua2 = cin.nextInt();//輸入華氏溫度
					float sir2;
					sir2 = (hua2-32)*5/9;//轉換成攝氏
					System.out.println( hua2 + " in Fahrenheit is equal to " + sir2 + " in Celcius.");	
					System.out.println();
				}
				
				System.out.println("Please choose the method you want to use:");
				System.out.println("1.toFahrenheit");
				System.out.println("2.toCelcius");
				System.out.println("3.Exit");
				number = cin.nextInt();
				
				
			}

			System.out.println("Good Bye");//輸入為3時的結果
		
	}

}