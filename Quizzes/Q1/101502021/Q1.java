package ce1002.s101502021;
import java.util.Scanner;

public class Q1 {
	public static void main(String[] args){
		
		int input_choose;
		double temperature,input_temperature;//轉換過的溫度.
		Scanner inputtion = new Scanner(System.in); //宣告一個掃描的指令來掃入輸入的東西.
		
		while(true){
			System.out.println("Please choose the method you want to use:");
			System.out.println("1.toFahrenheit");
			System.out.println("2.toCelcius");
			System.out.println("3.Exit ");
			
			input_choose=inputtion.nextInt(); //input_choose用來接收輸入的數字.
			
			//如果要將攝氏轉華氏.
			if(input_choose==1){
				System.out.println("Please input the temperature: ");
				input_temperature=inputtion.nextDouble();//輸入一個溫度.
				
				temperature=toFahrenheit(input_temperature);//將溫度進行換算.
				System.out.println(input_temperature+" in Celcius is equal to "+temperature+" in Fahrenheit.\n");
			}
			
			//如果要將華氏轉攝氏.
			else if(input_choose==2){
				System.out.println("Please input the temperature: ");
				input_temperature=inputtion.nextDouble();//輸入一個溫度.
				
				temperature=toCelcius(input_temperature);//將溫度進行換算.
				System.out.println(input_temperature+" in Celcius is equal to "+temperature+" in Fahrenheit.\n");
			}
			
			//如果要結束程式.
			else {
				System.out.println("Good Bye");
				return ;
			}
		}
	}

	public static double toFahrenheit(double input){
		input=(input*9/5)+32;
		return input;
	}
	
	public static double toCelcius(double input){
		input=(input-32)*5/9;
		return input;
	}
	
}
