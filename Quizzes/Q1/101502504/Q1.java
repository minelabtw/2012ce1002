package ce1002.s101502504;
import java.util.Scanner;

public class Q1
{
	public static void main(String[] args)
	{
		Scanner input = new Scanner(System.in); 
		
		int k = 0;
		while ( k == 0 )//loop until the user wants to exit  
		{
			System.out.println("Please choose the method you want to use:");
			System.out.println("1.toFahrenheit");
			System.out.println("2.toCelcius");
			System.out.println("3.Exit ");
			
			int method = input.nextInt();
			//method is the number that user chooses that can be 1 or 2 or 3
			
			if ( method==1 )//changes user input from Celsius to Fahrenheit.
			{
				System.out.println("Please input the temperature: ");
				double C = input.nextInt();//C = user input the temperature - Celsius
				double f = (double) ( C * 9 / 5 + 32 );//calculate Celsius to Fahrenheit
				System.out.println(C+" in Celcius is equal to "+f+" in Fahrenheit.\n");
			}
			
			if ( method==2 )//changes user input from Fahrenheit to Celsius
			{
				System.out.println("Please input the temperature: ");
				double F = input.nextInt();//F = user input the temperature - Fahrenheit
				double c = (double) ( (F - 32) * 5 / 9 );//calculate Fahrenheit to Celsius
				System.out.println(F+" in Fahrenheit is equal to "+c+" in Celcius.\n");
			}
			
			if  ( method==3 )
			{
				System.out.println("Good Bye");
				k = 1;//break from this loop
			}
			
			else if ( method!=1 && method!=2 && method!=3 )//user input error
				System.out.println("Please try again.\nYou have to input 1 or 2 or 3.\n");
			
		}//end while
	}//end main
}