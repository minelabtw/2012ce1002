package ce1002.s101502502;

import java.util.Scanner;

public class Q1 
{
	public static void main(String[] args)
	{	
		int a = 1;//預設一個值使while迴圈可重複執行
		
		while(a==1)
		{
			System.out.println("Please choose the method you want to use:");
			System.out.println("1.toFahrenheit");
			System.out.println("2.toCelcius");
			System.out.println("3.Exit");
			
			Scanner cin = new Scanner(System.in);
			int count = cin.nextInt();//選擇要執行哪個動作
			
			double convert;//儲存轉換後的數字
			
			if(count == 1)//攝氏轉華氏
			{
				System.out.println("Please input the temperature:");
				double temp = cin.nextDouble();
				
				convert = temp * 9 / 5 + 32;
				
				System.out.println(temp + " in Celcius is equal to " + convert + " in Fahrenheit.\n");
			}
		
			if(count == 2)//華氏轉攝氏
			{
				System.out.println("Please input the temperature:");
				double temp = cin.nextDouble();
				
				convert = (temp - 32) * 5 / 9;
				
				System.out.println(temp + " in Fahrenheit is equal to " + convert + " in Celcius.\n");
			}
		
			if(count == 3)//結束程式
			{
				System.out.println("Good Bye");
				break;
			}
			
			
		}
		
	}
	
}