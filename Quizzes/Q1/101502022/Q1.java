package ce1002.s101502022;
import java.util.*;

public class Q1 {
	
	static int a=0;//讓後面可以重複輸入
		
	public static void toFahrenheit(String[] args){//轉成華氏的function
		
		double t1,t2;
		
		System.out.println("Please input the temperature: ");
		Scanner y=new Scanner(System.in);
		t1=y.nextFloat();
		t2=t1*9/5+32;
		System.out.println(t1+" in Celcius is equal to "+t2+" in Fahrenheit.");		
	}
	
	public static void toCelcius(String[] args){//轉成攝氏的function
		
		double t1,t2;
		System.out.println("Please input the temperature: ");
		Scanner z=new Scanner(System.in);
		t2=z.nextFloat();
		t1=(t2-32)*5/9;
		System.out.println(t2+" in Fahrenheit is equal to "+t1+" in Celcius. ");	
	}
	
	public static void main(String[] args){
		
		do{
		    int t=0;
		    
			System.out.println("1.toFahrenheit");
			System.out.println("2.toCelcius");		
			System.out.println("3.Exit ");		
			
			Scanner x=new Scanner(System.in);
			t=x.nextInt();
			
			if(t==1)
			{
				toFahrenheit(args);
			}
			else if(t==2)
			{
				toCelcius(args);
			} 
			else 
			{
				System.out.println("Good Bye");	
				break;
			}
		}while(a==0);
		
	}

}