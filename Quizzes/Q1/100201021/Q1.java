package ce1002.s100201021;
import java.util.Scanner;
public class Q1 {
	public static void main (String[] args){
		Scanner num = new Scanner(System.in);
		int ch=0; //choose
		//if choose =3 ,leave the loop
		while(ch !=3){	
			System.out.println("Please choose the method you want to use:");
			float C,F;
			System.out.println("1.toFahrenheit");
			System.out.println("2.toCelcius");
			System.out.println("3.Exit");
			ch = num.nextInt();	//input the choose
			if(ch == 1){
				System.out.println("Please input the temperature");
				C = num.nextFloat();	//input Celcius temperature
				//output
				System.out.println(C+" in Celcius is equla to "+toF(C)+" in Fahrenheit.");
				System.out.println("");
			}
				
			else if(ch == 2){
				System.out.println("Please input the temperature");
				F = num.nextFloat();	//input Fahrenheit temperature
				//output
				System.out.println(F+" in Fahrenheit is equla to "+toC(F)+" in Celcius.");
				System.out.println("");
			}	
		}
		System.out.println("Good Bye");
	}
	public static float toF (float C) {
		float tof;
		tof = C * 9/5 + 32;
		return tof;
	}
	
	public static float toC (float F) {
		float toc;
		toc = (F-32) *5/9;
		return toc;
	}
}
