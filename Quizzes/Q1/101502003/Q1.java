package ce1002.s101502003;

import java.util.Scanner;

public class Q1 {
	//轉華氏的function
	public static void toFahrenheit(String[] aa)
	{
		//c=輸入的攝氏,y=運算後的華氏 並輸出文字
		Scanner input = new Scanner(System.in);
		System.out.println("Please input the temperature: ");
		double c = input.nextDouble();
		double y=c*9/5+32;
		System.out.println(c + " in Celcius is equal to " + y + " in Fahrenheit." + "\n");
	}
	
	//轉攝氏的function
	public static void toCelcius(String[] bb)
	{
		//f=輸入的華氏,f1=計算後的攝氏,並輸出文字
		Scanner input = new Scanner(System.in);
		System.out.println("Please input the temperature: ");
		double f = input.nextDouble();
		double f1 = (f-32)*5/9;
		System.out.println(f + " in Fahrenheit is equal to " + f1 + " in Celcius." + "\n");
	}
	public static void main(String[] args)
	{
		Scanner input = new Scanner(System.in);
		int a =0;//設一個數方便讓迴圈一直下去
		while(a==0)
		{
			System.out.println("Please choose the method you want to use:");//輸出的文字
			System.out.println("1.toFahrenheit");
			System.out.println("2.toCelcius");
			System.out.println("3.Exit");
			int x = input.nextInt();//輸入選項
			if(x==1)//如果x=1,進入toFahrenheit的function
				toFahrenheit(args);
			else if(x==2)//如果x=2,進入toCelcius的function
				toCelcius(args);
			else //如果x=3,輸出文字並跳出迴圈
			{
				System.out.println("Good Bye");
				break;
			}
		}		
	}
}