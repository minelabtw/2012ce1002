package ce1002.s101502007;

import java.util.Scanner;

public class Q1 {
	public static void main(String[] args)
	{
		int s=1;
		while(s!=0)
		{	
			System.out.println("Please choose the method you want to use:");
			System.out.println("1.toFahrenheit");
			System.out.println("2.toCelcius");
			System.out.println("3.Exit ");
			Scanner input=new Scanner(System.in);
			int x=input.nextInt();
			if (x==1) //1則是要轉成華氏 
			{
				System.out.println("Please input the temperature: ");
				double a=input.nextDouble();
				double e=a*9/5+32; //華氏的公式
				System.out.println(a+" in Celcius is equal to "+e+" in Fahrenheit.");
			}
			else if (x==2)//2則是要轉成攝氏 
			{
				System.out.println("Please input the temperature: ");
				double b=input.nextDouble();
				double f=(b-32)*5/9; //攝氏的公式
				System.out.println(b+" in Fahrenheit is equal to "+f+" in Celcius.");
			}
			else
			{
				System.out.println("Good Bye");
				break; //跳離程式
			}
			System.out.println(" "); //空行
		}
	}

}

