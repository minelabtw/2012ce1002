package ce1002.s100502203;

import java.util.Scanner;

public class Q1 {
	public static void toFahrenheit(double temperature) { // toFahrenheit
		System.out.printf("%.1f in Celcius is equal to %.1f in Fahrenheit\n",
				temperature, (temperature * 9 / 5) + 32); // F = C * (9/5) + 32
	}

	public static void toCelsius(double temperature) { // toCelsius
		System.out.printf("%.1f in Fahrenheit is equal to %.1f in Celcius.\n",
				temperature, (temperature - 32) * 5 / 9); // C = (F �V 32) * 5 / 9
	}

	public static void main(String[] args) {
		int opt;
		Scanner input = new Scanner(System.in);
		System.out.println("Please choose the method you want to use:\n"
				+ "1.toFahrenheit\n" + "2.toCelcius\n" + "3.Exit");
		opt = input.nextInt(); // input option
		
		while (opt != 3) {	// loop to use the functions of inverting temperature (3 to exit)

			switch (opt) {	// functions
			case 1:	// invert to Fahrenheit
				System.out.println("Please input the temperature:");
				Q1.toFahrenheit(input.nextDouble());	
				break;

			case 2:	// invert to Celsius
				System.out.println("Please input the temperature:");	
				Q1.toCelsius(input.nextDouble());	
				break;
			}
			
			System.out.println("");
			System.out.println("");
			System.out.println("");
			System.out.println("Please choose the method you want to use:\n"
					+ "1.toFahrenheit\n" + "2.toCelcius\n" + "3.Exit");
			opt = input.nextInt();
		}
		
		System.out.println("Good Bye");
	}
}
