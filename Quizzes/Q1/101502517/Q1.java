package ce1002.s101502517;
import java.util.Scanner;
public class Q1 {
	public static void main(String[] args){
		Scanner input=new Scanner(System.in);
			double t=0;
		while(true){//迴圈	
			int c=0;
			System.out.println("Please choose the method you want to use: ");
			System.out.println("1.toFahrenheit");
			System.out.println("2.toCelcius");
			System.out.println("3.Exit");
			c = input.nextInt();
			//↓↓↓進行選項判斷
			if(c==3){//離開條件
				System.out.println("Good Bye");
				System.exit(0);
			}
			else{//進行轉換
				System.out.println("Please input the temperature:");
				t = input.nextDouble();
			}
			if(c==1){//ctof
				System.out.println( t +" in Celcius is equal to "+ctf(t)+" in Fahrenheit.");
				System.out.println();
			}
			else{//ftoc
				System.out.println( t+" in Fahrenheit is equal to "+ftc(t)+" in Celcius.");
				System.out.println();
			}
			
		}
		
	}
	
	public static double ftc(double f){//華視轉攝氏
		double c=(f-32)*5/9;
		return c;
	}
	public static double ctf(double c){//攝氏轉華視
		double f=c*9/5+32;
		return f;
	}
}
