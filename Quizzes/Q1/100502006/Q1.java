package ce1002.s100502006;

import java.util.Scanner;

public class Q1 {
	static Scanner input= new Scanner(System.in);//class for input
	public static void main(String[] args){		
		int userChoose=0;//store user's choice
		do {
			System.out.println("Please choose the method you want to use:\n"+
								"1.toFahrenheit\n"+"2.toCelcius\n"+"3.Exit");
			userChoose=input.nextInt();//read in user's choice
			if(userChoose==1){
				toFahrenheit();//to convert Celcius into Fahrenheit
			}
			else if(userChoose==2){
				toCelcius();//to convert Fahrenheit into Celcius
			}
		} while (userChoose!=3);
	}
	private static void toFahrenheit(){//function to convert Celcius into Fahrenheit
		double a=0,b=0;
		System.out.println("Please input the temperature:");
		a= input.nextDouble();//read in a temperature
		b= (a*9.0)/5.0+32.0;//convert into Fahrenheit
		System.out.println(a+" in Celcius is equal to "+b+" in Fahrenheit.\n");
	}
	private static void toCelcius(){//function to convert Fahrenheit into Celcius
		double a=0,b=0;
		System.out.println("Please input the temperature:");
		a= input.nextDouble();//read in a temperature
		b= (a-32.0)*5.0/9.0;//convert into Celcius
		System.out.println(a+" in Fahrenheit is equal to "+b+" in Celcius.\n");
	}
}

