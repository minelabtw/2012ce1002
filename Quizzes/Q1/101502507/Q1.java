//攝氏華氏溫度轉換
package ce1002.s101502507;

import java.util.Scanner;

public class Q1 
{
	public static void main(String[] args)
	{
		int c=1;
		while(c==1)
		{
			//輸出問題
			System.out.println("Please choose the method you want to use:");
			System.out.println("1.toFahrenheit");
			System.out.println("2.toCelcius");
			System.out.println("3.Exit");
			
			Scanner a = new Scanner(System.in);
			int i = a.nextInt();		
			
			double k;
			
			if(i==1)//將攝氏轉成華氏
			{
				System.out.println("Please input the temperature:");
				double j = a.nextInt();
				k = j*9/5+32;			
				System.out.println(j+" in Celcius is equal to "+k+" in Fahrenheit.\n");	
			}
			
			if(i==2)//將華氏轉成攝氏
			{
				System.out.println("Please input the temperature:");
				double j = a.nextInt();
				k = (j-32)*5/9;			
				System.out.println(j+" in Fahrenheit is equal to "+k+" in Celcius.\n");	
			}
			
			if(i==3)//離開換算
			{
				System.out.println("Good Bye");	
				c=0;
			}			
		}
	} 
}

