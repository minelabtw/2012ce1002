package ce1002.s101502004;
import java.util.Scanner;

public class Q1 {
	public static void toFahrenheit(double t2){//轉成華氏的function
		t2=t2*9/5+32;//運算轉換
	}
	public static void toCelcius(double t2){//轉成攝氏的function
		t2= (t2-32)*5/9;//運算轉換
	}
	public static void main(String[] args){
		Scanner inp = new Scanner(System.in);
		System.out.println("Please choose the method you want to use:");
		System.out.println("1.toFahrenheit");
		System.out.println("2.toCelcius");
		System.out.println("3.Exit");
		int i = inp.nextInt();//輸入
		if(i==1){//若是輸入1則進入
			System.out.println("Please input the temperature");
			double t = inp.nextDouble();//輸入溫度
			System.out.println(t+" in Celcius is equal to "+(t*9/5+32)+" in Fahrenheit");//輸出
		}
		if(i==2){//若是輸入2則進入
			System.out.println("Please input the temperature");
			double t = inp.nextDouble();//輸入溫度
			System.out.println(t+" in Fahrenheit is equal to "+((t-32)*5/9)+" in Celcius");//輸出
		}
		if(i==3){//若是輸入3則進入
			System.out.println("Good Bye");//輸出
		}
	}
}
