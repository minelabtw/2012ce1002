package ce1002.s101502027;
import java.util.*;

public class Q1 {
	public static void tc (String[] args){//轉成攝氏
		System.out.println("Please input the temperature: ");
		Scanner n=new Scanner(System.in);
		double a,b;
		a=n.nextDouble();
		b=(a-32)*5/9;
		System.out.print(a);
		System.out.print(" in Fahrenheit is equal to ");
		System.out.print(b);
		System.out.println(" in Celcius.");
	}
	public static void tf (String[] args){//轉成華氏
		System.out.println("Please input the temperature: ");
		Scanner n=new Scanner(System.in);
		double a,b;
		a=n.nextDouble();
		b=a*9/5+32;
		System.out.print(a);
		System.out.print(" in Celcius is equal to ");
		System.out.print(b);
		System.out.println(" in Fahrenheit.");
	}
	public static void main (String[] args){
		int s1,a2=0; 
		while (a2==0){//進入一個迴圈
			System.out.println("1.toFahrenheit");
			System.out.println("2.toCelcius");
			System.out.println("3.Exit ");
			
			Scanner n1=new Scanner(System.in);
			s1= n1.nextInt();
		
			if (s1==3){
				System.out.println("Good Bye");
				break;
			}
			else if (s1==2){
				tc(args);//呼叫函式
			}
			else{
				tf(args);//呼叫函式
			}
		}
		
		
		
	}

}
