package ce1002.s101502002;

import java.util.Scanner;
public class Q1 {
	public static void main(String[]args){ 
		int method=0 ;//選項
		while (method!=3)//重複輸入迴圈，若為選項3則不進入迴圈
		{
			System.out.println("Please choose the method you want to use:\n1.toFahrenheit\n2.toCelcius\n3.Exit");//輸出文字
			Scanner input =new Scanner(System.in);
			method =input.nextInt();//輸入選項
			if (method==1)//選項1
			{
				System.out.println("Please input the temperature:");
				double i=input.nextDouble();//輸入攝氏溫度
				System.out.println(i+" in Celcius is equal to "+toFahrenheit(i) +" in Fahrenheit.\n");//執行函式並輸出
			}
			else if (method ==2)//選想2
			{
				System.out.println("Please input the temperature:");
				double i=input.nextDouble();
				System.out.println(i+" in Fahrenheit is equal to "+toCelcius(i) +" in Celcius.\n");
			}
			else//選項3
			{
				System.out.println("Good Bye");
				break;//離開迴圈
			}
		}
	
	}
	public static double toFahrenheit(double a){//攝轉華的函式
		double answer =a*9/5+32;
		return answer;//傳回華氏溫度
	}
	public static double toCelcius(double a){//華轉攝的函式
		double answer = (a-32)*5/9;
		return answer;//傳回攝氏溫度
	}
	

	

}
