package ce1002.s100502205;

import java.util.Scanner;

public class Q1 {
	public static double toFahrenheit(double C) {
		return C * (9.0 / 5.0) + 32;
	}

	public static double toCelcius(double F) {
		return (F - 32) * 5.0 / 9.0;
	}

	public static void main(String[] args) {
		Scanner cin = new Scanner(System.in);
		String cmd;
		while (true) {
			System.out.println("Please choose the method you want to use:");
			System.out.println("1.toFahrenheit");
			System.out.println("2.toCelcius");
			System.out.println("3.Exit");
			cmd = cin.next();
			if (cmd.equals("3"))
				break;
			System.out.println("Please input the temperature:");
			double num = cin.nextDouble();
			if (cmd.equals("1")) {
				System.out.print(num + " in Celcius is equal to "
						+ toFahrenheit(num) + " in Fahrenheit.");
			} else if (cmd.equals("2")) {
				System.out.print(num + " in Fahrenheit is equal to "
						+ toCelcius(num) + " in Celcius.");
			}
			System.out.println("\n");
		}
		System.out.println("Good bye");
	}
}
