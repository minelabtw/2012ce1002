package ce1002.s101502008;
import java.util.Scanner;
public class Q1 {
	public static void toFahrenheit(String[] args){//宣告轉華氏的函式
		double c,f;
		System.out.println("Please input the temperature: ");
		Scanner input=new Scanner(System.in);
		c=input.nextDouble();
		f=(c*(1.8))+32;
		System.out.println(c+"in Celcius is equal to "+f+" in Fahrenheit.");
	}
	public static void toCelcius(String[] args){//宣告轉攝氏的函式
		double c,f;
		System.out.println("Please input the temperature: ");
		Scanner input=new Scanner(System.in);
		f=input.nextDouble();
		c=(f-32)*5/9;
		System.out.println(f+"in Fahrenheit is equal to "+c+" in Celcius.");
	}
	public static void main(String[] args){//其實搞天還是不知道為何要寫args
		int inputt,check=1;
		while (check==1){
			System.out.println("Please choose the method you want to use:");
			System.out.println("1.toFahrenheit");
			System.out.println("2.toCelcius");
			System.out.println("3.Exit ");
			Scanner input=new Scanner(System.in);//這一行也不知道為何
			inputt=input.nextInt();
			if(inputt==1){
				toFahrenheit(args);
				check=1;
			}
			else if(inputt==2){
				toCelcius(args);
				check=1;
			}
			else if(inputt==3){
				System.out.println("Good Bye");
				check=0;
			}
			System.out.print("\n");
		}
	}
}
