package ce1002.s101502018;
import java.util.*;
public class Q1 {
	public static void toCelcius(){      //轉換程式
		Scanner x=new Scanner(System.in);
		double C,F;
		F=x.nextDouble();
		C =(F-32)*5/9;
		System.out.println(F+" in Celcius is equal to "+C+ " in Fahrenheit.");
	}
	public static void toFahrenheit(){      //轉換程式
		Scanner y=new Scanner(System.in);
		double C,F;
		C=y.nextDouble();
		F= C*1.8+32;
		System.out.println(C+" in Celcius is equal to "+F+ " in Fahrenheit.");
	}
	public static void main(String[] args){
		int b;
		System.out.println("Please choose the method you want to use:");
		System.out.println("1.toFahrenheit");
		System.out.println("2.toCelcius");
		System.out.println("3.Exit ");
		do{
			Scanner a=new Scanner(System.in);
			double C,F;
			b=a.nextInt();
			if(b==1){
				System.out.println("Please input the temperature:");
				toFahrenheit();
			}			
			if(b==2){
				System.out.println("Please input the temperature:");
				toCelcius();
			}
			if(b==3){
				System.out.println("Good Bye");
			}
		}while(b==1 || b==2);      //當b=1或2時 再做一遍
	}
}
