package ce1002.s976002019;

import java.util.Scanner;


public class Q1 {
	public static void main(String arg[]){
		System.out.println("Please choose the method you want to use:");
		System.out.println("1.toFahrenheit");
		System.out.println("2.toCelcius");
		System.out.println("3.Exit");
		
		int meth;/*方法變數*/
		Scanner input= new Scanner(System.in);/*輸入方法*/
		meth=input.nextInt();

		//float F, C;
		switch (meth){		/*依據選擇方法進行處理*/
			case 1:
				toFahrenheit();
				break;
			case 2:
				toCelcius();
				break;
			case 3:
				Exit();
				break;
		}
				
	}
	static void toFahrenheit(){  /*溫度攝氏轉華氏*/
		float F,C;
		System.out.println("Please input the temperature:");
		Scanner Celcius= new Scanner(System.in); /*輸入攝氏溫度*/
		C=Celcius.nextFloat();
		F=C*9/5+32; /*攝氏轉華氏*/
		System.out.println(C+" in Celcius is equal to "+F+" in Fahrenheit.");/*印出華氏溫度*/
	}
	static void toCelcius(){ /*溫度華氏轉攝氏*/
		float F,C;
		System.out.println("Please input the temperature:");
		Scanner Fahrenheit= new Scanner(System.in); /*輸入華氏溫度*/
		F=Fahrenheit.nextFloat();
		C=(F-32)*5/9; /*華氏轉攝氏*/
		System.out.println(F+" in Fahrenheit is equal to "+C+" in Celcius."); /*印出攝氏溫度*/
	}
	static void Exit(){ /*離開*/
		System.out.println("Good Bye"); /*印出Good Bye*/
	}
	

}
