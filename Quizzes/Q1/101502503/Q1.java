package ce1002.s101502503;

import java.util.Scanner;

public class Q1 {
	public static void main(String[] args){
		System.out.println("Please choose the method you want to use:");
		System.out.println("1.toFahrenheit \n2.toCelcius \n3.Exit");

		Scanner cin = new Scanner(System.in);//使用cin為輸入
		float answer;//轉換後的答案
		int choose = cin.nextInt();//輸入選擇
		while(choose!=3)//重複輸入
		{
			System.out.println("Please input the temperature:");
			float tem = cin.nextFloat();
			if(choose==1)//將攝氏轉為華氏
			{
				answer=tem*9/5+32;
				System.out.print(tem);
				System.out.print(" in Celcius is equal to ");
				System.out.print(answer);
				System.out.println(" in Fahrenheit.");
			}
			else if(choose==2)//將華氏轉為攝氏
			{
				answer=(tem-32)*5/9;
				System.out.print(tem);
				System.out.print(" in Fahrenheit is equal to ");
				System.out.print(answer);
				System.out.println(" in Celcius.");
			}		
			System.out.println("\nPlease choose the method you want to use:");
			System.out.println("1.toFahrenheit \n2.toCelcius \n3.Exit");
			choose = cin.nextInt();//輸入選擇
		}
		System.out.print("Good Bye");//結束程式
	}
}
