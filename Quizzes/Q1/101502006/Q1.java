import java.util.Scanner; // program uses class Scanner 

public class Q1
{
	
	public static void main(String[] args)
	{
		// create Scanner to obtain input from command window
		Scanner input = new Scanner( System.in ); 
		
		int casenumber; //used for switch case.
		double temper;
		
		while (true)
		{
			System.out.println("Please choose the method you want to use:\n1.toFahrenheit\n2.toCelcius\n3.Exit ");
			casenumber=input.nextInt();
			
			///////////////Case 1///////////////
			if(casenumber==1)
			{
				System.out.println("Please input the temperature:"); 
				temper=input.nextInt();
				toFahrenheit (temper);
			}
			////////////////////////////////////
			
			///////////////Case 2///////////////
			else if (casenumber==2)
			{
				System.out.println("Please input the temperature:"); 
				temper=input.nextInt();
				toCelcius (temper);
			}
			////////////////////////////////////
			
			///////////////Case 3///////////////
			else
			{
				System.out.println("Good Bye");
				break;
			}
			////////////////////////////////////
		}
		
	}
	
	///////// function to transfer Celcius to Fahrenheit /////////
	public static void toFahrenheit (double temper)
	{
		double result;
		result = temper*9/5 + 32;
		System.out.println(temper + " in Celcius is equal to " + result + " in Fahrenheit.\n");
	}
	//////////////////////////////////////////////////////////////
	
	///////// function to transfer Fahrenheit to Celcius /////////
	public static void toCelcius (double temper)
	{
		double result;
		result = (temper-32)*5/9;
		System.out.println(temper + " in Fahrenheit is equal to " + result+ " in Celcius.\n");
	}
	//////////////////////////////////////////////////////////////
}