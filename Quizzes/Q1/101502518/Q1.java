package ce1002.s101502518;

import java.util.Scanner;

public class Q1 {
	public static void main(String[] args){
			
		while(true)
		{	
			System.out.println("Please choose the method you want to use:");
			System.out.println("1.toFahrenheit");
			System.out.println("2.toCelcius");
			System.out.println("3.Exit");
		
			Scanner input = new Scanner(System.in);		
			int x = input.nextInt();
		
			if(x==1)//from Celsius to Fahrenheit
			{
				System.out.println("Please input the temperature: ");
				double y = input.nextInt();
				double f = y*9/5+32;
				System.out.println( y +" in Celcius is equal to "+ f +" in Fahrenheit.\n");				
			}
			else if(x==2)//from Fahrenheit to Celsius
			{
				System.out.println("Please input the temperature: ");
				double z = input.nextInt();
				double c = (z-32)*5/9;
				System.out.println( z +" in Celcius is equal to "+ c +" in Fahrenheit.\n");
			}
			else//exit
			{
				System.out.println("Good Bye");
				break;
			}
		}		
	}
}
