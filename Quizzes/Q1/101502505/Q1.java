package ce1002.s101502505;
import java.util.Scanner;

public class Q1 {
	public static void main(String[] args){
		Scanner input = new Scanner(System.in);
		for(int k = 0 ; k < 999999999 ; k++)//迴圈
		{
			System.out.println("Please choose the method you want to use:");
			System.out.println("1.toFahrenheit");
			System.out.println("2.toCelcius");
			System.out.println("3.Exit");
			
			
			int i = input.nextInt();//輸入選項
			
			if(i == 1)//C轉F
			{
				System.out.println("Please input the temperature:");
				double C,F;
				C = input.nextInt();
				F = C * 9 / 5 + 32;
				System.out.println(C + " in Celcius is equal to " + F + " in Fahrenheit.");
				System.out.println(" ");
			}
				
			else if(i == 2)//F轉C
			{
				System.out.println("Please input the temperature:");
				double C,F;
				F = input.nextInt();
				C = (F - 32) * 5 / 9;
				System.out.println(F + " in Fahrenheit is equal to " + C + " in Celcius.");
				System.out.println(" ");
			}
				
			else if(i == 3)//結束
			{
				System.out.println("Good Bye");
				break;
			}
		}
	}
}
