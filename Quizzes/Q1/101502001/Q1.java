package ce1002.s101502001;
import java.util.Scanner;

public class Q1 {
	public static void main(String[]args){
		
			System.out.println("Please choose the method you want to use:");
			System.out.println("1.toFahrenheit");
			System.out.println("2.toCelcius");
			System.out.println("3.Exit");
			Scanner input=new Scanner(System.in);//輸入
			int a=input.nextInt();//把輸入的數帶入a
			while(a<=3){
				if(a==1){
					toFahrenheit();//叫出fuction
				}
				else if(a==2){
					toCelcius();//叫出fuction
				}
				else if(a==3){
					System.out.println("Good Bye");
					break;//跳出
				}
				System.out.println("\n");
				System.out.println("Please choose the method you want to use:");
				System.out.println("1.toFahrenheit");
				System.out.println("2.toCelcius");
				System.out.println("3.Exit");
				Scanner input2=new Scanner(System.in);//輸入
				int b=input.nextInt();//把輸入的數帶入b
				a=b;			
			}
		}
		
	public static void toFahrenheit(){
		System.out.println("Please input the temperature:");
		Scanner input2=new Scanner(System.in);//輸入
		int c=input2.nextInt();//把輸入的數帶入c
		System.out.print(c);
		System.out.print(" in Celcius is equal to ");
		System.out.print(c*9/5+32);
		System.out.println(" in Fahrenheit.");	
	}
	public static void toCelcius(){
		System.out.println("Please input the temperature:");
		Scanner input3=new Scanner(System.in);//輸入
		int f=input3.nextInt();//把輸入的數帶入f
		System.out.print(f);
		System.out.print(" in Fahrenheit is equal to ");
		System.out.print((f-32)*5/9);
		System.out.println(" in Celcius.");
	}
}
