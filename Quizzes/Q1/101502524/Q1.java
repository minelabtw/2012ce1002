package ce1002.s101502524;
import java.util.Scanner;

public class Q1 {
	
	public static double toF(double T)//將Celcius換成Fahrenheit
	{
		double T1;
		T1=T*9/5+32;
		return T1;
	}
	public static double toC(double T)//將Fahrenheit換成Celcius
	{
		double T1;
		T1=(T-32)*5/9;
		return T1;
	}
	public static void main(String[] agrs)
	{
		while(true)//設定無限迴圈
		{
			Scanner input=new Scanner(System.in);
			int a;
			double T;
			System.out.println("Please choose the method you want to use:");
			System.out.println("1.toFahrenheit");
			System.out.println("2.toCelcius");
			System.out.println("3.Exit");
			a=input.nextInt();
			if(a==1)//輸入1，經函式運算後輸出
			{
				System.out.println("Please input the temperature:");
				T=input.nextDouble();
				System.out.println(T +" in Celcius is equal to "+ toF(T) +" in Fahrenheit.");
				System.out.println();
			}
			if(a==2)//輸入2，經函式運算後輸出
			{
				System.out.println("Please input the temperature:");
				T=input.nextDouble();
				System.out.println(T +" in Fahrenheit is equal to "+ toC(T) +" in Celcius.");
				System.out.println();
			}
			if(a==3)//輸入3，跳出迴圈
			{
				System.out.println("Good Bye");
				break;
			}
		}
	}
}
