package ce1002.s101502010;

import java.util.Scanner;

public class Q1 {
	public static void change(int method,double degree){
		if(method==1){
			degree=degree*9/5+32;//將攝氏轉成華氏並輸出
			System.out.println(" in Celcius is equal to "+degree+" in Fahrenheit.\n");
		}
		else{
			degree=(degree-32)*5/9;//將華氏轉成攝氏並輸出
			System.out.println(" in Fahrenheit is equal to "+degree+" in Celcius.\n");
		}
	}
	public static void main(String[] args){
		Scanner input = new Scanner(System.in);
		int method;
		double degree;
		while(true){
			System.out.println("Please choose the method you want to use:");
			System.out.println("1.toFahrenheit\n2.toCelcius\n3.Exit");
			method = input.nextInt();//將輸入字串轉成int型態
			if(method!=1&&method!=2)
				break;
			System.out.println("Please input the temperature: ");
			degree = input.nextDouble();//將輸入字串轉成double型態
			System.out.print(degree);
			change(method,degree);
		}
		System.out.println("Good Bye");
	}
}
