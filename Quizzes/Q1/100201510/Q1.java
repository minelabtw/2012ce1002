package CE1002.s100201510;

import java.util.Scanner ;

public class Q1 {
	public static void main(String[] args) {
		int flag = 0 ;
		double temp = 0;
		
		do{
			System.out.print("Please choose the method you want to use:\n");
			System.out.print("1.toFahrenheit\n2.toCelcius\n3.Exit \n");
			flag = new Scanner(System.in).nextInt();//use to select functions.
			
			if(flag == 1){ // C to F
				System.out.print("Please input the temperature:\n");
				temp = new Scanner(System.in).nextDouble();
				System.out.printf("%1.1f in Celcius is equal to %1.1f in Fahrenheit.\n\n", temp , toFahrenheit(temp) );
				flag = 0;
			}
			if(flag == 2){ // F to C
				System.out.print("Please input the temperature:\n");
				temp = new Scanner(System.in).nextDouble();
				System.out.printf("%1.1f in Fahrenheit is equal to %1.1f in Celcius.\n\n", temp , toCelsius(temp));
				flag = 0;
			}
			if(flag == 3){ // exit loop
				System.out.println("Good Bye");
			}
		}while(flag == 0);
	}
	
	
	public static double toFahrenheit(double CEL){
		return (CEL) * (9.0/5) + 32; // C to F formula 
	}
	public static double toCelsius(double FAH){
		return  (FAH - 32) * (5.0/9); // F to C formula
	}
}
