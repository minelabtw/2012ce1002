package ce1002.s101502014;
import java.util.Scanner; 
public class Q1 {
	public static double toFahrenheit(double x){ //toFahrenheit為攝氏轉華氏的函式
		return x * 1.8 + 32;
	}
	public static double toCelcius(double y){ //toCelcius為攝氏轉華氏的函式
		return (y - 32) * 5 / 9;
	}
	public static void main(String[] args) {
		int m; //m紀錄使用者所選擇的功能代號
		double t; //t記錄使用者所輸入的溫度
		Scanner cin = new Scanner(System.in); //cin為輸入的功能
		do
		{
			System.out.println("Please choose the method you want to use:");
			System.out.println("1.toFahrenheit");
			System.out.println("2.toCelcius");
			System.out.println("3.Exit");
			m = cin.nextInt(); //m讀取使用者輸入的代號
			if(m == 1) //若m為1，則把使用者輸入的溫度從攝氏轉換成華氏，並輸出轉換結果
			{
				System.out.println("Please input the temperature:");
				t = cin.nextDouble();
				System.out.println(t + " in Celcius is equal to " + toFahrenheit(t) + " in Fahrenheit.");
				System.out.println();
			}
			else if(m == 2) //若m為2，則把使用者輸入的溫度從華氏轉換成攝氏，並輸出轉換結果
			{
				System.out.println("Please input the temperature:");
				t = cin.nextDouble();
				System.out.println(t + " in Fahrenheit is equal to " + toCelcius(t) + " in Celcius.");
				System.out.println();
			}
			else //若m為3，則輸出離開訊息，跳出迴圈，結束程式
			{
				System.out.println("Good Bye");
				break;
			}
		}while(true);
	}
}