package ce1002.s101502511;

import java.util.*;

public class Q1 {
	public static void main(String[] args){	
		while(true){	
			System.out.println("Please choose the method you want to use:");
			System.out.println("1.toFahrenheit");
			System.out.println("2.toCelcius");
			System.out.println("3.Exit ");
		
			Scanner cs = new Scanner(System.in);//宣告scanner
			int a;
			a=cs.nextInt();
			
			if(a==1){//執行toFahrenheit
				double c;
				System.out.println("Please input the temperature:");
				c=cs.nextDouble();
				System.out.println(c + " in Celcius is equal to "+ toFahrenheit(c)+(" in Fahrenheit.\n") );
			}
			else if(a==2){//if a=2 ,execute toCelcius
				double f;
				System.out.println("Please input the temperature:");
				f=cs.nextDouble();
				System.out.println(f + " in Fahrenheit is equal to "+ toCelsius(f)+(" in Celcius.\n") );
			}
			else if(a==3){//如果等於3,結束程式
				System.out.println("Good Bye");
				break;//跳出迴圈
			}
		}
	}
	//攝氏轉換華氏的涵式
	public static double toFahrenheit (double c1){
		return c1*9/5+32;
	}
	//華氏轉換設氏的涵式
	public static double  toCelsius (double f1){
		return (f1-32)*5/9;
	}
}

