package ce1002.s101502521;

import java.util.Scanner;

public class Q1 {
	public static void main(String[] args) {
		while(true)
		{
			System.out.println("Please choose the method you want to use:");
			System.out.println("1.toFahrenheit");
			System.out.println("2.toCelcius");
			System.out.println("3.Exit"); // Show message
			Scanner input = new Scanner(System.in);
			int choose = input.nextInt(); // get choose
			if(choose==3)
			{
				System.out.println("Good Bye"); // good bye
				break;
			}
			System.out.println("Please input the temperature:"); // show message
			double in = input.nextDouble(); // get temperature
			if(choose==1)
				System.out.println(in+" in Celcius is equal to "+toFahrenheit(in)+" in Fahrenheit.");
			else if(choose==2)
				System.out.println(in+" in Fahrenheit is equal to "+toCelsius(in)+" in Celcius.");
			System.out.println(); // next line
		}
	}
	
	public static double toFahrenheit(double c) // method to convert c to f
	{
		return c*9/5+32;
	}
	
	public static double toCelsius(double f) // method to convert f to c
	{
		return (f-32)*5/9;
	}
}
