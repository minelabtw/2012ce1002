package ce1002.s101502520;

import java.util.Scanner;

public class Q1 {

	/**
	 * @param args
	 */
	public static void toFahrenheit(double temp)
	{
		//F = C*9/5-32
		double F = 0;
		F = temp * 9 / 5 + 32;
		System.out.println(temp + " in Celcius is equal to " + F + " in Fahrenheit.");
	}
	
	public static void toCelcius(double temp)
	{
		//C = (F-32)*5/9
		double C = 0;
		C = (temp - 32) * 5 / 9; 
		System.out.println(temp + " in Fahrenheit is equal to " + C + " in Celcius.");
	}
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		while(true)
		{
			System.out.println("Please choose the method you want to use:");
			System.out.println("1.toFahrenheit");
			System.out.println("2.toCelcius");
			System.out.println("3.Exit");
			
			Scanner inputScanner = new Scanner(System.in);
			int selection = inputScanner.nextInt();
			double temp = 0;
			
			switch(selection){//select which method to used and call the corresponding function
			case 1 :
				System.out.println("Please input the temprature:");
				temp = inputScanner.nextInt();
				toFahrenheit(temp);
				break;
				
			case 2 :
				System.out.println("Please input the temprature:");
				temp = inputScanner.nextInt();
				toCelcius(temp);
				break;
				
			case 3 :
				System.out.println("Good Bye");
				return;
				
			}
			
			System.out.println();
		}

	}

}
