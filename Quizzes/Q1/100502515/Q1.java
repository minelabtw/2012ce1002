package ce1002.s100502515;

import java.util.Scanner;

public class Q1 {
	public static void main(String[] args) {
		Q1 temp = new Q1();
		Scanner scanner = new Scanner(System.in);
		System.out.println("Please choose the method you want to use:");
		System.out.println("1.toFahrenheit");
		System.out.println("2.toCelcius");
		System.out.println("3.Exit");// Function choice for user.
		int choice = scanner.nextInt();
		switch (choice) {
		case 1:
			System.out.println("Please input the temperature:");
			double c = scanner.nextDouble();
			System.out.println(c + " in Celcius is equal to "
					+ temp.toFahrenheit(c) + " in Fahrenheit.");
			break;
		case 2:
			System.out.println("Please input the temperature:");
			double f = scanner.nextDouble();
			System.out.println(f + " in Fahrenheit is equal to "
					+ temp.toCelcius(f) + " in Celcius.");
			break;

		default:
			System.out.println(temp.exit());
			break;
		}// Function choice set.

	}

	private double toFahrenheit(double c) {
		return c * 9 / 5 + 32;
	}

	private double toCelcius(double f) {
		return (f - 32) * 5 / 9;
	}

	private String exit() {
		return "Good bye";
	}// Function set.
}
