package ce1002.s101502523;
import java.io.*;
public class Q1 {
	public static void main(String[] args) throws IOException{
		BufferedReader sc = new BufferedReader(new InputStreamReader(System.in));
		String str;
		double degree;
		int choice;
		while(true){
			//give a loop
			System.out.println("Please choose the method you want to use:");
			System.out.println("1.toFahrenheit");
			System.out.println("2.toCelcius");
			System.out.println("3.Exit");
			str = sc.readLine();
			choice = Integer.parseInt(str);
			switch(choice){		
			//option
				case 1:	//choice 1
					System.out.println("Please input the temperature:");
					str = sc.readLine();
					degree = Double.parseDouble(str);	//input degree			
					System.out.println(degree + " in Celcius is equal to " + toFahrenheit(degree) + " in Fahrenheit.");
					System.out.println();
					break;
				case 2: //choice 2
					System.out.println("Please input the temperature:");
					str = sc.readLine();
					degree = Double.parseDouble(str);	//input degree
					System.out.println(degree + " in Fahrenheit is equal to " + toCelcius(degree) + " in Celcius.");
					System.out.println();
					break;
				case 3: //choice 3
					System.out.println("Good Bye");
					return; //end the loop
			}
		}
	}
	public static double toFahrenheit(double args){
		//change the Celcius to the Fahrenheit
		return args*9/5+32;
	}
	public static double toCelcius(double args){
		//change the Fahrenheit to the Celcius
		return (args-32)*5/9;
	}
}
