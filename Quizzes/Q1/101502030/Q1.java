package ce1002.s101502030;

import java.util.Scanner;

public class Q1 {
	public static void main(String[] args){
		Scanner scanner=new Scanner(System.in);       //給一個位置讓他放輸入的數字.        
		while(true){
		System.out.println("Please choose the method you want to use:");
		System.out.println("1.toFahrenheit");
		System.out.println("2.toCelcius ");
		System.out.println("3.Exit");
		int	x=scanner.nextInt();								
		if( x==1){											//攝氏轉華氏.
			 System.out.println("Please input the temperature:");
			 double c=scanner.nextInt();
			 System.out.println(c+" in Celcius is equal to "+(c*9/5+32)+" in Fahrenheit.");
		}
		if(x==2){											//華氏轉攝氏.
			 System.out.println("Please input the temperature:");
			 double f=scanner.nextInt();
			 System.out.println(f+" in Fahrenheit is equal to "+(f-32)*5/9+" in Celcius.");
		}
		if(x==3){											//掰掰.
			 System.out.println("Good Bye");
			 break;
		}
	}
  }
}
