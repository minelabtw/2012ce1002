package ce1002.s101502015;

import java.util.Scanner;

public class Q1
{
	private static void toFahreheit(String args[]) //自定義函數c氏轉f氏
	{
		Scanner a1 = new Scanner(System.in);
		double c= a1.nextDouble();
		double f = (c*9/5)+32;
		System.out.println(c +" in Celcius is equal to " + f + " in Fahrenheit.\n");	
	}
	private static void toCelcius(String[] args) //自定義函數f氏轉c氏
	{
		Scanner a1 = new Scanner(System.in);
		double f= a1.nextDouble();
		double c = (f-32)*5/9;
		System.out.println(f +" in Fahrenheit is equal to " + c + " in Celcius.\n");	
	}
	public static void main(String[] args)
	{
		Scanner a1 = new Scanner(System.in);
		System.out.println("Please choose the method you want to use:\n1.toFahrenheit\n2.toCelcius\n3.Exit");
		
		
		int check= a1.nextInt();//輸入判斷選項
		while(check!=3)
		{
			System.out.println("Please input the temperature: ");
			if(check==1)//若要轉f氏
			{
				toFahreheit(args);
			}
			if(check==2)//若要轉c氏
			{
				toCelcius(args);
			}
			System.out.println("Please choose the method you want to use:\n1.toFahrenheit\n2.toCelcius\n3.Exit");
			check= a1.nextInt();
		}

		System.out.println("Good Bye");//程式結束
		
	}
}
