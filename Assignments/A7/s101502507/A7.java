/*有部分語法參考網路
 *包含功能:
 *1.可以連續運算
 *2.連續輸入超過九位數會出現errorr警告
 *3.運算後超過九位數會出現errorr警告
 *4.視窗背景定為淡藍色
 *5.數字顯示部分訂為白色
*/

package ce1002.A7.s101502507;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.*;
import java.awt.Color;
import java.awt.Font;

public class A7 extends JFrame implements ActionListener 
{
	static JButton btAdd = new JButton("+"); //加法
	static JButton btSub = new JButton("-"); //減法
	static JButton btEqu = new JButton("="); //等於
	static JButton btDel = new JButton("C"); //清除
	static JButton bt0 = new JButton("0"); //數字0~9
	static JButton bt1 = new JButton("1");
	static JButton bt2 = new JButton("2");
	static JButton bt3 = new JButton("3");
	static JButton bt4 = new JButton("4");
	static JButton bt5 = new JButton("5");
	static JButton bt6 = new JButton("6");
	static JButton bt7 = new JButton("7");
	static JButton bt8 = new JButton("8");
	static JButton bt9 = new JButton("9");
	static JLabel lab1 = new JLabel("0", JLabel.RIGHT); //Label-顯示數字-數字自右起
	static JLabel lab2 = new JLabel(); //背景
		
	private static final int X0=20,Y0=75; //設定按鈕位置之常數
	private static final int XL=42,YL=30; //設定按鈕長寬之常數
	private static final int XD= 6,YD= 7; //設定按鈕間距之常數
	
	int Number = 0, state = 4, temp;
	long totle = 0;
	String chose = "";

	public static void main(String args[]) 
	{
		A7 frm = new A7("A7");
		frm.setTitle("小算盤"); //標題
		frm.setLocationRelativeTo(null); //置中
		frm.setLayout(null); //不使用版面配置
		frm.setSize(250,300); //視窗大小		
		frm.setVisible(true);
	}
	
	public A7(String pTitle) //安排每個按鈕之大小及位置
	{
		btAdd.setBounds(X0+3*XL+3*XD, Y0+1*YL+1*YD, XL, YL);
		btSub.setBounds(X0+3*XL+3*XD, Y0+0*YL+0*YD, XL, YL);
		btEqu.setBounds(X0+3*XL+3*XD, Y0+2*YL+2*YD, XL, 2*YL+YD);
		btDel.setBounds(X0+2*XL+2*XD, Y0+3*YL+3*YD, XL, YL);
		bt0.setBounds(X0+0*XL+0*XD, Y0+3*YL+3*YD, 2*XL+XD, YL);
		bt1.setBounds(X0+0*XL+0*XD, Y0+2*YL+2*YD, XL, YL);
		bt2.setBounds(X0+1*XL+1*XD, Y0+2*YL+2*YD, XL, YL);
		bt3.setBounds(X0+2*XL+2*XD, Y0+2*YL+2*YD, XL, YL);
		bt4.setBounds(X0+0*XL+0*XD, Y0+1*YL+1*YD, XL, YL);
		bt5.setBounds(X0+1*XL+1*XD, Y0+1*YL+1*YD, XL, YL);
		bt6.setBounds(X0+2*XL+2*XD, Y0+1*YL+1*YD, XL, YL);
		bt7.setBounds(X0+0*XL+0*XD, Y0+0*YL+0*YD, XL, YL);
		bt8.setBounds(X0+1*XL+1*XD, Y0+0*YL+0*YD, XL, YL);
		bt9.setBounds(X0+2*XL+2*XD, Y0+0*YL+0*YD, XL, YL);
		
		btAdd.addActionListener(this); //按下按鈕後會執行actionPerformed程式
		btSub.addActionListener(this);
		btEqu.addActionListener(this);
		btDel.addActionListener(this);
		bt0.addActionListener(this);
		bt1.addActionListener(this);
		bt2.addActionListener(this);
		bt3.addActionListener(this);
		bt4.addActionListener(this);
		bt5.addActionListener(this);
		bt6.addActionListener(this);
		bt7.addActionListener(this);
		bt8.addActionListener(this);
		bt9.addActionListener(this);
		
		add(btAdd); //將按鈕加至版面上
		add(btSub);	
		add(btEqu);
		add(btDel);	
		add(bt0);
		add(bt1);
		add(bt2);
		add(bt3);
		add(bt4);
		add(bt5);
		add(bt6);
		add(bt7);
		add(bt8);
		add(bt9);
		
		lab1.setBackground(Color.white); //輸入數字之視窗為白色
		lab1.setOpaque(true); //調整透光度
		lab1.setBounds(20, 20, 185, 40); //輸入數字之視窗的位置及大小	
		lab1.setFont(new Font("SanSerif", Font.PLAIN, 25)); //字體-粗細正常-大小 	
		add(lab1);	
		
		lab2.setBackground(new Color(225,225,255)); //背景色為淡藍色
		lab2.setOpaque(true); //調整透光度
		lab2.setBounds(0, 0, 250, 300); //label位置及大小(整個視窗背景)	
		add(lab2);	
	}
		
	public void actionPerformed(ActionEvent e)  //按下按鈕後之判斷
	{
		JButton btn = (JButton) e.getSource();
		
		//按下按鈕後取得按鍵顯示之資訊
		if(btn.getLabel() == "+") //加法 
		{
			CheckNumber(); //呼叫檢查數字轉換程式
			state = 0; //加法
		} 
		else if(btn.getLabel() == "-") //減法
		{
			CheckNumber(); //呼叫檢查數字轉換程式
			state = 1;
		} 		
		else if(btn.getLabel() == "=") //等於
		{
			CheckNumber(); //呼叫檢查數字轉換程式
			state = 2;			
		} 
		else if(btn.getLabel() == "C") //清除
		{
			ClearNumber(); //呼叫清除程式
			lab1.setText("0"); //清除後顯示0
		} 
		else 
		{
			chose += btn.getLabel();	
			
			if(chose.length() > 9) //若連續輸入超過九位數則error
			{
				chose = "error";
				lab1.setText(chose);
				totle = 0;	
				chose = "";	
			}			
			else //轉換型態並印出於Label上
			{
				temp = Integer.parseInt(chose);
				chose = Integer.toString(temp);
				lab1.setText(chose);
			}
		}
	}
	
	public void CheckNumber() //檢查是否有輸入數字
	{
		if(chose == "") 
			chose = Long.toString(totle);
		
		Number = Integer.parseInt(chose); //將Label上顯示的數字存給Number
		chose = "";
		NumberText(state); //呼叫運算程式		
		chose = Long.toString(totle);
		
		if(chose.length() > 9) //若運算後的結果超過九位數則error
		{
			chose = "error";
			totle = 0;	
			lab1.setText(chose);
			chose = "";
			state = 4;
		}
		else 
		{
			lab1.setText(Long.toString(totle));
			chose="";
		}		
	}
	
	public void ClearNumber() //按下C後做清除之動作
	{
		totle = 0;	//歸零	
		chose = "";
	}

	public void NumberText(int a)  //運算程式
	{
		switch (a) 
		{
			case 0: //加法
				totle += Number;
				break;
			case 1: //減法
				totle -= Number;				
				break;
			case 2: //等於
				totle = Number;
				break;
			default:
				totle = Number;
				break;
		}
	}
}