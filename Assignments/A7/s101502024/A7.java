package ce1002.A7.s101502024;
//�Ѧ�http://tw.myblog.yahoo.com/jw!FY5bUZSXBEeRr82HNP9Hxg--/article?mid=6&prev=7&next=4

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.*;

public class A7 extends JFrame implements ActionListener {
	private static enum OPType {ADD, SUB}

	private static OPType OP;
	private static JButton bt0 = new JButton("0");
	private JButton bt1 = new JButton("1");
	private JButton bt2 = new JButton("2");
	private JButton bt3 = new JButton("3");
	private JButton bt4 = new JButton("4");
	private JButton bt5 = new JButton("5");
	private JButton bt6 = new JButton("6");
	private JButton bt7 = new JButton("7");
	private JButton bt8 = new JButton("8");
	private JButton bt9 = new JButton("9");
	private JButton btC = new JButton("C");
	private JButton btSUB = new JButton("-");
	private JButton btADD = new JButton("+");
	private JButton btEQU = new JButton("=");
	private JLabel tf1 = new JLabel("",JLabel.RIGHT);
	private static StringBuilder sb = new StringBuilder("");
	private static double temp, answer;

	public static void main(String[] args) {// the main function
		A7 huehuehue = new A7("huehuehue");
		huehuehue.setSize(375, 425);
		huehuehue.setLocation(100, 100);
		huehuehue.setVisible(true);
	}

	public A7(String pTitle) {// set button details
		super(pTitle);
		setLayout(null);
		add(tf1);
		add(bt0);
		add(bt1);
		add(bt2);
		add(bt3);
		add(bt4);
		add(bt5);
		add(bt6);
		add(bt7);
		add(bt8);
		add(bt9);
		add(btSUB);
		add(btADD);
		add(btEQU);
		add(btC);

		tf1.setBounds(50, 20, 260, 40);
		tf1.setFont(new Font("Serif", Font.PLAIN, 40));
		tf1.setBackground(Color.white);
		tf1.setOpaque(true);
		btC.setBounds(190, 280, 50, 50);
		bt0.setBounds(50, 280, 120, 50);
		bt1.setBounds(50, 210, 50, 50);
		bt2.setBounds(120, 210, 50, 50);
		bt3.setBounds(190, 210, 50, 50);
		bt4.setBounds(50, 140, 50, 50);
		bt5.setBounds(120, 140, 50, 50);
		bt6.setBounds(190, 140, 50, 50);
		bt7.setBounds(50, 70, 50, 50);
		bt8.setBounds(120, 70, 50, 50);
		bt9.setBounds(190, 70, 50, 50);
		btSUB.setBounds(260, 70, 50, 50);
		btADD.setBounds(260, 140, 50, 50);
		btEQU.setBounds(260, 210, 50, 120);

		btC.addActionListener(this);
		bt0.addActionListener(this);
		bt1.addActionListener(this);
		bt2.addActionListener(this);
		bt3.addActionListener(this);
		bt4.addActionListener(this);
		bt5.addActionListener(this);
		bt6.addActionListener(this);
		bt7.addActionListener(this);
		bt8.addActionListener(this);
		bt9.addActionListener(this);
		btADD.addActionListener(this);
		btSUB.addActionListener(this);
		btEQU.addActionListener(this);
	}

	public void actionPerformed(ActionEvent event) {//the actions taken after pressing the button
		if (event.getSource() == btC) {
			sb.delete(0, sb.length());
			temp = 0.0;
			answer = 0.0;
		}

		if (event.getSource() == bt0) {
			sb.append("0");
		}
		if (event.getSource() == bt1) {

			sb.append("1");
		}
		if (event.getSource() == bt2) {

			sb.append("2");
		}
		if (event.getSource() == bt3) {

			sb.append("3");
		}
		if (event.getSource() == bt4) {

			sb.append("4");
		}
		if (event.getSource() == bt5) {

			sb.append("5");
		}
		if (event.getSource() == bt6) {

			sb.append("6");
		}
		if (event.getSource() == bt7) {

			sb.append("7");
		}
		if (event.getSource() == bt8) {

			sb.append("8");
		}
		if (event.getSource() == bt9) {

			sb.append("9");
		}
		if (event.getSource() == btADD) {
			temp = Double.parseDouble(sb.toString());
			sb.delete(0, sb.length());
			OP = OPType.ADD;

		}
		if (event.getSource() == btSUB) {
			temp = Double.parseDouble(sb.toString());
			sb.delete(0, sb.length());
			OP = OPType.SUB;
		}
		if (event.getSource() == btEQU) {

			switch (OP) {
			case ADD:
				answer = temp + Double.parseDouble(sb.toString());
				break;
			case SUB:
				answer = temp - Double.parseDouble(sb.toString());
				break;

			default:
				answer = Double.parseDouble(sb.toString());
				break;
			}
			sb.delete(0, sb.length());
			temp = 0.0;
			sb.append(new Double(answer).toString());
		}
		if (sb.length() > 11	)
		{
			tf1.setText("error");
			
		}
		else
			tf1.setText(sb.toString());//show result
	}
}