package ce1002.A7.s101502013;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.*;
public class A7 extends JFrame implements ActionListener{
	static long answer = 0;
	static char action = '=';//紀錄按了甚麼動作鍵
	static int click = 0;//代表+ - 按了幾次  可以預防連按加數字一直往上加
	static JButton[] b1 = new JButton[14];
	static JTextField f1 = new JTextField(10); 
	static String aa = new String("");
	
	public A7(String title) 
	{
		//以下設定frame版面
		//frame
		setTitle(title);
		setSize(200 , 200);
		setLocation(300,300);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		//		


		
		//layout
		setLayout(new GridBagLayout());
		
		
		//顯示欄  排版		
		f1.setEditable(false);
		f1.setHorizontalAlignment(JTextField.RIGHT);
		GridBagConstraints c0 = new GridBagConstraints();
        c0.gridx = 0;
        c0.gridy = 0;
        c0.gridwidth = 4;
        c0.gridheight = 1;
        c0.weightx = 0;
        c0.weighty = 0;
        c0.fill = GridBagConstraints.BOTH;
        c0.anchor = GridBagConstraints.CENTER;
        add(f1, c0);
        
        
        //按鈕排版  從右上角依序 往左下角填滿
        c0.gridwidth = 1;
        c0.gridheight = 1;
        //
        int number=9;
        for (int i=1;i<=3;i++)
			for (int j=2;j>=0;j--)
			{
				c0.gridx = j;
		        c0.gridy = i;
		        b1[number] = new JButton("" + number);
		        add(b1[number],c0);
		        b1[number].addActionListener(this);

		        number--;
			}
        
        //- 與 +的排版
        c0.gridx = 3;
        c0.gridy = 1;
        c0.gridwidth = 1;
        c0.gridheight = 1;
        b1[10] = new JButton("-");
        b1[10].addActionListener(this);
        add(b1[10],c0);
        c0.gridx = 3;
        c0.gridy = 2;
        c0.gridwidth = 1;
        c0.gridheight = 1;
        b1[11] = new JButton("+");
        b1[11].addActionListener(this);
        add(b1[11],c0);
        
		// 0 與 C的	排版
        c0.gridx = 0;
        c0.gridy = 4;
        c0.gridwidth = 2;
        c0.gridheight = 1;
        b1[0] = new JButton("0");
        b1[0].addActionListener(this);
        add(b1[0],c0);
        //
        c0.gridx = 2;
        c0.gridy = 4;
        c0.gridwidth = 1;
        c0.gridheight = 1;
       
        b1[12] = new JButton("C");
        b1[12].addActionListener(this);
        add(b1[12],c0);
        
        // 	= 的排版
        c0.gridx = 3;
        c0.gridy = 3;
        c0.gridwidth = 1;
        c0.gridheight = 2;
        b1[13] = new JButton("=");
        b1[13].addActionListener(this);
        add(b1[13],c0);
        
		//顯示frame
		setVisible(true); // Display the frame
		//
		
		
		
	}
	public static void main(String[] args){
	
		A7 lol = new A7("A7");
		f1.setText("" + answer);
		
		
	}
	
	public void actionPerformed(ActionEvent e) {
		
		
		if (e.getSource() == b1[10])//-鍵
		{
			if (click != 0)
				;
			else if (action == '+')
			{
				answer = answer + Integer.parseInt(f1.getText());
			}
			else if (action == '-')
			{
				answer = answer - Integer.parseInt(f1.getText());
			}
			else
			{
				answer = Integer.parseInt(f1.getText());
			}
			
			f1.setText(""+answer);
			
			action = '-';
			click++;
			aa = "";

			//System.out.println("-");
		}
		else if (e.getSource() == b1[11])//+
		{
			if (click != 0)//正做到click
				;
			else if (action == '+')
			{
				answer = answer + Integer.parseInt(f1.getText());
			}
			else if (action == '-')
			{
				answer = answer - Integer.parseInt(f1.getText());
			}
			else
			{
				answer = Integer.parseInt(f1.getText());
			}
				
			f1.setText(""+answer);
			
			action = '+';
			click++;
			aa = "";
			//System.out.println("+");
		}
		else if (e.getSource() == b1[12])//C
		{
			answer = 0;
			action = 0;		
			aa = "";
			click = 0;
			f1.setText("" + answer);
			//System.out.println("C");
		}
		else if (e.getSource() == b1[13])//=
		{
			if (action == '+')
			{
				answer = answer + Integer.parseInt(f1.getText());
			}
			else if (action == '-')
			{
				answer = answer - Integer.parseInt(f1.getText());
			}
			f1.setText("" + answer);
			aa = "";
			click = 0;
			action = '=';
		}
		
		else if (e.getSource() == b1[0])//以下分別為一到九按鈕
		{
			aa = aa + "0";
			click = 0;
			f1.setText(aa);
		}
		else if (e.getSource() == b1[1])
		{
			aa = aa + "1";
			click = 0;
			f1.setText(aa);
		}
		else if (e.getSource() == b1[2])
		{
			aa = aa + "2";
			click = 0;
			f1.setText(aa);
		}
		else if (e.getSource() == b1[3])
		{
			aa = aa + "3";
			click = 0;
			f1.setText(aa);
		}
		else if (e.getSource() == b1[4])
		{
			aa = aa + "4";
			click = 0;
			f1.setText(aa);
		}
		else if (e.getSource() == b1[5])
		{
			aa = aa + "5";
			click = 0;
			f1.setText(aa);
		}
		else if (e.getSource() == b1[6])
		{
			aa = aa + "6";
			click = 0;
			f1.setText(aa);
		}
		else if (e.getSource() == b1[7])
		{
			aa = aa + "7";
			click = 0;
			f1.setText(aa);
		}
		else if (e.getSource() == b1[8])
		{
			aa = aa + "8";
			click = 0;
			f1.setText(aa);
		}
		else if (e.getSource() == b1[9])
		{
			aa = aa + "9";
			click = 0;
			f1.setText(aa);
		}
		
		if (f1.getText().length() > 9)//超出九位數則顯示ERROR
		{
			f1.setText("error");
		}
		

	}
	

}
