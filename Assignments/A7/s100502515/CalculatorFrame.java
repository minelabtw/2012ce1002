package ce1002.a7.s100502515;

import java.awt.BorderLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JTextArea;

public class CalculatorFrame {
	private JFrame frame = new JFrame("�p��L");
	private JTextArea area = new JTextArea("0");
	private JPanel panel1 = new JPanel();
	private JPanel panel2 = new JPanel();
	private JButton button1 = new JButton("1");
	private JButton button2 = new JButton("2");
	private JButton button3 = new JButton("3");
	private JButton button4 = new JButton("4");
	private JButton button5 = new JButton("5");
	private JButton button6 = new JButton("6");
	private JButton button7 = new JButton("7");
	private JButton button8 = new JButton("8");
	private JButton button9 = new JButton("9");
	private JButton button0 = new JButton("0");
	private JButton buttonC = new JButton("C");
	private JButton buttonEq = new JButton("=");
	private JButton buttonPl = new JButton("+");
	private JButton buttonMi = new JButton("-");
	private int resultNum = 0;
	private int tempNum = 0;
	private boolean isPlus = true;

	public CalculatorFrame() {
		frame.setSize(200, 300);
		frame.setResizable(false);
		frame.setVisible(true);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().add(BorderLayout.NORTH, area);
		setPanelOne();
		setPanelTwo();
		button0.addActionListener(new Button0Listener());
		button1.addActionListener(new Button1Listener());
		button2.addActionListener(new Button2Listener());
		button3.addActionListener(new Button3Listener());
		button4.addActionListener(new Button4Listener());
		button5.addActionListener(new Button5Listener());
		button6.addActionListener(new Button6Listener());
		button7.addActionListener(new Button7Listener());
		button8.addActionListener(new Button8Listener());
		button9.addActionListener(new Button9Listener());
		buttonEq.addActionListener(new ButtonEqListener());
		buttonMi.addActionListener(new ButtonMiListener());
		buttonPl.addActionListener(new ButtonPlListener());
		buttonC.addActionListener(new ButtonCListener());
	}

	private void setPanelOne() {
		panel1.setLayout(new GridLayout(3, 4));
		panel1.add(button7);
		panel1.add(button8);
		panel1.add(button9);
		panel1.add(buttonMi);
		panel1.add(button4);
		panel1.add(button5);
		panel1.add(button6);
		panel1.add(buttonPl);
		panel1.add(button1);
		panel1.add(button2);
		panel1.add(button3);
		panel1.add(buttonEq);
		frame.getContentPane().add(BorderLayout.CENTER, panel1);
		frame.getContentPane().add(BorderLayout.SOUTH, panel2);
	}

	private void setPanelTwo() {
		panel2.setLayout(new GridLayout(1, 2));
		panel2.add(button0);
		panel2.add(buttonC);
	}

	class Button0Listener implements ActionListener {//Listener for 0
		@Override
		public void actionPerformed(ActionEvent e) {
			if (tempNum == 0) {

			} else {
				tempNum *= 10;
				area.setText(Integer.toString(tempNum));

			}
		}
	}

	class Button1Listener implements ActionListener {//Listener for 1
		@Override
		public void actionPerformed(ActionEvent e) {
			if (Math.log10(tempNum) >= 8) {

			} else {
				tempNum *= 10;
				tempNum += 1;
				area.setText(Integer.toString(tempNum));

			}
		}
	}

	class Button2Listener implements ActionListener {//Listener for 2
		@Override
		public void actionPerformed(ActionEvent e) {
			if (Math.log10(tempNum) >= 8) {

			} else {
				tempNum *= 10;
				tempNum += 2;
				area.setText(Integer.toString(tempNum));

			}
		}
	}

	class Button3Listener implements ActionListener {//Listener for 3
		@Override
		public void actionPerformed(ActionEvent e) {
			if (Math.log10(tempNum) >= 8) {

			} else {
				tempNum *= 10;
				tempNum += 3;
				area.setText(Integer.toString(tempNum));

			}
		}
	}

	class Button4Listener implements ActionListener {//Listener for 4
		@Override
		public void actionPerformed(ActionEvent e) {
			if (Math.log10(tempNum) >= 8) {

			} else {
				tempNum *= 10;
				tempNum += 4;
				area.setText(Integer.toString(tempNum));

			}
		}
	}

	class Button5Listener implements ActionListener {//Listener for 5
		@Override
		public void actionPerformed(ActionEvent e) {
			if (Math.log10(tempNum) >= 8) {

			} else {
				tempNum *= 10;
				tempNum += 5;
				area.setText(Integer.toString(tempNum));

			}
		}
	}

	class Button6Listener implements ActionListener {//Listener for 6
		@Override
		public void actionPerformed(ActionEvent e) {
			if (Math.log10(tempNum) >= 8) {

			} else {
				tempNum *= 10;
				tempNum += 6;
				area.setText(Integer.toString(tempNum));

			}
		}
	}

	class Button7Listener implements ActionListener {//Listener for 7
		@Override
		public void actionPerformed(ActionEvent e) {
			if (Math.log10(tempNum) >= 8) {

			} else {
				tempNum *= 10;
				tempNum += 7;
				area.setText(Integer.toString(tempNum));

			}
		}
	}

	class Button8Listener implements ActionListener {//Listener for 8
		@Override
		public void actionPerformed(ActionEvent e) {
			if (Math.log10(tempNum) >= 8) {

			} else {
				tempNum *= 10;
				tempNum += 8;
				area.setText(Integer.toString(tempNum));

			}
		}
	}

	class Button9Listener implements ActionListener {//Listener for 9
		@Override
		public void actionPerformed(ActionEvent e) {
			if (Math.log10(tempNum) >= 8) {

			} else {
				tempNum *= 10;
				tempNum += 9;
				area.setText(Integer.toString(tempNum));

			}
		}
	}

	class ButtonCListener implements ActionListener {
		@Override
		public void actionPerformed(ActionEvent e) {
			tempNum = 0;
			resultNum = 0;
			area.setText(Integer.toString(resultNum));
		}
	}

	class ButtonPlListener implements ActionListener {//Listener for Plus
		@Override
		public void actionPerformed(ActionEvent e) {
			if ((tempNum + resultNum) > 999999999) {
				area.setText("ERROR");
				tempNum = 0;
				resultNum = 0;
			} else {
				resultNum += tempNum;
				tempNum = 0;
				area.setText(Integer.toString(resultNum));
			}
		}
	}

	class ButtonMiListener implements ActionListener {//Listener for Minus
		@Override
		public void actionPerformed(ActionEvent e) {
			isPlus = false;
			if ((tempNum - resultNum) < -999999999) {
				area.setText("ERROR");
				tempNum = 0;
				resultNum = 0;
			} else {
				resultNum -= tempNum;
				tempNum = 0;
				area.setText(Integer.toString(resultNum));
			}
		}
	}

	class ButtonEqListener implements ActionListener {//Listener for equal
		@Override
		public void actionPerformed(ActionEvent e) {
			if ((tempNum + resultNum) > 999999999
					|| (tempNum - resultNum) < -999999999) {
				area.setText("ERROR");
				tempNum = 0;
				resultNum = 0;
			} else if (isPlus) {
				resultNum += tempNum;
				tempNum = 0;
				area.setText(Integer.toString(resultNum));
			} else {
				resultNum -= tempNum;
				tempNum = 0;
				area.setText(Integer.toString(resultNum));
			}
		}
	}
}
