package ce1002.A7.s101502501;
//可以連續加,無法連續減,無法加了又減,減了又加,按=後無法繼續計算,需要按c才能算出正確答案
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.*;

public class A7 extends JFrame implements ActionListener 
{
	private JButton one = new JButton("1");private JButton two = new JButton("2");
	private JButton three = new JButton("3");private JButton four = new JButton("4");
	private JButton five = new JButton("5");private JButton six = new JButton("6");
	private JButton seven = new JButton("7");private JButton eight = new JButton("8");
	private JButton nine = new JButton("9");private JButton zero = new JButton("0");
	private JButton sub = new JButton("-");private JButton plus = new JButton("+");
	private JButton get = new JButton("=");private JButton clear = new JButton("C");
	private JTextField text1 = new JTextField(20);
    private static int num = 0, result=0;//num是目前數字,result是結果
    boolean boolPlus = true; boolean boolSub = true;
    boolean boolGet = true; boolean boolClear = true;

    private String str = new String();
	private int[] array = new int[10000];
	private int count = 0;//用來算有無溢位

	public static void main(String[] args) {
		A7 hw = new A7("小算盤");//視窗最上面那條顯示的字
		hw.setSize(300, 350);//視窗大小
		hw.setLocation(300, 350);
		hw.setVisible(true);//顯示視窗(如果是false就不會顯示)
	}
	public A7(String pTitle) {
		super(pTitle);
		setLayout(null);
		text1.setBounds(20, 20, 256, 50);//那一條白色的
		seven.setBounds(20, 80, 60, 40);//距左,距上,寬,高
		eight.setBounds(85, 80, 60, 40);nine.setBounds(150, 80, 60, 40);
		sub.setBounds(215, 80, 60, 40);
		four.setBounds(20, 130, 60, 40);five.setBounds(85, 130, 60, 40);
		six.setBounds(150, 130, 60, 40);plus.setBounds(215, 130, 60, 40);
		one.setBounds(20, 180, 60, 40);two.setBounds(85, 180, 60, 40);
		three.setBounds(150, 180, 60, 40);
		zero.setBounds(20, 230, 125, 40);clear.setBounds(150, 230, 60, 40);
		get.setBounds(215, 180, 60, 90);
		text1.setHorizontalAlignment(JTextField.RIGHT);
		
		one.addActionListener(this);//按按鈕後執行下面那些actionPerformed
		two.addActionListener(this);three.addActionListener(this);
		four.addActionListener(this);five.addActionListener(this);
		six.addActionListener(this);seven.addActionListener(this);
		eight.addActionListener(this);nine.addActionListener(this);
		zero.addActionListener(this);sub.addActionListener(this);
		plus.addActionListener(this);clear.addActionListener(this);
		get.addActionListener(this);

		add(text1);//新增在視窗裡		
		add(seven);add(eight);add(nine);add(sub);add(four);add(five);add(six);
		add(plus);add(one);add(two);add(three);add(clear);add(zero);add(get);	
	}

	public void actionPerformed(ActionEvent e) {
		
		if(e.getSource()==one||e.getSource()==two
				||e.getSource()==three||e.getSource()==four
				||e.getSource()==five||e.getSource()==six
				||e.getSource()==seven||e.getSource()==eight
				||e.getSource()==nine||e.getSource()==zero)
		{
			if(e.getSource()==one){//按1
				if(boolPlus == false||boolClear== false||boolGet== false){
					text1.setText("1");//顯示1
					num = 1;
					array[count]=1;
					count++;
					boolPlus = true;
					boolClear = true;
					boolGet = true;
				}	
				else if(boolSub == false){
					text1.setText("1");
					num = -1;
					array[count]=-1;
					count++;
					boolClear = true;
					boolGet = true;
					boolSub = true;
				}				
				else{//不是第一個位數時
					if(num>=0){
						text1.setText(text1.getText()+"1");
						num = num*10+1;
						array[count]=1;
						count++;
						text1.setText(" "+num);				
					}
					else{
						text1.setText(text1.getText()+"1");
						num = num*10-1;
						array[count]=1;
						count++;
					}
				}
			}
			else if(e.getSource()==two){
				if(boolPlus == false||boolClear== false||boolGet== false){
					text1.setText("2");
					num = 2;
					array[count]=2;
					count++;
					boolPlus = true;
					boolClear = true;
					boolGet = true;
				}	
				else if(boolSub == false){
					text1.setText("2");
					num = -2;
					array[count]=-2;
					count++;
					boolClear = true;
					boolGet = true;
					boolSub = true;
				}	
				else{
					if(num>=0){
					text1.setText(text1.getText()+"2");
					num = num*10+2;
					array[count]=2;
					count++;
					text1.setText(" "+num);						
					}
					else{
						text1.setText(text1.getText()+"2");
						num = num*10-2;
						array[count]=2;
						count++;
					}
				}		
			}
			else if(e.getSource()==three){
				if(boolPlus == false||boolClear== false||boolGet== false){
					text1.setText("3");
					num = 3;
					array[count]=3;
					count++;
					boolPlus = true;
					boolClear = true;
					boolGet = true;
				}
				else if(boolSub == false){
					text1.setText("3");
					num = -3;
					array[count]=-3;
					count++;
					boolClear = true;
					boolGet = true;
					boolSub = true;	
				}

				else{
					if(num>=0){
					text1.setText(text1.getText()+"3");
					num = num*10+3;
					array[count]=3;
					count++;
					text1.setText(" "+num);				
					}
					else{
						text1.setText(text1.getText()+"3");
						num = num*10-3;
						array[count]=3;
						count++;
					}
				}


			}
			else if(e.getSource()==four){
				if(boolPlus == false||boolClear== false||boolGet== false){
					text1.setText("4");
					num = 4;
					array[count]=4;
					count++;
					boolPlus = true;
					boolClear = true;
					boolGet = true;
				}
				else if(boolSub == false){
					text1.setText("4");
					num = -4;
					array[count]=-4;
					count++;
					boolClear = true;
					boolGet = true;
					boolSub = true;	
				}
				else{
					if(num>=0){
					text1.setText(text1.getText()+"4");
					num = num*10+4;
					array[count]=4;
					count++;
					text1.setText(" "+num);				
					}
					else{
						text1.setText(text1.getText()+"4");
						num = num*10-4;
						array[count]=4;
						count++;
					}
				}

			}
			else if(e.getSource()==five){
				if(boolPlus == false||boolClear== false||boolGet== false){
					text1.setText("5");
					num = 5;
					array[count]=5;
					count++;
					boolPlus = true;
					boolClear = true;
					boolGet = true;
				}	
				else if(boolSub == false){
					text1.setText("5");
					num = -5;
					array[count]=-5;
					count++;
					boolClear = true;
					boolGet = true;
					boolSub = true;	
				}
				else{
					if(num>=0){
					text1.setText(text1.getText()+"5");
					num = num*10+5;
					array[count]=5;
					count++;
					text1.setText(" "+num);				
					}
					else{
						text1.setText(text1.getText()+"5");
						num = num*10-5;
						array[count]=5;
						count++;
					}
				}
			
			}
			else if(e.getSource()==six){
				if(boolPlus == false||boolClear== false||boolGet== false){
					text1.setText("6");
					num = 6;
					array[count]=6;
					count++;
					boolPlus = true;
					boolClear = true;
					boolGet = true;
				}
				else if(boolSub == false){
					text1.setText("6");
					num = -6;
					array[count]=-6;
					count++;
					boolClear = true;
					boolGet = true;
					boolSub = true;	
				}
				else {
					if(num>=0){
					text1.setText(text1.getText()+"6");
					num = num*10+6;
					array[count]=6;
					count++;
					text1.setText(" "+num);				
					}
					else{
						text1.setText(text1.getText()+"6");
						num = num*10-6;
						array[count]=6;
						count++;
					}
					
				}
			}
			else if(e.getSource()==seven){
				if(boolPlus == false||boolClear== false||boolGet== false){
					text1.setText("7");
					num = 7;
					array[count]=7;
					count++;
					boolPlus = true;
					boolClear = true;
					boolGet = true;
				}	
				else if(boolSub == false){
					text1.setText("7");
					num = -7;
					array[count]=-7;
					count++;
					boolClear = true;
					boolGet = true;
					boolSub = true;	
				}
				else{
					if(num>=0){
					text1.setText(text1.getText()+"7");
					num = num*10+7;
					array[count]=7;
					count++;
					text1.setText(" "+num);				
					}
					else{
						text1.setText(text1.getText()+"7");
						num = num*10-7;
						array[count]=7;
						count++;
					}
				}
			}
			else if(e.getSource()==eight){
				if(boolPlus == false||boolClear== false||boolGet== false){
					text1.setText("8");
					num = 8;
					array[count]=8;
					count++;
					boolPlus = true;
					boolClear = true;
					boolGet = true;
				}	
				else if(boolSub == false){
					text1.setText("8");
					num = -8;
					array[count]=-8;
					count++;
					boolClear = true;
					boolGet = true;
					boolSub = true;	
				}
				else{
					if(num>=0){
					text1.setText(text1.getText()+"8");
					num = num*10+8;
					array[count]=8;
					count++;
					text1.setText(" "+num);				
					}
					else{
						text1.setText(text1.getText()+"8");
						num = num*10-8;
						array[count]=8;
						count++;
					}
				}			
			}
			else if(e.getSource()==nine){
				if(boolPlus == false||boolClear== false||boolGet== false){
					text1.setText("9");
					num = 9;
					array[count]=9;
					count++;
					boolPlus = true;
					boolClear = true;
					boolGet = true;
				}
				else if(boolSub == false){
					text1.setText("9");
					num = -9;
					array[count]=-9;
					count++;
					boolClear = true;
					boolGet = true;
					boolSub = true;	
				}
				else{
					if(num>=0){
					text1.setText(text1.getText()+"9");
					num = num*10+9;
					array[count]=9;
					count++;
					text1.setText(" "+num);				
					}
					else{
						text1.setText(text1.getText()+"9");
						num = num*10-9;
						array[count]=9;
						count++;
					}
				}		
			}
			else if(e.getSource()==zero){
				if(count!=0){
					if(boolPlus == false||boolClear== false||boolGet== false){
						text1.setText("0");
						num = 0;
						array[count]=0;
						count++;
						boolPlus = true;
						boolClear = true;
					}	
					else if(boolSub == false){
						text1.setText("0");
						num = 0;
						array[count]=0;
						count++;
						boolClear = true;
						boolGet = true;
						boolSub = true;	
					}
					else{
						text1.setText(text1.getText()+"0");
						num = num*10+0;
						array[count]=0;
						count++;
						text1.setText(" "+num);
					}					
				}
					
			}
			if(count>9)
				text1.setText("error");
		}

		else if(e.getSource()==sub||e.getSource()==plus
				||e.getSource()==get){
			if(e.getSource()==plus){//加
				if(boolGet==false){
					text1.setText(" "+result);
					result = num;
					
				}
				else{
					boolPlus = false;
					result += num;
					text1.setText(" "+result);					
				}	
			}
			else if(e.getSource()==sub){//減
				if(boolGet==false){
					boolSub = true;
					text1.setText(" "+result);
					result = num;
				}
				else{
					boolSub = false;
					result += num;
					text1.setText(" "+result);					
				}			
			}
			else if(e.getSource()==get){//等於
				if(boolPlus == false){
					boolGet = false;
					result += num;
					text1.setText(" "+result);					
				}
				else if(boolSub == false){
					boolGet = false;
					result -= num;
					text1.setText(" "+result);
				}
				else if(boolPlus == true){
					boolGet = false;
					result += num;
					text1.setText(" "+result);
					num = result;	
					
				}
				else if(boolSub == true){
					boolGet = false;
					result += num;
					text1.setText(" "+result);
					num = result;	
				}

							
			}
			
		}
		if(e.getSource()==clear){//全部歸零
			boolClear = false;
			text1.setText("0");
			num = 0;
			for(int i=0; i<count; i++){//清空陣列
				array[i] = 0;
			}		
			count = 0;
			result = 0;		
		}
	}
}