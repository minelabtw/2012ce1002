//有一小部分參考自http://blog.xuite.net/ray00000test/blog/21527129-(AWT)%E5%B0%8F%E8%A8%88%E7%AE%97%E6%A9%9F
package ce1002.A7.s101502524;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import java.io.IOException;

public class A7 extends JFrame implements ActionListener
{

	private JButton digits[]=new JButton[10];
	private JButton c,pl,mi,eq;
	private Label tf=new Label("0.",Label.RIGHT);
	private JPanel P;
	private int save=0,sum=0,check=0,save2=0,save3=0;
	public A7()
	{
		setVisible(true);
		setSize(300,400);//視窗大小
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setResizable(false);//設定使視窗不可改變大小
		setLayout(null);
		P=new JPanel(new GridLayout(3,3,10,10));//
		P.setBounds(20,80,180,210);//設定位置
		add(P);
		tf.setBounds(20,10,240,60);//設定位置
		tf.setBackground(Color.white);//底色設為白色
		tf.setFont(new Font("sansserif", Font.BOLD, 32));//字體粗體大小
		add(tf);
		
		//新增1-9的按鈕及位置
		for(int i=7;i<=9;i++)
		{
			digits[i]=new JButton(Integer.toString(i));
			P.add(digits[i]);
			digits[i].addActionListener(this);
		}
		for(int i=4;i<=6;i++)
		{
			digits[i]=new JButton(Integer.toString(i));
			P.add(digits[i]);
			digits[i].addActionListener(this);
		}
		for(int i=1;i<=3;i++)
		{
			digits[i]=new JButton(Integer.toString(i));
			P.add(digits[i]);
			digits[i].addActionListener(this);
		}
		//新增其他的按鈕及位置
		digits[0]=new JButton("0");
		add(digits[0]);
		digits[0].setBounds(20, 299, 115, 63);
		digits[0].addActionListener(this);
		c=new JButton("C");
		add(c);
		c.setBounds(146, 299, 53, 63);
		c.addActionListener(this);
		pl=new JButton("+");
		add(pl);
		pl.setBounds(210, 153, 52, 63);
		pl.addActionListener(this);
		mi=new JButton("-");
		add(mi);
		mi.setBounds(210, 80, 52, 63);
		mi.addActionListener(this);
		eq=new JButton("=");
		add(eq);
		eq.setBounds(210, 226, 52, 136);
		eq.addActionListener(this);
	}
	public static void main(String[] args)
	{
		new A7();
	}
	public void actionPerformed(ActionEvent e) 
	{
		//按下1-9時做出的反應
		for(int i=0;i<=9;i++)
		{
			if(e.getSource()==digits[i])
			{
				if(check==2 || check==3)
				{
					sum=sum*10+i;
					save3=sum;
					sum=(-1)*Math.abs(sum);
					check=3;
				}
				else
				{
					sum=sum*10+i;
					save3=sum;
					check=0;
				}
			}
		}
		//加
		if(e.getSource()==pl)
		{
			check=1;
		}
		//減
		if(e.getSource()==mi)
		{
			check=2;
		}
		//刪除
		if(e.getSource()==c)
		{
			check=0;
			sum=0;
			save=0;
			save2=0;
			save3=0;
		}
		//等於
		if(e.getSource()==eq)
		{
			if(sum!=0)
			{
				save=sum;
				sum=0;
				save2=save2+save;
				save3=save2;
			}
			save=0;
			save2=0;
			check=0;
		}
		if(check==0)
		{
			
		}
		if(check==1)
		{
			if(sum!=0)
			{
				save=sum;
				sum=0;
				save2=save2+save;
				save3=save2;
				check=0;
			}
		}
		if(check==2)
		{
			if(sum!=0)
			{
				save=sum;
				sum=0;
				save2=save2+save;
				save3=save2;
			}
		}
		//偵錯
		if(save3>999999999)
		{
			tf.setText("Error.");
		}
		else
		{
			tf.setText(Integer.toString(save3));
		}
	}
}
