package ce1002.A7.s101502519;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

public class A7 extends JFrame{
	JButton one=new JButton("1");
	JButton two=new JButton("2");
	JButton three=new JButton("3");
	JButton four=new JButton("4");
	JButton five=new JButton("5");
	JButton six=new JButton("6");
	JButton seven=new JButton("7");
	JButton eight=new JButton("8");
	JButton nine=new JButton("9");
	JButton zero=new JButton("0");
	JButton plus=new JButton("+");
	JButton minus=new JButton("-");
	JButton equal=new JButton("=");
	JButton c=new JButton("C");
	
	JLabel textField = new JLabel("0.");
	
	boolean b1,b2,b3,b4,b5,b6,b7,b8,b9,b0,bp,bm,bc,be,p,m,e;
	int i=0;
    long[] n=new long[2];
	String result=null;

	public static void main(String[] args) {
		new A7();
	}
	
	public A7() {       //建構式
		setTitle("A7");

		setLayout(null);        //視窗與排版
		setSize(300, 400);  
		this.getContentPane().setBackground(new Color(231,237,253));
		setVisible(true);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		textField.setOpaque(true);
		textField.setBackground(Color.white);
		textField.setBounds(20, 20, this.getContentPane().getWidth()-40, 40);
		textField.setHorizontalAlignment(JTextField.RIGHT);
		add(textField);	
		one.setBounds(20, 70, 50, 40);

		add(one);
		one.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				// TODO Auto-generated method stub
				b1=true;
				update();
			}
		});
		
		two.setBounds(84, 70, 50, 40);
		add(two);
		two.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				// TODO Auto-generated method stub
				b2=true;
				update();
			}
		});
		
		three.setBounds(148, 70, 50, 40);
		add(three);
		three.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				// TODO Auto-generated method stub
				b3=true;
				update();
			}
		});
		
		four.setBounds(20, 120, 50, 40);
		add(four);
		four.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				// TODO Auto-generated method stub
				b4=true;
				update();
			}
		});
		
		five.setBounds(84, 120, 50, 40);
		add(five);
		five.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				// TODO Auto-generated method stub
				b5=true;
				update();
			}
		});
		
		six.setBounds(148, 120, 50, 40);
		add(six);
		six.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				// TODO Auto-generated method stub
				b6=true;
				update();
			}
		});
		
		seven.setBounds(20, 170, 50, 40);
		add(seven);
		seven.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				// TODO Auto-generated method stub
				b7=true;
				update();
			}
		});
		
		eight.setBounds(84, 170, 50, 40);
		add(eight);
		eight.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				// TODO Auto-generated method stub
				b8=true;
				update();
			}
		});
		
		nine.setBounds(148, 170, 50, 40);
		add(nine);
		nine.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				// TODO Auto-generated method stub
				b9=true;
				update();
			}
		});
		
		zero.setBounds(20, 220, 114, 40);
		add(zero);
		zero.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				// TODO Auto-generated method stub
				b0=true;
				update();
			}
		});
		
		c.setBounds(148, 220, 50, 40);
		add(c);
		c.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				// TODO Auto-generated method stub
				bc=true;
				update();
			}
		});
		
		minus.setBounds(212, 70, 50, 40);
		add(minus);
		minus.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				// TODO Auto-generated method stub
				bm=true;
				
				update();
			}
		});
		
		plus.setBounds(212, 120, 50, 40);
		add(plus);
		plus.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				// TODO Auto-generated method stub
				bp=true;
				
				update();
			}
		});
		
		equal.setBounds(212, 170, 50, 90);
		add(equal);
		equal.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				// TODO Auto-generated method stub
				be=true;
				e=true;
				update();
			}
		});
	}
	
	public void update() {   //各按鈕功能
		
		if(b1){
			if(e){   //按過=則歸零
				n[1]=0;
				n[0]=0;
				e=false;
			}
			n[i]=n[i]*10+1;
			result=Long.toString(n[i]);
			b1=false;
		}
		if(b2){
			if(e){   //按過=則歸零
				n[1]=0;
				n[0]=0;
				e=false;
			}
			n[i]=n[i]*10+2;
			result=Long.toString(n[i]);
			b2=false;
		}
		if(b3){
			if(e){   //按過=則歸零
				n[1]=0;
				n[0]=0;
				e=false;
			}
			n[i]=n[i]*10+3;
			result=Long.toString(n[i]);
			b3=false;
		}
		if(b4){
			if(e){   //按過=則歸零
				n[1]=0;
				n[0]=0;
				e=false;
			}
			n[i]=n[i]*10+4;
			result=Long.toString(n[i]);
			b4=false;
		}
		if(b5){
			if(e){   //按過=則歸零
				n[1]=0;
				n[0]=0;
				e=false;
			}
			n[i]=n[i]*10+5;
			result=Long.toString(n[i]);
			b5=false;
		}
		if(b6){
			if(e){   //按過=則歸零
				n[1]=0;
				n[0]=0;
				e=false;
			}
			n[i]=n[i]*10+6;
			result=Long.toString(n[i]);
			b6=false;
		}
		if(b7){
			if(e){   //按過=則歸零
				n[1]=0;
				n[0]=0;
				e=false;
			}
			n[i]=n[i]*10+7;
			result=Long.toString(n[i]);
			b7=false;
		}
		if(b8){
			if(e){   //按過=則歸零
				n[1]=0;
				n[0]=0;
				e=false;
			}
			n[i]=n[i]*10+8;
			result=Long.toString(n[i]);
			b8=false;
		}
		if(b9){
			if(e){   //按過=則歸零
				n[1]=0;
				n[0]=0;
				e=false;
			}
			n[i]=n[i]*10+9;
			result=Long.toString(n[i]);
			b9=false;
		}
		if(b0){
			if(e){   //按過=則歸零
				n[1]=0;
				n[0]=0;
				e=false;
			}
			n[i]=n[i]*10;
			result=Long.toString(n[i]);
			b0=false;
		}
		
		if(bc){   //全數歸零
			n[0]=0;
			n[1]=0;
			i=0;
			result=Long.toString(n[i]);
			bc=false;
		}
		
		if(bp){  //按+
			if(e){    
				n[1]=0;    //被加數歸0
				e=false;   
			}	
			if(i==1){     //連續加減
				if(p)
					n[0]+=n[1];
				else
					n[0]-=n[1];
				n[1]=0;
				i=0;
			}
			if(m)
				m=false;
			result=Long.toString(n[0]);
			i+=1;
			p=true;
			bp=false;
		}
		
		if(bm){  //按-
			if(e){
				n[1]=0;   //被減數歸0
				e=false;
			}
			if(i==1){     //連續加減
				if(p)
					n[0]+=n[1];
				else
					n[0]-=n[1];
				n[1]=0;
				i=0;
			}
			if(p)
				p=false;
			result=Long.toString(n[0]);
			i+=1;
			m=true;
			bm=false;
		}
		
		if(be){
			
			if(p){    //按=持續加
				n[0]+=n[1];	
				result=Long.toString(n[0]);	
			}
			else if(m){    //按=持續減
				n[0]-=n[1];	
				result=Long.toString(n[0]);		
			}	
			else if(e) {     //連按=
				
				n[1]=0;
				result=Long.toString(n[0]);
			}
			else {     //完成計算
				n[0]=0;
				n[1]=0;
				result=Long.toString(n[0]);
			}
			i=0;
			
			be=false;
		}
		
		if(n[0]>999999999||n[1]>999999999||n[0]<-999999999||n[1]<-999999999){  //邊界判定
			n[0]=0;
			n[1]=0;
			result="error.";
		}
		
		textField.setText(result+".");    //設置答案
	}	
}
