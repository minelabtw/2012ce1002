package ce1002.A7.s101502012;

import javax.swing.*;

import java.awt.Color;
import java.awt.Font;
import java.awt.event.*;

public class A7 implements ActionListener {

	public JFrame frame;
	public JButton number[] = new JButton[10]; // 為了解決cmd的問題 去網路上發現按鈕可以變陣列
	public Font inputFont = new Font("Consolas", Font.PLAIN, 28);
	public Font numberFont = new Font("Verdana", Font.PLAIN, 14);
	public JTextField input = new JTextField("");
	public int digit = 0, action = 0, first = 0;// action=1時 啟動減法，=2時 啟動加法
	public String History = "";
	public int[] total = new int[3];// 設定三個total 第一個是顯示數字 第二個是被加減數字 第三個是檢查位數用的

	public A7() {
		frame = new JFrame("計算機");
		frame.setSize(250, 280);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);

		// 以下都是按鈕的設定 即設定字型跟windows計算機一樣
		int x = 10, y = 155;
		for (int i = 1; i <= 9; i++) {
			number[i] = new JButton(Integer.toString(i));
			number[i].setActionCommand(Integer.toString(i));
			number[i].addActionListener(this);
			frame.add(number[i]);
			number[i].setFont(numberFont);
			number[i].setBounds(x, y, 45, 30);
			x += 55;
			if (0 == i % 3) {
				y -= 40;
				x = 10;
			}
		}
		number[0] = new JButton("0");
		frame.add(number[0]);
		number[0].setFont(numberFont);
		number[0].setActionCommand("0");
		number[0].addActionListener(this);
		number[0].setBounds(10, 195, 100, 30);
		JButton C = new JButton("C");
		frame.add(C);
		C.setActionCommand("C");
		C.addActionListener(this);
		C.setFont(numberFont);
		C.setBounds(120, 195, 45, 30);
		JButton minus = new JButton("-");
		frame.add(minus);
		minus.setFont(numberFont);
		minus.setActionCommand("-");
		minus.addActionListener(this);
		minus.setBounds(175, 75, 45, 30);
		JButton plus = new JButton("+");
		frame.add(plus);
		plus.setFont(numberFont);
		plus.setActionCommand("+");
		plus.addActionListener(this);
		plus.setBounds(175, 115, 45, 30);
		JButton equal = new JButton("=");
		frame.add(equal);
		equal.setFont(numberFont);
		equal.setActionCommand("=");
		equal.addActionListener(this);
		equal.setBounds(175, 155, 45, 70);

		// JFrame的設定:白色、向右對齊、不可手動編輯
		frame.add(input);
		input.setBounds(10, 15, 210, 50);
		input.setEditable(false);
		input.setText("0.");
		input.setFont(inputFont);
		input.setBackground(Color.white);
		input.setHorizontalAlignment(JLabel.RIGHT);
	}

	public static void main(String[] args) {
		try {
			A7 window = new A7(); // 讀取A7視窗
			window.frame.setVisible(true);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void actionPerformed(ActionEvent arg) {
		String cmd = arg.getActionCommand();
		if (first != 0) { // 檢查是否前次按的按鍵是+或-
			total[0] = 0;
			first = 0;
		}
		if (cmd.equals("0") || cmd.equals("1") || cmd.equals("2")
				|| cmd.equals("3") || cmd.equals("4") || cmd.equals("5")
				|| cmd.equals("6") || cmd.equals("7") || cmd.equals("8")
				|| cmd.equals("9")) {
			total[0] = total[0] * 10 + Integer.valueOf(cmd);
		}
		// 以下是特殊按鈕的處理
		if (cmd.equals("-")) {
			first = 1;// 讓下次把total[0]=0
			if (action == 1)
				total[0] = total[1] - total[0];
			else if (action == 2)
				total[0] += total[1];
			action = 1;
			total[1] = total[0];// 交換兩數
		} else if (cmd.equals("+")) {
			first = 1;
			if (action == 1)
				total[0] = total[1] - total[0];
			else if (action == 2)
				total[0] += total[1];
			action = 2;
			total[1] = total[0];// 交換兩數
		} else if (cmd.equals("=")) {
			if (action == 1)
				total[0] = total[1] - total[0];
			else if (action == 2)
				total[0] += total[1];
			action = 0;
			first = 0;
		} else if (cmd.equals("C")) {// 全部歸0
			total[1] = 0;
			total[0] = 0;
			action = 0;
			first = 0;
		}
		// 結束
		for (total[2] = total[0]; total[2] != 0; digit++) {
			total[2] /= 10;
		}
		// 檢查是否超過int的最大範圍 超過用error顯示
		if (digit > 9) {
			total[0] = 0;
			input.setText("error.");
		} else {
			input.setText(total[0] + ".");
		}
		digit = 0;// 重置
	}

}