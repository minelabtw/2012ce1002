package ce1002.A7.s101502511;

import java.awt.*;
import javax.swing.*;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.*;

public class A7 extends JFrame {
	// ///C要按多次一點,不然有時會出錯?
	private static JButton btn0 = new JButton("0");
	private static JButton btn1 = new JButton("1");
	private static JButton btn2 = new JButton("2");
	private static JButton btn3 = new JButton("3");
	private static JButton btn4 = new JButton("4");
	private static JButton btn5 = new JButton("5");
	private static JButton btn6 = new JButton("6");
	private static JButton btn7 = new JButton("7");
	private static JButton btn8 = new JButton("8");
	private static JButton btn9 = new JButton("9");
	private static JButton btn10 = new JButton("+");
	private static JButton btn11 = new JButton("-");
	private static JButton btn12 = new JButton("C");
	private static JButton btn13 = new JButton("=");
	private static JTextField text = new JTextField(20);
	private static long sum = 0, ans = 0;

	public A7() {

		getContentPane().setBackground(Color.getHSBColor(35, 275, 55));

		Font font = new Font("Serif", Font.PLAIN, 49);
		text.setFont(font);// 改變字型
		text.setHorizontalAlignment(JTextField.RIGHT);// 顯示靠右
		// /////////增加按鈕以及設定大小位置
		add(btn0);// 0
		btn0.setBounds(50, 340, 130, 50);
		add(btn1);// 1
		btn1.setBounds(50, 270, 60, 50);
		add(btn2);// 2
		btn2.setBounds(120, 270, 60, 50);
		add(btn3);// 3
		btn3.setBounds(190, 270, 60, 50);
		add(btn4);// 4
		btn4.setBounds(50, 200, 60, 50);
		add(btn5);// 5
		btn5.setBounds(120, 200, 60, 50);
		add(btn6);// 6
		btn6.setBounds(190, 200, 60, 50);
		add(btn7);// 7
		btn7.setBounds(50, 130, 60, 50);
		add(btn8);// 8
		btn8.setBounds(120, 130, 60, 50);
		add(btn9);// 9
		btn9.setBounds(190, 130, 60, 50);
		add(btn10);// +
		btn10.setBounds(270, 200, 60, 50);
		add(btn11);// -
		btn11.setBounds(270, 130, 60, 50);
		add(btn12);// c
		btn12.setBounds(190, 340, 60, 50);
		add(btn13);// =
		btn13.setBounds(270, 270, 60, 120);
		add(text);
		text.setBounds(50, 40, 280, 70);
		// /////aaddActionListener///////////////
		ButtonListener e1 = new ButtonListener();
		btn0.addActionListener(e1);
		btn1.addActionListener(e1);
		btn2.addActionListener(e1);
		btn3.addActionListener(e1);
		btn4.addActionListener(e1);
		btn5.addActionListener(e1);
		btn6.addActionListener(e1);
		btn7.addActionListener(e1);
		btn8.addActionListener(e1);
		btn9.addActionListener(e1);
		btn10.addActionListener(e1);
		btn11.addActionListener(e1);
		btn12.addActionListener(e1);
		btn13.addActionListener(e1);
		text.setText("0.");

	}

	public static void main(String[] args) {
		A7 frame = new A7();
		frame.setLayout(null);
		frame.setTitle("A7");
		frame.setSize(400, 500);
		frame.setLocationRelativeTo(null);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setVisible(true);
	}

	int fun = 0, judge = 0;// fun=紀錄按哪個功能扭

	class ButtonListener implements ActionListener {
		public void actionPerformed(ActionEvent e) {

			if (e.getSource() == btn0) {// 0
				sum = sum * 10;
			} else if (e.getSource() == btn1) {
				sum = sum * 10 + 1;
			} else if (e.getSource() == btn2) {
				sum = sum * 10 + 2;
			} else if (e.getSource() == btn3) {
				sum = sum * 10 + 3;
			} else if (e.getSource() == btn4) {
				sum = sum * 10 + 4;
			} else if (e.getSource() == btn5) {
				sum = sum * 10 + 5;
			} else if (e.getSource() == btn6) {
				sum = sum * 10 + 6;
			} else if (e.getSource() == btn7) {
				sum = sum * 10 + 7;
			} else if (e.getSource() == btn8) {
				sum = sum * 10 + 8;
			} else if (e.getSource() == btn9) {
				sum = sum * 10 + 9;
			}
			text.setText(Long.toString(sum) + ".");// 顯示目前按的數字
			if (e.getSource() == btn10) {// +
				if (judge == 1) {// 代表按過=又按加,這樣按完=之後再按加才不會改變數值
					text.setText(Long.toString(ans) + ".");
					sum = 0;
					fun = 1;
					ans = ans + sum;
					judge = 0;// 歸零
				} else {
					if (fun == 2) {// 上一次為-的話
						ans = ans - sum;
						text.setText(Long.toString(ans) + ".");
						sum = 0;
					} else {
						ans = ans + sum;
						text.setText(Long.toString(ans) + ".");
						sum = 0;
					}
					fun = 1;
				}
			}
			if (e.getSource() == btn11) {// -
				if (judge == 1) {// 代表按過=又按-,這樣按完=之後再按-才不會改變數值
					text.setText(Long.toString(ans) + ".");
					sum = 0;
					ans = ans - sum;
					judge = 0;
				} else {
					if (ans == 0) {// 第一次按減的話
						text.setText(text.getText());
						ans += sum;
						sum = 0;
					} else {
						if (fun == 1) {// 上一次為+的話
							ans = ans + sum;
							text.setText(Long.toString(ans) + ".");
							sum = 0;
						} else {
							ans = ans - sum;
							text.setText(Long.toString(ans) + ".");
							sum = 0;
						}
					}
				}
				fun = 2;
			}
			if (e.getSource() == btn12) {// C 所有數值歸零
				ans = 0;
				sum = 0;
				fun = 0;
				judge=0;
				text.setText(Long.toString(ans) + ".");
			}
			if (e.getSource() == btn13) {// =
				 
					if (fun == 1) {// 上一個按加的話
						ans = ans + sum;
					} else if (fun == 2) {// 上一個按減的話
						ans = ans - sum;
					}
					text.setText(Long.toString(ans) + ".");
					judge = 1;
					if (ans > 999999999 || ans < -999999999) {// 溢位則error		
						text.setText("error.");
						sum = 0;
						ans = 0;
						fun = 0;
					}
					
					
			}
			if (sum > 999999999 || sum < -999999999) {
				text.setText("error.");
				sum = 0;
				ans = 0;
				fun = 0;
			}
		}
	}
}