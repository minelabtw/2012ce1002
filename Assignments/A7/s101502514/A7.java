package ce1002.A7.s101502514;
import java.awt.Color;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JTextField;
public class A7 extends JFrame implements ActionListener{
	static JButton btn0 = new JButton("0");//new button
	static JButton btn1 = new JButton("1");//new button
	static JButton btn2 = new JButton("2");
	static JButton btn3 = new JButton("3");
	static JButton btn4 = new JButton("4");
	static JButton btn5 = new JButton("5");
	static JButton btn6 = new JButton("6");
	static JButton btn7 = new JButton("7");
	static JButton btn8 = new JButton("8");
	static JButton btn9 = new JButton("9");
	static JButton btnc = new JButton("c");
	static JButton btnd = new JButton("-");
	static JButton btnp = new JButton("+");
	static JButton btne = new JButton("=");
	static int i=0,count=0,result=0,count1=0,x=0,re=0,num=10,y=0,z=0;
	static JTextField text = new JTextField(20);
	static Color c=new Color(235,255,255);//背景色
	public static void main(String[] args) {
		A7 hw = new A7("A7");
		hw.setSize(340, 380);//大小
		hw.setLocation(10, 10);
		hw.setVisible(true);	
		hw.getContentPane().setBackground(c);//背景	
	}
	
	public A7(String pTitle)   {
		super(pTitle);
		setLayout(null);
		text = new JTextField();
		text.setBounds(50,30, 230, 40);//按鈕位置
		btn0.setBounds(50,260 , 110, 50);
		btnc.setBounds(170,260,50,50);
		btn7.setBounds(50,80,50,50);
		btn4.setBounds(50,140,50,50);
		btn1.setBounds(50,200,50,50);
		btn8.setBounds(110,80,50,50);
		btn5.setBounds(110,140,50,50);
		btn2.setBounds(110,200,50,50);
		btn9.setBounds(170,80,50,50);
		btn6.setBounds(170,140,50,50);
		btn3.setBounds(170,200,50,50);
		btnd.setBounds(230,80,50,50);
		btnp.setBounds(230,140,50,50);
		btne.setBounds(230,200,50,110);
		
		btn7.addActionListener(this);//按button 執行動作
		btn0.addActionListener(this);
		btn1.addActionListener(this);
		btn2.addActionListener(this);
		btn3.addActionListener(this);
		btn4.addActionListener(this);
		btn5.addActionListener(this);
		btn6.addActionListener(this);
		btn8.addActionListener(this);
		btn9.addActionListener(this);
		btnp.addActionListener(this);
		btnd.addActionListener(this);
		btnc.addActionListener(this);
		btne.addActionListener(this);
		
		add(text);//新增button
		add(btn0);
		add(btnc);
		add(btn7);
		add(btn4);
		add(btn8);
		add(btn1);
		add(btn2);
		add(btn5);
		add(btn9);
		add(btn6);
		add(btn3);
		add(btnd);
		add(btnp);
		add(btne);
		
		Font font = new Font(" ", Font.ITALIC, 30);//改字型
		text.setBorder(null);//無邊框
		text.setHorizontalAlignment(JTextField.RIGHT);//靠右  
		text.setFont(font);
		text.setText("0.");
	}
	
	public void actionPerformed(ActionEvent arg0) {
		if(arg0.getSource()==btn7){//按7
			if(x>111111112){
				text.setText("error.");//溢位
			}
			else{
				x=x*num+7;
				if(x<999999999){
					text.setText(String.valueOf(x));//顯示
				}
			}	
		}
		
		if(arg0.getSource()==btn0){
			if(x>111111119){
				text.setText("error.");
			}
			else{
				x=x*num+0;
				if(x<999999999){
					text.setText(String.valueOf(x));
				}
			}
		}
		
		if(arg0.getSource()==btn1){
			if(x>111111118){
				text.setText("error.");
			}
			else{
				x=x*num+1;
				if(x<999999999){
					text.setText(String.valueOf(x));
				}
			}
		}
		
		if(arg0.getSource()==btn2){
			if(x>111111117){
				text.setText("error.");
			}
			else{
				x=x*num+2;
				if(x<999999999){
					text.setText(String.valueOf(x));
				}
			}
		}
		
		if(arg0.getSource()==btn3){
			if(x>111111116){
				text.setText("error.");
			}
			else{
				x=x*num+3;
				if(x<999999999){
					text.setText(String.valueOf(x));
				}
			}
		}
		
		if(arg0.getSource()==btn4){
			if(x>111111115){
				text.setText("error.");
			}
			else{
				x=x*num+4;
				if(x<999999999){
					text.setText(String.valueOf(x));
				}
			}
		}
		
		if(arg0.getSource()==btn5){
			if(x>111111114){
				text.setText("error.");
			}
			else{
				x=x*num+5;
				if(x<999999999){
					text.setText(String.valueOf(x));
				}
			}
		}
		
		if(arg0.getSource()==btn6){
			if(x>111111113){
				text.setText("error.");
			}
			else{
				x=x*num+6;
				if(x<999999999){
					text.setText(String.valueOf(x));
				}
			}
		}
		
		if(arg0.getSource()==btn8){
			if(x>111111111){
				text.setText("error.");
			}
			else{
				x=x*num+8;
				if(x<999999999){
					text.setText(String.valueOf(x));
				}
			}
		}
		
		if(arg0.getSource()==btn9){
			if(x>111111110){
				text.setText("error.");
			}
			else{
				x=x*num+9;
				if(x<999999999){
					text.setText(String.valueOf(x));
				}
			}
		}
		
		if(arg0.getSource()==btnp){//plus
			
			if(z==0){
				result=result+x;//result
			}
			
			if(z==2){//-完之後+
				result=result-x;
				if(result>999999999){
					text.setText("error.");
					result=0;
				}
				else
				text.setText(String.valueOf(result));
			}
			
			if(z==1){
				result=result+x;
				if(y>0){
					if(result>999999999){
						text.setText("error.");
						result=0;
					}
					else
					text.setText(String.valueOf(result));
				}
			}
			y++;
			z=1;//判斷數
			x=0;
		}
		
		if(arg0.getSource()==btnd){//減
			if(z==0){
				if(y>0){
					if(result>999999999){
						text.setText("error.");
						result=0;
					}
					else
					text.setText(String.valueOf(result));
					}
				else{
					result=x;
					y++;
				}
			}
			if(z==1){//+完之後-
				
				result=result+x;
				if(result>999999999){
					text.setText("error.");
					result=0;
				}
				else
				text.setText(String.valueOf(result));
			}
			if(z==2){
				result=result-x;
				if(y>0){
					if(result>999999999){
						text.setText("error.");
						result=0;
					}
					else
					text.setText(String.valueOf(result));
					}
				else{
					result=x;
					y++;
				}
			}
			z=2;//判斷
			x=0;//歸零
		}
		
		if(arg0.getSource()==btne){//等於
			if(y>0){
				if(z==1){//+
					result=result+x;
					if(result>999999999){
						text.setText("error.");
						result=0;
					}
					else
					text.setText(String.valueOf(result));
				}
				if(z==2){//-
					result=result-x;
					if(result>999999999){
						text.setText("error.");
						result=0;
					}
					else
					text.setText(String.valueOf(result));
				}
				x=0;//歸零
			}
				y++;
		}
		
		if(arg0.getSource()==btnc){//清除
			result=0;
			x=0;
			text.setText(String.valueOf("0."));
		}
	}
}
