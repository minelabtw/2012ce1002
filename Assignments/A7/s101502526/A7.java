package ce1002.A7.s101502526;

import java.awt.*;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Stack;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JTextField;


/*已經很盡力的DeBug了，但還是有錯誤
  運算盡量和小算盤做的一樣了，包括一些特例
 由於時間不足就只好交了*/

public class A7 extends JFrame implements ActionListener {
	
	JButton b7 = new JButton("7");
	JButton b8 = new JButton("8");
	JButton b9 = new JButton("9");
	JButton bsub = new JButton("-");
	JButton bplus = new JButton("+");
	JButton b6 = new JButton("6");
	JButton b5 = new JButton("5");
	JButton b4 = new JButton("4");
	JButton b3 = new JButton("3");
	JButton b2 = new JButton("2");
	JButton b1 = new JButton("1");
	JButton b0 = new JButton("0");
	JButton bc = new JButton("C");
	JButton bequal = new JButton("=");
	JLabel text = new JLabel("123");
	String array = "";
	int sum1 = 0 , sum2 = 0 , sum3 = 0;
	int i  = 0 ;
	int press = 10 ;
	boolean subfirst = true ;
	boolean equal = false ;
	boolean error = false ;
	int plusorsub = 1 ;
	
	public A7() {                            //基本配置
		
	  	Container c = getContentPane();                    
	    c = this.getContentPane();
	    c.setLayout(null);
		JPanel bot = new JPanel();
		bot.setBounds(30,80,285,250);
		text.setBackground(Color.WHITE);
		text.setOpaque(true);
		text.setBounds(30, 20, 285, 40);
		text.setHorizontalAlignment(JLabel.RIGHT);
		add(text);
		add(bot);
		bot.setLayout(null);
		b7.setBounds(10, 10,50,30);
		b8.setBounds(80, 10,50,30);
		b9.setBounds(150, 10,50,30);
		bsub.setBounds(220, 10,50,30);
		bplus.setBounds(220, 55,50,30);
		b6.setBounds(150, 55,50,30);
		b5.setBounds(80, 55,50,30);
		b4.setBounds(10, 55,50,30);
		b3.setBounds(150, 100,50,30);
		b2.setBounds(80, 100,50,30);
		b1.setBounds(10, 100,50,30);
		b0.setBounds(10, 145,120,30);
		bc.setBounds(150, 145,50,30);
		bequal.setBounds(220, 100,50,75);
		bot.add(b7);
		bot.add(b8);
		bot.add(b9);
		bot.add(bsub);
		bot.add(b4);
		bot.add(b5);
		bot.add(b6);
		bot.add(bplus);
		bot.add(b1);
		bot.add(b2);
		bot.add(b3);
		bot.add(bequal);
		bot.add(b0);
		bot.add(bc);
		text.setText("0.");
		b0.addActionListener(this);
		b1.addActionListener(this);
		b2.addActionListener(this);
		b3.addActionListener(this);
		b4.addActionListener(this);
		b5.addActionListener(this);
		b6.addActionListener(this);
		b7.addActionListener(this);
		b8.addActionListener(this);
		b9.addActionListener(this);
		bc.addActionListener(this);
		bsub.addActionListener(this);
		bplus.addActionListener(this);
		bequal.addActionListener(this);
		
	}


	public static void main(String[] args) {
	    A7 frame = new A7();
	    frame.setTitle("小算盤");
	    frame.setSize(350, 310);
	    frame.setLocationRelativeTo(null); 
	    frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	    frame.setVisible(true);
	    frame.setResizable(false);
	}
	
	  public void actionPerformed(ActionEvent e) { 

			if (e.getSource()== b1)                        //按0~9字串相加  
			{
				array = array  +1 ;
				text.setText(array+'.');
				i+=1;
			}
			if (e.getSource()== b2)                         
			{
				array = array  +2 ;
				text.setText(array+'.');
				i+=1;
			}
			if (e.getSource()== b3)                         
			{
				array = array  +3 ;
				text.setText(array+'.');
				i+=1;
			}
			if (e.getSource()== b4)                         
			{
				array = array  +4 ;
				text.setText(array+'.');
				i+=1;
			}
			if (e.getSource()== b5)                         
			{
				array = array  +5 ;
				text.setText(array+'.');
				i+=1;
			}
			if (e.getSource()== b6)                         
			{
				array = array  +6 ;
				text.setText(array+'.');
				i+=1;
			}
			if (e.getSource()== b7)                         
			{
				array = array  +7 ;
				text.setText(array+'.');
				i+=1;
			}
			if (e.getSource()== b8)                         
			{
				array = array  +8 ;
				text.setText(array+'.');
				i+=1;
			}
			if (e.getSource()== b9)                         
			{
				array = array  +9 ;
				text.setText(array+'.');
				i+=1;
			}
			if (e.getSource()== b0)                  		  //若0為第一個則不加     
			{
				if (i!=0)
				{
					array = array  +0 ;
					text.setText(array+'.');
					i+=1;
				}
			}
			if (e.getSource()== bplus)                       //若按+，則變數為1  
			{												 //原本和後輸入的1倍運算，所以相加
				if (error == false)							 //然後顯示結果，在將結果放到另一個變數中
				{											 //且得知按過加
					plusorsub=1;
					if (equal==true)
					{
						array = "";
						equal = false;
					}
					if (i!=0)
					{
						sum2 = Integer.parseInt(array); 
						sum3 = sum1 += sum2*plusorsub;
						text.setText(""+sum3+'.');
						sum1 = sum3 ;
						i=0;
						array = "";
						press =1;
					}
				}
			}
			if (e.getSource()== bsub)                         
			{
				if (error == false)								   //若按-，則變數為-1  
				{											   		//原本和後輸入的-1倍運算，所以相減
					plusorsub=-1;		
					if (equal==true)								//然後顯示結果，在將結果放到另一個變數中
					{
						array = "";
						equal = false;
					}
					if (i==0 && equal ==false)					   //若直接按負則為0-輸入數
					{
						text.setText(""+sum3+'.');
						i=0;
						array = "";
						press = 2;
					}
					else if (i!=0)                                 //若第一次按-則不顯示負值(因為0-輸入數)
					{
						if (subfirst==true && equal ==false)
						{
							sum3 = Integer.parseInt(array); 
							text.setText(""+sum3+'.');
							sum1 = sum3 ;
							i=0;
							array = "";
							press = 2;
							subfirst = false;
						}
						else 
						{
							sum2 = Integer.parseInt(array); 
							sum3 = sum1 += sum2*plusorsub;
							text.setText(""+sum3+'.');
							sum1 = sum3 ;
							i=0;
							array = "";
							press = 2;
						}
					}
				}
			}
			if (e.getSource()== bequal)                         
			{
				if (i != 0)  									//若尚未按+-直接按=則清空
				{
					if (press ==10 || equal == true)
					{
						array = "";
						i = 0 ;
					}
				}
				if (press!=10)                                   //一直按=則一直運算，變數判斷正或負
				{
					sum2 = Integer.parseInt(array); 
					sum3 = sum1 += sum2*plusorsub;
					text.setText(""+sum3+'.');
					sum1 = sum3 ;
					i=0;
					press = 4 ;
					equal = true ;
				}
			}
			
			if (e.getSource()== bc)                    	//清空     
			{
				array = "";
				sum1 = 0 ;
				sum2 = 0 ;
				sum3 = 0 ;
		  		text.setText("0."); 
		  		i =  0 ;
				subfirst = true;
				equal = false ;
				error = false  ;
			}
		  	if (i>9||sum3>999999999)					//超過
		  	{
		  		text.setText("error.");
				array = "";
				sum1 = 0 ;
				sum2 = 0 ;
				sum3 = 0 ;
				subfirst = true;
				equal = false ;
				error = true ;
		  	}
	  }
}

