package ce1002.A7.s101502556;

import java.awt.*;
import java.awt.event.*;
public class A7
{
	private static int ans=0;
	private static Frame frm=new Frame("小算盤");
    private static Panel pn1=new Panel(new GridLayout(3,3));
    private static Panel pn2=new Panel();
    private static Label lab=new Label("0",Label.RIGHT);
    
    //依序為清除、加、減、乘、除、等於
    private static Button cn,ad,sub,mul,div,amo,bot0;
    //0~9的數字
    private static Button digits[]=new Button[10];
    private static long num;//存放結果
    private static byte op;//代表運算子
    public static int sum=0,mode=1;
	public static void main(String args[])
	{
	    frm.setLayout(null);
	    frm.setBounds(450,250,200,230);
	    frm.setResizable(false);
	    frm.setBackground(new Color(85,170,255));
	   // frm.setBackground(new Color(0,0,50));
	    lab.setBounds(20,30,160,20);
	    lab.setText("");
	    lab.setBackground(new Color(255,255,255));
	    //lab.setBackground(new Color(240,220,190));
	    pn1.setBounds(20,60,120,105);
	    //0~9數字鈕
	    for(int i=9;i>0;i--){
	        digits[i]=new Button(Integer.toString(i));
	        pn1.add(digits[i]);
	        digits[i].addActionListener(new ActLis());
	    }
	    
	    sub=new Button("=");
	    sub.setBounds(150, 130, 35, 65);
	    sub.addActionListener(new ActLis());
	    
	    ad=new Button("+");
	    ad.setBounds(150, 95, 35, 35);
	    ad.addActionListener(new ActLis());
	    
	    amo=new Button("-");
	    amo.setBounds(150, 60,35 ,35);
	    amo.addActionListener(new ActLis());
	    
	    bot0=new Button("0");
	    bot0.setBounds(20, 160, 80, 35);
	    bot0.addActionListener(new ActLis());
	    
	    cn=new Button("C");
	    cn.setBounds(100, 160, 40, 35);
	    cn.addActionListener(new ActLis());
	    
	    frm.addWindowListener(new WindowAdapter(){public void
	    windowClosing(WindowEvent e){System.exit(0);}});   
	    frm.add(lab);
	    frm.add(pn1);
	    frm.add(amo);
	    frm.add(ad);
	    frm.add(sub);
	    frm.add(bot0);
	    frm.add(cn);
	    frm.setVisible(true);
	}
	public static class ActLis implements ActionListener
	{
	    public void actionPerformed(ActionEvent e)throws NumberFormatException,ArithmeticException{
	    	if((lab.getText()).length()<9)
	    	{
	    		if (e.getSource()==bot0)
	    			lab.setText(lab.getText()+"0");
	    		if (e.getSource()==digits[1])
	    			lab.setText(lab.getText()+"1");
	    		if (e.getSource()==digits[2])
	    			lab.setText(lab.getText()+"2");
	    		if (e.getSource()==digits[3])
	    			lab.setText(lab.getText()+"3");
	    		if (e.getSource()==digits[4])
	    			lab.setText(lab.getText()+"4");
	    		if (e.getSource()==digits[5])
	    			lab.setText(lab.getText()+"5");
	    		if (e.getSource()==digits[6])
	    			lab.setText(lab.getText()+"6");
	    		if (e.getSource()==digits[7])
	    			lab.setText(lab.getText()+"7");
	    		if (e.getSource()==digits[8])
	    			lab.setText(lab.getText()+"8");
	    		if (e.getSource()==digits[9])
	    			lab.setText(lab.getText()+"9");
	    	}
	    	
	    	if (e.getSource()==ad)
	    	{
	    		sum=Integer.parseInt(lab.getText());
	    		lab.setText("");
	    		mode=0;
	    	
	    	}
	    	if(e.getSource()==amo)
	    	{
	    		sum=Integer.parseInt(lab.getText());
	    		lab.setText("");
	    		mode=1;
	    	}
	    	if(e.getSource()==cn)
	    	{
	    		lab.setText("");
	    		sum=0;
	    		ans=0;
	    	}
	    		
	    	if (e.getSource()==sub)
	    	{
	    		
	    		if (mode==0)
	    		{
	    			ans=sum+Integer.parseInt(lab.getText());
	    		}	
	    		else if (mode==1)
	    		{
	    			ans=sum-Integer.parseInt(lab.getText());
	    				
	    		}
	    		lab.setText(""+ans);
	    		mode=-1;
	    		if((lab.getText()).length()>9)
	    		{
	    				lab.setText("error.");
	    		}
	    	}	
	    }
	}
}
