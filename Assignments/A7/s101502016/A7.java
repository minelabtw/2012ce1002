package ce1002.a7.s101502016;


//有參考網路上的程式碼

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;

public class A7 extends JFrame implements ActionListener{
        
    JTextField numberfield;
    String numberstring="";
    int number;//存取目前為止的值
    String error = "error";
    double result;
    
    char buttonname[]={'7','8','9','-',
                       '4','5','6','+',
                       '1','2','3','=',
                       '0','c'
                       };
                       
    JButton button[]=new JButton[14];
    
    char operator;
   
    public A7()
    {        
        super("小算盤");
 
        Container c=getContentPane();//連接到contentpanel
        c.setLayout(new BorderLayout(20,10));
 
        JPanel p=new JPanel();
 
        numberfield=new JTextField("0");
        numberfield.setHorizontalAlignment(JTextField.RIGHT);//text的內容靠右
 
        p.setLayout(null);
      
        for(int i=0;i<14;i++){
            button[i]=new JButton(buttonname[i]+"");
            button[i].addActionListener(this);
            p.add(button[i]);
        }//設置button
        
        button[0].setBounds(20,0,45,45);
        button[1].setBounds(70,0,45,45);
        button[2].setBounds(120,0,45,45);
        button[3].setBounds(170,0,45,45);
        button[4].setBounds(20,50,45,45);
        button[5].setBounds(70,50,45,45);
        button[6].setBounds(120,50,45,45);
        button[7].setBounds(170,50,45,45);
        button[8].setBounds(20,100,45,45);
        button[9].setBounds(70,100,45,45);
        button[10].setBounds(120,100,45,45);
        button[11].setBounds(170,100,45,95);
        button[12].setBounds(20,150,95,45);
        button[13].setBounds(120,150,45,45);
        

        
        c.add("North",numberfield);//(放置位置,物件)
        c.add("Center",p);
      
        setSize(260,300);
        setVisible(true);
        setLocationRelativeTo(null);
    }
 
    public void actionPerformed(ActionEvent event)
    {   
    	if((JButton)event.getSource()== button[13])
    		numberfield.setText("0");
    		
    	if(((JButton)event.getSource()).getText().charAt(0)<='9'&&
                 ((JButton)event.getSource()).getText().charAt(0)>='0'){
            numberstring+=((JButton)event.getSource()).getText().charAt(0)+"";//後面""使其數字轉為字串
            numberfield.setText(numberstring);//顯示出目前儲存的字串
        }else{

            numberstring="";
            numberfield.setText(operation(number,Double.parseDouble(numberfield.getText()),operator));//用來計算並顯示的函式
            operator=((JButton)event.getSource()).getActionCommand().charAt(0);//operator判斷我點+-的哪一個符號
   
            if(((JButton)event.getSource()).getText().charAt(0)!='=')                  
                number=(int) Double.parseDouble(numberfield.getText());     
        } //點擊0~9以外的按鈕產生的行為
    }
   
    public String operation(double number1,double number2,char operator)
    {      
    	switch(operator)
        {        
            case '+':
            	 result =  number1+number2;
            	 break;
            case '-':
                 result =  number1-number2;
                 break;
            default:
                 result =  number2;
                 break;
        }
    	
    	if(result>999999999)
    		return "error";
    	else
    		return ""+result;
    }
       
    public static void main(String args[])
    {      
        A7 a=new A7();
        a.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);        
    }
 
}
	


