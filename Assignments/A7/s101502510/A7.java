package ce1002.A7.s101502510;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.*;

public class A7 extends JFrame implements ActionListener {
	JButton b0 = new JButton("0");
	JButton b1 = new JButton("1");
	JButton b2 = new JButton("2");
	JButton b3 = new JButton("3");
	JButton b4 = new JButton("4");
	JButton b5 = new JButton("5");
	JButton b6 = new JButton("6");
	JButton b7 = new JButton("7");
	JButton b8 = new JButton("8");
	JButton b9 = new JButton("9");
	JButton btnadd = new JButton("+");
	JButton btnsub = new JButton("-");
	JButton c = new JButton("C");
	JButton ans = new JButton("=");
	Label text = new Label("0", Label.RIGHT);
	int cal = 0, n = 2, m = 0;
	int result = 0, num = 0;
	boolean start = true;

	public A7() {
		setLayout(null);
		text.setBackground(Color.WHITE);
		text.setFont(new Font("SansSerif", Font.PLAIN, 30));
		b0.setBounds(50, 240, 95, 45);
		b1.setBounds(50, 190, 45, 45);
		b2.setBounds(100, 190, 45, 45);
		b3.setBounds(150, 190, 45, 45);
		b4.setBounds(50, 140, 45, 45);
		b5.setBounds(100, 140, 45, 45);
		b6.setBounds(150, 140, 45, 45);
		b7.setBounds(50, 90, 45, 45);
		b8.setBounds(100, 90, 45, 45);
		b9.setBounds(150, 90, 45, 45);
		btnadd.setBounds(200, 140, 45, 45);
		btnsub.setBounds(200, 90, 45, 45);
		c.setBounds(150, 240, 45, 45);
		ans.setBounds(200, 190, 45, 95);
		text.setBounds(50, 40, 195, 40);

		add(b0);
		add(b1);
		add(b2);
		add(b3);
		add(b4);
		add(b5);
		add(b6);
		add(b7);
		add(b8);
		add(b9);
		add(btnadd);
		add(btnsub);
		add(c);
		add(ans);
		add(text);

		b0.addActionListener(this);
		b1.addActionListener(this);
		b2.addActionListener(this);
		b3.addActionListener(this);
		b4.addActionListener(this);
		b5.addActionListener(this);
		b6.addActionListener(this);
		b7.addActionListener(this);
		b8.addActionListener(this);
		b9.addActionListener(this);
		btnadd.addActionListener(this);
		btnsub.addActionListener(this);
		c.addActionListener(this);
		ans.addActionListener(this);
	}

	public static void main(String args[]) {
		A7 frame = new A7();
		frame.setTitle("小算盤");
		frame.setSize(300, 350);
		frame.setLocation(400, 250);
		frame.setVisible(true);
	}

	public void actionPerformed(ActionEvent e) {
		if (e.getSource() == b0) {// 數字0
			cal = 0;
			text.setText("" + cal);
			start = false;
		}
		if (e.getSource() == b1) {// 數字1
			cal = cal * 10 + 1;
			num++;// 計算有無溢位
			text.setText("" + cal);
			start = false;
		}
		if (e.getSource() == b2) {// 數字2
			cal = cal * 10 + 2;
			num++;// 計算有無溢位
			text.setText("" + cal);
			start = false;
		}
		if (e.getSource() == b3) {// 數字3
			cal = cal * 10 + 3;
			num++;// 計算有無溢位
			text.setText("" + cal);
			start = false;
		}
		if (e.getSource() == b4) {// 數字4
			cal = cal * 10 + 4;
			num++;// 計算有無溢位
			text.setText("" + cal);
			start = false;
		}
		if (e.getSource() == b5) {// 數字5
			cal = cal * 10 + 5;
			num++;// 計算有無溢位
			text.setText("" + cal);
			start = false;
		}
		if (e.getSource() == b6) {// 數字6
			cal = cal * 10 + 6;
			num++;// 計算有無溢位
			text.setText("" + cal);
			start = false;
		}
		if (e.getSource() == b7) {// 數字7
			cal = cal * 10 + 7;
			num++;// 計算有無溢位
			text.setText("" + cal);
			start = false;
		}
		if (e.getSource() == b8) {// 數字8
			cal = cal * 10 + 8;
			num++;// 計算有無溢位
			text.setText("" + cal);
			start = false;
		}
		if (e.getSource() == b9) {// 數字9
			cal = cal * 10 + 9;
			num++;// 計算有無溢位
			text.setText("" + cal);
			start = false;
		}
		if (e.getSource() == btnadd) {// 加法
			num = 0;
			m = 0;
			calculate(cal);
			cal = 0;
		}
		if (e.getSource() == btnsub) {// 減法
			num = 0;
			m = 1;
			calculate(cal);
			cal = 0;
		}
		if (e.getSource() == c) {// 歸零
			num = 0;
			n = 0;
			calculate(cal);
			cal = 0;
		}
		if (e.getSource() == ans) {// 等於
			num = 0;
			n = 1;
			calculate(cal);
		}
		if (num >= 10) {// 若超過9位數
			text.setText("error.");
			num = result = cal = 0;
			start = true;
		}
	}

	public void calculate(int x) {
		if (n == 1) {
			if (m == 0) {// 加法
				result += x;
				n = 2;
			} else if (m == 1) {// 減法
				result -= x;
				n = 2;
			}
		} else {
			if (start == false && m == 0) {// 連續加法
				result += x;
			} else if (start == false && m == 1) {// 連續減法
				result -= x;
			}
		}

		if (n == 0) {// 歸零
			result = 0;
			n = 2;
			m = 0;
		}

		if (result > 999999999 || result < -999999999) {// 溢位
			text.setText("error.");
			result = cal = 0;
			n = 2;
			m = 0;
			start = true;
		} else {// 若沒溢位
			start = true;
			text.setText("" + result);
		}
	}
}
