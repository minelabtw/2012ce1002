package ce1002.A7.s101502508;

import javax.swing.*;

import java.awt.Graphics;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class A7 extends JFrame implements ActionListener 
{
	String number="" ;//紀錄每次輸入的數值
	int IntNumber=0 ;//轉換成int用
	int CountNum=0 ;//每次加減的總數
	String sign="" ;//判別上一次是加號還是減號
	
	private JButton btn1 = new JButton("1");
	private JButton btn2 = new JButton("2");
	private JButton btn3 = new JButton("3");
	private JButton btn4 = new JButton("4");
	private JButton btn5 = new JButton("5");
	private JButton btn6 = new JButton("6");
	private JButton btn7 = new JButton("7");
	private JButton btn8 = new JButton("8");
	private JButton btn9 = new JButton("9");
	private JButton btn0 = new JButton("0");
	private JButton btnA = new JButton("+");
	private JButton btnB = new JButton("-");
	private JButton btnC = new JButton("c");
	private JButton btnD = new JButton("=");

	private JTextField text = new JTextField(20);
	
	public static void main(String[] args) {
		A7 hw = new A7("小算盤");
		
		hw.setSize(255 , 320);
		hw.setLocation(250, 250);
		hw.setVisible(true);
 
		//JTextField input = new JTextField();
		//input.setHorizontalAlignment(JTextField.RIGHT);
	}
	
	public void actionPerformed(ActionEvent e) {

		if(e.getSource()!=btnA && e.getSource()!=btnB && e.getSource()!=btnC && e.getSource()!=btnD){
			if(e.getSource()==btn0)
				number += 0 ; 
			else if(e.getSource()==btn1 && number.length()<=9)
				number += 1 ;
			else if(e.getSource()==btn2 && number.length()<=9)
				number += 2 ;
			else if(e.getSource()==btn3 && number.length()<=9)
				number += 3 ;
			else if(e.getSource()==btn4 && number.length()<=9)
				number += 4 ;
			else if(e.getSource()==btn5 && number.length()<=9)
				number += 5 ;
			else if(e.getSource()==btn6 && number.length()<=9)
				number += 6 ;
			else if(e.getSource()==btn7 && number.length()<=9)
				number += 7 ;
			else if(e.getSource()==btn8 && number.length()<=9)
				number += 8 ;
			else if(e.getSource()==btn9 && number.length()<=9)
				number += 9 ;
			else if( number.length()>9 ){//判斷如果超過9位數則輸出錯誤
				number="error" ;
			}
		}

		text.setText(number) ;//每次數字輸入在螢幕上呈現
		
		if(e.getSource()==btnA || e.getSource()==btnB || e.getSource()==btnC || e.getSource()==btnD){
			
			//////判斷上一次的輸入是加號還是減號或是這是計算試第一個輸入的數字
			if ( sign=="plus" ){
				IntNumber = Integer.parseInt(number) ;
				CountNum += IntNumber ;
				text.setText(Integer.toString(CountNum)) ;//int轉換String
				number="" ;
			}
			else if ( sign=="subtract" ){
				IntNumber = Integer.parseInt(number) ;
				CountNum -= IntNumber ;
				text.setText(Integer.toString(CountNum)) ;//int轉換String
				number="" ;
			}
			else if (sign==""){
				IntNumber = Math.abs(Integer.parseInt(number)) ;
				CountNum += IntNumber ;
			}

			
			if ( e.getSource()==btnA ){
				sign = "plus" ;
				number= "" ;
			}
			else if ( e.getSource()==btnB ){
				sign = "subtract" ;
				number= "" ;
			}
			else if ( e.getSource()==btnC ){
				CountNum=0 ;
				number="0" ;
				sign="" ;
				text.setText(number) ;
				number="" ;
			}
			else if ( e.getSource()==btnD ){
				number = Integer.toString(CountNum) ;
				if( number.length()>9 ){
					number="error" ;
					text.setText(number) ;
					CountNum=0 ;
					sign="" ;
				}
				else
				    text.setText(number) ;
			}
		}
		
		if( number=="error" )
			number="" ;
	}
	
	public A7(String pTitle) {

		super(pTitle);

		setLayout(null);// 不使用版面配置
		btn0.setBounds(20, 220, 94, 42);//左邊起始位置,高度位置,長,寬
		btn1.setBounds(20, 170, 42, 42);//左邊起始位置,高度位置,長,寬
		btn2.setBounds(72, 170, 42, 42);
		btn3.setBounds(124, 170, 42, 42);//左邊起始位置,高度位置,長,寬
		btn4.setBounds(20, 120, 42, 42);
		btn5.setBounds(72, 120, 42, 42);//左邊起始位置,高度位置,長,寬
		btn6.setBounds(124, 120, 42, 42);
		btn7.setBounds(20, 70, 42, 42);//左邊起始位置,高度位置,長,寬
		btn8.setBounds(72, 70, 42, 42);
		btn9.setBounds(124, 70, 42, 42);//左邊起始位置,高度位置,長,寬
		btnA.setBounds(176, 70, 42, 42);
		btnB.setBounds(176, 120, 42, 42);//左邊起始位置,高度位置,長,寬
		btnC.setBounds(124, 220, 42, 42);
		btnD.setBounds(176, 170, 42, 92);//左邊起始位置,高度位置,長,寬
		
        text.setBounds(20, 20, 199, 40);
        
		btn0.addActionListener(this);
		btn1.addActionListener(this);
		btn2.addActionListener(this);
		btn3.addActionListener(this);
		btn4.addActionListener(this);
		btn5.addActionListener(this);
		btn6.addActionListener(this);
		btn7.addActionListener(this);
		btn8.addActionListener(this);
		btn9.addActionListener(this);
		btnA.addActionListener(this);
		btnB.addActionListener(this);
		btnC.addActionListener(this);
		btnD.addActionListener(this);

        add(text);
        add(btn0);
		add(btn1);
		add(btn2);
		add(btn3);
		add(btn4);
		add(btn5);
		add(btn6);
		add(btn7);
		add(btn8);
		add(btn9);
		add(btnA);
		add(btnB);
		add(btnC);
		add(btnD);
	}

}