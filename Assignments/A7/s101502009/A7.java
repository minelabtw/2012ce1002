package ce1002.A7.s101502009;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JTextField;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class A7 extends JFrame {

	calculate c=new calculate();
	private JPanel contentPane;
	private JTextField textField;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					A7 frame = new A7();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public A7() {
		
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 515, 620);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		textField = new JTextField();
		textField.setBounds(20, 10, 414, 35);
		contentPane.add(textField);
		textField.setColumns(10);
		//------1------
		JButton btnNewButton_1 = new JButton("1");
		btnNewButton_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				c.now(1);
				textField.setText(Integer.toString(c.shownow()));
			}
		});
		btnNewButton_1.setBounds(20, 70, 100, 100);
		contentPane.add(btnNewButton_1);		
		//------1------
		
		//------2------
		JButton btnNewButton_2 = new JButton("2");
		btnNewButton_2.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				c.now(2);
				textField.setText(Integer.toString(c.shownow()));
			}
		});
		btnNewButton_2.setBounds(140, 70, 100, 100);
		contentPane.add(btnNewButton_2);
		//------2------
		
		//------3------
		JButton btnNewButton_3 = new JButton("3");
		btnNewButton_3.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				c.now(3);
				textField.setText(Integer.toString(c.shownow()));
			}
		});
		btnNewButton_3.setBounds(260, 70, 100, 100);
		contentPane.add(btnNewButton_3);
		//------3------
		
		//------4------
		JButton btnNewButton_4 = new JButton("4");
		btnNewButton_4.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				c.now(4);
				textField.setText(Integer.toString(c.shownow()));
			}
		});
		btnNewButton_4.setBounds(20, 190, 100, 100);
		contentPane.add(btnNewButton_4);
		//------4------
		
		//------5------
		JButton btnNewButton_5 = new JButton("5");
		btnNewButton_5.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				c.now(5);
				textField.setText(Integer.toString(c.shownow()));
			}
		});
		btnNewButton_5.setBounds(140, 190, 100, 100);
		contentPane.add(btnNewButton_5);
		//------5------
		
		//------6------
		JButton btnNewButton_6 = new JButton("6");
		btnNewButton_6.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				c.now(6);
				textField.setText(Integer.toString(c.shownow()));
			}
		});
		btnNewButton_6.setBounds(260, 190, 100, 100);
		contentPane.add(btnNewButton_6);
		//------6------
		
		//------7------
		JButton btnNewButton_7 = new JButton("7");
		btnNewButton_7.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				c.now(7);
				textField.setText(Integer.toString(c.shownow()));
			}
		});
		btnNewButton_7.setBounds(20, 310, 100, 100);
		contentPane.add(btnNewButton_7);
		//------7------
		
		//------8------
		JButton btnNewButton_8 = new JButton("8");
		btnNewButton_8.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				c.now(8);
				textField.setText(Integer.toString(c.shownow()));
			}
		});
		btnNewButton_8.setBounds(138, 310, 100, 100);
		contentPane.add(btnNewButton_8);
		//------8------
		
		//------9------
		JButton btnNewButton_9 = new JButton("9");
		btnNewButton_9.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				c.now(9);
				textField.setText(Integer.toString(c.shownow()));
			}
		});
		btnNewButton_9.setBounds(260, 310, 100, 100);
		contentPane.add(btnNewButton_9);
		//------9------
		
		//------0------
		JButton btnNewButton_0 = new JButton("0");
		btnNewButton_0.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				c.now(0);
				textField.setText(Integer.toString(c.shownow()));
			}
		});
		btnNewButton_0.setBounds(20, 430, 220, 100);
		contentPane.add(btnNewButton_0);
		//------0------
		
		//------+------
		JButton btnNewButton_add = new JButton("+");
		btnNewButton_add.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				c.setaddremain();
				if(c.showremain()==1000000000){
					textField.setText("error");
				}
				else{
					textField.setText(Integer.toString(c.showremain()));
				}
			}
		});
		btnNewButton_add.setBounds(380, 70, 100, 100);
		contentPane.add(btnNewButton_add);
		//------+------
		
		//-------------
		JButton btnNewButton_sub = new JButton("-");
		btnNewButton_sub.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				c.setsubremain();
				if(c.showremain()==-1000000000){
					textField.setText("error");
				}
				else{
					textField.setText(Integer.toString(c.showremain()));
				}
				
			}
		});
		btnNewButton_sub.setBounds(380, 190, 100, 100);
		contentPane.add(btnNewButton_sub);
		//-------------
		
		//------=------
		JButton btnNewButton_equ = new JButton("=");
		btnNewButton_equ.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				c.setequremain();
				if(c.showremain()==1000000000||c.showremain()==-1000000000){
					textField.setText("error");
				}
				else{
					textField.setText(Integer.toString(c.showremain()));
				}
				
			}
		});
		btnNewButton_equ.setBounds(380, 310, 100, 220);
		contentPane.add(btnNewButton_equ);
		//------=------
		
		JButton btnC = new JButton("c");
		btnC.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				c.setc();
				textField.setText(Integer.toString(c.showremain()));
				
			}
		});
		btnC.setBounds(260, 430, 100, 100);
		contentPane.add(btnC);
	}
}
