package ce1002.A7.s100201510;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.*;

public class A7 extends JFrame {
	private static final long serialVersionUID = 1L;
	//screen on calculator
	public static JTextField screen = new JTextField(15);
	//number show on the screen
	public static String NScreen = "0";
	//buffa , buffb , store the result temp
	public static int NBuffa = 0;
	public static int NBuffb = 0;
	public static boolean isOperating = true;
	public static boolean moreInput = false;
	public static String operator = "";
	
 	public static void main(String[] args){
		A7 frame = new A7();
		frame.setSize(270, 300);
		frame.setLocationRelativeTo(null);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.add(new Panel());
		frame.setVisible(true);
	}
	
	// set the layout
	public A7(){
		setTitle("�p��L");
		//SpringLayout layout = new SpringLayout();
		Panel mainPNL = new Panel(); // panel contain screen
		Panel numberPNL = new Panel(); // panel contain number buttons
		Panel operationPNL = new Panel(); // panel contain all operations
		// set layout of frame
		setLayout(new BorderLayout(3,1));
		numberPNL.setLayout(new GridLayout(4,3));
		operationPNL.setLayout(new GridLayout(4,1));
		add(mainPNL, BorderLayout.NORTH);
		add(numberPNL , BorderLayout.WEST);
		add(operationPNL ,BorderLayout.EAST); 
		// declare and add all components ,link all to listener
		add(screen);
		screen.setText(NScreen);
		JButton number_0 = new JButton("��");
		number_0.addActionListener(new lis_0());
		JButton number_1 = new JButton("��");
		number_1.addActionListener(new lis_1());
		JButton number_2 = new JButton("��");
		number_2.addActionListener(new lis_2());
		JButton number_3 = new JButton("��");
		number_3.addActionListener(new lis_3());
		JButton number_4 = new JButton("��");
		number_4.addActionListener(new lis_4());
		JButton number_5 = new JButton("��");
		number_5.addActionListener(new lis_5());
		JButton number_6 = new JButton("��");
		number_6.addActionListener(new lis_6());
		JButton number_7 = new JButton("��");
		number_7.addActionListener(new lis_7());
		JButton number_8 = new JButton("��");
		number_8.addActionListener(new lis_8());
		JButton number_9 = new JButton("��");
		number_9.addActionListener(new lis_9());
		//JButton dot = new JButton("�D");
		JButton PLUS = new JButton("��");
		PLUS.addActionListener(new lis_PLUS());
		JButton MINUS = new JButton("��");
		MINUS.addActionListener(new lis_MINUS());
		JButton MULT = new JButton("��");
		MULT.addActionListener(new lis_MULT());
		JButton DIVIDE = new JButton("��");
		DIVIDE.addActionListener(new lis_DIVIDE());
		JButton EQUL = new JButton("��");
		EQUL.addActionListener(new lis_EQUL());
		JButton CLEAR = new JButton("��");
		CLEAR.addActionListener(new lis_CLEAR());
		// add all components to panel
		mainPNL.add(screen);
		numberPNL.add(number_7);
		numberPNL.add(number_8);
		numberPNL.add(number_9);
		numberPNL.add(number_4);
		numberPNL.add(number_5);
		numberPNL.add(number_6);
		numberPNL.add(number_1);
		numberPNL.add(number_2);
		numberPNL.add(number_3);
		numberPNL.add(number_0);
		//numberPNL.add(dot);
		operationPNL.add(CLEAR);
		operationPNL.add(PLUS);
		operationPNL.add(MINUS);
		operationPNL.add(MULT);
		operationPNL.add(DIVIDE);
		operationPNL.add(EQUL);
	}
}

// all listeners
//listeners for number
class lis_0 implements ActionListener{
	public void actionPerformed(ActionEvent arg0) {
		if(A7.NScreen != "0" && (A7.NScreen).length() <= 9)
			A7.NScreen += "0";
		(A7.screen).setText(A7.NScreen);
	}
}
class lis_1 implements ActionListener{
	public void actionPerformed(ActionEvent arg0) {
		if(A7.NScreen == "0")
			A7.NScreen = "1";
		else if((A7.NScreen).length() < 9)
			A7.NScreen += "1";
		(A7.screen).setText(A7.NScreen);
		
	}
}
class lis_2 implements ActionListener{
	public void actionPerformed(ActionEvent arg0) {
		if(A7.NScreen == "0")
			A7.NScreen = "2";
		else if((A7.NScreen).length() < 9)
			A7.NScreen += "2";
		(A7.screen).setText(A7.NScreen);
	}
}
class lis_3 implements ActionListener{
	public void actionPerformed(ActionEvent arg0) {
		if(A7.NScreen == "0")
			A7.NScreen = "3";
		else if((A7.NScreen).length() <= 9)
			A7.NScreen += "3";
		(A7.screen).setText(A7.NScreen);
	}
}
class lis_4 implements ActionListener{
	public void actionPerformed(ActionEvent arg0) {
		if(A7.NScreen == "0")
			A7.NScreen = "4";
		else if((A7.NScreen).length() < 9)
			A7.NScreen += "4";
		(A7.screen).setText(A7.NScreen);
	}
}
class lis_5 implements ActionListener{
	public void actionPerformed(ActionEvent arg0) {
		if(A7.NScreen == "0")
			A7.NScreen = "5";
		else if((A7.NScreen).length() < 9)
			A7.NScreen += "5";
		(A7.screen).setText(A7.NScreen);
	}
}
class lis_6 implements ActionListener{
	public void actionPerformed(ActionEvent arg0) {
		if(A7.NScreen == "0")
			A7.NScreen = "6";
		else if((A7.NScreen).length() < 9)
			A7.NScreen += "6";
		(A7.screen).setText(A7.NScreen);
	}
}
class lis_7 implements ActionListener{
	public void actionPerformed(ActionEvent arg0) {
		if(A7.NScreen == "0")
			A7.NScreen = "7";
		else if((A7.NScreen).length() < 9)
			A7.NScreen += "7";
		(A7.screen).setText(A7.NScreen);
	}
}
class lis_8 implements ActionListener{
	public void actionPerformed(ActionEvent arg0) {
		if(A7.NScreen == "0")
			A7.NScreen = "8";
		else if((A7.NScreen).length() < 9)
			A7.NScreen += "8";
		(A7.screen).setText(A7.NScreen);
	}
}
class lis_9 implements ActionListener{
	public void actionPerformed(ActionEvent arg0) {
		if(A7.NScreen == "0")
			A7.NScreen = "9";
		else if((A7.NScreen).length() < 9)
			A7.NScreen += "9";
		(A7.screen).setText(A7.NScreen);
	}
}

//listeners for operations
// PLUS , MINS , MULT , DIVIDE , EQUL works only if (A7.isOperating == true)
class lis_PLUS implements ActionListener{
	public void actionPerformed(ActionEvent arg0) {
		if(A7.isOperating == true){
			if(A7.operator == "+")
				A7.NScreen = "" + (A7.NBuffa + Integer.parseInt(A7.NScreen));
			else if(A7.operator == "-")
				A7.NScreen = "" + (A7.NBuffa - Integer.parseInt(A7.NScreen));
			else if(A7.operator == "*")
				A7.NScreen = "" + (A7.NBuffa * Integer.parseInt(A7.NScreen));
			else if(A7.operator == "/" && Integer.parseInt(A7.NScreen) != 0)
				A7.NScreen = "" + (A7.NBuffa / Integer.parseInt(A7.NScreen));
			
			if((A7.NScreen).length() > 9){
				(A7.screen).setText("error");
				A7.isOperating = false;
			}
			else
				(A7.screen).setText(A7.NScreen);
			
			A7.NBuffa = Integer.parseInt(A7.NScreen);
			A7.NScreen = "0";
			A7.operator = "+";
		}
	}
}
class lis_MINUS implements ActionListener{
	public void actionPerformed(ActionEvent arg0) {
		if(A7.isOperating == true){
			if(A7.operator == "+")
				A7.NScreen = "" + (A7.NBuffa + Integer.parseInt(A7.NScreen));
			else if(A7.operator == "-")
				A7.NScreen = "" + (A7.NBuffa - Integer.parseInt(A7.NScreen));
			else if(A7.operator == "*")
				A7.NScreen = "" + (A7.NBuffa * Integer.parseInt(A7.NScreen));
			else if(A7.operator == "/" && Integer.parseInt(A7.NScreen) != 0)
				A7.NScreen = "" + (A7.NBuffa / Integer.parseInt(A7.NScreen));
			
			if((A7.NScreen).length() > 9){
				(A7.screen).setText("error");
				A7.isOperating = false;
			}
			else
				(A7.screen).setText(A7.NScreen);
			
			A7.NBuffa = Integer.parseInt(A7.NScreen);
			A7.NScreen = "0";
			A7.operator = "-";
		}
	}
}
class lis_MULT implements ActionListener{
	public void actionPerformed(ActionEvent arg0) {
		if(A7.isOperating == true){
			if(A7.operator == "+")
				A7.NScreen = "" + (A7.NBuffa + Integer.parseInt(A7.NScreen));
			else if(A7.operator == "-")
				A7.NScreen = "" + (A7.NBuffa - Integer.parseInt(A7.NScreen));
			else if(A7.operator == "*")
				A7.NScreen = "" + (A7.NBuffa * Integer.parseInt(A7.NScreen));
			else if(A7.operator == "/" && Integer.parseInt(A7.NScreen) != 0)
				A7.NScreen = "" + (A7.NBuffa / Integer.parseInt(A7.NScreen));
			
			if((A7.NScreen).length() > 9){
				(A7.screen).setText("error");
				A7.isOperating = false;
			}
			else
				(A7.screen).setText(A7.NScreen);
			
			A7.NBuffa = Integer.parseInt(A7.NScreen);
			A7.NScreen = "0";
			A7.operator = "*";
		}
	}
}
class lis_DIVIDE implements ActionListener{
	public void actionPerformed(ActionEvent arg0) {
		if(A7.isOperating == true){
			if(A7.operator == "+")
				A7.NScreen = "" + (A7.NBuffa + Integer.parseInt(A7.NScreen));
			else if(A7.operator == "-")
				A7.NScreen = "" + (A7.NBuffa - Integer.parseInt(A7.NScreen));
			else if(A7.operator == "*")
				A7.NScreen = "" + (A7.NBuffa * Integer.parseInt(A7.NScreen));
			else if(A7.operator == "/" && Integer.parseInt(A7.NScreen) != 0)
				A7.NScreen = "" + (A7.NBuffa / Integer.parseInt(A7.NScreen));
			
			if((A7.NScreen).length() > 9){
				(A7.screen).setText("error");
				A7.isOperating = false;
			}
			else
				(A7.screen).setText(A7.NScreen);
			
			A7.NBuffa = Integer.parseInt(A7.NScreen);
			A7.NScreen = "0";
			A7.operator = "/";
		}
	}
}
class lis_EQUL implements ActionListener{
	public void actionPerformed(ActionEvent arg0) {
		if(A7.isOperating == true){
			if(A7.operator == "+")
				A7.NScreen = "" + (A7.NBuffa + Integer.parseInt(A7.NScreen));
			else if(A7.operator == "-")
				A7.NScreen = "" + (A7.NBuffa - Integer.parseInt(A7.NScreen));
			else if(A7.operator == "*")
				A7.NScreen = "" + (A7.NBuffa * Integer.parseInt(A7.NScreen));
			else if(A7.operator == "/")
				A7.NScreen = "" + (A7.NBuffa / Integer.parseInt(A7.NScreen));
			
			if((A7.NScreen).length() > 9){
				(A7.screen).setText("error");
				A7.isOperating = false;
			}
			else
				(A7.screen).setText(A7.NScreen);
			
			A7.moreInput = false;
		}
	}
}
// reset all variables to initial state
class lis_CLEAR implements ActionListener{
	public void actionPerformed(ActionEvent arg0) {
		A7.NScreen = "0";
		A7.NBuffa = 0;
		A7.operator = "";
		A7.isOperating = true;
		A7.moreInput = false;
		(A7.screen).setText("0");
	}
}