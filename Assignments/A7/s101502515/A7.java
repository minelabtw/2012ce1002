package ce1002.A7.s101502515;

import java.awt.Color;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingConstants;
import javax.swing.SwingUtilities;
import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;

public class A7 extends JFrame{
	boolean chooser=true;//true + :false -
	boolean waitforin=false;
	static JLabel res;
	static int resout=0;//運算結果 
	//static int resout2=0;//主要
	static String bufstr="0";
	public A7() throws ClassNotFoundException, InstantiationException, IllegalAccessException, UnsupportedLookAndFeelException{
		//整體設定
		UIManager.setLookAndFeel("com.sun.java.swing.plaf.windows.WindowsLookAndFeel");//套用windows skin
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		this.setSize(250, 300);
		this.setLayout(null);
		this.setVisible(true);
		//上方排序
		JPanel gud =new JPanel();
		gud.setLayout(new GridLayout(3, 3, 7,7));
		gud.setBounds(25, 100, 180, 100);
		gud.setVisible(true);
		//輸出label setting
		final JLabel res=new JLabel();
		res.setText("0.");
		res.setBackground(Color.white);
		res.setOpaque(true);
		res.setBounds(25, 25, 180, 50);
		res.setFont(new Font("", Font.PLAIN, 20));
		res.setHorizontalAlignment(SwingConstants.RIGHT);
		//button
		JButton b0 = new JButton("0");
		JButton b1 = new JButton("1");
		JButton b2 = new JButton("2");
		JButton b3 = new JButton("3");
		JButton b4 = new JButton("4");
		JButton b5 = new JButton("5");
		JButton b6 = new JButton("6");
		JButton b7 = new JButton("7");
		JButton b8 = new JButton("8");
		JButton b9 = new JButton("9");
		JButton bp = new JButton("+");
		JButton bd = new JButton("-");
		JButton beq = new JButton("=");
		JButton bc = new JButton("C");
		//set button
		beq.setBounds(163, 171, 40, 66);
		b0.setBounds(25, 207, 87, 30);
		bc.setBounds(117, 207, 40, 30);
		//add
		gud.add(b7);
		gud.add(b8);
		gud.add(b9);
		gud.add(bp);
		gud.add(b4);
		gud.add(b5);
		gud.add(b6);
		gud.add(bd);
		gud.add(b1);
		gud.add(b2);
		gud.add(b3);
		this.add(res);
		this.add(beq);
		this.add(gud);
		this.add(b0);
		this.add(bc);
		//add over 
		SwingUtilities.updateComponentTreeUI( this );//刷新ui style
		//lisetner add
		b0.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				if(waitforin==false&&resout!=0){//是否已經按下+-等待輸入
					bufstr="0";
					resout=0;
				}
				if(Integer.parseInt(bufstr)!=0){//重複輸入0 或第一個為0 =>為0
					bufstr+="0";
					res.setText(bufstr+".");
				}
			}
		});
		b1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				if(waitforin==false&&resout!=0){
					bufstr="0";
					resout=0;
				}
				if(Integer.parseInt(bufstr)==0)
					bufstr="";
					bufstr+="1";
					res.setText(bufstr+".");
				if(bufstr.length()>9){//溢位
					res.setText("error.");
					resout=0;
					bufstr="0";
				}
			}
		});
		b2.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				if(waitforin==false&&resout!=0){
					bufstr="0";
					resout=0;
				}
				if(Integer.parseInt(bufstr)==0)
					bufstr="";
					bufstr+="2";
					res.setText(bufstr+".");
				if(bufstr.length()>9){//溢位
					res.setText("error.");
					resout=0;
					bufstr="0";
				}
			}
		});
		b3.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				if(waitforin==false&&resout!=0){
					bufstr="0";
					resout=0;
				}
				if(Integer.parseInt(bufstr)==0)
					bufstr="";
					bufstr+="3";
					res.setText(bufstr+".");
				if(bufstr.length()>9){//溢位
					res.setText("error.");
					resout=0;
					bufstr="0";
				}
			}
		});
		b4.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				if(waitforin==false&&resout!=0){
					bufstr="0";
					resout=0;
				}
				if(Integer.parseInt(bufstr)==0)
					bufstr="";
					bufstr+="4";
					res.setText(bufstr+".");
				if(bufstr.length()>9){//溢位
					res.setText("error.");
					resout=0;
					bufstr="0";
				}
			}
		});
		b5.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				if(waitforin==false&&resout!=0){
					bufstr="0";
					resout=0;
				}
				if(Integer.parseInt(bufstr)==0)
					bufstr="";
					bufstr+="5";
					res.setText(bufstr+".");
				if(bufstr.length()>9){//溢位
					res.setText("error.");
					resout=0;
					bufstr="0";
				}
			}
		});
		b6.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				if(waitforin==false&&resout!=0){
					bufstr="0";
					resout=0;
				}
				if(Integer.parseInt(bufstr)==0)
					bufstr="";
					bufstr+="6";
					res.setText(bufstr+".");
				if(bufstr.length()>9){//溢位
					res.setText("error.");
					resout=0;
					bufstr="0";
				}
			}
		});
		b7.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				if(waitforin==false&&resout!=0){
					bufstr="0";
					resout=0;
				}
				if(Integer.parseInt(bufstr)==0)
					bufstr="";
					bufstr+="7";
					res.setText(bufstr+".");
				if(bufstr.length()>9){//溢位
					res.setText("error.");
					resout=0;
					bufstr="0";
				}
			}
		});
		b8.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				if(waitforin==false&&resout!=0){
					bufstr="0";
					resout=0;
				}
				if(Integer.parseInt(bufstr)==0)
					bufstr="";
					bufstr+="8";
					res.setText(bufstr+".");
				if(bufstr.length()>9){//溢位
					res.setText("error.");
					resout=0;
					bufstr="0";
				}
			}
		});
		b9.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				if(waitforin==false&&resout!=0){
					bufstr="0";
					resout=0;
				}
				if(Integer.parseInt(bufstr)==0)
					bufstr="";
					bufstr+="9";
					res.setText(bufstr+".");
				if(bufstr.length()>9){//溢位
					res.setText("error.");
					resout=0;
					bufstr="0";
				}
			}
		});
		bc.addActionListener(new ActionListener() {//刷新
			public void actionPerformed(ActionEvent arg0) {
				resout=0;
				bufstr="0";
				res.setText(bufstr+".");
			}
		});
		bd.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				if(waitforin==false){
					resout+=Integer.parseInt(bufstr);//savein
					bufstr="0";//清空,讓使用者在輸入
				}
				if(waitforin==true){//判斷上次
					if(chooser==true)//加或減
					resout+=Integer.parseInt(bufstr);
					if(chooser==false)
					resout-=Integer.parseInt(bufstr);
					chooser=false;
					bufstr="0";
				}
				if(resout>999999999){//溢位
					res.setText("error.");
					resout=0;
					bufstr="0";
				}
				else
					res.setText(resout+".");
					waitforin=true;
			}
		});
		bp.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				if(waitforin==false){
					resout+=Integer.parseInt(bufstr);//savein
					bufstr="0";
				}
				if(waitforin==true){
					if(chooser==true)
					resout+=Integer.parseInt(bufstr);
					if(chooser==false)
					resout-=Integer.parseInt(bufstr);
					chooser=true;
					bufstr="0";
				}
				if(resout>999999999){//溢位
					res.setText("error.");
					resout=0;
					bufstr="0";
				}
				else
				res.setText(resout+".");
				waitforin=true;
			}
		});
		beq.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				if(waitforin==true){
					if(chooser==true)
					resout+=Integer.parseInt(bufstr);
					if(chooser==false)
					resout-=Integer.parseInt(bufstr);
					chooser=true;
					bufstr="0";
				}
				if(resout>999999999){//溢位
					res.setText("error.");
					resout=0;
					bufstr="0";
				}
				else
				res.setText(resout+".");
				waitforin=false;//結算
			}
		});
	}
	public static void main(String args[]) throws ClassNotFoundException, InstantiationException, IllegalAccessException, UnsupportedLookAndFeelException {
		  try {
			A7 ass= new A7();
		} catch (Exception e) {
			System.out.println("can't find");
			e.printStackTrace();
		}

	}
}
