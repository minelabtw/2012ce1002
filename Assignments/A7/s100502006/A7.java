/****************************************************************/
/*                                                              */
/*   輸入位數鎖定最多9位，而運算結果顯示則為整數範圍: -2147483648~2147483647    */
/*   "="可連按                                                                                                                                                                                        */
/*                                                              */
/****************************************************************/

package ce1002.A7.s100502006;

import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JTextField;
import javax.swing.UIManager;

public class A7 extends JFrame{
	private JButton[] jbt=new JButton[14];
	private JTextField jtf=new JTextField();
	private int sum=0;
	private int count=0;
	private int digit=1;
	private enum previousCommand{add,subtract,equalTo,C};
	private boolean lastStepNumaric=false;
	private previousCommand pCommand=previousCommand.C;
	public A7(){
		setLayout(new GridBagLayout());
		
		//set textField
		jtf.setHorizontalAlignment(JTextField.RIGHT);
		jtf.setFont(new Font("Consolas", 0, 22));
		jtf.setEditable(false);
		jtf.setText("0");
		GridBagConstraints c1=new GridBagConstraints();
		c1.gridx=0;
		c1.gridy=0;
		c1.gridwidth=4;
		c1.gridheight=1;
		c1.ipady=20;
		c1.insets=new Insets(8,2,1,2);
		c1.fill=GridBagConstraints.BOTH;
		add(jtf,c1);
		
		//set number buttons
		for (int i = 0; i < 3; i++) {
			for (int j = 1; j <= 3; j++) {
				jbt[3*i+j]=new JButton(String.valueOf(3*i+j));
				GridBagConstraints c2=new GridBagConstraints();
				c2.gridx=j-1;
				c2.gridy=3-i;
				c2.gridwidth=1;
				c2.gridheight=1;
				c2.insets=new Insets(0,1,1,1);
				c2.fill=GridBagConstraints.BOTH;
				add(jbt[3*i+j],c2);
			}
		}
		
		//set "0" button
		jbt[0]=new JButton("0");
		GridBagConstraints c3=new GridBagConstraints();
		c3.gridx=0;
		c3.gridy=4;
		c3.gridwidth=2;
		c3.gridheight=1;
		c3.insets=new Insets(0,1,2,1);
		c3.fill=GridBagConstraints.HORIZONTAL;
		add(jbt[0],c3);
		
		//set "C" button
		jbt[10]=new JButton("C");
		GridBagConstraints c4=new GridBagConstraints();
		c4.gridx=2;
		c4.gridy=4;
		c4.gridwidth=1;
		c4.gridheight=1;
		c4.insets=new Insets(0,1,2,1);
		c4.fill=GridBagConstraints.BOTH;
		add(jbt[10],c4);
		
		//set "-" button
		jbt[11]=new JButton("-");
		GridBagConstraints c5=new GridBagConstraints();
		c5.gridx=3;
		c5.gridy=1;
		c5.gridwidth=1;
		c5.gridheight=1;
		c5.insets=new Insets(0,1,1,1);
		c5.fill=GridBagConstraints.BOTH;
		add(jbt[11],c5);
		
		//set "+" button
		jbt[12]=new JButton("+");
		GridBagConstraints c6=new GridBagConstraints();
		c6.gridx=3;
		c6.gridy=2;
		c6.gridwidth=1;
		c6.gridheight=1;
		c6.insets=new Insets(0,1,1,1);
		c6.fill=GridBagConstraints.BOTH;
		add(jbt[12],c6);
		
		//set "=" button
		jbt[13]=new JButton("=");
		GridBagConstraints c7=new GridBagConstraints();
		c7.gridx=3;
		c7.gridy=3;
		c7.gridwidth=1;
		c7.gridheight=2;
		c7.insets=new Insets(0,1,2,1);
		c7.fill=GridBagConstraints.BOTH;
		add(jbt[13],c7);
		
		//action listener
		for (int i = 0; i < 14; i++) {
			jbt[i].addActionListener(new ButtonListener());
		}
	}
	
	private class ButtonListener implements ActionListener{
		@Override
		public void actionPerformed(ActionEvent e) {
			// TODO Auto-generated method stub
			
			//Button "C"
			if(e.getSource()==jbt[10]){
				sum=0;
				count=0;
				jtf.setText(String.valueOf(sum));
				pCommand=previousCommand.C;
				lastStepNumaric=false;
			}
			
			//Button "-"
			else if(e.getSource()==jbt[11]){
				if(lastStepNumaric){
					if(pCommand==previousCommand.C||pCommand==previousCommand.equalTo){
						sum=Integer.parseInt(jtf.getText());						
					}
					else if(pCommand==previousCommand.add){
						//Overflow check
						try{
							count=(Integer.parseInt(jtf.getText()));
							if (count > 0 ? sum > Integer.MAX_VALUE - count: sum < Integer.MIN_VALUE - count) {
								throw new ArithmeticException("Integer overflow");
							}
							sum+=count;
							jtf.setText(String.valueOf(sum));
						}
						catch(Exception e1){
							jtf.setText("error");
							sum=0;
							count=0;
						}						
					}
					else if(pCommand==previousCommand.subtract){
						//Overflow check
						try{
							count=(Integer.parseInt(jtf.getText()));
							if (count > 0 ? sum < Integer.MIN_VALUE + count : sum > Integer.MAX_VALUE + count){
								throw new ArithmeticException("Integer overflow");
							}
							sum-=count;
							jtf.setText(String.valueOf(sum));
						}
						catch(Exception e1){
							jtf.setText("error");
							sum=0;
							count=0;
						}
					}
				}
				//Overflow check			
				try{
					count=-(Integer.parseInt(jtf.getText()));
					pCommand=previousCommand.subtract;
					lastStepNumaric=false;
				}
				catch(Exception e1){
					jtf.setText("error");
					sum=0;
					count=0;
					pCommand=previousCommand.C;
					lastStepNumaric=false;
				}
			}
			
			//Button "+"
			else if(e.getSource()==jbt[12]){
				if(lastStepNumaric){
					if(pCommand==previousCommand.C||pCommand==previousCommand.equalTo){
						sum=Integer.parseInt(jtf.getText());
					}
					else if(pCommand==previousCommand.add){						
						//Overflow check
						try{
							count=(Integer.parseInt(jtf.getText()));
							if (count > 0 ? sum > Integer.MAX_VALUE - count: sum < Integer.MIN_VALUE - count) {
								throw new ArithmeticException("Integer overflow");
							}
							sum+=count;
							jtf.setText(String.valueOf(sum));
						}
						catch(Exception e1){
							jtf.setText("error");
							sum=0;
							count=0;
						}
					}
					else if(pCommand==previousCommand.subtract){
						//Overflow check
						try{
							count=(Integer.parseInt(jtf.getText()));
							if (count > 0 ? sum < Integer.MIN_VALUE + count : sum > Integer.MAX_VALUE + count){
								throw new ArithmeticException("Integer overflow");
							}
							sum-=count;
							jtf.setText(String.valueOf(sum));
						}
						catch(Exception e1){
							jtf.setText("error");
							sum=0;
							count=0;
						}						
					}
				}
				//Overflow check
				try{
					count=Integer.parseInt(jtf.getText());
					pCommand=previousCommand.add;
					lastStepNumaric=false;
				}
				catch(Exception e1){
					jtf.setText("error");
					sum=0;
					count=0;
					pCommand=previousCommand.C;
					lastStepNumaric=false;
				}
			}
			
			//Button "="
			else if(e.getSource()==jbt[13]){
				if(lastStepNumaric){
					if(pCommand==previousCommand.C||pCommand==previousCommand.equalTo){
						sum=Integer.parseInt(jtf.getText());
						count=0;
					}
					else if(pCommand==previousCommand.add){
						count=Integer.parseInt(jtf.getText());
					}
					else if (pCommand==previousCommand.subtract) {
						count=-(Integer.parseInt(jtf.getText()));
					}
				}
				
				//Overflow check
				try{
					if (count > 0 ? sum > Integer.MAX_VALUE - count: sum < Integer.MIN_VALUE - count) {
						throw new ArithmeticException("Integer overflow");
					}
					sum+=count;
					jtf.setText(String.valueOf(sum));
					pCommand=previousCommand.equalTo;
					lastStepNumaric=false;
				}
				catch(Exception e1){
					jtf.setText("error");
					sum=0;
					count=0;
					pCommand=previousCommand.C;
					lastStepNumaric=false;
				}
			}
			
			//Button "0"~"9"
			else{
				for (int i = 0; i <= 9; i++) {
					if(e.getSource()==jbt[i]){
						if(!lastStepNumaric||jtf.getText().compareTo("0")==0){
							jtf.setText(String.valueOf(i));
							digit=1;
							lastStepNumaric=true;
						}
						else if(digit<9){
							jtf.setText(jtf.getText()+i);
							digit++;
						}						
						break;
					}
				}			
			}			
		}
	}
	
	public static void main(String[] args) {
		//changing UI
		try
		{
		UIManager.setLookAndFeel("javax.swing.plaf.nimbus.NimbusLookAndFeel");
		}
		catch (Exception e)
		{
			System.err.println("error");
		}
		
		A7 frame =new A7();
		frame.setTitle("A7");
		frame.setResizable(false);
		frame.pack();
		frame.setLocationRelativeTo(null);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setVisible(true);
	}
}
