package ce1002.A7.s101502516;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.*;
import java.awt.*;   

public class A7 extends JFrame implements ActionListener {

	static JTextField space = new JTextField( "0." );
	JButton zero = new JButton( "0" );
	JButton one = new JButton( "1" );
	JButton two = new JButton( "2" );
	JButton three = new JButton( "3" );
	JButton four = new JButton( "4" );
	JButton five = new JButton( "5" );
	JButton six = new JButton( "6" );
	JButton seven = new JButton( "7" );
	JButton eight = new JButton( "8" );
	JButton nine = new JButton( "9" );
	JButton c = new JButton( "c" );
	JButton add = new JButton( "+" );
	JButton subtract = new JButton( "-" );
	JButton equal = new JButton( "=" );
	
	public static void main(String[] args){ // 設定視窗大小及跳出位子
		A7 hw = new A7( "小算盤" );
		space.setHorizontalAlignment( JTextField.RIGHT );
		hw.setSize( 350, 450 );
		hw.setLocationRelativeTo( null );
		hw.setVisible( true );
	}
		
	public A7(String pTitle){
		super(pTitle);
		setLayout(null);
		
		space.setBounds( 35, 40, 262, 60 );
		space.setFont( new Font( Font.DIALOG, Font.BOLD, 25 ) ); // 設定字體大小
		add( space );
			
		seven.setBounds( 35, 125, 50, 40 );
		seven.addActionListener(this);
		add( seven );
		
		eight.setBounds( 105, 125, 50, 40 );
		eight.addActionListener(this);
		add( eight );
		
		nine.setBounds( 175, 125, 50, 40 );
		nine.addActionListener(this);
		add( nine );
		
		subtract.setBounds( 245, 125, 50, 40 );
		subtract.addActionListener(this);
		add( subtract );
		
		four.setBounds( 35, 190, 50, 40 );
		four.addActionListener(this);
		add( four );
		
		five.setBounds( 105, 190, 50, 40 );
		five.addActionListener(this);
		add( five );
		
		six.setBounds( 175, 190, 50, 40 );
		six.addActionListener(this);
		add( six );
		
		add.setBounds( 245, 190, 50, 40 );
		add.addActionListener(this);
		add( add );
		
		one.setBounds( 35, 255, 50, 40 );
		one.addActionListener(this);
		add( one );
		
		two.setBounds( 105, 255, 50, 40 );
		two.addActionListener(this);
		add( two );
		
		three.setBounds( 175, 255, 50, 40 );
		three.addActionListener(this);
		add( three );
		
		zero.setBounds( 35, 320, 120, 40 );
		zero.addActionListener(this);
		add( zero );
		
		c.setBounds( 175, 320, 50, 40 );
		c.addActionListener(this);
		add( c );
		
		equal.setBounds( 245, 255, 50, 105 );
		equal.addActionListener(this);
		add( equal );
		
	}
	
	public void ce ( int a ) // 0~9
	{
		q++;
		if ( q > 9 ) // 判斷是否溢位
		{
			space.setText( "error." );
			answer = "";
			cacluate = 0;
		}
		else
		{
			String c = String.valueOf( a );
			answer += c; 
			space.setText( answer );
			cacluate = Integer.parseInt( answer );
		}
	}
	String answer = "";
	int b = 0, a = 1, cacluate, check = 8, end, q = 0;;
	double [] store = new double [200];

	public void actionPerformed(ActionEvent e) { // 按鍵動作設定	
		store[0] = 0;
		if ( e.getSource()== zero )
		{
			ce( 0 );
		}else if ( e.getSource() == one )
		{
			ce( 1 );
		}else if ( e.getSource() == two )
		{
			ce( 2 );
		}else if ( e.getSource() == three )
		{
			ce( 3 );
		}else if ( e.getSource() == four )
		{
			ce( 4 );
		}else if ( e.getSource() == five )
		{
			ce( 5 );
		}else if ( e.getSource() == six )
		{
			ce( 6 );
		}else if ( e.getSource() == seven )
		{
			ce( 7 );
		}else if ( e.getSource() == eight )
		{
			ce( 8 );
		}else if ( e.getSource() == nine )
		{
			ce( 9 );
		}
		
		if ( q <= 9 )
		{
			space.setText( answer );
		}
		
		if ( e.getSource() == add ) // +
		{
			q = 0;
			store[a] = cacluate;
			a++;
			if ( b > 0 )
			{
				if ( check == 0 )
				{
					if ( store[ a - 2 ] / 10 + store[ a - 1 ] / 10 >= 100000000 ) // 判斷是否溢位
					{
						space.setText( "error." );
						answer = "";
						cacluate = 0;
						b = 0;
					}
					else
					{
						store[a] = store[ a - 2 ] + store[ a - 1 ];
						space.setText( String.valueOf( (int)store[a] ) );
						answer = String.valueOf( (int)store[a] );	
						store[ a - 2 ] = store[ a - 1 ];
						store[ a - 1 ] = store[a];
					}

				}
				else
				{
					store[a] = store[ a - 2 ] - store[ a - 1 ];
					space.setText( String.valueOf( (int)store[a] ) );
					answer = String.valueOf( (int)store[a] );	
					store[ a - 2 ] = store[ a - 1 ];
					store[ a - 1 ] = store[a];
				}
			}
			answer = "";
			b++;
			check = 0;
		}else if ( e.getSource() == subtract ) // -
		{
			q = 0;
			store[a] = cacluate;
			a++;
			if ( b > 0 )
			{
				if ( check == 1 )
				{
					store[a] = store[ a - 2 ] - store[ a - 1 ];
					space.setText( String.valueOf( (int)store[a] ) );
					answer = String.valueOf( (int)store[a] );	
					store[ a - 2 ] = store[ a - 1 ];
					store[ a - 1 ] = store[a];
				}
				else 
				{
					if ( store[ a - 2 ] / 10 + store[ a - 1 ] / 10 >= 100000000 )
					{
						space.setText( "error." );
						answer = "";
						cacluate = 0;
						b = 0;
					}
					else
					{
						store[a] = store[ a - 2 ] + store[ a - 1 ];
						space.setText( String.valueOf( (int)store[a] ) );
						answer = String.valueOf( (int)store[a] );	
						store[ a - 2 ] = store[ a - 1 ];
						store[ a - 1 ] = store[a];
					}
				}
			}
			answer = "";
			b++;
			check = 1;
		}else if ( e.getSource() == equal ) // =
		{
			if ( check != 2 )
			{
				store[a] = cacluate;
				a++;
			}
			if ( check == 0 )
			{
				if ( store[ a - 2 ] / 10 + store[ a - 1 ] / 10 >= 100000000 )
				{
					space.setText( "error." );
					answer = "";
					cacluate = 0;
					b = 0;
				}
				else
				{
					store[a] = store[ a - 2 ] + store[ a - 1 ];
					space.setText( String.valueOf( (int)store[a] ) );
					answer = String.valueOf( (int)store[a] );	
				}
				q = 0;
			}else if ( check == 1 )
			{
				store[a] = store[ a - 2 ] - store[ a - 1 ];
				space.setText( String.valueOf( (int)store[a] ) );
				answer = String.valueOf( (int)store[a] );
				q = 0;
			}
			store[ a - 2 ] = store[ a - 1 ];
			store[ a - 1 ] = store[a];
			cacluate = (int)store[a];
			b = 0;
			check = 2;
		}else if ( e.getSource() == c ) // c
		{
			q = 0;
			space.setText( String.valueOf( 0 ) );
			for ( int i = 1; i < a; i++ )
			{
				store[i] = 0;
			}
			answer = "";
		}
	}
}
