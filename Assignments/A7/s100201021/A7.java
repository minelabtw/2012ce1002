package ce1002.A7.s100201021;
import javax.swing.*;

import java.awt.BorderLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class A7 extends JFrame implements ActionListener {
	JPanel P0=new JPanel(new BorderLayout());
	JPanel PA=new JPanel(new BorderLayout());
	JPanel P1=new JPanel(new GridLayout(3,3,6,13));
	JPanel P2=new JPanel(new GridLayout(3,1,5,5));
	JPanel P3=new JPanel(new GridLayout(1,2,3,3));
	//
    char[] JBname={ '7','8','9',
    				'4','5','6',
    				'1','2','3',
    				'+','-','=',
    				'0','c',};
	JButton[] JB=new JButton[14];
	//

	int num=0,nnum=0,L,aors=0,cc=0,inpin=0;
	String snum = "0";
	JTextField n=new JTextField("0");
	
	//
	public A7(){
		//set JFrame
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setVisible(true);
		setSize(300,300);
		//set JButton
		for(int i=0;i<14;i++){
			JB[i]=new JButton(JBname[i]+"");
			JB[i].setActionCommand(String.valueOf(i));
			JB[i].addActionListener(this);
			this.add(JB[i]);
		}
		//set JPanel and Layout
		PA.add(P1,BorderLayout.CENTER);
		PA.add(P3,BorderLayout.SOUTH);
		//
		for(int i=0;i<9;i++)
			P1.add(JB[i]);
		//
		P2.add(JB[9]);
		P2.add(JB[10]);
		P2.add(JB[11]);
		//
		P3.add(JB[12]);
		P3.add(JB[13]);
		//
		P0.add(PA,BorderLayout.CENTER);
		P0.add(P2,BorderLayout.EAST);
		//
		setLayout(new BorderLayout());
		add(n,BorderLayout.NORTH);
		add(P0,BorderLayout.CENTER);
		//
		n.setHorizontalAlignment(JTextField.RIGHT);

	}
	
	public static void main(String[] args){
		new A7();
	}
	
	////
    public void actionPerformed(ActionEvent e) {
        String get = e.getActionCommand();
        int buttonID = Integer.parseInt(get);	//get JButton Command
        switch(buttonID){	//buttonID >> button number
        case 0:
        	buttonID=7;
        	break;
        case 1:
        	buttonID=8;
        	break;
        case 2:
        	buttonID=9;
        	break;
        case 3:
        	buttonID=4;
        	break;
        case 4:
        	buttonID=5;
        	break;
        case 5:
        	buttonID=6;
        	break;
        case 6:
        	buttonID=1;
        	break;
        case 7:
        	buttonID=2;
        	break;
        case 8:
        	buttonID=3;
        	break;
        case 9:
        	buttonID=11;//"+"
        	break;
        case 10:
        	buttonID=12;//"-"
        	break;
        case 11:
        	buttonID=13;//"="
        	break;
        case 12:
        	buttonID=0;	//"0"
        	break;
        case 13:
        	buttonID=99;	//"C"
        default:
        	break;
        }
        num=Integer.parseInt(snum);	//now Text field
        if(buttonID>10){	//+,-,=,C;
        	switch(buttonID){
        	case 11:	//+
            	if(cc==1){
            		num=0;
            		cc=0;
            	}
        		if(aors==0){
        			aors=1;
        			cc=1;
        			nnum=num;
        			break;
        		}
        		if(inpin==0)
        			break;
        		aands();
        		aors=1;
        		inpin=1;
        		break;
        		
        	/////
        	case 12:	//-
            	if(cc==1){
            		num=0;
            		cc=0;
            	}
        		if(aors==0){
        			aors=-1;
        			cc=1;
        			nnum=num;
        			break;
        		}
        		if(inpin==0)
        			break;
        		aands();
        		aors=-1;
        		inpin=1;
        		break;
        		
        	//////
        	case 13:	//=
        		if(aors==0){
        			break;
        		}
        		aands();
        		aors=0;     
        		nnum=0;
        		break;
        	case 99:	//C
        		C();
        		break;
            default:
            	break;
        	}
        }
        else{				//0~9;
        	if(cc==1){
        		num=0;
        		cc=0;
        	}
        	if(buttonID==0){
        		if(num!=0)
        			num*=10;
        	}
        	else{
        		num=num*10+buttonID;
        	}
        	inpin=1;
        }
        snum=new String(num+"");
        n.setText(snum);
       //FF
        if(snum.length()>=10){
        	n.setText("ERROR");
        	C();
        }
    }
    void C(){	//"C"
		snum="0";
		nnum=0;
		num=0;
    }
    void aands(){
    	num = nnum+num*aors;
    	nnum=num;
		cc=1;         	
    }

}
