package ce1002.A7.s101502015;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JTextField;

import ce1002.E4.s101502015.E4;

public class A7 extends JFrame implements ActionListener {

	 private JButton c0 = new JButton("0");//設定按鈕
	 private JButton c1 = new JButton("1");
     private JButton c2 = new JButton("2");
     private JButton c3 = new JButton("3");
     private JButton c4 = new JButton("4");
     private JButton c5 = new JButton("5");
     private JButton c6 = new JButton("6");
     private JButton c7 = new JButton("7");
     private JButton c8 = new JButton("8");
     private JButton c9 = new JButton("9");
     private JButton plus = new JButton("+");
     private JButton subtract = new JButton("-");
     private JButton clean = new JButton("C");
     private JButton result = new JButton("=");
     private JTextField outp = new JTextField();
     String t = "0.";//設定字串
     
     
	public static void main(String[] args) {
		
		A7 frame = new A7("小算盤");//設計視窗大小
        frame.setSize(325, 270);
        frame.setLocation(250, 250);
        frame.setVisible(true);

	}
	 public A7(String pTitle)
	 {
		 super(pTitle);
		 setLayout(null);// 不使用版面配置
         outp.setBounds(40,20,225,30);//設定位置
         c7.setBounds(40, 60, 45, 30);
         c8.setBounds(100, 60, 45, 30);
         c9.setBounds(160, 60, 45, 30);
         c4.setBounds(40, 100, 45, 30);
         c5.setBounds(100, 100, 45, 30);
         c6.setBounds(160, 100, 45, 30);
         c1.setBounds(40, 140, 45, 30);
         c2.setBounds(100, 140, 45, 30);
         c3.setBounds(160, 140, 45, 30);
         c0.setBounds(40, 180, 105, 30);
         subtract.setBounds(220, 60, 45, 30);
         plus.setBounds(220, 100, 45, 30);
         result.setBounds(220, 140, 45, 70);
         clean.setBounds(160, 180, 45, 30);
         c0.addActionListener(this);//使按鈕產生作用
         c1.addActionListener(this);
         c2.addActionListener(this);
         c3.addActionListener(this);
         c4.addActionListener(this);
         c5.addActionListener(this);
         c6.addActionListener(this);
         c7.addActionListener(this);
         c8.addActionListener(this);
         c9.addActionListener(this);
         subtract.addActionListener(this);
         plus.addActionListener(this);
         result.addActionListener(this);
         clean.addActionListener(this);
         outp.setHorizontalAlignment(JTextField.RIGHT);
         add(outp);
         outp.setText(t);
         add(c0);
         add(c1);
         add(c2);
         add(c3);
         add(c4);
         add(c5);
         add(c6);
         add(c7);
         add(c8);
         add(c9);
         add(subtract);
         add(plus);
         add(result);
         add(clean);
     }
	
	 int k = 0;
	 int total = 0;
	 int n = 0;
	 public void actionPerformed(ActionEvent e)
	 {
		 if(e.getSource()==c0){//當按零時
			 n = n*10;
			 t = "" + n;
			 outp.setText(t);
		 }
		 if(e.getSource()==c1){//按一時
			 n = n*10+1;
			 t = "" + n;
			 outp.setText(t);
		 }
		 if(e.getSource()==c2){
			 n = n*10+2;
			 t = "" + n;
			 outp.setText(t);		 
		 }
		 if(e.getSource()==c3){
			 n = n*10+3;
			 t = "" + n;
			 outp.setText(t);
		 }
		 if(e.getSource()==c4){
			 n = n*10+4;
			 t = "" + n;
			 outp.setText(t);
		 }
		 if(e.getSource()==c5){
			 n = n*10+5;
			 t = "" + n;
			 outp.setText(t);
		 }
		 if(e.getSource()==c6){
			 n = n*10+6;
			 t = "" + n;
			 outp.setText(t);
		 }
		 if(e.getSource()==c7){
			 n = n*10+7;
			 t = "" + n;
			 outp.setText(t);
		 }
		 if(e.getSource()==c8){
			 n = n*10+8;
			 t = "" + n;
			 outp.setText(t);
		 }
		 if(e.getSource()==c9){
			 n = n*10+9;
			 t = "" + n;
			 outp.setText(t);
		 }
		 if(e.getSource()==subtract){//按減法時
			 if(k == 1){
				 total = total - n;
			 }
			 else if(k == 2){
				 total = total + n;
			 }
			 else
				 total = n;
			 t = "" + total;
			 k = 1;
			 n = 0;
			 outp.setText(t);
		 }
		 if(e.getSource()==plus){//按加法時
			 if(k == 1){
				 total = total - n;
			 }
			 else if(k == 2){
				 total = total + n;
			 }
			 else
				 total = n;
			 t = "" + total;
			 k = 2;
			 n = 0;
			 outp.setText(t);
		 }
		 if(e.getSource()==result){//顯示計算結果
			 if(k == 1){
				 total = total - n;
			 }
			 else if(k == 2){
				 total = total + n;
			 }
			 else
				 total = n;
			 t = "" + total;
			 k = 0;
			 n = 0;
			 outp.setText(t);
		 }
		 if(e.getSource()==clean){//清除所有計算
			 k = 0;
			 n = 0;
			 total = 0;
			 outp.setText("0.");
		 }

	 }
}
