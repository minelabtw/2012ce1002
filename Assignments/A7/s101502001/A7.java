package ce1002.A7.s101502001;
import javax.swing.*;
import java.awt.*;
import java.awt.event.*;

public class A7 extends JFrame implements ActionListener{
	private JButton a=new JButton("+");
	private JButton b=new JButton("-");
	private JButton c=new JButton("C");
	private JButton d=new JButton("=");
	private JButton e1=new JButton("0");
	private JButton f=new JButton("1");
	private JButton g=new JButton("2");
	private JButton h=new JButton("3");
	private JButton i=new JButton("4");
	private JButton j=new JButton("5");
	private JButton k=new JButton("6");
	private JButton l=new JButton("7");
	private JButton m=new JButton("8");
	private JButton n=new JButton("9");
	private TextField text=new TextField(20);
	private String str="";
	private String str1="";
	private int result=0;//結果
	private int check=0;//檢查是加還是減
	
	public A7(){
		JPanel panel=new JPanel();
		
		panel.add(text);
		panel.add(l);
		l.addActionListener(this);
		panel.add(m);
		m.addActionListener(this);
		panel.add(n);
		n.addActionListener(this);
		panel.add(b);
		b.addActionListener(this);
		
		panel.add(i);
		i.addActionListener(this);
		panel.add(j);
		j.addActionListener(this);
		panel.add(k);
		k.addActionListener(this);
		panel.add(a);
		a.addActionListener(this);
		
		panel.add(f);
		f.addActionListener(this);
		panel.add(g);
		g.addActionListener(this);
		panel.add(h);
		h.addActionListener(this);
		panel.add(d);
		d.addActionListener(this);
		
		panel.add(e1);
		e1.addActionListener(this);
		panel.add(c);
		c.addActionListener(this);
		
		add(panel);
	}
	public static void main(String[]args){
		A7 frame=new A7();
		frame.setTitle("小算盤");
		frame.setSize(200,200);
		frame.setLocationRelativeTo(null);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setVisible(true);
	}
	
	public void actionPerformed(ActionEvent e) {
		if(e.getSource()==c){//C
			str="";
			text.setText(str);
		}else if(e.getSource()==e1){//0
			str=str+0;
			text.setText(str);
		}else if(e.getSource()==f){//1
			str=str+1;
			text.setText(str);
		}else if(e.getSource()==g){//2
			str=str+2;
			text.setText(str);
		}else if(e.getSource()==h){//3
			str=str+3;
			text.setText(str);
		}else if(e.getSource()==i){//4
			str=str+4;
			text.setText(str);
		}else if(e.getSource()==j){//5
			str=str+5;
			text.setText(str);
		}else if(e.getSource()==k){//6
			str=str+6;
			text.setText(str);
		}else if(e.getSource()==l){//7
			str=str+7;
			text.setText(str);
		}else if(e.getSource()==m){//8
			str=str+8;
			text.setText(str);
		}else if(e.getSource()==n){//9
			str=str+9;
			text.setText(str);
		}else if(e.getSource()==a){//+
			str1=str;
			str="";
			check=1;
		}else if(e.getSource()==b){//-
			str1=str;
			str="";
			check=2;
		}else if(e.getSource()==d){//=
			int st1=Integer.parseInt(str1);//String轉型為int
			int st=Integer.parseInt(str);//String轉型為int
			if(check==1){
				result=st1+st;
				
			}else if(check==2){
				result=st1-st;
			}else if(check==0){
				str ="";
			}
			String re=String.valueOf(result);//int轉型為String
			text.setText(re);
		}
	}
}