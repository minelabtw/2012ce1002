package ce1002.A7.s101502503;

import java.awt.event.*;
import java.awt.*;

import javax.swing.*;

public class A7  extends JFrame implements ActionListener 
{
	private JTextField print = new JTextField("0");
	
	private JButton Btn0 = new JButton("0");
	private JButton Btn1 = new JButton("1");
	private JButton Btn2 = new JButton("2");
	private JButton Btn3 = new JButton("3");
	private JButton Btn4 = new JButton("4");
	private JButton Btn5 = new JButton("5");
	private JButton Btn6 = new JButton("6");
	private JButton Btn7 = new JButton("7");
	private JButton Btn8 = new JButton("8");
	private JButton Btn9 = new JButton("9");

	private JButton Btnplus = new JButton("+");
	private JButton Btnminus = new JButton("-");
	private JButton Btnequal = new JButton("=");
	private JButton Btnclear = new JButton("C");
	
	private int result=0; //存結果
	private String  n = ""; //存所輸入的數
	private boolean plusornot = false ;//判斷上一個動作是否按+,是則true
	private boolean minusornot = false ;//判斷上一個動作是否-,是則true
	
	
	public static void main(String[] args) 
	{
		A7 hw = new A7("小算盤");
		hw.setSize(280, 315);
		hw.setLocation(250, 250);
		hw.setVisible(true);
	}
	
	
	public A7(String pTitle) 
	{
		super(pTitle);
		setLayout(null);
		//size of TextField and button
		print.setBounds(20, 35, 225, 45);
		Btn0.setBounds(20, 210, 105, 30);
		Btn1.setBounds(20, 170, 45, 30);
		Btn2.setBounds(80, 170, 45, 30);
		Btn3.setBounds(140, 170, 45, 30);
		Btn4.setBounds(20, 130, 45, 30);
		Btn5.setBounds(80, 130, 45, 30);
		Btn6.setBounds(140, 130, 45, 30);
		Btn7.setBounds(20, 90, 45, 30);
		Btn8.setBounds(80, 90, 45, 30);
		Btn9.setBounds(140, 90, 45, 30);
		Btnplus.setBounds(200, 130, 45, 30);
		Btnminus.setBounds(200, 90, 45, 30);
		Btnequal.setBounds(200, 170, 45, 70);
		Btnclear.setBounds(140, 210, 45, 30);
		
		Btn0.addActionListener(this);
		Btn1.addActionListener(this);
		Btn2.addActionListener(this);
		Btn3.addActionListener(this);
		Btn4.addActionListener(this);
		Btn5.addActionListener(this);
		Btn6.addActionListener(this);
		Btn7.addActionListener(this);
		Btn8.addActionListener(this);
		Btn9.addActionListener(this);
		Btnplus.addActionListener(this);
		Btnminus.addActionListener(this);
		Btnequal.addActionListener(this);
		Btnclear.addActionListener(this);
		
		//add TextField and button
		add(print);
		print.setHorizontalAlignment(JTextField.RIGHT);//讓文字靠右
		add(Btn0);
		add(Btn1);
		add(Btn2);
		add(Btn3);
		add(Btn4);
		add(Btn5);
		add(Btn6);
		add(Btn7);
		add(Btn8);
		add(Btn9);
		add(Btnclear);
		add(Btnminus);
		add(Btnplus);
		add(Btnequal);
		
		//改字體及大小
		Font font = new Font("NSimSun",0, 20);
		Font font1 = new Font("Consolas", 0, 25);
		print.setFont(font1);
		Btn0.setFont(font);
		Btn1.setFont(font);
		Btn2.setFont(font);
		Btn3.setFont(font);
		Btn4.setFont(font);
		Btn5.setFont(font);
		Btn6.setFont(font);
		Btn7.setFont(font);
		Btn8.setFont(font);
		Btn9.setFont(font);
		Btnplus.setFont(font);
		Btnminus.setFont(font);
		Btnequal.setFont(font);
		Btnclear.setFont(font);
		
	}
	
	public void actionPerformed(ActionEvent e) 
	{
		if(e.getSource()==Btn0)//push Btn0
		{
			if(n=="0")        
				n="0";
			else
				n+='0';
			print.setText(""+n);
		}
		if(e.getSource()==Btn1)//push Btn1
		{
			n+='1';
			print.setText(""+n);
		}
		if(e.getSource()==Btn2)//push Btn2
		{
			n+='2';
			print.setText(""+n);
		}
		if(e.getSource()==Btn3)//push Btn3
		{
			n+='3';
			print.setText(""+n);
		}
		if(e.getSource()==Btn4)//push Btn4
		{
			n+='4';
			print.setText(""+n);
		}
		if(e.getSource()==Btn5)//push Btn5
		{
			n+='5';
			print.setText(""+n);
		}
		if(e.getSource()==Btn6)//push Btn6
		{
			n+='6';
			print.setText(""+n);
		}
		if(e.getSource()==Btn7)//push Btn7
		{
			n+='7';
			print.setText(""+n);
		}
		if(e.getSource()==Btn8)//push Btn8
		{
			n+='8';
			print.setText(""+n);
		}
		if(e.getSource()==Btn9)//push Btn9
		{
			n+='9';
			print.setText(""+n);
		}
		
		if(e.getSource()==Btnplus)//push Btnplus
		{
			if(n=="")
				n="0";
			else if(plusornot==true&&minusornot==false)//上個動作為+,先做
				result+=Integer.valueOf(print.getText());
			else if(minusornot==true&&plusornot==false)//上個動作為-,先做
				result=result-Integer.valueOf(print.getText());
			else if(plusornot==false&&minusornot==false)
				result=Integer.valueOf(print.getText());
			n="";
			plusornot = true;
			minusornot = false;
			print.setText(""+result);
			
		}
		if(e.getSource()==Btnminus)//push Btnminus
		{
			if(plusornot==true&&minusornot==false)//上個動作為+,先做
				result+=Integer.valueOf(print.getText());
			else if(minusornot==true&&plusornot==false)//上個動作為-,先做
				result=result-Integer.valueOf(print.getText());
			else if(plusornot==false&&minusornot==false)
				result=Integer.valueOf(print.getText());

			minusornot = true;
			plusornot = false;
			n="";
			print.setText(""+result);
			
		}
		if(e.getSource()==Btnequal)//push Btnequal
		{
			if(plusornot==true)//上個動作為+,先做
				result+=Integer.valueOf(print.getText());
			else if(minusornot==true)//上個動作為+,先做
				result=result-Integer.valueOf(print.getText());
			print.setText(""+result);
			minusornot = plusornot = false;
			n="";
			
		}
		if(e.getSource()==Btnclear)//push Btnclear
		{
			result=0;
			n="";
			minusornot = plusornot = false;
			print.setText(""+0);
		}
		if(result>999999999)//判斷運算後是否溢位
		{
			print.setText("error");
			result=0;
			n="";
			minusornot = plusornot = false;
		}
		if(n.length()>9)//判斷輸入是否溢位
		{
			print.setText("error");
			result=0;
			n="";
			minusornot = plusornot = false;
		}
	}
}
