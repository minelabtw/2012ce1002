//A7-100502201
//Calculator
package ce1002.A7.s100502201;

import java.awt.Color; //To set background color
import java.awt.Font; //To change font size
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.*;

public class A7 extends JFrame implements ActionListener {

	int[] stack = new int[9]; // To calculate every input
	private static int ptr = -1; // Stack pointer
	private static int num[] = new int[16]; // To save every input
	private static int np = 0; // num[] pointer
	private int res = 0; // Result
	boolean sub = false; // Whether the last operand is subtracted

	private JButton one = new JButton("1"); // Declaration
	private JButton two = new JButton("2");
	private JButton three = new JButton("3");
	private JButton four = new JButton("4");
	private JButton five = new JButton("5");
	private JButton six = new JButton("6");
	private JButton seven = new JButton("7");
	private JButton eight = new JButton("8");
	private JButton nine = new JButton("9");
	private JButton zero = new JButton("0");
	private JButton plus = new JButton("+");
	private JButton minus = new JButton("-");
	private JButton equal = new JButton("=");
	private JButton C = new JButton("C");

	private JLabel result = new JLabel();

	private Font Normal = new Font(Font.SANS_SERIF, Font.BOLD, 20); // Button
																	// font
	private Font Result = new Font(Font.SANS_SERIF, Font.BOLD, 48); // Calculator
																	// screen
																	// font

	public static void main(String[] args) { // Constructor

		A7 A7 = new A7("�p��L");

		A7.setSize(350, 550);

		A7.getContentPane().setBackground(Color.gray);

		A7.setLocation(250, 250);

		A7.setVisible(true);

	}

	public A7(String pTitle) {

		super(pTitle);
		setLayout(null);

		zero.setBounds(25, 425, 125, 50); // Layout
		zero.setFont(Normal);
		C.setBounds(175, 425, 50, 50);
		C.setFont(Normal);
		one.setBounds(25, 350, 50, 50);
		one.setFont(Normal);
		two.setBounds(100, 350, 50, 50);
		two.setFont(Normal);
		three.setBounds(175, 350, 50, 50);
		three.setFont(Normal);
		equal.setBounds(250, 350, 50, 125);
		equal.setFont(Normal);
		four.setBounds(25, 275, 50, 50);
		four.setFont(Normal);
		five.setBounds(100, 275, 50, 50);
		five.setFont(Normal);
		six.setBounds(175, 275, 50, 50);
		six.setFont(Normal);
		plus.setBounds(250, 275, 50, 50);
		plus.setFont(Normal);
		seven.setBounds(25, 200, 50, 50);
		seven.setFont(Normal);
		eight.setBounds(100, 200, 50, 50);
		eight.setFont(Normal);
		nine.setBounds(175, 200, 50, 50);
		nine.setFont(Normal);
		minus.setBounds(250, 200, 50, 50);
		minus.setFont(Normal);

		result.setBounds(25, 75, 275, 100);
		result.setOpaque(true);
		result.setBackground(Color.WHITE);
		result.setHorizontalAlignment(JLabel.RIGHT);
		result.setFont(Result);

		one.setActionCommand("1"); // Every button makes separated action
									// listener
		one.addActionListener(this);
		two.setActionCommand("2");
		two.addActionListener(this);
		three.setActionCommand("3");
		three.addActionListener(this);
		four.setActionCommand("4");
		four.addActionListener(this);
		five.setActionCommand("5");
		five.addActionListener(this);
		six.setActionCommand("6");
		six.addActionListener(this);
		seven.setActionCommand("7");
		seven.addActionListener(this);
		eight.setActionCommand("8");
		eight.addActionListener(this);
		nine.setActionCommand("9");
		nine.addActionListener(this);
		zero.setActionCommand("0");
		zero.addActionListener(this);
		plus.setActionCommand("+");
		plus.addActionListener(this);
		minus.setActionCommand("-");
		minus.addActionListener(this);
		equal.setActionCommand("=");
		equal.addActionListener(this);
		C.setActionCommand("C");
		C.addActionListener(this);

		result.setText(res + ".");

		add(one);
		add(two);
		add(three);
		add(four);
		add(five);
		add(six);
		add(seven);
		add(eight);
		add(nine);
		add(zero);
		add(plus);
		add(minus);
		add(C);
		add(equal);
		add(result);

	}

	public void actionPerformed(ActionEvent e) {

		String cmd = e.getActionCommand();
		int tmp = 0;
		
		if (ptr > 8) { // Over 9 digit is an error
			result.setText("error.");
			ptr = -1;
			num[np] = 0;
			res = 0;
			
		} else if (cmd == "1") {
			stack[++ptr] = 1;
			for (int i = ptr; i >= 0; i--)
				tmp += Math.pow(10, i) * stack[ptr - i];
			result.setText("" + tmp);
			num[np] = tmp;

		} else if (cmd == "2") {
			stack[++ptr] = 2;
			for (int i = ptr; i >= 0; i--)
				tmp += Math.pow(10, i) * stack[ptr - i];
			result.setText("" + tmp);
			num[np] = tmp;

		} else if (cmd == "3") {
			stack[++ptr] = 3;
			for (int i = ptr; i >= 0; i--)
				tmp += Math.pow(10, i) * stack[ptr - i];
			result.setText("" + tmp);
			num[np] = tmp;

		} else if (cmd == "4") {
			stack[++ptr] = 4;
			for (int i = ptr; i >= 0; i--)
				tmp += Math.pow(10, i) * stack[ptr - i];
			result.setText("" + tmp);
			num[np] = tmp;

		} else if (cmd == "5") {
			stack[++ptr] = 5;
			for (int i = ptr; i >= 0; i--)
				tmp += Math.pow(10, i) * stack[ptr - i];
			result.setText("" + tmp);
			num[np] = tmp;

		} else if (cmd == "6") {
			stack[++ptr] = 6;
			for (int i = ptr; i >= 0; i--)
				tmp += Math.pow(10, i) * stack[ptr - i];
			result.setText("" + tmp);
			num[np] = tmp;

		} else if (cmd == "7") {
			stack[++ptr] = 7;
			for (int i = ptr; i >= 0; i--)
				tmp += Math.pow(10, i) * stack[ptr - i];
			result.setText("" + tmp);
			num[np] = tmp;

		} else if (cmd == "8") {
			stack[++ptr] = 8;
			for (int i = ptr; i >= 0; i--)
				tmp += Math.pow(10, i) * stack[ptr - i];
			result.setText("" + tmp);
			num[np] = tmp;

		} else if (cmd == "9") {
			stack[++ptr] = 9;
			for (int i = ptr; i >= 0; i--)
				tmp += Math.pow(10, i) * stack[ptr - i];
			result.setText("" + tmp);
			num[np] = tmp;

		} else if (cmd == "0") {
			if (ptr == -1) { // When there's no input, pushing zero is useless
				result.setText("0");
				num[np] = 0;
			} else {
				stack[++ptr] = 0;
				for (int i = ptr; i >= 0; i--)
					tmp += Math.pow(10, i) * stack[ptr - i];
				result.setText("" + tmp);
				num[np] = tmp;
			}

		} else if (cmd == "+") { // Plus operand
			if (sub == true)
				num[np] = -num[np];
			res += num[np++];
			if (res > 999999999)
				result.setText("error.");
			else
				result.setText("" + res);
			ptr = -1;
			sub = false;

		} else if (cmd == "-") { // Minus operand
			if (sub == true)
				num[np] = -num[np];
			res += num[np++];
			if (res > 999999999)
				result.setText("error.");
			else
				result.setText("" + res);
			ptr = -1;
			sub = true;
			
		} else if (cmd == "=") {
			// Don't expect exception for weird inputing like a "+" or "-" after
			// "="
			if (sub == true)
				num[np] = -num[np];
			res += num[np++];
			if (res > 999999999)
				result.setText("error.");
			else
				result.setText("" + res);
			ptr = -1;
			sub = false;
			
		} else if (cmd == "C") { // Return 0
			ptr = -1;
			np = 0;
			res = 0;
			result.setText("0.");
		}

	}

}
