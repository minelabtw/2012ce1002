package ce1002.A7.s101502523;
import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.awt.geom.RoundRectangle2D;
//參考來源:http://blog.xuite.net/ray00000test/blog/21527129-(AWT)%E5%B0%8F%E8%A8%88%E7%AE%97%E6%A9%9F
//		http://s.yanghao.org/program/viewdetail.php?i=347221
public class A7 extends JButton{
	private static A7 button[] = new A7[10];
	private static Label label = new Label("0",Label.RIGHT);
	private static A7 cancel, add, subtract, equal;
	private static int result = 0 , temp1 = 0, temp2 = 0, option=0;
    
	public A7(String s)  {
		super(s);
		setContentAreaFilled(false);
	}		
	public static class ActionList implements ActionListener {
	    public void actionPerformed(ActionEvent e) throws NumberFormatException ,ArithmeticException{
	    	try{
	   	        for(int j=0 ; j<=9 ; j++){
	   	        	if(e.getSource()==button[j]){
	   	        		//限定位數輸入
	   	        		if(result>999999999||result<-999999999||(label.getText().length()==9&&temp1>0)||(label.getText().length()==10&&temp1<0))
	   	    	        	break;	    
	   	        		//初始沒有運算時給temp2值
	   	        		if(option==0){
	   	        			label.setText(Integer.toString(Integer.parseInt(""+temp2+j)));
	   	        			temp2 = Integer.parseInt(label.getText());
	   	        		}
	   	        		//有運算後
	   	        		else{
	   	        			label.setText(Integer.toString(Integer.parseInt(""+temp1+j)));
	   	        			temp1 = Integer.parseInt(label.getText());	 
	   	        		}
	   	        		break;
	   	        	}
	   	        }	   	        
	   	        //cancel
	   	        if(e.getSource()==cancel){
	   	        	label.setText("0");
	   	        	result = 0;
	   	        	temp1 = 0;
	   	        	temp2 = 0;
	   	        	option = 0;
	   	        }
	   	        //add
	   	        if(e.getSource()==add){
	   	        	if(option==2)
	   	        		//前一次運算的處理(subtract)
	   	        		temp2 -= temp1;
	   	        	else
	   	        		temp2 += temp1;
	   	        	label.setText(""+temp2);
	   	        	temp1 = 0;
	   	        	option = 1;
	   	        }
	   	        //subtract
	   	        if(e.getSource()==subtract){
	   	        	if(option==1)
	   	        		//前一次運算的處理(add)
	   	        		temp2 += temp1;
	   	        	else
	   	        		temp2 -= temp1 ;
	   	        	label.setText(""+temp2);
	   	        	temp1 = 0;
	   	        	option = 2;
	   	        }
	   	        //equal
	   	        if(e.getSource()==equal){
	   	        	if(option==0){
	   	        		//直接按等於
	   	        		result = Integer.parseInt(label.getText());
	   	        	}
	   	        	else if(option==1){
	   	        		//加法完後按等於
	   	        		result = temp1 + temp2;
	   	        		option = 0;
	   	        	}
	   	        	else if(option==2){
	   	        		//減法完後按等於
	   	        		result = temp2 - temp1;
	   	        		option = 0;
	   	        	}
	   	        	temp1 = 0;
	   	        	temp2 = result;
	   	        	label.setText(""+result);	   	        	
	   	        }
	   	        //溢位
	   	        if(result>999999999||result<-999999999||temp2>999999999||temp2<-999999999){
	   	        	label.setText("error");
	   	        	result = 0;
	   	        }     
	    	}catch(NumberFormatException ne){
	            //捕捉例外
	        }catch(ArithmeticException ae){
	            //捕捉被除數是零的例外
	        }    
	    }	    
	}
	protected void paintComponent(Graphics g) {
		Graphics2D g2d = (Graphics2D) g.create();
		int height = getHeight();
		int with = getWidth();
		g2d.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
		GradientPaint p1, p2;
		//按下按鈕
		if (getModel().isPressed()){
			p1 = new GradientPaint(0, 0, new Color(0, 0, 0), 0, height, new Color(100, 100, 100));
			p2 = new GradientPaint(0, 1, new Color(0, 0, 0, 50), 0, height, new Color(255, 255, 255, 100));
		}
		else{
			p1 = new GradientPaint(0, 0, new Color(100, 100, 100), 0, height, new Color(0, 0, 0));
			p2 = new GradientPaint(0, 1, new Color(255, 255, 255, 100), 0, height, new Color(0, 0, 0, 50));
		}
		
		RoundRectangle2D.Float r2d = new RoundRectangle2D.Float(0, 0, with - 1, height - 1, 10, 10);
		g2d.clip(r2d);
		//按鈕顏色漸層
		GradientPaint gp = new GradientPaint(0, 0, new Color(248, 250, 255), 0, height, new Color(238, 240, 242));
		g2d.setPaint(gp);
		g2d.fillRect(0, 0, with, height);
		g2d.setPaint(p1);
		g2d.drawRoundRect(0, 0, with-1, height-1, 10, 10);
		g2d.setPaint(p2);
		g2d.drawRoundRect(1, 1, with-1, height-1, 10, 10);
		g2d.dispose();
		super.paintComponent(g);
	}
	public void paintBorder(Graphics g) {
		//畫按鈕邊框
		g.drawRoundRect(0, 0, getSize().width - 1, getSize().height - 1, 10, 10);
	}
	public static void main(String[] args) {
		JFrame frame = new JFrame("小算盤");
		JPanel panel = new JPanel();
		JPanel number = new JPanel();
		cancel = new A7("C");
		add = new A7("+");
		subtract = new A7("-");
		equal = new A7("=");
		//設定panel
		panel.setLayout(null);
		panel.setBackground(new Color(217,228,241));
		number.setLayout(new GridLayout(3,3,10,8));
		number.setBackground(new Color(217,228,241));
		number.setBounds(45,120,170,120);
		//0~9 buttons
		for(int i=7 ; i>0 ; i++) {
			button[i] = new A7(""+i);
			button[i].setFocusPainted(false);
			number.add(button[i]);
			button[i].addActionListener(new ActionList());
			if(i%3==0)
				i-=6;
		}
		button[0] = new A7(""+0);
		button[0].setBounds(45,248,110,34);
		button[0].setFocusPainted(false);
		button[0].addActionListener(new ActionList());
		panel.add(button[0]);
		//label
		label.setBackground(Color.WHITE);
		label.setFont(new Font("微軟正黑體", Font.BOLD, 32));
		label.setBounds(45, 50, 230, 50);
		panel.add(label);
		//cancel
		cancel.setBounds(165,248,50,34);
		cancel.setFocusPainted(false);
		panel.add(cancel);
		cancel.addActionListener(new ActionList());
		//subtract
		subtract.setBounds(225,121,50,34);
		subtract.setFocusPainted(false);
		panel.add(subtract);
		subtract.addActionListener(new ActionList());
		//add
		add.setBounds(225,163,50,34);
		add.setFocusPainted(false);
		panel.add(add);	
		add.addActionListener(new ActionList());
		//equal
		equal.setBounds(225,205,50,77);
		equal.setFocusPainted(false);
		panel.add(equal);
		equal.addActionListener(new ActionList());
		
		panel.add(number);
		frame.add(panel);		
		frame.setBounds(200, 200, 320, 380);
		frame.setResizable(false);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setVisible(true);	
	}
}
