package ce1002.A7.s100502203;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.*;

/* 參考資料: http://pydoing.blogspot.tw/2011/05/java-gridbaglayout.html */

@SuppressWarnings("serial")
public class A7 extends JFrame {
	JTextField jText = new JTextField();
	int tmpNum, resNum;
	int operator;
	boolean error = false;
	boolean clear = false;

	public A7() {
		this.setTitle("小算盤");
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		this.setSize(325, 325);
		this.setResizable(false);
		this.setLayout(new GridBagLayout());
		jText.setEditable(false);
		jText.setBackground(Color.WHITE);

		String name[] = { "7", "8", "9", "-", "4", "5", "6", "+", "1", "2",
				"3", "=", "0", "C" };
		int gridX[] = { 0, 1, 2, 3, 0, 1, 2, 3, 0, 1, 2, 3, 0, 2 };
		int gridY[] = { 1, 1, 1, 1, 2, 2, 2, 2, 3, 3, 3, 3, 4, 4 };
		int gridWidth[] = { 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 2, 1 };
		int gridHeight[] = { 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 2, 1, 1 };
		/*	Use GridBagLayout	*/
		GridBagConstraints GBComponent = new GridBagConstraints();
		GBComponent.gridx = 0;
		GBComponent.gridy = 0;
		GBComponent.gridwidth = 4;
		GBComponent.gridheight = 1;
		GBComponent.weightx = 0;
		GBComponent.weighty = 0;
		GBComponent.insets = new Insets(10, 10, 10, 10);

		GBComponent.fill = GridBagConstraints.BOTH;
		GBComponent.anchor = GridBagConstraints.EAST;
		this.add(jText, GBComponent);

		ButtonListener bListener = new ButtonListener();
		for (int i = 0; i < 14; i++) {
			JButton jButton = new JButton(name[i]);
			GBComponent = new GridBagConstraints();
			GBComponent.gridx = gridX[i];
			GBComponent.gridy = gridY[i];
			GBComponent.gridwidth = gridWidth[i];
			GBComponent.gridheight = gridHeight[i];
			GBComponent.weightx = 0;
			GBComponent.weighty = 0;
			GBComponent.insets = new Insets(10, 10, 10, 10);
			GBComponent.fill = GridBagConstraints.BOTH;
			GBComponent.anchor = GridBagConstraints.WEST;
			jButton.setActionCommand(name[i]);
			jButton.addActionListener(bListener);	// 將所有按鍵listener放在同一函式
			this.add(jButton, GBComponent);
		}
		this.setVisible(true);
	}

	public int calculate(int operator) {
		switch (operator) {
		case 0:	// 0 = -
			resNum = -resNum + tmpNum;
			if (resNum < -999999999) {
				error = true;
				return 0;
			}
			tmpNum = 0;
			break;
		case 1:	// 1 = +
			resNum = resNum + tmpNum;
			if (resNum > 999999999) {
				error = true;
				return 0;
			}
			tmpNum = 0;
			break;
		}
		return resNum;
	}

	class ButtonListener implements ActionListener {
		public void actionPerformed(ActionEvent e) {
			String cmd = e.getActionCommand();
			if (error) {	// 錯誤時處理
				jText.setText(String.format("%73s", "error"));
				resNum = 0;
				tmpNum = 0;
				error = false;
				return;
			}
			if (cmd.matches("[0-9]")) {	// 指令讀到0-9時動作
				if (resNum > 99999999 || resNum < -99999999) {
					error = true;
					return;
				}
				if (resNum == 0 && cmd.equals("0"))
					return;
				resNum *= 10;
				resNum += Integer.parseInt(cmd);
				jText.setText(String.format("%73d", resNum));
			} else if (cmd.equals("-")) {	// sub
				if (tmpNum == 0) {
					tmpNum = resNum;
					resNum = 0;
					operator = 0;
				} else {
					tmpNum = calculate(operator);
					resNum = 0;
					operator = 0;
					jText.setText(String.format("%73d", tmpNum));
				}
			} else if (cmd.equals("+")) {	// add
				if (tmpNum == 0) {
					tmpNum = resNum;
					resNum = 0;
					operator = 1;
				} else {
					tmpNum = calculate(operator);
					resNum = 0;
					operator = 1;
					jText.setText(String.format("%73d", tmpNum));
				}
			} else if (cmd.equals("=")) {	// equal
				resNum = calculate(operator);
				operator = 2;
				jText.setText(String.format("%73d", resNum));
			} else if (cmd.equals("C")) {	// clear
				resNum = 0;
				tmpNum = 0;
				error = false;
				operator = 2;
				jText.setText(String.format("%73d", resNum));
			}
		}
	}

	public static void main(String[] args) {
		A7 frame = new A7();
	}

}
