package ce1002.A7.s101502005;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;

public class A7 extends JFrame implements ActionListener{
	private JPanel p1= new JPanel();
	private JButton[] button=new JButton[14];
	private String display="0.";
	private int temp=0;//計算使用 儲存第一個數字
	private boolean click=true;
	private String Cal=new String("");//存存運算按鍵狀態
	private JLabel displayLabel=new JLabel(display);
	public static void main(String[] args){
		A7 calculator = new A7();
	}
	A7(){
		init();
		for(int i = 0;i < 10;i++){						//設定button
			button[i]=new JButton(String.valueOf(i));
		}
		button[10]=new JButton("+");
		button[11]=new JButton("-");
		button[12]=new JButton("C");
		button[13]=new JButton("=");
		button[0].setBounds(20,370,130,60);
		button[1].setBounds(20,280,50,60);
		button[2].setBounds(100,280,50,60);
		button[3].setBounds(180,280,50,60);
		button[4].setBounds(20,190,50,60);
		button[5].setBounds(100,190,50,60);
		button[6].setBounds(180,190,50,60);
		button[7].setBounds(20,100,50,60);
		button[8].setBounds(100,100,50,60);
		button[9].setBounds(180,100,50,60);
		button[10].setBounds(260,190,50,60);
		button[11].setBounds(260,100,50,60);
		button[12].setBounds(180,370,50,60);
		button[13].setBounds(260,280,50,150);
		for(int i = 0;i < 14;i++){
			add(button[i]);
			button[i].addActionListener(this);
		}
		displayLabel.setBounds(20, 30, 290, 50);
		displayLabel.setBackground(Color.WHITE);
		displayLabel.setOpaque(true);
		displayLabel.setHorizontalAlignment(JLabel.RIGHT);
		add(displayLabel);
	}
	private void init(){
		setTitle("小算盤");
        setSize(340,470);//設定大小
        setLocationRelativeTo(null);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);//設定視窗標題列的關閉按鈕結束程式執行
        setVisible(true);//顯示視窗
        setResizable(false);//設定不可更改大小
        setLayout(null);
	}
	@Override
	public void actionPerformed(ActionEvent arg0) {
		for(int i=0;i<14;i++){
			if(arg0.getSource()==button[i]){
				display=display.substring(0, display.length()-1);
				modifeDisplay(button[i].getText());//將按鍵上的字串傳入
				display=display + ".";
				displayLabel.setText(display);
			}
		}
	}
	private void modifeDisplay(String a){
		if(Cal!="="){//如果按鍵剛剛是按=算出答案 則下次按鍵時把答案清空
			calculator(a,0);
		}
		else{
			int temp2=Integer.parseInt(display);
			display="";
			calculator(a,temp2);
			if(Cal=="="){
				Cal="";
			}
		}
	}
	private void calculator(String a,int b){
		if(a=="C"){
			temp=0;
			display="0";
		}
		else if(a=="+" || a=="-")
		{
			if(Cal=="="){		//剛剛按等於 在按+還是把數字加上去
				temp=b;
				Cal=a;
				click=false;
				return;
			}
			if(!click){
				return;
			}
			if(temp==0){//如果地一個數字沒有東西 直接等於
				temp=Integer.parseInt(display);
				if(temp > 999999999 || temp < -999999999){
					display="error";
					temp=0;
					click = true;
					Cal="";
					return;
				}
				else{
					display="0";
				}
			}
			else{
				if(Cal=="+"){							//累加上去
					temp+=Integer.parseInt(display);
				}
				else if(Cal=="-"){						//累減
					temp-=Integer.parseInt(display);
				}
				if(temp > 999999999 || temp < -999999999){
					display="error";
					temp=0;
					click = true;
					Cal="";
					return;
				}
				else{
					display=String.valueOf(temp);
				}
			}
			click=false;
			Cal=a;//設定按鍵狀態
		}
		else if(a=="="){
			if(Cal=="+" && click){	//判斷剛剛是按甚麼運算
				int number=Integer.parseInt(display);
				number=temp+number;
				if(number > 999999999 || number < -999999999){
					display="error";
					temp=0;
					click = true;
					Cal="";
					return;
				}
				else{
					display=String.valueOf(number);
				}
			}
			else if(Cal=="-" && click){
				int number=Integer.parseInt(display);
				number=temp-number;
				if(number > 999999999 || number < -999999999){
					display="error";
					temp=0;

					click = true;
					Cal="";
					return;
				}
				else{
					display=String.valueOf(number);
				}
			}
			else{
				display=String.valueOf(temp);
			}
			Cal="=";
			click=true;
			temp=Integer.parseInt(display);
		}
		else{
			if(Cal=="+" || Cal=="-"){
				if(!click){
					display="";
				}
				click=true;
			}
			if(Cal=="="){
				temp=0;
			}
			if(display == "0." || display=="error"){
				display="";
			}
			if(display.length() < 9){
				display=display+a;
			}
		}
	}
}
