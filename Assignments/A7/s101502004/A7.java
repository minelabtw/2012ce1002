package ce1002.A7.s101502004;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.*;
import javax.swing.border.Border;

public class A7 extends JFrame implements ActionListener{
	private Border emptyBorder = BorderFactory.createEmptyBorder(40, 40, 40, 40);//與視窗之間的框線
	private JTextField answer = new JTextField("0");
	private JButton seven = new JButton("7");
	private JButton eight = new JButton("8");
	private JButton nine = new JButton("9");
	private JButton minus = new JButton("-");
	private JButton four = new JButton("4");
	private JButton five = new JButton("5");
	private JButton six = new JButton("6");
	private JButton plus = new JButton("+");
	private JButton one = new JButton("1");
	private JButton two = new JButton("2");
	private JButton three = new JButton("3");
	private JButton equa = new JButton("=");
	private JButton zero = new JButton("0");
	private JButton C = new JButton("c");
	int buffle=0;//儲存目前輸入的數字
	int lastanswer=0;//儲存最後要顯示的數字
	int count=0;//計算按鈕點擊次數
	boolean pluscount=true;//+點擊
	boolean minuscount=true;//-點擊
	
	public A7(){
		JPanel left1 = new JPanel();
		left1.setLayout(new GridLayout(1, 2, 10, 10));
		left1.add(one);
		left1.add(two);
		
		JPanel left2 = new JPanel();
		left2.setLayout(new GridLayout(2, 1, 10, 10));
		left2.add(left1);
		left2.add(zero);
		
		JPanel right1 = new JPanel();
		right1.setLayout(new GridLayout(2, 1, 10, 10));
		right1.add(three);
		right1.add(C);
		
		JPanel right2 = new JPanel();
		right2.setLayout(new GridLayout(1, 2, 10, 10));
		right2.add(right1);
		right2.add(equa);
		
		JPanel down = new JPanel();
		down.setLayout(new GridLayout(1, 2, 10, 10));
		down.add(left2);
		down.add(right2);
		
		JPanel up = new JPanel();
		up.setLayout(new GridLayout(2, 4, 10, 10));
		up.add(seven);
		up.add(eight);
		up.add(nine);
		up.add(minus);
		up.add(four);
		up.add(five);
		up.add(six);
		up.add(plus);
		
		JPanel all = new JPanel();
		all.setLayout(new GridLayout(3, 1, 10, 10));
		all.add(answer);
		all.add(up);
		all.add(down);
		all.setBorder(emptyBorder);
		
		add(all);
		
		seven.addActionListener(this);
		eight.addActionListener(this);
		nine.addActionListener(this);
		minus.addActionListener(this);
		four.addActionListener(this);
		five.addActionListener(this);
		six.addActionListener(this);
		plus.addActionListener(this);
		one.addActionListener(this);
		two.addActionListener(this);
		three.addActionListener(this);
		equa.addActionListener(this);
		zero.addActionListener(this);
		C.addActionListener(this);
	}
	public static void main(String[] args){
		A7 f = new A7();
		f.setTitle("小算盤");
		f.setSize(300, 400);
		f.setLocationRelativeTo(null);
		f.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		f.setVisible(true);
	}
	
	public void actionPerformed(ActionEvent e){
		String word = null;//顯示的字串
		
		if(e.getSource()==seven){
			buffle=buffle*10+7;
			word = Integer.toString(buffle);
			if(word.length()>=9){//溢位
				word="error";
				lastanswer=0;
				buffle=0;
				count=0;
				pluscount=true;
				minuscount=true;
			}
			answer.setText(word);
		}
		if(e.getSource()==eight){
			buffle=buffle*10+8;
			word = Integer.toString(buffle);
			if(word.length()>=9){
				word="error";
				lastanswer=0;
				buffle=0;
				count=0;
				pluscount=true;
				minuscount=true;
			}
			answer.setText(word);
		}
		if(e.getSource()==nine){
			buffle=buffle*10+9;
			word = Integer.toString(buffle);
			if(word.length()>=9){
				word="error";
				lastanswer=0;
				buffle=0;
				count=0;
				pluscount=true;
				minuscount=true;
			}
			answer.setText(word);
		}
		if(e.getSource()==four){
			buffle=buffle*10+4;
			word = Integer.toString(buffle);
			if(word.length()>=9){
				word="error";
				lastanswer=0;
				buffle=0;
				count=0;
				pluscount=true;
				minuscount=true;
			}
			answer.setText(word);
		}
		if(e.getSource()==five){
			buffle=buffle*10+5;
			word = Integer.toString(buffle);
			if(word.length()>=9){
				word="error";
				lastanswer=0;
				buffle=0;
				count=0;
				pluscount=true;
				minuscount=true;
			}
			answer.setText(word);
		}
		if(e.getSource()==six){
			buffle=buffle*10+6;
			word = Integer.toString(buffle);
			if(word.length()>=9){
				word="error";
				lastanswer=0;
				buffle=0;
				count=0;
				pluscount=true;
				minuscount=true;
			}
			answer.setText(word);
		}
		if(e.getSource()==one){
			buffle=buffle*10+1;
			word = Integer.toString(buffle);
			if(word.length()>=9){
				word="error";
				lastanswer=0;
				buffle=0;
				count=0;
				pluscount=true;
				minuscount=true;
			}
			answer.setText(word);
		}
		if(e.getSource()==two){
			buffle=buffle*10+2;
			word = Integer.toString(buffle);
			if(word.length()>=9){
				word="error";
				lastanswer=0;
				buffle=0;
				count=0;
				pluscount=true;
				minuscount=true;
			}
			answer.setText(word);
		}
		if(e.getSource()==three){
			buffle=buffle*10+3;
			word = Integer.toString(buffle);
			if(word.length()>=9){
				word="error";
				lastanswer=0;
				buffle=0;
				count=0;
				pluscount=true;
				minuscount=true;
			}
			answer.setText(word);
		}
		if(e.getSource()==zero){
			buffle=buffle*10;
			word = Integer.toString(buffle);
			if(word.length()>=9){
				word="error";
				lastanswer=0;
				buffle=0;
				count=0;
				pluscount=true;
				minuscount=true;
			}
			answer.setText(word);
		}
		if(e.getSource()==C){//所有變數歸0
			buffle=0;
			lastanswer=0;
			count=0;
			pluscount=true;
			minuscount=true;
			answer.setText("0");
		}
		if(e.getSource()==plus){
			if(minuscount==true && pluscount==false){//加法仍需包含減法才能連續計算
				lastanswer-=buffle;
				buffle=0;
				pluscount=true;
				minuscount=false;
				word = Integer.toString(lastanswer);
				if(word.length()>=9){
					word="error";
					lastanswer=0;
					buffle=0;
					count=0;
					pluscount=true;
					minuscount=true;
				}
				answer.setText(word);
			}
			else{
				lastanswer+=buffle;
				buffle=0;
				count++;
				pluscount=true;
				minuscount=false;
				word = Integer.toString(lastanswer);
				if(word.length()>=9){
					word="error";
					lastanswer=0;
					buffle=0;
					count=0;
					pluscount=true;
					minuscount=true;
				}
				answer.setText(word);
			}
		}
		if(e.getSource()==minus){
			if(count==0){//第一次點擊為-按鈕時所作的特殊處理
				lastanswer=buffle;
				buffle=0;
				word = Integer.toString(lastanswer);
				answer.setText(word);
				pluscount=false;
				minuscount=true;
				count++;
			}
			else{
				if(pluscount==true && minuscount==false){
					lastanswer+=buffle;
					buffle=0;
					count++;
					pluscount=false;
					minuscount=true;
					word = Integer.toString(lastanswer);
					if(word.length()>=9){
						word="error";
						lastanswer=0;
						buffle=0;
						count=0;
						pluscount=true;
						minuscount=true;
					}
					answer.setText(word);
				}
				else{
					lastanswer-=buffle;
					buffle=0;
					pluscount=false;
					minuscount=true;
					word = Integer.toString(lastanswer);
					if(word.length()>=9){
						word="error";
						lastanswer=0;
						buffle=0;
						count=0;
						pluscount=true;
						minuscount=true;
					}
					answer.setText(word);
				}
			}
		}
		if(e.getSource()==equa){
			if(pluscount==true && minuscount==false){
				lastanswer+=buffle;
				buffle=0;
				count++;
				pluscount=true;
				minuscount=true;
				word = Integer.toString(lastanswer);
				if(word.length()>=9){
					word="error";
					lastanswer=0;
					buffle=0;
					count=0;
					pluscount=true;
					minuscount=true;
				}
				answer.setText(word);
			}
			if(minuscount==true && pluscount==false){
				lastanswer-=buffle;
				buffle=0;
				pluscount=true;
				minuscount=true;
				word = Integer.toString(lastanswer);
				if(word.length()>=9){
					word="error";
					lastanswer=0;
					buffle=0;
					count=0;
					pluscount=true;
					minuscount=true;
				}
				answer.setText(word);
			}
		}
	}
}
