package ce1002.A7.s101502502;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JTextField;

public class A7 extends JFrame implements ActionListener 
{
	public JButton btn0 = new JButton("0");
	public JButton btn1 = new JButton("1");
	public JButton btn2 = new JButton("2");
	public JButton btn3 = new JButton("3");
	public JButton btn4 = new JButton("4");
	public JButton btn5 = new JButton("5");
	public JButton btn6 = new JButton("6");
	public JButton btn7 = new JButton("7");
	public JButton btn8 = new JButton("8");
	public JButton btn9 = new JButton("9");
	public JButton btnPlus = new JButton("+");
	public JButton btnMinus = new JButton("-");
	public JButton btnClear = new JButton("c");
	public JButton btnEqual = new JButton("=");
	public JTextField show = new JTextField("0."); 
	
	public String NumberString = "";
	public int num = 0;
	public int operator = 0;//0是按除了加減的其他button
	//1是按加號
	//2是按減號
	public boolean EraseShow = false;//用來判斷是否要清除textfield裡面的數字
	
	public A7()
	{
		setLayout(null);
		btn0.setBounds(40, 225, 110, 30);
		btnClear.setBounds(160, 225, 50, 30);
		btn1.setBounds(40, 180, 50, 30);
		btn2.setBounds(100, 180, 50, 30);
		btn3.setBounds(160, 180, 50, 30);
		btnEqual.setBounds(220, 180, 50, 75);
		btn4.setBounds(40, 135, 50, 30);
		btn5.setBounds(100, 135, 50, 30);
		btn6.setBounds(160, 135, 50, 30);
		btnPlus.setBounds(220, 135, 50, 30);
		btn7.setBounds(40, 95, 50, 30);
		btn8.setBounds(100, 95, 50, 30);
		btn9.setBounds(160, 95, 50, 30);
		btnMinus.setBounds(220, 95, 50, 30);
		
		show.setBounds(40, 35, 230, 40);
		show.setHorizontalAlignment(JTextField.RIGHT);//讓textfield裡面的數字靠右
		
		add(btn0);
		add(btnClear);
		add(btn1);
		add(btn2);
		add(btn3);
		add(btnEqual);
		add(btn4);
		add(btn5);
		add(btn6);
		add(btnPlus);
		add(btn7);
		add(btn8);
		add(btn9);
		add(btnMinus);
		add(show);
		
		btn0.addActionListener(this);
		btn1.addActionListener(this);
		btn2.addActionListener(this);
		btn3.addActionListener(this);
		btn4.addActionListener(this);
		btn5.addActionListener(this);
		btn6.addActionListener(this);
		btn7.addActionListener(this);
		btn8.addActionListener(this);
		btn9.addActionListener(this);
		btnPlus.addActionListener(this);
		btnMinus.addActionListener(this);
		btnClear.addActionListener(this);
		btnEqual.addActionListener(this);
	}
	
	public static void main(String[] args)
	{
		A7 frame = new A7();
		frame.setTitle("小算盤");
		frame.setSize(320, 320);
		frame.setLocationRelativeTo(null);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setVisible(true);
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		// TODO Auto-generated method stub
		Object src = e.getSource();//用來判斷我按的是哪一個button
		
		if(EraseShow==true)//清除textfield的數字
		{
			NumberString = "";
			show.setText("");
			EraseShow = false;
		}
			
		
		if(src==btn0)
		{
			NumberString += "0";
			show.setText(NumberString);
		}
		else if(src==btn1)
		{
			NumberString += "1";
			show.setText(NumberString);
		}
		else if(src==btn2)
		{
			NumberString += "2";
			show.setText(NumberString);
		}
		else if(src==btn3)
		{
			NumberString += "3";
			show.setText(NumberString);
		}
		else if(src==btn4)
		{
			NumberString += "4";
			show.setText(NumberString);
		}
		else if(src==btn5)
		{
			NumberString += "5";
			show.setText(NumberString);
		}
		else if(src==btn6)
		{
			NumberString += "6";
			show.setText(NumberString);
		}
		else if(src==btn7)
		{
			NumberString += "7";
			show.setText(NumberString);
		}
		else if(src==btn8)
		{
			NumberString += "8";
			show.setText(NumberString);
		}
		else if(src==btn9)
		{
			NumberString += "9";
			show.setText(NumberString);
		}
		else if(src==btnPlus)
		{
			if(operator==1)//連加的情況
				num += Integer.parseInt(NumberString);
			else if(operator==2)//加換減的情況
				num -= Integer.parseInt(NumberString);
			else//一開始或按完等號的情況
				num = Integer.parseInt(NumberString);
			
			if(operator!=0)
				show.setText("" + num);
			
			operator = 1;
		}
		else if(src==btnMinus)
		{
			if(operator==1)//加換減的情況
				num += Integer.parseInt(NumberString);
			else if(operator==2)//連減的情況
				num -= Integer.parseInt(NumberString);
			else//一開始或按完等號的情況
				num = Integer.parseInt(NumberString);
			
			if(operator!=0)
				show.setText(""+num);
			
			operator = 2;
		}
		else if(src==btnClear)//清除鍵
		{
			NumberString = "";
			num = 0;
			show.setText("0.");
			operator = 0;
		}
		else if(src==btnEqual)//等號鍵
		{
			if(operator==1)
				num += Integer.parseInt(NumberString);
			else if(operator==2)
				num -= Integer.parseInt(NumberString);
			else
				num = Integer.parseInt(NumberString);
			
			if(operator!=0)
				show.setText(""+num);
			
			NumberString = "" + num;
			num = 0;
			operator = 0;
		}
		
		if(src==btnPlus || src==btnMinus)//若按完加減，則需要把textfield清空
			EraseShow=true;
		else
			EraseShow=false;
		
		if(Integer.parseInt(show.getText())>999999999)//溢位的情況
			show.setText("error.");
	}
}
