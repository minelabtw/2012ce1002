//按鈕排版參考:http://docs.oracle.com/javase/tutorial/uiswing/layout/gridbag.html+小算盤參考:YOUTUBE一堆影片
package ce1002.A7.s101502505;
import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import javax.swing.event.*;

public class A7 extends JFrame implements ActionListener{
	JFrame frame = new JFrame("小算盤");
	JTextArea txt = new JTextArea();
	JButton b7 = new JButton("7");
	JButton b8 = new JButton("8");
	JButton b9 = new JButton("9");
	JButton sub = new JButton("-");	
	JButton b4 = new JButton("4");
	JButton b5 = new JButton("5");
	JButton b6 = new JButton("6");
	JButton ad = new JButton("+");
	JButton b1 = new JButton("1");
	JButton b2 = new JButton("2");
	JButton b3 = new JButton("3");
	JButton eq = new JButton("=");
	JButton b0 = new JButton("0");
	JButton c = new JButton("C");
	int adc=0,subc=0;
	double num1,num2,res;
	
	public static void main(String[] args){
		new A7();
	}
	public A7() {
		frame.setBackground(new Color(255,175,175));
		frame.setLayout(new GridBagLayout());
		frame.setSize(230,230);
		frame.setVisible(true);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		//編排按鈕的位置
		GridBagConstraints C1 = new GridBagConstraints();
		C1.gridx = 0;
		C1.gridy = 0;
		C1.gridwidth = 4;
		C1.gridheight = 1;
		C1.fill = GridBagConstraints.HORIZONTAL;
		C1.anchor = GridBagConstraints.WEST;
	    frame.add(txt,C1);
	    
		GridBagConstraints C2 = new GridBagConstraints();
		C2.gridx = 0;
		C2.gridy = 1;
		C2.gridwidth = 1;
		C2.gridheight = 1;
		C2.insets = new Insets(5,0,0,3);
		C2.fill = GridBagConstraints.NONE;
		C2.anchor = GridBagConstraints.CENTER;
		frame.add(b7,C2);
		b7.addActionListener(this);
        
		GridBagConstraints C3 = new GridBagConstraints();
		C3.gridx = 1;
		C3.gridy = 1;
		C3.gridwidth = 1;
		C3.gridheight = 1;
		C3.insets = new Insets(5,3,0,3);
		C3.fill = GridBagConstraints.BOTH;
		C3.anchor = GridBagConstraints.CENTER;
		frame.add(b8,C3);
		b8.addActionListener(this);
		
		GridBagConstraints C4 = new GridBagConstraints();
		C4.gridx = 2;
		C4.gridy = 1;
		C4.gridwidth = 1;
		C4.gridheight = 1;
		C4.insets = new Insets(5,3,0,3);
		C4.fill = GridBagConstraints.BOTH;
		C4.anchor = GridBagConstraints.CENTER;
		frame.add(b9,C4);
		b9.addActionListener(this);
		
		GridBagConstraints C5 = new GridBagConstraints();
		C5.gridx = 3;
		C5.gridy = 1;
		C5.gridwidth = 1;
		C5.gridheight = 1;
		C5.insets = new Insets(5,3,0,0);
		C5.fill = GridBagConstraints.BOTH;
		C5.anchor = GridBagConstraints.CENTER;
		frame.add(sub,C5);
		sub.addActionListener(this);
		
		GridBagConstraints C6 = new GridBagConstraints();
		C6.gridx = 0;
		C6.gridy = 2;
		C6.gridwidth = 1;
		C6.gridheight = 1;
		C6.insets = new Insets(5,0,0,3);
		C6.fill = GridBagConstraints.NONE;
		C6.anchor = GridBagConstraints.CENTER;
		frame.add(b4,C6);
		b4.addActionListener(this);
		
		GridBagConstraints C7 = new GridBagConstraints();
		C7.gridx = 1;
		C7.gridy = 2;
		C7.gridwidth = 1;
		C7.gridheight = 1;
		C7.insets = new Insets(5,3,0,3);
		C7.fill = GridBagConstraints.NONE;
		C7.anchor = GridBagConstraints.CENTER;
		frame.add(b5,C7);
		b5.addActionListener(this);
		
		GridBagConstraints C8 = new GridBagConstraints();
		C8.gridx = 2;
		C8.gridy = 2;
		C8.gridwidth = 1;
		C8.gridheight = 1;
		C8.insets = new Insets(5,3,0,3);
		C8.fill = GridBagConstraints.NONE;
		C8.anchor = GridBagConstraints.CENTER;
		frame.add(b6,C8);
		b6.addActionListener(this);
			
		GridBagConstraints C9 = new GridBagConstraints();
		C9.gridx = 3;
		C9.gridy = 2;
		C9.gridwidth = 1;
		C9.gridheight = 1;
		C9.insets = new Insets(5,3,0,0);
		C9.fill = GridBagConstraints.NONE;
		C9.anchor = GridBagConstraints.CENTER;
		frame.add(ad,C9);
		ad.addActionListener(this);
		
		GridBagConstraints C10 = new GridBagConstraints();
		C10.gridx = 0;
		C10.gridy = 3;
		C10.gridwidth = 1;
		C10.gridheight = 1;
		C10.insets = new Insets(5,0,0,3);
		C10.fill = GridBagConstraints.NONE;
		C10.anchor = GridBagConstraints.CENTER;
		frame.add(b1,C10);
		b1.addActionListener(this);
			
		GridBagConstraints C11 = new GridBagConstraints();
		C11.gridx = 1;
		C11.gridy = 3;
		C11.gridwidth = 1;
		C11.gridheight = 1;
		C11.insets = new Insets(5,3,0,3);
		C11.fill = GridBagConstraints.NONE;
		C11.anchor = GridBagConstraints.CENTER;
		frame.add(b2,C11);
		b2.addActionListener(this);
			
		GridBagConstraints C12 = new GridBagConstraints();
		C12.gridx = 2;
		C12.gridy = 3;
		C12.gridwidth = 1;
		C12.gridheight = 1;
		C12.insets = new Insets(5,3,0,3);
		C12.fill = GridBagConstraints.NONE;
		C12.anchor = GridBagConstraints.CENTER;
		frame.add(b3,C12);
		b3.addActionListener(this);
		
		GridBagConstraints C13 = new GridBagConstraints();
		C13.gridx = 3;
		C13.gridy = 3;
		C13.gridwidth = 1;
		C13.gridheight = 2;
		C13.insets = new Insets(5,3,0,0);
		C13.fill = GridBagConstraints.VERTICAL;
		C13.anchor = GridBagConstraints.CENTER;
		frame.add(eq,C13);
		eq.addActionListener(this);
		
		GridBagConstraints C14 = new GridBagConstraints();
		C14.gridx = 0;
		C14.gridy = 4;
		C14.gridwidth = 2;
		C14.gridheight = 1;
		C14.insets = new Insets(5,0,0,3);
		C14.fill = GridBagConstraints.HORIZONTAL;
		C14.anchor = GridBagConstraints.CENTER;
		frame.add(b0,C14);
		b0.addActionListener(this);
		
		GridBagConstraints C15 = new GridBagConstraints();
		C15.gridx = 2;
		C15.gridy = 4;
		C15.gridwidth = 1;
		C15.gridheight = 1;
		C15.insets = new Insets(5,3,0,3);
		C15.fill = GridBagConstraints.NONE;
		C15.anchor = GridBagConstraints.CENTER;
		frame.add(c,C15);
		c.addActionListener(this);
		//編排按鈕的位置
	}
	//按下按鈕後行為
	public void actionPerformed(ActionEvent e){
		if(e.getSource() == b1)
		{
			txt.append("1");
		}
		if(e.getSource() == b2)
		{
			txt.append("2");
		}
		if(e.getSource() == b3)
		{
			txt.append("3");
		}
		if(e.getSource() == b4)
		{
			txt.append("4");
		}
		if(e.getSource() == b5)
		{
			txt.append("5");
		}
		if(e.getSource() == b6)
		{
			txt.append("6");
		}
		if(e.getSource() == b7)
		{
			txt.append("7");
		}
		if(e.getSource() == b8)
		{
			txt.append("8");
		}
		if(e.getSource() == b9)
		{
			txt.append("9");
		}
		if(e.getSource() == b0)
		{
			txt.append("0");
		}
		if(e.getSource() == ad)
		{
			num1 = numberget();
			txt.setText("");
			adc = 1;
			subc = 0;
		}
		if(e.getSource() == sub)
		{
			num1 = numberget();
			txt.setText("");
			adc = 0;
			subc = 1;
		}
		if(e.getSource() == eq)
		{
			num2 = numberget();
			res = num1 + num2;
			if(res >= 999999999)
				txt.setText("error.");
			else
				txt.setText(Double.toString(res));
		}
		if(e.getSource() == c)
		{
			num1 = 0.0;
			num2 = 0.0;
			txt.setText("");
		}
	}
	//讀取使用者輸入的數字
	public Double numberget(){
		Double number1;
		String str;
		str = txt.getText();
		number1 = Double.valueOf(str);
		return number1;
	}
}
