package ce1002.A7.s101502525;

import java.awt.*;
import java.awt.event.*;
import java.util.Stack;

import javax.swing.*;
import javax.swing.border.Border;

public class A7 extends JFrame{
	private JButton[] button;
	private JTextField screen;
	private GridBagConstraints gbc[],scrgbc;
	private String key[]={"7","8","9","-","4","5","6","+","1","2","3","=","0","C"};
	private int VKkey[]={KeyEvent.VK_NUMPAD7,KeyEvent.VK_NUMPAD8,KeyEvent.VK_NUMPAD9,KeyEvent.VK_SUBTRACT,KeyEvent.VK_NUMPAD4,KeyEvent.VK_NUMPAD5,KeyEvent.VK_NUMPAD6,
							KeyEvent.VK_ADD,KeyEvent.VK_NUMPAD1,KeyEvent.VK_NUMPAD2,KeyEvent.VK_NUMPAD3,KeyEvent.VK_ENTER,KeyEvent.VK_NUMPAD0,KeyEvent.VK_C};
	private Stack<String> stack,sign;
	private String numTmp,inTmp;
	private boolean value;
	
	public A7(){
		super("小算盤");
		setData();
		setUI();
	}
	private void setData(){
		stack=new Stack<String>();
		sign=new Stack<String>();
		numTmp="0";
		value=false;
	}
	private void setGridBagConstraints(){
		scrgbc=new GridBagConstraints(0,0,  4,1,  1,1,  GridBagConstraints.CENTER,GridBagConstraints.BOTH,new Insets(1, 1, 1, 1),  0,0);//screen width=4
		gbc=new GridBagConstraints[key.length];
		for(int i=0;i<gbc.length;i++)
			gbc[i]=new GridBagConstraints(i%4,i/4+1,  1,1,  1,1,  GridBagConstraints.CENTER,GridBagConstraints.BOTH,new Insets(1, 1, 1, 1),  0,0);
		gbc[11].gridheight=2;//"=", equal button
		gbc[12].gridwidth=2;//"0", 0 button
		gbc[13].gridx=2;//"C", clear button
	}
	private void setUI() {
		//layout
		setLayout(new GridBagLayout());
		setGridBagConstraints();
		
		//screen
		screen=new JTextField(numTmp+".");
		screen.setFont(new Font("", Font.BOLD, 20));
		screen.setHorizontalAlignment(JTextField.RIGHT);
		screen.setEditable(false);
		screen.setFocusable(false);
		add(screen,scrgbc);
		
		//button
		button=new JButton[key.length];
		for(int i=0;i<button.length;i++){
			final int tmp=i;
			button[i]=new JButton(key[i].toString());
			button[i].setFont(new Font("", Font.BOLD, 20));
			button[i].setBorder(new Border(){//round border
		        public Insets getBorderInsets(Component c) {
		            return new Insets(0,0,0,0);
		        }
		        public boolean isBorderOpaque() {
		            return true;
		        }
		        public void paintBorder(Component c, Graphics g, int x, int y, int width, int height) {
		            g.drawRoundRect(x,y,width-1,height-1,15,15);
		        }
			});
			button[i].addActionListener(new ActionListener(){
				public void actionPerformed(ActionEvent e) {
					inTmp=key[tmp];
					screen.setText(calc()+".");
				}
			});
			button[i].setFocusable(false);
			add(button[i],gbc[i]);
		}
		//frame
		setSize(200,300);
		setDefaultCloseOperation(EXIT_ON_CLOSE);
		setFocusable(true);
		addKeyListener(new KeyAdapter(){//key press
			public void keyPressed(KeyEvent e) {
				for(int i=0;i<VKkey.length;i++)
					if(e.getExtendedKeyCode()==VKkey[i]){
						inTmp=key[i];
						screen.setText(calc()+".");
					}
			}
		});
		setVisible(true);
	}
	private String calc(){
		switch(inTmp){
		case "+"://signs
		case "-":
		case "=":
			if(!value){//input number first time
				stack.push(numTmp);
				sign.push(inTmp);
				value=true;
			}
			else//input number after the first time
					stack.push(calcAns(sign.pop(),stack.pop(),numTmp));
			numTmp="0";
			sign.clear();
			sign.push(inTmp);
			if(stack.peek().length()>9){//error
				clear();
				return "error";
			}
			else
				return stack.peek();
		case "C"://clear
			return clear();
		default://numbers
			if(numTmp.length()<=8)
				numTmp+=inTmp;
			return numTmp=String.valueOf(Integer.valueOf(numTmp));//trim 0 at first
		}
	}
	private String clear(){
		stack.clear();
		sign.clear();
		value=false;
		return numTmp="0";
	}
	private String calcAns(String signSelect,String A,String B){
		return signSelect=="+"?String.valueOf(Integer.valueOf(A)+Integer.valueOf(B))://plus
			   signSelect=="-"?String.valueOf(Integer.valueOf(A)-Integer.valueOf(B))://minus
				   			   B;//equal
	}
	public static void main(String[] args){
		new A7();
	}
}
