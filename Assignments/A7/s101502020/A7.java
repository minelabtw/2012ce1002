//A7 by J
//this is fucking annoying
package ce1002.A7.s101502020;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;

public class A7 {

	JFrame frm ;		//main frame here
	JButton jbt1 , jbt2 , jbt3 , jbt4 , jbt5 , jbt6 , jbt7 , jbt8 , jbt9 , jbt0 , jbtc , jbtp , jbtm , jbte ;		//all buttons here 
	JTextField jtf ;		//maybe forget it!
	long x=0 , y=0 , buff  , r ;		//x as the first number; y as the second number ; buff for restoring all input data ; r as the result of the arithmetic.
	boolean xValue=false , yValue=false , plus=false , minus=false ;		
	//x&y value is used to represent that whether there's a number or not ; plus & minus is for executing them.
	
	public A7() throws InterruptedException{
		setUI();
	}		//constructor
	
	private void setUI(){

		//frame content
		frm = new JFrame("A7");
		frm.setSize(260, 350);
		frm.setResizable(false);
		frm.addWindowListener(new AdapterDemo());		//i'm not sure about this function yet!		
		frm.setLayout(null);		//still not sure about the exactly layout type to use!
		
		//buttons below
		jbt0 = new JButton("0");
		jbt0.setSize(95, 30);
		jbt0.setLocation(30,250);
		jbt0.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				set(0);
			}
		});
		frm.add(jbt0);
		
		jbt1 = new JButton("1");
		jbt1.setSize(45, 30);
		jbt1.setLocation(30, 200);
		jbt1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				set(1);
			}
		});
		frm.add(jbt1);
		
		jbt2 = new JButton("2");
		jbt2.setSize(45, 30);
		jbt2.setLocation(80, 200);
		jbt2.addActionListener(new ActionListener() {
			public void actionPerformed (ActionEvent arg0) {
				set(2);
			}
		});
		frm.add(jbt2);
		
		jbt3 = new JButton("3");
		jbt3.setSize(45, 30);
		jbt3.setLocation(130, 200);
		jbt3.addActionListener(new ActionListener() {
			public void actionPerformed (ActionEvent arg0) {
				set(3);
			}
		});
		frm.add(jbt3);
		
		jbt4 = new JButton("4");
		jbt4.setSize(45, 30);
		jbt4.setLocation(30, 150);
		jbt4.addActionListener(new ActionListener() {
			public void actionPerformed (ActionEvent arg0) {
				set(4);
			}
		});
		frm.add(jbt4);
		
		jbt5 = new JButton("5");
		jbt5.setSize(45,30);
		jbt5.setLocation(80, 150);
		jbt5.addActionListener(new ActionListener(){
			public void actionPerformed (ActionEvent arg0) {
				set(5);
			}
		});
		frm.add(jbt5);
		
		jbt6 = new JButton("6");
		jbt6.setSize(45, 30);
		jbt6.setLocation(130, 150);
		jbt6.addActionListener(new ActionListener() {
			public void actionPerformed (ActionEvent arg0) {
				set(6);
			}
		});
		frm.add(jbt6);
		
		jbt7 = new JButton("7");
		jbt7.setSize(45, 30);
		jbt7.setLocation(30, 100);
		jbt7.addActionListener(new ActionListener() {
			public void actionPerformed (ActionEvent arg0) {
				set(7);
			}
		});
		frm.add(jbt7);
		
		jbt8 = new JButton("8");
		jbt8.setSize(45, 30);
		jbt8.setLocation(80, 100);
		jbt8.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				set(8);
			}
		});
		frm.add(jbt8);
		
		jbt9 = new JButton("9");
		jbt9.setSize(45, 30);
		jbt9.setLocation(130, 100);
		jbt9.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				set(9);
			}
		});
		frm.add(jbt9);
		
		jbtc = new JButton("C");
		jbtc.setSize(45, 30);
		jbtc.setLocation(130, 250);
		jbtc.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0 ) {
				clear();
			}
		});
		frm.add(jbtc);
		
		jbtp = new JButton("+");
		jbtp.setSize(45, 30);
		jbtp.setLocation(180, 150);
		jbtp.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				plus();
			}
		});
		frm.add(jbtp);
		
		jbtm = new JButton("-");
		jbtm.setSize(45, 30);
		jbtm.setLocation(180, 100);
		jbtm.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				minus();
			}
		});
		frm.add(jbtm);
		
		jbte = new JButton("=");
		jbte.setSize(45,80);
		jbte.setLocation(180, 200);
		jbte.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				answer();
			}
		});
		frm.add(jbte);
		//fucking buttons above
		
		//Text here
		jtf = new JTextField("0");
		jtf.setHorizontalAlignment(SwingConstants.RIGHT);		//show text from right!
		jtf.setEditable(false);
		jtf.setLocation(30, 30);
		jtf.setSize(200, 50);
		jtf.setBackground(Color.white);
		frm.add(jtf);
		
		frm.setVisible(true);
	}		//major about the GUI
	
	private void set(int i) {
		
		if ( buff< 100000000) {
			buff = 10*buff+i;
			jtf.setText(""+buff);
		}
		else {
			jtf.setText("error");
			x=0;
			y=0;
			buff=0;
			xValue=false;
			yValue=false;
		}		//too much digits!!
	
	}		//this method is used to set numbers
	
	private void sign(){
		
		if (xValue) {
			y=buff ;
			buff=0;
			yValue=true ;
		}
		else {
			x=buff ;
			buff=0;
			xValue=true ;
		}
		
	}		//deciding the boolean variables value
	
	private void plus() {
		
		sign();
		
		if (yValue) {
			equal();
			x=r ;
			y=0 ;
			yValue=false;
		}		//it means that we have 2 value & do a algorithm now!
		
		plus=true;
		minus=false;
	}
	
	private void minus() {
		sign();
		if (yValue) {
			equal();
			x=r ;
			y=0 ;
			yValue=false;
		}		//same idea as plus()
		
		minus= true ;
		plus=false;
	}
	
	private void equal() {
		
		if (plus) {
			r=x+y;
		}		// do plus
		else if (minus) {
			r=x-y;
		}		//do minus
		
		if ( r>=1000000000 ) {
			jtf.setText("error");
			x=0 ;
			y=0 ;
			buff = 0 ;
			xValue=false ;
			yValue=false ;
		}
		else {
			jtf.setText("" + r);
			xValue=true ;
			yValue=false ;
			plus=false;
			minus=false;
			x=r ;
			y=0 ;
		}
	}
	
	private void answer() {
		
		sign();
		if (plus) {
			r=x+y ;
			plus=false ;
			x=r ;
			y=0 ;
			yValue=false ;
		}
		else if (minus) {
			r=x-y;
			minus=false ;
			x=r ;
			y=0;
			yValue=false ;
		}
		if ( r>=1000000000 ) {
			jtf.setText("error");
			x=0 ;
			y=0 ;
			buff = 0 ;
			xValue=false ;
			yValue=false ;
		}
		else {
			jtf.setText("" + r);
			xValue=true ;
			yValue=false ;
			plus=false;
			minus=false;
			x=r ;
			y=0 ;
		}
	}
	
	private void clear() {
		
		x=0 ;
		y=0 ;
		buff=0 ;
		xValue=false;
		yValue=false;
		//reset all needed data needed to be reseted
		jtf.setText(""+buff);
		
	}
	
	public static void main(String[] args) throws InterruptedException {
		new A7();
	}

}

class AdapterDemo extends WindowAdapter{
	public void windowClosing(WindowEvent e) {
		System.exit(0);
	}
	
}