package ce1002.A7.s101502506;

import java.awt.*;
import java.awt.event.*;

public class A7 extends Frame implements ActionListener
{
static A7 frm=new A7();
static Button btSum=new Button("+");
static Button btCut=new Button("-");
static Button bt=new Button("C");
static Button zero=new Button("0");
static Button btBe=new Button("=");
static Button one=new Button("1");
static Button two=new Button("2");
static Button three=new Button("3");
static Button four=new Button("4");
static Button five=new Button("5");
static Button six=new Button("6");
static Button seven=new Button("7");
static Button eight=new Button("8");
static Button nine=new Button("9");
static Label lab=new Label("0",Label.RIGHT);//數字0顯示在label的最右側
int Number=0 , state=3 , totle=0;//初始化三個變數
String chose="";

public static void main(String args[]){
frm.setTitle("小算盤");
frm.setLayout(null);
frm.setSize(210,270);
frm.setResizable(false);
btSum.setBounds(155,130,35,30);
btSum.addActionListener(frm);
btCut.setBounds(155,90,35,30);
btCut.addActionListener(frm);
bt.setBounds(110,210,35,30);
bt.addActionListener(frm);
zero.setBounds(20,210,80,30);
zero.addActionListener(frm);
btBe.setBounds(155,170,35,70);
btBe.addActionListener(frm);
lab.setBounds(20,40,170,40);
frm.setBackground(new Color(220,225,250));
lab.setBackground(new Color(255,255,255));
one.setBounds(20,170,35,30);
one.addActionListener(frm);
two.setBounds(65,170,35,30);
two.addActionListener(frm);
three.setBounds(110,170,35,30);
three.addActionListener(frm);
four.setBounds(20,130,35,30);
four.addActionListener(frm);
five.setBounds(65,130,35,30);
five.addActionListener(frm);
six.setBounds(110,130,35,30);
six.addActionListener(frm);
seven.setBounds(20,90,35,30);
seven.addActionListener(frm);
eight.setBounds(65,90,35,30);
eight.addActionListener(frm);
nine.setBounds(110,90,35,30);
nine.addActionListener(frm);

frm.add(btSum);
frm.add(bt);
frm.add(btCut);
frm.add(zero);
frm.add(btBe);
frm.add(lab);
frm.add(one);
frm.add(two);
frm.add(three);
frm.add(four);
frm.add(five);
frm.add(six);
frm.add(seven);
frm.add(eight);
frm.add(nine);
frm.setVisible(true);

//開始參考網路小算盤
frm.addWindowListener(new WindowAdapter()
{
public void windowClosing(WindowEvent e){System.exit(0);}});
}
public void actionPerformed(ActionEvent e)
{
Button btn=(Button)e.getSource();
if (btn.getLabel()=="+")
{
CheckNumber();
state=0;//0等於加法狀態
}
else if (btn.getLabel()=="-")
{
CheckNumber();
state=1;//1等於減法狀態
}
else if(btn.getLabel()=="C")
{
CheckNumber();
state=2;//2等於C狀態
}
else if(btn.getLabel()=="=")
{
CheckNumber();
state=3;//無運算狀態
}
else
{
chose+=btn.getLabel();
lab.setText(chose);
}
}
//結束參考網路小算盤

/**呼叫檢查是否有輸入數字method*/
public void CheckNumber(){
if (chose==""){
chose=Integer.toString(totle);
}
Number=Integer.parseInt(chose);//將Label的數字存給Number
chose="";
NumberText(state);//呼叫運算method

}


public void NumberText(int a){
switch(a){
case 0:
totle+=Number;
if(totle>999999999)
{
	lab.setText("error");
	totle=0;
	lab.setText(Integer.toString(totle));
}
else
	lab.setText(Integer.toString(totle));
break;
case 1:
totle-=Number;
lab.setText(Integer.toString(totle));
break;
case 2:
totle=0;
lab.setText(Integer.toString(totle));
break;
case 3:
totle=Number;
if(totle>999999999)
{
	lab.setText("error");
	totle=0;
}
else
	lab.setText(Integer.toString(totle));
break;
default:
totle=Number;
if(totle>999999999)
{
	lab.setText("error");
	totle=0;
}
else 
	lab.setText(Integer.toString(totle));
break;
}
}
}