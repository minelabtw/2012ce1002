package ce1002.A7.s101502512;

//參考網路 
//尚未解決的bug  1.連續按等於無法累加 2.溢位問題 3.加和減的按鈕交互按會出錯  4.面板完全是錯的  

import java.awt.*;
import java.awt.event.*;
import javax.swing.JFrame;

public class A7 {
	private static JFrame frm = new JFrame("小算盤");
	private static Panel pn1 = new Panel(new GridLayout(4, 3, 5, 5));
	private static Panel pn2 = new Panel(new GridLayout(3, 1, 5, 5));
	private static Label lab = new Label("0", Label.RIGHT);// 依序為清除、加、減、等於
	private static Button cancel, ad, sub, eql;
	// 0~9的數字
	private static Button number[] = new Button[10];
	private static long num = 0;// 存放結果
	private static byte op = 0;// 代表運算子
	private static long result = 0;

	public static void main(String args[]) {
		frm.setLayout(null);
		frm.setBounds(450, 250, 560, 550);
		frm.setResizable(false);
		frm.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

		Font font1 = new Font("SansSerif", Font.BOLD, 30);
		lab.setBounds(20, 30, 520, 50);
		lab.setFont(font1);
		lab.setBackground(Color.white);

		pn1.setFont(font1);
		pn2.setFont(font1);

		pn1.setBounds(20, 90, 390, 405);
		pn2.setBounds(415, 90, 125, 405);
		// 0~9數字鈕

		for (int i = 9; i >= 0; i--) {
			number[i] = new Button(String.valueOf(i));
			pn1.add(number[i]);
			number[i].addActionListener(new Listener());
		}
		// 清除鈕
		cancel = new Button("C");
		pn1.add(cancel);
		cancel.addActionListener(new Listener());

		// 加鈕
		ad = new Button("+");
		pn2.add(ad);
		ad.addActionListener(new Listener());

		// 減鈕
		sub = new Button("-");
		pn2.add(sub);
		sub.addActionListener(new Listener());

		// 等於鈕
		eql = new Button("=");
		pn2.add(eql);
		eql.addActionListener(new Listener());

		frm.add(lab);
		frm.add(pn1);
		frm.add(pn2);
		frm.setVisible(true);
	}

	public static class Listener implements ActionListener {
		public void actionPerformed(ActionEvent e) {
			Button btn = (Button) e.getSource();
			for (int i = 0; i <= 9; i++) {
				if (btn == number[i]) {
					if (op == 0) { // 第一次數字輸入
						lab.setText(Long.toString(Long.parseLong(lab.getText()
								+ btn.getLabel())));
					}
					if (op == 1 || op == 3) { // 加法的數字輸入
						if (op == 1) {// 輸入個位數
							lab.setText("" + 0);
							lab.setText(Long.toString(Long.parseLong(lab
									.getText() + btn.getLabel())));
						} else {// 輸入之後的數
							lab.setText(Long.toString(Long.parseLong(lab
									.getText() + btn.getLabel())));
						}
						op = 3; // 前往加法運算

					} else if (op == 2 || op == 4) {// 減法的數字輸入
						if (op == 2) {
							lab.setText("" + 0);
							lab.setText(Long.toString(Long.parseLong(lab
									.getText() + btn.getLabel())));
						} else {
							lab.setText(Long.toString(Long.parseLong(lab
									.getText() + btn.getLabel())));
						}
						op = 4; // 前往減法運算
					}
				}
			}

			result = Long.parseLong(lab.getText());
			if (result > 999999999) {
				lab.setText("error.");
			}

			if (btn == cancel) {
				result = 0;// 把儲存的結果歸0
				num = 0;
				op = 0;
				lab.setText("0");
			} else if (btn == ad) {// 加

				if (op == 4) { // 從減跳到加
					save1(ad);
				} else if (op != 1) {// 避免一按加就一直累加
					save(ad);
				}
				op = 1; // 跳到加法的數字輸入

			} else if (btn == sub) {// 減
				if (op == 0) {// 避免先被運算而出現負數
					num = result;
				} else if (op == 3) {// 從加跳到減
					save(ad);
				} else if (op != 2) {
					save1(sub);
				}
				op = 2;// 跳到減法的數字輸入

			} else if (btn == eql) {
				switch (op) {
				case 3:
					num += result;
					if (num > 999999999 || num < -999999999) {
						lab.setText("error.");
					} else
						lab.setText(Long.toString(num));
					break;
				case 4:
					num -= result;
					if (num > 999999999 || num < -999999999) {
						lab.setText("error.");
					} else
						lab.setText(Long.toString(num));

					break;
				default:
				}
				result = 0;
				op = 6;
			}
		}
		// 把第一組數值儲存起來
		private void save(Button x) {
			if (op != 6) {// 避免直接累加
				result = Long.parseLong(lab.getText());
				num += result;
				lab.setText(Long.toString(num));
			}
		}

		private void save1(Button x) {
			if (op != 6) {// 避免直接累減
				result = Long.parseLong(lab.getText());
				num -= result;
				lab.setText(Long.toString(num));
			}
		}
	}
}

