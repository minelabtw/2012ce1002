package ce1002.A7.s101502008;
import java.awt.EventQueue;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.Font;
import javax.swing.JLabel;
import javax.swing.SwingConstants;
import java.awt.Color;
//使用swing builder
public class A7 extends JFrame {
	private JPanel contentPane;
	
	private static JButton btnNewButton_11 = new JButton("=");
	private static JButton btnNewButton = new JButton("7");
	private static JButton btnNewButton_1 = new JButton("8");
	private static JButton btnNewButton_2 = new JButton("9");
	private static JButton btnNewButton_3 = new JButton("4");
	private static JButton btnNewButton_4 = new JButton("5");
	private static JButton btnNewButton_5 = new JButton("6");
	private static JButton btnNewButton_6 = new JButton("1");
	private static JButton btnNewButton_7 = new JButton("2");
	private static JButton btnNewButton_8 = new JButton("3");
	private static JButton btnNewButton_9 = new JButton("0");
	private static JButton btnNewButton_10 = new JButton("C");
	private static JButton btnNewButton_12 = new JButton("+");
	private static JButton btnNewButton_13 = new JButton("-");
	private static JLabel lblNewLabel = new JLabel("0.");
	char mul='0';
	boolean equl=false;
	public int i=0;
	static long tmp=0;
	static long result=0;
	
	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					A7 frame = new A7();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public A7() {
		setTitle("\u5C0F\u7B97\u76E4");
		
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 279, 368);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		lblNewLabel.setBackground(Color.WHITE);
		lblNewLabel.setForeground(Color.BLACK);
		lblNewLabel.setOpaque(true) ;
		lblNewLabel.setFont(new Font("新細明體", Font.PLAIN, 30));
		lblNewLabel.setHorizontalAlignment(SwingConstants.TRAILING);
		lblNewLabel.setBounds(28, 30, 225, 50);
		
		contentPane.add(lblNewLabel);
	
		
		btnNewButton.addActionListener(new ActionListener(){
          	public void actionPerformed(ActionEvent arg0) {
          		if(lblNewLabel.getText()!="error."){
          			if(equl){
	     
	          			if(lblNewLabel.getText()!="error.")
	          				tmp=0;
	          			equl=false;
	          			mul='0';
	          		}
	          		
	          		if(tmp*10+7<=999999999)
	          			tmp=tmp*10+7;
	          		lblNewLabel.setText(tmp+".");
          		}
          	}
        });
		btnNewButton.setFont(new Font("新細明體", Font.PLAIN, 18));
		btnNewButton.setBounds(28, 90, 50, 50);
		contentPane.add(btnNewButton);
		
		
		btnNewButton_1.addActionListener(new ActionListener(){
          	public void actionPerformed(ActionEvent arg0) {
          		if(lblNewLabel.getText()!="error."){
          			if(equl){
	     
	          			if(lblNewLabel.getText()!="error.")
	          				tmp=0;
	          			equl=false;
	          			mul='0';
	          		}
	          		
	          		if(tmp*10+8<=999999999)
	          			tmp=tmp*10+8;
	          		lblNewLabel.setText(tmp+".");
          		}
          	}
        });
		btnNewButton_1.setFont(new Font("新細明體", Font.PLAIN, 18));
		btnNewButton_1.setBounds(83, 90, 50, 50);
		contentPane.add(btnNewButton_1);
		
		
		btnNewButton_2.addActionListener(new ActionListener(){
          	public void actionPerformed(ActionEvent arg0) {
          		if(lblNewLabel.getText()!="error."){
          			if(equl){
	     
	          			if(lblNewLabel.getText()!="error.")
	          				tmp=0;
	          			equl=false;
	          			mul='0';
	          		}
	          		
	          		if(tmp*10+9<=999999999)
	          			tmp=tmp*10+9;
	          		lblNewLabel.setText(tmp+".");
          		}
          	}
        });
		btnNewButton_2.setFont(new Font("新細明體", Font.PLAIN, 18));
		btnNewButton_2.setBounds(143, 90, 50, 50);
		contentPane.add(btnNewButton_2);
		
		
		btnNewButton_3.addActionListener(new ActionListener(){
          	public void actionPerformed(ActionEvent arg0) {
          		if(lblNewLabel.getText()!="error."){
          			if(equl){
	     
	          			if(lblNewLabel.getText()!="error.")
	          				tmp=0;
	          			equl=false;
	          			mul='0';
	          		}
	          		
	          		if(tmp*10+4<=999999999)
	          			tmp=tmp*10+4;
	          		lblNewLabel.setText(tmp+".");
          		}
          	}
        });
		btnNewButton_3.setFont(new Font("新細明體", Font.PLAIN, 18));
		btnNewButton_3.setBounds(28, 150, 50, 50);
		contentPane.add(btnNewButton_3);
		
		
		btnNewButton_4.addActionListener(new ActionListener(){
          	public void actionPerformed(ActionEvent arg0) {
          		if(lblNewLabel.getText()!="error."){
          			if(equl){
	     
	          			if(lblNewLabel.getText()!="error.")
	          				tmp=0;
	          			equl=false;
	          			mul='0';
	          		}
	          		
	          		if(tmp*10+5<=999999999)
	          			tmp=tmp*10+5;
	          		lblNewLabel.setText(tmp+".");
          		}
          	}
        });
		btnNewButton_4.setFont(new Font("新細明體", Font.PLAIN, 18));
		btnNewButton_4.setBounds(83, 150, 50, 50);
		contentPane.add(btnNewButton_4);
		
		
		btnNewButton_5.addActionListener(new ActionListener(){
          	public void actionPerformed(ActionEvent arg0) {
          		if(lblNewLabel.getText()!="error."){
          			if(equl){
	     
	          			if(lblNewLabel.getText()!="error.")
	          				tmp=0;
	          			equl=false;
	          			mul='0';
	          		}
	          		
	          		if(tmp*10+6<=999999999)
	          			tmp=tmp*10+6;
	          		lblNewLabel.setText(tmp+".");
          		}
          	}
        });
		btnNewButton_5.setFont(new Font("新細明體", Font.PLAIN, 18));
		btnNewButton_5.setBounds(143, 150, 50, 50);
		contentPane.add(btnNewButton_5);
		
		
		btnNewButton_6.setFont(new Font("新細明體", Font.PLAIN, 18));
		btnNewButton_6.addActionListener(new ActionListener(){
          	public void actionPerformed(ActionEvent arg0) {
          		if(lblNewLabel.getText()!="error."){
          			if(equl){
	     
	          			if(lblNewLabel.getText()!="error.")
	          				tmp=0;
	          			equl=false;
	          			mul='0';
	          		}
	          		
	          		if(tmp*10+1<=999999999)
	          			tmp=tmp*10+1;
	          		lblNewLabel.setText(tmp+".");
          		}
          	}
        });
		btnNewButton_6.setBounds(28, 210, 50, 50);
		contentPane.add(btnNewButton_6);
		
		
		btnNewButton_7.addActionListener(new ActionListener(){
          	public void actionPerformed(ActionEvent arg0) {
          		if(lblNewLabel.getText()!="error."){
          			if(equl){
	     
	          			if(lblNewLabel.getText()!="error.")
	          				tmp=0;
	          			equl=false;
	          			mul='0';
	          		}
	          		
	          		if(tmp*10+2<=999999999)
	          			tmp=tmp*10+2;
	          		lblNewLabel.setText(tmp+".");
          		}
          	}
        });
		btnNewButton_7.setFont(new Font("新細明體", Font.PLAIN, 18));
		btnNewButton_7.setBounds(83, 210, 50, 50);
		contentPane.add(btnNewButton_7);
		
		
		btnNewButton_8.addActionListener(new ActionListener(){
          	public void actionPerformed(ActionEvent arg0) {
          		if(lblNewLabel.getText()!="error."){
          			if(equl){
	     
	          			if(lblNewLabel.getText()!="error.")
	          				tmp=0;
	          			equl=false;
	          			mul='0';
	          		}
	          		
	          		if(tmp*10+3<=999999999)
	          			tmp=tmp*10+3;
	          		lblNewLabel.setText(tmp+".");
          		}
          	}
        });
		btnNewButton_8.setFont(new Font("新細明體", Font.PLAIN, 18));
		btnNewButton_8.setBounds(143, 210, 50, 50);
		contentPane.add(btnNewButton_8);
		
		
		btnNewButton_9.addActionListener(new ActionListener(){
          	public void actionPerformed(ActionEvent arg0) {
          		if(lblNewLabel.getText()!="error."){
          			if(equl){
	     
	          			if(lblNewLabel.getText()!="error.")
	          				tmp=0;
	          			equl=false;
	          			mul='0';
	          		}
	          		
	          		if(tmp*10<=999999999)
	          			tmp=tmp*10;
	          		lblNewLabel.setText(tmp+".");
          		}
          	}
        });
		btnNewButton_9.setFont(new Font("新細明體", Font.PLAIN, 18));
		btnNewButton_9.setBounds(28, 270, 105, 50);
		contentPane.add(btnNewButton_9);
		
		
		btnNewButton_10.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent arg0) {
          		if(tmp!=0)
          			tmp=0;
          		else
          			result=0;
          		lblNewLabel.setText(tmp+".");
          		mul='0';
          	}
        });
		btnNewButton_10.setFont(new Font("新細明體", Font.PLAIN, 18));
		btnNewButton_10.setBounds(143, 270, 50, 50);
		contentPane.add(btnNewButton_10);
		
		
		
		
		btnNewButton_12.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent arg0) {
          		if(lblNewLabel.getText()!="error."){
	          		if(!equl)
	          			cal();
	          		mul='+';
	          		tmp=0;
	        		equl=false;
          		}
          	}
        });
		btnNewButton_12.setFont(new Font("新細明體", Font.PLAIN, 18));
		btnNewButton_12.setBounds(203, 90, 50, 50);
		contentPane.add(btnNewButton_12);
		
		
		btnNewButton_13.addActionListener(new ActionListener(){
          	public void actionPerformed(ActionEvent arg0) {
          		if(lblNewLabel.getText()!="error."){
          			if(!equl)
	          			cal();
	          		mul='-';
	          		tmp=0;
	        		equl=false;
          		}
          	}
        });
		btnNewButton_13.setFont(new Font("新細明體", Font.PLAIN, 18));
		btnNewButton_13.setBounds(203, 150, 50, 50);
		contentPane.add(btnNewButton_13);
		
		btnNewButton_11.addActionListener(new ActionListener(){
          	public void actionPerformed(ActionEvent arg0) {
          		if(lblNewLabel.getText()!="error.")	
          			cal();
          		if(tmp==0&&mul!='0'){
          			
          			tmp=result;
          			cal();
          		}
          		equl=true;
          	}
        });
		btnNewButton_11.setFont(new Font("新細明體", Font.PLAIN, 18));
		btnNewButton_11.setBounds(203, 210, 50, 110);
		contentPane.add(btnNewButton_11);
		
	}
	public void cal(){//計算的method
		if(mul=='+')
			result=result+tmp;
		else if(mul=='-')
			result=result-tmp;
		else if(mul=='0')
			result=tmp;
		if(result>999999999||result<-999999999){
			lblNewLabel.setText("error.");
			tmp=result;
		}
		else
			lblNewLabel.setText(result+".");
}
	}
