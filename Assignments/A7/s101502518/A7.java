package ce1002.A7.s101502518;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.*;

public class A7 extends JFrame implements ActionListener{
	
	JButton jButt1,jButt2,jButt3,jButt4,jButt5,jButt6,jButt7,jButt8,jButt9,jButt0;
	JButton jButtEqu,jButtPlus,jButtMin,jButtC;
	JTextField jTextField1;
	JPanel jPanel1;
	int i=0;
	long result=0;
	long[] sum=new long[3];
	boolean a = true;//上一步是否按"="
	boolean b = true;//上一步是"+"或"-"
		
    public A7() {
    	jButt1 = new JButton();
        jButt2 = new JButton();
        jButt3 = new JButton();
        jButt4 = new JButton();
        jButt5 = new JButton();
        jButt6 = new JButton();
        jButt7 = new JButton();
        jButt8 = new JButton();
        jButt9 = new JButton();
        jButtEqu = new JButton();
        jButt0 = new JButton();
        jButtC = new JButton();
        jButtPlus = new JButton();    
        jButtMin = new JButton();
        jPanel1 = new JPanel();
        jTextField1 = new JTextField();
        
        jButt1.setText("1");
        jButt1.addActionListener(this);
        jButt2.setText("2");
        jButt2.addActionListener(this);
        jButt3.setText("3");
        jButt3.addActionListener(this);
        jButt4.setText("4");
        jButt4.addActionListener(this);
        jButt5.setText("5");
        jButt5.addActionListener(this);
        jButt6.setText("6");
        jButt6.addActionListener(this);
        jButt7.setText("7");
        jButt7.addActionListener(this);
        jButt8.setText("8");
        jButt8.addActionListener(this);
        jButt9.setText("9");
        jButt9.addActionListener(this);
        jButt0.setText("0");
        jButt0.addActionListener(this);
        jButtPlus.setText("+");
        jButtPlus.addActionListener(this);
        jButtMin.setText("-");
        jButtMin.addActionListener(this);
        jButtEqu.setText("=");
        jButtEqu.addActionListener(this);
        jButtC.setText("c");
        jButtC.addActionListener(this);
        jTextField1.setText("0.");
        jTextField1.setHorizontalAlignment(JTextField.RIGHT);
           
        setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
////////////////////////////////排版//////////////////////////////////////////////////////////////////////    
        GroupLayout jPanel1Layout = new GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(jButt7, GroupLayout.PREFERRED_SIZE, 45, GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jButt8, GroupLayout.PREFERRED_SIZE, 45, GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jButt9, GroupLayout.PREFERRED_SIZE, 45, GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED, 8, Short.MAX_VALUE)
                        .addComponent(jButtPlus, GroupLayout.PREFERRED_SIZE, 45, GroupLayout.PREFERRED_SIZE))
                    .addComponent(jTextField1)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(jButt4, GroupLayout.PREFERRED_SIZE, 45, GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jButt5, GroupLayout.PREFERRED_SIZE, 45, GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jButt6, GroupLayout.PREFERRED_SIZE, 45, GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(jButtMin, GroupLayout.PREFERRED_SIZE, 45, GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGroup(jPanel1Layout.createParallelGroup(GroupLayout.Alignment.TRAILING, false)
                            .addComponent(jButt0, GroupLayout.Alignment.LEADING, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addGroup(GroupLayout.Alignment.LEADING, jPanel1Layout.createSequentialGroup()
                                .addComponent(jButt1, GroupLayout.PREFERRED_SIZE, 45, GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(jButt2, GroupLayout.PREFERRED_SIZE, 45, GroupLayout.PREFERRED_SIZE)))
                        .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel1Layout.createParallelGroup(GroupLayout.Alignment.LEADING)
                            .addComponent(jButt3, GroupLayout.PREFERRED_SIZE, 45, GroupLayout.PREFERRED_SIZE)
                            .addComponent(jButtC, GroupLayout.PREFERRED_SIZE, 45, GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(jButtEqu, GroupLayout.PREFERRED_SIZE, 45, GroupLayout.PREFERRED_SIZE)))
                .addContainerGap())
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jTextField1, GroupLayout.PREFERRED_SIZE, 50, GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addGroup(jPanel1Layout.createParallelGroup(GroupLayout.Alignment.BASELINE)
                    .addComponent(jButt7, GroupLayout.PREFERRED_SIZE, 40, GroupLayout.PREFERRED_SIZE)
                    .addComponent(jButt8, GroupLayout.PREFERRED_SIZE, 40, GroupLayout.PREFERRED_SIZE)
                    .addComponent(jButt9, GroupLayout.PREFERRED_SIZE, 40, GroupLayout.PREFERRED_SIZE)
                    .addComponent(jButtPlus, GroupLayout.PREFERRED_SIZE, 40, GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(GroupLayout.Alignment.BASELINE)
                    .addComponent(jButt4, GroupLayout.PREFERRED_SIZE, 40, GroupLayout.PREFERRED_SIZE)
                    .addComponent(jButt5, GroupLayout.PREFERRED_SIZE, 40, GroupLayout.PREFERRED_SIZE)
                    .addComponent(jButt6, GroupLayout.PREFERRED_SIZE, 40, GroupLayout.PREFERRED_SIZE)
                    .addComponent(jButtMin, GroupLayout.PREFERRED_SIZE, 40, GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(GroupLayout.Alignment.LEADING, false)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGroup(jPanel1Layout.createParallelGroup(GroupLayout.Alignment.BASELINE)
                            .addComponent(jButt1, GroupLayout.PREFERRED_SIZE, 40, GroupLayout.PREFERRED_SIZE)
                            .addComponent(jButt2, GroupLayout.PREFERRED_SIZE, 40, GroupLayout.PREFERRED_SIZE)
                            .addComponent(jButt3, GroupLayout.PREFERRED_SIZE, 40, GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel1Layout.createParallelGroup(GroupLayout.Alignment.BASELINE)
                            .addComponent(jButt0, GroupLayout.PREFERRED_SIZE, 40, GroupLayout.PREFERRED_SIZE)
                            .addComponent(jButtC, GroupLayout.PREFERRED_SIZE, 40, GroupLayout.PREFERRED_SIZE)))
                    .addComponent(jButtEqu, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap(GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                
        );

        GroupLayout layout = new GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        pack();
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    }                     
 
    public void actionPerformed(ActionEvent e) throws NumberFormatException,ArithmeticException{
    	
    	if(e.getSource()==jButt1)
    	{		
    		if(!a)
    		{
    			sum[0]=0;
    			i=0;
    			a=true;
    		}	
    		sum[i] = sum[i]*10+1;
    		result = sum[i];
    	}    			
    	if(e.getSource()==jButt2)
    	{
    		if(!a)
    		{
    			sum[0]=0;
    			i=0;
    			a=true;
    		}
    		sum[i] = sum[i]*10+2;
    		result = sum[i];   		
    	} 
    	if(e.getSource()==jButt3)
    	{
    		if(!a)
    		{
    			sum[0]=0;
    			i=0;
    			a=true;
    		}
    		sum[i] = sum[i]*10+3;
    		result = sum[i];	
    	} 
    	if(e.getSource()==jButt4)
    	{
    		if(!a)
    		{
    			sum[0]=0;
    			i=0;
    			a=true;
    		}
    		sum[i] = sum[i]*10+4;
    		result = sum[i];    		
    	} 
    	if(e.getSource()==jButt5)
    	{
    		if(!a)
    		{
    			sum[0]=0;
    			i=0;
    			a=true;
    		}
    		sum[i] = sum[i]*10+5;
    		result = sum[i];
    	} 
    	if(e.getSource()==jButt6)
    	{
    		if(!a)
    		{
    			sum[0]=0;
    			i=0;
    			a=true;
    		}    	
    		sum[i] = sum[i]*10+6;
    		result = sum[i];
    	} 
    	if(e.getSource()==jButt7)
    	{
    		if(!a)
    		{
    			sum[0]=0;
    			i=0;
    			a=true;
    		}    		
    		sum[i] = sum[i]*10+7;
    		result = sum[i];
    	} 
    	if(e.getSource()==jButt8)
    	{
    		if(!a)
    		{
    			sum[0]=0;
    			i=0;
    			a=true;
    		}   		
    		sum[i] = sum[i]*10+8;
    		result = sum[i];
    	} 
    	if(e.getSource()==jButt9)
    	{
    		if(!a)
    		{
    			sum[0]=0;
    			i=0;
    			a=true;
    		}   		
    		sum[i] = sum[i]*10+9;
    		result = sum[i];
    	}
    	if(e.getSource()==jButt0)
    	{
    		if(!a)
    		{
    			sum[0]=0;
    			i=0;
    			a=true;
    		}   		
    		sum[i] = sum[i]*10;
    		result = sum[i];
    	}
    	if(e.getSource()==jButtC)
    	{
    		sum[0] = 0;
    		sum[1] = 0;
    		result = sum[0];		
    		i=0;
    	}
    	if(e.getSource()==jButtPlus)//按"+"
    	{
    		if(b)
    			sum[0]+=sum[1];	
    		else
    			sum[0]-=sum[1];

    		if(i==1)
    		{
    			result = sum[0];
    			sum[1] = 0;
    			i=0;
    		}
    		i++;
    		a=true;
    		b=true;
    	}
    	if(e.getSource()==jButtMin)//按"-"
    	{
    		if(b)
    			sum[0]+=sum[1];
    		else
    			sum[0]-=sum[1];
    		
    		if(i==1)
    		{	
    			result = sum[0];
    			sum[1] = 0;
    			i=0;
    		}
    		i++;
    		a=true;
    		b=false;
    	}
    	if(e.getSource()==jButtEqu)//按"="
    	{
    		if(b)//上一次是按"+"
    		{
    			if(a)
    			{
    				sum[0]=sum[1]+sum[0];
    				sum[2]=sum[1];
            		sum[1]=0;
            		result = sum[0];
                	
    			}
    			else//連續按"="會遞增
    			{
    				sum[0]=sum[2]+sum[0];
    				result = sum[0];
    			}			
    		}
    		else//上一次是按撿"-"
    		{
    			if(a)
    			{
    				sum[0]=sum[0]-sum[1];
            		sum[2]=sum[1];
            		sum[1]=0;
            		result = sum[0];
    			}
    			else//連續按"="會遞減
    			{
    				sum[0]=sum[0]-sum[2];
    				result = sum[0];   				
    			}
    		}
    		a = false;    		
    	}
    	
    	if(sum[0]>999999999||sum[0]<-99999999||sum[1]>999999999||sum[1]<-99999999)//溢位
    	{
    		jTextField1.setText("error.");
    		sum[i] = 0;
    	}
    	else//無溢位則輸出結果
    		jTextField1.setText(result+".");
    	
    }
       
    public static void main(String args[]) {

        try {
            for (UIManager.LookAndFeelInfo info : UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(A7.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(A7.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(A7.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(A7.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new A7().setVisible(true);
            }
        });
    }
    
    
                   
}
