package ce1002.A7.s101502527;

import javax.swing.*;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class A7 extends JFrame implements ActionListener {
	
	JLabel lab = new JLabel("");//label
	JButton jb7 = new JButton("7");//按鈕
	JButton jb8 = new JButton("8");
	JButton jb9 = new JButton("9");
	JButton jbn = new JButton("-");
	JButton jb4 = new JButton("4");
	JButton jb5 = new JButton("5");
	JButton jb6 = new JButton("6");	
	JButton jbp = new JButton("+");	
	JButton jb1 = new JButton("1");
	JButton jb2 = new JButton("2");
	JButton jb3 = new JButton("3");
	JButton jbe = new JButton("=");
	JButton jb0 = new JButton("0");
	JButton jbc = new JButton("c");
	
	int num=0;  //儲存結果
	boolean cal=true;//儲存上一運算子
	boolean fzero = true;//是否要規0
	
	public static void main(String[] args){//gui
	JFrame a7 = new A7();
	a7.setLayout(null);
	a7.setTitle("小算盤");
	a7.setSize(291,438);
	a7.setLocationRelativeTo(null);
	a7.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	a7.setVisible(true);
	
	}
	
	public A7(){

		
		
		lab.setFont(new Font("font",Font.PLAIN,40));//設定位置
		lab.setOpaque(true);
		lab.setHorizontalAlignment(JLabel.RIGHT);
		lab.setBackground(Color.white);
		lab.setMinimumSize(new Dimension(245, 100));
		lab.setBounds(15,15,245,100);
		add(lab);
		
		jb7.setBounds(15,130,50,50);
		jb7.addActionListener(this);
		add(jb7);
		
		jb8.setBounds(80,130,50,50);
		jb8.addActionListener(this);
		add(jb8);
		
		jb9.setBounds(145,130,50,50);
		jb9.addActionListener(this);
		add(jb9);
		
		jbn.setBounds(210,130,50,50);
		jbn.addActionListener(this);
		add(jbn);
		
		jb4.setBounds(15,195,50,50);
		jb4.addActionListener(this);
		add(jb4);
		
		jb5.setBounds(80,195,50,50);
		jb5.addActionListener(this);
		add(jb5);
		
		jb6.setBounds(145,195,50,50);
		jb6.addActionListener(this);
		add(jb6);
		
		jbp.setBounds(210,195,50,50);
		jbp.addActionListener(this);
		add(jbp);
		
		jb1.setBounds(15,260,50,50);
		jb1.addActionListener(this);
		add(jb1);
		
		jb2.setBounds(80,260,50,50);
		jb2.addActionListener(this);
		add(jb2);
		
		jb3.setBounds(145,260,50,50);
		jb3.addActionListener(this);
		add(jb3);
		
		jbe.setBounds(210,260,50,115);
		jbe.addActionListener(this);
		add(jbe);
		
		jb0.setBounds(15,325,115,50);
		jb0.addActionListener(this);
		add(jb0);
		
		jbc.setBounds(145,325,50,50);
		jbc.addActionListener(this);
		add(jbc);
        
		szero();//規0函式
		
	}
	
	public void error(){//判斷溢位
		if(lab.getText().length()>9){
			lab.setText("error");
			fzero=true;
		}
	}
	
	public void szero(){//規0函式
		lab.setText("0");
		fzero=true;
	}
 
	public void actionPerformed(ActionEvent e) {
		if(e.getSource()==jb1){
			
			if(fzero){//如果需要從從頭打
				lab.setText(jb1.getText());
				fzero=false;
			}
			else{
				lab.setText(lab.getText()+jb1.getText());
			}
			error();
		}
		if(e.getSource()==jb2){//如果需要從從頭打
			if(fzero){
				lab.setText(jb2.getText());
				fzero=false;
			}
			else{
				lab.setText(lab.getText()+jb2.getText());
			}
			error();
		}
		if(e.getSource()==jb3){//如果需要從從頭打
			if(fzero){
				lab.setText(jb3.getText());
				fzero=false;
			}
			else{
				lab.setText(lab.getText()+jb3.getText());
			}
			error();
		}
		if(e.getSource()==jb4){//如果需要從從頭打
			if(fzero){
				lab.setText(jb4.getText());
				fzero=false;
			}
			else{
				lab.setText(lab.getText()+jb4.getText());
			}
			error();
		}//如果需要從從頭打
		if(e.getSource()==jb5){
			if(fzero){
				lab.setText(jb5.getText());
				fzero=false;
			}
			else{
				lab.setText(lab.getText()+jb5.getText());
			}
			error();
		}
		if(e.getSource()==jb6){//如果需要從從頭打
			if(fzero){
				lab.setText(jb6.getText());
				fzero=false;
			}
			else{
				lab.setText(lab.getText()+jb6.getText());
			}
			error();
		}
		if(e.getSource()==jb7){//如果需要從從頭打
			if(fzero){
				lab.setText(jb7.getText());
				fzero=false;
			}
			else{
				lab.setText(lab.getText()+jb7.getText());
			}
			error();
		}
		if(e.getSource()==jb8){//如果需要從從頭打
			if(fzero){
				lab.setText(jb8.getText());
				fzero=false;
			}
			else{
				lab.setText(lab.getText()+jb8.getText());
			}
			error();
		}
		if(e.getSource()==jb9){//如果需要從從頭打
			if(fzero){
				lab.setText(jb9.getText());
				fzero=false;
			}
			else{
				lab.setText(lab.getText()+jb9.getText());
			}
			error();
		}
		if(e.getSource()==jb0){//如果需要從從頭打
			if(lab.getText()=="0");//不能連續打0
			else if(fzero){
				lab.setText(jb0.getText());
				fzero=false;
			}
			else{
				lab.setText(lab.getText()+jb0.getText());
			}
			error();
		}
		if(e.getSource()==jbc){//案c規0
			szero();
		}
		if(e.getSource()==jbp){//每案一個運算子 計算上一個運算式
			if(cal){
				num+=Integer.valueOf(lab.getText());
				lab.setText(Integer.toString(num));
			}
			else{
				num-=Integer.valueOf(lab.getText());
				lab.setText(Integer.toString(num));
			}
			cal=true;//儲存加法
			fzero=true;
			error();
		}
		if(e.getSource()==jbn){//每案一個運算子 計算上一個運算式
			if(cal){
				num+=Integer.valueOf(lab.getText());
				lab.setText(Integer.toString(num));
			}
			else{
				num-=Integer.valueOf(lab.getText());
				lab.setText(Integer.toString(num));
			}
			cal=false;//儲存減法
			fzero=true;
			error();
		}
		if(e.getSource()==jbe){//等於
			if(cal){
				num+=Integer.valueOf(lab.getText());
				lab.setText(Integer.toString(num));
			}
			else{
				num-=Integer.valueOf(lab.getText());
				lab.setText(Integer.toString(num));
			}
			fzero=true;
			num=0;//數字規0;
			error();
		}
	}
}

