package ce1002.A7.s101502002;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.*;
public class A7 extends JFrame implements ActionListener {
	JTextField line = new JTextField(9); //最上方那條字
	JButton zero = new JButton("0"); //數字扭還有加、減、等於、清除
	JButton one = new JButton("1");
	JButton two = new JButton("2");
	JButton three = new JButton("3");
	JButton four = new JButton("4");
	JButton five = new JButton("5");
	JButton six = new JButton ("6");
	JButton seven = new JButton("7");
	JButton eight = new JButton("8");
	JButton nine = new JButton("9");
	JButton add = new JButton("+");
	JButton cut = new JButton("-");
	JButton equal = new JButton("=");
	JButton cancel = new JButton("C");
	int num=0,num2=0,total=0,check=2,again=0;
	//num是輸入的是字，num2是每次等於後留下來備用的值，total是總合，check是檢查是否為第一次運算，again是來看會不會用到num2
	public A7(){
		setLayout(null);
		line.setBounds(20,10, 255, 80);
		line.setText("0.");
		line.setHorizontalAlignment(JTextField.RIGHT); //文字靠右
		add(line); 
		
		seven.setBounds(20,95, 60, 40);
		seven.addActionListener(this); //讓按鈕做事
		add(seven);
		
		eight.setBounds(85, 95, 60, 40);
		eight.addActionListener(this);
		add(eight);
		
		nine .setBounds(150,95,60,40);
		nine.addActionListener(this);
		add(nine);
		
		cut.setBounds(215,95,60,40);
		cut.addActionListener(this);
		add(cut);
		
		four.setBounds(20,140,60,40);
		four.addActionListener(this);
		add(four);
		
		five.setBounds(85,140,60,40);
		five.addActionListener(this);
		add(five);
		
		six.setBounds(150,140,60,40);
		six.addActionListener(this);
		add(six);
		
		add.setBounds(215,140,60,40);
		add.addActionListener(this);
		add(add);
		
		one.setBounds(20,185,60,40);
		one.addActionListener(this);
		add(one);
		
		two.setBounds(85,185,60,40);
		two.addActionListener(this);
		add(two);
		
		three.setBounds(150,185,60,40);
		three.addActionListener(this);
		add(three);
		
		equal.setBounds(215,185,60,85);
		equal.addActionListener(this);
		add(equal);
		
		zero.setBounds(20,230,125,40);
		zero.addActionListener(this);
		add(zero);
		
		cancel.setBounds(150,230,60,40);
		cancel.addActionListener(this);
		add(cancel);
	
	}
	public static void main(String[]args){
		A7 frame = new A7();
		frame.setTitle("小算盤");
		frame.setSize(300,350); //視窗大小
		frame.setLocationRelativeTo(null); //顯示在正中間
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE); //案叉叉關閉
		frame.setVisible(true); //可被看見
	}
	@Override
	public void actionPerformed(ActionEvent e) { //按下按鈕後的動作
		if(e.getSource()==one){ //數字就是原本的是字*10在加上
			num=num*10+1;
			line.setText(""+num);
		}// TODO Auto-generated method stub
		else if(e.getSource()==two){
			num=num*10+2;
			line.setText(""+num);
		}
		else if(e.getSource()==three){
			num=num*10+3;
			line.setText(""+num);
		}
		else if(e.getSource()==four){
			num=num*10+4;
			line.setText(""+num);
		}
		else if(e.getSource()==five){
			num=num*10+5;
			line.setText(""+num);
		}
		else if(e.getSource()==six){
			num=num*10+6;
			line.setText(""+num);
		}
		else if(e.getSource()==seven){
			num=num*10+7;
			line.setText(""+num);
		}
		else if(e.getSource()==eight){
			num=num*10+8;
			line.setText(""+num);
		}
		else if(e.getSource()==nine){
			num=num*10+9;
			line.setText(""+num);
		}
		else if(e.getSource()==zero){
			num=num*10;
			line.setText(""+num);
			again=1;
		}
		if(e.getSource()==add){ //加號
			if(num2!=0 && check==2 && num==0 && again==0){
				//使用到等於後的值
				total=num2;
			}
			else
				total=total+num;
			num2=0; //回歸原樣
			line.setText(""+total);
			num=0;
			check=0; //表示是加
		}
		else if(e.getSource()==cut){ //減號
			if(num2!=0 && check==2 && num==0 && again==0){ //等於後的繼續運算
				total=num2;
			}
			else if(check==2) //第一次運算
				total=num;
			else
				total=total-num;
			num2=0;
			line.setText(""+total);
			num=0;
			check=1; //減號
		}
		else if(e.getSource()==equal){
			if(check==1) //減的
				total=total-num;
			else //加
				total=total+num;
			line.setText(""+total);
			num=0;
			num2=total;//讓等於後的結果可以繼續運算
			total=0;
			check=2;
			again=0;//回到第一次運算
			
		}
		else if(e.getSource()==cancel){
			num=0; //一切回歸最初模樣
			total=0;
			num2=0;
			check=2;
			line.setText("0.");
			again=0;
		}
		if(num>999999999||total>999999999){
			num=0; //回歸原始
			total=0;
			num2=0;
			check=2;
			line.setText("error");
			again=0;
		}
	}
}