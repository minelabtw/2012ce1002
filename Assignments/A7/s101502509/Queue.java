package ce1002.A7.s101502509;

import java.util.Vector;

public class Queue {
	private int result = 0;
	private int temp = 0;
	private int operator = 0, preOperator = 0;
	private boolean firstItem = true;
	private static Vector<Integer> queueData = new Vector<Integer>();
	final int PLUS = 256;
	final int SUBTRACT= 257;
	final int EQUAL= 258;
	
	public void offer(int num) {
		queueData.add(num);
	}
	
	public int poll() {		//get the first element and remove it from queueData
		int tmp = queueData.get(0);
		queueData.remove(0);
		return tmp;
	}
	
	public void calculate() { 
		while(!queueData.isEmpty()) {
			int peek = poll();
			preOperator = operator;		//retain the preOperator prepare to calculate
			if(peek == PLUS) {		//record the latest operator
				operator = PLUS;
			} 
		
			if(peek == SUBTRACT) {
				operator = SUBTRACT;
			}
			
			if(peek == EQUAL) {		
				break;
			}
			
			if(peek != PLUS && peek != SUBTRACT && peek != EQUAL) {		//record the number after preOperator into temp
				temp = temp * 10 + peek;
				//System.out.println("temp"+temp);  //check the temp
			}
		}
		
		if(firstItem) {	//If it is first Item, result = temp 
			firstItem = false;
			result = temp;
			temp = 0;
		} else if(preOperator == PLUS) {	//If preOperator is '+', add result and temp
			result += temp;
			temp = 0;
		} else if(preOperator == SUBTRACT) {	//If preOperator is '-', subtract temp from result
			result -= temp;
			temp = 0;
		}
		//System.out.println("result"+result); //check the result
	}
	
	public int getResult() {
		return result;
	}
	
	public String show() {	//show each element of queueData
		String tmp="";
		for(int i = 0; i < queueData.size(); i++) {
			tmp += (String)(queueData.get(i) + "");
		}
		return tmp;
	}
	public void clearQueue() {		//reset the result, temp, operator, preOperator, firstItem, queueData 
		result = 0;
		temp = 0;
		operator = 0;
		preOperator = 0;
		firstItem = true;
		for(int i = 0; i < queueData.size(); i++) {
			queueData.remove(i);
		}
	}
}
