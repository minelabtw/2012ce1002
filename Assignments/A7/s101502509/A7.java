package ce1002.A7.s101502509;

import ce1002.A7.s101502509.Queue;

import java.awt.Color;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JTextField;

public class A7 extends JFrame{
	
	Queue queue = new Queue();
	
	public JTextField content = new JTextField(10);
	public JButton zero = new JButton("0");
	public JButton one = new JButton("1");
	public JButton two = new JButton("2");
	public JButton three = new JButton("3");
	public JButton four = new JButton("4");
	public JButton five = new JButton("5");
	public JButton six = new JButton("6");
	public JButton seven = new JButton("7");
	public JButton eight = new JButton("8");
	public JButton nine = new JButton("9");
	public JButton plusIcon= new JButton("+");
	public JButton subIcon = new JButton("-");		//substractIcon
	public JButton cancelIcon = new JButton("C");
	public JButton equalIcon = new JButton("=");
	
	public A7() {
		setUI();
	}
	
	public void setUI() {
		setLayout(new GridBagLayout());
		GridBagConstraints c = new GridBagConstraints();
		
		// set output text place
		c.gridx = 0; 	//set start point x
		c.gridy = 0;	//set start point y
		c.gridwidth = 20;	//set the width
		c.gridheight = 20;	//set the height 
		c.fill = GridBagConstraints.BOTH;	//fill all the area of its scope
		c.anchor =  GridBagConstraints.EAST;
		c.insets = new Insets(3, 3, 3, 3);	//set the blank area around it
		content.setHorizontalAlignment(JTextField.RIGHT); //set output text from right
		content.setBackground(Color.WHITE);
		add(content, c);
		
		//set seven icon
		c.gridx = 0;
		c.gridy = 20;
		c.gridwidth = 5;
		c.gridheight = 5;
		c.fill = GridBagConstraints.BOTH;
		c.anchor =  GridBagConstraints.CENTER;
		seven.addActionListener(new sevenActionListener());
		add(seven, c);
		
		//set eight icon
		c.gridx = 5;
		c.gridy = 20;
		c.fill = GridBagConstraints.BOTH;
		eight.addActionListener(new eightActionListener());
		add(eight, c);
		
		//set nine icon
		c.gridx = 10;
		c.gridy = 20;
		c.fill = GridBagConstraints.BOTH;
		nine.addActionListener(new nineActionListener());
		add(nine, c);
		
		//set subtract icon
		c.gridx = 15;
		c.gridy = 20;
		c.fill = GridBagConstraints.BOTH;
		subIcon.addActionListener(new subActionListener());
		add(subIcon, c);
		
		//set four icon
		c.gridx = 0;
		c.gridy = 25;
		c.fill = GridBagConstraints.BOTH;
		four.addActionListener(new fourActionListener());
		add(four, c);
		
		//set five icon
		c.gridx = 5;
		c.gridy = 25;
		c.fill = GridBagConstraints.BOTH;
		five.addActionListener(new fiveActionListener());
		add(five, c);
		
		//set six icon
		c.gridx = 10;
		c.gridy = 25;
		c.fill = GridBagConstraints.BOTH;
		six.addActionListener(new sixActionListener());
		add(six, c);
		
		//set plus icon
		c.gridx = 15;
		c.gridy = 25;
		c.fill = GridBagConstraints.BOTH;
		plusIcon.addActionListener(new plusActionListener());
		add(plusIcon, c);
		
		//set one icon
		c.gridx = 0;
		c.gridy = 30;
		c.fill = GridBagConstraints.BOTH;
		one.addActionListener(new oneActionListener());
		add(one, c);
		
		//set two icon
		c.gridx = 5;
		c.gridy = 30;
		c.fill = GridBagConstraints.BOTH;
		two.addActionListener(new twoActionListener());
		add(two, c);
		
		//set three icon
		c.gridx = 10;
		c.gridy = 30;
		c.fill = GridBagConstraints.BOTH;
		three.addActionListener(new threeActionListener());
		add(three, c);
		
		//set equal icon
		c.gridx = 15;
		c.gridy = 30;
		c.gridheight = 10;
		c.fill = GridBagConstraints.BOTH;
		equalIcon.addActionListener(new equalActionListener());
		add(equalIcon, c);
		
		//set zero icon
		c.gridx = 0;
		c.gridy = 35;
		c.gridwidth = 10;
		c.gridheight = 5;
		c.fill = GridBagConstraints.BOTH;
		zero.addActionListener(new zeroActionListener());
		add(zero, c);
		
		//set cancel icon
		c.gridx = 10;
		c.gridy = 35;
		c.gridwidth = 5;
		c.fill = GridBagConstraints.BOTH;
		cancelIcon.addActionListener(new cancelActionListener());
		add(cancelIcon, c);
		
		setTitle("小算盤");
		setSize(300, 200);
		setLocationRelativeTo(null);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setVisible(true);
	}
	
	public class sevenActionListener implements ActionListener {	//click seven action
		@Override
		public void actionPerformed(ActionEvent arg0) {
			numButtonAction(seven);
		}
	}
	
	public class eightActionListener implements ActionListener {	//click eight action
		@Override
		public void actionPerformed(ActionEvent arg0) {
			numButtonAction(eight);
		}
	}
	
	public class nineActionListener implements ActionListener {	//click nine action
		@Override
		public void actionPerformed(ActionEvent arg0) {
			numButtonAction(nine);
		}
	}
	
	public class fourActionListener implements ActionListener {	//click four action
		@Override
		public void actionPerformed(ActionEvent arg0) {
			numButtonAction(four);
		}
	}
	
	public class fiveActionListener implements ActionListener {	//click five action
		@Override
		public void actionPerformed(ActionEvent arg0) {
			numButtonAction(five);
		}
	}
	
	public class sixActionListener implements ActionListener {		//click six action
		@Override
		public void actionPerformed(ActionEvent arg0) {
			numButtonAction(six);
		}
	}
	
	public class oneActionListener implements ActionListener {		//click one action
		@Override
		public void actionPerformed(ActionEvent arg0) {
			numButtonAction(one);
		}
	}
	
	public class twoActionListener implements ActionListener {		//click two action
		@Override
		public void actionPerformed(ActionEvent arg0) {
			numButtonAction(two);
		}
	}
	
	public class threeActionListener implements ActionListener {	//click three action
		@Override
		public void actionPerformed(ActionEvent arg0) {
			numButtonAction(three);
		}
	}
	
	public class zeroActionListener implements ActionListener {	//click zero action
		@Override
		public void actionPerformed(ActionEvent arg0) {
			numButtonAction(zero);
		}
	}
	
	public class subActionListener implements ActionListener {		//click subtract action
		@Override
		public void actionPerformed(ActionEvent arg0) {
			queue.offer(queue.SUBTRACT);
			queue.calculate();
			overflowDetect();
		}
	}
	
	public class plusActionListener implements ActionListener {	//click plus action
		@Override
		public void actionPerformed(ActionEvent arg0) {
			queue.offer(queue.PLUS);
			queue.calculate();
			overflowDetect();
		}
	}
	
	public class equalActionListener implements ActionListener {	//click equal action
		@Override
		public void actionPerformed(ActionEvent arg0) {
			queue.offer(queue.EQUAL);
			queue.calculate();
			overflowDetect();
		}
	}
	
	public class cancelActionListener implements ActionListener {	//click cancel action
		@Override
		public void actionPerformed(ActionEvent arg0) {
			content.setText("0.");
			queue.clearQueue();
		} 
	}
	
	public void numButtonAction(JButton button) {		//click number button action
		queue.offer(new Integer(button.getText()));		//push the text of button into queue
		content.setText(queue.show() + ".");			//show the text in the output text field
	}
	
	public void overflowDetect() {		//detect the result if overflow
		if(queue.getResult() > 999999999) {
			content.setText("error.");
			queue.clearQueue();
		} else {
			content.setText((String)(queue.getResult() +"."));
		}
	}
	
	public static void main(String[] args) {
		new A7();
	}

}
