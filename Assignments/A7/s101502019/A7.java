//參考http://www.javaworld.com.tw/jute/post/view?bid=29&id=87099&sty=3
//button排版是同學教的
package ce1002.A7.s101502019;

import java.awt.event.*;
import javax.swing.*;

public class A7 extends JFrame implements ActionListener{
    
    JTextField num;
    String numberstring="";
    double number;
    String error = "error.";
    char operator; 
    char buttonname[]={'7','8','9',
                       '4','5','6',
                       '1','2','3','-',
                       '0','+','c','='};                       
    JButton button[]=new JButton[14];
    
    public A7(){        
    	super("計算機");
		setLayout(null);
		num = new JTextField("0.");
		num.setHorizontalAlignment(JTextField.RIGHT);
//////////////////////  1 ~ 9 ////////////////////////////////
		button[0] = new JButton(buttonname[0] + "");
		button[1] = new JButton(buttonname[1] + "");
		button[2] = new JButton(buttonname[2] + "");
		button[3] = new JButton(buttonname[3] + "");
		button[4] = new JButton(buttonname[4] + "");
		button[5] = new JButton(buttonname[5] + "");
		button[6] = new JButton(buttonname[6] + "");
		button[7] = new JButton(buttonname[7] + "");
		button[8] = new JButton(buttonname[8] + "");
//////////////////////////////////////////////////////////////
		
		button[9] = new JButton(buttonname[9] + "");  // -
		button[10] = new JButton(buttonname[10] + "");// 0
		button[11] = new JButton(buttonname[11] + "");// +
		button[12] = new JButton(buttonname[12] + "");// c
		button[13] = new JButton(buttonname[13] + "");// =

//////////////////////1 ~ 9 ////////////////////////////////		
		button[0].setBounds(10, 60, 60, 50);
		button[1].setBounds(80, 60, 60, 50);
		button[2].setBounds(150, 60, 60, 50);
		button[3].setBounds(10, 120, 60, 50);
		button[4].setBounds(80, 120, 60, 50);
		button[5].setBounds(150, 120, 60, 50);
		button[6].setBounds(10, 180, 60, 50);
		button[7].setBounds(80, 180, 60, 50);
		button[8].setBounds(150, 180, 60, 50);
//////////////////////////////////////////////////////////////		
		button[9].setBounds(220, 60, 60, 50);
		button[10].setBounds(10, 240, 130, 50);
		button[11].setBounds(220, 120, 60, 50);
		button[12].setBounds(150, 240, 60, 50);
		button[13].setBounds(220, 180, 60, 110);
		
		button[0].addActionListener(this);
		button[1].addActionListener(this);
		button[2].addActionListener(this);
		button[3].addActionListener(this);
		button[4].addActionListener(this);
		button[5].addActionListener(this);
		button[6].addActionListener(this);
		button[7].addActionListener(this);
		button[8].addActionListener(this);
		button[9].addActionListener(this);
		button[10].addActionListener(this);
		button[11].addActionListener(this);
		button[12].addActionListener(this);
		button[13].addActionListener(this);
//////////////////////1 ~ 9 ////////////////////////////////		
		add(button[0]);
		add(button[1]);
		add(button[2]);
		add(button[3]);
		add(button[4]);
		add(button[5]);
		add(button[6]);
		add(button[7]);
		add(button[8]);
//////////////////////////////////////////////////////////////		
		add(button[9]);
		add(button[10]);
		add(button[11]);
		add(button[12]);
		add(button[13]);
		
		num.setBounds(10, 10, 265, 35);
		add(num);
		
		setSize(300, 350);
		setVisible(true);
	}
 
    public static void main(String args[])
    {      
        A7 cal = new A7();
        cal.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);        
    }
    
    public void actionPerformed(ActionEvent event)
    {   
        
    	if ((JButton) event.getSource() == button[12])// c
			num.setText("0");

		if (((JButton) event.getSource()).getText().charAt(0) <= '9'
				&& ((JButton) event.getSource()).getText().charAt(0) >= '0')
		{
			numberstring += ((JButton) event.getSource()).getText().charAt(0)
					+ "";
			num.setText(numberstring);
		} else {// 按下+或-的顯示
			numberstring = "";
			num.setText(operation(number, Double.parseDouble(num.getText()),operator));
			operator = ((JButton) event.getSource()).getActionCommand().charAt(0);

			if (((JButton) event.getSource()).getText().charAt(0) != '=')
				number = Double.parseDouble(num.getText());
		}

    }
   
    public String operation(double number1,double number2,char operator)
    {      
        switch(operator)
        {        
        case '+':
			if (number1 + number2 > 999999999)
				return error;
			else
				return "" + (number1 + number2);
		case '-':
			if (number1 - number2 > 999999999)
				return error;
			else
				return "" + (number1 - number2);
		default:
			if (number1 > 999999999 || number1 > 999999999)//第一個數字溢位時 無法在第一次按下符號時顯示error
				return error;
			else
				return "" + (number2);
        }
    }
}
