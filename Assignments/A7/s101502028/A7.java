// bug while exchange plus and minus button
// bug after clicking equal button many times
package ce1002.A7.s101502028;

import java.awt.Color;
import java.awt.Font;
import java.awt.Label;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.*;

public class A7 extends JFrame implements ActionListener {
	private Label ans = new Label("0", Label.RIGHT);
	private JButton one = new JButton("1");
	private JButton two = new JButton("2");
	private JButton three = new JButton("3");
	private JButton four = new JButton("4");
	private JButton five = new JButton("5");
	private JButton six = new JButton("6");
	private JButton seven = new JButton("7");
	private JButton eight = new JButton("8");
	private JButton nine = new JButton("9");
	private JButton zero = new JButton("0");
	private JButton clear = new JButton("C");
	private JButton minus = new JButton("-");
	private JButton plus = new JButton("+");
	private JButton equal = new JButton("=");
	private int result = 0;
	private int lastNumber = 0;
	private int totalNumber = 0;
	private int show = 0;
	private int timesOperation = 0;
	private String operation;

	public A7(String pTitle) {
		super(pTitle);
		setLayout(null);
		ans.setBounds(30, 30, 225, 50);
		ans.setBackground(new Color(255, 255, 255));
		Font font = new Font ("Serif", Font.PLAIN, 50);
		ans.setFont(font);
		seven.setBounds(30, 100, 45, 25); // button setting
		eight.setBounds(90, 100, 45, 25);
		nine.setBounds(150, 100, 45, 25);
		minus.setBounds(210, 100, 45, 25);
		four.setBounds(30, 140, 45, 25);
		five.setBounds(90, 140, 45, 25);
		six.setBounds(150, 140, 45, 25);
		plus.setBounds(210, 140, 45, 25);
		one.setBounds(30, 180, 45, 25);
		two.setBounds(90, 180, 45, 25);
		three.setBounds(150, 180, 45, 25);
		equal.setBounds(210, 180, 45, 65);
		zero.setBounds(30, 220, 105, 25);
		clear.setBounds(150, 220, 45, 25);
		add(ans);
		add(seven);
		add(eight);
		add(nine);
		add(minus);
		add(four);
		add(five);
		add(six);
		add(plus);
		add(one);
		add(two);
		add(three);
		add(equal);
		add(zero);
		add(clear);
		equal.addActionListener(this);
		plus.addActionListener(this);
		minus.addActionListener(this);
		clear.addActionListener(this);
		zero.addActionListener(this);
		one.addActionListener(this);
		two.addActionListener(this);
		three.addActionListener(this);
		four.addActionListener(this);
		five.addActionListener(this);
		six.addActionListener(this);
		seven.addActionListener(this);
		eight.addActionListener(this);
		nine.addActionListener(this);
	}

	public void actionPerformed(ActionEvent e) { // action of the button

		if (e.getSource() == clear) { // clear button
			ans.setText("0");
			operation = "";
			result = 0;
			totalNumber = 0;
			lastNumber = 0;
			show = 0;
			timesOperation = 0;

		} else if (e.getSource() == zero) { // zero button
			totalNumber++;
			if (totalNumber <= 9) {
				show = show * 10;
				show = show + 0;
				ans.setText(String.valueOf(show));
			} else
				ans.setText("error.");

		} else if (e.getSource() == one) { // one button
			totalNumber++;
			if (totalNumber <= 9) {
				show = show * 10;
				show = show + 1;
				ans.setText(String.valueOf(show));
			} else
				ans.setText("error.");

		} else if (e.getSource() == two) { // two button
			totalNumber++;
			if (totalNumber <= 9) {
				show = show * 10;
				show = show + 2;
				ans.setText(String.valueOf(show));
			} else
				ans.setText("error.");

		} else if (e.getSource() == three) { // three button
			totalNumber++;
			if (totalNumber <= 9) {
				show = show * 10;
				show = show + 3;
				ans.setText(String.valueOf(show));
			} else
				ans.setText("error.");

		} else if (e.getSource() == four) { // four button
			totalNumber++;
			if (totalNumber <= 9) {
				show = show * 10;
				show = show + 4;
				ans.setText(String.valueOf(show));
			} else
				ans.setText("error.");

		} else if (e.getSource() == five) { // five button
			totalNumber++;
			if (totalNumber <= 9) {
				show = show * 10;
				show = show + 5;
				ans.setText(String.valueOf(show));
			} else
				ans.setText("error.");

		} else if (e.getSource() == six) { // six button
			totalNumber++;
			if (totalNumber <= 9) {
				show = show * 10;
				show = show + 6;
				ans.setText(String.valueOf(show));
			} else
				ans.setText("error.");

		} else if (e.getSource() == seven) { // seven button
			totalNumber++;
			if (totalNumber <= 9) {
				show = show * 10;
				show = show + 7;
				ans.setText(String.valueOf(show));
			} else
				ans.setText("error.");

		} else if (e.getSource() == eight) { // eight button
			totalNumber++;
			if (totalNumber <= 9) {
				show = show * 10;
				show = show + 8;
				ans.setText(String.valueOf(show));
			} else
				ans.setText("error.");

		} else if (e.getSource() == nine) { // nine button
			totalNumber++;
			if (totalNumber <= 9) {
				show = show * 10;
				show = show + 9;
				ans.setText(String.valueOf(show));
			} else
				ans.setText("error.");

		} else if (e.getSource() == minus) { // action of the minus button
			lastNumber = Integer.parseInt(ans.getText());
			if (operation == "+") { // if have plus action before
				result = result + lastNumber;
				if (result >= 999999999 || result <= -999999999)
					ans.setText("error");
				else
					ans.setText(String.valueOf(result));
			}
			operation = "-";
			if (timesOperation == 0) {
				result = lastNumber - result;
			} else {
				result -= lastNumber; // minus action
				if (result >= 999999999 || result <= -999999999)
					ans.setText("error");
				else
					ans.setText(String.valueOf(result));
			}
			show = 0;
			totalNumber = 0;
			timesOperation++;

		} else if (e.getSource() == plus) { // action of the plus button
			lastNumber = Integer.parseInt(ans.getText());
			if (operation == "-") { // if have minus action before
				result = result - lastNumber - 1;
				if (result >= 999999999 || result <= -999999999)
					ans.setText("error");
				else
					ans.setText(String.valueOf(result));
			}
			operation = "+";
			result += lastNumber; // plus action
			if (result >= 999999999 || result <= -999999999)
				ans.setText("error");
			else
				ans.setText(String.valueOf(result));
			show = 0;
			totalNumber = 0;

		} else if (e.getSource() == equal) { // action of the equal button
			lastNumber = Integer.parseInt(ans.getText());
			if (operation == "-") { // minus action
				result -= lastNumber;
				if (result >= 999999999 || result <= -999999999)
					ans.setText("error");
				else
					ans.setText(String.valueOf(result));
			} else if (operation == "+") { // plus action
				result += lastNumber;
				if (result >= 999999999 || result <= -999999999)
					ans.setText("error");
				else
					ans.setText(String.valueOf(result));
			}
		}
	}

	public static void main(String[] args) {
		A7 calc = new A7("�p��L");
		calc.setSize(300, 310); // frame setting
		calc.setLocationRelativeTo(null);
		calc.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		calc.setVisible(true);
	}
}
