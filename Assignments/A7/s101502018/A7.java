package ce1002.A7.s101502018;
//有參考網路http://www.javaworld.com.tw/jute/post/view?bid=29&id=87099&sty=3的範例
import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
public class A7 extends JFrame implements ActionListener{
    
	JTextField numberfield=new JTextField("0");;
	
	String numberstring="";
	String error="error.";
	double number;
	
	char buttonname[]={'7','8','9',
	                   '4','5','6',
	                   '1','2','3',
	                   '-','0','+',
	                   'c','='};
	                   
	JButton button[]=new JButton[14];
	
	char operator;
	
	public A7()
	{        
	    super("計算機");
	    Container c=getContentPane();
	    setLayout(null);
	    numberfield.setHorizontalAlignment(JTextField.RIGHT);
	
	    button[0]=new JButton(buttonname[0]+"");//7
	    button[0].setBounds(10, 60, 60, 50);
	    button[0].addActionListener(this);
	    add(button[0]);	
	    button[1]=new JButton(buttonname[1]+"");//8
	    button[1].setBounds(80, 60, 60, 50);
	    button[1].addActionListener(this);
	    add(button[1]);
	    button[2]=new JButton(buttonname[2]+"");//9
	    button[2].setBounds(150, 60, 60, 50);
	    button[2].addActionListener(this);
	    add(button[2]);
	    button[3]=new JButton(buttonname[3]+"");//4
	    button[3].setBounds(10, 140, 60, 50);
	    button[3].addActionListener(this);
	    add(button[3]);
	    button[4]=new JButton(buttonname[4]+"");//5
	    button[4].setBounds(80, 140, 60, 50);
	    button[4].addActionListener(this);
	    add(button[4]);
	    button[5]=new JButton(buttonname[5]+"");//6
	    button[5].setBounds(150, 140, 60, 50);
	    button[5].addActionListener(this);
	    add(button[5]);
	    button[6]=new JButton(buttonname[6]+"");//1
	    button[6].setBounds(10, 220, 60, 50);
	    button[6].addActionListener(this);
	    add(button[6]);
	    button[7]=new JButton(buttonname[7]+"");//2
	    button[7].setBounds(80, 220, 60, 50);
	    button[7].addActionListener(this);
	    add(button[7]);
	    button[8]=new JButton(buttonname[8]+"");//3
	    button[8].setBounds(150, 220, 60, 50);
	    button[8].addActionListener(this);
	    add(button[8]);
	    button[9]=new JButton(buttonname[9]+"");//-
	    button[9].setBounds(220, 60, 60, 50);
	    button[9].addActionListener(this);
	    add(button[9]);
	    button[10]=new JButton(buttonname[10]+"");//0
	    button[10].setBounds(10, 300, 130, 50);
	    button[10].addActionListener(this);
	    add(button[10]);
	    button[11]=new JButton(buttonname[11]+"");//+
	    button[11].setBounds(220, 140, 60, 50);
	    button[11].addActionListener(this);
	    add(button[11]);
	    button[12]=new JButton(buttonname[12]+"");//C
	    button[12].setBounds(150, 300, 60, 50);
	    button[12].addActionListener(this);
	    add(button[12]);
	    button[13]=new JButton(buttonname[13]+"");//=
	    button[13].setBounds(220, 220, 60, 130);
	    button[13].addActionListener(this);
	    add(button[13]);
	    numberfield.setBounds(10, 10, 265, 35);
	    c.add(numberfield);
	  
	    setSize(305,410);
	    setVisible(true);
	}
	
	public void actionPerformed(ActionEvent event)
	{   
	    if((JButton)event.getSource()==button[12])//歸零
	    	numberfield.setText("0");
	    if(((JButton)event.getSource()).getText().charAt(0)<='9'&&//顯示數字
	             ((JButton)event.getSource()).getText().charAt(0)>='0'){
	        numberstring+=((JButton)event.getSource()).getText().charAt(0)+"";
	        numberfield.setText(numberstring);
	    }
	    else{//顯示計算過的數字
	        	numberstring="";
	        	numberfield.setText(operation(number,
		                Double.parseDouble(numberfield.getText()),operator));
		        operator=((JButton)event.getSource()).getActionCommand().charAt(0);
		        
		        if(((JButton)event.getSource()).getText().charAt(0)!='=')                  
		            number=Double.parseDouble(numberfield.getText());
	    }

	}
	
	public String operation(double number1,double number2,char operator)//計算數字                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                             
	{      
	    switch(operator)
	    {        
	        case '+':
	        	if(number1+number2>999999999)
	        		return error;
	        	else
	        		return ""+(number1+number2);
	        case '-':
	        	if(number1+number2>999999999)
	        		return error;
	        	else
	        		return ""+(number1-number2);
	        default:
	        	if(number1+number2>999999999)
	        		return error;
	        	else
	        		return ""+(number2); 
	    }
	}
	   
	public static void main(String args[])
	{      
	    A7 a=new A7();
	    a.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);        
	}

}
