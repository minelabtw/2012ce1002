package ce1002.A7.s101502517;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.*;

import java.awt.Color;
import java.awt.Font;
import java.awt.Component;
import java.awt.Graphics;
import java.awt.Graphics2D;

public class A7 extends JFrame implements ActionListener{
	
	public static int wb=0,wc=0,eq_btn=0;//wb紀錄上一個運算紐 wc紀錄狀況
	long sum=0,temp=0,eq_temp=0;//sum總和  temp暫存

	public static JButton btn_c = new JButton("C");//建立BUTTON
	public static JButton btn_mi = new JButton("-");//-
	public static JButton btn_plus = new JButton("+");//+
	public static JButton btn_eq = new JButton("=");//=
	public static JButton btn_0 = new JButton("0");
	
	public static JButton btn_1 = new JButton("1");
	public static JButton btn_2 = new JButton("2");
	public static JButton btn_3 = new JButton("3");
	public static JButton btn_4 = new JButton("4");
	public static JButton btn_5 = new JButton("5");
	public static JButton btn_6 = new JButton("6");
	public static JButton btn_7 = new JButton("7");
	public static JButton btn_8 = new JButton("8");
	public static JButton btn_9 = new JButton("9");
	public static JTextField sh = new JTextField("0.");//顯示欄
	
	Color bgc=new Color(212,231,247);//顏色
	Font tf=new Font("Verdana",Font.BOLD,28);//字體
	public static void main(String[] args) {
		A7 cal = new A7("小算盤");
		cal.setSize(285, 370);//設定大小
		cal.setLocation(0,0);
		cal.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);		
		cal.setVisible(true);
	}	
	public A7(String pTittle) {
		super(pTittle);
		setLayout(null);// 不使用版面配置
		sh.setBounds(20,20 ,230 , 60 );//文字欄設定
		this.getContentPane().setBackground(bgc);//設背景色
		sh.setFont(tf);//改字體.大小
		add(sh);
		//排列按鈕
		SBtn(btn_0,20,270,110,50);
		SBtn(btn_1,20,90,50,50);
		SBtn(btn_2,80,90,50,50);
		SBtn(btn_3,140,90,50,50);
		SBtn(btn_4,20,150,50,50);
		SBtn(btn_5,80,150,50,50);
		SBtn(btn_6,140,150,50,50);
		SBtn(btn_7,20,210,50,50);
		SBtn(btn_8,80,210,50,50);
		SBtn(btn_9,140,210,50,50);
		SBtn(btn_c,140,270,50,50);
		SBtn(btn_plus,200,90,50,50);
		SBtn(btn_mi,200,150,50,50);
		SBtn(btn_eq,200,210,50,110);
		sh.setHorizontalAlignment(javax.swing.JTextField.RIGHT);//文字靠右
	}
	
	public void SBtn(JButton B,int x , int y ,int w ,int h){//設定按鈕長寬位置
		B.setBounds(x, y, w, h);
		B.addActionListener(this);
		add(B);
	}

	public void actionPerformed(ActionEvent e) {	
		if(temp*10>999999999){//暫存溢位
		//不做任何事
		}else{//暫存沒溢位
			if(e.getSource() == btn_0)temp=temp*10+0;
			if(e.getSource() == btn_1)temp=temp*10+1;
			if(e.getSource() == btn_2)temp=temp*10+2;
			if(e.getSource() == btn_3)temp=temp*10+3;
			if(e.getSource() == btn_4)temp=temp*10+4;
			if(e.getSource() == btn_5)temp=temp*10+5;
			if(e.getSource() == btn_6)temp=temp*10+6;
			if(e.getSource() == btn_7)temp=temp*10+7;
			if(e.getSource() == btn_8)temp=temp*10+8;
			if(e.getSource() == btn_9)temp=temp*10+9;
		}
			if(e.getSource() == btn_c){
				sum=temp=wb=wc=eq_btn=0;//清空歸零
			}
		
			if(e.getSource() == btn_plus){//
				if(wb==1||wb==0 ){//上個運算鈕是+或沒有
					sum=sum+temp;
				}else if(wb==2){//上個運算鈕是-
					sum=sum-temp;
				}
				temp=eq_btn=0;//暫存歸零
				wb=1;//運算鈕一(+)
				wc=1;//狀況一
			}
			if(e.getSource() == btn_mi){//
			if(wb==1||wb==0){//上個運算鈕是+或沒有
					sum=sum+temp;
				}else if(wb==2){//上個運算鈕是-
					sum=sum-temp;
				}
				temp=eq_btn=0;//暫存歸零
				wb=2;//運算鈕二(-)
				wc=1;//狀況一
			}
			
			if(e.getSource() == btn_eq){//
				if(wb==1||wb==0){//上個運算鈕是+或沒有
					sum=sum+temp;
				}else if(wb==2){//上個運算鈕是-
					sum=sum-temp;
				}	
				temp=0;
				wb=0;
				wc=1;
				//eq_btn=1;
			}
			if(sum>999999999){//總和溢位
				sh.setText("error.");
				sum=temp=wb=wc=eq_btn=0;//歸零
			}
			else if(wc==0){//狀況0(數字鍵)
				sh.setText(Long.toString(temp)+".");
			}else{//狀況一(運算鍵)
				sh.setText(Long.toString(sum)+".");
				wc=0;//回到狀態0
			}			
	}	
}
