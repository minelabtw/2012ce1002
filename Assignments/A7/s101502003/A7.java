package ce1002.A7.s101502003;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.*;
import java.awt.*;
public class A7 extends JFrame implements ActionListener
{
	//設按鈕0~9 +-=C
	private JButton b1 = new JButton("1"); 
	private JButton b2 = new JButton("2");
	private JButton b3 = new JButton("3");
	private JButton b4 = new JButton("4");
	private JButton b5 = new JButton("5");
	private JButton b6 = new JButton("6");
	private JButton b7 = new JButton("7");
	private JButton b8 = new JButton("8");
	private JButton b9 = new JButton("9");
	private JButton b0 = new JButton("0");
	private JButton badd = new JButton("+");
	private JButton bcut = new JButton("-");
	private JButton bequ = new JButton("=");
	private JButton bcan = new JButton("C");
	private JTextField text = new JTextField(9);
	private int sum=0; //算總合
	private int a=0,check=0,b=0; //a=輸入的數,b=a,方便之後計算,check=看是加還減
	public A7()
	{
		text.setText("0."); //一開始是0
		text.setHorizontalAlignment(JTextField.RIGHT); //靠右
		setLayout(null); //版面自己配置
		/////// 配置版面 ////////////
		b1.setBounds(20, 150, 45, 35);
		add(b1);
		b2.setBounds(70,150,45, 35);
		add(b2);
		b3.setBounds(120, 150, 45, 35);
		add(b3);
		b4.setBounds(120, 110, 45, 35);
		add(b4);
		b5.setBounds(70, 110,45, 35);
		add(b5);
		b6.setBounds(20, 110, 45, 35);
		add(b6);
		b7.setBounds(20, 70, 45, 35);
		add(b7);
		b8.setBounds(70, 70, 45, 35);
		add(b8);
		b9.setBounds(120, 70, 45, 35);
		add(b9);
		b0.setBounds(20, 190, 95, 35);
		add(b0);
		badd.setBounds(170, 110, 45, 35);
		add(badd);
		bcut.setBounds(170, 70, 45, 35);
		add(bcut);
		bcan.setBounds(120, 190, 45, 35);
		add(bcan);
		bequ.setBounds(170, 150, 45, 75);
		add(bequ);
		text.setBounds(20, 20, 200, 30);
		add(text);
		/////// 配置版面 ////////////
		////// 按按鈕可執行//////////
		b1.addActionListener(this);
		b2.addActionListener(this);
		b3.addActionListener(this);
		b4.addActionListener(this);
		b5.addActionListener(this);
		b6.addActionListener(this);
		b7.addActionListener(this);
		b8.addActionListener(this);
		b9.addActionListener(this);
		b0.addActionListener(this);
		bequ.addActionListener(this);
		badd.addActionListener(this);
		bcut.addActionListener(this);
		bcan.addActionListener(this);
		////// 按按鈕可執行//////////
	}
	public void actionPerformed(ActionEvent e) {
		if(e.getSource()==b1)
		{ //按一就讓a=a*10+1,以此類推
			a=a*10+1;
			text.setText(""+a);
			b=a; //b=a是用來算sum,因為後來會讓a=0繼續計算
		}
		if(e.getSource()==b2)
		{
			a=a*10+2;
			text.setText(""+a);
			b=a;
		}
		if(e.getSource()==b3)
		{
			a=a*10+3;
			text.setText(""+a);
			b=a;
		}
		if(e.getSource()==b4)
		{
			a=a*10+4;
			text.setText(""+a);
			b=a;
		}
		if(e.getSource()==b5)
		{
			a=a*10+5;
			text.setText(""+a);
			b=a;
		}
		if(e.getSource()==b6)
		{
			a=a*10+6;
			text.setText(""+a);
			b=a;
		}
		if(e.getSource()==b7)
		{
			a=a*10+7;
			text.setText(""+a);
			b=a;
		}
		if(e.getSource()==b8)
		{
			a=a*10+8;
			text.setText(""+a);
			b=a;
		}
		if(e.getSource()==b9)
		{
			a=a*10+9;
			text.setText(""+a);
			b=a;
		}
		if(e.getSource()==b0)
		{
			a=a*10+0;
			text.setText(""+a);
			b=a;
		}
		if(e.getSource()==badd)
		{ //加號,先把這個數存入sum裡,在和下一個數相加
				text.setText(""+b);
				sum=b;
				a=0;
				check=0;
		}
		if(e.getSource()==bcut)
		{ //減號,先把這個數存入sum裡,在和下一個數相減
				text.setText(""+b);
				sum=b;
				a=0;
				check=1;
		}
		if(e.getSource()==bequ)
		{
			if(check==1)
			{ //check=1表示是減號,讓sum減並輸出結果,a=0繼續計算
				sum = sum-b;
				text.setText(""+sum);
				b=sum;
				a=0;
			}
			else
			{ //check=0表示是加號,讓sum加並輸出結果,a=0繼續計算
				sum = sum+b;
				text.setText(""+sum);
				b=sum;
				a=0;
			}
		}
		if(e.getSource()==bcan)
		{ //清除掉,所有=0
			text.setText("0");
			a=0;
			sum=0;
			b=0;
		}
		if(sum>999999999||a>999999999){
			sum=0; //輸出error後所有等於零
			a=0;
			check=2;
			text.setText("error");
		}
	}
	
	public static void main(String[] args) //設定外型等
	{
		A7 frame = new A7();
		frame.setTitle("小算盤");
		frame.setSize(250,280);
		frame.setLocationRelativeTo(null);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setVisible(true);
	}
}