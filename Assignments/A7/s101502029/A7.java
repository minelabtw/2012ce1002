package ce1002.A7.s101502029;

import java.awt.Color;
import java.awt.Label;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.*;

public class A7 extends JFrame implements ActionListener {
	private static JButton btn0 = new JButton("0");
	private static JButton btn1 = new JButton("1");
	private static JButton btn2 = new JButton("2");
	private static JButton btn3 = new JButton("3");
	private static JButton btn4 = new JButton("4");
	private static JButton btn5 = new JButton("5");
	private static JButton btn6 = new JButton("6");
	private static JButton btn7 = new JButton("7");
	private static JButton btn8 = new JButton("8");
	private static JButton btn9 = new JButton("9");
	private static JButton btn10 = new JButton("-");
	private static JButton btn11 = new JButton("+");
	private static JButton btn12 = new JButton("=");
	private static JButton btn13 = new JButton("C");
	private static Label window = new Label("0", Label.RIGHT);
	private static int result=0, nowNumber=0,s=0;
	private static int numClicks = 0;
	private static String tempChar;

	public static void main(String[] args) {
		A7 hw = new A7("衡絃");

		hw.setSize(265, 300);

		hw.setLocation(250, 250);

		hw.setVisible(true);

	}

	public A7(String pTitle) {

		super(pTitle);

		setLayout(null);

		btn0.setBounds(10, 210, 110, 30);
		btn0.addActionListener(this);
		btn1.setBounds(10, 170, 50, 30);
		btn1.addActionListener(this);
		btn2.setBounds(70, 170, 50, 30);
		btn2.addActionListener(this);
		btn3.setBounds(130, 170, 50, 30);
		btn3.addActionListener(this);
		btn4.setBounds(10, 130, 50, 30);
		btn4.addActionListener(this);
		btn5.setBounds(70, 130, 50, 30);
		btn5.addActionListener(this);
		btn6.setBounds(130, 130, 50, 30);
		btn6.addActionListener(this);
		btn7.setBounds(10, 90, 50, 30);
		btn7.addActionListener(this);
		btn8.setBounds(70, 90, 50, 30);
		btn8.addActionListener(this);
		btn9.setBounds(130, 90, 50, 30);
		btn9.addActionListener(this);
		btn10.setBounds(190, 90, 50, 30);
		btn10.addActionListener(this);
		btn11.setBounds(190, 130, 50, 30);
		btn11.addActionListener(this);
		btn12.setBounds(190, 170, 50, 70);
		btn12.addActionListener(this);
		btn13.setBounds(130, 210, 50, 30);
		btn13.addActionListener(this);
		window.setBounds(10, 20, 230, 50);
		window.setBackground(Color.white);
		add(btn0);
		add(btn1);
		add(btn2);
		add(btn3);
		add(btn4);
		add(btn5);
		add(btn6);
		add(btn7);
		add(btn8);
		add(btn9);
		add(btn10);
		add(btn11);
		add(btn12);
		add(btn13);
		add(window);
	}

	public void actionPerformed(ActionEvent e) {
		if (e.getSource() == btn0) {
			numClicks++;
			if (numClicks < 2) {   //计0
				window.setText("" + "0");
				nowNumber = Integer.parseInt(window.getText());
			}
			if (numClicks >= 2) {
				window.setText((window.getText() + "0"));
				nowNumber = Integer.parseInt(window.getText());
			}
		} else if (e.getSource() == btn1) {//计1
			numClicks++;
			if (numClicks < 2) {
				window.setText("" + "1");
				nowNumber = Integer.parseInt(window.getText());
			}
			if (numClicks >= 2) {
				window.setText((window.getText() + "1"));
				nowNumber = Integer.parseInt(window.getText());
			}
		} else if (e.getSource() == btn2) {//计2
			numClicks++;
			if (numClicks < 2) {
				window.setText("" + "2");
				nowNumber = Integer.parseInt(window.getText());
			}
			if (numClicks >= 2) {
				window.setText((window.getText() + "2"));
				nowNumber = Integer.parseInt(window.getText());
			}
		} else if (e.getSource() == btn3) {//计3
			numClicks++;
			if (numClicks < 2) {
				window.setText("" + "3");
				nowNumber = Integer.parseInt(window.getText());
			}
			if (numClicks >= 2) {
				window.setText((window.getText() + "3"));
				nowNumber = Integer.parseInt(window.getText());
			}
		} else if (e.getSource() == btn4) {//计4
			numClicks++;
			if (numClicks < 2) {
				window.setText("" + "4");
				nowNumber = Integer.parseInt(window.getText());
			}
			if (numClicks >= 2) {
				window.setText((window.getText() + "4"));
				nowNumber = Integer.parseInt(window.getText());
			}
		} else if (e.getSource() == btn5) {//计5
			numClicks++;
			if (numClicks < 2) {
				window.setText("" + "5");
				nowNumber = Integer.parseInt(window.getText());
			}
			if (numClicks >= 2) {
				window.setText((window.getText() + "5"));
				nowNumber = Integer.parseInt(window.getText());
			}
		} else if (e.getSource() == btn6) {//计6
			numClicks++;
			if (numClicks < 2) {
				window.setText("" + "6");
				nowNumber = Integer.parseInt(window.getText());
			}
			if (numClicks >= 2) {
				window.setText((window.getText() + "6"));
				nowNumber = Integer.parseInt(window.getText());
			}
		} else if (e.getSource() == btn7) {//计7
			numClicks++;
			if (numClicks < 2) {
				window.setText("" + "7");
				nowNumber = Integer.parseInt(window.getText());
			}
			if (numClicks >= 2) {
				window.setText((window.getText() + "6"));
				nowNumber = Integer.parseInt(window.getText());
			}
		} else if (e.getSource() == btn8) {//计8
			numClicks++;
			if (numClicks < 2) {
				window.setText("" + "8");
				nowNumber = Integer.parseInt(window.getText());
			}
			if (numClicks >= 2) {
				window.setText((window.getText() + "8"));
				nowNumber = Integer.parseInt(window.getText());
			}
		} else if (e.getSource() == btn9) {//计9
			numClicks++;
			if (numClicks < 2) {
				window.setText("" + "9");
				nowNumber = Integer.parseInt(window.getText());
			}
			if (numClicks >= 2) {
				window.setText((window.getText() + "9"));
				nowNumber = Integer.parseInt(window.getText());
			}
		} else if (e.getSource() == btn10){//搭腹
			nowNumber = Integer.parseInt(window.getText());
			tempChar="+";
			s++;
			if(s<2)
				result=nowNumber;
			else if(s>=2)
				result-=nowNumber;	
			window.setText(String.valueOf(result));
			numClicks = 0;
		}else if (e.getSource() == btn11) {//腹
			nowNumber = Integer.parseInt(window.getText());
			tempChar="-";
			result +=nowNumber;
			window.setText(String.valueOf(result));
			numClicks = 0;
		} else if (e.getSource() == btn12) {//单腹
			window.setText(String.valueOf(result));
		}
		if (e.getSource() == btn13) {//C
			result=0;
			window.setText("");
			nowNumber=0;
			numClicks=0;
			window.setText("0");
		}
		if (numClicks > 9) {//计计
			window.setText("error");
		}
	}

}
