package ce1002.A7.s101502014;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.*;
public class A7 extends JFrame implements ActionListener{
	int num = 0 , buffer = 0 , c = 0;
	JTextField cal = new JTextField(9);
	JButton btn1 = new JButton("1");
	JButton btn2 = new JButton("2");
	JButton btn3 = new JButton("3");
	JButton btn4 = new JButton("4");
	JButton btn5 = new JButton("5");
	JButton btn6 = new JButton("6");
	JButton btn7 = new JButton("7");
	JButton btn8 = new JButton("8");
	JButton btn9 = new JButton("9");
	JButton btn0 = new JButton("0");
	JButton btn_sub = new JButton("-");
	JButton btn_plus = new JButton("+");
	JButton btn_equal = new JButton("=");
	JButton btn_cln = new JButton("C");
	public A7() {
		setLayout(null);
		cal.setBounds(15 , 15 , 190 , 65);
		
		btn7.setBounds(15 , 85 , 43 , 40);
		btn8.setBounds(64 , 85 , 43 , 40);
		btn9.setBounds(113 , 85 , 43 , 40);
		btn_sub.setBounds(162 , 85 , 43 , 40);
		
		btn4.setBounds(15 , 132 , 43 , 40);
		btn5.setBounds(64 , 132 , 43 , 40);
		btn6.setBounds(113 , 132 , 43 , 40);
		btn_plus.setBounds(162 , 132 , 43 , 40);
		
		btn1.setBounds(15 , 177 , 43 , 40);
		btn2.setBounds(64 , 177 , 43 , 40);
		btn3.setBounds(113 , 177 , 43 , 40);
		btn_equal.setBounds(162 , 177 , 43 , 87);
		
		btn0.setBounds(15 , 224 , 92 , 40);
		btn_cln.setBounds(113 , 224 , 43 , 40);
		
		cal.setHorizontalAlignment(JTextField.RIGHT);
		Font font = new Font("Consolas" , Font.PLAIN , 30);
		Font font1 = new Font("msjh" , Font.PLAIN , 17);
		cal.setFont(font);
		btn1.setFont(font1);
		btn2.setFont(font1);
		btn3.setFont(font1);
		btn4.setFont(font1);
		btn5.setFont(font1);
		btn6.setFont(font1);
		btn7.setFont(font1);
		btn8.setFont(font1);
		btn9.setFont(font1);
		btn0.setFont(font1);
		btn_sub.setFont(font1);
		btn_plus.setFont(new Font("msjh" , Font.PLAIN , 15));
		btn_equal.setFont(new Font("msjh" , Font.PLAIN , 15));
		btn_cln.setFont(new Font("msjh" , Font.PLAIN , 13));
		cal.setText("0");
		
		add(cal);
		add(btn7);
		add(btn8);
		add(btn9);
		add(btn_sub);
		add(cal);
		add(btn6);
		add(btn5);
		add(btn4);
		add(btn_plus);
		add(cal);
		add(btn3);
		add(btn2);
		add(btn1);
		add(btn_equal);
		add(btn0);
		add(btn_cln);
		
		btn1.addActionListener(this);
		btn2.addActionListener(this);
		btn3.addActionListener(this);
		btn4.addActionListener(this);
		btn5.addActionListener(this);
		btn6.addActionListener(this);
		btn7.addActionListener(this);
		btn8.addActionListener(this);
		btn9.addActionListener(this);
		btn0.addActionListener(this);
		btn_sub.addActionListener(this);
		btn_plus.addActionListener(this);
		btn_equal.addActionListener(this);
		btn_cln.addActionListener(this);
	}
	public static void main(String[] args) {
		A7 frame = new A7(); //建立視窗
		frame.setTitle("A6"); //命名視窗
		frame.setSize(230 , 320); //設定視窗大小
		frame.setLocationRelativeTo(null); //固定視窗位置出現在螢幕的中間
		frame.setResizable(false);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setVisible(true); //讓視窗出現
	}
	public void actionPerformed(ActionEvent e) {
		if(e.getSource() == btn1) {
			num = num * 10 + 1;
			String label = Integer.toString(num);
			cal.setText(label);
		}
		else if(e.getSource() == btn2) {
			num = num * 10 + 2;
			String label = Integer.toString(num);
			cal.setText(label);
		}
		else if(e.getSource() == btn3) {
			num = num * 10 + 3;
			String label = Integer.toString(num);
			cal.setText(label);
		}
		else if(e.getSource() == btn4) {
			num = num * 10 + 4;
			String label = Integer.toString(num);
			cal.setText(label);
		}
		else if(e.getSource() == btn5) {
			num = num * 10 + 5;
			String label = Integer.toString(num);
			cal.setText(label);
		}
		else if(e.getSource() == btn6) {
			num = num * 10 + 6;
			String label = Integer.toString(num);
			cal.setText(label);
		}
		else if(e.getSource() == btn7) {
			num = num * 10 + 7;
			String label = Integer.toString(num);
			cal.setText(label);
		}
		else if(e.getSource() == btn8) {
			num = num * 10 + 8;
			String label = Integer.toString(num);
			cal.setText(label);
		}
		else if(e.getSource() == btn9) {
			num = num * 10 + 9;
			String label = Integer.toString(num);
			cal.setText(label);
		}
		else if(e.getSource() == btn0) {
			num = num * 10;
			String label = Integer.toString(num);
			cal.setText(label);
		}
		else if(e.getSource() == btn_cln) {
			buffer = 0;
			num = 0;
			c = 0;
			String label = Integer.toString(num);
			cal.setText(label);
		}
		else if(e.getSource() == btn_plus) {
			if(buffer == 0)
			{
				buffer = num;
				num = 0;
				c = 1;
			}
			else
			{
				if(c == 1)
					num = buffer + num;
				else if(c == 2)
					num = buffer - num;
				if(num < 1000000000)
				{
					String label = Integer.toString(num);
					cal.setText(label);
					buffer = num;
					num = 0;
					c = 1;
				}
				else
				{
					cal.setText("error");
					buffer = 0;
					num = 0;
					c = 0;
				}
			}
		}
		else if(e.getSource() == btn_sub) {
			if(buffer == 0)
			{
				buffer = num;
				num = 0;
				c = 2;
			}
			else
			{
				if(c == 1)
					num = buffer + num;
				else if(c == 2)
					num = buffer - num;
				if(num < 1000000000)
				{
					String label = Integer.toString(num);
					cal.setText(label);
					buffer = num;
					num = 0;
					c = 2;
				}
				else
				{
					cal.setText("error");
					buffer = 0;
					num = 0;
					c = 0;
				}
			}
		}
		else if(e.getSource() == btn_equal) {
			if(c == 1)
				num = buffer + num;
			else if(c == 2)
				num = buffer - num;
			if(c != 0)
			{
				if(num < 1000000000)
				{
					String label = Integer.toString(num);
					cal.setText(label);
					buffer = 0;
					num = 0;
					c = 0;
				}
				else
				{
					cal.setText("error");
					buffer = 0;
					num = 0;
					c = 0;
				}
			}
		}
	}
}
