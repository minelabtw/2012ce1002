
//按鍵功能有參考學習http://eportfolio.lib.ksu.edu.tw/~4980C023/blog?node=000000135網站上的設定
package ce1002.A7.s101502522;
import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
public class A7 extends JFrame implements ActionListener 
{
	private JTextField monitor = new JTextField("0");
	JButton jbt0 = new JButton("0");
	JButton jbt1 = new JButton("1");
	JButton jbt2 = new JButton("2");
	JButton jbt3 = new JButton("3");
	JButton jbt4 = new JButton("4");
	JButton jbt5 = new JButton("5");
	JButton jbt6 = new JButton("6");
	JButton jbt7 = new JButton("7");
	JButton jbt8 = new JButton("8");
	JButton jbt9 = new JButton("9");
	JButton jbtCancel = new JButton("C");
	JButton jbtPlus= new JButton("+");
	JButton jbtMinus = new JButton("-");
	JButton jbtEqual = new JButton("=");
    Font font = new Font("DialogInput",Font.BOLD,30);
	int ans=0;
	int sum =0;
	int mode=-1;
	
	public A7()
	{
		setLayout(null);//不使用排版功能
		monitor.setHorizontalAlignment(JTextField.RIGHT); //將顯示器由右往左輸入
		
		//增設按鍵
		add(jbt0);
		add(jbt1);
		add(jbt2);
		add(jbt3);
		add(jbt4 );
		add(jbt5);
		add(jbt6);
		add(jbt7);
		add(jbt8);
		add(jbt9);
		add(jbtCancel);
		add(jbtPlus);
		add(jbtMinus);
		add(jbtEqual);
		add(monitor);
		
		//按鍵位置大小設定
		jbt0.setBounds(35,300,130,40); 
		jbt1.setBounds(35,250,60,40);
		jbt2.setBounds (105,250,60,40);
		jbt3.setBounds (175,250,60,40);
		jbt4.setBounds (35,200,60,40);
		jbt5.setBounds (105,200,60,40);
		jbt6.setBounds (175,200,60,40);
		jbt7.setBounds (35,150,60,40);
		jbt8.setBounds (105,150,60,40);
		jbt9.setBounds (175,150,60,40);
		jbtCancel.setBounds (175,300,60,40);
		jbtPlus.setBounds (260,200,55,40);
		jbtMinus.setBounds (260,150,55,40);
		jbtEqual.setBounds (260,250,55,90);
		monitor.setBounds(35,40,280,60);
		
		//字體、大小設定
		jbt0.setFont(font);
		jbt1.setFont(font); 
		jbt2.setFont(font); 
		jbt3.setFont(font); 
		jbt4.setFont(font); 
		jbt5.setFont(font); 
		jbt6.setFont(font); 
		jbt7.setFont(font); 
		jbt8.setFont(font); 
		jbt9.setFont(font); 
		jbtCancel.setFont(font); 
		jbtPlus.setFont(font); 
		jbtMinus.setFont(font); 
		jbtEqual.setFont(font); 
		monitor.setFont(font); 
		
		//激發按鈕功能
		jbt0.addActionListener(this);
		jbt1.addActionListener(this);
		jbt2.addActionListener(this); 
		jbt3.addActionListener(this);
		jbt4.addActionListener(this);
		jbt5.addActionListener(this);
		jbt6.addActionListener(this);
		jbt7.addActionListener(this);
		jbt8.addActionListener(this);
		jbt9.addActionListener(this);
		jbtCancel.addActionListener(this);
		jbtPlus.addActionListener(this); 
		jbtMinus.addActionListener(this);
		jbtEqual.addActionListener(this);
	}
	public void actionPerformed(ActionEvent click)//按鍵功能、運算
	{
		if((monitor.getText()).length()<9)//避免超過9位數
		{
			if (click.getSource() == jbt0 )
			{
				monitor.setText(String.valueOf(Integer.valueOf(monitor.getText()+"0")));
			}
			if (click.getSource() == jbt1)
			{
				monitor.setText(String.valueOf(Integer.valueOf(monitor.getText()+"1")));
			}
			if (click.getSource()==jbt2)
			{
				monitor.setText(String.valueOf(Integer.valueOf(monitor.getText()+"2")));
			}
			if (click.getSource()==jbt3)
			{
				monitor.setText(String.valueOf(Integer.valueOf(monitor.getText()+"3")));
			}  
			if (click.getSource()==jbt4)
			{
			 monitor.setText(String.valueOf(Integer.valueOf(monitor.getText()+"4")));
			}  
			if (click.getSource()==jbt5)
			{
				monitor.setText(String.valueOf(Integer.valueOf(monitor.getText()+"5")));
			}  
			if (click.getSource()==jbt6)
			{
				monitor.setText(String.valueOf(Integer.valueOf(monitor.getText()+"6")));
			}
			if (click.getSource()==jbt7)
			{
				monitor.setText(String.valueOf(Integer.valueOf(monitor.getText()+"7")));
			}  
			if (click.getSource()==jbt8)
			{
				monitor.setText(String.valueOf(Integer.valueOf(monitor.getText()+"8")));
			}  
			if (click.getSource()==jbt9)
			{
				monitor.setText(String.valueOf(Integer.valueOf(monitor.getText()+"9")));
			}
		}
	 
		if (click.getSource()==jbtCancel)
		{
			monitor.setText("0");
		}
	 
		if (click.getSource()==jbtPlus)
		{
			sum=Integer.parseInt(monitor.getText());  //將顯示器上的字串數值轉為int
			monitor.setText("");
			mode=1; 
		}
		if (click.getSource()==jbtMinus)
		{
			sum=Integer.parseInt(monitor.getText());
			monitor.setText("");
			mode=2;
			
		}
		if (click.getSource()==jbtEqual)
		{
			switch(mode) //加法或減法模式
			{
				case 1:
					ans=sum+Integer.parseInt(monitor.getText());
					break;
				case 2:
					ans=sum-Integer.parseInt(monitor.getText());
					break;
			}	
			monitor.setText(ans+"");
			mode=-1;
		}
		
		if((monitor.getText()).length()>9)//溢位即輸出error
		{	
			monitor.setText("");
			monitor.setText("error.");
		}
	}
	public static void main(String[] args)
	{
		A7 frame = new A7();
		frame.setTitle("小算盤");
		frame.setSize(375,425);
		frame.setLocationRelativeTo(null);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setVisible(true);
		
	}

}
