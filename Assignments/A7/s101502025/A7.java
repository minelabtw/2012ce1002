package ce1002.A7.s101502025;

import javax.swing.*;

import java.util.*;
import java.util.Timer;
import java.awt.event.*;
import java.awt.*;

public class A7 extends JFrame{
	public static int x;
	public static int ori=0,in=0,jud=0,n=0; // ori is the answer, in is the intger input, jud for judge +or-,n for check the first time
	public static JButton int0 =new JButton("0");
	public static JButton int1 =new JButton("1"); 
	public static JButton int2 =new JButton("2");
	public static JButton int3 =new JButton("3");
	public static JButton int4 =new JButton("4");
	public static JButton int5 =new JButton("5");
	public static JButton int6 =new JButton("6");
	public static JButton int7 =new JButton("7");
	public static JButton int8 =new JButton("8");
	public static JButton int9 =new JButton("9");
	public static JButton min =new JButton("-");
	public static JButton plu =new JButton("+");
	public static JButton equ =new JButton("=");
	public static JButton c = new JButton("c");
	public static String sce= new String();
	public static JTextField scr= new JTextField("0.");  // all the buttons & label
	
	public A7(){
		setLayout(null);
		scr.setHorizontalAlignment(JTextField.RIGHT);
		int0.setBounds(20, 350, 115, 50);
		add(int0);
		int1.setBounds(20, 270, 55, 50);
		add(int1);
		int2.setBounds(80, 270, 55, 50);
		add(int2);
		int3.setBounds(140, 270, 55, 50);
		add(int3);
		int4.setBounds(20, 190, 55, 50);
		add(int4);
		int5.setBounds(80, 190, 55, 50);
		add(int5);
		int6.setBounds(140, 190, 55, 50);
		add(int6);
		int7.setBounds(20, 110, 55, 50);
		add(int7);
		int8.setBounds(80, 110, 55, 50);
		add(int8);
		int9.setBounds(140, 110, 55, 50);
		add(int9);
		min.setBounds(200, 110, 55, 50);
		add(min);
		plu.setBounds(200, 190, 55, 50);
		add(plu);
		equ.setBounds(200, 270, 55, 130);
		add(equ);
		c.setBounds(140, 350, 55, 50);
		add(c);
		scr.setBounds(20, 30, 233, 50);
		add(scr);
		
		// all the buttons
		int0.addActionListener(   // click it
		 	new ActionListener(){
				public void actionPerformed(ActionEvent e){
					in=in*10;
					if(in>999999999||ori>999999999)
						scr.setText("error.");
					else
					{
						sce= Integer.toString(in);
						scr.setText(sce+".");
					}
				}
			}
		);
		int1.addActionListener(   // click it
			 	new ActionListener(){
					public void actionPerformed(ActionEvent e){
						in=in*10+1;
						if(in>999999999||ori>999999999)
							scr.setText("error.");
						else
						{
							sce= Integer.toString(in);
							scr.setText(sce+".");
						}
					}
				}
		);
		int2.addActionListener(   // click it
			 	new ActionListener(){
					public void actionPerformed(ActionEvent e){
						in=in*10+2;
						if(in>999999999||ori>999999999)
							scr.setText("error.");
						else
						{
							sce= Integer.toString(in);
							scr.setText(sce+".");
						}
					}
				}
		);
		int3.addActionListener(   // click it
			 	new ActionListener(){
					public void actionPerformed(ActionEvent e){
						in=in*10+3;
						if(in>999999999||ori>999999999)
							scr.setText("error.");
						else
						{
							sce= Integer.toString(in);
							scr.setText(sce+".");
						}
					}
				}
		);
		int4.addActionListener(   // click it
			 	new ActionListener(){
					public void actionPerformed(ActionEvent e){
						in=in*10+4;
						if(in>999999999||ori>999999999)
							scr.setText("error.");
						else
						{
							sce= Integer.toString(in);
							scr.setText(sce+".");
						}
					}
				}
		);
		int5.addActionListener(   // click it
			 	new ActionListener(){
					public void actionPerformed(ActionEvent e){
						in=in*10+5;
						if(in>999999999||ori>999999999)
							scr.setText("error.");
						else
						{
							sce= Integer.toString(in);
							scr.setText(sce+".");
						}
					}
				}
		);
		int6.addActionListener(   // click it
			 	new ActionListener(){
					public void actionPerformed(ActionEvent e){
						in=in*10+6;
						if(in>999999999||ori>999999999)
							scr.setText("error.");
						else
						{
							sce= Integer.toString(in);
							scr.setText(sce+".");
						}
				}
			}
		);
		int7.addActionListener(   // click it
			 	new ActionListener(){
					public void actionPerformed(ActionEvent e){
						in=in*10+7;
						if(in>999999999||ori>999999999)
							scr.setText("error.");
						else
						{
							sce= Integer.toString(in);
							scr.setText(sce+".");
						}
					}
				}
		);
		int8.addActionListener(   // click it
			 	new ActionListener(){
					public void actionPerformed(ActionEvent e){
						in=in*10+8;
						if(in>999999999||ori>999999999)
							scr.setText("error.");
						else
						{
							sce= Integer.toString(in);
							scr.setText(sce+".");
						}
					}
				}
		);
		int9.addActionListener(   // click it
			 	new ActionListener(){
					public void actionPerformed(ActionEvent e){
						in=in*10+9;
						if(in>999999999||ori>999999999)
							scr.setText("error.");
						else
						{
							sce= Integer.toString(in);
							scr.setText(sce+".");
						}
					}
				}
		);
		min.addActionListener(   // minus
			 	new ActionListener(){
					public void actionPerformed(ActionEvent e){
						if(jud==0)
							ori=ori+in;
						else if(jud==1)
							ori=ori-in;
						if(in>999999999||ori>999999999)
							scr.setText("error.");
						else
						{
							sce= Integer.toString(ori);
							scr.setText(sce+".");	
							in=0;
							jud=1;
							n++;
						}
					}
			 	}
		);	 	
		plu.addActionListener(   // plus
			 	new ActionListener(){
					public void actionPerformed(ActionEvent e){
						if(jud==0||n==0)
							ori=ori+in;
						else if(jud==1)
							ori=ori-in;
						if(in>999999999||ori>999999999)
							scr.setText("error.");
						else
						{
							sce= Integer.toString(ori);
							scr.setText(sce+".");	
							in=0;
							jud=0;
							n++;
						}
					}
				}
		);
		equ.addActionListener(   // equal
			 	new ActionListener(){
					public void actionPerformed(ActionEvent e){
						if(jud==0||n==0)
							ori=ori+in;
						else if(jud==1)
							ori=ori-in;	
						if(in>999999999||ori>999999999)
							scr.setText("error.");
						else
						{
							sce= Integer.toString(ori);
							scr.setText(sce+".");
							jud=2;
						}
						n++;
				}
			}
		);
		c.addActionListener(   // clean
			 	new ActionListener(){
					public void actionPerformed(ActionEvent e){
						ori=0;
						in=0;
						jud=0;
						n=0;
						sce= Integer.toString(ori);
						scr.setText(sce+".");
					}
				}
		);
	}	
	public static void main(String[] args){   // the window 
		A7 frame = new A7();
		frame.setSize(275,450);
		frame.setTitle("小算盤");
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setLocationRelativeTo(null);
		frame.setVisible(true);
	}
}