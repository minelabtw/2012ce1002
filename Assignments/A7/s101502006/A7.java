package ce1002.A7.s101502006;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.*;


public class A7 extends JFrame implements ActionListener
{

	/////////////// Create the Button ////////////
	private JButton number0 = new JButton("0");
	private JButton number1 = new JButton("1");
	private JButton number2 = new JButton("2");
	private JButton number3 = new JButton("3");
	private JButton number4 = new JButton("4");
	private JButton number5 = new JButton("5");
	private JButton number6 = new JButton("6");
	private JButton number7 = new JButton("7");
	private JButton number8 = new JButton("8");
	private JButton number9 = new JButton("9");
	private JButton numberplus = new JButton("+");
	private JButton numbermin = new JButton("- ");
	private JButton numbere = new JButton("=");
	private JButton numberc = new JButton("c");
	///////////////////////////////////////////////
	
	/////// create variable to calculate /////////
	private boolean positive=true;
	private int ini=0;  //initial number
	private int aft=0;//later number
////////////////////////////////////////////////

    private static JTextField output = new JTextField("0");
    
    
	public static void main(String[] args) 
	{ 
		////////// create frame /////////////
		A7 frame=new A7("");
		frame.setSize(180, 172);
	    frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	    frame.setLocation(400,400);
        frame.setVisible(true);
       //////////////////////////////////////
    }	
	
	private A7(String pTitle)
	{
		setLayout(new GridBagLayout());
		
		///////////  Constraints for TextField //////////////
		output.setHorizontalAlignment(JTextField.RIGHT);
        GridBagConstraints c0 = new GridBagConstraints();
        c0.gridx = 0;
        c0.gridy = 0;
        c0.gridwidth = 4;
        c0.gridheight = 1;
        c0.weightx = 1;
        c0.weighty = 0;
        c0.fill = GridBagConstraints.HORIZONTAL;
        c0.anchor = GridBagConstraints.EAST;
        add(output, c0);
        ////////////////////////////////////////////////////
		
		
		/////////////GridBagConstraints/////////////////
		
    	GridBagConstraints c = new GridBagConstraints();
    	c.gridx = 0;
        c.gridy = 1;
        c.gridwidth = 1;
        c.gridheight = 1;
        c.weightx = 0;
        c.weighty = 0;
        c.fill = GridBagConstraints.NONE;
        c.anchor = GridBagConstraints.WEST;
		
		//////////////////////////////////////////////////
        
        /////////// add the button to frame ////////////
        add(number7, c);
        number7.addActionListener(this);
        
        c.gridx = 1;
 
        add(number8, c);
        number8.addActionListener(this);
        
        c.gridx = 2;
        add(number9, c);
        number9.addActionListener(this);
        
        c.gridx = 3;
        add(numbermin, c);
        numbermin.addActionListener(this);
        
        c.gridx = 0;
        c.gridy = 2;
        add(number4, c);
        number4.addActionListener(this);
        
        c.gridx = 1;
        c.gridy = 2;
        add(number5, c);
        number5.addActionListener(this);
        
        c.gridx = 2;
        c.gridy = 2;
        add(number6, c);
        number6.addActionListener(this);
        
        c.gridx = 3;
        c.gridy = 2;
        add(numberplus, c);
        numberplus.addActionListener(this);
        
        c.gridx = 0;
        c.gridy = 3;
        add(number1, c);
        number1.addActionListener(this);
        
        c.gridx = 1;
        c.gridy = 3;
        add(number2, c);
        number2.addActionListener(this);
        
        c.gridx = 2;
        c.gridy = 3;
        add(number3, c);
        number3.addActionListener(this);
        
        c.gridx = 2;
        c.gridy = 4;
        add(numberc, c);
        numberc.addActionListener(this);
        
        c.gridx = 3;
        c.gridy = 3;
        c.gridheight = 2;
        c.fill = GridBagConstraints.VERTICAL;
        add(numbere, c);
        numbere.addActionListener(this);
        
        c.gridx = 0;
        c.gridy = 4;
        c.gridheight = 1;
        c.gridwidth=2;
        c.fill = GridBagConstraints.HORIZONTAL;
        add(number0, c);
        number0.addActionListener(this);
        /////////////////////////////////////////////
	}
	
	
	//////////// funtion for all the button ///////////
	public void actionPerformed(ActionEvent e) 
	{
		if(ini<=999999999 && ini>=(-999999999) ) //error detection.
		{
			if(positive)
			{
				////////// plus mode ///////////
				if(e.getSource()==number7)
				{
					ini=ini*10+7;
					output.setText(Integer.toString(ini));
				}
				
				if(e.getSource()==number8)
				{
					ini=ini*10+8;
					output.setText(Integer.toString(ini));
				}
				
				if(e.getSource()==number9)
				{
					ini=ini*10+9;
					output.setText(Integer.toString(ini));
				}
				
				if(e.getSource()==number4)
				{
					ini=ini*10+4;
					output.setText(Integer.toString(ini));
				}
				
				if(e.getSource()==number5)
				{
					ini=ini*10+5;
					output.setText(Integer.toString(ini));
				}
				
				if(e.getSource()==number6)
				{
					ini=ini*10+6;
					output.setText(Integer.toString(ini));
				}
				
				if(e.getSource()==number1)
				{
					ini=ini*10+1;
					output.setText(Integer.toString(ini));
				}
				
				if(e.getSource()==number2)
				{
					ini=ini*10+2;
					output.setText(Integer.toString(ini));
				}
				
				if(e.getSource()==number3)
				{
					ini=ini*10+3;
					output.setText(Integer.toString(ini));
				}
				
				if(e.getSource()==number0)
				{
					ini=ini*10+0;
					output.setText(Integer.toString(ini));
				}
			}
			
			else
			{
				////////// minus mode //////////
				if(e.getSource()==number7)
				{
					ini=ini*10-7;
					output.setText(Integer.toString(ini));
				}
				
				if(e.getSource()==number8)
				{
					ini=ini*10-8;
					output.setText(Integer.toString(ini));
				}
				
				if(e.getSource()==number9)
				{
					ini=ini*10-9;
					output.setText(Integer.toString(ini));
				}
				
				if(e.getSource()==number4)
				{
					ini=ini*10-4;
					output.setText(Integer.toString(ini));
				}
				
				if(e.getSource()==number5)
				{
					ini=ini*10-5;
					output.setText(Integer.toString(ini));
				}
				
				if(e.getSource()==number6)
				{
					ini=ini*10-6;
					output.setText(Integer.toString(ini));
				}
				
				if(e.getSource()==number1)
				{
					ini=ini*10-1;
					output.setText(Integer.toString(ini));
				}
				
				if(e.getSource()==number2)
				{
					ini=ini*10-2;
					output.setText(Integer.toString(ini));
				}
				
				if(e.getSource()==number3)
				{
					ini=ini*10-3;
					output.setText(Integer.toString(ini));
				}
				
				if(e.getSource()==number0)
				{
					ini=ini*10-0;
					output.setText(Integer.toString(ini));
				}
			}
			
			if(e.getSource()==numberplus)
			{
				positive=true;
				aft+=ini;
				ini=0;
				output.setText(Integer.toString(aft));
			}
			
			if(e.getSource()==numbermin)
			{
				positive=false;
				aft+=ini;
				ini=0;
				output.setText(Integer.toString(aft));
			}
			
			if(e.getSource()==numbere)
			{
				output.setText(Integer.toString(aft+ini));
			}
			
			
			if(e.getSource()==numberc)
			{
				ini=0;
				aft=0;
				positive=true;
				output.setText(Integer.toString(ini));
			}
		}
		else
		{
			//////// error mode ////////////
			output.setText("error.");
			
			if(e.getSource()==numberc)
			{
				ini=0;
				aft=0;
				output.setText(Integer.toString(ini));
			}
		}
		
		
	}
}
