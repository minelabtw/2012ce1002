package ce1002.A5.s101502510;

import java.awt.*;
import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class A5 extends JFrame implements ActionListener {

	private int number = 0;
	private ImageIcon myIcon = new ImageIcon("test.jpg");// 傳入圖片
	private ImageIcon myIcon_1 = new ImageIcon("test1.jpg");
	private ImageIcon myIcon_2 = new ImageIcon("test2.jpg");
	private ImageIcon myIcon_3 = new ImageIcon("test3.jpg");
	JButton btn = new JButton(myIcon);

	public A5() {// 建構子
		add(btn);
		btn.addActionListener(this);
	}

	public static void main(String[] args) {

		A5 frame = new A5();
		frame.setTitle("A5");
		frame.setSize(500, 500);// 設定起始視窗大小
		frame.setLocationRelativeTo(null);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setVisible(true);
	}

	public void actionPerformed(ActionEvent arg0) {
		number++;// 計算旋轉次數
		if (number % 4 == 1) {
			btn.setIcon(myIcon_1);
		}
		if (number % 4 == 2) {
			btn.setIcon(myIcon_2);
		}
		if (number % 4 == 3) {
			btn.setIcon(myIcon_3);
		}
		if (number % 4 == 0) {
			btn.setIcon(myIcon);
		}
	}
}