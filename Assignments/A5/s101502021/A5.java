package ce1002.A5.s101502021;

import java.util.*;
import javax.swing.*;
import java.awt.event.*;
import java.awt.Button;
import java.awt.FlowLayout;

public class A5 extends JFrame{
	//圖片按鈕
	private ImageIcon JPG=new ImageIcon("JPG.jpg");
	private ImageIcon JPG2=new ImageIcon("JPG2.jpg");
	private ImageIcon JPG3=new ImageIcon("JPG3.jpg");
	private ImageIcon JPG4=new ImageIcon("JPG4.jpg");
	static int x=0;
	JButton bt=new JButton(JPG);
	
	public static void main(String[] args){
		A5 a5= new A5();
		a5.setTitle("A51");
		a5.setSize(400,300);
		a5.setLocationRelativeTo(null);
		a5.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		a5.setVisible(true);
		
	}
	
	public A5(){
		JPanel panel=new JPanel();
		setLayout(new FlowLayout(FlowLayout.CENTER,10,20));
		panel.add(bt);
		add(panel);
		
		pushListener listener1= new pushListener();
		bt.addActionListener(listener1);
	}
	
	//將按鈕設定為每按1.2.3.4次會有什麼反應
	class pushListener implements ActionListener{
		public void actionPerformed(ActionEvent e) {
			x++;
			if(x%4==1)
				bt.setIcon(JPG2);
			else if(x%4==2)
				bt.setIcon(JPG3);	
			else if(x%4==3)
				bt.setIcon(JPG4);
			else
				bt.setIcon(JPG);
		}
	}	
}
