package ce1002.A5.s101502526;
import java.awt.BorderLayout;
import java.awt.Container;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;

public class A5 {
	public static void main(String[] args){
		JFrame frame=new Button();
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setVisible(true);
	}
}

class Button extends JFrame {
	
	public Button(){                                            //先創造一個按鈕，按鈕的圖從另一個CLASS來
		setSize(400,300);
		final Imageicon imageicon=new Imageicon();
		JButton button=new JButton();
		button.add(imageicon);
		add(button);
		button.addActionListener(new ActionListener(){         //按下去後，先設定好角度且重新繪畫
			public void actionPerformed(ActionEvent e) {
				imageicon.setRotate(imageicon.getRotate()+Math.PI/2);
				imageicon.repaint();
			}
		});

	}
}

class Imageicon extends JPanel{
	
	public void paintComponent(Graphics g){                    //方法不是很懂，從網路找的猜測是將圖改變座標和旋轉後重新繪畫
		Graphics2D g2=(Graphics2D) g;
		g2.rotate(rotate, 175, 125);
		
		image=new ImageIcon("src/ce1002/A5/s101502526/test.jpg").getImage();
		int imgW=image.getWidth(this);
		int imgH=image.getHeight(this);
		g2.drawImage(image, (350-imgW)/2, (250-imgH)/2,this);
		g2.dispose();

	}
	public Image getImage(){
		return image;
	}
	public double getRotate(){								    //回傳目前角度
		return rotate;
	}
	public void setRotate(double rotate){						//設定角度
		this.rotate=rotate;
	}
	
	private Image image;
	private double rotate=0;
}