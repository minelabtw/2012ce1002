package ce1002.A5.s101502012;


import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;


import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;


public class A5 implements ActionListener{
	static int i=0;
	static JButton button = new JButton();
    static ImageIcon img = new ImageIcon("src\\ce1002\\A5\\s101502012\\testu.jpg");
	public static void main(String[] args) {
		frame();
		button.setIcon(img);
	}
   
	public static void frame() //視窗
	{
        JFrame frame = new JFrame();
        frame.setSize(400, 300);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setVisible(true);
		
		A5 a = new A5();
		button.addActionListener(a); //因為是static 不能用this 只好把A5實體化
		frame.add(button);
	}
	public void actionPerformed(ActionEvent e) {
		i++;
		if(1==i%4)
		{
			img = new ImageIcon("src\\ce1002\\A5\\s101502012\\testr.jpg"); //更換圖片
		}
		else if (2==i%4)
		{
			img = new ImageIcon("src\\ce1002\\A5\\s101502012\\testd.jpg"); //更換圖片
		}
		else if (3==i%4)
		{
			img = new ImageIcon("src\\ce1002\\A5\\s101502012\\testl.jpg"); //更換圖片
		}
		else
		{
			img = new ImageIcon("src\\ce1002\\A5\\s101502012\\testu.jpg"); //更換圖片
		}
		button.setIcon(img); //重新設置圖片
	}
}
