package ce1002.A5.s101502030;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
public class A5 implements ActionListener{
static int i=0;					//點擊的變數。
static JButton b ;				//按鈕。
public static void main(String[] args) 
{
	JFrame frame = new JFrame("A5");
	ImageIcon test = new ImageIcon("src/ce1002/A5/s101502030/test.jpg");		//把圖檔放到 test裡面。
	b = new JButton(test);						//按鈕跑出圖片。
	
	A5 t = new A5();
	
	b.addActionListener(t);
	
	frame.add(b);
	frame.setVisible(true);
	frame.setSize(400, 300);
	
}
public void actionPerformed(ActionEvent arg0) 
{
	i++;									//每次點擊。
	ImageIcon test = new ImageIcon("src/ce1002/A5/s101502030/test.jpg");
	ImageIcon test1 = new ImageIcon("src/ce1002/A5/s101502030/test1.jpg");
	ImageIcon test2 = new ImageIcon("src/ce1002/A5/s101502030/test2.jpg");
	ImageIcon test3 = new ImageIcon("src/ce1002/A5/s101502030/test3.jpg");
	if(i%4==1)				//轉90度。
	{
		b.setIcon(test1);
	}
	if(i%4==2)				//轉180度。
	{
		b.setIcon(test2);
	}
	if(i%4==3)				//轉270度。
	{
		b.setIcon(test3);
	}
	if(i%4==4)				//轉360度。
	{
		b.setIcon(test);
	}
}
}
