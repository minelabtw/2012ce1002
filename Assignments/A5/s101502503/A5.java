package ce1002.A5.s101502503;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
 
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;

public class A5 extends JFrame implements ActionListener
{
	////creat four image icons and a button
	private ImageIcon pic = new ImageIcon ("src/ce1002/A5/s101502503/test.jpg");
	private ImageIcon pic1 = new ImageIcon ("src/ce1002/A5/s101502503/test1.jpg");
	private ImageIcon pic2 = new ImageIcon ("src/ce1002/A5/s101502503/test2.jpg");
	private ImageIcon pic3 = new ImageIcon ("src/ce1002/A5/s101502503/test3.jpg");
	private JButton Btn = new JButton(pic);
	private int count=0;//計算點了幾次
	
	public static void main(String[] args) 
	{
		A5 frame = new A5("A5");
		frame.setSize(400, 400);//視窗大小
		frame.setVisible(true);//顯示視窗(如果是false就不會顯示)
	}
	
	public A5(String pTitle)
	{

		super(pTitle);
		
		setLayout(null);
		Btn.setBounds(50,50,250,250);
		add(Btn);//加按鈕
		
		Btn.addActionListener(this);//按按鈕後執行下面那些actionPerformed
	}
	
	public void actionPerformed(ActionEvent e) 
	{
		if(e.getSource()==Btn){//計算按下幾次
			count++;
		}
		if(count%4==0)
		{
			Btn.setIcon(pic);
		}
		else if(count%4==1)
		{
			Btn.setIcon(pic1);
		}
		else if(count%4==2)
		{
			Btn.setIcon(pic2);
		}
		else if(count%4==3)
		{
			Btn.setIcon(pic3);
		}
	}
}
