package ce1002.A5.s101502517;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.*;
import javax.swing.*;
public class A5 extends JFrame implements ActionListener{

	 	int bc=0;//計次
		//按鈕圖像
		ImageIcon SASA1 = new ImageIcon("SASA1.jpg");
		ImageIcon SASA2 = new ImageIcon("SASA2.jpg");
		ImageIcon SASA3 = new ImageIcon("SASA3.jpg");
		ImageIcon SASA4 = new ImageIcon("SASA4.jpg");
		private JButton btn = new JButton();
	
		public static void main(String[] args) {

			A5 hw = new A5("A5");

			hw.setSize(300, 300);

			hw.setLocation(0,0);

			hw.setVisible(true);

		}

		public A5(String pTitle) {

			super(pTitle);

			setLayout(null);// 不使用版面配置
			btn.setIcon(SASA1);
			btn.setBounds(30, 30, 210, 210);
			btn.addActionListener(this);
			add(btn);
		}

		public void actionPerformed(ActionEvent e) {
			bc++;
			bc=bc%4;
			if(bc==0){//判斷BTN方向
				btn.setIcon(SASA1);
			}else if(bc==1){
				btn.setIcon(SASA2);
			}else if(bc==2){
				btn.setIcon(SASA3);
			}else {
				btn.setIcon(SASA4);
			}
		}
}
