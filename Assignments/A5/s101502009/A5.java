package ce1002.A5.s101502009;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.geom.AffineTransform;
import java.awt.image.AffineTransformOp;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;

public class A5{
	JFrame jf=new JFrame("A5");
	BufferedImage bfi;
	ImageIcon ii;
	JButton jb;
	//======the way to spin
	BufferedImage spin(BufferedImage img)
	{
		AffineTransform at = new AffineTransform();
		at.rotate(Math.PI/2, img.getWidth()/2,img.getHeight()/2);
		//        角度                                                        選轉中心
		//at.rotate(Math.PI/4);
		AffineTransformOp op = new AffineTransformOp(at, AffineTransformOp.TYPE_BILINEAR);
		BufferedImage rtn = op.filter(img, null);
		return rtn;
	}
	//======the way to spin
	
	
	
	A5()throws IOException{
		bfi=ImageIO.read(new File("test.jpg"));
		ii=new ImageIcon(bfi);
		jf.setLayout(null);
		jf.setSize(500,500);
		jf.setVisible(true);
		jf.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		jb=new JButton(ii);
		jf.add(jb);
		
		
		
		jb.setBounds(100,100,200,200);
		jb.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				// TODO Auto-generated method stub
				bfi=spin(bfi);
				ii.setImage(bfi);				
			}
		});
	}
	public static void main(String[] args) throws IOException{
		A5 a5=new A5();
	}//end of main

}
