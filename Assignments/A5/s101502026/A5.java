//bonus assignment
package ce1002.A5.s101502026;

import javax.swing.*;
import java.awt.*;
import javax.imageio.ImageIO;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.image.BufferedImage;
import java.io.File;

public class A5 extends JFrame
{
	private ImageIcon Icon1 = new ImageIcon("test.jpg");
	private JButton button = new JButton(Icon1); 
	BufferedImage bufferedimage;
	private int control_number=0;
	
	public A5()
	{
        try{
            bufferedimage = ImageIO.read(new File("test.jpg"));}
        catch (Exception e){
            e.printStackTrace();}
		add(button);
		button.addActionListener(new ButtonListener()); //set a ActionListener for button
	}
	
	private class ButtonListener implements ActionListener//build ActionListener
	{
		public void actionPerformed(ActionEvent e)
		{
			if(e.getSource()==button && control_number==0) //rotate the bufferedimage about 90 degrees
			{
				BufferedImage bufferedimage2 = rotate(bufferedimage, 90);
				button.setIcon(new ImageIcon(bufferedimage2));
				control_number++;
			}
			else if(e.getSource()==button && control_number==1)  //rotate the bufferedimage about 180 degrees
			{
					BufferedImage bufferedimage3 = rotate(bufferedimage, 180);
					button.setIcon(new ImageIcon(bufferedimage3));
					control_number++;
			}
			else if(e.getSource()==button && control_number==2) //rotate the bufferedimage about 270 degrees
			{
					BufferedImage bufferedimage4 = rotate(bufferedimage, 270);
					button.setIcon(new ImageIcon(bufferedimage4));
					control_number++;
			}
			else if(e.getSource()==button && control_number==3) //rotate the bufferedimage about 360 degrees
			{
				button.setIcon(new ImageIcon(bufferedimage));
				control_number=0;
			}
		}
	}
	
    public static BufferedImage rotate(BufferedImage img, int angle) //this is the method to rotate bufferedimage
    {
        int w = img.getWidth();
        int h = img.getHeight();
        BufferedImage dimg = new BufferedImage(w, h, img.getType());
        Graphics2D g = dimg.createGraphics();
        g.rotate(Math.toRadians(angle), w / 2, h / 2);
        g.drawImage(img, null, 0, 0);
        return dimg;
    }
	 
	public static void main(String[] args)
	{
		A5 frame = new A5();
		frame.setTitle("A5");
		frame.setSize(400,400);
		frame.setLocationRelativeTo(null);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setVisible(true);
	}
}