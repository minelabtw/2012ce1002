package ce1002.A5.s101502506;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class A5 extends JFrame
{
	int time =0;

	private JButton btn = new JButton(new ImageIcon("test1.jpg"));//創造一個圖像按鈕

	public static void main(String[] args)
	{
		A5 hw = new A5("A5");//創造一個名稱為A5的GUI視窗
		hw.setSize(300, 300);
		hw.setLocation(200, 200);
		hw.setVisible(true);
	}

	public A5(String pTitle)
	{
		super(pTitle);
		setLayout(null);//不使用版面配置

		btn.setBounds(40, 40, 200, 200);
		ButtonHandler handler = new ButtonHandler();
		btn.addActionListener(handler);//在按鈕上裝一個監聽

		add(btn);
	}

	private class ButtonHandler implements ActionListener
	{
		public void actionPerformed(ActionEvent event)
		{
			if(event.getSource()== btn)
			{
				time++;
				if(time%4==1)//如果按按鈕的次數除以4餘1就轉換成圖片2
					btn.setIcon(new ImageIcon("test2.jpg"));
				if(time%4==2)//如果按按鈕的次數除以4餘2就轉換成圖片3
					btn.setIcon(new ImageIcon("test3.jpg"));
				if(time%4==3)//如果按按鈕的次數除以4餘3就轉換成圖片4
					btn.setIcon(new ImageIcon("test4.jpg"));
				if(time%4==0)//如果按按鈕的次數除以4餘0就轉換成圖片1
					btn.setIcon(new ImageIcon("test1.jpg"));

			}
		}

	}
}


