package ce1002.A5.s101502007;

import java.awt.geom.AffineTransform;
import java.awt.image.AffineTransformOp;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.imageio.ImageIO;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;

public class A5 extends JFrame{
	public JButton b=new JButton();
	public BufferedImage image;
	public static void main(String[] args) throws IOException{
		A5 frame=new A5();
		frame.setSize(400,300);
		frame.setVisible(true);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	}
	public A5()throws IOException
	{
		setLayout(null);
		
		image = ImageIO.read(new File("test.jpg"));
		b.setIcon(new ImageIcon(image));
		b.setBounds(0,0,200,200);
		b.addActionListener(new ActionListener(){ //設置按鈕

			@Override
			public void actionPerformed(ActionEvent e) {
				image=rotate(image);
				b.setIcon(new ImageIcon(image));
			}
		});
		
		add(b);
	}
	BufferedImage rotate(BufferedImage img) //旋轉的function 網路上找的
	{
		AffineTransform transform = new AffineTransform();
		transform.rotate(Math.PI/2, img.getWidth()/2,img.getHeight()/2);
		//at.rotate(Math.PI/4);
		AffineTransformOp out = new AffineTransformOp(transform, AffineTransformOp.TYPE_BILINEAR);
		BufferedImage rtn = out.filter(img, null);
		return rtn;
	}
	

}
