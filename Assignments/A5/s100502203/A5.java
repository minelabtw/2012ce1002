package ce1002.A5.s100502203;

import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.*;
import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

@SuppressWarnings("serial")
public class A5 extends JFrame {
	BufferedImage jin = ImageIO.read(new File("test.jpg"));
	
	public A5() throws IOException {
		final JButton jbt = new JButton();
		jbt.setIcon(new ImageIcon(jin));
		jbt.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				jin = A5.rotate(jin);	// push button -> rotate
				jbt.setIcon(new ImageIcon(jin));	// change icon
				setSize(jin.getWidth() + 100, jin.getHeight() + 100);
			}
		});
		setLayout(new GridLayout());
		setSize(jin.getWidth() + 100, jin.getHeight() + 100);
		add(jbt);
	}

	public static BufferedImage rotate(BufferedImage inImage) {
		int width = inImage.getWidth();
		int height = inImage.getHeight();
		BufferedImage returnImage = new BufferedImage(height, width,
				inImage.getType());	// data stream, can use it as data

		for (int x = 0; x < width; x++) {
			for (int y = 0; y < height; y++) {	// rotate by "pixel"
				returnImage.setRGB((height - y - 1), x, inImage.getRGB(x, y));
			}
		}
		return returnImage;
	}

	public static void main(String[] args) throws IOException {
		A5 frame = new A5();
		frame.setTitle("A5");
		frame.setLocationRelativeTo(null);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setVisible(true);
	}
}
