package ce1002.A5.s101502512;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.*;

public class A5 implements ActionListener {
	private static int i = 0;//計算點了幾次
	private static JButton s;
	private ImageIcon test = new ImageIcon("src/ce1002/A5/s101502512/test.jpg");
	private ImageIcon test1 = new ImageIcon("src/ce1002/A5/s101502512/test1.jpg");
	private ImageIcon test2 = new ImageIcon("src/ce1002/A5/s101502512/test2.jpg");
	private ImageIcon test3 = new ImageIcon("src/ce1002/A5/s101502512/test3.jpg");
	
	public static void main(String[] args) {
		JFrame frame = new JFrame("A5");
		ImageIcon test = new ImageIcon("src/ce1002/A5/s101502512/test.jpg");//一開始的圖案
		s = new JButton(test);

		A5 photo = new A5();

		s.addActionListener(photo);

		frame.add(s);
		frame.setVisible(true);
		frame.setLocationRelativeTo(null);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setSize(500, 400);

	}

	public void actionPerformed(ActionEvent arg0) {//計算何時要換成什麼圖片
		i++;
		
		if (i % 4 == 1) {
			s.setIcon(test1);
		}
		if (i % 4 == 2) {
			s.setIcon(test2);
		}
		if (i % 4 == 3) {
			s.setIcon(test3);
		}
		if (i % 4 == 0) {
			s.setIcon(test);
		}
	}
}
