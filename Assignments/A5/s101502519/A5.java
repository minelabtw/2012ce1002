package ce1002.A5.s101502519;

import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.RenderingHints;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.geom.AffineTransform;
import java.awt.image.AffineTransformOp;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

import javax.imageio.IIOException;
import javax.imageio.ImageIO;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;

public class A5 extends JFrame {
	
	public BufferedImage image;
	
	public static void main(String[] args) throws IOException {
		new A5();
	}
	
	public A5() throws IOException{    //建構式
		super("A5");
		setSize(500,500);
		setVisible(true);

		image=ImageIO.read(new File("test.jpg"));   //讀檔
		final JButton bt = new JButton();
		bt.setIcon(new ImageIcon(image));      //按鈕設置圖片
		bt.setLayout(null);
		bt.setBounds(0, 0, 200, 200);
		add(bt);                               //加入視窗
		bt.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				// TODO Auto-generated method stub
				image=r(image);            //重設image
				bt.setIcon(new ImageIcon(image));    //重設按鈕圖片
			}
		});
	}

	public BufferedImage r(BufferedImage img)
	{
		AffineTransform at = new AffineTransform();
		at.rotate(Math.toRadians(90), img.getWidth()/2,img.getHeight()/2);   //旋轉圖片
		AffineTransformOp op = new AffineTransformOp(at, AffineTransformOp.TYPE_BILINEAR);
		BufferedImage bi = op.filter(img, null);     //輸出圖片
		return bi;
	}
}

