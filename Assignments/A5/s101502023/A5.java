package ce1002.A5.s101502023;

import java.util.*;
import javax.swing.*;
import java.awt.event.*;
import java.awt.Button;
import java.awt.FlowLayout;

public class A5 extends JFrame
{
	private ImageIcon picture1=new ImageIcon("picture1.jpg");
	private ImageIcon picture2=new ImageIcon("picture2.jpg");
	private ImageIcon picture3=new ImageIcon("picture3.jpg");
	private ImageIcon picture4=new ImageIcon("picture4.jpg");
	static int a=0;
	JButton jbtpush=new JButton(picture1);
	public A5()
	{
		setLayout(new FlowLayout(FlowLayout.CENTER,10,20));		
		JPanel panel=new JPanel();
		panel.add(jbtpush);
		add(panel);
		pushListener listener1= new pushListener();
		jbtpush.addActionListener(listener1);
	}
	
	class pushListener implements ActionListener
	{
		public void actionPerformed(ActionEvent e) 
		{
			a++;
			if(a%4==1)
				jbtpush.setIcon(picture2);
			else if(a%4==2)
				jbtpush.setIcon(picture3);	
			else if(a%4==3)
				jbtpush.setIcon(picture4);
			else
				jbtpush.setIcon(picture1);
		}
	}
	
	public static void main(String[] args)
	{
		A5 frame= new A5();
		frame.setTitle("A51");
		frame.setSize(400,300);
		frame.setLocationRelativeTo(null);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setVisible(true);
	}
}