package ce1002.A5.s101502516;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.*;

public class A5 extends JFrame implements ActionListener {
	
	int check = 0;
	ImageIcon test = new ImageIcon ( "test.jpg" ); // 讀取圖片
	ImageIcon test2 = new ImageIcon ( "test2.jpg" );
	ImageIcon test3 = new ImageIcon ( "test3.jpg" );
	ImageIcon test4 = new ImageIcon ( "test4.jpg" );
	JButton move = new JButton();
	
	public static void main(String[] args) { // 設定視窗大小及跳出位子
		A5 hw = new A5( "A5test" );
		hw.setSize( 400, 300 );
		hw.setLocationRelativeTo( null );
		hw.setVisible( true );
	}
	
	public A5(String pTitle) { // 按鍵設定
		super(pTitle);
		setLayout(null);
		move.setBounds( 0, 0, 400, 300 );
		move.addActionListener(this);
		move.setIcon( test );
		add( move );

	}
	public void actionPerformed(ActionEvent e) { // 按鍵動作設定
		check++;
		if ( check == 0 )
		{
			move.setIcon( test2 );
		}else if ( check % 4 == 0 )
		{
			move.setIcon( test );
		}else if ( check % 4 == 1 )
		{
			move.setIcon( test2 );
		}else if ( check % 4 == 2)
		{
			move.setIcon( test3 );
		}else if ( check % 4 == 3 )
		{
			move.setIcon( test4 );
		}
	}
}
