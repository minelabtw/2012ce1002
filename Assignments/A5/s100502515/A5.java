package ce1002.A5.s100502515;

import java.awt.BorderLayout;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;

public class A5 implements ActionListener {
	Image image = new ImageIcon("test.jpg").getImage();
	JFrame frame;
	int count = 0;

	public static void main(String[] args) {
		A5 a5 = new A5();
		a5.frameMethod();
	}

	public void frameMethod() {
		frame = new JFrame("A51");
		frame.setSize(600, 600);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setResizable(false);
		frame.setVisible(true);
		JButton button = new JButton();
		Rotate rotate = new Rotate();
		button.add(BorderLayout.CENTER, rotate);//put the panel on the button
		button.addActionListener(this);
		frame.add(BorderLayout.CENTER, button);//put the button on the frame.

	}

	@Override
	public void actionPerformed(ActionEvent e) {
		frame.repaint();//repaint the frame.
		count++;

	}

	class Rotate extends JPanel {
		public void paintComponent(Graphics g) {
			Graphics2D g2d = (Graphics2D) g;
			g2d.rotate(count % 4 * Math.PI / 2, 300, 300);//translate and rotate image.
			g2d.drawImage(image, 80, 0, this);//create the image.
		}
	}

}
