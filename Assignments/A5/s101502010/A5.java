package ce1002.A5.s101502010;

import java.awt.*;
import java.awt.RenderingHints;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.image.BufferedImage;
import java.io.*;

import javax.imageio.ImageIO;
import javax.swing.*;

public class A5 {
	static BufferedImage img ;
	ImageIcon icon ;
	ImageIcon icon2;
	JButton btn ;
	public static void main(String srgs[])throws IOException{
		A5 frame = new A5();
	}
	public A5() throws IOException{
		JFrame frame = new JFrame();
		frame.setTitle("A5-101502010");
		frame.setSize(480,480);
		img =  ImageIO.read(new File("test.jpg"));
		icon = new ImageIcon(img);
		btn = new JButton(icon);
		btn.addActionListener(new ActionListener(){
          	public void actionPerformed(ActionEvent arg0) {
          		img=changeImg(img);
          		icon2 = new ImageIcon(img);
          		btn.setIcon(icon2);
          	}
        });
		frame.add(btn);
		frame.setVisible(true);
	}
	 public static BufferedImage changeImg(final BufferedImage bufferedimage) {
	        int w = bufferedimage.getWidth();
	        int h = bufferedimage.getHeight();
	        int type = bufferedimage.getColorModel().getTransparency();
	        BufferedImage img;
	        Graphics2D graphics2d;
	        (graphics2d = (img = new BufferedImage(w, h, type))
	                .createGraphics()).setRenderingHint(
	                RenderingHints.KEY_INTERPOLATION,
	                RenderingHints.VALUE_INTERPOLATION_BILINEAR);
	        graphics2d.rotate(Math.toRadians(90), w / 2, h / 2);
	        graphics2d.drawImage(bufferedimage, 0, 0, null);
	        graphics2d.dispose();
	        return img;
	    }
}
