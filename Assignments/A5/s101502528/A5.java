package ce1002.A5.s101502528;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;

public class A5 extends JFrame implements ActionListener
{
	private ImageIcon Icon = new ImageIcon("test1.jpg");
	private JLabel image = new JLabel(Icon);
	private JButton but = new JButton();
	private int change=1;
	
	public A5()
	{
		//將按鈕加入圖片
		but.add(image);
		but.addActionListener(this);
		JPanel p1 = new JPanel();
		p1.setLayout(new GridLayout(1, 1));
		//將按鈕加進p1
		p1.add(but);
		add(p1);
		
	}
	
	public static void main(String[] args)
	{
		A5 frame = new A5();
		frame.setTitle("A5");
		frame.setSize(250, 250);
		frame.setLocationRelativeTo(null);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setVisible(true);
	}
	
	public void actionPerformed(ActionEvent e)
	{
		//將四方向圖片輪轉加進按鈕中
		if(change<4)
			change++;
		
		else
			change=1;
		
		switch(change)
		{
			case 1:
				Icon = new ImageIcon("test1.jpg");
				break;
			case 2:
				Icon = new ImageIcon("test2.jpg");
				break;
			case 3:
				Icon = new ImageIcon("test3.jpg");
				break;
			case 4:
				Icon = new ImageIcon("test4.jpg");
				break;
		}
		image.setIcon(Icon);	
	}
	
}
