package ce1002.A5.s101502025;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.geom.AffineTransform;
import javax.swing.*;
import javax.swing.plaf.IconUIResource;
	// a window with a picture
	public class A5 implements ActionListener{
		public static int a=0;
		public static ImageIcon p= new ImageIcon("test.jpg"); 
		public static JButton b =new JButton(); 
		public JFrame fra;
		public static void main (String[] args){
			JFrame fra = new JFrame("A5");
			fra.setVisible(true);
			fra.setSize(500, 300);	
			fra.getContentPane().setLayout(new FlowLayout());
			A5 y=new A5();
			b.addActionListener(y);
			b.setIcon(p);
			fra.add(b);
		}
    // decide the appearance of the picture
	public void actionPerformed(ActionEvent e) {
	if (a%4==0)
		b.setIcon(new Rotate(p, 90));
	if (a%4==1)
		b.setIcon(new Rotate(p, 180));
	if (a%4==2)
		b.setIcon(new Rotate(p, 270));
	if(a%4==3)
		b.setIcon(new Rotate(p, 0));
	a++;
	}
	// rotate the picture
	class Rotate extends IconUIResource {
		private int degree;
		public Rotate(Icon icon, int deg) {
			super(icon);
			this.degree = deg;
		}
		public void paintIcon(Component c, Graphics g, int r, int u) {
			Graphics2D g2 = (Graphics2D) g;
			AffineTransform atf = g2.getTransform();
			AffineTransform clone = (AffineTransform) atf.clone();
			atf.rotate(Math.PI / 180 * degree, r + getIconWidth() / 2, u + getIconHeight() / 2);
			g2.setTransform(atf);
			super.paintIcon(c, g, r, u);
			g2.setTransform(clone);
		}
	}
}