//
//
//
//額外加分：程式只讀進一個圖片，剩下的翻轉用程式來模擬。
//
//
//
package ce1002.A5.s101502520;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.geom.AffineTransform;

import javax.imageio.ImageIO;

import java.awt.image.AffineTransformOp;
import java.awt.image.BufferedImage;

import java.io.File;
import java.io.IOException;


import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;

public class A5 extends JFrame{

	private JButton rotateImg = new JButton();  
	
	private BufferedImage bufferedImage = ImageIO.read(new File("test.jpg"));
	
	public A5() throws IOException
	{
		iniUI();
	}
	
	public void iniUI() throws IOException
	{
		setDefaultCloseOperation(EXIT_ON_CLOSE);
		setSize(300, 300);
		rotateImg.setSize(300, 300);
		
		rotateImg.setIcon(new ImageIcon(bufferedImage));
		rotateImg.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent arg0) {
				// TODO Auto-generated method stub
				rotate();
			}
		});
		getContentPane().add(rotateImg);
		setVisible(true);
	}
	
	public void rotate()
	{
//		Graphics2D tempimg = (Graphics2D)bufferedImage.getGraphics();
//		tempimg.rotate(Math.PI / 2,bufferedImage.getWidth() / 2,bufferedImage.getHeight() / 2);
//		tempimg.drawImage(bufferedImage, 0, 0, null);
//		rotateImg.setIcon(new ImageIcon(bufferedImage));
		AffineTransform Affine = new AffineTransform();
		Affine.rotate(Math.PI/2, bufferedImage.getWidth()/2,bufferedImage.getHeight()/2);
		AffineTransformOp Affineop = new AffineTransformOp(Affine, AffineTransformOp.TYPE_BILINEAR);
		bufferedImage = Affineop.filter(bufferedImage, null);
		rotateImg.setIcon(new ImageIcon(bufferedImage));
	}
	/**
	 * @param args
	 * @throws IOException 
	 */
	public static void main(String[] args) throws IOException {
		// TODO Auto-generated method stub
		new A5();
	}

}
