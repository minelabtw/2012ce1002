package ce1002.A5.s101502008;
import java.awt.*;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.*;
import java.awt.RenderingHints;
import java.awt.image.BufferedImage;
import java.io.*;
import javax.imageio.ImageIO;

public class A5 {
	static BufferedImage image ;
	ImageIcon icon ;
	ImageIcon iconn;
	JButton btn ;
	public static void main(String srgs[])throws IOException{
		A5 frame = new A5();
	}
	public A5() throws IOException{
		JFrame frame = new JFrame();
		frame.setTitle("A5-101502008");
		frame.setSize(500,500);
		image = ImageIO.read(new File("test.jpg"));
		icon = new ImageIcon(image);
		btn = new JButton(icon);
		btn.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent arg0) {
				image=changeImg(image);
				iconn = new ImageIcon(image);
				btn.setIcon(iconn);
			}
		});
		frame.add(btn);
		frame.setVisible(true);
	}
	public static BufferedImage changeImg(final BufferedImage bufferedimage) {//����
		int w = bufferedimage.getWidth();
		int h = bufferedimage.getHeight();
		int type = bufferedimage.getColorModel().getTransparency();
		BufferedImage image;
		Graphics2D graphics2d;
		(graphics2d = (image = new BufferedImage(w, h, type))
		.createGraphics()).setRenderingHint(
		RenderingHints.KEY_INTERPOLATION,
		RenderingHints.VALUE_INTERPOLATION_BILINEAR);
		graphics2d.rotate(Math.toRadians(90), w / 2, h / 2);//�੷��
		graphics2d.drawImage(bufferedimage, 0, 0, null);
		graphics2d.dispose();
		return image;
	}
}