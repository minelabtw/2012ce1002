package ce1002.A5.s101502511;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.*;

public class A5 extends JFrame implements ActionListener{
	/////new icon 
	private ImageIcon icon4 = new ImageIcon("4.jpg");
	private ImageIcon icon3 = new ImageIcon("3.jpg");
	private ImageIcon icon2 = new ImageIcon("2.jpg");
	private ImageIcon icon1 = new ImageIcon("1.jpg");
	private JButton btn = new JButton();
	private int i=1;//計算點及次數

	public static void main(String[] args) {
		A5 frame = new A5();
		frame.setTitle("A5");
		frame.setSize(600, 600);
		frame.setLocationRelativeTo(null);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setVisible(true);
	}
	
	public A5(){
		btn.setBounds(0, 0, 600, 600);
		btn.setIcon(icon1);//一開始為第一張
		btn.addActionListener(this);//actionlistener
		add(btn);
	}

	public void actionPerformed(ActionEvent e) {
		i++;
		switch(i){
		case 1://第一下出現第一張
			btn.setIcon(icon1);
			break;
		case 2://第二張
			btn.setIcon(icon2);
			break;
		case 3://第三張
			btn.setIcon(icon3);
			break;
		case 4://第四張
			btn.setIcon(icon4);
			i=0;	//第五下的時候i變回初始值0		
			break;
		}
	}
}
