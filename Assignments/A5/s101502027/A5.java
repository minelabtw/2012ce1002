package ce1002.A5.s101502027;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.geom.AffineTransform;
import java.util.*;
import javax.swing.*;
import javax.swing.plaf.IconUIResource;
public class A5 implements ActionListener{
	public static int a=0;
	public static ImageIcon  ib1= new ImageIcon("src/ce1002/A5/s101502027/test.jpg");  
	public static JButton b1 =new JButton();  
	
	public JFrame frame;
	public static void main (String[] args){
		JFrame frame = new JFrame("A51");
		frame.setVisible(true);
		frame.setSize(400, 300);	
		A5 t=new A5();
		b1.addActionListener(t);
		b1.setIcon(ib1);
		frame.add(b1);
		//一個視窗+一個ICON圖
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		a++;
		if (a%4==1)
			b1.setIcon(new RotateIcon(ib1, 90));
		else if (a%4==2)
			b1.setIcon(new RotateIcon(ib1, 180));
		else if (a%4==3)
			b1.setIcon(new RotateIcon(ib1, 270));
		else
			b1.setIcon(new RotateIcon(ib1, 0));
	}
	//按下去後的動作
	class RotateIcon extends IconUIResource {//GOOGLE的 ㄏㄏ
		 
	    private int degree;
	 
	    public RotateIcon(Icon icon, int degree) {
	        super(icon);
	        this.degree = degree;
	    }
	    public void paintIcon(Component c, Graphics g, int x, int y) {
	        Graphics2D g2d = (Graphics2D) g;
	      
	        AffineTransform atf = g2d.getTransform();
	        AffineTransform clone = (AffineTransform) atf.clone();
	 
	        atf.rotate(Math.PI / 180 * degree, x + getIconWidth() / 2, y + getIconHeight() / 2);
	        g2d.setTransform(atf);

	        super.paintIcon(c, g, x, y);

	        g2d.setTransform(clone);
	    }
}
}

