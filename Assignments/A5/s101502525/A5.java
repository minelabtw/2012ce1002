package ce1002.A5.s101502525;

import java.awt.*;
import java.awt.event.*;
import java.awt.image.BufferedImage;
import java.io.*;

import javax.imageio.ImageIO;
import javax.swing.*;

//bonus: rotate with loading only one image
public class A5{
	//declare
	JFrame frame;
	JButton button;
	BufferedImage image;
	
	
	A5() throws IOException{
		setUI();
	}
	
	public void setUI() throws IOException{
		//image
		image=ImageIO.read(new File("test.jpg"));

		//button
		button=new JButton(new ImageIcon(image));
		button.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent arg0) {
				image=rotateImage90(image);
				button.setIcon(new ImageIcon(image));//reset Icon
			}
		});
		//frame
		frame=new JFrame("A5");
		frame.setSize(400,300);
		frame.add(button);
		frame.setVisible(true);
	}
	
	//arranged from http://blog.csdn.net/cping1982/article/details/2078999
	public BufferedImage rotateImage90(BufferedImage sourceImage) {
        int w = sourceImage.getWidth();
        int h = sourceImage.getHeight();
        BufferedImage newImage=new BufferedImage(h,w,sourceImage.getType());
        Graphics2D graphics2d=newImage.createGraphics();//load into graphics2D	
        graphics2d.rotate(Math.toRadians(90),h/2,h/2);
        graphics2d.drawImage(sourceImage, 0, 0, null);//draw sourceImage on newImage
        return newImage;
    }
	
	public static void main(String[] args) throws IOException{
		new A5();
	}
}