package ce1002.A5.s101502514;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.Icon;
import javax.swing.JLabel;
public class A5 implements ActionListener{
	public static int i=1;
	static Icon pic;
	static JButton b = new JButton();
	public static void main(String[] args) {
		pic = new ImageIcon("src/ce1002/A5/s101502514/test.jpg");
		JFrame frame = new JFrame("A5");
		b = new JButton(pic);//將圖片弄在button上
		A5 t = new A5();
		b.addActionListener(t);
		b.setSize(300,300);
		frame.add(b);//新增button
		frame.setVisible(true);
		frame.setSize(300, 300);	
	}

	public void actionPerformed(ActionEvent arg0) {
		if (i < 4)
			i++;
		else
            i=1;
		
		switch (i) {//翻轉
			case 1:{
				pic = new ImageIcon("src/ce1002/A5/s101502514/test.jpg");
				break;
			}
			case 2:{
				pic = new ImageIcon("src/ce1002/A5/s101502514/test2.jpg");
                break;
            }
            case 3:{
            	pic = new ImageIcon("src/ce1002/A5/s101502514/test3.jpg");
                break;
            }
            case 4:{
            	pic = new ImageIcon("src/ce1002/A5/s101502514/test4.jpg");
                break;
            }

		}
		b.setIcon(pic);//選擇哪一張圖片弄在button上
	}

}


