package ce1002.A5.s101502518;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.geom.AffineTransform;
import java.awt.image.AffineTransformOp;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;

public class A5 extends JFrame{
	
	int i = 0;
	JButton bt = new JButton();
	public BufferedImage img;
	
	public static void main(String[] args) throws IOException
	{
		A5 win = new A5();
		win.setSize(500,500);
		win.setVisible(true);
		win.setDefaultCloseOperation(EXIT_ON_CLOSE);
	}
	
	public A5() throws IOException
	{
		img = ImageIO.read(new File("test.jpg"));
		setLayout(null);
		bt.setIcon(new ImageIcon(img));
		bt.setBounds(150, 150, 200, 200);
		
		bt.addActionListener(new ActionListener()
		{
			public void actionPerformed(ActionEvent e)//������s
				{
					img = rotateImage(img);
					bt.setIcon(new ImageIcon(img));
				}
		});
		add(bt);
	}
	
	BufferedImage rotateImage(BufferedImage i)//rotate the sasa
	{
		AffineTransform atf = new AffineTransform();
		atf.rotate(Math.PI/2, i.getWidth()/2,i.getHeight()/2);
		AffineTransformOp atfop = new AffineTransformOp(atf, AffineTransformOp.TYPE_BILINEAR);
		BufferedImage r = atfop.filter(i, null);
		return r;
	}
}
