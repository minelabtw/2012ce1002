package ce1002.A5.s101502003;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.*;

public class A5 extends JFrame implements ActionListener 
{
	private ImageIcon oneIcon = new ImageIcon("SASA1.jpg"); //設一個imageIcon存入第一張圖片
	private ImageIcon twoIcon = new ImageIcon("SASA2.jpg"); //第二張
	private ImageIcon threeIcon = new ImageIcon("SASA3.jpg"); //第三張
	private ImageIcon fourIcon = new ImageIcon("SASA4.jpg"); //第四張
	private JButton but = new JButton(oneIcon); //設一個按鈕上面顯示第一張圖片
	private int count =0; //用來算按了第幾次
	public static void main(String[] args) {
		A5 hw = new A5("A51"); //標題名稱
		hw.setSize(350, 350);
		hw.setLocation(350, 350);
		hw.setVisible(true);
	}
	public A5(String pTitle) {
		super(pTitle);
		setLayout(null);// 不使用版面配置
		but.setBounds(50,50,200,200);
		add(but);
		but.addActionListener(this);
	}
	public void actionPerformed(ActionEvent e) 
	{
			count++; //按一次count加一
			if(count%4==1) //如果除以四於一表示是第二張圖片,把第一張圖片取代掉
				but.setIcon(twoIcon);
			if(count%4==2) //如果除以四於二表示是第三張圖片,把第二張圖片取代掉
				but.setIcon(threeIcon);
			if(count%4==3) //如果除以四於三表示是第三張圖片,把第三張圖片取代掉
				but.setIcon(fourIcon);
			if(count%4==0) //如果除以四於零表示是第一張圖片,把第四張圖片取代掉
				but.setIcon(oneIcon);
	}	
}