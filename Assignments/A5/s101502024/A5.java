package ce1002.A5.s101502024;//旋轉方法是上網找的(bonus)

import java.awt.*;
import java.awt.event.*;
import java.awt.geom.AffineTransform;
import javax.swing.*;
import javax.swing.plaf.IconUIResource;

public class A5 implements ActionListener {
	public static int Rcount = 0;
	public static ImageIcon ha = new ImageIcon("test.jpg");
	public static JButton hue = new JButton();

	public static void main(String[] args) {//設定jframe & button
		JFrame huehue = new JFrame("A51");
		huehue.setSize(299, 299);
		huehue.setVisible(true);
		A5 t = new A5();
		hue.addActionListener(t);
		hue.setIcon(ha);
		huehue.add(hue);
	}

	public void actionPerformed(ActionEvent e) {
		Rcount++;//判斷旋轉多少度
		if (Rcount % 4 == 1) {
			hue.setIcon(new RotateIcon(ha, 90));
		}

		else if (Rcount % 4 == 2) {
			hue.setIcon(new RotateIcon(ha, 180));
		}

		else if (Rcount % 4 == 3) {
			hue.setIcon(new RotateIcon(ha, 270));
		}

		else {
			hue.setIcon(new RotateIcon(ha, 0));
		}

	}

	class RotateIcon extends IconUIResource {//旋轉方法這裡就是GOOGLE的了
		private static final long serialVersionUID = 1L;
		private int degree;

		public RotateIcon(Icon icon, int degree) {
			super(icon);
			this.degree = degree;
		}

		public void paintIcon(Component c, Graphics g, int x, int y) {
			Graphics2D g2d = (Graphics2D) g;

			AffineTransform atf = g2d.getTransform();
			AffineTransform clone = (AffineTransform) atf.clone();

			atf.rotate(Math.PI / 180 * degree, x + getIconWidth() / 2, y
					+ getIconHeight() / 2);
			g2d.setTransform(atf);

			super.paintIcon(c, g, x, y);

			g2d.setTransform(clone);
		}
	}
}
