package ce1002.A5.s101502019;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.*;

public class A5 extends JFrame implements ActionListener {
		private JButton button;
		int count = 0;
		public A5(){
			super("A5");
			setSize(300,300);
			setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
			ImageIcon imageIcon = new ImageIcon("test.jpg");
			button = new JButton(imageIcon);
			button.addActionListener(this);
			add(button);
			setVisible(true);
		}

		public static void main(String[] args) {
			new A5();
		}
	/*public JButton button;
	public static void main(String[] args) {
		A5 label = new A5("A5");
		label.setSize(300, 300);
		label.setLocationRelativeTo(null);
		label.setVisible(true);
	}

	public A5(String pTitle) {
		super(pTitle);                           
		setLayout(null);// 不使用版面配置
		ImageIcon imageIcon = new ImageIcon("test.jpg");
		button = new JButton(imageIcon);
		button.addActionListener(this);
		add(button);                                                                                                                                                             
	}*/                                                

	public void actionPerformed(ActionEvent e) {
		if(e.getSource() == button){
			//switch(count){
			//case 1:
			if(count == 0){
				ImageIcon imageIconRight = new ImageIcon("test1.jpg");
				button.setIcon(imageIconRight);
				count++;
			}
			else if(count == 1)//case 2:
			{	
				ImageIcon imageIconDown = new ImageIcon("test2.jpg");
				button.setIcon(imageIconDown);
				count++;
			}
			else if(count == 2)//case 3:
			{	
				ImageIcon imageIconLeft = new ImageIcon("test3.jpg");
				button.setIcon(imageIconLeft);
				count++;
			}
			else if(count == 3)//case 4:
			{	
				ImageIcon imageIcon = new ImageIcon("test.jpg");
				button.setIcon(imageIcon);
				count++;
			}
			if(count >= 4){
				count = count%4;
			}
		}
	}
}