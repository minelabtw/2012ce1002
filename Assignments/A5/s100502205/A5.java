package ce1002.A5.s100502205;

import java.io.File;

import java.awt.Color;
import java.awt.Image;
import java.awt.image.BufferedImage;
import java.awt.Graphics2D;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.imageio.ImageIO;
import javax.swing.JFrame;
import javax.swing.JButton;
import javax.swing.ImageIcon;

public class A5 extends JFrame {
	private JButton btn;
	private BufferedImage buffer;

	public BufferedImage rotate90(BufferedImage source) { // only width == height
		int width, height;
		width = source.getWidth();
		height = source.getHeight();
		BufferedImage out = new BufferedImage(height, width, source.getType());
		Graphics2D g2d = out.createGraphics();
		g2d.rotate(Math.toRadians(90), height/2, width/2);
		g2d.drawImage(source, 0, 0, null);
		return out;
	}

	public A5() {
		this.setTitle("A5");
		this.setSize(450, 350);
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

		try {
			btn = new JButton();
			buffer = ImageIO.read(new File("test.jpg"));
		} catch (Exception e) {

		}

		btn.setIcon(new ImageIcon(buffer));
		btn.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				buffer = rotate90(buffer);
				btn.setIcon(new ImageIcon(buffer));
			}
		});

		this.add(btn);

		this.setVisible(true);
		this.setResizable(false);
	}

	public static void main(String[] args) {
		new A5();
	}
}
