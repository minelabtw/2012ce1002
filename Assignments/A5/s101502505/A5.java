package ce1002.A5.s101502505;
import java.awt.event.*;
import javax.swing.*;

public class A5 extends JFrame implements ActionListener{
	
	JButton picturn = new JButton();//製造按鈕
	
	public static void main(String[] args)
	{
		A5 frame = new A5("A51");//製造視窗
		frame.setTitle("A51");//主題
		frame.setSize(300,300);//大小
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);//關閉視窗
		frame.setVisible(true);//顯示
	}
	
	public A5(String pTitle) 
	{
		super(pTitle);
		picturn.setBounds(0, 0, 300, 300);//按鈕大小&位置
		setLayout(null);//版面配置
		picturn.addActionListener(this);
		add(picturn);	
		ImageIcon picpic = new ImageIcon("pic1.jpg");
		picturn.setIcon(picpic);
	}
	
	int o = 0;//按按鈕的次數
	
	public void actionPerformed(ActionEvent e) 
	{
		if(o%4==0)
		{
			ImageIcon testIcon = new ImageIcon("pic2.jpg");
			picturn.setIcon(testIcon);
		}
		else if(o%4==1)
		{
			ImageIcon testIcon = new ImageIcon("pic3.jpg");
			picturn.setIcon(testIcon);
		}
		else if(o%4==2)
		{
			ImageIcon testIcon = new ImageIcon("pic4.jpg");
			picturn.setIcon(testIcon);
		}
		else if(o%4==3)
		{
			ImageIcon testIcon = new ImageIcon("pic1.jpg");
			picturn.setIcon(testIcon);	
		}		
		o++;	
	}
}