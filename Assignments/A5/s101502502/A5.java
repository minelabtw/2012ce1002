package ce1002.A5.s101502502;

import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;

public class A5 extends JFrame implements ActionListener
{
	private ImageIcon picIcon = new ImageIcon("src/ce1002/A5/s101502502/test.jpg");
	private ImageIcon picIcon1 = new ImageIcon("src/ce1002/A5/s101502502/test1.jpg");
	private ImageIcon picIcon2 = new ImageIcon("src/ce1002/A5/s101502502/test2.jpg");
	private ImageIcon picIcon3 = new ImageIcon("src/ce1002/A5/s101502502/test3.jpg");//讀取四張圖
	private JButton picButton = new JButton(picIcon);//把原始圖放在button上
	private int a = 0;//用來計算點了幾下button
	
	public A5()
	{
		setLayout(new BorderLayout(100,100));
		add(picButton, BorderLayout.CENTER);//把button放在中間
		picButton.addActionListener(this);
	}
	
	public static void main(String[] args)
	{
		A5 frame = new A5();
		frame.setTitle("A5");
		frame.setSize(500,500);
		frame.setLocationRelativeTo(null);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setVisible(true);
	}

	public void actionPerformed(ActionEvent arg0)
	{
		a++;
		
		if(a%4==1)
		{
			picButton.setIcon(picIcon1);//換成90度的圖
		}
		else if(a%4==2)
		{
			picButton.setIcon(picIcon2);//換成180度的圖
		}
		else if(a%4==3)
		{
			picButton.setIcon(picIcon3);//換成270度的圖
		}
		else if(a%4==0)
		{
			picButton.setIcon(picIcon);//換成轉回原位的圖
		}
	}
}
