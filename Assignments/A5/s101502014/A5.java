package ce1002.A5.s101502014;
import javax.swing.*; //引入GUI需要的功能
import java.awt.event.ActionEvent; //引入按鈕的事件功能
import java.awt.event.ActionListener; //引入按鈕的反應功能
public class A5 extends JFrame implements ActionListener{
	private ImageIcon image = new ImageIcon("src/ce1002/A5/s101502014/test.jpg"); //引入圖片到image
	private	JButton btn = new JButton(image); //設定btn按鈕的圖案為image
	int count = 0; //count用來判斷使用者點擊的次數
	public static void main(String[] args) {
		A5 frame = new A5("A5"); //建立視窗
		frame.setSize(300, 300); //設定視窗大小
		frame.setLayout(null); //不使用版面設定
		frame.setLocationRelativeTo(null); //固定視窗位置出現在螢幕的中間
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setVisible(true); //讓視窗出現
	}
	public A5(String pTitle){
		super(pTitle);
		setLayout(null); //不使用版面設定 
		btn.setBounds(40 , 30 , 200 , 200); //設定btn按鈕的位置
		btn.addActionListener(this); //把btn按鈕加入反應的功能
		add(btn); //加入btn按鈕到視窗中
	}
	public void actionPerformed(ActionEvent e){
		count++; //當偵測到按鈕被按下 , 點擊次數count加一
		if(count % 4 == 1) //當點擊次數除以四餘一 , 則把圖片路徑改為向右旋轉90度的圖片路徑
			btn.setIcon(new ImageIcon("src/ce1002/A5/s101502014/test1.jpg"));
		else if(count % 4 == 2) //當點擊次數除以四餘二 , 則把圖片路徑改為向右旋轉180度的圖片路徑
			btn.setIcon(new ImageIcon("src/ce1002/A5/s101502014/test2.jpg"));
		else if(count % 4 == 3) //當點擊次數除以四餘三 , 則把圖片路徑改為向右旋轉270度的圖片路徑
			btn.setIcon(new ImageIcon("src/ce1002/A5/s101502014/test3.jpg"));
		else  //當點擊次數除以四餘零 , 則把圖片路徑改為原本的圖片路徑
			btn.setIcon(new ImageIcon("src/ce1002/A5/s101502014/test.jpg"));
	}
}