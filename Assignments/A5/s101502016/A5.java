package ce1002.a5.s101502016;

import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;

public class A5 implements ActionListener {


 	private ImageIcon image1 ;
	private ImageIcon image2 ;
	private ImageIcon image3 ;
	private ImageIcon image4 ;
	private static JButton turn;
	
	public A5()
	{
		image1 = new ImageIcon("test1.jpg");
		image2 = new ImageIcon("test2.jpg");
		image3 = new ImageIcon("test3.jpg");
		image4 = new ImageIcon("test4.jpg");
		turn = new JButton(image1);
		
	}


	public int counter = 1; 
	public static void main(String[] args) {
		
		
		JFrame frame = new JFrame("A5");
		frame.setLayout(new FlowLayout());
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setSize(300, 300);

		A5 t = new A5();
		
		frame.add(turn);
		turn.addActionListener(t);
		frame.setVisible(true);


	}

	@Override
	public void actionPerformed(ActionEvent arg0) {
		// TODO Auto-generated method stub
		
		counter++;
		if (counter%4==1)
			turn.setIcon(image1);
		else if (counter%4==2)
			turn.setIcon(image2);
		else if (counter%4==3)
			turn.setIcon(image3);
		else if (counter%4==0)
			turn.setIcon(image4);
	}


}