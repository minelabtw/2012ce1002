package ce1002.A5.s101502017;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.*;

@SuppressWarnings("serial")
public class A5 extends JFrame implements ActionListener {
	private JButton button;
	int count = 0;
	
	public A5(){
		super("A5");
		setSize(300,300);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		ImageIcon imageIcon = new ImageIcon("test.jpg");
		
		button =new JButton(imageIcon);//把圖片當作1個Button
		
		button.addActionListener(this);
		
		add(button);
		
		setVisible(true);
	}
	
	public static void main(String[] args) 
	{
		new A5();
	}
	
	public void actionPerformed(ActionEvent e) 
	{
		count++;
		if(e.getSource()==button && count%4==1)
		{
			ImageIcon imageIcon1 = new ImageIcon("test1.jpg");
			button.setIcon(imageIcon1);//換圖1
			
		}
		else if(e.getSource()==button && count%4==2)
		{
			ImageIcon imageIcon2 = new ImageIcon("test2.jpg");
			button.setIcon(imageIcon2);//換圖2
			
		}
		else if(e.getSource()==button && count%4==3)
		{
			ImageIcon imageIcon3 = new ImageIcon("test3.jpg");
			button.setIcon(imageIcon3);//換圖3
			

		}
		else if(e.getSource()==button && count%4==0)
		{
			ImageIcon imageIcon = new ImageIcon("test.jpg");
			button.setIcon(imageIcon);//回原本的圖

		}
		
	}
}
	
	
