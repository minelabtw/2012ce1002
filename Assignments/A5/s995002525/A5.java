package ce1002.A5.s995002525;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.*;

public class A5 extends JFrame implements ActionListener {
	private JButton btn1 = new JButton(new ImageIcon("test1.jpg"));
	private JButton btn2 = new JButton(new ImageIcon("test2.jpg"));
	private JButton btn3 = new JButton(new ImageIcon("test3.jpg"));
	private JButton btn4 = new JButton(new ImageIcon("test4.jpg"));
	private int i = 1;
	public static void main(String[] args)throws Exception {
		A5 hw = new A5("A5");
		hw.setSize(280,300);
		hw.setLocation(50,50);
		hw.setVisible(true);
	}
	public A5(String pTitle) {
		super(pTitle);
		setLayout(null);// 不使用版面配置		
		btn1.setBounds(30,30,200,200);//x y length weight
		btn2.setBounds(30,30,200,200);
		btn3.setBounds(30,30,200,200);
		btn4.setBounds(30,30,200,200);
		btn1.addActionListener(this);//this class
		btn2.addActionListener(this);
		btn3.addActionListener(this);
		btn4.addActionListener(this);
		add(btn1);
		add(btn2);
		add(btn3);
		add(btn4);
		btn2.hide();
		btn3.hide();
		btn4.hide();
	}
	public void actionPerformed(ActionEvent e) {
		if(i==1){
			btn1.hide();
			btn2.show();
			//System.out.println("1");
			i=2;
		}
		else if(i==2){
			btn2.hide();
			btn3.show();
			//System.out.println("2");
			i=3;
		}
		else if(i==3){
			btn3.hide();
			btn4.show();
			//System.out.println("3");
			i=4;
		}
		else if(i==4){
			btn4.hide();
			btn1.show();
			//System.out.println("4");
			i=1;
		}
		
	}
}

