package ce1002.A5.s101502004;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.*;

public class A5 extends JFrame implements ActionListener{
	private JButton Bu = new JButton(new ImageIcon("test.jpg"));//一共設四個按鈕放上四種圖片
	private JButton Bu2 = new JButton(new ImageIcon("test2.jpg"));
	private JButton Bu3 = new JButton(new ImageIcon("test3.jpg"));
	private JButton Bu4 = new JButton(new ImageIcon("test4.jpg"));
	
	public A5(){
		add(Bu);//先將第一個按鈕add進去
		Bu.addActionListener(this);//所有按鈕addAction
		Bu2.addActionListener(this);
		Bu3.addActionListener(this);
		Bu4.addActionListener(this);
	}
	
	public static void main(String[] args){
		A5 frame = new A5();//將視窗設定好
		frame.setTitle("A5");
		frame.setSize(800, 600);
		frame.setLocationRelativeTo(null);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setVisible(true);
	}
	
	public void actionPerformed(ActionEvent e){
		if(e.getSource()==Bu){//按了Bu後要顯示Bu2
			this.remove(Bu);//先將Bu消掉
			this.add(Bu2);//將Bu2add進去
			this.validate();//重排
			repaint();//重畫
			
		}
		else if(e.getSource()==Bu2){//按了Bu2後要顯示Bu3
			this.remove(Bu2);
			this.add(Bu3);
			this.validate();
			repaint();
		}
		else if(e.getSource()==Bu3){//按了Bu3後要顯示Bu4
			this.remove(Bu3);
			this.add(Bu4);
			this.validate();
			repaint();
		}
		else if(e.getSource()==Bu4){//按了Bu4後要顯示Bu
			this.remove(Bu4);
			this.add(Bu);
			this.validate();
			repaint();
		}
	}
}
