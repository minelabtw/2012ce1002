// I have to do the Bonus for the assignment

package ce1002.A5.s101502509;

import java.awt.*;
import java.awt.event.*;
import java.awt.image.BufferedImage;
import java.io.File;
import javax.imageio.ImageIO;
import javax.swing.*;
												//BufferImage extends image
public class A5 extends JFrame{				//image
	private ImageIcon inputFile = new ImageIcon(paint());	//create and send imageTmp to imageIcon of the button
	JButton graph = new JButton(inputFile);
	int clickCount = 0;		//record the click times
	
	public A5() {
		setUI();
	}
	
	private BufferedImage paint() {		//read the picture file into imageTmp  
	    BufferedImage imageTmp = null;
	    try {								//image
	    	imageTmp = ImageIO.read(new File("test.jpg"));
	    }
	    catch (Exception ex) {
	        System.out.println("No picture!!");	//if it fails to read the file,print"No picture!!"
	    }
	    return imageTmp;
	}
	
	public BufferedImage rotate(ImageIcon inputFile) {
		clickCount++;
		
		int imageWidth = inputFile.getIconWidth();
		int imageHeight = inputFile.getIconHeight();
		BufferedImage image = (BufferedImage)inputFile.getImage();	//change the class of ImageIcon into "BufferedImage" 
		BufferedImage newImage = new BufferedImage(imageWidth, imageHeight, image.getType());	//create a new BufferImage
		Graphics2D goal = (Graphics2D)newImage.createGraphics();
		
		goal.rotate(Math.toRadians(90*clickCount), imageHeight/2, imageWidth/2);
		goal.drawImage(image, null, 0, 0);	//draw newImage
		return newImage;
	}
 
	public void setUI() {
		graph.addActionListener(new graphActionListener());
		add(graph);
		setTitle("A5");
		setSize(400, 300);
		setLocationRelativeTo(null);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setVisible(true);
	}
	
	public class graphActionListener implements ActionListener {
		@Override
		public void actionPerformed(ActionEvent e) {
			// TODO Auto-generated method stub
			System.out.println("rotate");
			graph.setIcon(new ImageIcon(rotate(inputFile)));	//update the imageIcon of the button 
		}
	}
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		new A5();
	}
}
