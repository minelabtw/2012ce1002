package ce1002.A5.s101502522;
import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
public class A5 extends JFrame implements ActionListener
{
	private JButton imgbutton = new JButton(new ImageIcon("up.jpg"));  //宣告按鈕
	private int i=0;//計數器
		
	public A5()
	{
		add(imgbutton); //放按鈕
		imgbutton.addActionListener(this);//激發按鈕功能

	}
	public static void main(String[] args)
	{
		A5 frame = new A5();
		frame.setSize(400,300);
		frame.setTitle("A5");
		frame.setLocationRelativeTo(null);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setVisible(true);
	}
	
	public void actionPerformed(ActionEvent click) //按鈕功能，以計數器來換圖片
	{
		if(i==0)
		{
			if(click.getSource()== imgbutton)
			{
				imgbutton.setIcon(new ImageIcon("right.jpg"));
				i++;
			}
		}
		else if(i==1)
		{
			if(click.getSource()== imgbutton)
			{
				imgbutton.setIcon(new ImageIcon("down.jpg"));
				i++;
			}
		}
		else if(i==2)
		{
			if(click.getSource()== imgbutton)
			{
				imgbutton.setIcon(new ImageIcon("left.jpg"));
				i++;
			}
		}
		else if(i==3)
		{
			if(click.getSource()== imgbutton)
			{
				imgbutton.setIcon(new ImageIcon("up.jpg"));
				i=0;
			}
		}
	}
 
}
