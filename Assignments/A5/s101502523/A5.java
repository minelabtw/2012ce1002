//加分題><
package ce1002.A5.s101502523;

import javax.imageio.ImageIO;
import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.awt.image.BufferedImage;
import java.io.*;

public class A5 extends JFrame implements ActionListener {	
	private JButton jbt = new JButton();
	private BufferedImage image = ImageIO.read(new File("test.jpg"));//讀取圖檔
	
	public A5() throws IOException {
		//新增圖檔按鈕
		jbt.setIcon(new ImageIcon(image));
		add(jbt);			
		jbt.addActionListener(this);//按鈕事件
	}
	
	public static void main(String[] args) throws IOException {
		//設定面板		
		A5 frame = new A5();
		frame.setTitle("A5");
		frame.setSize(300,300);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setVisible(true);	
	}
	
	public BufferedImage rotateImage(final BufferedImage bufferedimage) {
		//旋轉圖片 參考來源: http://blog.csdn.net/cping1982/article/details/2078999
        int w = bufferedimage.getWidth();
        int h = bufferedimage.getHeight();
        int type = 1;
        BufferedImage img = new BufferedImage(h, w, type);
        Graphics2D graphics2d = img.createGraphics();//Creates a Graphics2D, which can be used to draw into this BufferedImage
        graphics2d.rotate(Math.toRadians(90), w/2, h/2);//順時針旋轉90度
        graphics2d.drawImage(bufferedimage, 0, 0, null);//作圖
        graphics2d.dispose();//擺置圖片
        return img;//回傳圖像
    }
	
	public void actionPerformed(ActionEvent e) {   
		image = rotateImage(image);//圖檔變更為旋轉過後
		jbt.setIcon(new ImageIcon(image));//button圖案更新
    }
}
