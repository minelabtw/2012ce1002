//A5
//Rotate picture with JButton
//A lot of functions are used
package ce1002.A5.s100502201;

import java.awt.Graphics2D; //Draw pic
import java.awt.RenderingHints; 
import java.awt.event.ActionEvent; //Receive actions
import java.awt.event.ActionListener;
import java.awt.image.BufferedImage; //Img as variable
import java.io.File; 
import java.io.IOException;
import javax.imageio.ImageIO; //Read and write pic
import javax.swing.*;

public class A5 extends JFrame implements ActionListener {

	private static BufferedImage bi; //Buffered image as test.jpg
	public static JButton rotate = new JButton(new ImageIcon("test.jpg")); //Read test.jpg as JButton

	public static BufferedImage rotateImage(final BufferedImage bufferedimage,
			final int degree) { //Rotate pic

		int w = bufferedimage.getWidth();
		int h = bufferedimage.getHeight();
		int type = bufferedimage.getColorModel().getTransparency();
		BufferedImage img;
		Graphics2D graphics2d;
		(graphics2d = (img = new BufferedImage(w, h, type)).createGraphics())
				.setRenderingHint(RenderingHints.KEY_INTERPOLATION,
						RenderingHints.VALUE_INTERPOLATION_BILINEAR);
		graphics2d.rotate(Math.toRadians(degree), w / 2, h / 2);
		graphics2d.drawImage(bufferedimage, 0, 0, null);
		graphics2d.dispose();
		return img;

	}

	public static void main(String[] args) {

		try { //Read test.jpg into bi
			bi = ImageIO.read(new File("test.jpg"));
		} catch (IOException eM) {
		}
		A5 A5 = new A5("A5");
		A5.setSize(350, 350);
		A5.setLocation(250, 250);
		A5.setVisible(true);

	}

	public A5(String pTitle) { //Frame constructor

		super(pTitle);
		setLayout(null);
		rotate.setBounds(50, 50, 200, 200);
		add(rotate);
		rotate.addActionListener(this);

	}

	public void actionPerformed(ActionEvent A) { //Action

		bi = rotateImage(bi, 90);
		remove(rotate); //We have to remove and add to make memory clear so we won't load the same pic again
		rotate = new JButton(new ImageIcon(bi));
		rotate.setBounds(50, 50, 200, 200);
		add(rotate);
		rotate.addActionListener(this); //Reconstruct an action-listener and make it recursive since we've removed an object

	}

}
