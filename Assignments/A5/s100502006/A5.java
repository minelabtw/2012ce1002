/********************************************************/
/*                                                      */
/*         need only one image, not need four           */
/*                                                      */
/********************************************************/
package ce1002.A5.s100502006;

import java.awt.Graphics2D;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.image.BufferedImage;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;

import javax.imageio.ImageIO;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;

public class A5 extends JFrame{
	
	//declare components
	private JButton jbt;
	private static BufferedImage currentImage;
	private BufferedImage nextImage;
	private ImageIcon icon;
	
	public A5() throws FileNotFoundException, IOException{//class constructor
		
		//add original image to the JButton
		currentImage=ImageIO.read(new FileInputStream("test.jpg"));
		icon=new ImageIcon(currentImage);
		jbt=new JButton(icon);
		add(jbt);
		
		//draw next image (rotate the origin image) before you can click on the button
		//to reduce the execute time in need for changing the image at the click
		nextImage=new BufferedImage(currentImage.getHeight(), currentImage.getWidth(), currentImage.getType());
		Graphics2D graphics = (Graphics2D) nextImage.getGraphics();
		graphics.rotate(Math.toRadians(90), nextImage.getWidth() / 2, nextImage.getHeight() / 2);
		//move the image to the correct place
		graphics.translate((nextImage.getWidth() - currentImage.getWidth()) / 2, (nextImage.getHeight() - currentImage.getHeight()) / 2);
		//draw image according to the origin image
		graphics.drawImage(currentImage, 0, 0, currentImage.getWidth(), currentImage.getHeight(), null);
		
		jbt.addActionListener(new ButtonListener());//Action listener to the JButton
		
	}
	
	private class ButtonListener implements ActionListener{

		@Override
		public void actionPerformed(ActionEvent e) {
			// TODO Auto-generated method stub
			
			//change the icon on the button to the next image at next click
			currentImage=nextImage;
			icon=new ImageIcon(currentImage);
			jbt.setIcon(icon);
			
			//draw next image (rotate the origin image) before next click
			//to reduce the execute time in need for changing the image
			nextImage=new BufferedImage(currentImage.getHeight(), currentImage.getWidth(), currentImage.getType());
			Graphics2D graphics = (Graphics2D) nextImage.getGraphics();
			graphics.rotate(Math.toRadians(90), nextImage.getWidth() / 2, nextImage.getHeight() / 2);
			//move the image to the correct place
			graphics.translate((nextImage.getWidth() - currentImage.getWidth()) / 2, (nextImage.getHeight() - currentImage.getHeight()) / 2);
			//draw image according to the origin image
			graphics.drawImage(currentImage, 0, 0, currentImage.getWidth(), currentImage.getHeight(), null);
			
		}
		
	}
	
	public static void main(String[] args) throws FileNotFoundException, IOException {
		
		//create and set a frame
		A5 frame=new A5();
		frame.setTitle("A5");
		//set a proper size of the frame according to the image
		if(currentImage.getWidth()>=currentImage.getHeight()){
			frame.setSize((currentImage.getWidth()+100)*4/3, currentImage.getWidth()+100);
		}
		else{
			frame.setSize((currentImage.getHeight()+100)*4/3, currentImage.getHeight()+100);
		}				
		frame.setLocationRelativeTo(null);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setVisible(true);
		
	}
	
}