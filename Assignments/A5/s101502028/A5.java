package ce1002.A5.s101502028;

import javax.imageio.*;
import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.awt.image.*;
import java.io.*;

public class A5 extends JFrame implements ActionListener {
	public BufferedImage readImage() {
		BufferedImage image = null; // buffer space for the image
		try {
			image = ImageIO.read(new File("test.jpg")); // put the read image on the buffer
		} catch (IOException e) {
		}
		return image;
	}

	private ImageIcon testIcon = new ImageIcon(readImage()); // take the read image
	private JButton btn = new JButton(testIcon); // set the read image on the button
	private int angle = 0; // angle

	public static void main(String[] args) {
		A5 frame = new A5(); // make a window
		frame.setSize(400, 300);
		frame.setLocationRelativeTo(null);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setVisible(true);
	}

	public A5() {
		setTitle("A5"); // the title of the window
		btn.addActionListener(this); // actionListener of the button
		add(btn); // add the button to the window
	}

	public void actionPerformed(ActionEvent e) {
		btn.setIcon(new ImageIcon(rotate(testIcon))); // action of the button to rotate
	}

	public BufferedImage rotate(ImageIcon test) {
		angle += 90; // add angle 90
		int width = test.getIconWidth(); // count its width
		int height = test.getIconHeight(); // count its height
		BufferedImage bimage = (BufferedImage) test.getImage(); // put the read image on the buffer
		BufferedImage rotatedImage = new BufferedImage(width, height, bimage.getType()); // create a buffer to put the rotated image
		Graphics2D g = (Graphics2D) rotatedImage.createGraphics(); // convert the image into graphics2d
		g.rotate(Math.toRadians(angle), width / 2, height / 2); // apply the rotation
		g.drawImage(bimage, null, 0, 0); // draw the applied image on the new buffer
		return rotatedImage; // return the rotated image
	}
}
