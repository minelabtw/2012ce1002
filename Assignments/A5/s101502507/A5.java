package ce1002.A5.s101502507; //加分版

import java.awt.*;
import java.awt.event.*;
import java.awt.geom.AffineTransform;
import javax.swing.*;
import javax.swing.plaf.IconUIResource;

public class A5 implements ActionListener 
{
	public static int i = 0;
	public static ImageIcon picIcon = new ImageIcon("test.jpg");//圖片
	public static JButton btn = new JButton();
	
	public static void main(String[] args) 
	{
		JFrame A51 = new JFrame("A51");//視窗標題
		A51.setSize(400, 400);//視窗大小
		A51.setVisible(true);
		A5 h = new A5();
		btn.setIcon(picIcon);//裝圖片
		btn.addActionListener(h);
		A51.add(btn);//裝按鈕
	}
	
	public void actionPerformed(ActionEvent e) //利用按滑鼠次數除以4之後的餘數做判斷
	{
		if (i%4==0) 
			btn.setIcon(new RotateIcon(picIcon, 90));	//圖片轉90度
		else if (i%4==1) 
			btn.setIcon(new RotateIcon(picIcon, 180));	//圖片轉180度
		else if (i%4==2) 
			btn.setIcon(new RotateIcon(picIcon, 270));	//圖片轉270度	
		else if (i%4==3)  
			btn.setIcon(new RotateIcon(picIcon, 0));	//圖片轉0度
		i++;//計算點擊滑鼠次數
	}
	class RotateIcon extends IconUIResource //旋轉的程式是查網路的^_^
	{
		private static final long serialVersionUID = 1L;
		private int degree; //旋轉角度
		
		public RotateIcon(Icon icon, int degree) 
		{
			super(icon);
			this.degree=degree;
		}
		
		public void paintIcon(Component m, Graphics j, int x, int y) 
		{
			Graphics2D k = (Graphics2D) j;
			AffineTransform atf = k.getTransform();
			AffineTransform clone = (AffineTransform) atf.clone();
			atf.rotate(Math.PI/180*degree, x+getIconWidth()/2, y+getIconHeight()/2);
			k.setTransform(atf);
			super.paintIcon(m, j, x, y);
			k.setTransform(clone);
		}
	}
}