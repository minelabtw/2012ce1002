package ce1002.A5.s101502521;
//�����[��
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.geom.AffineTransform;
import java.awt.image.AffineTransformOp;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;

public class A5 extends JFrame {
	JButton btn;
	BufferedImage bi;
	public static void main(String[] args) throws IOException
	{
		A5 ins = new A5();
		ins.setVisible(true);
	}
	A5() throws IOException
	{
		super("A5");
		this.setDefaultCloseOperation(EXIT_ON_CLOSE);
		this.setLayout(null);
		this.setSize(500,500);
		bi = ImageIO.read(new File("test.jpg"));
		btn = new JButton(new ImageIcon(bi));
		btn.setBounds(0,0,500,500);
		add(btn);
		btn.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				// TODO Auto-generated method stub
				bi = rotate(bi);
				btn.setIcon(new ImageIcon(bi));
			}
		});
		// end of constructor
	}
	
	BufferedImage rotate(BufferedImage img)
	{
		AffineTransform at = new AffineTransform();
		at.rotate(Math.PI/2, img.getWidth()/2,img.getHeight()/2);// rotate pi/2 (90 degree)
		//at.rotate(Math.PI/4);
		AffineTransformOp op = new AffineTransformOp(at, AffineTransformOp.TYPE_BILINEAR);
		BufferedImage rtn = op.filter(img, null);
		return rtn;
	}
}
