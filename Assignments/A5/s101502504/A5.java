package ce1002.A5.s101502504;
import java.awt.*;
import java.awt.event.*;
import java.awt.image.*;
import javax.swing.*;

public class A5 extends JFrame implements ActionListener 
{
	private static ImageIcon icon1 = new ImageIcon("src//ce1002//A5//s101502504//test1.jpg");
	private static ImageIcon icon2 = new ImageIcon("src//ce1002//A5//s101502504//test2.jpg");
	private static ImageIcon icon3 = new ImageIcon("src//ce1002//A5//s101502504//test3.jpg");
	private static ImageIcon icon4 = new ImageIcon("src//ce1002//A5//s101502504//test4.jpg");
	private static JButton jbt = new JButton(icon1);
	private static int count=0;//determine which photo to change
	
	public static void main(String[] args)
	{
		A5 frame = new A5();
		frame.setTitle("A5");
		frame.setSize(500, 350);//set size
		frame.setLocationRelativeTo(null);
		frame.setVisible(true);
	}
	
	public A5()
	{
		setLayout(new GridLayout(1,4,5,5));
		add(jbt);//add button jbt
		jbt.addActionListener(this);//action
	}

	public void actionPerformed(ActionEvent e)
	{
		if(e.getSource()==jbt)//if user push button
			count++;
		////////////////////////////turn////////////////////////////
		if(count%4==0)
			jbt.setIcon(icon1);
		if(count%4==1)
			jbt.setIcon(icon2);
		if(count%4==2)
			jbt.setIcon(icon3);
		if(count%4==3)
			jbt.setIcon(icon4);
	}
	
}