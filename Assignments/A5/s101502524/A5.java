//加分題
package ce1002.A5.s101502524;

import javax.imageio.ImageIO;
import javax.swing.*;
import java.awt.Graphics2D;
import java.awt.RenderingHints;
import java.awt.event.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;


public class A5 extends JFrame implements ActionListener
{
	private JButton btn;
	private BufferedImage img;
	public static void main(String[] args) throws IOException
	{
		A5 h = new A5("shake that");//設定視窗名稱
		h.setSize(400,300);//設定視窗大小
		h.setVisible(true);//設定使視窗可被看見
		h.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	}
	public A5(String p) throws IOException
	{
		super(p);
		img=ImageIO.read(new File("test.jpg"));//讀取圖片
		btn=new JButton(new ImageIcon(img));
		btn.addActionListener(this);
		add(btn);//新增按鈕
	}
	public void actionPerformed(ActionEvent e)//點擊按鈕後反應
	{
		img=rotateImage(img);
		btn.setIcon(new ImageIcon(img));//將轉好的圖片存回btn
	}
	public static BufferedImage rotateImage(BufferedImage bimage)
	{
		//將圖片順時鐘旋轉90度，參考自http://blog.csdn.net/cping1982/article/details/2078999
        int w = bimage.getWidth();
        int h = bimage.getHeight();
        int type = 1;
        BufferedImage img= new BufferedImage(h, w, type);//設定一個新的圖片
        Graphics2D graphics2d;
        graphics2d = img.createGraphics();//將img傳入graphic2d
        graphics2d.rotate(Math.toRadians(90), h / 2, h / 2);//轉90度
        graphics2d.drawImage(bimage, 0, 0, null);//將更改後的bimage畫到img
        graphics2d.dispose();
        return img;
	}
}