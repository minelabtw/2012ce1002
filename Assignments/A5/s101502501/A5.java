package ce1002.A5.s101502501;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.*;

public class A5 extends JFrame implements ActionListener
{
	private int count = 0;//用來計算按了幾次
	private ImageIcon sasaIcon = new ImageIcon("test.jpg");//讀一張圖進來
	private JButton sasaBtn = new JButton(sasaIcon);//弄一個按鈕,上面放圖
	public A5(){
		setLayout(null);
		add(sasaBtn);
		sasaBtn.setBounds(0, 0,290, 290);//設定按鈕大小
		sasaBtn.addActionListener(this);//按一下執行下面的動作
	}
	
	public static void main(String[] args){
		A5 frame = new A5();

		frame.setTitle("A51");
		frame.setSize(300,300);
		frame.setLocationRelativeTo(null);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setVisible(true);
		
	}

	public void actionPerformed(ActionEvent e) {
			
			if(e.getSource()==sasaBtn){
				count++;//按一下count加1
				if(count%4==1){//按一下換一張圖
					ImageIcon sasaIcon = new ImageIcon("src/ce1002/A5/s101502501/test1.jpg");
					sasaBtn.setIcon(sasaIcon);
				}
				else if(count%4==2){//按一下換一張圖
					ImageIcon sasaIcon = new ImageIcon("src/ce1002/A5/s101502501/test2.jpg");
					sasaBtn.setIcon(sasaIcon);
				}
				else if(count%4==3){//按一下換一張圖
					ImageIcon sasaIcon = new ImageIcon("src/ce1002/A5/s101502501/test3.jpg");
					sasaBtn.setIcon(sasaIcon);
				}
				else{//按一下換一張圖
					ImageIcon sasaIcon = new ImageIcon("src/ce1002/A5/s101502501/test.jpg");
					sasaBtn.setIcon(sasaIcon);
				}
			}
	}
}
