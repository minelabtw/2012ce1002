/**no bonus**/

package ce1002.A5.s101502508;

import javax.swing.* ;
import java.awt.* ;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class A5 extends JFrame implements ActionListener{
	
	private ImageIcon icon = new ImageIcon("src/ce1002/A5/s101502508/test.jpg") ;
	private ImageIcon icon2 = new ImageIcon("src/ce1002/A5/s101502508/test2.jpg") ;
	private ImageIcon icon3 = new ImageIcon("src/ce1002/A5/s101502508/test3.jpg") ;
	private ImageIcon icon4 = new ImageIcon("src/ce1002/A5/s101502508/test4.jpg") ;
	private int counter=0 ;
	
	JButton btn = new JButton(icon) ;
	
	public A5() {

		setLayout( new GridLayout(1, 0) ) ;//版面配置
		btn.addActionListener(this);
		add(btn) ;
	}
	
	public void actionPerformed(ActionEvent e) {
		//四次輪轉的方式，利用counter計數
		if (counter%4==0)
		    btn.setIcon(icon2) ;
		if (counter%4==1)
		    btn.setIcon(icon3) ;
		if (counter%4==2)
		    btn.setIcon(icon4) ;
		if (counter%4==3)
		    btn.setIcon(icon) ;

		counter++ ;
	}
	
	public static void main (String[] args){
		
		A51 frame = new A51() ;
		
		frame.setTitle("A5") ;
		frame.setSize(400,300) ;
		frame.setLocationRelativeTo(null) ;
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE) ;
		frame.setVisible(true) ;	
		
	}
}
