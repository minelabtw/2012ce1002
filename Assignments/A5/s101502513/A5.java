package ce1002.A5.s101502513;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class A5 extends JFrame implements ActionListener {
	private ImageIcon Icon0 = new ImageIcon("test0.jpg"); //first picture
	private ImageIcon Icon90 = new ImageIcon("test90.jpg"); //second picture
	private ImageIcon Icon180 = new ImageIcon("test180.jpg"); //third picture
	private ImageIcon Icon270 = new ImageIcon("test270.jpg"); //fourth picture
	private JButton clockwise = new JButton(Icon0); //Beginning, button's pattern is Icon0
	private static int i = 0; //counter
	
	public A5() {
		setLayout(new GridLayout(1, 4, 5, 5));
		add(clockwise);
		clockwise.addActionListener(this);
	}
	
	public static void main(String[] args) {
		A5 frame = new A5();
		frame.setTitle("A5");
		frame.setSize(300, 300);
		frame.setLocationRelativeTo(null);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setVisible(true);
	}
	
	//rotate 90 degrees closewise if you press the button
	public void actionPerformed(ActionEvent e) {
		i++;
		if( i == 1 )
			clockwise.setIcon(Icon90);
		else if( i == 2 )
			clockwise.setIcon(Icon180);
		else if( i == 3 )
			clockwise.setIcon(Icon270);
		else if( i == 4 ) {
			clockwise.setIcon(Icon0);
			i = 0; //let i initialize
		}
	}
}
