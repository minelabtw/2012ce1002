package ce1002.A5.s101502002;
import javax.swing.*;
import java.awt.event.*;
public class A5 extends JFrame implements ActionListener{
	private ImageIcon pic = new ImageIcon("test.jpg"); //第一張圖
	private ImageIcon pic2 = new ImageIcon("test2.jpg"); //第二張圖 
	private ImageIcon pic3 = new ImageIcon("test3.jpg"); //第三張圖
	private ImageIcon pic4 = new ImageIcon("test4.jpg"); //第四張圖
	private JButton one = new JButton(pic); //一個按鈕
	private int click =0; //計算按了幾下
	public A5 (){
		one.addActionListener(this); //讓按鈕做動作
		add(one); //把按鈕加到frame上
	}
	public static void main(String[]args){ //主程式
		A5 frame = new A5();
		frame.setTitle("A5"); //狀態列顯示
		frame.setSize(400,300); //設定長寬
		frame.setLocationRelativeTo(null); //放在中間
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE); //按叉叉關閉
		frame.setVisible(true); //可以看見視窗
	}
	@Override
	public void actionPerformed(ActionEvent e) {
		if(e.getSource()==one){ //點下按鈕
			click++; //加一次
			if(click%4==0) //按4的倍數次的時候
				one.setIcon(pic);
			if(click%4==1) //按1,5,9....次(餘一的時候)
				one.setIcon(pic2);				
			if(click%4==2) //按2,6,10....次(餘2的時候)
				one.setIcon(pic3);
			if(click%4==3) //按3,7,11....次(餘3的時候)
				one.setIcon(pic4);	
		}
		// TODO Auto-generated method stub
	}
}
