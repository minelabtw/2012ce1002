package ce1002.A2.s101502004;
import java.util.Scanner;

public class A22 {
	public static void main(String[] args){
		Scanner inp=new Scanner(System.in);
		System.out.print("Array:");
		int[] arr = new int[10];//輸入陣列
		for(int i=0;i<10;i++){
			arr[i]=inp.nextInt();
		}
		int [] temp = new int[10];//輸出陣列
		int t=0;//輸出陣列的長度
		for(int i=0;i<10;i++){
			boolean repeatflag=true;//檢查是否重複
			for(int j=0;j<=t;j++){
				if(arr[i]==temp[j]){//若輸入陣列有元素和輸出陣列相同則break
					repeatflag=false;
					break;
				}
			}
			if(repeatflag==true){//輸出與輸入不重複，則輸出陣列新增元素
				temp[t]=arr[i];
				t++;
			}
		}
		System.out.print("Eliminated array:");
		for(int i=0;i<t;i++){
			System.out.print(temp[i]+" ");
		}
	}
}
