package ce1002.A2.s101502004;
import java.util.Scanner;

public class A21 {
	public static void main(String[] args){
		Scanner inp=new Scanner(System.in);
		System.out.println("lower bound=");
		int low=inp.nextInt();
		System.out.println("upper bound=");
		int up=inp.nextInt();
		for(int i=low;i<=up;i++){//從low開始檢查至up
			boolean checkflag=true;//若是false則為合數，true為質數
			if(i==1)//若low為i，從2開始檢查
				continue;
			for(int j=2;j<i;j++){//從2開始去除
				if(i%j==0){//若有一數整除
					checkflag=false;//則為合數
					break;
				}
			}
			if(checkflag==true)
				System.out.print(i+" ");
		}
	}
}
