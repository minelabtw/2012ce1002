package ce1002.A2.s101502513;

import java.util.Scanner;

public class A21 {
	public static void main(String[] args) {
		int L, U;
		Scanner input = new Scanner(System.in);
		
		System.out.print("lower bound=");
		L = input.nextInt();
		System.out.print("upper bound=");
		U = input.nextInt();
		
		while( L <= U ) {
			boolean Prime = true;
			
			if( L == 1 ) //1 isn't a prime number
				Prime = false;
			else //search for prime number between L and U
				for( int i = 2; i <= Math.sqrt(L) ; i++ ) {
					if( L % i == 0 ) {
						Prime = false;
						break;
					}
				}
			
			if( Prime == true )
				System.out.print(L + " ");
			
			L++;
		}
	}
}
