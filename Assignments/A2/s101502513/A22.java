package ce1002.A2.s101502513;

import java.util.Scanner;

public class A22 {
	public static void main(String[] args) {
		int x, y = 0;
		int[] array = new int[10];
		Scanner input = new Scanner(System.in);
		
		System.out.print("Array:");
		//input 10 times
		for( int a = 0; a < 10; a++ ) {
			int z;
			
			x = input.nextInt();
			for( z = 0; z < y; z++ ) { //confirm input doesn't repeat
				if( x == array[z] )
					break;
			}
			if( z == y ) { //if input doesn't repeat, deposit
				array[y] = x;
				y++;
			}
		}
		
		System.out.print("Eliminated array:");
		for( int i = 0; i < y; i++ ) {
			System.out.print(array[i] + " ");
		}
	}
}
