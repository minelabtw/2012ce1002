// 第二個輸出結果有排序
package ce1002.A2.s101502516;
import java.util.Scanner;
import java.util.Arrays;
public class A22 {
	public static void main (String []args){
		int [] a = new int [10];
		int [] b = new int [10];
		
		Scanner input = new Scanner(System.in); // 輸入一個長度為10的陣列
		System.out.print( "Array:" );
		for ( int i = 0; i < 10; i++ )
		{
			a[i] = input.nextInt();
		}
		
		int j = 0;
		int n;
		for ( int i = 1; i < 10; i++ ) // 判斷是否重複，沒重複則將數字輸入到b陣列中
		{
			int x = 1;
			b[0] = a[0];
			n = i;
			while ( 0 <= n - 1 )
			{
				if ( a[n - 1] != a[i] )
				{
					x = 0;
				}
				else
				{
					x = 1;
					break;
				}
				n--;
			}
			if ( x == 0 )
			{
				j++;
				b[j] = a[i];
			}
		}
		
		System.out.print( "Eliminated array:" ); 
		for ( int i = 0; i < j + 1; i++  )
		{
			System.out.print( b[i] + " " );
		}
		
		int [] plus = new int [j+1];
		for ( int i = 0; i < plus.length; i++ )
		{
			plus[i] = b[i];
		}
		Arrays.sort(plus); // 將陣列內的數字由小排到大
			
		System.out.println( " " );
		System.out.print( "Eliminated array(plus):" );
		for ( int i : plus )
		{
			System.out.print( i + " " );
		}
	}
}
