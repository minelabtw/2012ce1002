package ce1002.A2.s101502516;
import java.util.Scanner;
public class A21 {
	public static void main (String[] args){
		int lower,upper;
		
		System.out.print("lower bound=");
		Scanner input1 = new Scanner(System.in);
		lower = input1.nextInt();
		
		System.out.print("upper bound=");
		Scanner input2 = new Scanner(System.in);
		upper = input2.nextInt();
		
		for ( ; lower <= upper; lower++ ) // 找出上下限之間的質數
		{
			int x = 1;
			for ( int i = 2; i < lower; i++ )
			{
				if ( lower % i == 0 )
				{
					x = 0;
				}
			}
			if ( lower == 1 ) // 去除1
			{
				x = 0;
			}
			if ( x == 1 )
			{
				System.out.print( lower + " ");
			}
		}
	}
}
