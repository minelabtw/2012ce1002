package ce1002.A2.s101502501;
import java.util.Scanner;
public class A22 {
	public static void main(String[] args){
		Scanner cin = new Scanner(System.in);
		System.out.print("Array:");
		int[] cinArray = new int[10];
		for(int i=0; i<10; i++){//輸入長度為10的陣列
			cinArray[i] = cin.nextInt();
		}
			
		int[] ansArray = new int[10] ;//自訂一個新陣列ansArray,長度預設10(因為最大到10)
		int number = 0;//用來計算ansArray的實際長度
		for(int i=0; i<10; i++){
			boolean same = true;//用來判斷有沒有相同的數字
			for(int j=i+1; j<10; j++){
				if(cinArray[i]==cinArray[j]){//發現有相同數字	
					same = false;									
				}
			}
			if(same==true){//都沒有相同的狀況
				ansArray[number] = cinArray[i];//存進ansArray
				number++;
			}
		}
		System.out.print("Eliminated array:");
		for(int i=0; i<number; i++){
			System.out.print(ansArray[i]+" ");
		}	
	}
}
