package ce1002.A2.s101502501;
import java.util.Scanner;
public class A21 {
	public static void main(String[] args){
		Scanner cin = new Scanner (System.in);
		System.out.print("lower bound=");
		int lowerBound = cin.nextInt();
		System.out.print("upper bound=");
		int upperBound = cin.nextInt();
		
		boolean isPrime; 
		while(lowerBound <= upperBound){//當lowerBound小於等於upperBound時進入迴圈
			isPrime = true;
			if(lowerBound == 1){//1不是質數
				isPrime = false;
			}
			else{
				for(int i=2 ; i<=Math.sqrt(lowerBound); i++){//從2開始一個一個除除看
					if(lowerBound%i ==0 ){//不是質數時
						isPrime = false;
						break;
					}	
				}
			}
			if(isPrime){
				System.out.print(lowerBound+" ");
			}
			lowerBound++;
		}
	}
}
