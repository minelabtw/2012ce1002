//A2-1
//Print prime numbers out
package ce1002.A2.s100502201;

import java.util.Scanner;

public class A21 
{
	public static void main(String[] args)
	{
		Scanner input = new Scanner(System.in);
		System.out.print("lower bound=");
		int low=input.nextInt();
		Scanner input1= new Scanner(System.in);
		System.out.print("upper bound=");
		int up=input1.nextInt();
		int flag=1; //Set a flag of judging prime 
		for (int i=low;i<=up;i++)
		{
			if(i==1) //Exception of 1
				flag=0;
			else if (i==2||i==3) //2 and 3 are prime
				System.out.print(i + " ");
			else //Algorithm
			{
		        for( int j = 2 ; j <= Math.sqrt( (double) i ) ; j++ ) 
		        {
		        	flag=1;
		            if( i%j == 0 )
		            {
		                flag = 0;
		                break;
		            }
		        }
		        if(flag==1)
		            System.out.print(i + " ");
		        else
		            continue;
		    }
		}
	}
}
