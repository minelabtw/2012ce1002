//A2-2
//An easy deletion of repetitive numbers
//Set repetitive numbers to zero in order not to print these numbers
package ce1002.A2.s100502201;

import java.util.Scanner;

public class A22 
{
	public static void main(String[] args)
	{
		int[] array=new int[10]; //Set array of size 10
		System.out.print("Array:");
		Scanner input = new Scanner(System.in);
		for(int i=0;i<=9;i++)	//Continuous input
			array[i]=input.nextInt();
		for(int j=0;j<=8;j++) //Comparison
		{
			for (int k=j;k<=8;k++)
			{
				if(array[j]==array[k+1])
					array[k+1]='\0';
			}
		}
		System.out.print("Eliminated array:");
		for (int l=0;l<=9;l++)
		{
			if (array[l]==0) //Don't print out zeros
				continue;
			else
				System.out.print(array[l]+" ");
		}
	}
}
