package ce1002.A2.s101502030;

import java.util.Scanner;

public class A22 {
public static void main(String[] args){
	Scanner space=new Scanner(System.in);
	
	int[] cha=new int[10];		//10長度的陣列.
	System.out.print("Array:");
	for(int i=0;i<=9;i++)			//輸入.
	{
		cha[i]=space.nextInt();
	}
	for(int q=0;q<=9;q++)		//把重複的數字都變為0.
	{
		for(int w=q+1;w<=9;w++)
		{
			if(cha[q]==cha[w])
			{
				cha[w]=0;
			}
		}
	}
	System.out.print("Eliminated array:");	
	for(int p=0;p<=9;p++)
	{
		if(cha[p]!=0)		//輸出不是0的.
		{
			System.out.print(cha[p]+(" "));
		}
		
	}
}
}
