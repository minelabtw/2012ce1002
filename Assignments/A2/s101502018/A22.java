package ce1002.A2.s101502018;
import java.util.*;
public class A22 {
	public static void main(String[] args){
		int counter=0;
		int[] n;
		n=new int[10];
		Scanner a=new Scanner(System.in);
		System.out.print("Array:");
		for(int i=0;i<10;i++){
			n[i]=a.nextInt();
        }
		System.out.print("Eliminated array:");
		for(int i=0;i<10;i++){
			for(int j=i;j<10;j++){
				if(n[i]==n[j])
					counter++;//如果等於就加1
			}
			if(counter==1)//只有1個的數才會輸出
				System.out.print(n[i]+" ");
			counter=0;
		}
	}
}
	