package ce1002.A2.s101502018;
import java.util.Scanner;
public class A21 {
	public static void main(String[] args){
		Scanner a=new Scanner(System.in);
		boolean isPrimeFlag;
		int l,u;
		System.out.print("lower bound=");
		l=a.nextInt();
		System.out.print("upper bound=");
		u=a.nextInt();
		while(u>=l){//上限大於下限
			isPrimeFlag = true;
	        if(l==1)
	            isPrimeFlag = false;
	        else
	            for( double i=2,end=Math.sqrt(l);i<=end;i++)//每個數去算算看  看哪個是質數
	                if( l%i == 0 )
	                {
	                    isPrimeFlag = false;
	                    break;
	                }
	        if(isPrimeFlag)
	        	System.out.print(l+" ");
	        l++;
		}
	}
}
