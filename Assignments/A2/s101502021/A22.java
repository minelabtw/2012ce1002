package ce1002.A2.s101502021;
import java.util.Scanner;

public class A22 {
	public static void main(String[] args){
		//宣告一個長度為10的陣列
		int arr[] = new int[10];
		Scanner input = new Scanner(System.in);
		
		System.out.print("Array:");
		for(int i=0;i<10;i++)
			arr[i]=input.nextInt();
		
		//如果陣列中有重複的數字則將其變為n
		for(int i=0;i<10;i++)
			for(int j=i+1;j<10;j++)
				if(arr[i]==arr[j])
					arr[j]='n';
		
		System.out.print("Eliminated array:");
		//將非n的數字輸出
		for(int i=0;i<10;i++)
			if(arr[i]!='n')
				System.out.print(arr[i]+" ");	
	}

}
