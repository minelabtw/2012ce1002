package ce1002.A2.s101502021;
import java.util.Scanner;

public class A21 {
	public static void main(String[] args){
		int Start,End;
		Scanner input = new Scanner(System.in);
		
		System.out.print("lower bound=");
		Start=input.nextInt();
		System.out.print("upper bound=");
		End=input.nextInt();
		
		for(;Start<=End;Start++){
			if(check(Start))//如果是質數的話就印出來
				System.out.print(Start+" ");
			
		}	
	}


	//檢查是否為質數
	public static Boolean check(double n){
		boolean A=false;
		
		for(int x=2;x<=Math.sqrt(n);x++){
			if(n%x!=0)
				A=true;
			else{
				A=false;
			    break;
			}
		}
		if(n==2||n==3)
			A=true;
		
		return A;
	}

}
