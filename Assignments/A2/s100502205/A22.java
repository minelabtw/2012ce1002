package ce1002.A2.s100502205;

import java.util.Scanner;
import java.util.Arrays;

public class A22 {
	public static void main(String[] args) {
		Scanner cin = new Scanner(System.in);
		int[] A = new int[10];
		System.out.print("Array:");
		for (int i = 0; i < 10; i++)
			A[i] = cin.nextInt();
		Arrays.sort(A);
		System.out.print("Eliminated array:");
		for (int i = 0; i < 10; i++) {
			if (i == 0 || A[i] != A[i - 1]) {
				if (i != 0)
					System.out.printf(" ");
				System.out.printf("%d", A[i]);
			}
		}
		System.out.println();
		cin.close();
	}
}
