package ce1002.A2.s100502205;

import java.util.Scanner;

public class A21 {
	public static boolean isprime(int n) {
		if (n < 2)
			return false;
		for (int i = 2; i * i <= n; i++)
			if (n % i == 0)
				return false;
		return true;
	}

	public static void main(String[] args) {
		Scanner cin = new Scanner(System.in);
		int L, R;
		boolean firstflag = false;
		System.out.print("lower bound=");
		L = cin.nextInt();
		System.out.print("upper bound=");
		R = cin.nextInt();
		for (int i = L; i <= R; i++) {
			if (isprime(i)) {
				if (firstflag)
					System.out.printf(" ");
				firstflag = true;
				System.out.printf("%d", i);
			}
		}
		cin.close();
	}
}