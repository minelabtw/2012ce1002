package ce1002.A2.s101502526;
import java.util.Scanner;
public class A22 {

	public static void main(String[] args) {
		int[]array = new int[10] ;
		int type , time = 0;
		boolean real = false;
		Scanner input= new Scanner(System.in);
		System.out.print("Array:");
		for (int i = 0 ; i < 10 ; i++)
		{
			type = input.nextInt();            //先將輸入的放進暫存
			for (int j = 0 ; j < i ; j++)
			{
				if ( type==array[j] )          //若輸入值重複則跳到下一個輸入
				{	
					real = true;
					break ;
				}
				else
					real = false;
			}
			if (real == false)                //若都沒重複則放進陣列中，且陣列中的數量+1
			{
				array[i] = type ;
				time += 1;	
			}
			real = false;
		}
		System.out.print("Eliminated array:");	
		for (int x = 0 ; x < time ; x ++ )      //輸出陣列
			System.out.print(array[x]+" ");		
	}
}
