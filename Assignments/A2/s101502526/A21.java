package ce1002.A2.s101502526;
import java.util.Scanner;
public class A21 {

	public static void main(String[] args) {
		int low , high ;
		boolean real = false;
		Scanner input= new Scanner(System.in);
		System.out.print("lower bound=");
		low = input.nextInt();
		System.out.print("upper bound=");
		high = input.nextInt();
		for (int i = low ; i <= high ; i ++)   //從最低到最高的迴圈
		{
			if (i == 1 )                       //捨棄1
				real = true ;
			for (int j = 2 ; j < i ; j++)      //判斷此數是否為質數
			{
				if (i%j == 0)
				{
					real = true;
					break ;
				}
				else 
					real = false;
			}
			if (real == false)                 //若為質數則輸出
				System.out.print(i+" ");	
			real = false;
		}
		
	}

}
