package ce1002.A2.s101502524;

import java.util.Scanner;

public class A22 {
	public static void main(String[] args)
	{
		Scanner input = new Scanner(System.in);
		int m = 10;
		double[] a = new double[m];
		System.out.print("Array:");
		for(int i=0;i<m;i++)//把數字輸入進去陣列a[]中
		{
			a[i] =input.nextDouble();
		}
		for(int i=0;i<m;i++)//將重複的數字轉換成1/2
		{
			for(int j=i+1;j<m;j++)
			{
				if(a[i]==a[j])
				{
					a[j]=1/2;
				}
			}
		}
		
		System.out.print("Eliminated array:");
		for(int i=0;i<m;i++)
		{
			if(a[i]!=1/2)//將不是1/2的數字輸出
			{
				System.out.print((int)a[i] + " ");
			}
		}
	}
}
