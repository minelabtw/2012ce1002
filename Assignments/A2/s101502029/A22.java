package ce1002.A2.s101502029;
import java.util.*;
public class A22 {
	public static void main (String[] args){
		Scanner place = new Scanner (System.in); 
		int [] array= new int [10];
		System.out.print("Array:");
		for(int i=0; i<array.length; i++){ //輸入字串
			array[i] = place.nextInt();		
		}	
		//判斷輸入字串裡是否有重複的數字
		for(int i=0; i<array.length; i++){ //原來的字串
			boolean temp = true;  
			for(int j=0; j<i; j++){ //新的字串
				if(array[i]==array[j]){ //若新的字串和原來的字串有相同的字，
					temp=false;        //則不會把重複的字印出來
				}	 
			}
			if(temp)
				System.out.print(array[i] + " ");
		}
	}
}				
		

