package ce1002.A2.s101502029;
import java.util.*;
public class A21 {
	public static void main (String[] args){
		Scanner place = new Scanner (System.in);
		int num1,num2;
		System.out.print("lower bound=");
		num1=place.nextInt();
		System.out.print("upper bound=");
		num2=place.nextInt();	
		for(int i=num1; i<=num2; i++){ //判斷兩個數字之間的所有數字
			if(i==1){ // 1不是質數 所以要從2開始
				i=2;
			}
			boolean isPrime = false;
			for (int j = 2; j <= i/2; j++) {//判斷是否為質數
				if (i % j == 0) { 
					isPrime = true; 
					break;
				}
			}
			if(!isPrime){   
				 System.out.print(i+" "); 
			}			
		}
	}
}
	
