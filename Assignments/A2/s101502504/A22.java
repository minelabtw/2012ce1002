package ce1002.A2.s101502504;
import java.util.Scanner;
public class A22
{
	public static void main(String[] args)
	{
		Scanner input = new Scanner(System.in);
		int count=0,determine=0;
		
		final int length = 10;
		int[] array = new int[length];
		
		System.out.print("Array:");//user input ten integers
		for(int i=0; i<length; i++)//save the  ten integers into array
			array[i] = input.nextInt();

		int[] outputarray = new int[length];
		for(int i=0; i<length; i++)//initialize outputarray
			outputarray[i]=0;
		
		for(int i=0; i<length; i++)//if user input 0 
			if (array[i]==0)
			{
				outputarray[count]=0;
				count=1;
			}//end for

		System.out.print("Eliminated array:");//show
		for(int i=0; i<length; i++)
		{
			determine=0;
			for(int j=0; j<length; j++)//to check if the repeated integer
			{
				if(array[i]==outputarray[j])
				{
					determine=1;
					break;
				}
			}//end for
			
			if(determine==0)//save integers into outputarray
			{
				outputarray[count]=array[i];
				count++;
			}//end if
		}
		
		for(int i=0; i<count; i++)//shoe the final answer
			System.out.print(outputarray[i]+" ");
	}
}