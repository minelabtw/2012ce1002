package ce1002.A2.s101502504;
import java.util.Scanner;
public class A21
{
	public static void main(String[] args)
	{
		Scanner input = new Scanner(System.in);
		
		System.out.print("lower bound=");//input lower number
		int lower = input.nextInt();
		System.out.print("upper bound=");//input upper number
		int upper = input.nextInt();
	
		while(lower<=upper)
		{
			int determine=1;//determint is prime or not
			
			for(int i=2; i<=(float)Math.pow(lower,0.5);i++)
				if(lower%i==0)//not a prime
				{
					determine=0;
					break;
				}
			
			if( lower!=1 && determine==1 )//is prime
				System.out.print(lower+" ");
			lower++;//to research the next number
		}//end while
	}
}