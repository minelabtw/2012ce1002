package ce1002.A2.s101502022;
import java.util.*;

public class A21 {
	

	
	public static void main(String[] args){
		
		int a,b;
		Boolean t;
		Scanner x= new Scanner(System.in);
		
		System.out.print("lower bound=");
		a=x.nextInt();
		System.out.print("upper bound=");
		b=x.nextInt();
		
		while(a<=b){
			t=true;
		
			if(a==1)
				t=false;
			else{
				
				for(int i=2,end = (int) Math.pow(a,0.5);i<=end;i++){//pow是為了用來平方(0.5次方)
					if(a%i==0){
						t=false;
						break;
					}
				}
				if(t==true){
					System.out.print(a+" ");
				}
			}	
			a++;
		}		
		
	}

}
