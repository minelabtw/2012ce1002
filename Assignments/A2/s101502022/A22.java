package ce1002.A2.s101502022;
import java.util.*;

public class A22 {
	public static void main( String args[] ){
		final int i=10;
		int[] array = new int[i];
		
		java.util.Scanner input = new java.util.Scanner(System.in);
		System.out.print("Array:");
		for(int j=0; j<i;j++){

			array[j]= input.nextInt();
			
		}
		
		for(int x=0;x<i;x++){
			for(int y=x+1;y<i;y++){
				if(array[x]==array[y]){//將重複的數都變成跟第一個陣列一樣
					array[y]=array[0];
				}
			}
		}

		System.out.print("Eliminated array:"+array[0]+" ");
		for(int x=0;x<i;x++){//顯示跟第一個陣列不同的數
			if(array[x]!=array[0]){
				System.out.print(array[x]+" ");
			}
		}
		
	}
}

