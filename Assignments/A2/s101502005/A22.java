package ce1002.A2.s101502005;
import java.util.*;
public class A22 {
	public static void main(String[]args){
		Scanner input = new Scanner(System.in);
		int array[] = new int[10];
		
		System.out.print("Array:");
		for(int i=0;i<10;i++)//輸出與檢查必須分開
			array[i] =input.nextInt();
		
		System.out.print("Eliminated array:");
		for(int i=0;i<10;i++) //i從第0個位置開始到第9個位置檢查
		{
			//array[i] =input.nextInt();
			boolean isPrimeFlag1 = false  ;
			for(int j=0; j<i; j++)
			{	
				if (array[i] == array[j])//當比較相等時則跳出
				{
					isPrimeFlag1 = true;
					break;
				}
			}
			
			if (array[i] == 0 || isPrimeFlag1 != true)
			{	
				System.out.print(array[i]+" ");
			}
		}
}
}
