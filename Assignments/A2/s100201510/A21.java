package ce1002.A2.s100201510;

import java.util.Scanner;

public class A21 {
	public static void main(String[] args){
		System.out.print("lower bound=");
		Scanner input = new Scanner(System.in);
		int lb = input.nextInt();
		System.out.print("upper bound=");
		int ub = input.nextInt();
		input.close();
		
		if( lb >= ub ) // terminate if input does not make sense.
			System.exit(1);
		
		for(int i = lb ; i <= ub ; i++ ){ // loop use to check each number.
			boolean primeflag = true ;// use to check j is prime or not.
			
			for(int j = 2 ; j <= Math.sqrt(i) ; j++){ // loop use to check i is prime or not.
				if(i%j == 0){
					primeflag = false ;
					break;
				}
			}
			
			if(primeflag == true && i > 1) // only print positive primes.
				System.out.printf( "%d ", i );
		}
	}
}
