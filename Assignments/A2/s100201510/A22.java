package ce1002.A2.s100201510;

import java.util.Scanner;

public class A22 {
	public static void main(String[] args) {
		double[] tennumbers = new double[10] ;
		System.out.print("Array:");
		Scanner input = new Scanner(System.in) ;
		for(int i = 0 ; i < 10 ; i++){ // loop for input array.
			tennumbers[i] = input.nextDouble();
		}
		input.close();
		
		System.out.print("Eliminated array:");
		
		for(int i = 0 ; i < 10 ; i++){ // loop for output results.
			boolean flag = false ;
			for(int j = i-1 ; j >= 0 ; j--){
				if(tennumbers[j] == tennumbers[i]){
					flag = true; // flag will be true if detected repeated number.
					break;
				}
			}
			if(flag == false)
				System.out.printf("%1.0f " , tennumbers[i]);
		}
	}
}