package ce1002.A2.s101502515;

import java.util.Scanner;

public class A22 {
	public static void check_print (int[] a){//檢查是否有前面的元素
		for(int i=0;i<10;i++){
			boolean yes=true;//目前沒有
			for(int j=0;j<i;j++){//檢查前面
				if(a[i]==a[j])
					yes=false;//有重複
			}
			if(yes==true){//沒重複
				System.out.print(a[i]+" ");//輸出
			}
		}
	}
	public static void main(String args[]){
		Scanner input= new Scanner(System.in);
		int[] num = new int[10];
		System.out.print("Array:");
		for(int i=0;i<10;i++){//回圈抓值
			num[i]=input.nextInt();
		}
		System.out.print("Eliminated array:");
		check_print(num);//檢查&輸出
	}

}
