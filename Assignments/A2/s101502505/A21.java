package ce1002.A2.s101502505;
import java.util.Scanner;
import java.lang.Math;

public class A21 {
	public static void main(String[] args){
		Scanner input = new Scanner(System.in);
		int s,e;
		boolean ipf = true;
		System.out.print("lower bound=");
		s = input.nextInt();
		System.out.print("upper bound=");
		e = input.nextInt();
		
		for(int k = s; k <= e ; k++)
		{
			
			for(int i = 2 ; (i*i) <= k ; i++)
			{
				if(k%i == 0)
				{
					ipf = false;
					break;
				}
			}
			if(k != 1 && ipf == true)
				System.out.print( k + " ");
				
			ipf = true;
		}	
	}
}
