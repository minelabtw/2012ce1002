package ce1002.A2.s101502003;
import java.util.Scanner;
import java.lang.Math;//可以有開根號
public class A21 {
	public static void main (String[] args)
	{
		Scanner input = new Scanner(System.in);
		System.out.print("lower bound=");
		int low = input.nextInt();
		System.out.print("upper bound=");
		int high = input.nextInt();
		while(low<=high)//如果比較小的數小於等於比較大的數就進入迴圈
		{
			boolean a=true;
			if(low==1)//如果數是1,不是質數,所以就直接false
				a=false;
			else
			{
				for( int i = 2 ; i <= java.lang.Math.sqrt(low); i++ )
				{//在小於low開根號的數中,如果有數字可以整除low,表示它不是質數,a=false
					//跳出if,繼續看下一個數
					if( low%i == 0 )
		            {
		              	a=false;
		                break;
		            }
				}
			}
			if(a)//如果a=true,輸出low的值,low+1繼續算
				System.out.print(low + " ");
			low++;
		}
	}
}
