package ce1002.A2.s101502020;
//A2-1 by J
import java.util.Scanner;
//input instruction-1
public class A21 {

	public static void main(String[] args) {
		
		Scanner input=new Scanner(System.in) ; 
		//input instruction-2
		
		System.out.print("lower bound=") ;
		long start=input.nextLong() ;
		System.out.print("upper bound=") ;
		long end=input.nextLong() ;
		//basic imformation
		
		while (start<=end )
		{
			//int k=Math.pow(arg0, arg1) ;		//do it until free...
			
			long chr=2 ;	//chr for checker which checks data
			boolean TF=false ;		//true for the fact that it consists divisor
			
			while ( chr<start ) {
				
				if (start%chr==0)		//there exists a divisor
					TF=true ;
					
				chr++ ;		//check next integer for being a divisor
			}
			
			if (TF==false && start!=1 ) {
				System.out.print(start + " ");
			}
			
			start ++ ;		//check next integer
		}
		
	}

}
