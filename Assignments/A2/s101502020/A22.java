package ce1002.A2.s101502020;
//A2-2 by J
import java.util.Scanner;

public class A22 {

	public static void main(String[] args) {
		
		int term=10 ;
		long[] ary1 = new long[10] ;
		long[] ary2 = new long[10] ;
		int m = 0 ;
		int k =0 ;
		Scanner input = new Scanner(System.in);
		
		System.out.print("Array:") ;
		for (int i=0 ; i<10 ; i++ ){
			ary1[i]=input.nextLong();
		}		//data input
		
		while ( m<10 ) {
			int ctr=0 ;
			int n = 0 ;
			
			while ( n<10 ) {
				
				if ( ary1[n]==ary1[m] && n!=m && ary1[m]!=-32768 ){
					ary1[n]=-32768 ;		//use -32768 to represent ending situation, DAMN I can't think of a good way to do this.
					term-- ;		//it means there's a redundant datum.
				}
				n++ ;
			}
			
			m++ ;		//check next datum
		}
		
		m=0 ;
		while (m<10 )  {
			
			if (ary1[m]!=-32768) {
			ary2[k]=(long)ary1[m];
			k++ ;
			}		//put the right data only if it wasn't -32768!
			
			m++ ;
		}		//this loop puts all right data to a new array so that I can just print a easier version.
		
		m=0 ;
		System.out.print("Eliminated array:");
		while ( m<term ){
			System.out.print(ary2[m] + " ") ;
			m++ ;
		}		//this loop shows the result
	}

}
