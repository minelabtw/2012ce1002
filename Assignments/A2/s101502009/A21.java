package ce1002.A2.s101502009;

import java.util.Scanner;

public class A21 {
	public static void main(String[] arts){
		Scanner input=new Scanner(System.in);
		System.out.println("lower bound=");
		int lb=input.nextInt();
		System.out.println("upper bound=");
		int ub=input.nextInt();
		//lb<=i<=ub
		for(int i=2;i<=ub;i++){
			boolean flag=true;
			//use j to check that whether i is prim
			for(int j=2;j<i;j++){
				if(i%j==0){
					flag=false;
					break;
				}				
			}//end of for inner
			if(flag==true){
				System.out.print(i+" ");
			}
		}//end of for outer
	}//end of main

}
