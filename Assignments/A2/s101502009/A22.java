package ce1002.A2.s101502009;

import java.util.Scanner;

public class A22 {
	public static void main(String[] arts){
		
		System.out.println("Array:");
		Scanner input=new Scanner(System.in);
		int[] Array=new int[10];
		for(int i=0;i<10;i++){
			Array[i]=input.nextInt();
		}//end of for(input)
				
		for(int i=0;i<10;i++){
			
			//check Array[i] had been checked or not 
			boolean flag=true;
			if(Array[i]<0){
				flag=false;
			}
			//end of check
			
			int check=Array[i];
			for(int j=1;i+j<10;j++){
				if(Array[i+j]==check){
					Array[i+j]=-1;
				}				
			}//end of inner for 
			
			if(flag==true){
			System.out.print(Array[i]+" ");	
			}			
			Array[i]=-1;
		}//end of outer for
	}//end of main

}
