package ce1002.A2.s101502502;

import java.util.Scanner;

public class A21 
{
	public static void main(String[] args)
	{
		boolean prime;
		Scanner cin = new Scanner(System.in);
		int i;
		
		System.out.print("lower bound=");
		int num1 = cin.nextInt();
		System.out.print("upper bound=");
		int num2 = cin.nextInt();//輸入上下限
		
		while(num1<=num2)
		{
			prime = true;//宣告假設prime是質數
			
			if(num1 == 1)//1不是質數，直接加1
				num1+=1;
			
			else
			{
				for(i = 2 ; i <= Math.sqrt(num1) ; i++)//判斷是否為質數，若不是則跳出迴圈
				{
					if(num1 % i== 0)
					{
						prime = false;
						break;	
					}
				}
				
			}
			
			if(prime)//若是質數，則輸出
				{
					System.out.print(num1 + " ");
				}
			
			
			num1++;
			
		}
		
	}
}
