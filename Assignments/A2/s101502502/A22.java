package ce1002.A2.s101502502;

import java.util.Scanner;

public class A22 
{
	public static void main(String[] args)
	{
		Scanner cin = new Scanner(System.in);
		int[] inputArray = new int[10];
		int count = 0;
		
		System.out.print("Array:");
		
		for(int i = 0 ; i < 10 ; i++)
			inputArray[i] = cin.nextInt();
		
		int[] outputArray = new int[10];//宣告儲存沒重複數字的陣列
		boolean[] Array = new boolean[10];//看是否有數字重複
		
		for(int i = 0 ; i < 9 ; i++)
		{
			Array[i] = true;//令第一個數為true
			
			if(Array[i])//這個數字沒有重複過的情況
			{
				if(inputArray[i] != inputArray[i+1])
				{
					Array[i+1] = true;
					outputArray[count] = inputArray[i];
					count++;
				}
				else
				{
					Array[i+1] = false;
				}
			}
			else//這個數字有重複過的情況
			{
				if(inputArray[i] != inputArray[i+1])
				{
					Array[i+1] = true;
					outputArray[count] = inputArray[i];
					count++;
				}
				else
				{
					Array[i+1] = false;
				}
			}
			
		}
		
		if(inputArray[8] != inputArray[9])//判斷最後一項
		{
			Array[9] = true;
			outputArray[count] = inputArray[9];
			count++;
		}
		else
		{
			outputArray[count] = inputArray[9];
			count++;
		}
		
		
		System.out.print("Eliminated array:");
		for(int i = 0 ; i < count ; i++)
			System.out.print(outputArray[i]+" ");
	}
}
