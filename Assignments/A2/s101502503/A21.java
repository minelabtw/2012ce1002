package ce1002.A2.s101502503;

import java.util.Scanner;

public class A21 {
	public static void main(String[] args){
		Scanner cin = new Scanner(System.in);
		System.out.print("lower bound=");
		int low = cin.nextInt();//輸入下限
		System.out.print("upper bound=");
		int up = cin.nextInt();//輸入上限
		if(low==1)//1非質數跳過
			low++;
		for(int i=low;i<=up;i++)//從下限到上限,一個一個判斷是否為質數
		{
			int n = (int)Math.sqrt((float)i);
			int j=2;
			boolean judge = true;//為質數時judge為true,反之則false
			while( j<=n)
			{
				if(i%j==0)
				{
					judge=false;
					break;
				}
				j++;
			}
			if(judge)//若為質數,輸出
				System.out.print(i+" ");
		}
	}
}
