package ce1002.A2.s101502503;

import java.util.Scanner;

public class A22 {
	public static void main(String[] args) {
		Scanner cin = new Scanner(System.in);
		int[] intarr = new int[10];
		int k=0;
		int[] newarr = new int[10];
		System.out.print("Array:");

		for (int i = 0; i < 10; i++)//輸入陣列
			intarr[i] = cin.nextInt();
		System.out.print("Eliminated array:");
		for (int i = 0; i < 10; i++) 
		{
			boolean sameornot = false;//判斷是否與前面重複
			if (i > 0) 
			{
				for (int j = 0; j < i; j++)//與前面比較
					if (intarr[i] == intarr[j])
					{
						sameornot = true;
						break;
					}
			}
			if (sameornot == false)//沒有重複,丟入新陣列
			{
				newarr[k] = intarr[i];
				k++;
			}
		}
		for (int i = 0; i <k; i++)//輸出
			System.out.print(newarr[i]+" ");
	}
}