package ce1002.A2.s101502026;

import java.util.Scanner;

public class A22
{
	public static void main(String[] args)
	{
		Scanner scanner = new Scanner(System.in);
		while(true)
		{
			int[] Array= new int[10];
			System.out.print("Array:");
			for(int i=0;i<10;i++) //this is the loop to catch the numbers insert
			{
				Array[i] = scanner.nextInt();
			}
			for(int i=0;i<10;i++) //this is the loop to delete the repeated numbers
			{
				for(int j=0;j<10;j++)
				{
					if(i==j)
						continue;
					else if(Array[i]==Array[j])
						Array[j]=0;
				}
			}
			System.out.print("Eliminated array:");
			for(int i=0;i<10;i++)
			{
				if(Array[i]==0)
					continue;
				else
					System.out.print(Array[i]+" ");
			}
			System.out.println("\n");
		}
	}
}
