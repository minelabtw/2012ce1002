package ce1002.A2.s101502026;

import java.util.Scanner;

public class A21
{
	public static void main(String[] args)
	{
		Scanner scanner = new Scanner(System.in);
		
		while(true)
		{
			int a,b;
			System.out.print("lower bound=");
			a=scanner.nextInt();
			System.out.print("upper bound=");
			b=scanner.nextInt();
			for(;a<=b;a++)
			{
				if(a==2||a==3) //if the number is 2 or 3 , print out the prime number directly
					System.out.print(a+" ");
				for(int i=2;i<=Math.sqrt(a);i++)
				{
					if(a%i==0)
						break;
					else
					{
						if(i+1>Math.sqrt(a))
						{
							System.out.print(a+" "); //this makes sure that "a" is a prime number and then print it out
							break;
						}
						else
							continue;
					}
				}
			}
			System.out.println("\n");
		}
	}
}
