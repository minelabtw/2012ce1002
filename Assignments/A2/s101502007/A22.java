package ce1002.A2.s101502007;

import java.util.Scanner;

public class A22 {
	public static void main(String[] args)
	{
		Scanner input=new Scanner(System.in);
		int [] x;
		x=new int[10];
		System.out.print("Array:");
		for (int i=0;i<=9;i++)
		{
			x[i]=input.nextInt();
		}
		int y=10;
		for (int j=0;j<=9;j++)
		{
			for (int t=j+1;t<=9;t++)
			{
				if (x[j]==x[t]) //若有重複 則將較後面的數歸0
				{
					x[t]=0;
				}
			}
		}
		for (int e=0;e<=9;e++)
		{
			if (x[e]==0) //推算出新陣列的大小
				y--;
		}
		int [] z;
		z=new int[y];
		int u=0;
		for (int q=0;q<=9;q++) //換成新陣列
		{
			if (x[q]!=0)
			{
				z[u]=x[q];
				u++;
			}
		}
		System.out.print("Eliminated array:");
		for (int w=0;w<z.length;w++)
			System.out.print(z[w]+" ");
	}
}
