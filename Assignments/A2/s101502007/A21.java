package ce1002.A2.s101502007;
import java.util.Scanner;

public class A21 {
		
		public static void main(String[] args)
		{
			Scanner input=new Scanner(System.in);
			System.out.print("lower bound=");
			int a=input.nextInt();
			System.out.print("upper bound=");
			int b=input.nextInt();
			for (int i=a;i<=b;i++)
			{
				int c=0; 
				for (int j=1;j<=i;j++)
				{
					if (i%j==0) //若可以整除 則c加1
						c++;
				}
				if (c==2) //若c=2則代表此數為質數
					System.out.print(i+" ");
			}
		}
}
