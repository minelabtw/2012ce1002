package ce1002.s101502519;

import java.util.Scanner;

public class A21 {
	public static void main(String[] args){
		Scanner in = new Scanner(System.in);
		System.out.print("lower bound=");
		int a=in.nextInt();                        //輸入較小的數
		System.out.print("upper bound=");
		int b=in.nextInt();                        //輸入較大的數
		
		for(int i=a;i<=b;i++){
			int count=0;
			for(int n=1;n<=i/2;n++){
				if(i%n==0){
					count=count+1;                 //有一除了自己的因數就加1
				}
			}
			if(count==1){                          //除了自己只有一個因數的即為質數
				System.out.print(i+" ");
			}	
		}
	}
}
