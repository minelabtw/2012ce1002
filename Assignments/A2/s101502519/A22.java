package ce1002.s101502519;

import java.util.Scanner;

public class A22 {
	public static void main(String[] args) {
		Scanner in=new Scanner(System.in);
		System.out.print("Array:");
		int[] arr = new int[10];       //宣告長度為10的整數陣列
		for(int i=0;i<=9;i++){
			 arr[i]=in.nextInt();      //輸入10個值
		}
        System.out.print("Eliminated array:"+arr[0]+" ");   //先輸出第一個值
        
        for(int i=1;i<10;i++){
        	int p=0;
        	for(int o=0;o<i;o++){            //判斷是否和前面數字重複
        		if(arr[o]==arr[i]){
        			p=p+1;
        		}     			
        	}
        	if(p==0){                        //沒有重複就輸出
    			System.out.print(arr[i]+" ");
    		}	
        }
	}
}
