package ce1002.A2.s101502001;
import java.util.Scanner;

public class A21 {
	public static void main(String[]args){
		Scanner input=new Scanner(System.in);
		System.out.print("lower bound=");
		int a=input.nextInt();
		System.out.print("upper bound=");
		int b=input.nextInt();
		boolean isPrimeFlag;//判斷是否為質數
		while(a<=b){
			isPrimeFlag=true;
			if(a==1){
				isPrimeFlag=false;//1不為質數
			}else{
				for(int i=2;i<=Math.sqrt(a);i++){
					if(a%i==0){
						isPrimeFlag=false;//a若能被i整除則不為質數
						break;//跳出迴圈
					}
				}
			}
			if(isPrimeFlag){
				System.out.print(a);
				System.out.print(" ");
			}
			a++;
		}
	}
}
