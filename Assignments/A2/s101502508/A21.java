package ce1002.A2.s101502508;

import java.util.Scanner ;

public class A21 {
	public static void main(String args[]){
		Scanner input = new Scanner (System.in ) ;
		
		System.out.print("lower bound=") ;
		int low = input.nextInt() ;
		System.out.print("upper bound=") ;
		int up = input.nextInt() ; 
		
		for( int n=low ; n<=up ; n++ ){
			boolean a = true ;//設一個a去儲存它的布林值，若為true則為質數可在下面做輸出
			for( int k=2 ; k<=Math.sqrt(n) ; k++ ){//判斷是否為質數
				if ( n%k==0 ){
					a = false ;
					break ;
				}	
			}
			if ( n==1 ){
				a = false ;
			}
			if ( a == true ){
				System.out.print(n+" ") ;
			}
		}	
		
	}
}
