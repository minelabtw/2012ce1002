package ce1002.A2.s101502508;

import java.util.Scanner ;

public class A22 {
	public static void main(String args[]){
		
		Scanner input = new Scanner(System.in) ;
		
		int[] array = new int [10] ;
		System.out.print("Array:") ;	
		for ( int i=0 ; i<10 ; i++ ){//儲存進至列
			array[i] = input.nextInt() ;
		}
		
		System.out.print("Eliminated array:") ;
		
		////把每一個數字和前面的數字比較////
		for ( int i=0 ; i<10 ; i++ ){
			boolean a = true ;
			for( int k=0 ; k<=i ; k++ ){
				if ( array[i]==array[k] && i!=k ){
					a = false ;//如果已經重複就把數字列為錯誤，不做輸出
				}
			}
			if( a==true ){
				System.out.print(array[i]+" ") ;
			}
				
		}
		
		
	}

}
