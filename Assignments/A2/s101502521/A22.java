package ce1002.A2.s101502521;

import java.util.Arrays;
import java.util.Scanner;

public class A22 {
	public static void main(String[] args)
	{
		Scanner input = new Scanner(System.in);
		System.out.print("Array:");
		int[] arr = new int[10];
		for(int i=0;i<10;i++)
			arr[i] =  input.nextInt();
		Arrays.sort(arr); // sort to clear the numbers
		System.out.print("Eliminated array:"+arr[0]+" ");
		for(int i=1;i<arr.length;i++)
			if(arr[i]!=arr[i-1]) // because has sorted, next two diff number is the number to show
				System.out.print(arr[i]+" ");
		input.close();
	}
}
