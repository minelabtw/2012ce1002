package ce1002.A2.s101502521;

import java.util.Scanner;

public class A21 {
	public static void main(String[] args)
	{
		Scanner input = new Scanner(System.in);
		System.out.print("lower bound=");
		int lb = input.nextInt();
		System.out.print("upper bound=");
		int ub = input.nextInt(); // from lower to upper bound
		for(int i=lb;i<=ub;i++)
		{
			if(i<=1)
				continue;
			int flag = 1; // is it a prime?
			for(int j=2;j<i;j++)
				if(i%j==0)
				{
					flag = 0;
					break;
				}
			if(flag==1) // yes it is
				System.out.print(i+" ");
		}
		input.close();
	}
}
