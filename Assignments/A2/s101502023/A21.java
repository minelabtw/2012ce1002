package ce1002.A2.s101502023;

import java.util.Scanner;

public class A21 
{
	public static void main(String[] args){
		int first;
		int second;
		
		Scanner input = new Scanner(System.in);
		
		System.out.print("lower bound=");
		first=input.nextInt();
		
		System.out.print("upper bound=");
		second=input.nextInt();
		
		for(;first<=second;first++)
		{
			if(check(first))
				System.out.print(first+" ");
		}//display the prime numbers from first to second.
	}



    public static Boolean check(double x)
    {
	    boolean y=false;
	    for(int i=2;i<=Math.sqrt(x);i++)
	    {
		    if(x % i !=0)
			    y=true;
		    else
		    {
			    y=false;
		        break;
		    }
	    }//find the prime number

	    if(x==2||x==3)
		    y=true;
	    return y;
  }

}
