package ce1002.A2.s101502023;

import java.util.Scanner;

public class A22 
{
	public static void main(String[] args)
	{
		int array[] = new int[10];//10 digitals array
		Scanner input = new Scanner(System.in);
		
		System.out.print("Array:");
		for(int a=0;a<10;a++)
			array[a]=input.nextInt();//enter ten number
		
		for(int a=0;a<10;a++)
			for(int b=a+1;b<10;b++)
				if(array[a]==array[b])
					array[b]='u';//check whether the repeat numbers
		
		System.out.print("Eliminated array:");
		for(int a=0;a<10;a++)
			if(array[a]!='u')
				System.out.print(array[a]+" ");	
	}
}
