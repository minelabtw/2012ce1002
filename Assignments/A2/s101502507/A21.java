package ce1002.A2.s101502507;

import java.util.Scanner;
import java.lang.Math;

public class A21
{
	public static void main(String[] args)
	{
		Scanner input = new Scanner (System.in);
		int a,b,i,j,c=0;
		System.out.print("lower bound=");
		a = input.nextInt();
		System.out.print("upper bound=");
		b = input.nextInt();	
		
		if(a<=2)//2為唯一偶數質數
			System.out.print(2+" ");
		
		for(i=a;i<=b;i++)
		{
			int max = (int)Math.sqrt(i)+1;//檢查到該數開根號為止
			
			for(j=2;j<=max;j++)
			{
				if((i%j)==0||i==0||i==1)//可整除則非質數
					c=1;
			}
			
			if(c==0)//若是質數則印出
				System.out.print(i+" ");		
			c=0;
		}			
	}
}