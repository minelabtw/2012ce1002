package ce1002.A2.s101502507;

import java.util.Scanner;

public class A22 
{
	public static void main(String[] args)
	{
		Scanner input = new Scanner (System.in);
		int [] a = new int[10];
		int j,k,c=0;
		
		for(int i=0;i<10;i++)//讀取使用者輸入之資料
		{
			a[i]=input.nextInt();
		}				
		
		for(j=0;j<10;j++)//開始檢查
		{
			for(k=0;k<j;k++)//和前面的重複則不印出
			{				
				if(a[j]==a[k])
					c=1;				
			}		
			if(c==0)//印出無重複者
				System.out.print(a[j]+" ");			
			c=0;		
		}
	}
}