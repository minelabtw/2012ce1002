package ce1002.A2.s101502028;

import java.util.*;

public class A22 {
	public static void main (String[] args){
		Scanner arr = new Scanner (System.in); // new scanner object
		int[] array = new int[10]; // 10 arrays
		System.out.print("Array:"); // message
		for (int i = 0; i < array.length; i++)
		{
			array[i] = arr.nextInt(); // input arrays
		}
		System.out.print("Eliminated array:"); // message
		for (int j = 0; j < array.length; j++) // check arrays
		{
			boolean repeat = false; // bool to see if repeat or not
			for (int n = 0; n < j; n++) // check all the numbers before it
			{
				if (array[j] == array[n]) // if there's repeater array
				{
					repeat = true; // true
				}
			}
			if (repeat == false) // if not repeated
			{
				System.out.print(array[j] + " "); // output it
			}
		}
	}
}
