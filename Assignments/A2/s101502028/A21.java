package ce1002.A2.s101502028;

import java.util.*;

public class A21 {
	public static void main (String[] args){
		Scanner bound = new Scanner (System.in); // new scanner object
		int l, u; // integrals
		System.out.print("lower bound="); // message
		l = bound.nextInt(); // input
		System.out.print("upper bound="); // message
		u = bound.nextInt(); // input
		for (int a = l; a <= u; a++) // from lower bound to upper bound
		{
			boolean prime = true; // suppose it's prime
			for (int j = 2; j < a; j++) // the first prime is 2 so start from 2
			{
				if (a % j == 0) // if reminder is equal to 0
				{
					prime = false; // it's not prime
				}
			}
			if (prime && a != 1) // prime and not 1
			{
				System.out.print(a + " "); // output it
			}
		}
	}
}
