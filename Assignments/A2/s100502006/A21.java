package ce1002.A2.s100502006;

import java.util.Scanner;

public class A21 {
	static Scanner input = new Scanner(System.in);
	public static void main(String[] args){
		int lowerBound=0,upperBound=0;
		System.out.print("lower bound=");
		lowerBound=input.nextInt();
		System.out.print("upper bound=");
		upperBound=input.nextInt();
		for(int i=lowerBound;i<=upperBound;i++){//Check the numbers
			boolean isPrime=true;
			for(int j=2;j<=Math.sqrt(i);j++){//Check the prime
				if(i%j==0){
					isPrime=false;
					break;
				}
			}
			if(isPrime&&i!=1){
				System.out.print(i+" ");
			}
		}
	}
}
