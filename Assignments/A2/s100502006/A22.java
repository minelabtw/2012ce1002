package ce1002.A2.s100502006;

import java.util.Scanner;

public class A22 {
	static Scanner input = new Scanner(System.in);
	public static void main(String[] args){		
		final int ARRAY_SIZE=10;//array size
		int[] intArray= new int[ARRAY_SIZE];
		int totalNums=0;//count total numbers
		System.out.print("Array:");
		for(int i=0;i<ARRAY_SIZE;i++){//for 10 times input
			int a =input.nextInt();
			boolean isNotReapeated=true;//flag for repeated check
			for(int j=0;j<totalNums;j++){//check repeated
				if(a==intArray[j]){
					isNotReapeated=false;
					break;
				}
			}
			if(isNotReapeated){
				intArray[totalNums]=a;
				totalNums++;
			}
		}
		System.out.print("Eliminated array:");
		for(int k=0;k<totalNums;k++){
			System.out.print(intArray[k]+" ");
		}
	}
}
