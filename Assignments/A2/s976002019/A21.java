package ce1002.A2.s976002019;

import java.util.Scanner;

public class A21 {
	public static void main(String arg[]){
		int lower, upper, lowertemp; /*設定變數*/
		System.out.print("lower bound=");
		Scanner input1 = new Scanner(System.in);
		lower=input1.nextInt(); /*輸入下界*/
		System.out.print("upper bound=");
		Scanner input2 = new Scanner(System.in);
		upper=input2.nextInt(); /*輸入上界*/
		int primearray[][]=new int[upper-lower+1][2]; /*設定陣列大小*/
		lowertemp=lower;
		
		for(int i=0;i<primearray.length;i++ ){ /*將上下界之間所有術用陣列貯存*/
			primearray[i][0]=lowertemp++;
			//primearray[i][1]=1;
		}
		//System.out.println(primearray[1][0]);
		for(int i=0;i<(upper-lower+1);i++){ /*檢查是否為質數*/
			if(primearray[i][0]==2){ /*偵測是否為2*/
				primearray[i][1]=1;
				System.out.print(primearray[i][0]+" ");
				continue;
			}
			for(int j=2;j<primearray[i][0];j++){ /*若不是質數則標記起來*/
				
				if((primearray[i][0]%j)==0){
					primearray[i][1]=0;
					break;
				}
				else{
					primearray[i][1]=1;
				}
			}
			if(primearray[i][1]==1){ /*將所有質數印出*/
				System.out.print(primearray[i][0]+" "); 
			}
			
		}
		
		
	}

}
