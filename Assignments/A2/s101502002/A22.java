package A2.s101502002;
import java.util.Scanner;

public class A22 {
	public static void main(String[]args){
		Scanner input=new Scanner(System.in);
		System.out.print("Array:");
		int arr[]=new int[10];//設一個十項的陣列

		for(int i=0;i<=9;i++){//輸入陣列每一項的值
			arr[i]=input.nextInt();
		}
		System.out.print("Eliminated array:");
		for(int i=0;i<=9;i++){//取每一項
			int check=0;
			for(int l=0;l<i;l++){//檢查是否與前面重複
				if(arr[i]==arr[l]){
					check=1;
					break;//重複則跳出迴圈
				}	
			}
			if(check==0){
				System.out.print(arr[i]+" ");//沒重複則輸出
			}
		}
		
	}
	

}
