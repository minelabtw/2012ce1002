package ce1002.A2.s101502506;
import java.util.Scanner;

public class A22 {
	public static void main(String[] args){
		Scanner input = new Scanner(System.in);
		System.out.print("Array:");
		int []x;//宣告一個x陣列
		int c=0;//宣告一個變數c初始值為0
		x=new int[10];//讓陣列知道你有10個值要進來
		for(int i=0;i<10;i++)
		{
			x[i]=input.nextInt();//輸入10個整數
		}
		System.out.print("Eliminated array:");
		for(int k=0;k<10;k++)
		{
			if(k==0)//如果是第一個值進來,就直接輸出,不用繼續接下來的迴圈,因為根本沒有前一個值讓他比較
				{
				    System.out.print(x[k]+" ");
					continue;
				}
			for(int t=0;t<k;t++)
			{
				
				if(x[k]!=x[t])//如果跟前面的值沒有重複,c就為1
					c=1;
				else//若重覆了就c為0
				{
					c=0;
				    break;
				}    
			}
			
			if(c==1)//如果跟前面的值沒有重複,就輸出其值
				System.out.print(x[k]+" ");
		}
		
		
	}

}
