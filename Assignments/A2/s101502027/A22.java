package ce1002.A2.s101502027;
import java.util.*;
public class A22 {
	public static void main (String[]args){
		 int[] a = new int[10];
		Scanner in= new Scanner(System.in);
		System.out.print("Array:");
		for(int i=0;i<10;i++){
			a[i]=in.nextInt();
		}
		for (int b=0;b<10;b++){//do double loop to change same item to the first one of the array
			for (int c=b+1;c<10;c++ ){
				if (a[b]==a[c]){
					a[c]=a[0];
				}
			}
		}
		System.out.print("Eliminated array:");
		System.out.print(a[0]+" ");
		for (int d=0;d<10;d++){//print out
			if (a[d]!=a[0]){
				System.out.print(a[d]+" ");
			}
		}
	}
}
