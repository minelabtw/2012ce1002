package ce1002.A2.s101502512;

import java.util.Scanner;

public class A22 {
	public static void main(String[] args) {

		Scanner input = new Scanner(System.in);
		int[] inputInt = new int[10];// 輸入的陣列
		int[] outputInt = new int[10];// 輸出的陣列

		System.out.print("Array:");
		for (int i = 0; i < 10; i++) {// input array
			inputInt[i] = input.nextInt();
		}
		int j;
		int y = 0;// 輸出的數字有y個
		for (int i = 0; i < 10; i++) {

			for (j = 0; j < y; j++) {// 判斷已要輸出的數字是否重複

				if (inputInt[i] == outputInt[j])
					break;// 重複
			}

			if (j == y) {// 沒重複
				outputInt[y] = inputInt[i];
				y++;
			}
		}

		System.out.print("Eliminated array:");
		for (int i = 0; i < y; i++) {

			System.out.print(+outputInt[i] + " ");
		}

	}

}
