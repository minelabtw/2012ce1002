package ce1002.A2.s101502512;

import java.util.Scanner;

public class A21 {
	public static void main(String[] args) {
		int lower, upper;
		int k = 0;// 判斷是否為質數，k=0為質數
		Scanner input = new Scanner(System.in);

		System.out.print("lower bound=");
		lower = input.nextInt();

		System.out.print("upper bound=");
		upper = input.nextInt();

		if (lower == 1 && upper == 1) {
			System.out.println("1 is not a prime number");
		}
			
		for (int a = lower; a <= upper; a++) {//開始判斷是否有質數
			k = 0;
			if (a == 1)
				k = 1;

			for (int i = 2; i <= Math.sqrt((float) a); i++) {
				if (a % i == 0) {
					k = 1;
					break;
				}
			}

			if (k == 0)
				System.out.print(a + " ");//輸出質數
		}
	
	}
}