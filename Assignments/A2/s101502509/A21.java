package ce1002.A2.s101502509;

import java.util.Scanner;

public class A21 {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		int lownum, upnum;
		Scanner input = new Scanner(System.in);
		System.out.print("lower bound=");	
		lownum = input.nextInt();	//input lower bound
		
		System.out.print("upper bound=");
		upnum = input.nextInt();	//input upper bound
		
		checkPrime(lownum, upnum);  //check and print prime number between lower bound and upper bound
	}
	private static void checkPrime(int small, int big) {
		for(int i = small; i <= big; i++) {
			boolean isPrime = true;		//initialize isPrime tag 
			
			for(int j = 2; j <= Math.sqrt(i); j++) {
				if(i % j == 0) {	//if number "i" has another divisor,it is not a prime 
					isPrime = false;
					break;
				}
			}
			
			if(isPrime == true && i != 1) {  // isPrime tag is true and number is not "1", print it
				System.out.print(i+" ");
			}
		}
	}
}

