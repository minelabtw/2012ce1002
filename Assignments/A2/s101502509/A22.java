package ce1002.A2.s101502509;

import java.util.Scanner;

public class A22 {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner input = new Scanner(System.in);
		int size = 10;
		int[] array =  new int[size];
		
		System.out.print("Array:");
		for(int i = 0; i < size; i++) {		//input ten element
			array[i] = input.nextInt();
		}
		System.out.print("Eliminated array:");
		checkReapeat(size, array);			//check repeat element and print eliminated array
	}
	public static void checkReapeat(int length, int[] arr) {
		for(int i = 0; i < length; i++) {
			boolean isReapeat = false;		//initialize isReapeat tag
			
			for(int j = 0; j < i; j++) {	//check the element whether repeat before from arr[0]
				if(arr[j] == arr[i]) {
					isReapeat = true;
					break;
				}
			}
			
			if(!isReapeat) {				//if isReapeat is false,print the element
				System.out.print(arr[i]+" ");
			}
		}
	}
}

