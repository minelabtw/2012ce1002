package a2.s100502515;

import java.util.Scanner;

public class A21 {
	public static void main(String[] args) {
		A21 a21 = new A21();
		Scanner scanner = new Scanner(System.in);
		int lob;
		int upb;
		System.out.print("lower bound = ");
		lob = scanner.nextInt();
		System.out.print("upper bound = ");
		upb = scanner.nextInt();
		a21.checkPrime(lob, upb);
		scanner.close();

	}

	private void checkPrime(int lb, int ub) {
		for (int i = lb; i <= ub; i++) {
			if (i == 1) {
				continue;//one is defined as not a prime number.
			}
			boolean flag = true;
			for (int j = 2; j < i; j++) {
				if (i % j == 0) {
					flag = false;//if i is divided by a number less than it.
					break;//flag will be false which means it's not a prime number.
				}
			}
			if (flag)
				System.out.print(i + " ");
		}
		System.out.println();
	}
}
