package a2.s100502515;

import java.util.Scanner;

public class A22 {
	public static void main(String[] args) {
		int[] array = new int[10];
		A22 a22 = new A22();
		Scanner scanner = new Scanner(System.in);
		System.out.print("Array: ");
		for (int k = 0; k < array.length; k++) {
			array[k] = scanner.nextInt();
		}

		System.out.print("Eliminated array: ");
		a22.outputDistinct(array);
		scanner.close();
	}

	private void outputDistinct(int[] a) {
		for (int i = 0; i < a.length; i++) {
			boolean flag = true;
			for (int j = 0; j < i; j++) {
				if (a[i] == a[j]) {
					flag = false;//if the same number is found.
				}//flag is false meaning that the number is not distinct.
			}
			if (flag) {
				System.out.print(a[i] + " ");
			}
		}
	}

}
