package ce1002.A2.s100502203;

import java.util.Scanner;
import java.lang.Math;

public class A21 {
	public static boolean isPrime(int number) {
		if (number == 1 || number == 0) // 1 or 0 is not prime
			return false;
		else
			for (int i = 2; i <= Math.pow(number, 0.5); i++)
				// check if the number is prime from 2 to square root
				if (number % i == 0) // existing a factor means not prime
					return false;
		return true;
	}

	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);
		int lowb, upb;
		System.out.print("lower bound=");
		lowb = input.nextInt();
		System.out.print("upper bound=");
		upb = input.nextInt();
		for (int i = lowb; i <= upb; i++)
			// check from lower to upper bound
			if (A21.isPrime(i) == true)
				System.out.printf(i + " ");
		input.close();
	}
}
