package ce1002.A2.s100502203;

import java.util.Scanner;
import java.util.Arrays;

public class A22 {
	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);
		int Max_size = 10;
		int arr[] = new int[10];
		System.out.print("Array:");
		for (int i = 0; i < Max_size; i++)
			arr[i] = input.nextInt();
		Arrays.sort(arr); // sort array
		System.out.print("Eliminated array:");
		for (int i = 0; i < Max_size; i++)
			if (i == 0 || arr[i] != arr[i - 1])
				// index = 0 or current number unequal to previous one to print
				System.out.printf("%d ", arr[i]);
			else // if equal, continue the loop 
				continue;
		input.close();
	}
}
