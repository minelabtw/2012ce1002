package ce1002.A2.s101502523;
import java.util.*;
public class A22 {
	public static void main(String[] args){
		Scanner input = new Scanner(System.in);
		ArrayList arr = new ArrayList();	//creat another array for null length
		int[] array = new int[10];	//initialize the array length
		System.out.print("Array:");
		array[0] = input.nextInt();
		arr.add(array[0]);	
		//check the number in the array has the same one or not
		for(int i=1 ; i<array.length ; i++){
			boolean HasSameNumber = false;
			int x = input.nextInt();
			for(int j=i ; j>0 ; j--){
				//check the present input is duplicate or not
				if(j>=1&&x==array[j-1]){
					//has the same number, breaking the loop
					HasSameNumber = true;
					break;
				}
			}
			if(HasSameNumber==false){
				//dosen't have the same number 
				array[i] = x;
				arr.add(x);
			}
			else
				continue;
		}
		//print the result
		System.out.print("Eliminated array:");
		for(int k=0 ; k<arr.size() ; k++){
			System.out.print(arr.get(k)+" ");
		}
	}
}
