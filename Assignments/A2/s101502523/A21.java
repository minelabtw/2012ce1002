package ce1002.A2.s101502523;
import java.util.Scanner;
public class A21 {
	public static void main(String[] args){
		Scanner input = new Scanner(System.in);
		System.out.print("lower bound=");	
		int a = input.nextInt();
		System.out.print("upper bound=");
		int b = input.nextInt();
		//determine the number is prime or not (from a to b)
		for(int i=a ; i<=b ; i++){
			Boolean Isprime = true;
			if(i==1){
				//because 1 is not a prime
				continue;
			}
			for(int j=2 ; j<i ; j++){	
				if(i%j==0){	
					//if the number is not prime, Isprime is false
					Isprime = false;
					break;	//break the loop
				}
			}
			if(Isprime==true){	
				//if the number is prime, output the number
				System.out.print(i+" ");
			}
		}
	}
}
