package ce1002.s101502016;

import java.util.*;

public class A21 {
	public static void main(String[] args){
		Scanner input = new Scanner(System.in);
		
		int upper,lower;
		boolean judge=true;//true即為質數
		
		System.out.print("lower bound=");
		lower = input.nextInt();
		System.out.print("upper bound=");
		upper = input.nextInt();
		
		for( int i=lower ; i<=upper ; i++ )
		{
			judge = true;
			if(i==1)
				judge = false;//1為質數
			for( int j=2 ; j<i ; j++ )
			{
				if(i%j==0)//能被整除
				{
					judge = false;
					break;
				}
			}
			if(judge)
				System.out.print(i+" ");		
		}
		
	}
}
