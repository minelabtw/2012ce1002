package ce1002.s101502016;

import java.util.*;

public class A22 {
	public static void main(String[] args){
		Scanner input = new Scanner(System.in);
		int count=0;
		
		int[] array = new int[10];
		
		System.out.print("Array: ");
		
		for ( int i=0 ; i<10 ; i++)
		{
			array[i] = input.nextInt();
		}
		
		System.out.print("Eliminated array: ");
		
		for(int i=0 ; i<10 ; i++)
		{
			count = 0;
			for(int j=i ; j>=0 ;j--)
			{
				if(array[i]==array[j])
					count++;
			}
			if(count==1)
				System.out.print(array[i]+" ");
		}
		
	}

}
