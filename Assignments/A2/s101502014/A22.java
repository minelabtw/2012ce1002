package ce1002.A2.s101502014;
import java.util.Scanner;
public class A22 {
	public static void main(String[] args) {
		Scanner cin = new Scanner(System.in);
		int[] a = new int[10];
		while(true)
		{
			System.out.print("Array:");
			for(int i = 0 ; i < 10 ; i++)
				a[i] = cin.nextInt(); //輸入10個整數到a陣列
			System.out.print("Eliminated array:");
			for(int j = 0 ; j < 10 ; j++)  
			{
				if(a[j] > 0) //若a[j]為正整數，則a[j]為還沒重複的數
				{
					for(int k = j + 1 ; k < 10 ; k++)
					{
						if(a[j] == a[k])
							a[k] = -1; //重複的數改為-1
					}
					System.out.print(a[j] + " ");
				}
			}
			System.out.println("");
		}
	}
}