package ce1002.s100201021;
import java.util.Scanner;
public class A21 {
	public static void main(String[] args){
		Scanner num = new Scanner(System.in);
		int d=0;	//d=0 >> this number is prime
		System.out.print("lower bound=");
		int lb = num.nextInt(); 	//lb = lower bound
		if(lb<=1)
			lb=2;
		System.out.print("upper bound=");
		int ub = num.nextInt();		//ub = upper bound
		for(int i=lb;i <= ub;i++){
			d=0;
			for(int j=2;j<i;j++){
				if(i%j==0){	
					d++;
					break;
				}
			}
			if(d==0)
				System.out.print(i+" ");	//output prime
		}
		System.out.println();
		num.close();
	}
}
