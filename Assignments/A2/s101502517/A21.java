package ce1002.A2.s101502517;
import java.util.Scanner;
public class A21 {
	public static void main (String[] args){
		Scanner input =new Scanner(System.in);
		System.out.print("lower bound= ");
		int l=input.nextInt();//下限
		System.out.print("upper bound= ");
		int h=input.nextInt();//上限
		int c=0;//判斷數
		for(;l<=h;l++){//下限 向 上限逼近
			for(int i=2;i<l;i++){//1到下限檢查因數
				if(l%i==0){
					c=1;//非質數 判斷數不為零
				}
			}
			if(c==1 || l==1){//排除非質數或下限為1
				c=0;
			}
			else{//輸出
				System.out.print(l+" ");			
			}
		}
	}
}
