package ce1002.A2.s101502511;

import java.util.*;

public class A22 {

	public static void main(String[] args) {
		int[] number = new int[10];
		Scanner input = new Scanner(System.in);
		
		System.out.print("Array:");
		for(int i=0;i<10;i++)
			number[i] = input.nextInt();
		
		int[] number1 = eliminated(number);
		
		System.out.print("Eliminated array:");
		for(int i=0;i<number1.length;i++){//把涵式的陣列指定給新陣列
			System.out.print(number1[i]+" ");//輸出
		}
	}
	
	public static int[] eliminated(int [] number){
		int[] ary = new int[10];
		int k=0;
		Arrays.sort(number);//排序
		for(int i=0;i<10;i++){
			if(i!=0 && number[i]==ary[k-1]){//如果前後相等,k減1
				k--;//防止重複的數指定給新陣列
			}
			ary[k]=number[i];//指定數值到新陣列
			k++;
		}
		int [] ary1 = new int[k];//把0消除
		for (int i=0; i < k; i++) {
			ary1[i] = ary[i];
		}
		return ary1;

	}
}