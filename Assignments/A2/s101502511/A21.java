package ce1002.A2.s101502511;

import java.util.*;

public class A21 {
	public static void main(String[] args) {
		int lower_bound,upper_bound;
		boolean check;
		
		Scanner input = new Scanner(System.in);//new scanner
		
		System.out.print("lower bound=");
		lower_bound = input.nextInt();
		System.out.print("upper bound=");
		upper_bound = input.nextInt();
		
		while(lower_bound<=upper_bound){//while loop to find the prime number
			check=true;//initial
			if(lower_bound==1)//1 is not a prime number
				check=false;
			for(int i=2;i<=Math.sqrt(lower_bound);i++){//judge the number whether a prime number
				if(lower_bound%i==0){
					check=false;
					break;
				}
			}
			if(check==true)
				System.out.print(lower_bound + " ");
			lower_bound++;
		}
	}
}
