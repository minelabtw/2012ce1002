package ce1002.A2.s101502525;

import java.util.Scanner;
//with error check and repeat input
//input not integer to exit!
public class A22 {
	public static void main(String[] args){
		boolean exit=false;
		while(true){
			//initialize
			Scanner input=new Scanner(System.in);
			int[] array=new int[10];
			
			//input
			System.out.print("Array:");
			for(int i=0;i<10;i++)
				if(input.hasNextInt())
					array[i]=input.nextInt();
				else{
					exit=true;
					break;
				}
			if(exit)
				break;
			
			//compute & output
			System.out.print("Eliminated array:");
			for(int i=0;i<10;i++){
				boolean reappear = false;
				for(int j=0;j<i;j++)//check if the number reappear
					if(array[i]==array[j])
						reappear=true;
				if(!reappear)//if the number didn't reappear, output it
					System.out.printf("%d ",array[i]);
			}
			System.out.println("");
		}
	}
}
