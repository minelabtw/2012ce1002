package ce1002.A2.s101502525;

import java.util.Scanner;
//with error check and repeat input
//input not integer to exit!
public class A21 {
	public static void main(String[] args){
		do{
			//initialize
			int lowerbound=0,upperbound=0;
			Scanner input=new Scanner(System.in);
			
			
			//input
			System.out.print("lower bound=");
			if(input.hasNextInt())//check input
				lowerbound=input.nextInt();
			else
				break;
			System.out.print("upper bound=");
			if(input.hasNextInt())//check input
				upperbound=input.nextInt();
			else
				break;
			
			
			//exception
			if(lowerbound>upperbound){//exchange if upperbound is smaller
				int tmp=lowerbound;
				lowerbound=upperbound;
				upperbound=tmp;
			}
			
			
			//compute & output
			for(int i=lowerbound;i<=upperbound;i++)
				if(isPrime(i))
					System.out.printf("%d ", i);
			System.out.println("");
		}while(true);
	}
	private static boolean isPrime(int n){
		if(n<=1)
			return false;
		for(int i=2;i<=Math.sqrt((double)n);i++)
			if(n%i==0)
				return false;
		return true;
	}
}
