package ce1002.A2.s101502522;
import java.util.Scanner;
public class A22 
{
	public static void main(String[] args)
	{
		Scanner input =new Scanner(System.in);
		double[] array=new double[10]; //宣告陣列
		System.out.print("Array:");
		for(int i=0 ; i<10 ;i++) //輸出陣列
			array[i]=input.nextDouble();
		
		for(int j=0 ; j<10 ;j++) //判斷是否重覆
			for(int k=j+1 ;k<10 ;k++)
				if(array[j]==array[k])
					array[k]=0.1;
		
		System.out.print("Eliminated array:");
		for(int i=0 ; i<10 ;i++) //輸出消除重覆的值
		{
			if(array[i]!=0.1)
				System.out.print((int)array[i]+" ");
		}
		
	}
}
