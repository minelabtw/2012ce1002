package ce1002.A2.s101502520;

import java.util.Scanner;

public class A21 {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		int upper, lower, primebound;
		boolean flag = true;
		Scanner inputScanner = new Scanner(System.in);
		System.out.print("lower bound=");
		lower = inputScanner.nextInt();
		System.out.print("upper bound=");
		upper = inputScanner.nextInt();
		inputScanner.close();
		
		for(int i = lower; i <= upper; i++)
		{
			if(i <= 1)//skip 0 1
				continue;
			
			flag = true;
			primebound = (int)Math.sqrt(i);
			
			for(int j = 2; j <= primebound; j ++)
			{
				if(i % j == 0)//能整除就不是質數
				{
					flag = false;
					break;
				}
			}
			if(flag)
				System.out.print(i + " ");
		}
		
	}

}
