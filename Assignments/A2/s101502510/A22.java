package ce1002.A2.s101502510;

import java.util.Scanner;

public class A22 {
	public static void main(String args[]) {
		Scanner input = new Scanner(System.in);
		int[] array = new int[10];

		System.out.print("Array:");
		for (int i = 0; i < 10; i++) {// 輸入十個數字
			array[i] = input.nextInt();
		}
		System.out.print("Eliminated array:");
		for (int i = 0; i < 10; i++) {
			boolean test = false;

			for (int k = 0; k < i; k++) {
				if (array[i] == array[k]) {// 檢測第n+1位數是否有與前n個數重複
					test = true;
					break;
				}
			}

			if (test == false) {// 輸出沒重複的數
				System.out.print(array[i] + " ");
			}
		}
	}
}
