package ce1002.A2.s101502510;

import java.util.Scanner;

public class A21 {
	public static void main(String args[]) {
		Scanner input = new Scanner(System.in);

		System.out.print("lower bound=");
		int first = input.nextInt();
		System.out.print("upper bound=");
		int last = input.nextInt();

		for (int i = first; i < last; i++) {
			boolean test = false;

			for (int j = 2; j <= Math.sqrt(i); j++) {// 檢測是否為質數
				if (i % j == 0) {
					test = true;
				}
			}
			if (test == false && i != 1) {// 若為質數則輸出
				System.out.print(i + " ");
			}
		}
	}
}