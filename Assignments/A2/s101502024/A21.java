package ce1002.A2.s101502024;

import java.util.Scanner;

public class A21 {
	public static void main(String[] args) {
		int a, b;
		boolean isPrimeFlag;
		Scanner hue = new Scanner(System.in);

		System.out.print("lower bound =");//show line
		a = hue.nextInt();//enter the  number
		System.out.print("upper bound =");
		b = hue.nextInt();

		while (a <= b) {//from ce1001 .
			isPrimeFlag = true;

			if (a == 1) {
				isPrimeFlag = false;

			} else {
				for (int i = 2, end = (int) Math.pow(a, 0.5); i <= end; i++) {
					if (a % i == 0) {
						isPrimeFlag = false;
						break;
					}
				}
			}
			if (isPrimeFlag) {
				System.out.print(a + " ");//print result
			}
			a++;
		}

	}
}
