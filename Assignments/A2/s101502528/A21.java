package ce1002.A2.s101502528;

import java.util.Scanner;

public class A21 {
	public static void main(String[] args){
		int lb,ub,check;
		
		System.out.print("lower bound=");
		Scanner input=new Scanner(System.in);
		lb=input.nextInt();
		
		System.out.print("upper bound=");
		ub=input.nextInt();
		
		//逐一判斷從lower bound到upper bound的數字
		for ( int i=lb ; i<=ub ; i++ ){
			check=1;
			if ( i==1 )
				i++;
			//判斷是否為質數
			for ( int j=2 ; j<=i ; j++ ){
				if ( i==2 ){
					check=1;
					break;
				}
				
				else if ( j<i && i%j==0 ){
					check=0;
					break;
				}
			}
			if ( check==1 )
				System.out.print(i+" ");

		}
	}

}
