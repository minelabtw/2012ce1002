package ce1002.A2.s101502006;

import java.util.Scanner;
// program uses class Scanner

public class A21 
{
	public static void main(String[] args)
	{
		// create Scanner to obtain input from command window
		Scanner input = new Scanner( System.in ); 
		int lowerBound, upperBound;
		boolean isPrimeFlag ;//isPrimeFlag is used for record whether this number is a prime number.
		
		// input value:lowerBound and upperBound
		System.out.print("lower bound=");
		lowerBound=input.nextInt();
		
		System.out.print("upper bound=");
		upperBound=input.nextInt();
		
		
		while( lowerBound <= upperBound )
	    {
	        isPrimeFlag = true;
	        
	        // 1 is not a prime number.
	        if( lowerBound == 1 )
	            isPrimeFlag = false;
	        else
	            for( double i = 2 , end = Math.sqrt(lowerBound) ; i <= end ; i++ )
	                if( lowerBound%i == 0 )
	                {
	                    isPrimeFlag = false;
	                    break;
	                }
	        if(isPrimeFlag)
	        	System.out.print(lowerBound + " ");
	        lowerBound++;
	    }
	}
}