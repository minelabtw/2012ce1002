package ce1002.A2.s101502006;

import java.util.Scanner;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

public class A22 
{
	public static void main(String[] args)
	{
		Scanner input = new Scanner(System.in);
		int arr[]=new int[10];
		
		System.out.print("Array:");
		
		// input value to array.
		for(int i=0;i<arr.length;i++)
		{
			arr[i]=input.nextInt();
		}
		
		 // use set to delete the same number
		Set<Integer> intSet = new HashSet<Integer>();
		for (int element : arr)
		{
			intSet.add(element);
		}
		
		// intSet.size() is the size of new Array -> arrfixed.
		int arrfixed[] = new int[intSet.size()];
		
		
		// retrieve the value to arrfixed from Set. 

		Object[] tempArray = intSet.toArray();
		for (int i = 0; i < tempArray.length; i++) 
		{
			arrfixed[i] = (Integer) tempArray[i];
		}
		
		// Output result
		System.out.print("Eliminated array:");
		for(int i=0;i<arrfixed.length;i++)
		{
			System.out.print(arrfixed[i] + " ");
		}
	}
}
