package ce1002.A4.s100502515;

import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Scanner;

public class A4 {
	static int xTop = -1;
	static int yTop = -1;
	int[][] mazeArray = new int[15][15];
	int[] xArray = new int[100];
	int[] yArray = new int[100];

	public static void main(String[] args) throws IOException {
		Scanner scanner = new Scanner(System.in);
		A4 a4 = new A4();

		System.out.print("Enter the path of the maze:");
		String mazePath = scanner.nextLine();
		a4.readFile(mazePath);//read the file.
		a4.maze();
		a4.writeFile();

		scanner.close();
	}

	private void readFile(String mP) throws IOException {

		FileReader fileReader = new FileReader(mP);
		for (int i = 0; i < 15; i++) {
			for (int j = 0; j < 15; j++) {
				int temp = (fileReader.read() - 48);
				if (temp == 0 || temp == 1)
					mazeArray[i][j] = temp;
				else
					j--;
			}
		}
		fileReader.close();

	}//function to read the file

	private void writeFile() throws IOException {
		FileWriter fileWriter = new FileWriter("s100502515.txt");

		for (int i = 0; i < 15; i++) {
			for (int j = 0; j < 15; j++) {
				if (mazeArray[i][j] == 2)
					fileWriter.write("2");
				else if (mazeArray[i][j] == 4)
					fileWriter.write("4");
				else if (i == 14 && j == 14)
					fileWriter.write("2");
				else
					fileWriter.write("1");
			}
			fileWriter.write("\r\n");
		}

		fileWriter.flush();
		fileWriter.close();
	}//function to create and write the file.

	private void maze() {
		int x = 0;
		int y = 0;
		while (x != 14 || y != 14) {
			if (x > 0 && mazeArray[(x - 1)][y] == 0) {
				mazeArray[x][y] = 2;
				x--;
				push(x, y);//try to move left
			} else if (x < 14 && mazeArray[(x + 1)][y] == 0) {
				mazeArray[x][y] = 2;
				x++;
				push(x, y);//try to move right
			} else if (y > 0 && mazeArray[x][(y - 1)] == 0) {
				mazeArray[x][y] = 2;
				y--;
				push(x, y);//try to move up
			} else if (y < 14 && mazeArray[x][(y + 1)] == 0) {
				mazeArray[x][y] = 2;
				y++;
				push(x, y);//try to move down
			} else if (y > 0 && x > 0 && mazeArray[(x - 1)][(y - 1)] == 0) {
				mazeArray[x][y] = 2;
				x--;
				y--;
				push(x, y);//try to move left up
			} else if (y < 14 && x > 0 && mazeArray[(x - 1)][(y + 1)] == 0) {
				mazeArray[x][y] = 2;
				x--;
				y++;
				push(x, y);//try to move left down
			} else if (y > 0 && x < 14 && mazeArray[(x + 1)][(y - 1)] == 0) {
				mazeArray[x][y] = 2;
				x++;
				y--;
				push(x, y);//try to move right up
			} else if (y < 14 && x < 14 && mazeArray[(x + 1)][(y + 1)] == 0) {
				mazeArray[x][y] = 2;
				x++;
				y++;
				push(x, y);//try to move right down
			} else {
				mazeArray[x][y] = 4;
				x = popX();
				y = popY();//no way out and go back to the other way
			}
		}
	}

	private void push(int xNum, int yNum) {
		xArray[++xTop] = xNum;
		yArray[++yTop] = yNum;//record the moves
	}

	private int popX() { 
		return xArray[--xTop];//go back to the last point
	}

	private int popY() {
		return yArray[--yTop];//go back to the last point.
	}

}
