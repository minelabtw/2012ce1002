
package ce1002.A4.s101502013;
 
public class Stack 
{
    int[][] stack;
    //第一維是輸入數字 第二維則是表示此數字是否有效  若為0則無效 為1則表示有效(模擬stack)
	Stack()
    {
		;//something initialize
    }
    public Stack(int input)
    {
    	stack = new int[input][2];
    }
    public void push(int input)
    {
		int pointer=-1;//pointer表示陣列stack第一維的第幾個開始為有效資料
		for (int i=(stack.length-1);i>=0;i--)//stack.length=100  抓出pointer
		{
			if (stack[i][1]==1)
			{
				pointer=i;
				break;
			}
		}
		if (pointer<stack.length)
		{
			stack[pointer+1][0]=input;//將資料存入
			stack[pointer+1][1]=1;//標示為有效資料
			return;//結束 其實並沒有回傳值
		}
		else
		{
			System.out.println("The stack is overflow.");
			return;//結束
		}
    }
    public int pop()
    {
		int pointer=-1;
		for (int i=(stack.length-1);i>=0;i--)//抓出pointer
		{
			if (stack[i][1]==1)
			{
				pointer=i;
				break;
			}
		}
		if (pointer == -1)
		{	
			System.out.print("Stack is empty! The integer returned is invalid!");
			return 0;
		}
		else
		{
			stack[pointer][1]=0;
			return stack[pointer][0];
		}
    }
    public void show()
    {
    	int pointer=-1;
		for (int i=(stack.length-1);i>=0;i--)//抓出pointer
		{
			if (stack[i][1]==1)
			{
				pointer=i;
				break;
			}
		}
		for (int i=0;i<=pointer;i++)
			System.out.print(stack[i][0] + " ");		
    }
}
