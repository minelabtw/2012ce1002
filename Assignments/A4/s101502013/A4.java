package ce1002.A4.s101502013;

import java.io.*;

import ce1002.A4.s101502013.Stack;
public class A4 {
	public static void main(String[] args)throws IOException{
		{
		int[] a = new int[300];
		int[] point = new int[2];
		int[][] map = new int[15][15];
		Stack pass = new Stack(225);
		System.out.println("請輸入一個 15*15 的迷宮: (能走為 0 ,牆壁為 1)");
		System.out.println("( 規定:左上角[0][0]為入口, 右下角[14][14]為出口, 其餘邊緣皆是牆壁 )");
		System.out.println("＊注意:測出路線之後,已走過路線會被記為2,若是死路則記為4");
		System.out.println("請輸入要讀取的檔案路徑 (例: c:\\maze.txt): ");
		BufferedReader buf = new BufferedReader(new InputStreamReader(System.in));
		String s1 = buf.readLine();
		FileInputStream fis = new FileInputStream(s1);
		int p = 0;
        boolean sw = true,sw2 = true;
        //以下 將maze.txt傳進來 此時a[]陣列裡儲存的資料為 48、49　非48、49則不儲存   	即非0或1不傳進a[]
        while ((a[p] = fis.read()) != -1)
			if(a[p] == 48 || a[p] == 49)
				p++;	
        fis.close();
        p=0;
        //以下將a[]陣列轉為map[][]二維陣列 且將48 & 49轉為 0 & 1 第一維代表列 第二維代表行
        for (int i=0;i<15;i++)
			for(int j=0;j<15;j++)
			{
				map[i][j] = a[p]-48;
				p++;
			}
        //以下顯示迷宮
        System.out.println("\r\n您的迷宮為:");
        for (int i=0;i<15;i++)
        {
        	for (int j=0;j<15;j++)
        	{
        		System.out.print(map[i][j]);
        	}
        	System.out.println();
        }
		//以上顯示迷宮
        
        //point[]代表現在位置  point[0]代表列	X座標  	point[1]代表行		Y座標
        point[0] = 0;
        point[1] = 0;
        p=0;
        //以下為八方向轉 先從   	順時鐘      方向依序檢查
        while(sw2 == true)
        {
        	if (sw == false)//代表繞了一圈沒路了
        	{
        		map[point[0]][point[1]] = 4;
        		point[1] = pass.pop();//先拿 Y座標回來 再拿X座標回來
        		point[0] = pass.pop();
        		sw = true;
        	}
        	if (point[0]-1>=0)
	        {//上
	        	if (map[point[0]-1][point[1]] == 0 )
	        	{
	        		map[point[0]][point[1]] = 2;
		        	pass.push(point[0]);//先push X座標		 後push Y座標
		        	pass.push(point[1]);
		        	point[0] = point[0]-1;
		        	p++;
		        	//System.out.println("跑了 " + p +" 次       point 在 ("+point[0]+","+point[1]+")往上" );
		        	continue;
	        	}
	        }
        	if ((point[0]-1)>=0 && (point[1]+1)<=14)
    	    {//右上
        		if(map[point[0]-1][point[1]+1] == 0)
	        	{
        			map[point[0]][point[1]] = 2;
	        		pass.push(point[0]);//先push X座標		 後push Y座標
		        	pass.push(point[1]);
	        		point[0]=point[0]-1;
	        		point[1]=point[1]+1;
		        	p++;
		        	//System.out.println("跑了 " + p +" 次       point 在 ("+point[0]+","+point[1]+")往右上" );
	        		continue;
	        	}
    	    }
        	if ((point[1]+1)<=14)
    	    {//右
        		if (map[point[0]][point[1]+1] == 0)
        		{
	        		map[point[0]][point[1]] = 2;
	        		pass.push(point[0]);//先push X座標		 後push Y座標
		        	pass.push(point[1]);
		        	point[1]=point[1]+1;
		        	p++;
		        	//System.out.println("跑了 " + p +" 次       point 在 ("+point[0]+","+point[1]+")往右" );
	        		continue;
        		}
    	    }
        	if( (point[0]+1)<=14 && (point[1]+1)<=14)
    	    {//右下
        		if (map[point[0]+1][point[1]+1] == 0)
        		{
        			map[point[0]][point[1]] = 2;
	        		pass.push(point[0]);//先push X座標		 後push Y座標
		        	pass.push(point[1]);
		        	point[0]=point[0]+1;
		        	point[1]=point[1]+1;
		        	p++;
		        	//System.out.println("跑了 " + p +" 次       point 在 ("+point[0]+","+point[1]+")往右下" );
	        		continue;
        		}
    	    }
        	if  ((point[0]+1)<=14)
    	    {//下
        		if (map[point[0]+1][point[1]] == 0)
        		{
        			map[point[0]][point[1]] = 2;
        			pass.push(point[0]);//先push X座標		 後push Y座標
        			pass.push(point[1]);
        			point[0]=point[0]+1;
		        	p++;
		        	//System.out.println("跑了 " + p +" 次       point 在 ("+point[0]+","+point[1]+")往下" );
	        		continue;
	        	}
    	    }
        	if ( (point[0]+1)<=14 && (point[1]-1)>=0)
    	    {//左下
        		if(map[point[0]+1][point[1]-1] == 0 )
        		{		
        			map[point[0]][point[1]] = 2;
	        		pass.push(point[0]);//先push X座標		 後push Y座標
		        	pass.push(point[1]);
		        	point[0]=point[0]+1;
		        	point[1]=point[1]-1;
		        	p++;
		        	//System.out.println("跑了 " + p +" 次       point 在 ("+point[0]+","+point[1]+")往左下" );
	        		continue;
	        	}
    	    }
        	if ( (point[1]-1)>=0)
    	    {//左
        		if (map[point[0]][point[1]-1] == 0 )
        		{
        			map[point[0]][point[1]] = 2;
	        		pass.push(point[0]);//先push X座標		 後push Y座標
		        	pass.push(point[1]);
		        	point[1]=point[1]-1;
		        	p++;
		        	//System.out.println("跑了 " + p +" 次       point 在 ("+point[0]+","+point[1]+")往左" );
	        		continue;
	        	}
    	    }
        	if ( (point[0]-1)>=0 && (point[1]-1)>=0)
    	    {//左上
        		if (map[point[0]-1][point[1]-1] == 0 )
        		{
        			map[point[0]][point[1]] = 2;
	        		pass.push(point[0]);//先push X座標		 後push Y座標
		        	pass.push(point[1]);
		        	point[0]=point[0]-1;
		        	point[1]=point[1]-1;
		        	p++;
		        	//System.out.println("跑了 " + p +" 次       point 在 ("+point[0]+","+point[1]+")往左上" );
	        		continue;
        		}
    	    } 
        	sw = false;
        	if (point[0] == 14 && point[1] == 14)
        	{	
        		sw2 = false;
        		map[14][14] = 2;
        	}
        	//System.out.println("跑了 " + p +" 次       point 在 ("+point[0]+","+point[1]+")" );
        	
        	
        }
        /*for (int i=0;i<15;i++)以下註解區是顯示輸出結果
        {
        	for (int j=0;j<15;j++)
        	{
        		System.out.print(map[i][j]);
        	}
        	System.out.println();
        }
        *///以上註解區是顯示輸出結果方便檢查
        //以下為寫檔 
        FileOutputStream fos = new FileOutputStream("s101502013.txt");
        for (int i=0;i<15;i++)
		{
			for (int j=0;j<15;j++)
			{
				fos.write(map[i][j]+48);
			}
			fos.write(13);//換行
			fos.write(10);
		}
        fos.close();
        System.out.println("\r\n---路線已畫出: s101502013.txt---");
        }
	}
}

