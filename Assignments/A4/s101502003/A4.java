package ce1002.A4.s101502003;
import java.io.*;
public class A4 {
	 public static void main(String args[]) throws Exception
     {  
         String getbr;
         BufferedReader br= new BufferedReader(new InputStreamReader(System.in));
         System.out.println("請輸入一個 15*15 的迷宮: (能走為 0 ,牆壁為 1) ");
         System.out.println("( 規定:左上角[0][0]為入口, 右下角[14][14]為出口, 其餘邊緣皆是牆壁 )");
         System.out.println("＊注意:測出路線之後,已走過路線會被記為2,若是死路則記為4 ");
         System.out.print("請輸入要讀取的檔案路徑 (例: c:\\maze.txt):");
         getbr = br.readLine(); //取得input file
         System.out.println("您的迷宮為：");
         //////////////參考網路來源:
         ////////www.im.cjcu.edu.tw/~schsu/powerpoints/Java2/CH07.ppt
         char buffer[] = new char[255];
         FileReader fr= new FileReader(getbr);
         int len =fr.read(buffer);
         String s =new String (buffer,0,len);
         fr.close();
         System.out.println(s);
         /////////////////////////////
         char[] maze;
         maze = s.toCharArray();////string轉char
         for(int i=0;i<15;i++)//第一行找入口
         {
        	 if(maze[i]=='0')
        		 maze[i]='2';
         }
         for(int i=16;i<209;i++)
         {//中間如果0或2順時針加起來<2表示死路,變成4
        	 int count=0;
        	 if(maze[i-16]=='0'||maze[i-16]=='2')
        	 {
        		 count ++;
        		 maze[i-16]='2';
        	 }
        	 if(maze[i-15]=='0'||maze[i-15]=='2')
        	 {
        		 count ++;
        		 maze[i-15]='2';
        	 }
        	 if(maze[i-14]=='0'||maze[i-14]=='2')
        	 {
        		 count ++;
        		 maze[i-14]='2';
        	 }
        	 if(maze[i-1]=='0'||maze[i-1]=='2')
        	 {
        		 count ++;
        		 maze[i-1]='2';
        	 }
        	 if(maze[i+1]=='0'||maze[i+1]=='2')
        	 {
        		 count ++;
        		 maze[i+1]='2';
        	 }
        	 if(maze[i+14]=='0'||maze[i+14]=='2')
        	 {
        		 count ++;
        		 maze[i+14]='2';
        	 }
        	 if(maze[i+15]=='0'||maze[i+15]=='2')
        	 {
        		 count ++;
        		 maze[i+15]='2';
        	 }
        	 if(maze[i+16]=='0'||maze[i+16]=='2')
        	 {
        		 count ++;
        		 maze[i+16]='2';
        	 }
        	 if(count==2)
        	 {
        		 maze[i]='4';
        	 }
         }
         for(int i=209;i<225;i++)//最後一行找出口
         {
        	 if(maze[i]=='0')
        		 maze[i]='2';
         }
         System.out.println("\n---路線已畫出: s101502003.txt---");
         ///////////寫檔//////////////////
         java.io.File write_file = new java.io.File( "s101502003.txt" );
         java.io.PrintWriter output=new java.io.PrintWriter(write_file);
 		 for(int i=0;i<225;i++)
 		 { 
 			output.print(maze[i]);
 			if(i%15==14)///////每十五個換一行
 				System.out.print("\n");
 		}
 		output.close(); //關掉檔案
        
     }
}
