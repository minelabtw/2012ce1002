package ce1002.A4.s101502002;
import java.util.Scanner;
public class A4 {
	public static void main(String[]args) throws Exception{
		System.out.println("請輸入一個 15*15 的迷宮: (能走為 0 ,牆壁為 1)\n( 規定:左上角[0][0]為入口, 右下角[14][14]為出口, 其餘邊緣皆是牆壁 ) \n＊注意:測出路線之後,已走過路線會被記為2,若是死路則記為4 ");
		System.out.print("請輸入要讀取的檔案路徑 (例: c:\\maze.txt): ");
		Scanner input = new Scanner(System.in);  //輸入用
		String open= input.nextLine();  //開檔用
		System.out.println("\n您的迷宮為:");
		java.io.File file =new java.io.File(open); 
		Scanner yes = new Scanner(file);
		char [][]line = new char[15][15]; //一個15*15的陣列用來存迷宮的每一格　
		for (int i=0;i<15;i++){  //存取檔案內的東西
			String there=yes.next();
			for(int l=0;l<15;l++){
				line[i][l]=there.charAt(l); //一行為一個string
			}
		}
		yes.close(); //關閉那個檔案吧吧吧
		for(int i=0;i<15;i++){ //
			for(int l=0;l<15;l++){
				System.out.print(line[i][l]);
			}
			System.out.println();
		}
		
		int []stackone=new int[1000]; //stack來存走過的路徑
		int []stacktwo=new int[1000];		
		int a=0,b=0; //一開始的位置
		stackone[0]=a; //第一步
		stacktwo[0]=b;
		int before=1; //開始第二步囉
		while(true){
			if(a<14&&b<14){ //還沒到終點前
				if(line[a+1][b+1]=='0'){ //可以往右下耶
					line[a][b]='2'; //標記他為可走的路
					a=a+1; //走向下一格
					b=b+1;
					stackone[before]=a; //記錄這一步
					stacktwo[before]=b;
					before=before+1; //準備紀錄下一步囉
				}
				else if(line[a][b+1]=='0'){ //可以往右欸
					line[a][b]='2';
					b=b+1;
					stackone[before]=a;
					stacktwo[before]=b;
					before=before+1;
				}
				else if(line[a+1][b]=='0'){ //可以往下欸
					line[a][b]='2';
					a=a+1;
					stackone[before]=a;
					stacktwo[before]=b;
					before=before+1;
				}
				else if(line[a+1][b-1]=='0'){ //往左下
					line[a][b]='2';
					a=a+1;
					b=b-1;
					stackone[before]=a;
					stacktwo[before]=b;
					before=before+1;
				}
				else if(line[a-1][b+1]=='0'){ //往右上
					line[a][b]='2';
					a=a-1;
					b=b+1;
					stackone[before]=a;
					stacktwo[before]=b;
					before=before+1;
				}
				else if(line[a-1][b]=='0'){ //上
					line[a][b]='2';
					a=a-1;
					stackone[before]=a;
					stacktwo[before]=b;
					before=before+1;
				}
				else if(line[a][b-1]=='0'){ //左
					line[a][b]='2';
					b=b-1;
					stackone[before]=a;
					stacktwo[before]=b;
					before=before+1;
				}
				else if(line[a-1][b-1]=='0'){ //左上
					line[a][b]='2';
					a=a-1;
					b=b-1;
					stackone[before]=a;
					stacktwo[before]=b;
					before=before+1;
				}
				///////在這之前都是有路可走
				else{ //沒有路走了QQ
					line[a][b]='4'; //標記她是死路
					a=stackone[before-2]; //回到上一步
					b=stacktwo[before-2];
					before=before-1; //重新記錄這一步
				}
			}
			else if(a==14 && b==14){ //阿終於走完了
				line[a][b]='2';
				break; //離開迴圈囉
			}	
		}
		System.out.println("\n---路線已畫出: s101502002.txt---");
		java.io.File write_file = new java.io.File("s101502002.txt"); //創造一個檔案
		java.io.PrintWriter output=new java.io.PrintWriter(write_file);
		for(int i=0;i<15;i++){ //把剛剛走的迷宮路線記錄在檔案裡
			for(int l=0;l<15;l++){
				output.print(line[i][l]);
			}
			output.println();
		}
		output.close(); //關掉檔案
	}

}
