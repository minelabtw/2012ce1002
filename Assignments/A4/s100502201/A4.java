//Escape the maze
//0 is aisle, 1 is wall
//2 is the right route, 4 is the route of dead end
package ce1002.A4.s100502201;

import java.util.Scanner;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.File;
import java.io.PrintStream;

public class A4
{
	public static void main(String[] args)
	{  
        Scanner input = new Scanner(System.in);
        Scanner fip = null; //Initialize
        PrintStream fop = null;
        java.io.File read_File; //Initialize for later use, in fact don't need to add "java.io...blablabla" cause we've already imported functions
        java.io.File write_File = new java.io.File("s100502201.txt"); //Meet the task of problem
        char[][] maze = new char[15][];
        System.out.println("請輸入一個 15*15 的迷宮: (能走為 0 ,牆壁為 1)");
        System.out.println("( 規定:左上角[0][0]為入口, 右下角[14][14]為出口, 其餘邊緣皆是牆壁 )");
        System.out.println("＊注意:測出路線之後,已走過路線會被記為2,若是死路則記為4");
        System.out.println("請輸入要讀取的檔案路徑 (例: c:\\maze.tryColt): ");
        try //When exception is met, it would automatically exit
        {
        	read_File = new File(input.next());
        	fip = new Scanner(new FileInputStream(read_File)); //fip for read
        	fop = new PrintStream(new FileOutputStream(write_File)); // fop for printout
        	System.out.println("\n您的迷宮為:");
        	int row = 0;
        	while (fip.hasNext()) //Input maze
        	{
        		maze[row] = fip.next().toCharArray();
        		System.out.println(maze[row]);
        		if (maze[row].length != 15)
        			throw new Exception(); //Make sure that there are 15 columns, or error will occur
        		row++;
        	}
        	if (row != 15) //Same as above, check the row number
        		throw new Exception();
        }
    	catch (Exception error) //Format error and exit 
    	{
    		System.out.println("輸入格式錯誤,離開程式");
    		System.exit(0);
    	}
        int[] campCol = new int[225];
        int[] campRow = new int[225];
        int[][] route = new int[15][15];
        int idx = 0;
        campCol[idx] = 0;
        campRow[idx] = 0;
        route[0][0] = 1;
        for (int i = 0; i <= idx; i++) 
        {
            int x = campCol[i];
            int	y = campRow[i];
            for (int j = -1; j <= 1; j++) 
            {
                for (int k = -1; k <= 1; k++) 
                {
                    int tryCol = x + j;
                    int tryRow = y + k;
                    if (tryCol < 0 || tryRow < 0 || tryCol >= 15 || tryRow >= 15)
                        continue;
                    if (maze[tryCol][tryRow] == '1' || route[tryCol][tryRow] != 0)
                        continue;
                    route[tryCol][tryRow] = route[x][y] + 1; //Mark the right route
                    campCol[++idx] = tryCol;
                    campRow[idx] = tryRow;
                }
            }
            if (route[14][14] != 0) //Reached
                break;
        }
        int markCol = 14;
        int markRow = 14; //Backward marking(from end to start), right route will be marked as "2"
        maze[markCol][markRow] = '2';
        while (markCol != 0 || markRow != 0) 
        {
            for (int i = -1; i <= 1; i++) 
            {
                for (int j = -1; j <= 1; j++) 
                {
                    int tryCol = markCol + i;
                    int	tryRow = markRow + j;
                    if (tryCol < 0 || tryRow < 0 || tryCol >= 15 || tryRow >= 15)
                        continue;
                    if (route[tryCol][tryRow] == route[markCol][markRow] - 1) //A right route is found 
                    {
                        markCol = tryCol;
                        markRow = tryRow;
                        i = 2;
                        j = 2;
                    }
                }
            }
            maze[markCol][markRow] = '2';
        }
        for (int i = 0; i < 15; i++) //Printout
            fop.println(maze[i]);
        System.out.println("\n---路線已畫出: s100502201.txt---");
    }
}