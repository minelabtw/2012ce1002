package ce1002.A4.s101502514;
import java.io.*;
import java.util.Scanner;
import java.util.Stack;
import java.lang.String;

public class A4 {
	public static void main(String[] args) throws Exception {
		System.out.println("請輸入一個 15*15 的迷宮: (能走為 0 ,牆壁為 1)");
		System.out.println("( 規定:左上角[0][0]為入口, 右下角[14][14]為出口, 其餘邊緣皆是牆壁 )   ");
		System.out.println("＊注意:測出路線之後,已走過路線會被記為2,若是死路則記為4");
		System.out.print("請輸入要讀取的檔案路徑 (例: c:\\maze.txt): ");
			
		String str[][] = new String[15][15];
		Stack stack = new Stack();//make a stack
		Stack stack1 = new Stack();//make a stack
		Scanner input = new Scanner(System.in);
		String q=input.next();
		java.io.File file = new java.io.File(q);
		Scanner scanner = new Scanner(file);
		int num = 0;
		System.out.println();
		System.out.println("您的迷宮為:");
		while(scanner.hasNext()) {//store the number from file to array
			String[] number=scanner.next().split("");
		    for(int i=1;i<=15;i++) {
		    	str[num][i-1] = number[i];
		    }
		         num++;
		}
		scanner.close();
		
		for(int i=0;i<15;i++) {//maze
			for(int j=0;j<15;j++){
				System.out.print(str[i][j]);
		    }
		    System.out.println();
		}     
		
		int istr[][]=new int[15][15];
		for(int i=0;i<15;i++){//turn the string to int
			for(int j=0;j<15;j++){
				istr[i][j] = Integer.parseInt(str[i][j]);
		    }
		}
		
		istr[0][0] = 2;
		istr[1][1] = 2;
		int a=1,b=1;

		while(true){//如果周圍有0，往那邊走，並將那格變成2，如果周圍都不是0，將那個變成4，並pop出，並存到座標裡(後退)
			if(istr[a-1][b-1]==0){
				stack.push(a-1);
		        stack1.push(b-1);
		        istr[a-1][b-1]=2;
		        a--;
		        b--;
		    }
		    	else if(istr[a-1][b]==0){
		    		stack.push(a-1);
		        	stack1.push(b);
		        	istr[a-1][b]=2;
		        	a--;
		        }
		        	 else if(istr[a-1][b+1]==0){
		        	   	 stack.push(a-1);
		        	   	 stack1.push(b+1);
		        	   	 istr[a-1][b+1]=2; 
		        	   	 a--;
		        	   	 b++;
		        	}
		        	 	else if(istr[a][b+1]==0){
		        	 		stack.push(a);
		        	 		stack1.push(b+1);
		        	 		istr[a][b+1]=2;
		        	 		b++;
		        	    }
		        	 		else if(istr[a+1][b+1]==0){
		        	 			stack.push(a+1);
		        	 			stack1.push(b+1);
		        	 			istr[a+1][b+1]=2;
		        	 			b++;
		        	 			a++;	
		        	 		}
		        	 			else if(istr[a+1][b]==0){
		        	 				stack.push(a+1);
		        	 				stack1.push(b);
		        	 				istr[a+1][b]=2;
		        	 				a++;	
		        	 			}
		        	 				else if(istr[a+1][b-1]==0) {
		        	 					stack.push(a+1);
		        	 					stack1.push(b-1);
		        	 					istr[a+1][b-1]=2;
		        	 					a++;
		        	 					b--;
		        	 				}
		        	 					else if(istr[a][b-1]==0){
		        	 						stack.push(a);
		        	 						stack1.push(b-1);
		        	 						istr[a][b-1]=2;
		        	 						b--;
		        	 					}
		        	 						else {
		        	 							istr[a][b]=4;
		        	 							a=(int)stack.pop();
		        	 							b=(int)stack1.pop();
		        	 						}
		        	 
			if(a==14&&b==14){
				break;
		    }
		}
		
		java.io.File write_file = new java.io.File( "s101502514.txt" );//寫檔
		PrintWriter fileWriter = new PrintWriter(write_file);
		for(int i=0;i<15;i++){
			for(int j=0;j<15;j++){
				fileWriter.print(istr[i][j]);
		    }
			fileWriter.println();
		}
		        
		fileWriter.close();
		System.out.println();
		System.out.print("---路線已畫出: s101502514.txt---");
		}         
}
		  


	