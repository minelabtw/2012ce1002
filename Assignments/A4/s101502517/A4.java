package ce1002.A4.s101502517;
import java.io.*;
import java.lang.*;
import java.util.*;
import java.text.*;
public class A4 {
public static void main(String[] args)throws IOException{
		
		Scanner input=new Scanner(System.in);
		System.out.println("請輸入一個 15*15 的迷宮: (能走為 0 ,牆壁為 1)");
		System.out.println("( 規定:左上角[0][0]為入口, 右下角[14][14]為出口, 其餘邊緣皆是牆壁 )");
		System.out.println("＊注意:測出路線之後,已走過路線會被記為2,若是死路則記為4");
		System.out.print("請輸入要讀取的檔案路徑 (例: c:\\maze.txt): ");
		String s=input.next();

		File file=new File(s);
		Reader reader = null;
		int tremp=5,np=1,nr=0,tmai=1;//tremp=讀檔緩衝 np=現在位子 nr=死路 tmai=tmai[]陣列記數
		int ma []=new int[226];//地圖
		int tma[]=new int[226];//走過路徑
		int dir[]={1,16,15,14,-1,-16,-15,-14};//八方位陣列
	
		try {
			reader = new InputStreamReader(new FileInputStream(file));
            int temp;
            //↓↓↓↓↓↓↓↓↓↓↓↓讀檔↓↓↓↓↓↓↓↓↓↓↓↓↓↓
            while ((temp = (int)reader.read()) != -1) {//逐字讀取
            	if (((char) temp) != '\r') {//跳過換行符號
            		if(temp==48){//讀取為0時
            			ma[np]=0;
            			np++;	
	            	}else if(temp==49){//讀取為1時
	            		ma[np]=1;
	            		np++;
	            	}
	            }    	
            }
            np=1;
            System.out.print("\n\n您的迷宮為:\n");
            //↓↓↓↓↓↓↓↓↓↓↓↓輸出讀取檔案↓↓↓↓↓↓↓↓↓↓↓↓
             	for(int i=1;i<=225;i++){
             		if(i!=1){
	             		if(i%15==1){
		            		System.out.println();
		            	}
             		}
            	System.out.print(ma[i]);
            }
            //↑↑↑↑↑↑↑↑↑↑↑↑輸出讀取檔案↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑-	
	        
             while(np!=225){//規劃路徑
	        	for(int i=0;i<=7;i++){//八方位探路
	        		if(np+dir[i]>0){//不超過地圖邊界
	        			if(ma[np+dir[i]]==0){//若有路
	        				ma[np]=2;//寫入2
	        				tma[tmai]=np;//紀錄路徑
	        				tmai++;
	        				np=np+dir[i];//前進
	        				nr=0;//非死路  死路判斷歸0
	        				//System.out.print(np+" "+ma[np]+" , ");
	        				if(np==225){//若為出口
	        	        		ma[np]=2;
	        	        	}
	        				break;
	        			}
	        		}
	        		nr++;//此方向不通
	        	}
	        	if(nr==8){//八方死路
	        		ma[np]=4;//寫入4
	        		tmai--;//回到上一個紀錄點
	        		np=tma[tmai];
	        		nr=0;//重新探路
	        		//System.out.print(np+" "+ma[np]+" , ");
	        	}
	        }

	      //↓↓↓↓↓↓↓↓↓↓↓↓寫檔↓↓↓↓↓↓↓↓↓↓↓↓↓↓
	        java.io.File write_file = new java.io.File( "s101502517.txt" );
	        java.io.PrintWriter bw = new java.io.PrintWriter(write_file);
	        for (int i = 1,k=0; i <=225 ; i++) {
	        	if(i!=1){
	        		if(i%15==1){
	        			bw.println();
	        		}
	        	}
	        	bw.print(ma[i]);	
	        }
	        bw.close();//關檔
	        System.out.println("\n\n\n---路線已畫出: s101502517.txt---");
	      
	      //↑↑↑↑↑↑↑↑↑↑↑↑↑寫檔↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑
	      
	       	//↓↓↓↓↓↓↓確認迷宮路徑↓↓↓↓↓↓
	        /*
	        for(int i=1;i<=225;i++){
            	if(i%15==1){
            		System.out.println();
            	}
            	System.out.print(ma[i]);
            }
			*/

        } catch (IOException e) {//當錯誤時
            System.out.println("Error Loading!!");
        	e.printStackTrace();
            return;
        }

	}
}