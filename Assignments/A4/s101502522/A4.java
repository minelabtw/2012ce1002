package ce1002.A4.s101502522;
import java.io.*;
import java.util.Scanner;
import java.util.Stack;
public class A4 {
	public static void main (String[] args) throws IOException
	{
		System.out.println("請輸入一個 15*15 的迷宮: (能走為 0 ,牆壁為 1)");
		System.out.println("( 規定:左上角[0][0]為入口, 右下角[14][14]為出口, 其餘邊緣皆是牆壁 )");
		System.out.println("＊注意:測出路線之後,已走過路線會被記為2,若是死路則記為4");
		System.out.println("請輸入要讀取的檔案路徑 (例: c:\\maze.txt): ");
		Scanner input=new Scanner(System.in);
		System.out.println("您的迷宮為:");
		String mazeaddress; //迷宮檔案位置
		mazeaddress=input.next();
		char[][] maze=new char[17][17];
		readmaze(mazeaddress,maze);
		judge(maze);
		writefile(maze);
	}
	private static void readmaze(String mazeaddress,char[][] maze) throws IOException { //讀取迷宮檔案
		BufferedReader mazedata=new BufferedReader(new FileReader(mazeaddress));
		for(int a=0;a<17;a++) //為迷宮加一圈外層，以防系統出錯
		{
			maze[a][0]='1';
			maze[0][a]='1';
			maze[16][a]='1';
			maze[a][16]='1';
		}
		
		for(int i=1;i<16;i++)
		{
			for(int j=1;j<16;j++)
			{	
				char read=(char)mazedata.read(); //讀取迷宮數值，改為字元
				while(read!='1'&&read!='0')//只讀取正確迷宮數值
					read=(char)mazedata.read();
				maze[i][j]=read;
				System.out.print(maze[i][j]);
			}
			System.out.println();
		}
	}
	
	private static void judge(char[][] maze){ //判斷行走路線
		int direction[][]={{0,1},{1,1},{1,0},{1,-1},{0,-1},{-1,1},{-1,0},{-1,-1}}; //行走路線的八個方向
		int x=1,y=1;
		Stack<Integer> xstack=new Stack<Integer>(); //用stack去記錄走過的路線
		Stack<Integer> ystack=new Stack<Integer>();
		boolean dead=false;
		maze[1][1]='2'; //開始的地方先設為走過
		
		while(maze[15][15]=='0') //判斷通路或死路，重複迴圈直至走出迷宮
		{
			for(int i=0 ;i<8 ;i++)
			{
				if(maze[x+direction[i][0]][y+direction[i][1]]=='0') //走八個方向
				{
					x=x+direction[i][0];
					y=y+direction[i][1];
					maze[x][y]='2';
					xstack.push(x); //紀錄走過的路線
					ystack.push(y);
					break;
				}
				if(i==7) //八個方向都不能走時，紀錄為死路
					dead=true;
			}
			if(dead==true) //死路判定
			{
				maze[x][y]='4';
				x=xstack.pop(); //判斷為死路後，回上一個動作繼續判斷是否為死路或通路
				y=ystack.pop();
				dead=false;
			}
		}
	}
	private static void writefile(char[][] maze) throws IOException { //寫入迷宮走法檔案
		 java.io.File write_file = new java.io.File("s101502522.txt"); 
		 java.io.PrintWriter mazeanswer =new java.io.PrintWriter(write_file);
		 for(int x=1 ;x<16;x++) //寫入迷宮走法腳本
		 {
			 for(int y=1;y<16;y++)
				 mazeanswer.print(maze[x][y]);
			 mazeanswer.print((char)13); //換行
			 mazeanswer.print((char)10);
		 }
		 mazeanswer.close(); //關閉寫入器
		 System.out.print("---路線已畫出: s101502522.txt---");
	}

}