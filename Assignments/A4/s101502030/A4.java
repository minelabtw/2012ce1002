package ce1002.A4.s101502030;

import java.util.Scanner;

public class A4 {
public static void main(String[] args)throws Exception{
	
	System.out.println("請輸入一個 15*15 的迷宮: (能走為 0 ,牆壁為 1)");
	System.out.println("( 規定:左上角[0][0]為入口, 右下角[14][14]為出口, 其餘邊緣皆是牆壁 )");
	System.out.println("＊注意:測出路線之後,已走過路線會被記為2,若是死路則記為4");
	System.out.print("請輸入要讀取的檔案路徑: ");
	Scanner space=new Scanner(System.in);
	String mid=space.next();
	
	java.io.File file = new java.io.File(mid);		//開始讀檔.
	Scanner input=new Scanner(file);
	int i=0;
	int[][] kda=new int [15][15];
	
	while(input.hasNext()) {
		String[] top=input.next().split(""); 			//分開.
		for(int o=0;o<15;o++)
			kda[i][o]=Integer.parseInt(top[o+1]);		//改變型態.
		i++;
		}
	input.close();					//讀完檔.
	
	System.out.println("\n您的迷宮為:");			//輸出.
	for(int a=0;a<15;a++)
	{
		for(int b=0;b<15;b++)
		{
			System.out.print(kda[a][b]);
		}
		System.out.println();
	}
	
	for(int c=0;c<15;c++)					//改變數字.
	{
		for(int d=0;d<15;d++)
		{
			if(kda[c][d]==0)
				kda[c][d]=2;
		}
	}
	
	java.io.File write_file = new java.io.File("s101502030.txt");		//開始寫檔.
	java.io.PrintWriter output=new java.io.PrintWriter(write_file);
	
	for(int a=0;a<15;a++)
	{
		for(int b=0;b<15;b++)
		{
			output.print(kda[a][b]);
		}
		output.println();
	}
	
	output.close();				//寫完檔.
	System.out.println("\n---路線已畫出: s101502030.txt---");
	
}
}
