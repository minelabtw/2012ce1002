package ce1002.A4.s101502526;
import java.util.*;
import java.io.*;
import java.util.Scanner;
public class A4 {


	public static void main(String[] args) throws Exception {
		Scanner site= new Scanner(System.in);
		FileWriter fw =null;
		BufferedWriter bw =null;
		fw = new FileWriter("s101502526.txt ", false); 
		bw = new BufferedWriter(fw);
		Stack<Integer> stack= new Stack<Integer>();               //從網路找的輸入檔案及輸出檔案
		String read = "";                                         //其實還是不是很懂這兩個的用法
		String plus = "" ;
		String site2 = "";
		int count = 0 ;
		char[][]array = new char[15][15] ;
		System.out.println("請輸入一個 15*15 的迷宮: (能走為 0 ,牆壁為 1)");
		System.out.println("( 規定:左上角[0][0]為入口, 右下角[14][14]為出口, 其餘邊緣皆是牆壁 ) ");
		System.out.println("＊注意:測出路線之後,已走過路線會被記為2,若是死路則記為4 ");
		System.out.print("請輸入要讀取的檔案路徑 (例: c:\\maze.txt): ");
		site2 = site.next(); 
		java.io.File readin = new java.io.File(site2);
		Scanner input = new Scanner(readin);
		while (input.hasNext())                                    //直到文字檔結束前都放進一個字串
			read = read + input.next();
		for (int i = 0 ; i < 15 ; i++)
		{
			for (int j = 0 ; j < 15 ; j++)
			{
				array[i][j] =  (char) read.codePointAt(count);     //字串一個一個拆解放到二維陣列
				count += 1 ;
			}
		}
		for (int i = 0 , j = 0 ; j < 15 ; j++)                     //原地若不是死路則直接判斷為活路，活路則將座標放入陣列中
		{
			if (array[i][j] != '4')
			{
				array[i][j] = '2';
				stack.push(i);
				stack.push(j);
			}
			if (i >= 0 && i <=14 && j+1 >=0 && j+1<=14 && array[i][j+1] == '0')    //向右前進且不到底
			{
				continue;
			}
			else if ( i-1 >= 0 && i-1 <=14 && j-1 >=0 && j-1 <=14 && array[i-1][j-1] == '0')   //向左上前進且不到底
			{
				i -= 1;
				j -= 2 ;
			}
			else if ( i-1 >= 0 && i-1 <=14 && j >=0 && j <=14 && array[i-1][j] == '0')   //向上前進且不到底
			{
				i -= 1;
				j -= 1 ;
			}
			else if ( i-1 >= 0 && i-1 <=14 && j+1 >=0 && j+1 <=14 && array[i-1][j+1] == '0')   //向右上前進且不到底
			{
				i -= 1;
			}
			else if ( i+1 >= 0 && i+1 <=14 && j+1 >=0 && j+1 <=14 && array[i+1][j+1] == '0')   //向右下前進且不到底
			{
				i += 1;
			}
			else if ( i+1 >= 0 && i+1 <=14 && j >=0 && j<=14 && array[i+1][j] == '0')   //向下前進且不到底
			{
				i+=1;
				j-=1;
			}
			else if ( i+1 >= 0 && i+1 <=14 && j-1 >=0 && j-1<=14 && array[i+1][j-1] == '0')   //向左下前進且不到底
			{
				i+=1;
				j-=2;
			}
			else if ( i >= 0 && i <=14 && j-1 >=0 && j-1<=14 && array[i][j-1] == '0')   //向左前進且不到底
			{
				i+=1;
				j-=2;
			}
			else if ( i != 14 && j!=14)    //若都無通路則為死路，且彈出座標，回到上一個活路
			{
				stack.pop();
				stack.pop();
				array[i][j] = '4';
				j = stack.get(stack.size()-1)-1;
				i = stack.get(stack.size()-2);
			}
		}
		for (int i = 0 ; i < 15 ; i++)    //寫入檔案
		{
			for (int j = 0 ; j < 15 ; j++)
			{
				bw.write(array[i][j]);
			}
			bw.newLine();
		}
		bw.flush();
	}

}
