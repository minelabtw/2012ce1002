package ce1002.A4.s101502520;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Scanner;

public class A4 {
	public static char[][] map = new char[15][15];
	public static boolean finished = false;
	
	/**
	 * function "readMap" can read a 15*15 map into 15*15 char array
	 * @param pathname the location of map file
	 */
	public static void readMap(String pathname) {
		BufferedReader br = null;
		try {
 
			String sCurrentLine;
			br = new BufferedReader(new FileReader(pathname));
 
			for (int i = 0; i < 15 && (sCurrentLine = br.readLine()) != null; i++) {
				for(int j = 0; j < 15; j++)
					map[i][j] = sCurrentLine.charAt(j); 
			}
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			try {
				if (br != null)br.close();
			} catch (IOException ex) {
				ex.printStackTrace();
			}
		}
	}
	
	/**function "writeRoute" generate the map with the route named s101502520.txt at defualt direction.
	 * 
	 */
	public static void writeRoute() throws IOException{
		FileWriter fw = new FileWriter("s101502520.txt");
		for(int i = 0; i < 15; i++, fw.append("\r\n"))
			for(int j = 0; j < 15; j++)
				fw.append(map[i][j]);
		fw.close();
		System.out.print("---路線已畫出: s101502520.txt---");
	}
	
	/**
	 * function "findRoute" will recursively go to the eight directions of current location on the map,
	 * and return true if it is a dead end.
	 * @param i the i-index of map
	 * @param j the j-index of map
	 * @return boolean to decide current location is a dead end or not
	 */
	public static boolean findRoute(int i, int j){
		if(finished) return false;//if the route if is find then end the findRoute function
			
		boolean deadend = true;//assume current location is a dead end
		
		map[i][j] = '2';//mark '2' first to avoid recursive back
		
		if(i == 14 && j ==14){
			finished = true;
			return false;
		}
		
		if(j < 14 && map[i][j + 1] == '0')
			deadend = findRoute(i, j + 1);		
		if(i < 14 && j < 14 && map[i + 1][j + 1] == '0')
			deadend = findRoute(i + 1, j + 1);		
		if(i < 14 && map[i + 1][j] == '0')
			deadend = findRoute(i + 1, j);		
		if(i > 0 && j < 14 && map[i - 1][j + 1] == '0')
			deadend = findRoute(i - 1, j + 1);		
		if(i < 14 && j > 0 && map[i + 1][j - 1] == '0')
			deadend = findRoute(i + 1, j - 1);		
		if(i > 0 && map[i - 1][j] == '0')
			deadend = findRoute(i - 1, j);		
		if(j > 0 && map[i][j - 1] == '0')
			deadend = findRoute(i, j - 1);
		if(i > 0 && j > 0 && map[i - 1][j - 1] == '0')
			deadend = findRoute(i - 1, j - 1);
		if(deadend){
			map[i][j] = '4';
			return true;
		}		
		return false;		
	}

	public static void main(String[] args) {
		Scanner inputsScanner = new Scanner(System.in);
		System.out.println("請輸入一個 15*15 的迷宮: (能走為 0 ,牆壁為 1)");
		System.out.println("( 規定:左上角[0][0]為入口, 右下角[14][14]為出口, 其餘邊緣皆是牆壁 )");
		System.out.println("＊注意:測出路線之後,已走過路線會被記為2,若是死路則記為4");
		System.out.print("請輸入要讀取的檔案路徑 (例: c:\\maze.txt): ");
		String pathname = inputsScanner.nextLine();
		inputsScanner.close();
		readMap(pathname);
		System.out.println();
		System.out.println("您的迷宮為:");
		for(int i = 0; i < 15; i++, System.out.println())
			for(int j = 0; j < 15; j++)
				System.out.print(map[i][j]);
		System.out.println();
		findRoute(0, 0);
		
		try {
			writeRoute();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}
