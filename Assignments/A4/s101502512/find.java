package ce1002.A4.s101502512;

import java.io.FileNotFoundException;
//找出口
public class find {
	
	public void walk(int maze[][], int i, int j) throws Exception {
		write wr = new write();
		// 把走過的路以2表示
		maze[i][j] = 2;

		// 走到終點就寫入
		if (i == 13 && j == 13) 
			wr.writedata(maze);
		
		// 從右邊以順時鐘方向找起
		if (maze[i][j + 1] == 0)
			walk(maze, i, j + 1);
		// 如果是0就往右下走
		if (maze[i + 1][j + 1] == 0)
			walk(maze, i + 1, j + 1);
		// 如果是0就往下走
		if (maze[i + 1][j] == 0)
			walk(maze, i + 1, j);
		// 如果是0就往左下走
		if (maze[i + 1][j - 1] == 0)
			walk(maze, i + 1, j - 1);
		// 如果是0就往左走
		if (maze[i][j - 1] == 0)
			walk(maze, i, j - 1);
		// 如果是0就往左上走
		if (maze[i - 1][j - 1] == 0)
			walk(maze, i - 1, j - 1);
		// 如果是0就往上走
		if (maze[i - 1][j] == 0)
			walk(maze, i - 1, j);
		// 如果是0就往右上走
		if (maze[i - 1][j + 1] == 0)
			walk(maze, i - 1, j + 1);	
		//死路
		maze[i][j] = 4;
		
	}
}
