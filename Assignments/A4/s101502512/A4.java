package ce1002.A4.s101502512;

import java.util.Scanner;
import java.io.*;

public class A4 {
	public static void main(String[] args) throws Exception {
		find fi = new find();
		int[][] maze = new int[15][15];
		System.out.println("請輸入一個 15*15 的迷宮: (能走為 0 ,牆壁為 1)");
		System.out.println("( 規定:左上角[0][0]為入口, 右下角[14][14]為出口, 其餘邊緣皆是牆壁 )");
		System.out.println("＊注意:測出路線之後,已走過路線會被記為2,若是死路則記為4");
		System.out.print("請輸入要讀取的檔案路徑 (例: c:\\maze.txt): ");
		//輸入路徑
		Scanner ReadData = new Scanner(System.in);
		String file = ReadData.next();
		System.out.println();
		System.out.println("您的迷宮為:");
		//讀檔
		File fr = new File(file);
		Scanner input = new Scanner(fr);
		int k = 0;
		while (input.hasNext()) {
			String[] number = input.next().split("");
			for (int i = 1; i <= 15; i++) {
				maze[k][i - 1] = Integer.parseInt(number[i]);//變回數字
			}
			k++;
		}
		input.close();
		//輸出迷宮
		for (int i = 0; i < 15; i++) {
			for (int j = 0; j < 15; j++) {
				System.out.print(maze[i][j]);
			}
			System.out.println();
		}
		System.out.println();
		System.out.println("---路線已畫出: s101502512.txt---");
		//直接把出口跟入口設定為走過
		maze[0][0]=2;
		maze[14][14]=2;
		
		fi.walk(maze, 1, 1);
	}
}
