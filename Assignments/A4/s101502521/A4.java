package ce1002.A4.s101502521;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Scanner;

public class A4 {
	static int dx[] = new int[] {-1,-1,-1,0,0,1,1,1}; //定義方向
	static int dy[] = new int[] {-1,0,1,-1,1,-1,0,1};
	public static void main(String[] args) throws IOException
	{
		System.out.println("請輸入一個 15*15 的迷宮: (能走為 0 ,牆壁為 1)");
		System.out.println("( 規定:左上角[0][0]為入口, 右下角[14][14]為出口, 其餘邊緣皆是牆壁 )");
		System.out.println("＊注意:測出路線之後,已走過路線會被記為2,若是死路則記為4");
		System.out.print("請輸入要讀取的檔案路徑 (例: c:\\maze.txt): ");
		Scanner input = new Scanner(System.in);
		String path = input.nextLine();
		System.out.println();
		System.out.println("您的迷宮為:");
		BufferedReader in = new BufferedReader(new FileReader(path));
		String tmp;
		int G[][] = new int[15][15];
		for(int i=0;i<15;i++)
		{
			tmp = in.readLine();
			for(int j=0;j<15;j++) //讀檔並轉為INT陣列
				if(tmp.substring(j,j+1).equals("0"))
					G[i][j] = 0;
				else
					G[i][j] = 1;
			System.out.println(tmp);
		}
		gogo(G, 0, 0);//從起點
		FileWriter fwriter=new FileWriter("s101502521.txt");
		for(int i=0;i<15;i++,fwriter.write("\r\n"))
			for(int j=0;j<15;j++)
				fwriter.write(Integer.toString(G[i][j]));
		fwriter.close();
		System.out.println("");
		System.out.println("---路線已畫出: s101502521.txt---");
		
	}
	public static int gogo(int G[][],int x, int y) // 找到終點: return 1; else 0
	{
		if(x<0||y<0||x>=15||y>=15||G[x][y]!=0)//邊界
			return 0;
		if(x==14&&y==14&&G[x][y]==0)//終點
		{
			G[x][y] = 2;
			return 1;
		}
		G[x][y] = 4;
		for(int d=0;d<8;d++)
		{
			int nx = x + dx[d];
			int ny = y + dy[d];
			if(gogo(G, nx, ny)==1)//不是死路
			{
				G[x][y]=2;
				return 1;//找到即退回上層
			}
		}
		return 0;
	}
}
