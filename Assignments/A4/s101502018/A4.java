package ce1002.A4.s101502018;
import java.io.*;
import java.util.Scanner;
public class A4 {
	public static void main(String[] args) throws IOException {
		Scanner x = new Scanner(System.in);
		String xyz;
		System.out.println("請輸入一個 15*15 的迷宮: (能走為 0 ,牆壁為 1)");
		System.out.println("( 規定:左上角[0][0]為入口, 右下角[14][14]為出口, 其餘邊緣皆是牆壁 )");
		System.out.println("＊注意:測出路線之後,已走過路線會被記為2,若是死路則記為4");
		System.out.print("請輸入要讀取的檔案路徑 (例: c:\\maze.txt): ");
		String p = x.nextLine();
		System.out.println("您的迷宮為:");
		BufferedReader read = new BufferedReader(new FileReader(p));
		int map[][] = new int[15][15];
		for (int i = 0; i < 15; i++) {
			xyz = read.readLine();
			System.out.println(xyz);
			for (int j = 0; j < 15; j++) {
				if (xyz.substring(j, j + 1).equals("0"))
					map[i][j] = 0;
				else
					map[i][j] = 1;
			}
		}
		m = map;
		map(0, 0);
		System.out.print("\n\n");
		FileWriter writer = new FileWriter("s101502018.txt");
		for (int i = 0; i < 15; i++) {
			for (int j = 0; j < 15; j++) {
				writer.write(String.valueOf(m[i][j]));
			}
			writer.write(String.format("%n"));
		}
		System.out.printf("---路線已畫出: s101502018.txt---");
		writer.close();
		}
	private static int[][] m;
	private static boolean judge = false;
	public static void map(int i, int j) {
		m[i][j] = 2;
		if (i ==14 && j==14) {
			judge = true;
			return;
		}
		if (judge==false)
			if (m[i][j + 1] == 0 && judge == false)
			{
				map(i, j + 1);
			}
		if (judge==false)
			if (m[i + 1][j + 1] == 0 && judge == false)
			{
				map(i + 1, j + 1);
			}
		if (judge==false)
			if (m[i + 1][j] == 0 && judge == false)
			{
				map(i + 1, j);
			}
		if (judge==false)
			if (m[i + 1][j - 1] == 0 && judge == false)
			{
				map(i + 1, j - 1);
			}
		if (judge==false)
			if (m[i][j - 1] == 0 && judge == false)
			{
				map(i, j - 1);
			}
		if (judge==false)
			if (m[i - 1][j - 1] == 0 && judge == false)
			{
				map(i - 1, j - 1);
			}
		if (judge==false)
			if (m[i - 1][j] == 0 && judge == false)
			{
				map(i - 1, j);
			}
		if (judge==false)
			if (m[i - 1][j + 1] == 0 && judge == false)
			{
				map(i - 1, j + 1);
			}
		if (judge==false)
			m[i][j] = 4;
		return;
}
}
