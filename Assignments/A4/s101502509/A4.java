package ce1002.A4.s101502509;
import java.io.*;
import java.util.Scanner;

public class A4 {
	static int rowsize = 15, colsize = 15;           //maze size
	static int maze[][] = new int[15][15];
	static int directionRow[] = {0, 1, 1,  1,  0, -1, -1, -1};  //direction array from east to northeast clockwise
	static int directionCol[] = {1, 1, 0, -1, -1, -1,  0,  1};
	static int startRow = 0, startCol = 0, endRow = 14, endCol = 14; //entrance coordinate and exit coordinate

	private static boolean inMaze(int x, int y) {		//check the element whether in the maze
		return x >= 0 && x < rowsize && y >= 0 && y < colsize;
	}

	private static boolean findPath(int row, int col) {	//findPath function
		if(row == endRow && col == endCol) {	//if arrive the exit coordinate, roadPath is found
			maze[row][col] = '2';
			return true;
		} else if (maze[row][col] == '1') {		//walk into wall
			return false;
		} else if (maze[row][col] == '0') {		//the way can be walked
			maze[row][col] = '2';
			boolean goTag = false;
			for (int j = 0; j < 8; j++) {		//check the eight directions of the coordinate if can walk
				if (inMaze(row + directionRow[j], col + directionCol[j])
					&& (findPath(row + directionRow[j], col + directionCol[j]))) {
					goTag = true;
					break;
				}
			}
			if (goTag) {
				maze[row][col] = '2';		//the coordinate can be walked
				return true;
			} else {
				maze[row][col] = '4';		//the coordinate can not be walked
				return false;
			}
		} else {
			return false;
		}
	}

	public static void main(String[] args) throws IOException {
		// TODO Auto-generated method stub
		System.out.println("請輸入一個 15*15 的迷宮: (能走為 0 ,牆壁為 1)");
		System.out.println("( 規定:左上角[0][0]為入口, 右下角[14][14]為出口, 其餘邊緣皆是牆壁 )");
		System.out.println("＊注意:測出路線之後,已走過路線會被記為2,若是死路則記為4");
		System.out.print("請輸入要讀取的檔案路徑 (例: c:\\maze.txt): ");
		String inputFile = new Scanner(System.in).next();
		FileInputStream fis = new FileInputStream(inputFile);		//read file from input
		try {
			for(int i = 0; i < 15; i++)
				for(int j = 0; j < 15; j++) {
					int buffer = fis.read();
					if(buffer == '\r' || buffer == '\n') {	//if read change line token, ignore it
						j--;
						continue;
					} else if(buffer != -1) {		//while file Read is not finished 
						maze[i][j] = buffer;
					} else {
						break;
					}
				}
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			fis.close();
		}

		System.out.println("\n您的迷宮為:");		//print the question maze
		for(int i = 0; i < 15; i++) {
			for(int j = 0; j < 15; j++) {
				System.out.print((char)maze[i][j]);
			}
			System.out.println();
		}
		
		System.out.println("\n---路線已畫出: s101502509.txt---");	
		findPath(startRow,startCol);		//find maze path from entrance coordinate
		BufferedWriter buf = new BufferedWriter (new FileWriter("s101502509.txt"));
		try {								//write the result into file("s101502509.txt")
			for(int i = 0; i < 15; i++) {
				for(int j = 0; j < 15; j++) {
					buf.write(maze[i][j]);
				} 
				buf.newLine(); //change line
			}
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			buf.close();
		}
	} 
}