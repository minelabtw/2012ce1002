package ce1002.A4.s101502556;
import java.io.*;
import java.util.*;
public class A4 {
	public static int maze [][]=new int[15][15];
	public static void main(String[] args) throws IOException {
		 Scanner input=new Scanner(System.in);
		 System.out.println("請輸入一個 15*15 的迷宮: (能走為 0 ,牆壁為 1)");//題目說明
		 System.out.println("( 規定:左上角[0][0]為入口, 右下角[14][14]為出口, 其餘邊緣皆是牆壁 )");
		 System.out.println("＊注意:測出路線之後,已走過路線會被記為2,若是死路則記為4");
		 System.out.print("請輸入要讀取的檔案路徑 (例: c:\\maze.txt): ");
		 int y=0;
		 BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
		 String line=in.readLine();//讀入路徑
		 in = new BufferedReader(new FileReader(line));
		 System.out.println("\n您的迷宮為:");
		 while ((line =in.readLine())!=null) 
		 {
			for(int x=0;x<15;x++)
			{
				maze[y][x]=line.charAt(x)-48;
			}
			y++;
		 }
		 for(int a=0;a<15;a++)
			 for(int b=0;b<15;b++)
			 {
				 System.out.print(maze[a][b]);
				 if(b==14)
				 {
					 System.out.println();
				 }
			 }
		 int i=0,j=0;
		 maze[i][j]=2;
		 maze(i,j);//呼叫function
		 java.io.File write_file = new java.io.File( "s101502556.txt");//輸出檔案
		 if (!write_file.exists()) {
			write_file.createNewFile();
		 }
		 FileWriter fw = new FileWriter(write_file.getAbsoluteFile());
		 BufferedWriter bw = new BufferedWriter(fw);
		 for(int a=0;a<15;a++){  //輸出迷宮
			 for(int b=0;b<15;b++)
				 bw.write(maze[a][b]+48);
			 bw.write("\n");
			}
		 bw.close();
		 System.out.println("\n---路線已畫出: s101502556.txt---");//結束
	}	 
	public static void maze(int i ,int j)//尋找0並標記為2以及紀錄死路
	{
		Stack<Integer> stackx = new Stack<Integer>();
		Stack<Integer> stacky = new Stack<Integer>();
		int way[][] = {{0,1},{1,1},{1,0},{1,-1},{0,-1},{-1,-1},{-1,0},{-1,1}}; 
		stackx.push(i);
		stacky.push(j);
		while(!(i==14&&j==14))
		{
			int z=0;
			for(;z<8;z++){
				if(i+way[z][0]<=14&&i+way[z][0]>=0&&j+way[z][1]>=0&&j+way[z][1]<=14&&maze[i+way[z][0]][j+way[z][1]]==0)	//如果有0代表有路遞迴進去
				{
					j=j+way[z][1];
					i=i+way[z][0];
					maze[i][j]=2;
					stackx.push(i);
					stacky.push(j);
					break;	
				}  
			}	 
			if(z==8)
			{
				maze[i][j]=4;
				stackx.pop();
				stacky.pop();
				
				i=stackx.peek();
				j=stacky.peek();
			}	 
		}		 
 }
}
