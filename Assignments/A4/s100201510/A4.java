package ce1002.A4.s100201510;

import java.io.*;
import java.util.Scanner;

public class A4 {
	private static final int E = 0 , SE = 1 , S = 2 , SW = 3 , W = 4 , NW = 5 , N = 6 , NE = 7 ;
	private static int[][] mazeMAP = new int[15][15];
	private static int direction = E ; // the direction of little mouse.
	private static int Xaxis = 0 , Yaxis = 0 ;// the coordinate of little mouse.
	// function use to move little mouse.
	public static void move(int way){
		if(way == E){
			Xaxis++;
		}
		else if(way == SE){
			Xaxis++;
			Yaxis++;
		}
		else if(way == S){
			Yaxis++;
		}
		else if(way == SW){
			Xaxis--;
			Yaxis++;
		}
		else if(way == W){
			Xaxis--;
		}
		else if(way == NW){
			Xaxis--;
			Yaxis++;
		}
		else if(way == N){
			Yaxis--;
		}
		else if(way == NE){
			Xaxis++;
			Yaxis--;
		}
	} 
	// function return the "thing" ahead of the mouse.
	private static int mouseheadto(int way){
		if(way%8 == E)
			return mazeMAP[Yaxis][Xaxis + 1];
		else if(way%8 == SE)
			return mazeMAP[Yaxis + 1][Xaxis + 1];
		else if(way%8 == S)
			return mazeMAP[Yaxis + 1][Xaxis];
		else if(way%8 == SW)
			return mazeMAP[Yaxis + 1][Xaxis - 1];
		else if(way%8 == W)
			return mazeMAP[Yaxis][Xaxis - 1];
		else if(way%8 == NW)
			return mazeMAP[Yaxis - 1][Xaxis - 1];
		else if(way%8 == N)
			return mazeMAP[Yaxis - 1][Xaxis];
		else if(way%8 == NE)
			return mazeMAP[Yaxis - 1][Xaxis + 1];
		return 0;
	}
	// function determine which way to go.
	public static int checkdirection(){
		int tempdir = direction;
		while(mouseheadto(tempdir) != 0 || mouseheadto(tempdir) != 2){
			if(mouseheadto((tempdir + 2)%8) == 0){
				direction = (tempdir + 2)%8;
				break;
			}
			else if(mouseheadto((tempdir + 1)%8) == 0){
				direction = (tempdir + 1)%8;
				break;
			}
			else if(mouseheadto(tempdir%8) == 0){
				direction = tempdir%8;
				break;
			}
			else if(mouseheadto((tempdir+7)%8) == 0){
				direction = (tempdir+7)%8;
				break;
			}
			else if(mouseheadto((tempdir+6)%8) == 0){
				direction = (tempdir+6)%8;
				break;
			}
			else if(mouseheadto((tempdir + 2)%8) == 2){
				direction = (tempdir + 2)%8;
				break;
			}
			else if(mouseheadto((tempdir + 1)%8) == 2){
				direction = (tempdir + 1)%8;
				break;
			}
			else if(mouseheadto(tempdir%8) == 2){
				direction = tempdir%8;
				break;
			}
			else if(mouseheadto((tempdir+7)%8) == 2){
				direction = (tempdir+7)%8;
				break;
			}
			else if(mouseheadto((tempdir+6)%8) == 2){
				direction = (tempdir+6)%8;
				break;
			}
			tempdir++;
		}
		return direction;
	}
	public static void main(String[] args) throws Exception {
		System.out.println("請輸入一個 15*15 的迷宮: (能走為 0 ,牆壁為 1)");
		System.out.println("( 規定:左上角[0][0]為入口, 右下角[14][14]為出口, 其餘邊緣皆是牆壁 )");
		System.out.println("＊注意:測出路線之後,已走過路線會被記為2,若是死路則記為4");
		System.out.print("請輸入要讀取的檔案路徑 (例: c:\\maze.txt): ");
		// load the maze from file into buff.
		Scanner input = new Scanner(System.in);
		String filePATH = input.nextLine();
		FileReader sourcefile = new FileReader(filePATH);
		char buff[] = new char[256];
		sourcefile.read(buff);
		int k = 0;
		for(int i = 0 ; i < 15*15 ; i++){ // load the map into array.
			mazeMAP[i/15][i%15] = (int)buff[i + 2*k] - '0';
			if((i + 1)%15 == 0)
				k++;
		}
		input.close();
		sourcefile.close();
		System.out.println("您的迷宮為:\n");
		//print the maze
		for(int i = 0 ; i < 15 ; i++){
        	for(int j = 0 ; j < 15 ; j++){
        		System.out.printf("%d",mazeMAP[i][j]);
        	}
        	System.out.print("\n");
        }
		//start solving the maze
		//little mouse will always turn right. start at left-up corner.  
		// check direction, if the path has been walked, mark it with "2", if it is dead end, mark with "4".
		while(!(Xaxis == 14 && Yaxis == 14)){// end if little mouse reach the goal.
			//System.out.printf("X = %d , Y = %d , direction %d \n" , Xaxis , Yaxis , direction);
			move(checkdirection());
			mazeMAP[Yaxis][Xaxis] += 2;
		}
		
		System.out.println("---路線已畫出: s100201510.txt---");
		//output the result.
		File write_file = new File( "s100201510.txt" );
		PrintWriter output = new PrintWriter(write_file);
		for(int i = 0 ; i < 15 ; i++){
			for(int j = 0 ; j < 15 ; j++){
				output.print(mazeMAP[i][j]);
			}
			output.print("\r\n");
		}
		output.close();	
	}
}