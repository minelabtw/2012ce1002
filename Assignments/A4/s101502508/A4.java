package ce1002.A4.s101502508;

import java.util.Scanner ;
import java.io.BufferedReader;
import java.io.File ;
import java.io.FileReader;
import java.io.IOException;

public class A4 {
	public static void main(String args[]) throws Exception{
		
		Scanner input = new Scanner(System.in) ;
		int [][] Maze = new int [15][15] ;
		
		System.out.println("請輸入一個 15*15 的迷宮: (能走為 0 ,牆壁為 1)") ;
		System.out.println("( 規定:左上角[0][0]為入口, 右下角[14][14]為出口, 其餘邊緣皆是牆壁 )") ;
		System.out.println("＊注意:測出路線之後,已走過路線會被記為2,若是死路則記為4") ;
		System.out.print("請輸入要讀取的檔案路徑 (例: c:\\maze.txt): ") ;
		
		String FileSite ;
		FileSite = input.nextLine() ;
		
		Maze = ReadFile(FileSite) ;
		
		
		
	}
	
	public static int[][] ReadFile( String site ) throws Exception{
		
		java.io.File file = new java.io.File( site ) ;//讀取檔案
		int [][] maze = new int [15][15] ;
		
		String str ;
		int strInt, n=0 ;
		Scanner input = new Scanner (file) ;
		
		while ( input.hasNext() ){
			str = input.next() ; //一行一行做輸入
			strInt=Integer.parseInt(str);//string 轉  int
				for( int m=14 ; m>=0 ; m-- )					
					maze[n][m] = strInt % 10 ;
			
			n++ ;
		}

		return maze ;
	}
	
	public static void WriteFile() throws Exception{
		
		File newTxt = new File("s101502508.txt");

        if( !newTxt.exists() ){
             //在此java檔目錄下建立test.txt檔
             newTxt.createNewFile();
        }
        else{
             System.out.println("檔案已存在!");
        }

	}
	
	
}


