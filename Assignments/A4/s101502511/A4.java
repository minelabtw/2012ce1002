package ce1002.A4.s101502511;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.BufferedReader;
import java.util.*;

public class A4 {

	public static void main(String[] args) throws FileNotFoundException,
			IOException {

		System.out.println("請輸入一個 15*15 的迷宮: (能走為 0 ,牆壁為 1\n"
				+ "( 規定:左上角[0][0]為入口, 右下角[14][14]為出口, 其餘邊緣皆是牆壁 )\n"
				+ "＊注意:測出路線之後,已走過路線會被記為2,若是死路則記為4\n"
				+ "請輸入要讀取的檔案路徑 (例: c:\\maze.txt): ");
		Scanner scanner = new Scanner(System.in);
		String filename = scanner.next();
		System.out.println("\n您的迷宮為:");
		// 開檔
		BufferedReader input = new BufferedReader(new FileReader(filename));
		String read;
		int maze[][] = new int[15][15];// 儲存迷宮的陣列
		int k = 0;
		while ((read = input.readLine()) != null) {// 讀檔
			String read1[] = read.split("");// 切割每行的數字
			for (int i = 1; i <= 15; i++) {// 分別儲存到陣列
				maze[k][i - 1] = Integer.parseInt(read1[i]);//Integer.parseInt網路找的
			}                                               //String轉換成integer
			k++;
			System.out.println(read);// 輸出迷宮
		}
        /*有方法能用1個stack就好嗎?*/
		Stack stx = new Stack();// 紀錄x座標
		Stack sty = new Stack();// y座標

		int j = 0, i = 0;//初始座標,j為X,i為Y
		while (j < 14 && i < 14) {
			maze[0][0] = 2;
			if (maze[i][j + 1] == 0) {// 右邊有路的話往右走
				j++;
			} else if (maze[i + 1][j + 1] == 0) {// 右下
				j++;
				i++;
			} else if (maze[i + 1][j] == 0) {// 下
				i++;
			} else if (maze[i + 1][j - 1] == 0) {// 左下
				j--;
				i++;
			} else if (maze[i][j - 1] == 0) {// 左
				j--;
			} else if (maze[i - 1][j - 1] == 0) {// 左上
				j--;
				i--;
			} else if (maze[i - 1][j] == 0) {// 上
				i--;
			} else if (maze[i - 1][j + 1] == 0) {// 右上
				j++;
				i--;
			} else {//死路
				maze[i][j] = 4;
				i = (int) stx.pop();// 返回上一次走過的路
				j = (int) sty.pop();
				continue;
			}
			maze[i][j] = 2;//紀錄位置
			stx.push(i);
			sty.push(j);
		}
		System.out.println("\n---路線已畫出: s101502511.txt---");
		// //////以下為輸出至.txt檔
		java.io.File write_file = new java.io.File("s101502511.txt");
		java.io.PrintWriter output = new java.io.PrintWriter(write_file);

		for (int y = 0; y < 15; y++) {
			for (int x = 0; x < 15; x++) {
				output.print(maze[y][x]);
			}
			output.println();
		}
		output.close();
	}
}