package ce1002.A4.s101502502;

import java.io.*;
import java.util.Stack;
import java.util.Scanner;

public class A4 
{
	public static void main(String[] args)
	{
		Scanner cin = new Scanner(System.in);
		int count = 0;
		int[] maze = new int[225];
		
		System.out.println("請輸入一個 15*15 的迷宮: (能走為 0 ,牆壁為 1)");
		System.out.println("( 規定:左上角[0][0]為入口, 右下角[14][14]為出口, 其餘邊緣皆是牆壁 )");
		System.out.println("＊注意:測出路線之後,已走過路線會被記為2,若是死路則記為4");
		System.out.print("請輸入要讀取的檔案路徑 (例: c:\\maze.txt):");
		String MazeLocation = cin.next();
		System.out.println();
		
	    try{
	    	FileReader file = new FileReader(MazeLocation);
			BufferedReader InputMaze = new BufferedReader(file);//讀檔
			
		    for(int i = 0 ; i <= 14 ; i++)
		    {
		    	String str = InputMaze.readLine();//一行一行讀出來
		    	int length = str.length();//每行字串的長度
		    	for(int j = 0 ; j < length ; j++)
		    	{
		    		maze[count] = str.charAt(j) - '0';//把每個0,1讀出來存到maze的陣列裡
		    		count++;//計算全部有多少個數字
		    	}
		    }
		    cin.close();
			InputMaze.close();  
	    }
	    catch (IOException e) {
	    	System.out.println(e);//輸出錯誤訊息
	    }
	    
	    System.out.println("您的迷宮為:");
	    for(int i = 0 ; i < count ; i++)
	    {
	    	System.out.print(maze[i]);
	    	
	    	if((i+1)%15==0)
	    		System.out.println();
	    }
	    System.out.println();//把讀出來的迷宮輸出
	    
	    Stack <Integer> WalkedOver = new Stack <Integer>();
	    int current = 0;//此變數為走迷宮的位置
	    boolean bFind = false;//判斷是否為死路
	    
		while(current<224)
		{
			bFind = false;
			for(int i = current + 15 ; i >= current-15 ; i -= 15)
			{
				for(int j = i+1 ; j >= i-1 ; j--)
				{
			   		int left = (j/15) * 15;
			   		int right = left + 15 - 1 ;
			   		
					if(j<0 || j>225 || j==current)
			   			continue;
			   			
			   		if( j<left || j>right)
						continue;
			   		
			   		if(maze[j]==0)
			   		{
			   		    maze[current] = 2;
			   		    if(j==224)//走到出口了
			   		    	maze[j] = 2;
			   		    else
			   		    	WalkedOver.push(current);//push現在的位置進去,然後繼續往前走
											    
					    current = j;
			   			bFind = true;
			   			break;
			   		}		
			   	}
				
				if(bFind==true)
					break;
			}
			
		    if(bFind == false)//死路的情況
		    {
		    	maze[current] = 4;
		    	current = WalkedOver.pop();//把上個位置pop出來
		    }
		}
	    
		String stro = "";//宣告一個string來存要輸出的迷宮
		for(int i = 0 ; i < 225 ; i++)
	    {
	    	stro += String.format("%d", maze[i]);
	    	
	    	if((i+1)%15==0)
	    		stro += String.format("\r\n");
	    }
	    
		try
	    {
	    	File write_file = new File("s101502502.txt");
		    FileWriter fileWriter = new FileWriter(write_file);//寫檔
		    fileWriter.append(stro);
		    System.out.println("---路線已畫出: " + write_file + "---");
		    fileWriter.close();
	    }
	    catch(IOException e)
	    {
	    	System.out.println(e);//輸出錯誤訊息
	    }
	}
}