package ce1002.A4.s101502015;

import java.io.FileNotFoundException;
import java.util.Scanner;

public class A4 {
	
	public static void main(String[] args) throws Exception {
		System.out.println("請輸入一個 15*15 的迷宮: (能走為 0 ,牆壁為 1)");
		System.out.println("( 規定:左上角[0][0]為入口, 右下角[14][14]為出口, 其餘邊緣皆是牆壁 )");
		System.out.println("＊注意:測出路線之後,已走過路線會被記為2,若是死路則記為4 ");
		System.out.print("請輸入要讀取的檔案路徑 (例: c:maze.txt): ");
		Scanner t = new Scanner(System.in);
		String searchfile = t.next();
		
		java.io.File infile = new java.io.File(searchfile);//
		
		
		Scanner input = new Scanner(infile);//可以從迷宮題目中讀出
		
		System.out.println();
		System.out.println("您的迷宮為:");
		int [][] array = new int [15][15];
		for(int i = 0;i<15;i++)
		{
			for(int j = 0;j<15;j++)
				array[i][j] = 0;
		}
		int s = 0;
		while(input.hasNext()){
			for(int i = 0; i<15 ; i++){
				String change = input.next();
				System.out.println(change);
				char[] charArray = change.toCharArray();
				for(int j=0;j<15;j++){
					array[i][j] = charArray[j];
					array[i][j] = array[i][j] - 48 ;
				}
			}
			s++;
		}
		System.out.println();
		
		
		///////讀檔結束  開始走迷宮了////////
		int []hor = new int [1000];
		int []ver = new int [1000];
		int a = 0 , b = 0 ;
		s=0;
		
		while(a < 15 && b < 15 && a>=0 && b>=0 ){
			if(a==14 && b==14)
				break;
			if( a>=0 && b>=0 && array[a][b+1]==0){
				hor[s] = a;
				ver[s] = b;
				b++;
				s++;
				array[a][b] = 2 ;
				//System.out.println("右");
			}
			
			else if(array[a+1][b+1]==0 && a>=0 && b>=0){
				hor[s] = a;
				ver[s] = b;
				a++;
				b++;
				s++;
				array[a][b] = 2 ;
				//System.out.println("右下");
			}
			
			else if(array[a+1][b]==0 && a>=0 && b>=0){
				hor[s] = a;
				ver[s] = b;
				a++;
				s++;
				array[a][b] = 2 ;
				//System.out.println("下");
			}
			
			else if( a>=0 && b>0 && array[a+1][b-1]==0){
				hor[s] = a;
				ver[s] = b;
				a++;
				b--;
				s++;
				array[a][b] = 2 ;
				//System.out.println("左下");
			}
			
			else if(a>=0 && b>0 && array[a][b-1]==0){
				hor[s] = a;
				ver[s] = b;
				b--;
				s++;
				array[a][b] = 2 ;
				//System.out.println("左");
			}
			
			else if(a>0 && b>0 && array[a-1][b-1]==0){
				hor[s] = a;
				ver[s] = b;
				a--;
				b--;
				s++;
				array[a][b] = 2 ;
				//System.out.println("左上");
			}
			
			else if(a>0 && b>=0 && array[a-1][b]==0){
				hor[s] = a;
				ver[s] = b;
				a--;
				s++;
				array[a][b] = 2 ;
				//System.out.println("上");
			}
			
			else if(a>0 && b>=0 && array[a-1][b+1]==0){
				hor[s] = a;
				ver[s] = b;
				a--;
				b++;
				s++;
				array[a][b] = 2 ;
				//System.out.println("右上");
			}
			///若為死路的話 返回上一個點位///
			else{
				if(a>=0 && b>=0)
				{
					array[a][b] = 4;
					s--;
					a = hor[s] ;
					b = ver[s] ;
				}
				//System.out.println("QQ");
			}
			//System.out.println(a + " " + b);
		}
		
		System.out.println("---路線已畫出: s101522071.txt---");
		
		java.io.File outfile = new java.io.File("s101502015.txt");//
		java.io.PrintWriter output = new java.io.PrintWriter(outfile);//最後把答案輸出到檔案的地方
		////開始把儲存的陣列存進檔案裡
		System.out.print("hello");
		for(int i = 0;i<15;i++)
		{
			System.out.print("hello");
			for(int j = 0;j<15;j++)
				output.print(array[i][j]);
			output.println();
		}
		output.close();
	}	
}
