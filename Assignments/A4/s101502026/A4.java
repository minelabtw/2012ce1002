/*after reading the maze,check whether the maze has entering gate and
    exiting gate or not. If not,print out an error message and exit the program */
package ce1002.A4.s101502026;
import java.util.Scanner;

class stack
{
    public int[] stackData = new int[8];
    public static int array_number=0; //this is the counter to count the length of "stackData" array so far
    Scanner scanner = new Scanner(System.in);
    stack() //constructor
    {
        for(int i=0;i<8;i++)
        	stackData[i]=9;
    }
    void push(int num1,int num2,int num3,int num4,int num5,int num6,int num7,int num8) //this is the method to push numbers inserted
    {
    		stackData[0]=num1;
    		stackData[1]=num2;
    		stackData[2]=num3;
    		stackData[3]=num4;
    		stackData[4]=num5;
    		stackData[5]=num6;
    		stackData[6]=num7;
    		stackData[7]=num8;
    }
    int pop0() //this is the method to retrieve numbers
    {
    	int i=0;
		for(;i<8;i++)
		{
	        if(stackData[i]==0)
	        	break;
		}
		if(i==8)
			i=-1;
		return i;
    }
    int pop2()
    {
    	int i=0;
		for(;i<8;i++)
		{
	        if(stackData[i]==2)
	        	break;
		}
		if(i==8)
			i=-1;
    	return i;
    }
    void wipe() //this is the method to show numbers left
    {
    	for(int i=0;i<8;i++)
    		stackData[i]=9;
    }
}

public class A4 
{
	static int[][] number_store = new int[17][17]; //Use a two-dimension array sized 17-17 to store the maze to prevent it from overloading
	static int[][] number_store2 = new int[15][15];
	A4() //constructor
	{
	}
	static void read_file() throws Exception //this method is used to read file from the path indicated
	{
		Scanner path = new Scanner(System.in);
		String p = path.next();
		path.close();
		java.io.File file = new java.io.File(p);
		Scanner input = new Scanner(file);
		
		int k=1;
		while(input.hasNext())
		{
			String[] number=input.next().split(""); //Use a string to store lines read from file
			for(int i=1;i<=15;i++)
				number_store[k][i] = Integer.parseInt(number[i]);
			for(int i=1;i<=15;i++)
				number_store2[k-1][i-1] = Integer.parseInt(number[i]);
			k++;
        }
		input.close();
	}
	static void error_message() //this method is used to show error messages if wrong instruction is signed
	{
		if(number_store[1][1]!=0)
		{
			System.out.println("The maze has no entering gate,please read another maze");
			System.exit(1);
		}
		else
			number_store[1][1]=2;
		
		if(number_store[15][15]!=0)
		{
			System.out.println("The maze has no exiting gate,please read another maze");
			System.exit(1);
		}
	}
	static void find_maze() //this method is used to go through the maze
	{
		stack stack = new stack();
		
		int i=1,j=1;
		while(i<16)
		{
			int y=0;
			while(j<16)
			{
				int x=0;
				if(number_store[i][j]==2)
					number_store[i][j]=4;
				if(i==15&&j==15)
				{
					y=8;
					number_store[15][15]=2;
					break;
				}
				stack.push(number_store[i-1][j-1],number_store[i-1][j],number_store[i-1][j+1],number_store[i][j-1],
						number_store[i][j+1],number_store[i+1][j-1],number_store[i+1][j],number_store[i+1][j+1]);
				if(number_store[i+1][j-1]==0&&number_store[i+1][j]==0&&number_store[i+1][j+1]==2)
					number_store[i+1][j]=2;
				if(number_store[i][j-1]==4&&number_store[i][j+1]==4)
					number_store[i][j]=4;
				switch(stack.pop0())
				{
					case -1:
						number_store[i][j]=4;
						x=9;
						break;
					case 0:
						number_store[i][j]=2;
						i=i-1;
						j=j-1;
						break;
					case 1:
						number_store[i][j]=2;
						i=i-1;
						break;
					case 2:
						number_store[i][j]=2;
						i=i-1;
						j=j+1;
						break;
					case 3:
						number_store[i][j]=2;
						j=j-1;
						break;
					case 4:
						number_store[i][j]=2;
						j=j+1;
						break;
					case 5:
						number_store[i][j]=2;
						i=i+1;
						j=j-1;
						break;
					case 6:
						number_store[i][j]=2;
						i=i+1;
						break;
					case 7:
						number_store[i][j]=2;
						i=i+1;
						j=j+1;
						break;
				}
				if(x==9)
				{
					switch(stack.pop2())
					{
						case -1:
							System.out.println("The maze has no exiting gate.");
							System.exit(1);
						case 0:
							number_store[i][j]=4;
							if(number_store[i-1][j]==2)
								number_store[i-1][j]=4;
							else if(number_store[i][j-1]==2)
								number_store[i][j-1]=4;
							i=i-1;
							j=j-1;
							break;
						case 1:
							number_store[i][j]=4;
							i=i-1;
							break;
						case 2:
							number_store[i][j]=4;
							if(number_store[i-1][j]==2)
								number_store[i-1][j]=4;
							else if(number_store[i][j+1]==2)
								number_store[i][j+1]=4;
							i=i-1;
							j=j+1;
							break;
						case 3:
							number_store[i][j]=4;
							j=j-1;
							break;
						case 4:
							number_store[i][j]=4;
							j=j+1;
							break;
						case 5:
							number_store[i][j]=4;
							if(number_store[i][j-1]==2)
								number_store[i][j-1]=4;
							else if(number_store[i+1][j]==2)
								number_store[i+1][j]=4;
							i=i+1;
							j=j-1;
							break;
						case 6:
							number_store[i][j]=4;
							i=i+1;
							break;
						case 7:
							number_store[i][j]=4;
							if(number_store[i][j+1]==2)
								number_store[i][j+1]=4;
							else if(number_store[i+1][j]==2)
								number_store[i+1][j]=4;
							i=i+1;
							j=j+1;
							break;
					}
				}
			}
			if(y==8)
				break;
			stack.wipe();
		}
	}
	static void write_file() throws Exception //this method is used to build a new file and write the maze in
	{
		java.io.File file = new java.io.File("s101502026.txt");
		if(file.exists())
		{
			System.out.println("File already exists.");
			System.exit(0);
		}
		java.io.PrintWriter output = new java.io.PrintWriter(file);
		for(int i=0;i<15;i++)
		{
			for(int j=0;j<15;j++)
			{
				output.print(number_store2[i][j]);
			}
			output.println();
		}
		output.close();
	}
	public static void main(String[] args) throws Exception
	{
		System.out.println("請輸入一個 15*15 的迷宮: (能走為 0 ,牆壁為 1)");
		System.out.println("( 規定:左上角[0][0]為入口, 右下角[14][14]為出口, 其餘邊緣皆是牆壁 )");
		System.out.println("＊注意:測出路線之後,已走過路線會被記為2,若是死路則記為4");
		System.out.print("請輸入要讀取的檔案路徑 (例: c:\\maze.txt): ");
		
		for(int i=0;i<17;i++)
			for(int j=0;j<17;j++)
				number_store[i][j]=8;
		read_file();
		System.out.println("\n您的迷宮為:");
		for(int i=0;i<15;i++)
		{
			for(int j=0;j<15;j++)
			{
				System.out.print(number_store2[i][j]);
			}
			System.out.println();
		}
		error_message();
		find_maze();
		for(int i=1;i<16;i++)
		{
			for(int j=1;j<16;j++)
			{
				number_store2[i-1][j-1]=number_store[i][j];
			}
		}
		write_file();
		System.out.println("\n---路線已畫出: s101502026.txt---");
	}
}