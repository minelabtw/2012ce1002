package ce1002.A4.s101502004;
import java.util.*;//
import java.io.*;

public class A4 {
	public static void main(String[] args){
		System.out.println("請輸入一個 15*15 的迷宮: (能走為 0 ,牆壁為 1)\n( 規定:左上角[0][0]為入口, 右下角[14][14]為出口, 其餘邊緣皆是牆壁 )\n＊注意:測出路線之後,已走過路線會被記為2,若是死路則記為4\n請輸入要讀取的檔案路徑 (例: c:\\maze.txt):");
		Scanner inp = new Scanner(System.in);
		char[][] maze = new char[15][15];//存放迷宮的二維陣列
		String path = null;//暫存讀檔
		try{
			File f = new File(inp.next());//建立新的檔案物件
			FileReader fr = new FileReader(f);//讀檔
			BufferedReader bf=new BufferedReader(fr);//讀檔後寫入
			for(int i=0;i<15;i++){
				path = bf.readLine();//一行一行讀進來
				for(int j=0;j<15;j++)
					maze[i][j]=path.charAt(j);//一個一個字元放進陣列
			}	
			bf.close();//關閉檔案
			fr.close();//關閉檔案
		}
		catch(Exception e){}//例外資訊處理
		
		System.out.println();
		System.out.println("您的迷宮為:");
		for(int i=0;i<15;i++){//顯示迷宮
			for(int j=0;j<15;j++)
				System.out.print(maze[i][j]);
			System.out.println();
		}
		
		int x=0,y=0;
		Stack<Integer> peoplex = new Stack<Integer>();//存目前位置的行數的stack
		Stack<Integer> peopley = new Stack<Integer>();//存目前位置的列數的stack
		peoplex.push(x);peopley.push(y);//先將初始位置push進去
		while(x!=14 && y!=14){//迴圈停止於走到終點maze[14][14]
			if(x==14&&y==14){//走到終點時代換成2然後break
				maze[x][y]='2';
				break;
			}
			if((x-1>=0) && (x-1<15) && (y>=0) && (y<15) && maze[x-1][y]=='0'){//第一個先檢測目前位置的正上方，限制條件以防跑出陣列
				maze[x][y]='2';maze[x-1][y]='2';//代換
				x=x-1;//目標前進
				peoplex.push(x);peopley.push(y);//stack push 更新
				continue;//由新目標重新檢測
			}
			else{
				if((x-1>=0) && (x-1<15) && (y+1>=0) && (y+1<15) && maze[x-1][y+1]=='0'){//第二個檢測目前位置的右上方
					maze[x][y]='2';maze[x-1][y+1]='2';
					x=x-1;y=y+1;
					peoplex.push(x);peopley.push(y);
					continue;
				}
				else{
					if((x>=0) && (x<15) && (y+1>=0) && (y+1<15) && maze[x][y+1]=='0'){//第三個檢測目前位置的正右方
						maze[x][y]='2';maze[x][y+1]='2';
						y=y+1;
						peoplex.push(x);peopley.push(y);
						continue;
					}
					else{
						if((x+1>=0) && (x+1<15) && (y+1>=0) && (y+1<15) && maze[x+1][y+1]=='0'){//第四個檢測目前位置的右下方
							maze[x][y]='2';maze[x+1][y+1]='2';
							x=x+1;y=y+1;
							peoplex.push(x);peopley.push(y);
							continue;
						}
						else{
							if((x+1>=0) && (x+1<15) && (y>=0) && (y<15) && maze[x+1][y]=='0'){//第五個檢測目前位置的正下方
								maze[x][y]='2';maze[x+1][y]='2';
								x=x+1;
								peoplex.push(x);peopley.push(y);;
								continue;
							}
							else{
								if((x+1>=0) && (x+1<15) && (y-1>=0) && (y-1<15) && maze[x+1][y-1]=='0'){//第六個檢測目前位置的左下方
									maze[x][y]='2';maze[x+1][y-1]='2';
									x=x+1;y=y-1;
									peoplex.push(x);peopley.push(y);
									continue;
								}
								else{
									if((x>=0) && (x<15) && (y-1>=0) && (y-1<15) && maze[x][y-1]=='0'){//第七個檢測目前位置的正左方
										maze[x][y]='2';maze[x][y-1]='2';
										y=y-1;
										peoplex.push(x);peopley.push(y);
										continue;
									}
									else{
										if((x-1>=0) && (x-1<15) && (y-1>=0) && (y-1<15) && maze[x-1][y-1]=='0'){//第八個檢測目前位置的左上方
											maze[x][y]='2';maze[x-1][y-1]='2';
											x=x-1;y=y-1;
											peoplex.push(x);peopley.push(y);
											continue;
										}
										else{//若八個方向都沒有路可走
											maze[x][y]='4';//紀錄為死路
											x=peoplex.pop();y=peopley.pop();//pop出去，目標位置回去一格
											continue;//由新目標開始重新檢測
										}
									}
								}
							}
						}
					}
				}
			}
		}
		
		try{
			File write_file = new File( "s101502004.txt" );//建立新的檔案物件
			FileWriter fw = new FileWriter(write_file);//寫檔
			BufferedWriter bw = new BufferedWriter(fw);//寫檔進檔案
			for(int i=0;i<15;i++){
				for(int j=0;j<15;j++)
					bw.write(maze[i][j]);//一行行寫入
				bw.newLine();//換行
			}	
			bw.close();//關閉檔案
			fw.close();//關閉檔案
		}
		catch(Exception e){}//例外資訊處理
		
		System.out.println();
		System.out.println("---路線已畫出: s101502004.txt---");
	}
}
