package ce1002.A4.s101502008;
import java.io.*;
import java.util.Scanner;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.FileWriter;
import java.io.FileReader;
public class A4 {
	
	
	public  static void main(String[] args) throws IOException{
		/*int[][] maze={{0,1,1,1,1,1,1,1,1,1,1,1,1,1,1},
				{1,0,0,0,0,0,0,1,1,1,1,1,1,1,1},
				{1,1,0,1,1,1,1,1,1,1,1,1,1,1,1},
				{1,1,0,1,1,1,0,0,0,0,0,0,0,1,1},
				{1,1,0,0,0,1,0,1,1,0,1,1,0,1,1},
				{1,1,0,1,1,0,0,1,0,1,1,1,0,1,1},
				{1,1,0,1,1,1,1,1,1,0,1,1,1,1,1},
				{1,1,0,1,1,1,0,0,0,0,1,1,1,1,1},
				{1,0,1,1,0,0,1,1,1,1,0,1,1,1,1},
				{1,0,0,1,0,1,1,1,1,1,0,0,0,1,1},
				{1,1,1,1,1,0,1,0,1,1,1,0,1,1,1},
				{1,1,1,1,1,0,0,1,0,1,1,1,1,1,1},
				{1,1,0,0,1,1,0,1,1,0,0,0,1,1,1},
				{1,1,1,0,0,0,0,1,1,1,1,0,0,0,1},
				{1,1,1,1,1,1,1,1,1,1,1,1,1,1,0}};*/
		int[][] maze=new int[15][15];
		String sss;
		Scanner inputStream = null;
		String fileName = null;
		System.out.println("請輸入一個 15*15 的迷宮: (能走為 0 ,牆壁為 1)  ");
		System.out.println("( 規定:左上角[0][0]為入口, 右下角[14][14]為出口, 其餘邊緣皆是牆壁 )    ");
		System.out.println("＊注意:測出路線之後,已走過路線會被記為2,若是死路則記為4  ");
		System.out.print("請輸入要讀取的檔案路徑 (例: c:\\maze.txt):");
		Scanner input = new Scanner(System.in); 
		fileName= input.next();		
		String path = input.nextLine();
		System.out.print("\n");
		System.out.println("您的迷宮為:");
		BufferedReader in = new BufferedReader(new FileReader(fileName));
		String tmp;
		for(int i=0;i<15;i++)
		{
			tmp = in.readLine();
			for(int j=0;j<15;j++) //讀檔並轉為INT陣列
				if(tmp.substring(j,j+1).equals("0"))
					maze[i][j] = 0;
				else
					maze[i][j] = 1;
			System.out.println(tmp);
		}//讀檔寫檔部分參考同學
		int x=1,y=1;
		maze[0][0]=2;
		
		function(x,y,maze);
		FileWriter filewriter = new FileWriter( "s101502008.txt" );
		for(int k=0;k<15;k++,filewriter.write("\r\n")){
			for(int l=0;l<15;l++)
				filewriter.write(Integer.toString(maze[k][l]));
		}
		filewriter.close();
		System.out.print("\n");
		System.out.println("---路線已畫出: s101502008.txt---");
	}
	
	public  static void function(int x,int y,int[][]maze){
		maze[x][y]=2;
		if(x==14 &&y==14){
		}
		else{
			if(maze[x+1][y+1]==0){
				x=x+1;
				y=y+1;
				function(x,y,maze);
			}
			else if(maze[x][y+1]==0){
				y++;
				function(x,y,maze);
			}
			else if(maze[x+1][y]==0){
				x=x+1;
				function(x,y,maze);
			}
			
			else if(maze[x-1][y+1]==0){
				y=y+1;
				x--;
				function(x,y,maze);
			}
			else if(maze[x+1][y-1]==0){
				x=x+1;
				y=y-1;
				function(x,y,maze);
			}
			else if(maze[x-1][y]==0){
				x=x-1;
				function(x,y,maze);
			}
			else if(maze[x][y-1]==0){
				y--;
				function(x,y,maze);
			}
			else if(maze[x-1][y-1]==0){
				y--;
				x--;
				function(x,y,maze);
			}
			else if(maze[x-1][y-1]!=0 &&maze[x-1][y]!=0 &&maze[x-1][y+1]!=0
						&&maze[x][y+1]!=0 &&maze[x+1][y+1]!=0 &&maze[x+1][y]!=0
						&&maze[x+1][y-1]!=0 &&maze[x][y-1]!=0){
				maze[x][y]=4;
				}
				if(maze[x+1][y+1]==2){
					x=x+1;
					y=y+1;
					function(x,y,maze);
				}
				else if(maze[x][y+1]==2){
					y++;
					function(x,y,maze);
				}
				else if(maze[x+1][y]==2){
					x=x+1;
					function(x,y,maze);
				}
				
				else if(maze[x-1][y+1]==2){
					y=y+1;
					x--;
					function(x,y,maze);
				}
				else if(maze[x+1][y-1]==2){
					x=x+1;
					y=y-1;
					function(x,y,maze);
				}
				else if(maze[x-1][y]==2){
					x=x-1;
					function(x,y,maze);
				}
				else if(maze[x][y-1]==2){
					y--;
					function(x,y,maze);
				}
				else if(maze[x-1][y-1]==2){
					y--;
					x--;
					function(x,y,maze);
				}
				
			}
		}
		
	}


