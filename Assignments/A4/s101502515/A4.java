package ce1002.A4.s101502515;
import java.io.*;
import java.util.Scanner;
import java.util.Stack;

public class A4 {
	static int x=1;//now at map x,y first step
	static int y=1;
	static int[][] map = new int[15][15];
	static Stack stacker = new Stack(); 
	public static void main(String[] args){
		FileReader fin;
		Scanner input = new Scanner(System.in);
		try {
			System.out.println("請輸入一個 15*15 的迷宮: (能走為 0 ,牆壁為 1)");
			System.out.println("( 規定:左上角[0][0]為入口, 右下角[14][14]為出口, 其餘邊緣皆是牆壁 )");
			System.out.println("＊注意:測出路線之後,已走過路線會被記為2,若是死路則記為4");
			System.out.println("請輸入要讀取的檔案路徑 (例: c:\\maze.txt): ");
			fin = new FileReader(input.next());
			//System.out.println("file get");
				for(int i=0;i<15;i++){
					for(int j=0;j<15;j++){
						map[j][i] = (fin.read());
					}
					int p= fin.read();//空讀取換行
					p= fin.read();
				}
				System.out.println("");
				System.out.println("您的迷宮為:");
				for(int i=0;i<15;i++){
					for(int j=0;j<15;j++){
						System.out.print(map[j][i]-48);
					}
					System.out.println("");
				}
		} catch (IOException e) {
			e.printStackTrace();
			System.out.println("no file");
		}
		System.out.println("");
		map[0][0]=50;
		map[1][1]=50;//初始地點為走過
		judge();
		map[14][14]=50;//通過終點
		/////////output/////////
		 java.io.File outf = new java.io.File("s101502515.txt");
		try
	    {
			FileWriter fout = new FileWriter(outf);
	      for(int i=0;i<15;i++){
				for(int j=0;j<15;j++){
					fout.write(map[j][i]);
				}
				fout.write("\r\n");
			}
	      fout.close();
	      System.out.println("---路線已畫出: s101522071.txt---");
	      //System.out.println(map[0][0]==48);
	    }
	    catch(Exception e)
	    {
	      e.printStackTrace();
	    }
	}
	public static void judge(){
		for(;;){
			if(x==14 && y==14)
				break;
			else if(map[x+1][y]==48){//尋找右邊
				map[x][y]=ifwalk(x+1, y);//傳回數值
				x+=1;//移動
				putsta();//放入stack
			}///順時針
			else if(map[x+1][y+1]==48){
				map[x][y]=ifwalk(x+1, y+1);
				x+=1;
				y+=1;
				putsta();
			}
			else if(map[x][y+1]==48){
				map[x][y]=ifwalk(x, y+1);
				y+=1;
				putsta();
			}
			else if(map[x-1][y+1]==48){
				map[x][y]=ifwalk(x-1, y+1);
				x-=1;
				y+=1;
				putsta();
			}
			else if(map[x-1][y]==48){
				map[x][y]=ifwalk(x-1, y);
				x-=1;
				putsta();
			}
			else if(map[x-1][y-1]==48){
				map[x][y]=ifwalk(x-1, y-1);
				x-=1;
				y-=1;
				putsta();
			}
			else if(map[x][y-1]==48){
				map[x][y]=ifwalk(x, y-1);
				y-=1;
				putsta();
			}
			else if(map[x+1][y-1]==48){
				map[x][y]=ifwalk(x+1, y-1);
				x+=1;
				y-=1;
				putsta();
			}
/////////////////////////沒有路
			else {
				map[x][y]=52;//設定死路
/////////////////////////回頭走
				y=(int) stacker.pop();
				x=(int) stacker.pop();
			}
			/*System.out.println(map[x][y]);測試使用路徑圖
			System.out.println("");
			System.out.println(x);
			System.out.println(y);
			System.out.println("");*/
		}
	}
	public static int ifwalk(int a,int b){
		if(map[a][b]==48){
			return 50;
		}
		else if(map[a][b]==50){//回頭路
			return 52;
		}
		return 9;//error
	}
	public static void putsta(){//放入stack
		stacker.push(x);
		stacker.push(y);
	}
}