package ce1002.A4.s101502503;

import java.io.*;
import java.util.Scanner;

public class A4 {
	public static void main(String[] args) throws FileNotFoundException{
		int[][] arr = new int[15][15];
		Scanner input = new Scanner(System.in);
		System.out.println("請輸入一個 15*15 的迷宮: (能走為 0 ,牆壁為 1)");
		System.out.println("( 規定:左上角[0][0]為入口, 右下角[14][14]為出口, 其餘邊緣皆是牆壁 )");
		System.out.println("＊注意:測出路線之後,已走過路線會被記為2,若是死路則記為4");
		System.out.println("請輸入要讀取的檔案路徑 (例: c:\\maze.txt): ");
		
		File file = new File(input.nextLine());//讀檔
		try {
			FileReader fr = new FileReader(file);
			BufferedReader br = new BufferedReader(fr);
			String line;
			while((line = br.readLine())!=null){
				System.out.println(line);
				int num=1;
				String[] array = line.split("\\t");
				for(int i=0;i<15;i++)
					for(int j=0;j<15;j++)
					{
						arr[i][j]=Integer.parseInt(array[num]);//將字串存到陣列
	            		num++;
					}
			}
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		System.out.println("\n您的迷宮為:");
		for(int i=0; i<15; i++)
			for(int j=0; j<15; j++)
				System.out.print(arr[i][j]);
			System.out.println();
		
		int[] walkedxarr = new int[30];
		int[] walkedyarr = new int[30];
		
		
		int x=0 , y=0 , m=0 ;
		arr[x][y]=2;
		while(arr[2][2]==0)
		{
			boolean roadornot = false;
			if(x==0&&y==0)//arr[0][0]檢查
			{
				arr[x][y]=2;
				if(arr[0][1]==0)
				{
					x=0;
					y=1;
					arr[x][y]=2;
					walkedxarr[m]=x;
					walkedyarr[m]=y;
					m++;
					roadornot = true;
				}
				if(arr[1][1]==0)
				{
					x=1;
					y=1;
					arr[x][y]=2;
					walkedxarr[m]=x;
					walkedyarr[m]=y;
					m++;
					roadornot = true;
				}
				if(arr[1][0]==0)
				{
					x=1;
					y=0;
					arr[x][y]=2;
					walkedxarr[m]=x;
					walkedyarr[m]=y;
					m++;
					roadornot = true;
				}
			}
			else if(x==0&&y>0&&y<14)//arr[0][1]~arr[0][13]檢查
			{
				if(arr[0][y+1]==0)
				{
					x=0;
					y=y+1;
					arr[x][y]=2;
					walkedxarr[m]=x;
					walkedyarr[m]=y;
					m++;
					roadornot = true;
				}
				else if(arr[0][y-1]==0)
				{
					x=0;
					y=y-1;
					arr[x][y]=2;
					walkedxarr[m]=x;
					walkedyarr[m]=y;
					m++;
					roadornot = true;
				}
				else
				{
					for(int i=0;i<3;i++)
					{
						if(arr[1][y-1+i]==0)
						{
							x=1;
							y=y-1+i;
							arr[x][y]=2;
							walkedxarr[m]=x;
							walkedyarr[m]=y;
							m++;
							roadornot = true;
						}
					}
				}
			}
			else if(y==0&&x>0&&x<14)//arr[1][0]~arr[13][0]檢查
			{
				if(arr[x+1][0]==0)
				{
					x=x+1;
					y=0;
					arr[x][y]=2;
					walkedxarr[m]=x;
					walkedyarr[m]=y;
					m++;
					roadornot = true;
				}
				else if(arr[x-1][0]==0)
				{
					x=x-1;
					y=0;
					arr[x][y]=2;
					walkedxarr[m]=x;
					walkedyarr[m]=y;
					m++;
					roadornot = true;
				}
				else
				{
					for(int i=0;i<3;i++)
					{
						if(arr[x-1+i][1]==0)
						{
							x=x-1+i;
							y=1;
							arr[x][y]=2;
							walkedxarr[m]=x;
							walkedyarr[m]=y;
							m++;
							roadornot = true;
						}
					}
				}	
			}
			else if(x==14&&y>0&&y<14)//arr[14][0]~arr[14][13]檢查
			{
				if(arr[14][y+1]==0)
				{
					x=14;
					y=y+1;
					arr[x][y]=2;
					walkedxarr[m]=x;
					walkedyarr[m]=y;
					m++;
					roadornot = true;
				}
				else if(arr[14][y-1]==0)
				{
					x=14;
					y=y-1;
					arr[x][y]=2;
					walkedxarr[m]=x;
					walkedyarr[m]=y;
					m++;
					roadornot = true;
				}
				else
				{
					for(int i=0;i<3;i++)
					{
						if(arr[13][y-1+i]==0)
						{
							x=13;
							y=y-1+i;
							arr[x][y]=2;
							walkedxarr[m]=x;
							walkedyarr[m]=y;
							m++;
							roadornot = true;
						}
					}
				}
			}
			else if(y==14&&x>0&&x<14)//arr[1][14]~arr[13][14]檢查
			{
				if(arr[x+1][14]==0)
				{
					x=x+1;
					y=14;
					arr[x][y]=2;
					walkedxarr[m]=x;
					walkedyarr[m]=y;
					m++;
					roadornot = true;
				}
				else if(arr[x-1][14]==0)
				{
					x=x-1;
					y=14;
					arr[x][y]=2;
					walkedxarr[m]=x;
					walkedyarr[m]=y;
					m++;
					roadornot = true;
				}
				else
				{
					for(int i=0;i<3;i++)
					{
						if(arr[x-1+i][13]==0)
						{
							x=x-1+i;
							y=1;
							arr[x][y]=2;
							walkedxarr[m]=x;
							walkedyarr[m]=y;
							m++;
							roadornot = true;
						}
					}
				}	
			}
			if(x==0&&y==14)//arr[0][14]檢查
			{
				if(arr[0][13]==0)
				{
					x=0;
					y=13;
					arr[x][y]=2;
					walkedxarr[m]=x;
					walkedyarr[m]=y;
					m++;
					roadornot = true;
				}
				if(arr[1][13]==0)
				{
					x=1;
					y=13;
					arr[x][y]=2;
					walkedxarr[m]=x;
					walkedyarr[m]=y;
					m++;
					roadornot = true;
				}
				if(arr[1][14]==0)
				{
					x=1;
					y=14;
					arr[x][y]=2;
					walkedxarr[m]=x;
					walkedyarr[m]=y;
					m++;
					roadornot = true;
				}
			}
			if(x==14&&y==0)//arr[14][0]檢查
			{
				if(arr[13][0]==0)
				{
					x=13;
					y=0;
					arr[x][y]=2;
					walkedxarr[m]=x;
					walkedyarr[m]=y;
					m++;
					roadornot = true;
				}
				if(arr[13][1]==0)
				{
					x=13;
					y=1;
					arr[x][y]=2;
					walkedxarr[m]=x;
					walkedyarr[m]=y;
					m++;
					roadornot = true;
				}
				if(arr[14][1]==0)
				{
					x=14;
					y=1;
					arr[x][y]=2;
					walkedxarr[m]=x;
					walkedyarr[m]=y;
					m++;
					roadornot = true;
				}
			}
			else if(x>0&&x<14&&y>0&&y<14)//剩下中間的檢查
			{
				for(int i=0;i<3;i++)
					for(int j=0;j<3;j++){
						if(arr[x-1+i][y-1+j]==0)
						{
							x=x-1+i;
							y=y-1+j;
							arr[x][y]=2;
							walkedxarr[m]=x;
							walkedyarr[m]=y;
							m++;
							roadornot = true;
						}
					}
			}
			if(roadornot = false)
			{
				arr[x][y]=4;
				x=walkedxarr[m];
				y=walkedyarr[m];
				m--;
			}
		}
		java.io.File newfile = new java.io.File( "s101502503.txt" ); //寫檔
		if(newfile.exists())//determine if file exists
		{
			System.out.println("File already exits.");
			System.exit(0);
		}
		java.io.PrintWriter output = new java.io.PrintWriter(newfile);
		for(int i=0; i<15; i++)
		{
			for(int j=0; j<15; j++)
				output.print(arr[i][j]);//write file in s101502504.txt
			output.println();//fifteen numbers to the next line
		}
		output.close();
		System.out.println("\n---路線已畫出: s101502503.txt---");
	}
}
