package ce1002.A4.s100502006;

import java.util.Scanner;

public class A4 {
	private static Scanner input= new Scanner(System.in);
	private static char[][] maze=new char[15][];//static maze map
	private static int[] dx={1,1,0,-1,-1,-1,0,1};//next step for x
	private static int[] dy={0,1,1,1,0,-1,-1,-1};//next step for y
	public static void main(String[] args) throws Exception{
		System.out.print("請輸入一個 15*15 的迷宮: (能走為 0 ,牆壁為 1)\n" +
				"( 規定:左上角[0][0]為入口, 右下角[14][14]為出口, 其餘邊緣皆是牆壁 )\n" +
				"＊注意:測出路線之後,已走過路線會被記為2,若是死路則記為4\n" +
				"請輸入要讀取的檔案路徑 (例: c:\\maze.txt): ");
		String fileName = input.nextLine();//input filename
		java.io.File file= new java.io.File(fileName);
		if(!file.exists()){//check file existence
			System.out.print("File does not exist");
			System.exit(1);
		}
		Scanner fileInput = new Scanner(file);
		System.out.println("\n您的迷宮為:");
		for(int i=0;i<15;i++){
			maze[i]=fileInput.next().toCharArray();//store map to char array "maze" from the file
			System.out.println(maze[i]);//print the maze on screen
		}
		fileInput.close();
		visit(0,0);//start running the maze
		System.out.print("\nThere is no way to the exit");//for the map without a way to the exit
	}
	private static void visit(int y,int x) throws Exception{
		if(x>=0&&y>=0&&x<=14&&y<=14&& maze[y][x]=='0'){//if this step is available(in the size of maze, not on the wall)
			maze[y][x]='2';
		}
		else return;//return if it is a invalid step
		if(x==14&&y==14){//if reach the exit
			printMaze();
			System.out.print("\n---路線已畫出: s100502006.txt---");
			System.exit(0);//end the program
		}
		for(int i=0;i<8;i++){//eight directions for next step
			visit(y+dy[i], x+dx[i]);//next step
		}
		maze[y][x]='4';//step of dead rode
	}
	private static void printMaze() throws Exception{//output the maze to file
		java.io.File write_file = new java.io.File( "s100502006.txt" );
		java.io.PrintWriter output= new java.io.PrintWriter(write_file);
		for(int i=0;i<15;i++){
			output.println(maze[i]);
		}
		output.close();
	}
}
