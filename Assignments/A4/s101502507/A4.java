package ce1002.A4.s101502507;

import java.util.Scanner;

public class A4
{
	public static void main(String[] args)throws Exception
	{
		Scanner input = new Scanner(System.in);
		String filename = null;
		int [][]a = new int[15][15];
		int []pathX = new int[256];
		int []pathY = new int[256];
		int i,j,m=0;
		
		System.out.print("請輸入一個 15*15 的迷宮: (能走為 0 ,牆壁為 1)\n"+
				"規定:左上角[0][0]為入口, 右下角[14][14]為出口, 其餘邊緣皆是牆壁 )\n"+
				"＊注意:測出路線之後,已走過路線會被記為2,若是死路則記為4\n"+
				"請輸入要讀取的檔案路徑 (例: c:\\maze.txt): ");
		
		filename = input.nextLine();		
		java.io.File file = new java.io.File(filename);
		Scanner inputfile = new Scanner(file);
		
		i=0;//左至右 上至下 一列一列讀檔存入a[][]中
		
		while(inputfile.hasNext())
		{
			String b = inputfile.nextLine();
			
			for(j=0;j<15;j++)
			{
				if(b.charAt(j)=='0')
					a[i][j]=0;
				else 
					a[i][j]=1;
			}
			i++;
		}
		
		System.out.println("\n您的迷宮為:");//輸出原迷宮		
		for(i=0;i<15;i++)
		{
			for(j=0;j<15;j++)
				System.out.print(a[i][j]);
			System.out.println();
		}
		
		i=0;//迷宮左上角為起點
		j=0;
		
		while((i!=14)||(j!=14))//重複判斷直到迷宮終點
		{
			if(a[i][j+1]==0)//右
			{			
				pathX[m]=i;//將走過位置存入pathX pathY中
				pathY[m]=j;
				m++;
				a[i][j]=2;//走過路徑(2)
				j=j+1;
			}	
			else if(a[i+1][j+1]==0)//右下
			{			
				pathX[m]=i;//將走過位置存入pathX pathY中
				pathY[m]=j;
				m++;
				a[i][j]=2;//走過路徑(2)
				i=i+1;
				j=j+1;
			}
			else if(a[i+1][j]==0)//下
			{
				pathX[m]=i;//將走過位置存入pathX pathY中
				pathY[m]=j;
				m++;
				a[i][j]=2;//走過路徑(2)
				i=i+1;
			}
			else if(a[i+1][j-1]==0)//左下
			{
				pathX[m]=i;//將走過位置存入pathX pathY中
				pathY[m]=j;
				m++;
				a[i][j]=2;//走過路徑(2)
				i=i+1;
				j=j-1;
			}
			else if(a[i][j-1]==0)//左
			{
				pathX[m]=i;//將走過位置存入pathX pathY中
				pathY[m]=j;
				m++;
				a[i][j]=2;//走過路徑(2)
				j=j-1;
			}
			else if(a[i-1][j-1]==0)//左上
			{
				pathX[m]=i;//將走過位置存入pathX pathY中
				pathY[m]=j;
				m++;
				a[i][j]=2;//走過路徑(2)
				i=i-1;
				j=j-1;
			}
			else if(a[i-1][j]==0)//上
			{
				pathX[m]=i;//將走過位置存入pathX pathY中
				pathY[m]=j;
				m++;
				a[i][j]=2;//走過路徑(2)
				i=i-1;
			}
			else if(a[i-1][j+1]==0)//右上
			{
				pathX[m]=i;//將走過位置存入pathX pathY中
				pathY[m]=j;
				m++;
				a[i][j]=2;//走過路徑(2)
				i=i-1;
				j=j+1;
			}	
			else //若八方皆無0則代表死路(4)
			{
				a[i][j]=4;//死路(4)
				m--;//退回上一步
				i=pathX[m];
				j=pathY[m];
			}
		}
		
		a[14][14]=2;//終點為走過路徑(2)
		
		 java.io.File write_file = new java.io.File("s101502507.txt");//開檔
		java.io.PrintWriter output = new java.io.PrintWriter(write_file);
		
		for(i=0;i<15;i++)//寫檔 
		{	
			for(j=0;j<15;j++)
				output.print(a[i][j]);
			output.println();
		}
		
		output.close();//關檔
		System.out.println("\n---路線已畫出: s101502507.txt---");
	}
}