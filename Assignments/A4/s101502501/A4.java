package ce1002.A4.s101502501;
//stack和寫檔和切割的方法是同學教的,但不是很懂//
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.BufferedReader;
import java.util.*;

public class A4 {

	public static void main(String[] args) throws FileNotFoundException,IOException {

		System.out.println("請輸入一個 15*15 的迷宮: (能走為 0 ,牆壁為 1)");
		System.out.println("( 規定:左上角[0][0]為入口, 右下角[14][14]為出口, 其餘邊緣皆是牆壁 )");
		System.out.println("＊注意:測出路線之後,已走過路線會被記為2,若是死路則記為4");
		System.out.print("請輸入要讀取的檔案路徑 (例: c:\\maze.txt): ");
		
		Scanner scanner = new Scanner(System.in);
		String filename = scanner.next();
		System.out.print("\n");
		System.out.println("您的迷宮為:");
		
		//讀檔
		BufferedReader input = new BufferedReader(new FileReader(filename));
		String read;
		int maze[][] = new int[15][15];// 儲存迷宮的陣列
		int num = 0;
		while ((read = input.readLine()) != null){
			String read1[] = read.split("");//切割每行的數字
			for (int i = 1; i <= 15; i++) {//再分別儲存到陣列
				maze[num][i - 1] = Integer.parseInt(read1[i]);
			}  
			num++;
			System.out.println(read);//輸出迷宮
		}
		
        int j = 0, i = 0;//定初始座標
		Stack stackx = new Stack();//紀錄x座標
		Stack stacky = new Stack();//紀錄y座標

		
		while (j < 14 && i < 14) {
			maze[0][0] = 2;
			if (maze[i][j + 1] == 0){//如果右邊有路的話往右走
				j++;
			} 
			else if (maze[i + 1][j + 1] == 0) {//走右下
				j++;
				i++;
			} 
			else if (maze[i + 1][j] == 0) {//下
				i++;
			} 
			else if (maze[i + 1][j - 1] == 0) {//左下
				j--;
				i++;
			} 
			else if (maze[i][j - 1] == 0) {//左
				j--;
			}
			else if (maze[i - 1][j - 1] == 0) {//左上
				j--;
				i--;
			} 
			else if (maze[i - 1][j] == 0) {//上
				i--;
			} 
			else if (maze[i - 1][j + 1] == 0) {//右上
				j++;
				i--;
			} 
			else {//死路
				maze[i][j] = 4;
				i = (int) stackx.pop();//回去走上一次走過的路
				j = (int) stacky.pop();
				continue;
			}
			maze[i][j] = 2;//紀錄位置
			stackx.push(i);
			stacky.push(j);
		}
		System.out.print("\n");
		System.out.println("---路線已畫出: s101502501.txt---");
		//寫檔
		java.io.File write_file = new java.io.File("s101502501.txt");
		java.io.PrintWriter output = new java.io.PrintWriter(write_file);

		for (int y = 0; y < 15; y++) {
			for (int x = 0; x < 15; x++) {
				output.print(maze[y][x]);
			}
			output.println();
		}
		output.close();
	}
}