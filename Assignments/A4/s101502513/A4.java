package ce1002.A4.s101502513;

import java.util.*;

public class A4 {
	private static boolean way = false; //determine whether you arrive destination
	
	public static void main(String[] args) throws Exception {
		int i = 0, x = 0, y = 0;
		int[][] array = new int[15][15];
		Scanner QQ = new Scanner(System.in);
		
		System.out.println("請輸入一個 15*15 的迷宮: (能走為 0 ,牆壁為 1)");
		System.out.println("( 規定:左上角[0][0]為入口, 右下角[14][14]為出口, 其餘邊緣皆是牆壁 )");
		System.out.println("＊注意:測出路線之後,已走過路線會被記為2,若是死路則記為4");
		System.out.print("請輸入要讀取的檔案路徑 (例: c:\\maze.txt): ");
		String address = QQ.next(); //the address where I place maze.txt
		System.out.println("\n您的迷宮為:");
		
		//begin to read file
		java.io.File read_file = new java.io.File(address);
		Scanner input = new Scanner(read_file); //read data from maze.txt
		
		while( input.hasNext() ) {
			String[] Line = input.next().split(""); //split character string
			
			for( int j = 0; j < 15; j++ ) {
				array[i][j] = Integer.parseInt(Line[j+1]); //transform pattern(from String into int)
				System.out.print(array[i][j]);
			}
			i++;
			System.out.println();
		}	
		input.close(); //end
		
		escape(array, x, y); //begin to escape maze
		
		//begin to write file
		java.io.File write_file = new java.io.File("s101502513.txt");
		java.io.PrintWriter output = new java.io.PrintWriter(write_file);
		
		for( int a = 0; a < 15; a++ ) {
			for( int b = 0; b < 15; b++ )
				output.print(array[a][b]); //write data(array) into s101502513.txt
			output.println();
		}
		output.close(); //end
		
		System.out.println("\n---路線已畫出: s101502513.txt---");
	}
	
	private static void escape(int[][] maze, int x, int y) {
		maze[x][y] = 2;
		
		if( x == 14 && y == 14 ) //arrive destination
			way = true;
		
		//find way clockwise
		if( !way && maze[x][y+1] == 0 ) //right
			escape(maze, x, y+1);
		
		if( !way && maze[x+1][y+1] == 0 ) //lower right
			escape(maze, x+1, y+1);
		
		if( !way && maze[x+1][y] == 0 ) //down
			escape(maze, x+1, y);
		
		if( !way && maze[x+1][y-1] == 0 ) //lower left
			escape(maze, x+1, y-1);
		
		if( !way && maze[x][y-1] == 0 ) //left
			escape(maze, x, y-1);
		
		if( !way && maze[x-1][y-1] == 0 ) //upper left
			escape(maze, x-1, y-1);
		
		if( !way && maze[x-1][y] == 0 ) //up
			escape(maze, x-1, y);
		
		if( !way && maze[x-1][y+1] == 0 ) //upper right
			escape(maze, x-1, y+1);
		
		if( !way ) //wrong way
			maze[x][y] = 4;
	}
}
