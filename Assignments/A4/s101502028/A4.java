package ce1002.A4.s101502028;

import java.util.*;
import java.io.*;

public class A4 {
	public static void main(String[] args) throws Exception {
		Scanner input = new Scanner(System.in);
		int f = 0, s = 0;
		char[] way = new char[255];
		int[][] map = new int[15][15];
		System.out.println("請輸入一個 15*15 的迷宮: (能走為 0 ,牆壁為 1)");
		System.out.println("( 規定:左上角[0][0]為入口, 右下角[14][14]為出口, 其餘邊緣皆是牆壁 )");
		System.out.println("＊注意:測出路線之後,已走過路線會被記為2,若是死路則記為4");
		System.out.print("請輸入要讀取的檔案路徑 (例: c:\\maze.txt): ");
		String filename = input.next(); // input the file directory
		File fileName = new File(filename); // make the input file into file
		BufferedReader br = new BufferedReader(new FileReader(fileName)); // read the file
		System.out.println("\n您的迷宮為:");
		String line;
		while (br.ready()) { // when reading the file is ready
			line = br.readLine(); // read the line
			for (int i = 0; i < line.length(); i++) {
				way[f] = line.charAt(i); // convert the line into character by character
				f++;
			}
		}
		for (int a = 0; a < 15; a++) {
			for (int b = 0; b < 15; b++) {
				if (way[s] == 48) // change the character into integer
					way[s] = 0;
				else if (way[s] == 49)
					way[s] = 1; // change the character into integer
				map[a][b] = way[s]; // put the array into double array
				s++;
			}
		}
		br.close(); // close reader
		for (int x = 0; x < 15; x++) {
			for (int y = 0; y < 15; y++) {
				System.out.print(map[x][y]); // output the maze map
			}
			System.out.println();
		}
		for (int a = 0; a < 15; a++) { // find the entrance
			if (map[0][a] == 0)
				map[0][a] = 2;
		}
		for (int b = 0; b < 15; b++) { // find the exit
			if (map[14][b] == 0)
				map[14][b] = 2;
		}
		
		// HERE HAVE SOMETHING WRONG
		way(map, 1, 1); // use way function to move
		System.out.println();
		System.out.print("---路線已畫出: s101502028.txt---");
	}

	public static void way(int map[][], int row, int line) throws Exception { // way function
		map[row][line] = 2; // start moving
		if (row == 13 && line == 13) // if finish, write file
			write(map);
		if (map[row - 1][line] == 0) // move up
			way(map, row - 1, line);
		if (map[row - 1][line + 1] == 0) // move right up
			way(map, row - 1, line + 1);
		if (map[row][line + 1] == 0) // move to the right
			way(map, row, line + 1);
		if (map[row + 1][line + 1] == 0) // move to the right down
			way(map, row + 1, line + 1);
		if (map[row + 1][line] == 0) // move down
			way(map, row + 1, line);
		if (map[row + 1][line - 1] == 0) // move to the left down
			way(map, row + 1, line - 1);
		if (map[row][line - 1] == 0) // move to the left
			way(map, row, line - 1);
		if (map[row - 1][line - 1] == 0) // move to the left up
			way(map, row - 1, line - 1);
		map[row][line] = 4; // dead way
	}

	public static void write(int map[][]) throws Exception { // write function
		java.io.File write_file = new java.io.File("s101502028.txt"); // write file function
		java.io.PrintWriter write = new java.io.PrintWriter(write_file); // write file
		for (int x = 0; x < 15; x++) {
			for (int y = 0; y < 15; y++) {
				if (map[x][y] == 2)
					write.print("2"); // write the moved way
				else if (map[x][y] == 1)
					write.print("1"); // write the wall
				else
					write.print("4"); // write the dead way
			}
			write.println();
		}
		write.close(); // close writer
	}
}
