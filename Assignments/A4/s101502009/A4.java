package ce1002.A4.s101502009;
import java.io.*;
import java.util.Scanner;

public class A4 {
	public static void main(String[] args) throws Exception{
		stack stack=new stack();
		String m;
		String getpath;	
		int map[][]=new int[15][15];
		Scanner input=new Scanner(System.in);
		
		System.out.println("請輸入一個 15*15 的迷宮: (能走為 0 ,牆壁為 1)");
		System.out.println("( 規定:左上角[0][0]為入口, 右下角[14][14]為出口, 其餘邊緣皆是牆壁 )");
		System.out.println("＊注意:測出路線之後,已走過路線會被記為2,若是死路則記為4");
		System.out.print("請輸入要讀取的檔案路徑 (例: c:\\maze.txt): ");
		
		getpath=input.nextLine();//路徑
		System.out.println("您的迷宮為:");
		FileReader fr=new FileReader(getpath);//讀檔
		BufferedReader br=new BufferedReader(fr);
		for(int i=0;i<15;i++){
			m=br.readLine();
			for(int j=0;j<15;j++){
				if(m.substring(j,j+1).equals("0")){
					map[i][j] = 0;
				}
				else{
					map[i][j] = 1;
				}
			}//end of for j
					System.out.println(m);
		}//end of for i
		System.out.println();
		
		//開始走迷宮
		int i=0,ii=0,j=0,jj=0,route=0;
		int livei[]=new int[125];
		int livej[]=new int[125];
		//ii jj 為方向
		//live 為2
		do{
			//System.out.println(Integer.toString(i)+":"+Integer.toString(j));
			map[i][j]=2;
			
			if(livei[route]!=i||livej[route]!=j){
			route=route+1;
			livei[route]=i;
			livej[route]=j;	
			}
			jj=stack.pop(i,j);
			ii=stack.pop(i,j);
			//System.out.println("ii:jj= "+Integer.toString(ii)+":"+Integer.toString(jj));
			if(ii==9||jj==9){
				
				map[i][j]=4;
				//System.out.println("livei["+Integer.toString(route)+"]="+Integer.toString(livei[route]));
				//System.out.println("livej["+Integer.toString(route)+"]="+Integer.toString(livej[route]));
				route=route-1;
				i=livei[route];
				j=livej[route];
				//System.out.println("back to :"+Integer.toString(i)+":"+Integer.toString(j));
				
				continue;
			}//回頭找2
			
			j=j+jj;
			i=i+ii;
			//System.out.println("moved "+Integer.toString(i)+":"+Integer.toString(j));
			if(j<0||i<0){
				j=j-jj;
				i=i-ii;	
				continue;
			}//邊界
			if(i>15||j>15){
				j=j-jj;
				i=i-ii;
				continue;
			}
			
			if(map[i][j]==2){				
				/*route=route+1;
				livei[route]=i;
				livej[route]=j;*/
				j=j-jj;
				i=i-ii;
				continue;
			}//走過了 先回上一步 死路再回來
			
			if(map[i][j]==1){
				j=j-jj;
				i=i-ii;
				continue;
			}//碰壁回上一步
			
			if(i==14&&j==14){
				map[14][14]=2;
				System.out.println("---路線已畫出: s101522071.txt---");
				break;				
			}//走完了
		}while(map[i][j]==0||map[i][j]==2||map[i][j]==4);
		//走迷宮
		
		FileWriter fw=new FileWriter("s101502009.txt"); 
		
		for(int q=0;q<15;q++){
			for(int k=0;k<15;k++){
				fw.write(Integer.toString(map[q][k]));
			}
			fw.write("\r\n");
		}
		fw.close();
		
	}//end of main

}
