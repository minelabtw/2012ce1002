package ce1002.A4.s101502005;

import java.util.Scanner;
import java.util.Stack;
import java.lang.*;
import java.io.*;

public class A4 {
	public static void main(String[] args) throws Exception

	{ // 來自網路:www.im.cjcu.edu.tw/~schsu/powerpoints/Java2/CH07.ppt
		String getbr, getfr;
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		System.out.println("請輸入一個 15*15 的迷宮: (能走為 0 ,牆壁為 1) ");
		System.out.println("( 規定:左上角[0][0]為入口, 右下角[14][14]為出口, 其餘邊緣皆是牆壁 )");
		System.out.println("＊注意:測出路線之後,已走過路線會被記為2,若是死路則記為4 ");
		System.out.print("請輸入要讀取的檔案路徑 (例: c:\\maze.txt):");
		getbr = br.readLine(); // 取得input file
		System.out.println("您的迷宮為：");
		char buffer[] = new char[1024];
		FileReader fileread = new FileReader(getbr);
		int len = fileread.read(buffer);
		String s = new String(buffer, 0, len);
		fileread.close();
		System.out.println(s);
		System.out.println("\n---路線已畫出: s101502005.txt---");

		char maze[][] = new char[15][17];
		int a = 0;
		for (int i = 0; i < 15; i++) {
			for (int j = 0; j < 17; j++) {
				maze[i][j] = buffer[a];
				a++;

			}
		}

		Stack sk = new Stack();
		// 如果周圍牆壁都是1
		maze[0][0] = 2 + 48;
		maze[1][1] = 2 + 48;
		System.out.println(maze[0][0]);
		System.out.println(maze[1][1]);
		sk.push(new Integer(0));
		sk.push(new Integer(101));

		int i = 1;
		int j = 1;

		while (i != 14 && j != 14) {

			boolean Ifpush = false;
			// System.out.print(maze[i][j]+" ");
			if (maze[i - 1][j - 1] == 0 + 48) // 左上
			{
				maze[i - 1][j - 1] = 2 + 48;
				sk.push(new Integer(i * 100 + j)); // 用來記錄第maze[i][j]的位置
				i = i - 1;
				j = j - 1;
				Ifpush = true; // 判斷是否有路
				// 跳出for迴圈
			} else if (maze[i - 1][j] == 0 + 48) // 正上
			{
				maze[i - 1][j] = 2 + 48;
				sk.push(new Integer(i * 100 + j)); // 用來記錄第maze[i][j]的位置
				i = i - 1;
				Ifpush = true; // 判斷是否有路
				// 跳出for迴圈
			} else if (maze[i - 1][j + 1] == 0 + 48) // 右上
			{
				maze[i - 1][j + 1] = 2 + 48;
				sk.push(new Integer(i * 100 + j)); // 用來記錄第maze[i][j]的位置
				i = i - 1;
				j = j + 1;
				Ifpush = true; // 判斷是否有路
				// 跳出for迴圈
			} else if (maze[i][j + 1] == 0 + 48) // 正右
			{
				maze[i][j + 1] = 2 + 48;
				sk.push(new Integer(i * 100 + j)); // 用來記錄第maze[i][j]的位置
				j = j + 1;
				Ifpush = true; // 判斷是否有路
				// 跳出for迴圈
			} else if (maze[i + 1][j + 1] == 0 + 48) // 右下
			{
				maze[i + 1][j + 1] = 2 + 48;
				sk.push(new Integer(i * 100 + j)); // 用來記錄第maze[i][j]的位置
				i = i + 1;
				j = j + 1;
				Ifpush = true; // 判斷是否有路
				// 跳出for迴圈
			} else if (maze[i + 1][j] == 0 + 48) // 正下
			{
				maze[i + 1][j] = 2 + 48;
				sk.push(new Integer(i * 100 + j)); // 用來記錄第maze[i][j]的位置
				i = i + 1;
				Ifpush = true; // 判斷是否有路
				// 跳出for迴圈
			} else if (maze[i + 1][j - 1] == 0 + 48) // 左下
			{
				maze[i + 1][j - 1] = 2 + 48;
				sk.push(new Integer(i * 100 + j)); // 用來記錄第maze[i][j]的位置
				i = i + 1;
				j = j - 1;
				Ifpush = true; // 判斷是否有路
				// 跳出for迴圈
			} else if (maze[i][j - 1] == 0 + 48) // 正左
			{
				maze[i][j - 1] = 2 + 48;
				sk.push(new Integer(i * 100 + j)); // 用來記錄第maze[i][j]的位置
				j = j - 1;
				Ifpush = true; // 判斷是否有路
				// 跳出for迴圈
			}

			// 如果是死路就設成4 & pop
			if (!Ifpush) {
				System.out.println("hi~~" + i + " " + j);
				maze[i][j] = 4 + 48;
				a = (int) sk.pop(); // 讀取上一個位置
				i = a / 100;
				j = a % 100;
			}

		}
		java.io.File write_file = new java.io.File( "s101502005.txt" );
        java.io.PrintWriter output=new java.io.PrintWriter(write_file);
        for(int k=0;k<15;k++){ //把剛剛走的迷宮路線記錄在檔案裡
			for(int l=0;l<15;l++){
				output.print(maze[k][l]);
			}
			output.println();
		}
		output.close(); //關掉檔案
		
	}

}
