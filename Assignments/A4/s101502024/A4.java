package ce1002.A4.s101502024;

import java.util.Scanner;
import java.util.Stack;
import java.io.*;

public class A4 {
	public static void main(String[] args) throws Exception {
		int[][] maze = new int[15][15];
		Stack hueY = new Stack();// y-axis
		Stack hueX = new Stack();// x-axis
		System.out.println("請輸入一個 15*15 的迷宮: (能走為 0 ,牆壁為 1)");
		System.out.println("( 規定:左上角[0][0]為入口, 右下角[14][14]為出口, 其餘邊緣皆是牆壁 )");
		System.out.println("＊注意:測出路線之後,已走過路線會被記為2,若是死路則記為4");
		System.out.print("請輸入要讀取的檔案路徑 (例: c:\\maze.txt): ");

		Scanner hue = new Scanner(System.in);
		String file = hue.next();
		System.out.println();
		System.out.println("您的迷宮為:");

		File huehue = new File(file);
		Scanner input = new Scanner(huehue);

		int linenumber = 0;
		while (input.hasNext()) {
			String[] hueha = input.next().split("");
			for (int i = 1; i <= 15; i++) {
				maze[linenumber][i - 1] = Integer.parseInt(hueha[i]);// change
																		// the
																		// array
				// back into
				// numbers.
			}
			linenumber++;
		}
		input.close();

		for (int i = 0; i < 15; i++) {
			for (int j = 0; j < 15; j++) {
				System.out.print(maze[i][j]);
			}
			System.out.println();
		}
		int j = 0, i = 0;
		while (j < 14 && i < 14) {// walk into different directions.觀念是同學教我的QAQ
			maze[0][0] = 2;
			if (maze[i][j + 1] == 0) {
				j = j + 1;
			} else if (maze[i - 1][j] == 0) {
				i = i - 1;
			} else if (maze[i + 1][j] == 0) {
				i = i + 1;
			} else if (maze[i - 1][j - 1] == 0) {
				j = j - 1;
				i = i - 1;
			} else if (maze[i + 1][j - 1] == 0) {
				j = j - 1;
				i = i + 1;
			} else if (maze[i][j - 1] == 0) {
				j = j - 1;
			} else if (maze[i - 1][j + 1] == 0) {
				j = j + 1;
				i = i - 1;
			} else if (maze[i + 1][j + 1] == 0) {
				j = j + 1;
				i = i + 1;
			} else {
				maze[i][j] = 4;
				i = (int) hueX.pop();// walk back.
				j = (int) hueY.pop();
				continue;
			}
			maze[i][j] = 2;
			hueY.push(j);
			hueX.push(i);
		}
		java.io.File huehaha = new java.io.File("s101502024.txt");

		java.io.PrintWriter output = new java.io.PrintWriter(huehaha);// write
																		// file
		for (int asd = 0; asd < 15; asd++) {
			for (int qwe = 0; qwe < 15; qwe++) {
				output.print(maze[asd][qwe]);
			}
			output.println();
		}
		output.close();

		System.out.println();
		System.out.println("---路線已畫出: s101502024.txt---");
	}
}
