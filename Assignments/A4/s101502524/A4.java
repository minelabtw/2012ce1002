package ce1002.A4.s101502524;

import java.io.*;
import java.util.Scanner;
import java.util.Stack;

public class A4 {
	public static void main(String[] args)throws IOException
	{
		Scanner input = new Scanner(System.in);
		int[][] m=new int[17][17];
		System.out.println("請輸入一個 15*15 的迷宮: (能走為 0 ,牆壁為 1)");
		System.out.println("( 規定:左上角[0][0]為入口, 右下角[14][14]為出口, 其餘邊緣皆是牆壁 )");
		System.out.println("＊注意:測出路線之後,已走過路線會被記為2,若是死路則記為4");
		System.out.println("請輸入要讀取的檔案路徑:");
		BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
		String name=in.readLine();
		in = new BufferedReader(new FileReader(name));
		System.out.println("\n您的迷宮為:");
		for(int a=1;(name=in.readLine())!=null;a++)//將迷宮讀入
		{
			for(int b=1,c=0;b<16;b++,c++)
			{
				m[a][b]=(int)name.charAt(c)-48;
				System.out.print(m[a][b]);
			}
			System.out.println();
		}
		for(int i=0;i<17;i++)//在外圈加上牆壁
		{
			m[0][i]=1;
			m[16][i]=1;
			m[i][0]=1;
			m[i][16]=1;
		}
		int x=1,y=1,ch=0;
		Stack<Integer> X=new Stack<Integer>();
		Stack<Integer> Y=new Stack<Integer>();

		m[x][y]=2;
		int d[][]={{0,1},{1,1},{1,0},{1,-1},{0,-1},{-1,-1},{-1,0},{-1,1}};//定義八個方向
		boolean out=true;
		X.push(x);
		Y.push(y);
		while(out!=false)
		{
			for(int i=0;i<8;i++)//往八個方向找路
			{
				if(m[x+d[i][0]][y+d[i][1]]==0)
				{
					m[x+d[i][0]][y+d[i][1]]=2;
					X.push(x);
					Y.push(y);
					x=x+d[i][0];//移動
					y=y+d[i][1];

					break;
				}
				if(i==7)
				{
					ch=1;
				}
			}
			if(ch==1)//如果是死路，則跳回上一次走的地方
			{
				m[x][y]=4;
				x=X.pop();
				y=Y.pop();
				ch=0;
			}
			if(x==15&&y==15)//到達終點則跳出
			{
				out=false;
			}
		}
		java.io.File write_file = new java.io.File("s101502524.txt");
		FileWriter outs = new FileWriter(write_file.getAbsoluteFile());
		BufferedWriter bw = new BufferedWriter(outs);
		
		for(int i=1;i<16;i++){  //輸出迷宮
			for(int j=1;j<16;j++)
				bw.write(m[i][j]+48);
			bw.write(13);//換行
			bw.write(10);
		}
		bw.close();
		System.out.println("\n---路線已畫出: s101502524.txt---");
	}
}
