package ce1002A4.s101502017;
import java.io.*;
import java.util.Scanner;
public class A4 {
	private static boolean judge = false;
	private static int[][] m;
	public static void ans(int i, int j) {
		
		m[i][j] = 2;
		if (i == 14 && j == 14)
		{
			judge = true;
			return;
		}
		if (judge == false)
			if (m[i][j + 1] == 0 && judge == false)// 右
			{
				ans(i, j + 1);
			}
		if (judge == false)
			if (m[i + 1][j + 1] == 0 && judge == false)// 右下
			{
				ans(i + 1, j + 1);
			}
		if (judge == false)
			if (m[i + 1][j] == 0 && judge == false)// 下
			{
				ans(i + 1, j);
			}
		if (judge == false)
			if (m[i + 1][j - 1] == 0 && judge == false)// 左下
			{
				ans(i + 1, j - 1);
			}
		if (judge == false)
			if (m[i][j - 1] == 0 && judge == false)// 左
			{
				ans(i, j - 1);
			}
		if (judge == false)
			if (m[i - 1][j - 1] == 0 && judge == false)// 左上
			{
				ans(i - 1, j - 1);
			}
		if (judge == false)
			if (m[i - 1][j] == 0 && judge == false)// 上
			{
				ans(i - 1, j);
			}
		if (judge == false)
			if (m[i - 1][j + 1] == 0 && judge == false)// 右上
			{
				ans(i - 1, j + 1);
			}
		
		if (judge == false)
			m[i][j] = 4;

		return;

	}// 同時間檢查八方位的函式
	
	public static void main(String[] args) throws Exception{
		Scanner in = new Scanner(System.in);//輸入路徑
		System.out.println("請輸入一個 15*15 的迷宮: (能走為 0 ,牆壁為 1)");
		System.out.println("( 規定:左上角[0][0]為入口, 右下角[14][14]為出口, 其餘邊緣皆是牆壁 )");
		System.out.println("＊注意:測出路線之後,已走過路線會被記為2,若是死路則記為4");
		System.out.print("請輸入要讀取的檔案路徑 (例: c:\\maze.txt): ");
		////////////////////////讀檔///////////////////////////
		String p = in.nextLine();
		java.io.File file = new java.io.File(p);
		Scanner input = new Scanner(file);
		System.out.println("");
		System.out.println("您的迷宮為:");
		int[][] maze = new int [15][15];
		String s;//暫存
		while(input.hasNext())
		{
			for(int i = 0;i < 15;i++)
	        {
				s = input.nextLine();
				System.out.println(s);
				for(int j = 0;j < 15;j++)
				{
					if (s.substring(j, j + 1).equals("0"))// 由於s裡是string之類別,藉由equal的函式判斷來使其成為int屬性的二維陣列
						maze[i][j] = 0;
					else
						maze[i][j] = 1;
				}
	        }
		}
		in.close();
		input.close();
		/////////////解答////////////////
		m = maze ;
		ans(0,0);
		///////////////////////寫檔/////////////////////////////
		java.io.File write_file = new java.io.File("s101502017.txt");
		PrintWriter write = new PrintWriter(write_file);
		for (int i = 0; i < 15; i++) {
			for (int j = 0; j < 15; j++) {
				write.print(String.valueOf(maze[i][j]));

			}
			write.print(String.format("%n"));
		}
		System.out.print("\n---路線已畫出: s101502017.txt---");
		write.close();

	}
}
