package ce1002.A4.s101502025;

import java.io.BufferedReader;
import java.io.FileReader;
import java.util.Scanner;

public class A4{
	public static void main(String[] args) throws Exception{
		
		System.out.println("請輸入一個 15*15 的迷宮: (能走為 0 ,牆壁為 1)"); // something not important
		System.out.println("( 規定:左上角[0][0]為入口, 右下角[14][14]為出口, 其餘邊緣皆是牆壁 )");
		System.out.println("＊注意:測出路線之後,已走過路線會被記為2,若是死路則記為4 ");
		System.out.print("請輸入要讀取的檔案路徑 (例: c:\\maze.txt): ");
		
		Scanner inn = new Scanner(System.in); // input the place of the file
		String in=inn.next(); 
		
		System.out.println(" ");
		System.out.println("您的迷宮為:");
		BufferedReader read = new BufferedReader(new FileReader(in)); // get the map

		String map; // transfer the map
		int m[][] = new int[15][15];
		for(int e=0 ; e<15 ; e++)
		{
			map= read.readLine();
			System.out.println(map);
			for(int u=0 ; u<15 ; u++)
			{
				if(map.substring(u,u+1).equals("0"))
					m[e][u] = 0;
				else
					m[e][u] = 1;
			}
		}
		
	    //	find the way ( concept from classmate)
		int j = 0, i = 0;
		while (j < 14 && i < 14) 
		{
			m[0][0] = 2;
			if (m[i + 1][j + 1] == 0) // right & down (this way should be first, or it will explode) 
			{
				m[i][j]=2;
				j = j + 1;
				i = i + 1;
			} 
			else if(m[i+1][j-1]==0) // left & down
			{	
				m[i][j]=2;
				j = j - 1;
				i = i + 1;
			}
			else if (m[i - 1][j - 1] == 0)  // left & up
			{
				m[i][j]=2;
				j = j - 1;
				i = i - 1;
			}
			else if (m[i - 1][j] == 0)  // up
			{
				m[i][j]=2;
				i = i - 1;
			} 
			else if (m[i - 1][j + 1] == 0) // up & right
			{
				m[i][j]=2;
				j = j + 1;
				i = i - 1;
			}
			else if (m[i][j + 1] == 0) // right
			{
				m[i][j]=2;
				j = j + 1;
			} 
			
			else if (m[i + 1][j] == 0) // down
			{
				m[i][j]=2;
				i = i + 1;
			} 
			
			else if (m[i][j - 1] == 0) // left
			{
				m[i][j]=2;
				j = j - 1;
			}
			else  // no way, fuck
			{
				if(i==14&&j==14)
					break;
				m[i][j] = 4;
				for(int h=0;h<15;h++) // clear the wrong way
				{
					for(int d=0;d<15;d++) 
						if(m[h][d]==2)
							m[h][d]=0;
				}
				i=0;
				j=0;
			}
		}
		
		m[14][14]=2; // the exit is the only way
		
		System.out.println(" ");
		System.out.println("---路線已畫出: s101502025.txt---");
		
		
		java.io.File write_file = new java.io.File( "s101502025.txt" ); // the name of the answer
		java.io.PrintWriter output = new java.io.PrintWriter(write_file); // save the answer into the data
		for(int g=0;g<15;g++)
		{
			for(int r=0;r<15;r++)
			{	
				output.print(m[g][r]);
				if(r==14)
					output.println(" ");
			}
		}
		output.close();
}
}