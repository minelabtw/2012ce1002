package ce1002.A4.s101502505;

public class Stack {
	int[][] stack;
	Stack()
    {
		;
    }
    
	public Stack(int input)
    {
    	stack = new int[input][2];
    }
    
	public void push(int input)
    {
		int pointer=-1;//pointer表示陣列stack第一維的第幾個開始為有效資料
		for (int i=(stack.length-1);i>=0;i--)//stack.length=100  抓出pointer
		{
			if (stack[i][1]==1)
			{
				pointer=i;
				break;
			}
		}
		if (pointer<stack.length)
		{
			stack[pointer+1][0]=input;//將資料存入
			stack[pointer+1][1]=1;//標示為有效資料
			return;
		}
		else
		{
			System.out.println("The stack is overflow.");
			return;
		}
    }
    
	public int pop()
    {
		int pointer=-1;
		for (int i=(stack.length-1);i>=0;i--)//抓出pointer
		{
			if (stack[i][1]==1)
			{
				pointer=i;
				break;
			}
		}
		if (pointer == -1)
		{	
			System.out.print("Stack is empty! The integer returned is invalid!");
			return 0;
		}
		else
		{
			stack[pointer][1]=0;
			return stack[pointer][0];
		}
    }
    
	public void show()
    {
    	int pointer=-1;
		for (int i=(stack.length-1);i>=0;i--)//抓出pointer
		{
			if (stack[i][1]==1)
			{
				pointer=i;
				break;
			}
		}
		for (int i=0;i<=pointer;i++)
			System.out.print(stack[i][0] + " ");		
    }
}
