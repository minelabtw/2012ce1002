package ce1002.A4.s101502016;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.OutputStream;
import java.util.Scanner;

public class A4 {
	private static int[][] maze;
	private static boolean judge = false;

	public static void visit(int i, int j) {

		maze[i][j] = 2;
		if (i == 14 && j == 14) {
			judge = true;
			return;
		}
		if (judge == false)
			if (maze[i][j + 1] == 0 && judge == false)// 右
			{
				visit(i, j + 1);
			}
		if (judge == false)
			if (maze[i + 1][j + 1] == 0 && judge == false)// 右下
			{
				visit(i + 1, j + 1);
			}
		if (judge == false)
			if (maze[i + 1][j] == 0 && judge == false)// 下
			{
				visit(i + 1, j);
			}
		if (judge == false)
			if (maze[i + 1][j - 1] == 0 && judge == false)// 左下
			{
				visit(i + 1, j - 1);
			}
		if (judge == false)
			if (maze[i][j - 1] == 0 && judge == false)// 左
			{
				visit(i, j - 1);
			}
		if (judge == false)
			if (maze[i - 1][j - 1] == 0 && judge == false)// 左上
			{
				visit(i - 1, j - 1);
			}
		if (judge == false)
			if (maze[i - 1][j] == 0 && judge == false)// 上
			{
				visit(i - 1, j);
			}
		if (judge == false)
			if (maze[i - 1][j + 1] == 0 && judge == false)// 右上
			{
				visit(i - 1, j + 1);
			}

		if (judge == false)
			maze[i][j] = 4;

		return;

	}// 單一元素上下左右的函式

	public static void main(String[] args) throws IOException {
		Scanner input = new Scanner(System.in);
		String tmp;// 用來當暫存讀取的資料

		// ///////////////////////////////////////////////////////////////
		System.out.println("請輸入一個 15*15 的迷宮: (能走為 0 ,牆壁為 1)");
		System.out.println("( 規定:左上角[0][0]為入口, 右下角[14][14]為出口, 其餘邊緣皆是牆壁 )");
		System.out.println("＊注意:測出路線之後,已走過路線會被記為2,若是死路則記為4");
		System.out.print("請輸入要讀取的檔案路徑 (例: c:\\maze.txt): ");
		String path = input.nextLine();
		System.out.println("您的迷宮為:");

		BufferedReader read = new BufferedReader(new FileReader(path));// 打開path的資料

		int map[][] = new int[15][15];
		for (int i = 0; i < 15; i++) {
			tmp = read.readLine();// 存取一行的字串資料
			System.out.println(tmp);
			for (int j = 0; j < 15; j++) {
				if (tmp.substring(j, j + 1).equals("0"))// 由於tmp裡是string之類別,藉由equal的函式判斷來使其成為int屬性的二維陣列
					map[i][j] = 0;
				else
					map[i][j] = 1;

			}
		}

		// ///////////////////////////////////////////////////////////////////
		maze = map;
		visit(0, 0);// 呼叫判斷式
		
		System.out.print("\n\n");

		// /////////////////////////////////////////////////////////////////////寫入檔案

		FileWriter writer = new FileWriter("s101502016.txt");
		for (int i = 0; i < 15; i++) {
			for (int j = 0; j < 15; j++) {
				writer.write(String.valueOf(maze[i][j]));

			}
			writer.write(String.format("%n"));
		}
		System.out.printf("---路線已畫出: s101502016.txt---");
		writer.close();

	}

}
