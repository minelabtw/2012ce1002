package ce1002.A4.s101502027;
import java.io.*;
import java.util.*;
public class A4 {
	boolean out = false;
	boolean visit(int[][] r,int x,int y) {
	        r[x][y] = 2;

	        if(x==14&&y==14)
	            out = true;

	        if(!out && r[x][y+1]==0)
	            visit(r, x, y+1);

	        if(!out && r[x+1][y+1]==0)
	           visit(r, x+1, y+1);
	           
	        if(!out && r[x+1][y]==0)
	            visit(r, x+1, y);
	            
	        if(!out && r[x+1][y-1]==0)
	            visit(r, x+1, y-1);    
	            
	        if(!out && r[x][y-1]==0)
	            visit(r, x, y-1);
	            
	        if(!out && r[x-1][y-1]==0)
	            visit(r, x-1, y-1);
	            
	        if(!out && r[x-1][y]==0)
	            visit(r, x-1, y);

	        if(!out && r[x-1][y+1]==0)
	           visit(r, x-1, y+1);

	        if(!out)
	            r[x][y] = 4;       
	        
	        return out;
	    
	    }

	public static void main(String[] args) throws IOException{
		System.out.print("請輸入一個 15*15 的迷宮: (能走為 0 ,牆壁為 1)\n( 規定:左上角[0][0]為入口, 右下角[14][14]為出口, 其餘邊緣皆是牆壁 )\n＊注意:測出路線之後,已走過路線會被記為2,若是死路則記為4\n請輸入要讀取的檔案路徑 (例: c:\\maze.txt): ");
		Scanner a= new Scanner(System.in);
		String file=a.nextLine();
		int[][] maze= new int[15][15];
		Scanner in = new Scanner(new FileReader(file));
		int j=0;
		while(in.hasNext())
		{
		String[] b=in.next().split("");
			for(int i=1;i<=15;i++)
			{
				maze[j][i-1] = Integer.parseInt(b[i]);
			}
				j++;
			}
		in.close();
		for(int i=0;i<15;i++){
			for(int q=0;q<15;q++){
				System.out.print(maze[i][q]);
			}
			System.out.print("\n");
		}
		//==================PRINT出來
		A4 s=new A4();
		s.visit(maze,0,0);//走迷宮
		
		
		java.io.File outfile = new java.io.File( "s101502027.txt" );
		java.io.PrintWriter output = new java.io.PrintWriter(outfile);
		
		for(int r=0;r<15;r++){
			for(int t=0;t<15;t++){
				output.print(maze[r][t]);
			}
			output.println();
		}
		output.close();
		System.out.println("\n---路線已畫出: s101502027.txt---");
	}
}
