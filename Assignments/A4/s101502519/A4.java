package ce1002.A4.s101502519;

import java.io.*;
import java.util.Scanner;

import org.omg.CORBA.PRIVATE_MEMBER;
import org.omg.CORBA.PUBLIC_MEMBER;

public class A4 {
	public static void main(String[] args) throws Exception{
		System.out.println("請輸入一個 15*15 的迷宮: (能走為 0 ,牆壁為 1)");
		System.out.println("( 規定:左上角[0][0]為入口, 右下角[14][14]為出口, 其餘邊緣皆是牆壁 )");
		System.out.println("＊注意:測出路線之後,已走過路線會被記為2,若是死路則記為4");
		System.out.print("請輸入要讀取的檔案路徑 (例: c:\\maze.txt): ");
		BufferedReader file = new BufferedReader(new InputStreamReader(System.in));//將輸入的暫存在 file
		String filename = file.readLine();
		
		FileReader fr = new FileReader(filename);    //讀檔
		BufferedReader br = new BufferedReader(fr);
		String line;
		int i=0,o=0;
		int[][] a=new int[15][15];
		
		while((line = br.readLine())!=null){			//逐行讀入
			
			StringReader in1 = new StringReader(line);
			int c;
			while ((c = in1.read()) != -1){      //逐字讀入
				if(o<15){
					a[i][o]=c-48;
					o=o+1;
				}
				else{
					o=0;
					i=i+1;
					a[i][o]=c-48;
					o=o+1;
				}	
			}
		}	
		
		System.out.println();
		System.out.println("您的迷宮為:");
		for( i=0;i<15;i++){       //輸出迷宮
			for(o=0;o<15;o++){
				System.out.print(a[i][o]+" ");
			}
			System.out.println();
		}
		
		A4 go= new A4();
		
		if(go.map(a,0,0)){   //呼叫遞迴函式
			FileWriter fw = new FileWriter( "s101502519.txt" );    //建檔
			for( i=0;i<15;i++,fw.write("\r\n")){       //寫檔
				for(o=0;o<15;o++)
					fw.write(Integer.toString(a[i][o]));
			}
			fw.close();
			System.out.println("\n---路線已畫出: s101502519.txt---");
		}
	}
	
	public boolean flag = false;
	public boolean map(int[][] a, int i, int o){    
	    a[i][o] = 2;     //走過的路
	    
		if(i == 14 && o == 14)       //終點
			flag = true;
		
		if(!flag && a[i+1][o+1] == 0)     //八個方位偵測
			map(a, i+1, o+1); 
		if(!flag && a[i-1][o+1] == 0) 
			map(a, i-1, o+1);
		if(!flag && a[i][o+1] == 0) 
			map(a, i, o+1);
		if(!flag && a[i+1][o-1] == 0) 
			map(a, i+1, o-1);	
		if(!flag && a[i][o-1] == 0)
			map(a, i, o-1); 
		if(!flag && a[i-1][o-1] == 0) 
			map(a, i-1, o-1); 
		if(!flag && a[i+1][o] == 0)
			map(a, i+1, o); 
		if(!flag && a[i-1][o] == 0)
			map(a, i-1, o);
		if(!flag)         //死路
			a[i][o] = 4; 

		return flag;     //回傳是否到達終點
	}
}
