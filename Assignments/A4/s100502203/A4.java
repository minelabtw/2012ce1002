package ce1002.A4.s100502203;

import java.util.*;
import java.io.FileNotFoundException;

public class A4 {

	public static void main(String[] args) throws Exception {
		final int Height = 15;
		final int Width = 15;

		System.out.println("請輸入一個 15*15 的迷宮: (能走為 0 ,牆壁為 1)");
		System.out.println("( 規定:左上角[0][0]為入口, 右下角[14][14]為出口, 其餘邊緣皆是牆壁 )");
		System.out.println("＊注意:測出路線之後,已走過路線會被記為2,若是死路則記為4");
		System.out.print("請輸入要讀取的檔案路徑 (例: c:\\maze.txt): ");
		
		Scanner input = new Scanner(System.in);
		java.io.File file = new java.io.File(input.next()); // input file name
		System.out.println("\n您的迷宮為:");
		Scanner finput = new Scanner(file);
		char tmp[];
		int map[][] = new int[Height + 2][Width + 2]; // create a map (size + 2 is for the boundary)

		for (int y = 0; y < Height + 2; y++)	// the loop to set the boundary as 1
			for (int x = 0; x < Width + 2; x++)
				if (x == 0 || y == 0 || x == Width + 1 || y == Height + 1)
					map[y][x] = 1;
				else
					continue;
		for (int y = 1; finput.hasNext(); y++) {	// get the file value
			tmp = finput.next().toCharArray();
			for (int x = 1; x < Width + 1; x++)
				map[y][x] = Character.getNumericValue(tmp[x - 1]);
		}
		for (int y = 1; y < Height + 1; y++) {
			for (int x = 1; x < Width + 1; x++)
				System.out.print(map[y][x]);
			System.out.println("");
		}
		Maze Maze = new Maze(map);
		Maze.WriteSoulution();
		System.out.println("\n---路線已畫出: s100502203.txt---");
		finput.close();
		input.close();
	}

	public static class Maze {

		private static final int Height = 15;
		private static final int Width = 15;

		int maze[][], index_x, index_y;
		boolean stepRecord[][];	// check if the program ever stepped on
		Scanner finput;

		Maze(int fmaze[][]) {
			maze = fmaze;
			stepRecord = new boolean[Height + 2][Width + 2];
			index_x = index_y = 1;
		}

		boolean isDead() {	// check if the current position is dead
			for (int y = -1; y <= 1; y++) {
				for (int x = -1; x <= 1; x++) {
					if (y == 0 && x == 0)
						continue;
					else if (!stepRecord[index_y + y][index_x + x]
							&& maze[index_y + y][index_x + x] == 0)
						return false;
				}
			}
			return true;
		}

		boolean isFailed() {	// no way to go, all dead
			if ((index_y == 1 && index_x == 1) && this.isDead())
				return true;
			else
				return false;
		}

		boolean isEnd() {	// finish the maze
			if (index_y == Height && index_x == Width)
				return true;
			else
				return false;
		}

		void WriteSoulution() throws FileNotFoundException {
			Stack<Integer> s_maze = new Stack<Integer>(); // store index
			stepRecord[index_y][index_x] = true;
			int index = index_y * (Height + 2) + index_x; // use a simple encode -> (1, 1) means 18
			while (!this.isEnd() && !this.isFailed()) {	// if finish, then out
				while (this.isDead() && !s_maze.isEmpty()) { // if dead, then pop
					index = s_maze.pop();		// simple way to decode
					index_x = index % (Width + 2);	
					index_y = index / (Height + 2);
				}
				boolean check = false;	// check if ever pushed
				for (int y = -1; y <= 1; y++) {
					for (int x = -1; x <= 1; x++) {
						if (y == 0 && x == 0)
							continue;
						else if (!stepRecord[index_y + y][index_x + x]
								&& maze[index_y + y][index_x + x] == 0) {
							index = index_y * (Height + 2) + index_x;
							s_maze.push(index);
							index_x += x;
							index_y += y;
							check = true;
							break;	// push -> out the inside loop
						}
					}
					if (check)	// push -> out the outside loop
						break;
				}
				stepRecord[index_y][index_x] = true; // note the index stepped on
			}
			for (int y = 1; y < Height + 1; y++)	// assume all steps are dead road
				for (int x = 1; x < Width + 1; x++)
					if (stepRecord[y][x])
						maze[y][x] = 4;
			if (this.isEnd()) {	// if finish, then pop the correct way and note available
				maze[index_y][index_x] = 2;
				while (!s_maze.isEmpty()) {
					index = s_maze.pop();
					index_x = index % (Width + 2);
					index_y = index / (Height + 2);
					maze[index_y][index_x] = 2;
				}
			}

			java.io.File write_file = new java.io.File("s100502203.txt"); // write
			if (write_file.exists()) {
				System.out.println("\n---檔案已存在: s100502203.txt---");
				System.exit(0);
			}
			java.io.PrintWriter output = new java.io.PrintWriter(write_file);
			for (int y = 1; y < Height + 1; y++) {
				for (int x = 1; x < Width + 1; x++)
					output.print(maze[y][x]);
				output.println("");
			}
			output.close();
		}
	}
}
