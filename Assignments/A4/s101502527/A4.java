package ce1002.A4.s101502527;
import java.io.*;  

public class A4 {
	public static int maze[][] = new int[15][15]; //maze
	private static int way[][] = {{0,1},{1,1},{1,0},{1,-1},{0,-1},{-1,-1},{-1,0},{-1,1}}; //八個方向
	public static void main(String[] args)throws IOException{
		
		System.out.println("請輸入一個 15*15 的迷宮: (能走為 0 ,牆壁為 1)");//題目說明
		System.out.println("( 規定:左上角[0][0]為入口, 右下角[14][14]為出口, 其餘邊緣皆是牆壁 )");
		System.out.println("＊注意:測出路線之後,已走過路線會被記為2,若是死路則記為4");
		System.out.print("請輸入要讀取的檔案路徑 (例: c:\\maze.txt): ");
		BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
		String line=in.readLine();//讀入路徑
		in = new BufferedReader(new FileReader(line));
		System.out.println("\n您的迷宮為:");
		for(int x=0;(line = in.readLine()) != null;x++){//用兩迴圈把迷宮讀進15*15整數陣列
		   for(int y=0;y<15;y++)
			   maze[x][y]=(int)line.charAt(y)-48;
		   System.out.println(line);
		}
		int i=0,j=0; //初始化迷宮
		maze[i][j]=2;
		
		maze(i,j);//call function
		
		java.io.File write_file = new java.io.File( "s101502527.txt");//輸出檔案
		if (!write_file.exists()) {
			write_file.createNewFile();
		}
		FileWriter fw = new FileWriter(write_file.getAbsoluteFile());
		BufferedWriter bw = new BufferedWriter(fw);
		
		
		for(int x=0;x<15;x++){  //輸出迷宮
			for(int y=0;y<15;y++)
				bw.write(maze[x][y]+48);
			bw.write("\n");
		}
		bw.close();
				System.out.println("\n---路線已畫出: s101502527.txt---");//結束
	}
	public static boolean maze(int i,int j){
		boolean con = true;//con用來記錄是否走到底
		maze[i][j]=2;//為0   所以此0變成2
		if(i==14&&j==14)//如果是出口則 傳回false代表不繼續走迷宮
			return false;
		for(int x=0;x<8;x++){//跑八個方向
			if(i+way[x][0]<=14&&i+way[x][0]>=0&&j+way[x][1]>=0&&j+way[x][1]<=14&&maze[i+way[x][0]][j+way[x][1]]==0)	//如果有0代表有路遞迴進去
				con = maze(i+way[x][0],j+way[x][1]);//傳回true代表碰到牆    此點必需換方向
			if(!con)//如果已經走到底就會傳回false 代表已經結束   並接著傳回false
				return false;
		}
		maze[i][j]=4;//碰到底的情況
		return true;
	}			
}
	

