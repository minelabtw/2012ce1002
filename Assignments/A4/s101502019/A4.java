package ce1002.A4.s101502019;
//讀檔寫檔以及import是參考同學的
import java.util.Scanner;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.OutputStream;

public class A4 {
	public static void main(String[] args) throws IOException {
		Scanner input = new Scanner(System.in);
		String tmp;
		System.out.println("請輸入一個 15*15 的迷宮: (能走為 0 ,牆壁為 1)");
		System.out.println("( 規定:左上角[0][0]為入口, 右下角[14][14]為出口, 其餘邊緣皆是牆壁 )");
		System.out.println("＊注意:測出路線之後,已走過路線會被記為2,若是死路則記為4");
		System.out.print("請輸入要讀取的檔案路徑 (例: c:\\maze.txt): ");
		String path = input.nextLine();
		System.out.println("您的迷宮為:");
		BufferedReader read = new BufferedReader(new FileReader(path));
		int[][] maze = new int[15][15];
		int i = 0, j = 0;
		for (int x = 0; x < 15; x++) {
			tmp = read.readLine();
			System.out.println(tmp);
			for (int y = 0; y < 15; y++) {
				if (tmp.substring(y, y + 1).equals("0"))
					maze[x][y] = 0;
				else
					maze[x][y] = 1;
			}
		}
		while (i == 14&&j == 14) {
			if (maze[i + 1][j + 1] == 0) {// d r
				maze[i][j] = 2;
				i++;
				j++;
				continue;
			}
			if (maze[i + 1][j] == 0) {// d 0
				maze[i][j] = 2;
				i++;
				continue;
			}
			if (maze[i][j + 1] == 0) {// 0 r
				maze[i][j] = 2;
				j++;
				continue;
			}
			if (maze[i + 1][j - 1] == 0) {// d l
				maze[i][j] = 2;
				i++;
				j--;
				continue;
			}
			if (maze[i - 1][j + 1] == 0) {// u r
				maze[i][j] = 2;
				i--;
				j++;
				continue;
			}
			if (maze[i][j - 1] == 0) {// 0 l
				maze[i][j] = 2;
				j--;
				continue;
			}
			if (maze[i - 1][j] == 0) {// u 0
				maze[i][j] = 2;
				i--;
				continue;
			}
			if (maze[i - 1][j - 1] == 0) {// u l
				maze[i][j] = 2;
				i--;
				j--;
				continue;
			}
			maze[i][j] = 4;
		}
		FileWriter writer = new FileWriter("s101502019.txt");
		for (int t = 0; t < 15; t++) {
			for (int k = 0; k < 15; k++) {
				writer.write(String.valueOf(maze[t][k]));
			}
			writer.write(String.format("%n"));
		}
		System.out.printf("---路線已畫出: s101502019.txt---");
		writer.close();
	}
}
