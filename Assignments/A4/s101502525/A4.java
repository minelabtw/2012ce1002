package ce1002.A4.s101502525;

import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Scanner;
import java.util.Stack;

public class A4 {
	static int[][] d={{0,1},{1,1},{1,0},{1,-1},{0,-1},{-1,-1},{-1,0},{-1,1}};//eight directions
	static int[][] maze=new int[17][17];
	public static void main(String[] args) throws IOException {
		System.out.println("請輸入一個 15*15 的迷宮: (能走為 0 ,牆壁為 1) ");
		System.out.println("( 規定:左上角[0][0]為入口, 右下角[14][14]為出口, 其餘邊緣皆是牆壁 )");
		System.out.println("＊注意:測出路線之後,已走過路線會被記為2,若是死路則記為4");
		System.out.print("請輸入要讀取的檔案路徑 (例: c:\\maze.txt): ");
		
		//build maze
		readMaze();
		initialize();
		System.out.println("\n您的迷宮為:");
		showMaze();
		
		//solving
		wallFollowing();

		//output
		writeMaze();
		System.out.println("\n---路線已畫出: s101502525.txt---");
	}
	
	private static void wallFollowing(){
		Stack<Integer> X=new Stack<Integer>();
		Stack<Integer> Y=new Stack<Integer>();
		Stack<Integer> F=new Stack<Integer>();//record path
		int x=1,y=1,f=0,dx=0,dy=0;
		maze[x][y]=2;//start
		while(x!=15||y!=15){//while not goal
			switch(maze[x+d[f][0]][y+d[f][1]]){
			case 1://wall
			case 4://see deadEnd as wall
				f++;
				break;
			case 0:
				X.push(x);
				Y.push(y);
				F.push(f);//record path
				x+=d[f][0];
				y+=d[f][1];//move
				f-=2;//turn back to find wall
				maze[x][y]=2;//record walked road
				break;
			case 2://in a dead end
				dx=x+d[f][0];
				dy=y+d[f][1];//record entrance of dead end
				while(x!=dx||y!=dy){//go back to entrance of dead end
					maze[x][y]=4;//record dead end
					x=X.pop();
					y=Y.pop();
					f=F.pop();
				}
				break;
			}
			//loop direction
			if(f>=8)
				f-=8;
			if(f<0)
				f+=8;
		}
	}
	private static void initialize(){
		for(int i=0;i<17;i++){
			maze[0][i]=1;
			maze[16][i]=1;
			maze[i][0]=1;
			maze[i][16]=1;//frame
		}
	}
	private static void readMaze() throws IOException{
		Scanner input=new Scanner(System.in);
		FileReader fileReader = new FileReader(input.next()); 
		for(int y=1;y<=15;y++){
			for(int x=1;x<=15;x++){
				int read=fileReader.read();
				while(read!='1'&&read!='0')
					read=fileReader.read();
				maze[x][y]=read-48;
			}
		}
	}
	private static void writeMaze() throws IOException{
        java.io.File write_file = new java.io.File("s101502525.txt");
        FileWriter fileWriter=new FileWriter(write_file);
        for(int y=1;y<=15;y++){
        	for(int x=1;x<=15;x++)
        		fileWriter.write(maze[x][y]+48);
        	fileWriter.write(13);//
        	fileWriter.write(10);//'\n'
        }
        fileWriter.close();
	}
	private static void showMaze(){
		for(int y=1;y<=15;y++){
			for(int x=1;x<=15;x++)
				System.out.print(maze[x][y]);
			System.out.println("");
		}	
	}
}