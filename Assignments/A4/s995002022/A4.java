package ce1002.A4.s995002022;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.*;

//import java.util.Stack;

//import java.util.Scanner;

public class A4 {

	public static void main(String[] args) throws Exception {
		// 1. 建立一個 FileReader 文字讀取物件
		FileReader FSource = new FileReader(
				"c:\\Users\\Mioasy\\Desktop\\maze.txt");
		char z[];
		z = new char[253];
		System.out.println("請輸入一個 15*15 的迷宮: (能走為 0 ,牆壁為 1)                      --");
		System.out.println("( 規定:左上角[0][0]為入口, 右下角[14][14]為出口, 其餘邊緣皆是牆壁 )    |--> 這些都要顯示");
		System.out.println("＊注意:測出路線之後,已走過路線會被記為2,若是死路則記為4                --");
		System.out.println("請輸入要讀取的檔案路徑 (例: c:\\maze.txt): C:\\Users\\Wayne\\Desktop\\maze.txt  ---> 路徑為使用者輸入");
		System.out.println("");
		System.out.println("您的迷宮為:");
		// 2. 利用 Reader 物件的 read 方法讀文字資料 
		FSource.read(z);

		System.out.println(z);
		System.out.println("");
		System.out.println("---路線已畫出: s995002022.txt---");
		//------new-----

		//----stack st-----
		//Stack st = new Stack();   
		//System.out.println("stack:" + st);
		//---------
		int i = 0; //initial i=0 起始位子
		
		z[i] = '2';
		// st.push(z[i]);

		while (z[252] == '0') { // 最後一個位子0的話就無限跑這個回圈直到最後一個位子被寫成2

			//-test-go

			if (z[i + 1] == '0') {//往右GO
				z[i + 1] = '2';
				//st.push(z[i+1]);
				i = i + 1;
			} else if (z[i + 18] == '0') {//往右下GO
				z[i + 18] = '2';
				//st.push(z[i+18]);
				i = i + 18;
			} else if (z[i - 16] == '0') {//往右上GO
				z[i - 16] = '2';
				//st.push(z[i-16]);
				i = i - 16;
			} else if (z[i + 17] == '0') {//往下GO
				z[i + 17] = '2';
				//st.push(z[i+17]);
				i = i + 17;
			} else if (z[i + 16] == '0') {//往左下GO
				z[i + 16] = '2';
				//st.push(z[i+16]);
				i = i + 16;
			} else if (z[i - 1] == '0') {//往左GO
				z[i - 1] = '2';
				//st.push(z[i-1]);
				i = i - 1;
			} else if (z[i - 18] == '0') {//往左上GO
				z[i - 18] = '2';
				//st.push(z[i-18]);
				i = i - 18;
			} else if (z[i - 17] == '0') {//往上GO
				z[i - 17] = '2';
				//st.push(z[i-17]);
				i = i - 17;
			}//-finish test-go
			else {
				z[i] = '4'; //stop and ready to go back

				//-start test go back

				if (z[i - 16] == '2') {
					z[i - 16] = '4';
					//st.pop();
					i = i - 16;
				} else if (z[i - 17] == '2') {
					z[i - 17] = '4';
					//st.pop();
					i = i - 17;
				} else if (z[i - 18] == '2') {
					z[i - 18] = '4';
					//st.pop();
					i = i - 18;
				} else if (z[i - 1] == '2') {
					z[i - 1] = '4';
					//st.pop();
					i = i - 1;
				} else if (z[i + 16] == '2') {
					z[i + 16] = '4';
					//st.pop();
					i = i + 16;
				} else if (z[i + 17] == '2') {
					z[i + 17] = '4';
					//st.pop();
					i = i + 17;
				} else if (z[i + 18] == '2') {
					z[i + 18] = '4';
					//st.pop();
					i = i + 18;
				} else if (z[i + 1] == '2') {
					z[i + 1] = '4';
					//st.pop();
					i = i + 1;
				}
			}
			//System.out.println("i="+i);//test if some section are right
			//System.out.println("stack:" + st);//test what is in stack
		}

		//System.out.println(z); // see output in console
		
		
		
		//-----output txt------
		BufferedWriter wStream = new BufferedWriter(new FileWriter(
				"s995002022.txt"));
		wStream.write(z);
		wStream.newLine();
		wStream.close();

	}

}
