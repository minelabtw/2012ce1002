package CE1002.A4.s101502518;

import java.io.*;
import java.util.Scanner;

public	class A4 {
		
	private int starti, startj;
	private boolean flag = false;

	public static void main(String[] args) throws IOException {
			
		char[] data = new char[500];
		int[][] maze = new int[15][15];
		
		System.out.println("請輸入一個 15*15 的迷宮: (能走為 0 ,牆壁為 1)");
		System.out.println("( 規定:左上角[0][0]為入口, 右下角[14][14]為出口, 其餘邊緣皆是牆壁 )");
		System.out.println("＊注意:測出路線之後,已走過路線會被記為2,若是死路則記為4");
		System.out.print("請輸入要讀取的檔案路徑 (例: c:\\maze.txt): ");
		
		Scanner input = new Scanner(System.in);
		String path = input.nextLine();
		System.out.println("\n您的迷宮為:");
		BufferedReader in = new BufferedReader(new FileReader(path));
		String t;
		for(int i=0;i<15;i++)
		{
			t = in.readLine();
			for(int j=0;j<15;j++) //讀檔
				if(t.substring(j,j+1).equals("0"))
					maze[i][j] = 0;
				else
					maze[i][j] = 1;
			System.out.println(t);
		}
	 
	A4 man = new A4();
		
	if(man.go(maze))
	{
		FileWriter filewriter = new FileWriter( "s101502518.txt" );
		for(int i=0;i<15;i++,filewriter.write("\r\n"))
		{
			for(int j=0;j<15;j++)//寫檔
				filewriter.write(Integer.toString(maze[i][j]));
		}
		filewriter.close();
		System.out.println("\n---路線已畫出: s101502518.txt---");
	}
}

	public boolean go(int[][] maze)
	{
		return visit(maze, starti, startj);
	}

	private boolean visit(int[][] maze, int i, int j) //走位
	{
	    maze[i][j] = 2; 
	    
		if(i == 14 && j == 14) //確認已完成迷宮
			flag = true;
		
		if(!flag && maze[i+1][j+1] == 0 && i<14 && j<14) 
			visit(maze, i+1, j+1); 
		if(!flag && maze[i-1][j+1] == 0 && i>0 && j<14) 
			visit(maze, i-1, j+1); 
		if(!flag && maze[i-1][j-1] == 0 && i>0 && j>0) 
			visit(maze, i-1, j-1); 
		if(!flag && maze[i+1][j-1] == 0 && i<14 && j>0) 
			visit(maze, i+1, j-1);
		if(!flag && maze[i][j+1] == 0 && j<14) 
			visit(maze, i, j+1); 
		if(!flag && maze[i+1][j] == 0 && i<14)
			visit(maze, i+1, j); 
		if(!flag && maze[i][j-1] == 0 && j>0)
			visit(maze, i, j-1); 
		if(!flag && maze[i-1][j] == 0 && i>0)
			visit(maze, i-1, j);
		if(!flag) 
			maze[i][j] = 4; 

		return flag; 
	}
} 

