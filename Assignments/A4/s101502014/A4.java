package ce1002.A4.s101502014;
import java.util.Scanner;
import java.util.Stack;
public class A4 {
	public static void main(String[] args) throws Exception{
		int j = 0 , x = 0 , y = 0; //j為輸入地圖時的列 , x為目前走迷宮的列 , y為目前走迷宮的行
		int[][] map = new int[15][15]; //map為15x15的二維地圖
		Scanner input = new Scanner(System.in); //input為輸入的功能
		System.out.println("請輸入一個 15*15 的迷宮: (能走為 0 ,牆壁為 1)");
		System.out.println("( 規定:左上角[0][0]為入口, 右下角[14][14]為出口, 其餘邊緣皆是牆壁 )");
		System.out.println("＊注意:測出路線之後,已走過路線會被記為2,若是死路則記為4");
		System.out.print("請輸入要讀取的檔案路徑");
		String target = input.next(); //輸入地圖檔案的路徑 , target紀錄路徑
		java.io.File ifile = new java.io.File(target);  //ifile為開啟檔案的功能
		Scanner cin = new Scanner(ifile); //cin為讀取檔案的功能
		while(cin.hasNext()) //讀取檔案直到沒東西可以讀取
		{
			String[] number = cin.next().split(""); //將讀取的字串拆成一個個的字元
			for(int i = 0 ; i < 15 ; i++)
				map[j][i] = Integer.parseInt(number[i + 1]); //將字元轉換成數字 , 並存入map陣列中
			j++;
		}
		cin.close(); //關閉檔案
		System.out.println("\n您的迷宮為:");
		for(int k = 0 ; k < 15 ; k++) //輸出檔案的地圖資訊
		{
			for(int l = 0 ; l < 15 ; l++)
				System.out.print(map[k][l]);
			System.out.println("");
		}
		Stack<Integer> track_x = new Stack<Integer>(); //建立一個紀錄走迷宮每一步的列值的stack
		Stack<Integer> track_y = new Stack<Integer>(); //建立一個紀錄走迷宮每一步的行值的stack
		map[0][0] = 2; //設起點為已走過的路 , 並改為2
		track_x.push(x); //把起點的列值丟入track_x
		track_y.push(y); //把起點的行值丟入track_y
		while(true)
		{
			if(x - 1 >= 0 && y - 1 >= 0 && map[x - 1][y - 1] == 0) //判斷左上方的路是否能走
			{ 
				map[x - 1][y - 1] = 2; //若能走的話 , 則將該格的值改為2(已走過)
				x--; //將目前的位置移到那一格
				y--;
				track_x.push(x); //把目前的位置丟入track_x
				track_y.push(y); //把目前的位置丟入track_y
			}
			else if(x - 1 >= 0 && map[x - 1][y] == 0) //判斷上方的路是否能走
			{
				map[x - 1][y] = 2; //若能走的話 , 則將該格的值改為2(已走過)
				x--; //將目前的位置移到那一格
				track_x.push(x); //把目前的位置丟入track_x
				track_y.push(y); //把目前的位置丟入track_y	
			}
			else if(x - 1 >= 0 && y + 1 < 15 && map[x - 1][y + 1] == 0) //判斷右上方的路是否能走
			{
				map[x - 1][y + 1] = 2; //若能走的話 , 則將該格的值改為2(已走過)
				x--; //將目前的位置移到那一格
				y++;
				track_x.push(x); //把目前的位置丟入track_x
				track_y.push(y); //把目前的位置丟入track_y	
			}
			else if(y + 1 < 15 && map[x][y + 1] == 0) //判斷右方的路是否能走
			{
				map[x][y + 1] = 2; //若能走的話 , 則將該格的值改為2(已走過)
				y++; //將目前的位置移到那一格
				track_x.push(x); //把目前的位置丟入track_x
				track_y.push(y); //把目前的位置丟入track_y		
			}
			else if(x + 1 < 15 && y + 1 < 15 && map[x + 1][y + 1] == 0) //判斷右下方的路是否能走
			{
				map[x + 1][y + 1] = 2; //若能走的話 , 則將該格的值改為2(已走過)
				x++; //將目前的位置移到那一格
				y++;
				track_x.push(x); //把目前的位置丟入track_x
				track_y.push(y); //把目前的位置丟入track_y
			}
			else if(x + 1 < 15 && map[x + 1][y] == 0) //判斷下方的路是否能走
			{
				map[x + 1][y] = 2; //若能走的話 , 則將該格的值改為2(已走過)
				x++; //將目前的位置移到那一格	
				track_x.push(x); //把目前的位置丟入track_x
				track_y.push(y); //把目前的位置丟入track_y
			}
			else if(x + 1 < 15 && y - 1 >= 0 && map[x + 1][y - 1] == 0) //判斷左下方的路是否能走
			{
				map[x + 1][y - 1] = 2; //若能走的話 , 則將該格的值改為2(已走過)
				x++; //將目前的位置移到那一格	
				y--;
				track_x.push(x); //把目前的位置丟入track_x
				track_y.push(y); //把目前的位置丟入track_y
			}
			else if(y - 1 >= 0 && map[x][y - 1] == 0) //判斷左方的路是否能走
			{
				map[x][y - 1] = 2; //若能走的話 , 則將該格的值改為2(已走過)
				y--; //將目前的位置移到那一格		
				track_x.push(x); //把目前的位置丟入track_x
				track_y.push(y); //把目前的位置丟入track_y
			}
			else //若八個方向的路都無法走 , 則回到上一步
			{
				map[x][y] = 4; //將該格的值改為4(死路)
				track_x.pop(); //把目前的位置丟出track_x
				track_y.pop(); //把目前的位置丟出track_y
				x = track_x.peek(); //x改為上一個位置的列(track_x的最高點)
				y = track_y.peek(); //x改為上一個位置的行(track_y的最高點)
			}
			if(map[14][14] == 2) //若終點的值為2(已走過) , 則代表走到終點了 , 並跳出迴圈
 				break;
		}
		java.io.File write_file = new java.io.File("s101502014.txt"); //write_file為開啟檔案的功能
		java.io.PrintWriter output = new java.io.PrintWriter(write_file); //output為寫入檔案的功能
		for(int k = 0 ; k < 15 ; k++) //將後來有逃離路線的迷宮地圖寫入檔案
		{
			for(int l = 0 ; l < 15 ; l++)
				output.print(map[k][l]);
			output.println("");
		}
		output.close(); //關閉檔案
		System.out.println("\n---路線已畫出: s101502014.txt---");
	}
}