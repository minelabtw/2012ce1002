package ce1002.A4.s101502523;
import java.io.*;
import java.util.*;
public class A4 {
	public static void main(String[] args) throws IOException{
		Scanner input = new Scanner(System.in);
		char[] array = new char[225];
		System.out.print("請輸入一個 15*15 的迷宮: (能走為 0 ,牆壁為 1)\n" + "( 規定:左上角[0][0]為入口, 右下角[14][14]為出口, 其餘邊緣皆是牆壁 )\n" +
				"＊注意:測出路線之後,已走過路線會被記為2,若是死路則記為4\n" + "請輸入要讀取的檔案路徑 (例: c:\\maze.txt): ");
		String file = input.nextLine();
		BufferedReader reader = new BufferedReader(new FileReader(file));
		String str = new String();
		String tempString = null;
		//讀取資料並儲存於陣列中(先存string型態再轉char[]型態)
		while((tempString = reader.readLine())!=null){
			str = str + tempString;
		}
		array = str.toCharArray(); //string to char[]
		//顯示迷宮
		System.out.println("\n您的迷宮為:");
		for(int i=0 ; i<225 ; i++){
			System.out.print(array[i]);
			if((i-14)%15==0)
				System.out.println("");
		}
		goToGoal(array);
		creatFile(array);
	}
	//走迷宮
	public static void goToGoal(char[] maze) throws IOException{
		int x = 0, place=0, counter=0;
		int[] stack = new int[225];
		//起點先設定走過,即為2
		maze[0] = '2';
		//走迷宮迴圈
		while(true){
			//每到新地點皆從右上開始順時鐘方向判斷可走的路,走過的路馬上變為2,利用stack陣列儲存依序走過的位置(方便往回走)
			switch(counter){
				case 0:
					//右上判斷
					if(x-14>0 && (x-14)%15!=0 && maze[x-14]=='0'){
						x-=14;
						maze[x] = '2';
						stack[place] = x;
						place++;
						counter = 0;
					}
					else
						counter++;
					break;
				case 1:
					//右邊判斷
					if(x+1<=224 && (x-14)%15!=0 && maze[x+1]=='0'){
						x++;
						maze[x] = '2';
						stack[place] = x;
						place++;
						counter = 0;
					}
					else
						counter++;
					break;
				case 2:		
					//右下判斷
					if(x+16<=224 && (x-14)%15!=0 && maze[x+16]=='0'){
						x+=16;
						maze[x] = '2';
						stack[place] = x;
						place++;
						counter = 0;
					}
					else
						counter++;
					break;
				case 3:
					//下方判斷
					if(x+15<=224 && maze[x+15]=='0'){
						x+=15;
						maze[x] = '2';
						stack[place] = x;
						place++;
						counter = 0;
					}
					else
						counter++;
					break;
				case 4:
					//左下判斷
					if(x+14<=224 && x%15!=0 && maze[x+14]=='0'){
						x+=14;
						maze[x] = '2';
						stack[place] = x;
						place++;	
						counter = 0;
					}
					else
						counter++;
					break;
				case 5:
					//左邊判斷
					if(x-1>=0 && x%15!=0 && maze[x-1]=='0'){
						x--;
						maze[x] = '2';
						stack[place] = x;
						place++;
						counter = 0;
					}
					else
						counter++;
					break;
				case 6:
					//左上判斷
					if(x-16>=0 && x%15!=0 && maze[x-16]=='0'){
						x-=16;
						maze[x] = '2';
						stack[place] = x;
						place++;	
						counter = 0;
					}
					else
						counter++;
					break;
				case 7:
					//上方判斷
					if(x-15>=0 && maze[x-15]=='0'){
						x-=15;
						maze[x] = '2';
						stack[place] = x;
						place++;
						counter = 0;
					}
					else
						counter++;
					break;
				default:
					//死路時回上一動(此時上一動為2),並重新找尋四周可走的新路(重新開始switch裡的方向判斷),死路的路變為4
					if(place-2>=0){
						place--;
						maze[stack[place]] = '4';	
						x = stack[place-1];
						counter = 0;
					}
					break;		
			}//end of switch
			if(x==224)
				break;
		}//end of while 
	}
	//建立檔案並寫入
	public static void creatFile(char[] maze) throws IOException{	
		java.io.File write_file = new java.io.File("s101502523.txt");	//檔案路徑
		java.io.PrintWriter output = new java.io.PrintWriter(write_file);
		//寫入檔案
		for(int j = 0; j < 225; j++){
			output.write(String.valueOf(maze[j]));
			if((j-14)%15==0)
				output.write("\r\n");
        }
		output.close();
		System.out.print("\n---路線已畫出: " + write_file + "---");
	}	
}
