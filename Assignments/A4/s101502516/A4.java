package ce1002.A4.s101502516;
import java.util.Stack;
import java.io.File;
import java.util.Scanner;
import java.io.*;
public class A4 {
	public static void main(String[] args)throws Exception{ // 經過throws修飾，則可直接使用檔案，不需try-catch
		System.out.println( "請輸入一個 15*15 的迷宮: (能走為 0 ,牆壁為 1)" );
		System.out.println( "( 規定:左上角[0][0]為入口, 右下角[14][14]為出口, 其餘邊緣皆是牆壁 )" );
		System.out.println( "＊注意:測出路線之後,已走過路線會被記為2,若是死路則記為4" );
		System.out.print( "請輸入要讀取的檔案路徑 (例: c:\\maze.txt): " );
		
		Scanner add = new Scanner(System.in); // 輸入檔案位址
		String a = add.nextLine(); 
		
		java.io.File maze = new File( a );
		Scanner input = new Scanner( maze ); // 取得檔案內容
		int [][] b = new int [15][15];

		while( input.hasNext() ) // 將所有檔案內容寫進二維陣列b
		{
			for( int i = 0; i < 15; i++ )
			{
				String[] c = input.next().split( "" );
				for ( int j = 0; j < 15; j++ )
				{
					b[i][j] = Integer.parseInt( c[j+1] );
				}
			}
		}
		input.close(); // 關檔
		
		System.out.println( "" );
		System.out.println( "您的迷宮為:" );
		
		for ( int k = 0; k < 15; k++ ) // 輸出迷宮
		{
			for ( int l = 0; l < 15; l++ )
			{
				System.out.print( b[k][l] + " " );
			}
			System.out.println( " " );
		}
		System.out.println( " " );
		System.out.println( "---路線已畫出: s101502516.txt---" );
			
		Stack ai = new Stack(); // 建立兩個stack
		Stack aj = new Stack();
		Stack bi = new Stack(); 
		Stack bj = new Stack();
		int i = 0, j = 0, check = 0;
		int x ,y;
		
		ai.push( i );
		aj.push( j );
		/* 走迷宮，遇到周圍有0就變成2，且push下一步的陣列座標，新座標取代舊座標繼續走，
		如果遇到周圍都不是0則變成4，且pop上一步座標，並且使其回到上一步的座標。*/
		while ( i != 14 && j != 14 )
		{
			b[0][0] = 2;
			if ( i - 1 >= 0 && b[i - 1][j] == 0 )
			{
				b[i - 1][j] = 2;
				ai.push( i - 1 );
				aj.push( j );
				bi.push( i - 1 );
				bj.push( j );
				i = i - 1;
			}else if ( i - 1 >= 0 && j + 1 <= 15 && b[i - 1][j + 1] == 0 )
			{
				b[i - 1][j + 1] = 2;
				ai.push( i - 1 );
				aj.push( j + 1 );
				bi.push( i - 1 );
				bj.push( j + 1 );
				i = i - 1;
				j = j + 1;
			}else if ( j + 1 <= 15 && b[i][j + 1] == 0 )
			{	
				b[i][j + 1] = 2;
				ai.push( i );
				aj.push( j + 1 );
				bi.push( i );
				bj.push( j + 1 );
				j = j + 1;	
			}else if ( i + 1 <= 15 && j + 1 <= 15 && b[i + 1][j + 1] == 0 )
			{
				b[i + 1][j + 1] = 2;
				ai.push( i + 1 );
				aj.push( j + 1 );
				bi.push( i + 1 );
				bj.push( j + 1 );
				i = i + 1;
				j = j + 1;
			}else if ( i + 1 <= 15 && b[i + 1][j] == 0 )
			{
				b[i + 1][j] = 2;
				ai.push( i + 1 );
				aj.push( j );
				bi.push( i + 1 );
				bj.push( j );
				i = i + 1;	
			}else if ( i + 1 <= 15 && j -1 >= 0 && b[i + 1][j - 1] == 0 )
			{
				b[i + 1][j - 1] = 2;
				ai.push( i + 1 );
				aj.push( j - 1 );
				bi.push( i + 1 );
				bj.push( j - 1 );
				i = i + 1;
				j = j - 1;
			}else if ( j - 1 >= 0 && b[i][j - 1] == 0 )
			{
				b[i][j - 1] = 2;
				ai.push( i );
				aj.push( j - 1 );
				bi.push( i );
				bj.push( j - 1 );
				j = j - 1;
			}else if ( j -1 >= 0 && i - 1 >= 0 && b[i - 1][j - 1] == 0 )
			{
				b[i - 1][j - 1] = 2;
				ai.push( i - 1 );
				aj.push( j - 1 );
				bi.push( i - 1 );
				bj.push( j - 1 );
				i = i - 1;
				j = j - 1;			
			}else
			{
				b[i][j] = 4;
				i = (int)ai.pop();
				j = (int)aj.pop();
			}
		}
		
		bi.pop(); //  debug(若有分岔死路路口不會變成4)
		bj.pop();
		while ( !bi.empty() && !bj.empty() )
		{
			i = (int)bi.pop();
			j = (int)bj.pop();
 			if ( i - 1 >= 0 && b[i - 1][j] == 2 )
			{
 				check++;
			}
 			if ( i - 1 >= 0 && j + 1 <= 15 && b[i - 1][j + 1] == 2 )
			{
				check++;
			}
 			if ( j + 1 <= 15 && b[i][j + 1] == 2 )
			{	
				check++;
			}
 			if ( i + 1 <= 15 && j + 1 <= 15 && b[i + 1][j + 1] == 2 )
			{
				check++;
			}
 			if ( i + 1 <= 15 && b[i + 1][j] == 2 )
			{
				check++;
			}
 			if ( i + 1 <= 15 && j -1 >= 0 && b[i + 1][j - 1] == 2 )
			{
				check++;
			}
 			if ( j - 1 >= 0 && b[i][j - 1] == 2 )
			{
				check++;
			}
 			if ( j -1 >= 0 && i - 1 >= 0 && b[i - 1][j - 1] == 2 )
			{
				check++;		
			}
 			if ( check < 2 )
 			{
 				b[i][j] = 4;
 			}
 			else
 			{
 				b[i][j] = 2;
 			}
 			check = 0;
		}
		// 創新檔案並將過關路徑寫入檔案中
		java.io.File write_file = new java.io.File( "C:\\Users\\Wayne\\workspace\\ce1002-A\\s101502516.txt" );
		PrintWriter out = new PrintWriter( write_file );
		for ( int k = 0; k < 15; k++ )
		{
			for ( int l = 0; l < 15; l++ )
			{
				out.print( b[k][l] );
			}
			out.println( "" );
		}
		out.close();
	}
}