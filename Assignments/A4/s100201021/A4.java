package ce1002.A4.s100201021;
import java.io.*;
import java.util.Scanner;
public class A4 {
	public static void main(String[] args) throws IOException{
		Scanner inp=new Scanner(System.in);
		int[][] maze=new int[15][15];
		int i=0,j=0;
		System.out.println("請輸入一個 15*15 的迷宮: (能走為 0 ,牆壁為 1)");
		System.out.println("( 規定:左上角[0][0]為入口, 右下角[14][14]為出口, 其餘邊緣皆是牆壁 )");
		System.out.println("＊注意:測出路線之後,已走過路線會被記為2,若是死路則記為4");
		System.out.print("請輸入要讀取的檔案路徑 (例: c:\\maze.txt): ");
		String filename = inp.next();
		java.io.File file=new File(filename);
		while(file.exists()==false){
			System.out.print("請輸入要讀取的檔案路徑 (例: c:\\maze.txt): ");
			filename = inp.next();
			file=new File(filename);
		}
		Scanner input=new Scanner(file);
		//
		while(input.hasNext()){
			String[] number= input.next().split("");
			for(j=0;j<15;j++)
				maze[i][j] = Integer.parseInt(number[j+1]); 
			i++;
		}
		inp.close();
		//
		System.out.println("您的迷宮為:");
		for(i=0;i<15;i++){
			for(j=0;j<15;j++){
				System.out.print(maze[i][j]);
			}
			System.out.println();
		}
		///////////
		int x=1,y=1,anm=0,cx=0,cy=0,ss=0; 	//(x,y)=now,(cx,cy)=last point. ss is for debug and not used in normal.
		int[] can = new int[10];	//Jiugongge, i is Direction
		while(x!=14 || y!=14){
			maze[x][y]+=2;	//path point = "2"
			maze[0][0]=2;	//start and end must be "2"
			anm=0;			//anm = number of "0" around.
			cx=x;cy=y;
			for(int to0=1;to0<10;to0++)
				can[to0]=0;
			if(maze[x+1][y+1]==0){
				anm++;can[3]=1;
			}
			if(maze[x+1][y]==0){
				anm++;can[2]=1;
			}
			if(maze[x+1][y-1]==0){
				anm++;can[1]=1;
			}
			if(maze[x][y+1]==0){
				anm++;can[6]=1;
			}
			if(maze[x][y-1]==0){
				anm++;can[4]=1;			
			}
			if(maze[x-1][y+1]==0){
				anm++;can[9]=1;				
			}
			if(maze[x-1][y]==0){
				anm++;can[8]=1;				
			}
			if(maze[x-1][y-1]==0){
				anm++;can[7]=1;		
			}
			//
			if(anm>0)
				maze[x][y]=2;
			if(anm==0){	//Blind alley
				maze[x][y]+=2;
				if(maze[x][y]>4)
					maze[x][y]=4;	//cannot bigger than 4
				if(maze[x+1][y+1]==2){
					x++;y++;
				}
				else if(maze[x+1][y]==2){
					x++;
				}
				else if(maze[x][y+1]==2){
					y++;
				}
				else if(maze[x+1][y-1]==2){
					x++;y--;
				}
				else if(maze[x-1][y+1]==2){
					x--;y++;
				}
				else if(maze[x][y-1]==2){
					y--;
				}
				else if(maze[x-1][y]==2){
					x--;
				}
				else if(maze[x-1][y-1]==2){
					x--;y--;
				}

			}	//if move to "0",last is "2" not "4"
			else if(can[3]==1){
				x++;y++;
			}
			else if(can[2]==1){
				x++;
			}
			else if(can[6]==1){
				y++;
			}
			else if(can[1]==1){
				x++;y--;
			}
			else if(can[9]==1){
				x--;y++;
			}
			else if(can[4]==1){
				y--;
			}
			else if(can[8]==1){
				x--;
			}
			else if(can[7]==1){
				x--;y--;
			}
			else{	//anm=0
			}
		}
		maze[14][14]=2;	//end must be "2"
		//output
		java.io.PrintWriter write_file = new java.io.PrintWriter( "s100201021.txt" );
		for(x=0;x<15;x++){
			for(y=0;y<15;y++)
				write_file.print(maze[x][y]);
			write_file.println();
		}
		write_file.close();
		//
		System.out.println("\n---路線已畫出: s100201021.txt---");
	}
}
//c:\Downloads\maze.txt
