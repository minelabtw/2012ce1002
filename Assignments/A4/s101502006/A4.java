package ce1002.A4.s101502006;

import java.io.*;
import java.util.Scanner;

public class A4 
{
	public static void main(String[] args) throws IOException 
	{  
        Scanner scanner=new Scanner(System.in);
        System.out.print("請輸入一個 15*15 的迷宮: (能走為 0 ,牆壁為 1)\n( 規定:左上角[0][0]為入口, 右下角[14][14]為出口, 其餘邊緣皆是牆壁 )\n＊注意:測出路線之後,已走過路線會被記為2,若是死路則記為4\n請輸入要讀取的檔案路徑 (例: c:\\maze.txt):");
        
        
        String address= scanner.next(); // to record the address of the file.
        FileReader fr=new FileReader(address);  
        System.out.println("");
        scanner.close();
        
        
        
        ////////////////   file to Array   ///////////////////
        Scanner input = new Scanner(fr);
        int[][] maze = new int[15][15];
        int k=0;
        while(input.hasNext())
        {
	        String[] number=input.next().split("");
	        
	        for(int i=1;i<=15;i++)
	        {
	        	maze[k][i-1] = Integer.parseInt(number[i]);
	        }
	        
	        k++;
        }
        input.close();
        /////////////////////////////////////////////////////
        
        System.out.println("您的迷宮為:");
        point(maze);
        System.out.println();
        
        
        
        VISIT(maze,0,0); // start at (0,0)
    }  
	
	public static void VISIT(int maze[][],int i,int j)
    {
    	//把走過的路以2表示
    	maze[i][j] = 2;

    	
    	//走到終點就輸出
    	if(i == 14 && j == 14){
    		write(maze);
    	}
    	
    	//如果是0就往右走
    	else if(maze[i][j+1] == 0)
    		VISIT(maze,i,j+1);
    	//如果是0就右下走
    	else if(maze[i+1][j+1] == 0)
    		VISIT(maze,i+1,j+1);
    	//如果是0就往下走
    	else if(maze[i+1][j] == 0)
    		VISIT(maze,i+1,j);
    	//如果是0就往左下走
    	else if(maze[i+1][j-1] == 0)
    		VISIT(maze,i+1,j-1);
    	//如果是0就往左走
    	else if(maze[i][j-1] == 0)
    		VISIT(maze,i,j-1);
    	//如果是0就往左上走
    	else if(maze[i-1][j-1] == 0)
    		VISIT(maze,i-1,j-1);
    	//如果是0就往上走
    	else if(maze[i-1][j] == 0)
    		VISIT(maze,i-1,j);
    	//如果是0就往右上走
    	else if(maze[i-1][j+1] == 0)
    		VISIT(maze,i-1,j+1);
    	
    	
    	
    	//如果是2就往左上走
    	else if(maze[i-1][j-1] == 2)
    	{
    		maze[i][j] =4;
    		VISIT(maze,i-1,j-1);
    	}
    	//如果是2就往左走
    	else if(maze[i][j-1] == 2)
    	{
    		maze[i][j] =4;
    		VISIT(maze,i,j-1);
    	}
    	//如果是2就往左下走
    	else if(maze[i+1][j-1] == 2)
    	{
    		maze[i][j] =4;
    		VISIT(maze,i+1,j-1);
    	}
    	//如果是2就往下走
    	else if(maze[i+1][j] == 2)
    	{
    		maze[i][j] =4;
    		VISIT(maze,i+1,j);
    	}
    	//如果是2就右下走
    	else if(maze[i+1][j+1] == 2)
    	{
    		maze[i][j] =4;
    		VISIT(maze,i+1,j+1);
    	}
    	//如果是2就往右走
    	else if(maze[i][j+1] == 2)
    	{
    		maze[i][j] =4;
    		VISIT(maze,i,j+1);
    	}
    	//如果是2就往右上走
    	else if(maze[i-1][j+1] == 2)
    	{
    		maze[i][j] =4;
    		VISIT(maze,i,j+1);
    	}
    	//如果是2就往上走
    	else if(maze[i-1][j] == 2)
    	{
    		maze[i][j] =4;
    		VISIT(maze,i-1,j);
    	}
    	
    }


	
	/////////// void to print the Array //////////////////
	public static void point(int maze[][])
    {
		for(int i=0;i<15;i++)
        {
        	for(int j=0;j<15;j++)
        	{
        		System.out.print(maze[i][j]);
        	}
        	System.out.println();
        }
    }
	
	
	///////////////  void to create file  ////////////////
	public static void write(int maze[][])
	{
		String mazeString="";  //initialize mazeString.
		
		File write_file = new java.io.File("s101502006.txt");
		
		try
	    {
	      FileWriter fwriter=new FileWriter(write_file);
	      
	      for(int i=0;i<15;i++)
	        {
	        	for(int j=0;j<15;j++)
	        	{
	        		int temp = maze[i][j];
	    			String strtemp = String.valueOf(temp);  // Integer to String.
	    			mazeString += strtemp;
	        	}
	        	fwriter.write(mazeString+"\r\n");
	        	mazeString=""; //initialize mazeString.
	        }
	      fwriter.close();
	    }
		
	    catch(Exception e)
	    {
	      e.printStackTrace();
	    }
		
		System.out.print("---路線已畫出: s101502006.txt---");
	}
	
}
