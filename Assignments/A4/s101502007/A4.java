package ce1002.A4.s101502007;
import java.util.Scanner;
import java.util.Stack;
import java.io.*;  
public class A4 {
	public static void main(String[] args) throws IOException {
        System.out.println("請輸入一個 15*15 的迷宮: (能走為 0 ,牆壁為 1) ");
        System.out.println("( 規定:左上角[0][0]為入口, 右下角[14][14]為出口, 其餘邊緣皆是牆壁 )  ");
        System.out.println("＊注意:測出路線之後,已走過路線會被記為2,若是死路則記為4");
        System.out.println("請輸入要讀取的檔案路徑");
        Scanner input=new Scanner(System.in);
        String ch=input.nextLine();
		int [][]x=new int [15][15];
        BufferedReader reader=new BufferedReader(new FileReader(ch));
        String s;
        for(int i=0;i<15;i++)
        {
        	s = reader.readLine();
        	for(int j=0;j<15;j++) 
        	{
        	if(s.substring(j,j+1).equals("0"))
        		x[i][j] = 0;
        	else
        		x[i][j] = 1;
        }
         
        int a=0,b=0;
        Stack x1=new Stack();
        Stack x2=new Stack();
        Stack x3=new Stack();
        Stack x4=new Stack();
        int z=0;
        while (a<=14 && b<=14)
        {
        	int y=0;
        	if (a>=1 && a<=13 && b>=1 && b<=13)
        	{
	        	for (int k=-1;k<=1;k++)
	        	{
	        		for (int n=-1;n<=1;n++)
	        		{
	        			if (x[a+k][b+n]==0 && k!=0 && n!=0)
	        			{
	        				x[a][b]=2;
	        				y++;
	        			}
	        		}
	        	}
        	}
        	else if (a==0 && b==0)
        	{
        		for (int k=0;k<=1;k++)
	        	{
	        		for (int n=0;n<=1;n++)
	        		{
	        			if (x[a+k][b+n]==0 && k!=0 && n!=0)
	        			{
	        				x[a][b]=2;
	        				y++;
	        			}
	        		}
	        	}
        	}
        	if (y>1)
			{
				x1.push(a);
				x2.push(b);
			}
        	if (a>=1 && a<=13 && b>=1 && b<=13)
        	{
        		for (int k=-1;k<=1;k++)
        		{
	        		for (int n=-1;n<=1;n++)
	        		{
	        			if (x[a+k][b+n]==0 && y>1 && k!=0 && n!=0)
	        			{
	        				z++;
	        				x[a+k][b+n]=2;
	        				a=a+k;
	        				b=b+n;
	        				break;
	        			}
	        			else if (x[a+k][b+n]==0 && y==1 && z>0 && k!=0 && n!=0)
	        			{
	        				x[a+k][b+n]=2;
	        				x3.push(a);
	        				x4.push(b);
	        				a=a+k;
	        				b=b+n;
	        				break;
	        			}
	        			else if (x[a+k][b+n]==0 && y==1 && z==0  && k!=0 && n!=0)
	        			{
	        				x[a+k][b+n]=2;
	        				a=a+k;
	        				b=b+n;
	        				break;
	        			}
	        			else if (y==0)
	        			{
	        				int q,r;
	        				if (x3.empty()!=true)
	        				{
	        					q=(int)(x3.pop());
	        					r=(int)(x4.pop());
	        					x[q][r]=4;
	        				}
	        				a=(int)(x1.pop());
	    					b=(int)(x2.pop());
	        			}
	        		}
	        		break;
	        	}
        	}
        	
        	else if (a==0 && b==0)
        	{
        		for (int k=0;k<=1;k++)
	        	{
	        		for (int n=0;n<=1;n++)
	        		{
	        			if (x[a+k][b+n]==0 && y>1 && k!=0 && n!=0)
	        			{
	        				z++;
	        				x[a+k][b+n]=2;
	        				a=a+k;
	        				b=b+n;
	        				break;
	        			}
	        			else if (x[a+k][b+n]==0 && y==1 && z>0  && k!=0 && n!=0)
	        			{
	        				x[a+k][b+n]=2;
	        				x3.push(a);
	        				x4.push(b);
	        				a=a+k;
	        				b=b+n;
	        				break;
	        			}
	        			else if (x[a+k][b+n]==0 && y==1 && z==0  && k!=0 && n!=0)
	        			{
	        				x[a+k][b+n]=2;
	        				a=a+k;
	        				b=b+n;
	        				break;
	        			}
	        			else if (y==0)
	        			{
	        				int q,r;
	        				if (x3.empty()!=true)
	        				{
	        					q=(int)(x3.pop());
	        					r=(int)(x4.pop());
	        					x[q][r]=4;
	        				}
	        				a=(int)(x1.pop());
	    					b=(int)(x2.pop());
	        			}
	        		}
	        		break;
	        	}
        	}
        }
        for (int e=0;e<=14;e++)
        	for (int f=0;f<=14;f++)
        	{
        		if (f==14)
        			System.out.println(x[e][f]);
        		else
        			System.out.print(x[e][f]);
        	}
        }
	}
}