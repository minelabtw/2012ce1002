package ce1002.A4.s101502504;
import java.io.*;
import java.util.Scanner;
public class A4 
{
	private static int[][] array = new int[15][15];//initial array
///////////////////////////////////////////////readfile()
	public static void readfile()throws IOException
	{  
        Scanner input = new Scanner(System.in);//user input the address of maze.txt
        File file = new File(input.nextLine());//find it
        try 
        {  
            BufferedReader rd = new BufferedReader(new FileReader(file)); 
            String str = "";//initial string str
            String s = rd.readLine();//store the first line of maze.txt 
            while (s != null)//if there isn't any data to read
            {  
                str += s;//store the whole numbers
                s = rd.readLine();//s is every line of maze.txt
            }

            int num=1;
            String[] strarray = str.split("");//String to Array
   
            for(int i=0; i<15; i++)
            	for(int j=0; j<15; j++)
            	{
            		array[i][j]=Integer.parseInt(strarray[num]);//Array to intArray
            		num++;
            	}
        } 
        
        catch (FileNotFoundException e)//false
        {  
            e.printStackTrace();  
        } 
        catch (IOException e)//false
        {  
            e.printStackTrace();  
        }  
    }
///////////////////////////////////////////////maze()
	
	public static void maze( int a, int b )
	{
		int array1[] = new int[200];//store array[x][]
		int array2[] = new int[200];//store array[][x]
		
		int a1=0,finish=0,count=0;
		
		while( finish==0 )
		{
			////////////////////////////[0][0]
			if( a==0 && b==0 )
			{
				array[0][0]=2;
				for(int i=0; i<2; i++)
					for(int j=0; j<2; j++)
						if(array[a+i][b+j]==0)
						{
							a=a+i;
							b=b+j;
							array[a][b]=2;
							array1[a1]=a;//push[x][] in array1 
							array2[a1]=b;//push[][x] in array2
							a1++;
						}
			}
			////////////////////////////[1][1]~[13][13]
			else if( a>0 && a<14 && b>0 && b<14 )
			{
				count=0;
				for(int i=0; i<3; i++)
					for(int j=0; j<3; j++)
					{
						if( array[a-1+i][b-1+j] == 0 )//find the way that can go
						{
							a=a-1+i;
							b=b-1+j;
							
							array[a][b]=2;
							
							array1[a1]=a;//push[x][] in array1 
							array2[a1]=b;//push[][x] in array2
							a1++;
						}
						else if( array[a-1+i][b-1+j] != 0 )
							count++;
						if( count==9 )//there isn't any road can go
						{
							array[array1[a1]][array2[a1]]=4;
							a1--;
							a=array1[a1];//pop[x][] of array1 
							b=array2[a1];//pop[][x] of array2
						}
					}//end for
			}
			////////////////////////////[0][14]
			else if( a==0 && b==14)
			{
				count=0;
				for(int i=0; i<2; i++)
					for(int j=0; j<2; j++)
						if(array[a-i][b+j]==0)
						{
							a=a-i;
							b=b+j;
							array[a][b]=2;
							array1[a1]=a;//push[x][] in array1 
							array2[a1]=b;//push[][x] in array2
							a1++;
							count=1;
						}
				if(count==0)
				{
					array[array1[a1]][array2[a1]]=4;
					a1--;
					a=array1[a1];//pop[x][] of array1 
					b=array2[a1];//pop[][x] of array2
				}
			}
			////////////////////////////[14][0]
			else if( a==14 && b==0 )
			{
				count=0;
				for(int i=0; i<2; i++)
					for(int j=0; j<2; j++)
						if(array[a+i][b-j]==0)
						{
							a=a+i;
							b=b-j;
							array[a][b]=2;
							array1[a1]=a;//push[x][] in array1 
							array2[a1]=b;//push[][x] in array2
							a1++;
							count=1;
						}
				if(count==0)
				{
					array[array1[a1]][array2[a1]]=4;
					a1--;
					a=array1[a1];//pop[x][] of array1 
					b=array2[a1];//pop[][x] of array2
				}
				
			}
			////////////////////////////[14][14]
			else if( a==14 && b==14)
			{
				array[14][14]=2;
				finish=1;//end while ---> reach the terminal point
			}
			////////////////////////////[0][1]~[0][13]
			else if( a==0 && b>0 && b<14 )
			{
				if(array[a][b+1]==0)
					b=b+1;
				else if(array[a+1][b]==0)
					a=a+1;
				else if(array[a][b-1]==0)
					b=b-1;
				else if(array[a+1][b+1]==0)
				{
					a=a+1;
					b=b+1;
				}
				else if(array[a+1][b-1]==0)
				{
					a=a+1;
					b=b-1;
				}
				else
				{
					array[array1[a1]][array2[a1]]=4;
					a1--;
					a=array1[a1];//pop[x][] of array1 
					b=array2[a1];//pop[][x] of array2
					break;
				}
				array[a][b]=2;
				array1[a1]=a;//push[x][] in array1 
				array2[a1]=b;//push[][x] in array2
				a1++;
			}
			////////////////////////////[1][0]~[13][0]
			else if( b==0 && a>0 && a<14 )
			{
				if(array[a][b+1]==0)
					b=b+1;
				else if(array[a+1][b]==0)
					a=a+1;
				else if(array[a-1][b]==0)
					a=a-1;
				else if(array[a+1][b+1]==0)
				{
					a=a+1;
					b=b+1;
				}
				else if(array[a-1][b+1]==0)
				{
					a=a-1;
					b=b+1;
				}
				else
				{
					array[array1[a1]][array2[a1]]=4;
					a1--;
					a=array1[a1];//pop[x][] of array1 
					b=array2[a1];//pop[][x] of array2
					break;
				}
				array[a][b]=2;
				array1[a1]=a;//push[x][] in array1 
				array2[a1]=b;//push[][x] in array2
				a1++;
			}
			////////////////////////////[14][1]~[14][13]
			else if( a==14 && b>0 && b<14 )
			{
				if(array[a][b-1]==0)
					b=b-1;
				else if(array[a][b+1]==0)
					b=b+1;
				else if(array[a-1][b]==0)
					a=a-1;
				else if(array[a-1][b-1]==0)
				{
					a=a-1;
					b=b-1;
				}
				else if(array[a-1][b+1]==0)
				{
					a=a-1;
					b=b+1;
				}		
				else
				{
					array[array1[a1]][array2[a1]]=4;
					a1--;
					a=array1[a1];//pop[x][] of array1 
					b=array2[a1];//pop[][x] of array2
					break;
				}
				array[a][b]=2;
				array1[a1]=a;//push[x][] in array1 
				array2[a1]=b;//push[][x] in array2
				a1++;
			}
			
			////////////////////////////[1][14]~[13][14]
			else if( b==14 && a>0 && a<14 )
			{
				if(array[a][b-1]==0)
					b=b-1;
				else if(array[a+1][b]==0)
					a=a+1;
				else if(array[a][b-1]==0)
					b=b-1;
				else if(array[a-1][b-1]==0)
				{
					a=a-1;
					b=b-1;
				}
				else if(array[a+1][b-1]==0)
				{
					a=a+1;
					b=b-1;
				}		
				else
				{
					array[array1[a1]][array2[a1]]=4;
					a1--;
					a=array1[a1];//pop[x][] of array1 
					b=array2[a1];//pop[][x] of array2
					break;
				}
				array[a][b]=2;
				array1[a1]=a;//push[x][] in array1 
				array2[a1]=b;//push[][x] in array2
				a1++;
			}
			
		}
	}
///////////////////////////////////////////////writefile()
	public static void writefile()throws Exception
	{  
		java.io.File write_file = new java.io.File( "s101502504.txt" ); 
		if(write_file.exists())//determine if file exists
		{
			System.out.println("File already exits.");
			System.exit(0);
		}
		java.io.PrintWriter output = new java.io.PrintWriter(write_file);
		for(int i=0; i<15; i++)
		{
			for(int j=0; j<15; j++)
				output.print(array[i][j]);//write file in s101502504.txt
			output.println();//fifteen numbers to the next line
		}
		output.close();
	}
///////////////////////////////////////////////main
	public static void main(String[] args) throws Exception
	{
		System.out.print("請輸入一個 15*15 的迷宮: (能走為 0 ,牆壁為 1)\n" +
				"( 規定:左上角[0][0]為入口, 右下角[14][14]為出口, 其餘邊緣皆是牆壁 )\n"
				+"＊注意:測出路線之後,已走過路線會被記為2,若是死路則記為4\n" +
				"請輸入要讀取的檔案路徑 (例: c:\\maze.txt): ");
		
		readfile();//read file from maze.txt

		System.out.println("\n您的迷宮為:");
		for(int i=0; i<15; i++)
		{
			for(int j=0; j<15; j++)
				System.out.print(array[i][j]);//print the initial maze
			System.out.println();//fifteen numbers to the next line
		}
		System.out.println("\n---路線已畫出: s101502504.txt---");
		
		maze(0,0);//change the maze
		array[0][0]=2;
		writefile();//write file to s101502504.txt
	}
}