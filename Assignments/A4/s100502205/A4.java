package ce1002.A4.s100502205;

import java.util.Scanner;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.PrintStream;
import java.io.File;

public class A4 {
	private static final int HEIGHT = 15;
	private static final int WIDTH = 15;

	static boolean dfs(int x, int y, char[][] map) {
		map[x][y] = '2';
		if (x == HEIGHT - 1 && y == WIDTH - 1) // end_x end_y;
			return true;
		boolean argv = false;
		for (int i = 1; i >= -1; i--) {
			for (int j = 1; j >= -1; j--) {
				int tx = x + i, ty = y + j;
				if (tx < 0 || ty < 0 || tx >= HEIGHT || ty >= WIDTH)
					continue;
				if (map[tx][ty] == '0') {
					argv = dfs(tx, ty, map);
					if (argv)
						return true;
				}
			}
		}
		map[x][y] = '4';
		return false;
	}

	static void bfs(char[][] map) {
		int[] Qx = new int[HEIGHT * WIDTH + 5], Qy = new int[HEIGHT * WIDTH + 5];
		int[][] stepRecord = new int[HEIGHT][WIDTH]; // initialize all zero
		int Qt = 0;
		Qx[Qt] = 0;// start_x
		Qy[Qt] = 0;// start_y
		stepRecord[0][0] = 1;
		for (int i = 0; i <= Qt; i++) {
			int x = Qx[i], y = Qy[i];
			for (int j = -1; j <= 1; j++) {
				for (int k = -1; k <= 1; k++) {
					int tx = x + j, ty = y + k;
					if (tx < 0 || ty < 0 || tx >= HEIGHT || ty >= WIDTH)
						continue;
					if (map[tx][ty] == '1' || stepRecord[tx][ty] != 0)
						continue;
					stepRecord[tx][ty] = stepRecord[x][y] + 1;
					Qx[++Qt] = tx;
					Qy[Qt] = ty;
				}
			}
			if (stepRecord[HEIGHT - 1][WIDTH - 1] != 0)
				break;
		}
		int px = HEIGHT - 1, py = WIDTH - 1;//end_x, end_y
		map[px][py] = '2';//backtracking
		while (px != 0 || py != 0) {
			loop:
			for (int i = -1; i <= 1; i++) {
				for (int j = -1; j <= 1; j++) {
					int tx = px + i, ty = py + j;
					if (tx < 0 || ty < 0 || tx >= HEIGHT || ty >= WIDTH)
						continue;
					if (stepRecord[tx][ty] == stepRecord[px][py] - 1) {
						px = tx;
						py = ty;
						break loop;
					}
				}
			}
			map[px][py] = '2';
		}
	}

	public static void main(String[] args) {
		Scanner cin = new Scanner(System.in);
		Scanner fin = null;
		PrintStream fout = null;
		File writeFile = new File("s100502205.txt");
		File readFile = null;
		char[][] map = new char[HEIGHT][];
		System.out.println("請輸入一個 15*15 的迷宮: (能走為 0 ,牆壁為 1)");
		System.out.println("( 規定:左上角[0][0]為入口, 右下角[14][14]為出口, 其餘邊緣皆是牆壁 )");
		System.out.println("＊注意:測出路線之後,已走過路線會被記為2,若是死路則記為4");
		System.out.print("請輸入要讀取的檔案路徑 (例: c:\\maze.txt): ");
		try {
			readFile = new File(cin.next());
			fin = new Scanner(new FileInputStream(readFile));
			fout = new PrintStream(new FileOutputStream(writeFile));
			System.out.println("\n您的迷宮為:");
			int row = 0;
			while (fin.hasNext()) {
				map[row] = fin.next().toCharArray();
				System.out.println(map[row]);
				if (map[row].length != WIDTH)
					throw new Exception("Number of column Error!");
				row++;
			}
			if (row != HEIGHT)
				throw new Exception("Number of row Error!");
		} catch (Exception e) {
			System.out.println(e.getMessage());
			System.exit(0);
		}
		//bfs(map); // BFS method process
		dfs(0, 0, map); //(start_x, start_y, map_info);
		for (int i = 0; i < HEIGHT; i++)
			fout.println(map[i]);
		System.out.println("\n---路線已畫出: s100502205.txt---");
		fin.close();
		fout.close();
	}
}
