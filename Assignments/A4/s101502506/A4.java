package ce1002.A4.s101502506;
import java.io.*;
import java.util.*;

public class A4 {
private static final String Convert = null;
private static Object[][] a1;
static Stack si=new Stack();//建立一個空的stack放走過的i
static Stack sj=new Stack();//建立一個空的stack放走過的j
static int h;
static int f;

public static void main(String args[])
{
	System.out.println("請輸入一個 15*15 的迷宮: (能走為 0 ,牆壁為 1)");
	System.out.println("( 規定:左上角[0][0]為入口, 右下角[14][14]為出口, 其餘邊緣皆是牆壁 ) ");
	System.out.println("＊注意:測出路線之後,已走過路線會被記為2,若是死路則記為4");
	System.out.print("請輸入要讀取的檔案路徑 (例: c:\\maze.txt): ");
	Scanner scanner = new Scanner( System.in );
    String k=scanner.next();
    System.out.println();
    System.out.println("您的迷宮為:");
    
    
	try
	{
        FileReader fr =new FileReader(k);//建立文字檔讀取物件
        BufferedReader br =new BufferedReader(fr);
		String data;
		String a1[][];
		Vector<String []> str_vec = new Vector<String []>(); //暫存
		while ((data = br.readLine()) != null) //每次讀一行
		{                                      
			str_vec.add(data.split("\\s")); //將此行以空白(white space)切成字串陣列, 存入 Vector暫存 
		}
		br.close(); //關檔
		a1= new String [15][]; //產生二維陣列, 依據暫存的size(txt檔的行數=15)
		for (int i = 0; i < 15; i++)
			a1[i] = str_vec.elementAt(i); //將暫存區每一個元素(一行裡的字串陣列)放入二維陣列
		//印出二維陣列內容
		for (int i = 0; i < a1.length; i++) 
		{
			for (int j = 0; j < a1[i].length; j++) 
			{
				System.out.print(a1[i][j] + " ");
			}
			System.out.println();
		}
	}	
		catch(IOException e)
		{
		}
	
	System.out.println();
	int maze[][]=new int[15][15];
	for(int m=0;m<15;m++)
	{
		for(int n=0;n<15;n++)
		{
			maze[m][n] = Integer.parseInt((String) a1[m][n]);
		}
	}
	
	VISIT(maze,0,0);
}

public static void VISIT(int maze1[][],int i,int j)
{
	si.push(i);
	sj.push(j);
	maze1[i][j] = 2;  //把走過的路以2表示
	
	if(i == 14 && j == 14)
		point(maze1); //走到終點就輸出
	
	else if(maze1[i-1][j+1] == 0)
		VISIT(maze1,i-1,j+1); //如果是0就往右上走
	else if(maze1[i][j+1] == 0)
		VISIT(maze1,i,j+1); //如果是0就往右走
	else if(maze1[i+1][j+1] == 0)
		VISIT(maze1,i+1,j+1); //如果是0就往右下走
	else if(maze1[i+1][j] == 0)
		VISIT(maze1,i+1,j); //如果是0就往下走
	else if(maze1[i+1][j-1] == 0)
		VISIT(maze1,i+1,j-1); //如果是0就往左下走
	else if(maze1[i][j-1] == 0)
		VISIT(maze1,i,j-1); //如果是0就往左走
	else if(maze1[i-1][j-1] == 0)
		VISIT(maze1,i-1,j-1); //如果是0就往左上走
	else if(maze1[i-1][j] == 0)
		VISIT(maze1,i-1,j); //如果是0就往上走
	else //走到死胡同
	{
		i=(int) si.pop();
		j=(int) sj.pop();
		maze1[i][j] = 4;
		h=(int) si.pop();
		f=(int) sj.pop();
		VISIT(maze1,h,f);
	}
	maze1[i][j] = 0;//?
}

public static void point(int maze2[][])
{
	for(int k=0;k<15;k++)
	{
		for(int l=0;l<15;l++)
		{
			if(maze2[k][l]==4)
				maze2[k][l]=4; //如果是1就輸出4當死路
			else if(maze2[k][l]==2) 
				maze2[k][l]=2; //如果是 2就輸出 2當移動路徑
			else
				maze2[k][l]=1;
		}
		 System.out.println();
	}
	 System.out.println();
	 
	 try
	 {
		 File f=new File("C:\\Users\\Wayne\\workspace\\ce1002-A\\s101502506.txt");
		 if( f.exists() != true)
			 f.createNewFile();
		 FileWriter fw=new FileWriter("s101502506.txt");
		 BufferedWriter bw=new BufferedWriter( fw);
		 for(int s=0;s<15;s++)
		 {
			 for(int c=0;c<15;c++)
			 {
				 bw.write(maze2[s][c]+" ");
			 }
			 System.out.println();
		 }
		 try
		 {
			 bw.close();
		 }
		 catch(IOException e){} 
	 }
	 catch(IOException e){}
	 System.out.println("---路線已畫出: s101522071.txt---");
}
}
