package ce1002.A4.s101502510;

import java.util.*;
import java.io.*;

public class A4 {
	private int startx, starty, endx, endy;
	private boolean outmaze = false;

	public static void main(String args[]) throws Exception {

		System.out.println("請輸入一個 15*15 的迷宮: (能走為 0 ,牆壁為 1)");
		System.out.println("( 規定:左上角[0][0]為入口, 右下角[14][14]為出口, 其餘邊緣皆是牆壁 )");
		System.out.println("＊注意:測出路線之後,已走過路線會被記為2,若是死路則記為4");
		System.out.print("請輸入要讀取的檔案路徑 (例: c:\\maze.txt): ");

		Scanner input = new Scanner(System.in);
		File file = new File(input.next());
		Scanner in = new Scanner(file);
		System.out.println("\n您的迷宮為: ");

		int k = 0;
		int[][] array = new int[15][15];

		while (in.hasNext()) {// 將txt檔資料傳入陣列
			String[] number = in.next().split("");
			for (int i = 1; i <= 15; i++) {
				array[k][i - 1] = Integer.parseInt(number[i]);
			}
			k++;
		}
		in.close();

		for (int i = 0; i < 15; i++) {// 將迷宮輸出
			for (int j = 0; j < 15; j++) {
				System.out.print(array[i][j]);
			}
			System.out.println();
		}

		A4 play = new A4();
		play.Start(0, 0);// 設置起點
		play.End(14, 14);// 設置終點
		play.Buffer(array);// 將迷宮,起點,終點,傳入Go函式

		File write_file = new File("s101502510.txt");
		PrintWriter output = new PrintWriter(write_file);
		for (int i = 0; i < 15; i++) {// 將路徑輸出至txt檔
			for (int j = 0; j < 15; j++) {
				output.print(array[i][j]);
			}
			output.println();
		}
		output.close();

		System.out.println("\n---路線已畫出: s101502510.txt---");
	}

	public void Start(int x, int y) {// 設置起點
		this.startx = x;
		this.starty = y;
	}

	public void End(int x, int y) {// 設置終點
		this.endx = x;
		this.endy = y;
	}

	public void Buffer(int[][] r) {// 將迷宮,起點,終點,傳入Go函式
		Go(r, startx, starty);
	}

	public void Go(int[][] r, int x, int y) {
		r[x][y] = 2;// 走過的路記錄為2
		if (x == endx && y == endy)// 檢測走到終點沒
			outmaze = true;
		if (!outmaze && r[x][y + 1] == 0)// 檢測右邊是否有路
			Go(r, x, y + 1);

		if (!outmaze && r[x + 1][y + 1] == 0)// 檢測右下角是否有路
			Go(r, x + 1, y + 1);

		if (!outmaze && r[x + 1][y] == 0)// 檢測下方是否有路
			Go(r, x + 1, y);

		if (!outmaze && r[x + 1][y - 1] == 0)// 檢測左下角是否有路
			Go(r, x + 1, y - 1);

		if (!outmaze && r[x][y - 1] == 0)// 檢測左邊是否有路
			Go(r, x, y - 1);

		if (!outmaze && r[x - 1][y - 1] == 0)// 檢測左上角是否有路
			Go(r, x - 1, y - 1);

		if (!outmaze && r[x - 1][y] == 0)// 檢測上方是否有路
			Go(r, x - 1, y);

		if (!outmaze && r[x - 1][y + 1] == 0)// 檢測右上角是否有路
			Go(r, x - 1, y + 1);

		if (!outmaze)// 若八方都沒路則記錄死路
			r[x][y] = 4;
	}
}
