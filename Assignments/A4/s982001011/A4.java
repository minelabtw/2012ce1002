package ce1002.A4.s982001011;

import java.util.*;
import java.io.*;

class Position{
	int x;
	int y;
	char isWall;
	public Position(int a,int b,char w){
		x=a;
		y=b;
		isWall=w;
	}
	
public static class A4 {
	public static void main(String[] args)throws Exception{
		
		//imput file name here
		System.out.println("�п�J�g�c�ɮ׸��|");   //C:\\Users\\user\\workspace\\CE1002-A4\\maze.txt
		Scanner input = new Scanner(System.in);
		
		FileReader fin=new FileReader( input.next() );
		//output file name here
		FileWriter fout=new FileWriter( "s982001011.txt" );
		
		//read the maze file
		Vector< Vector< Character > > char2D=new Vector< Vector< Character > >();
		Vector< Character > temp=new Vector< Character >();
		char[]charTemp=new char[1];
		while(fin.ready()){
			fin.read(charTemp);
			if( charTemp[0]=='\n' ){
				char2D.add(temp);
				temp=new Vector< Character >();
			}else{
				temp.add( new Character( charTemp[0] ) );
			}
		}
		if(!temp.isEmpty())char2D.add(temp);
		
		System.out.println("============================");
		Stack<Position> path=new Stack<Position>();
		
		//--------------------------------------------------------------
		//copying the map
		Vector< Vector< Character > > openRoad = new Vector< Vector< Character > >();
		
		for(Vector< Character > i:char2D){
			temp=new Vector< Character >();
			for(Character j:i){
				temp.add( new Character( j.charValue() ) );
			}
			openRoad.add(temp);
		}
		//--------------------------------------------------------------
		path.push( new Position( 0 , 0 , char2D.get(0).get(2) ) );
		openRoad.get(0).set(0, new Character('1'));
		
		while( path.peek().x != (char2D.get(char2D.size()-1).size()-1) || path.peek().y != (char2D.size()-1) ){
			int x = path.peek().x;
			int y = path.peek().y;
			for(Vector< Character > i:char2D){
				for(Character j:i){
					fout.write(j.charValue());
				}
				fout.write('\n');
			}
	//		fout.write('\n');
			
			if( y != (char2D.size()-1) && openRoad.get(y+1).get(x).charValue() == '0' ){
				//down
		//		System.out.println("a");
				path.push( new Position( x , y+1 , '0' ) );
				openRoad.get( y+1 ).set( x, new Character( '1' ) );
			}else if(  y != (char2D.size()-1) && x != (char2D.get(y).size()-1) && openRoad.get(y+1).get(x+1).charValue() == '0' ){
				//down right	
				path.push( new Position( x+1 , y+1 , '0' ) );
				openRoad.get( y+1 ).set( x+1 , new Character( '1' ) );
			}else if( x != (char2D.get(y).size()-1) && openRoad.get(y).get(x+1).charValue() == '0' ){
				//right
		//		System.out.println("b");
				path.push( new Position( x+1 , y , '0' ) );
				openRoad.get( y ).set( x+1 , new Character( '1' ) );
			}else if(  y != 0 && x != (char2D.get(y).size()-1) && openRoad.get(y-1).get(x+1).charValue() == '0' ){
				//up right	
				path.push( new Position( x+1 , y-1 , '0' ) );
				openRoad.get( y-1 ).set( x+1 , new Character( '1' ) );
			}else if( y != 0 && openRoad.get(y-1).get(x).charValue() == '0' ){
				//up
		//		System.out.println("c");
				path.push( new Position( x , y-1 , '0' ) );
				openRoad.get( y-1 ).set( x , new Character( '1' ) );
			}else if(  y != 0 && x != 0 && openRoad.get(y-1).get(x-1).charValue() == '0' ){
				//up left	
				path.push( new Position( x-1 , y-1 , '0' ) );
				openRoad.get( y-1 ).set( x-1 , new Character( '1' ) );
			}else if( x != 0 && openRoad.get(y).get(x-1).charValue() == '0' ){
				//left
		//		System.out.println("d");
				path.push( new Position( x-1 , y , '0' ) );
				openRoad.get( y ).set( x-1 , new Character( '1' ) );
			}else if( y != (char2D.size()-1) && x != 0 && openRoad.get(y+1).get(x-1).charValue() == '0' ){
				//down left	
				path.push( new Position( x-1 , y+1 , '0' ) );
				openRoad.get( y+1 ).set( x-1 , new Character( '1' ) );
			}else{
				//die corner
		//		System.out.println("e");
				char2D.get( y ).set( x , new Character( '4' ) );
				path.pop();
			}
		//	System.out.printf("%d %d\r\n",path.peek().x,path.peek().y);
		}
		
	/*	for( Vector< Character > i : char2D ){
			for( Character j : i ){
				System.out.printf( " %c" , j.charValue() );
			}
			System.out.print("\r\n");
		}*/
		
		//output
		while( !path.empty() ){
			int x = path.peek().x;
			int y = path.peek().y;
			char2D.get( y ).set( x , new Character( '2' ) );
			path.pop();
		}
		
		

		
		System.out.print("\r\n\r\n");
		for( Vector< Character > i : char2D ){
			for( Character j : i ){
				System.out.printf(" %c" , j.charValue() );
			}
			System.out.print("\r\n");
		}
		
		fin.close();
		fout.close();
	}
}
}
