package ce1002.A4.s101502010;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Scanner;
import java.util.Vector;

public class A4 {
	public static void drawmap(int map[][] ,int i,int j) throws IOException{
		
		map[i][j]=2;
		if(j==14&&i==14){
			FileWriter filewriter = new FileWriter( "s101502010.txt" );//寫檔
			for(int k=0;k<15;k++,filewriter.write("\r\n")){
				for(int l=0;l<15;l++)
					filewriter.write(Integer.toString(map[k][l]));
			}
			filewriter.close();
			System.out.println("\n---路線已畫出: s101502010.txt---");
		}
		else{
			if(i+1<=14&&map[i+1][j]==0)
				drawmap(map,i+1,j);
			if(i+1<=14&&j+1<=14&&map[i+1][j+1]==0)
				drawmap(map,i+1,j+1);
			if(j+1<=14&&map[i][j+1]==0)
				drawmap(map,i,j+1);
			if(i-1>=0&&j+1<=14&&map[i-1][j+1]==0)
				drawmap(map,i-1,j+1);
			if(i-1>=0&&j-1>=0&&map[i-1][j-1]==0)
				drawmap(map,i-1,j-1);
			if(i+1<=14&&j-1>=0&&map[i+1][j-1]==0)
				drawmap(map,i+1,j-1);
			if(j-1>=0&&map[i][j-1]==0)
				drawmap(map,i,j-1);
			if(i-1>=0&&map[i-1][j]==0)
				drawmap(map,i-1,j);
			map[i][j]=4;
		}
		
		
	}
	public static void main(String[] args) throws IOException
	{
		System.out.println("請輸入一個 15*15 的迷宮: (能走為 0 ,牆壁為 1)");
		System.out.println("( 規定:左上角[0][0]為入口, 右下角[14][14]為出口, 其餘邊緣皆是牆壁 )");
		System.out.println("＊注意:測出路線之後,已走過路線會被記為2,若是死路則記為4");
		System.out.print("請輸入要讀取的檔案路徑 (例: c:\\maze.txt): ");
		Scanner input = new Scanner(System.in);
		String path = input.nextLine();
		System.out.println("您的迷宮為:");
		BufferedReader in = new BufferedReader(new FileReader(path));
		String tmp;
		int G[][] = new int[15][15];
		for(int i=0;i<15;i++)
		{
			tmp = in.readLine();
			for(int j=0;j<15;j++) //讀檔並轉為int陣列
				if(tmp.substring(j,j+1).equals("0"))
					G[i][j] = 0;
				else
					G[i][j] = 1;
			System.out.println(tmp);
		}
		drawmap(G,0,0);
		
	}
}
