package ce1002.A6.s101502514;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JFrame;
public class A6 extends JFrame implements ActionListener{
	
	static double x1=Math.cos(Math.random()*100);
	static double y1=Math.sin(Math.random()*100);
	int num=0;
	static JButton btn = new JButton();//new button
	static double x=150,y=150;
	public static void main(String[] args) {
		A6 hw = new A6("A6");
		hw.setSize(400, 400);
		hw.setLocation(10, 10);
		hw.setVisible(true);
		int insr =hw.getInsets().right;//邊框大小
		int insb =hw.getInsets().bottom;
		int inst =hw.getInsets().top;
		int insl =hw.getInsets().left;
		while(true){
			x=x+x1;//開始跑
			y=y+y1;
			btn.setBounds((int)x, (int)y, 50, 50);//跑
			try { 
                Thread.sleep(7); 
            } 
            catch(InterruptedException e) { 
            }
			
			if((int)x+50+insl+insr==hw.getWidth()){//撞到邊邊
				x1=-x1;//彈
			}
			
			if((int)y+50+inst+insb==hw.getHeight()){
				y1=-y1;
			}
			
			if((int)x<=0){
				x1=-x1;
			}
			
			if((int)y<=0){
				y1=-y1;
			}
		}
	}
	
	public A6(String pTitle)   {
		super(pTitle);
		setLayout(null);
		btn.setBounds(0, 0, 50, 50);//初始位置
		btn.addActionListener(this);//按button 執行動作
		add(btn);
		btn.setLabel(String.valueOf(num));		
	}
	
	public void actionPerformed(ActionEvent arg0) {
		num++;
		btn.setLabel(String.valueOf(num));	
		x1=Math.cos(Math.random()*100);//改變方向
		y1=Math.sin(Math.random()*100);
	}
}