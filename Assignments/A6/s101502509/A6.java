package ce1002.A6.s101502509;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.*;

public class A6 extends JFrame{

	Integer count = 0;
	double directorX = 0;
	double directorY = 0;
	int speed;
	JButton moveButton = new JButton();
	
	public A6() throws InterruptedException {
		setDirector();
		setUI();
		while(true) {
			Thread.sleep(10);
			move();
		}
	}
	
	public void setUI() {
		this.getContentPane().setLayout(null);	//clear the Layout of the JFrame
		moveButton.setSize(50, 50);
		moveButton.setText(count.toString());
		moveButton.addActionListener(new clickListener());
		add(moveButton);
		
		setTitle("A6");
		setSize(400, 300);
		setLocationRelativeTo(null);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setVisible(true);
	}
	
	private void setDirector() {	//initialize the director
		randomDirector();
		speed = 3; 
	}
	
	private void randomDirector() {
		double angle = Math.random() * Math.toRadians(360);	//Random the angle to the director
		directorX = Math.cos(angle);	//Random the directorX 
		directorY = Math.sin(angle);	//Random the directorY
	}
	private void move() {
		int nowX = moveButton.getLocation().x;	//get the X coordinate of the button
		int nowY = moveButton.getLocation().y;	//get the Y coordinate of the button
		int boundX = getWidth() - 60;	//get the X bound of the frame
		int boundY = getHeight() - 80;	//get the Y bound of the frame
		if((nowX >= boundX && directorX > 0)	//if the position of the button is over the right and goes right,
		   || (nowX < 0 && directorX < 0)) {	//if the position of the button is over the left and goes left,
			directorX *= -1;				//reverse the directorX
			count++;
		} else if((nowY >= boundY && directorY > 0)		//if the position of the button is over the top and goes up,
				  || (nowY < 0 && directorY < 0)) {		//if the position of the button is over the bottom and goes down
			directorY *= -1;						//reverse the directorY
			count++;
		}
		moveButton.setText(count.toString());	//change the text of the button
		
		//change the new position of the button
		moveButton.setLocation((nowX + (int)(directorX * speed)), (nowY + (int)(directorY * speed))); 
	}
	
	public class clickListener implements ActionListener {
		@Override
		public void actionPerformed(ActionEvent arg0) {
			double oldDirectorX = directorX;
			double oldDirectorY = directorY;
			randomDirector();
			
			if(directorX == 0 && directorY == 0) {	//if the directorX and directorY are 0, random Director again
				randomDirector();
				return;
			} else if(oldDirectorX == directorX && oldDirectorY == directorY) { //if new director x,y are the same as old director x,y , random Director again
				randomDirector();
				return;
			}
		}
	}
	
	public static void main(String[] args) throws InterruptedException {
		// TODO Auto-generated method stub
		new A6();
	}
}
