package ce1002.A6.s101502510;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.*;

public class A6 extends JFrame implements ActionListener {
	static String num = "0";// 計數
	static int i = Integer.parseInt(num);
	static int dx = 1;// 設定x座標移動速度及方向
	static int dy = 1;// 設定y座標移動速度及方向
	static int mx;
	static int my;
	static JButton move = new JButton();

	public A6() {
		setLayout(null);
		move.setBounds(100, 100, 50, 50);
		move.setText(num);
		add(move);
		move.addActionListener(this);
	}

	public void actionPerformed(ActionEvent e) {// 按下按鈕隨機改變移動方向
		double r = Math.random() * (Math.PI * 2);
		dx = (int) (4 * Math.cos(r));
		dy = (int) (4 * Math.sin(r));
	}

	public static void main(String[] args) {
		A6 frame = new A6();
		frame.setTitle("A6");
		frame.setSize(500, 400);
		frame.setLocation(400, 250);
		frame.setVisible(true);
		try {
			while (true) {
				// 改變按鈕位置//
				mx = move.getX() + dx;
				my = move.getY() + dy;
				move.setLocation(mx, my);
				// /////////////

				Thread.sleep(10);// 延遲

				// 碰到邊界改變方向//
				if (my >= 310 || my <= 0) {
					dy = -dy;
					i++;
					num = "" + i;
					move.setText(num);
				}
				if (mx >= 440 || mx <= 0) {
					dx = -dx;
					i++;
					num = "" + i;
					move.setText(num);
				}
				// /////////////////
			}
		} catch (InterruptedException e1) {
		}
	}
}
