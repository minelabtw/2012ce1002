package ce1002.A6.s101502522;
import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Random;
public class A6 extends JFrame implements ActionListener
{
	private int i=0;
    private JButton button =new JButton(String.valueOf(i));
	private double xspeed=3;
	private double yspeed=3;
	private double x=(int)(Math.random()*800%600);//隨機位置
	private double y=(int)(Math.random()*800%600);
	public A6() throws InterruptedException
	{
		setLayout(null);
		button.setBounds((int)x,(int)y,50,50);
		add(button);
		button.addActionListener(this);
	}
	public void actionPerformed(ActionEvent e)//點選按鈕即隨機改變移動方向
	{
		if(e.getSource()==button)
		{
			Random r = new Random();
			double direction =r.nextDouble()*Math.toRadians(360);
			xspeed=Math.cos(direction)*4;
			yspeed=Math.sin(direction)*4;
		}
	}
	public void move() throws InterruptedException //移動函式，碰壁反彈
	{
		while(true)
		{
			button.setLocation((int)x,(int)y);//移動
			x=x+xspeed;
			y=y+yspeed;
			
			if(x<=0||x>=getWidth()-button.getWidth()-20)//反彈
			{
				xspeed=-xspeed;
				i++;  //每點選一次增加1
				button.setText(String.valueOf(i));
			}
			if(y<=0||y>=getHeight()-button.getHeight()-40)
			{
				yspeed=-yspeed;
				i++;
				button.setText(String.valueOf(i));
			}
			Thread.sleep(30); //每一迴圈中止一小段時間
		}
	}
	public static void main(String[] args) throws InterruptedException  //主程式，興建視窗
	{
		A6 frame = new A6();
		frame.setSize(800,600);
		frame.setTitle("A6");
		frame.setLocationRelativeTo(null);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setVisible(true);
		frame.move();
	}
	

}
