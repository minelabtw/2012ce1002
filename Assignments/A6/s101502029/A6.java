package ce1002.A6.s101502029;

import java.awt.FlowLayout;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.*;

public class A6 extends JFrame implements ActionListener {
	private static JButton btn = new JButton();
	private static int Num = 0;
	private static int x = 0, y = 0, Xdiretion, Ydiretion;

	public static void main(String[] args) throws InterruptedException {
		A6 hw = new A6("A6");

		hw.setSize(600, 500);

		hw.setLocation(250, 100);

		hw.setVisible(true);
		//行進方向隨機移動
		Xdiretion = (int) (Math.random() * 3);
		Ydiretion = (int) (Math.random() * 3);
		if (Xdiretion == 0 && Ydiretion == 0) {
			Xdiretion = (int) (Math.random() * 3);
			Ydiretion = (int) (Math.random() * 3);
		}
		while (true) {
			Thread.sleep(8);
			// butten的位置
			x = btn.getLocation().x;
			y = btn.getLocation().y;
			if (x < 530 && y < 410) {// butten行進的方向
				x += Xdiretion;// x方向
				y += Ydiretion;// y方向
			}
			if (x < 0 || x >= 530) {// 若x方向撞到壁，x會往反方向移動
				Xdiretion *= -1;
				x += Xdiretion;
				Num++;
				btn.setText("" + Num);
			}
			if (y < 0 || y >= 410) {// 若y方向撞到壁，y會往反方向移動
				Ydiretion *= -1;
				y += Ydiretion;
				Num++;
				btn.setText("" + Num);
			}
			btn.setLocation(x, y);
		}
	}

	public A6(String pTitle) {

		super(pTitle);

		setLayout(null);

		btn.setBounds(x, y, 50, 50);
		btn.setText("" + Num);
		btn.addActionListener(this);
		add(btn);

	}

	public void actionPerformed(ActionEvent e) {
		// butten隨機改變方向
		Xdiretion = (int) (Math.random() * 3);
		Ydiretion = (int) (Math.random() * 3);
		if (Xdiretion == 0 && Ydiretion == 0) {
			Xdiretion = (int) (Math.random() * 3);
			Ydiretion = (int) (Math.random() * 3);
		}
	}

}
