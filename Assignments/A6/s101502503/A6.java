package ce1002.A6.s101502503;

import java.awt.event.*;
import java.awt.*;
import javax.swing.*;

public class A6 extends JFrame implements ActionListener{
	
	private static int x=12, y=21;//初始位置
	private static double dx=0, dy=0;//改變量
	private static int count=0;//計算撞到牆幾次
	private static JButton btn = new JButton();//set button
	
	public A6(String pTitle)
	{
		super(pTitle);
		
		setLayout(null);
		btn.setBounds(x,y,60,60);//btn位置及大小
		add(btn);//加按鈕
		
		btn.addActionListener(this);//按按鈕後執行下面那些actionPerformed
	}
	
	public static void main(String args[])
	{
		A6 frame = new A6("A6");
		frame.setSize(550, 550);//set size
		frame.setLocationRelativeTo(null);//視窗置中
		frame.setVisible(true);
		
		double r=Math.random()*3.14;
		dx=3*Math.cos(r);
		dy=3*Math.sin(r);
		
		while(true)
		{
			x+=dx;
			y+=dy;
			btn.setLocation((int)x,(int)y);
			
			if( btn.getLocation().x>=475 || btn.getLocation().x<=0 )//碰左右壁
			{
				dx*=-1;//改變方向
				count++;
			}
			
			if( btn.getLocation().y>=450 || btn.getLocation().y<=0 )//碰上下壁
			{
				dy*=-1;//改變方向
				count++;
			}
			
			btn.setText(Integer.toString(count));//show the button when number change
			frame.getContentPane().repaint();//repaint the pane
			
			try
			{
				Thread.sleep(15);
			}
			catch (InterruptedException e)
			{
				e.printStackTrace();
			}
		}
	}

	public void actionPerformed(ActionEvent e)//change the direction randomly
	{
		double r=Math.random()*3.14;
		dx=3*Math.cos(r);
		dy=3*Math.sin(r);
	}

}