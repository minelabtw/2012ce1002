package ce1002.A6.s101502556;
import java.awt.*;
import javax.swing.*;
import java.awt.event.*;
import java.util.Timer;
import java.util.TimerTask;
import java.lang.Math;
public class A6 extends JFrame {
	int a=1;
	private JButton jbtComputeLoan =new JButton(""+a);
	int x=(int)(Math.random()*300)+1;
	int y=(int)(Math.random()*300)+1;
	int random =(int)(Math.random()*360);
	int wx=(int)(Math.cos(random)*5)+1;
	int wy=(int)(Math.sin(random)*5)+1;
	int speed =10;
	public static void main(String[] args){
		A6 frame= new A6();
		frame.setTitle("A6");
		frame.setSize(1000,1000);
		frame.setLocationRelativeTo(null);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setVisible(true);	 	
	}
	public A6() {
		Timer timer = new Timer();
		JPanel p1=new JPanel();
		p1.setLayout(null);
		jbtComputeLoan.setSize(150,150);
		p1.add(jbtComputeLoan);
		add(p1);
		jbtComputeLoan.setLocation(x,y);//起始點
		timer.schedule(new DateTask(), 100 ,100);//計時器
		ButtonListener bl=new ButtonListener();
		jbtComputeLoan.addActionListener(bl);
	}
	public class DateTask extends TimerTask{
		public void run(){
			x+=speed*wx;//按鈕移動
			y+=speed*wy;
			jbtComputeLoan.setLocation(x,y);
			
			if(x>getWidth()-jbtComputeLoan.getWidth())//碰到牆壁反彈
			{
				wx*=-1;
				a++;
				jbtComputeLoan.setText(""+a);
			}
				
			if(x<=0)
			{
				wx*=-1;
				a++;
				jbtComputeLoan.setText(""+a);
			}
				
			if(y>getHeight()-jbtComputeLoan.getHeight())
			{
				wy*=-1;
				a++;
				jbtComputeLoan.setText(""+a);
			}
				
			if(y<=0)
			{
				wy*=-1;
				a++;
				jbtComputeLoan.setText(""+a);
			}		
		}	
	}
	private class ButtonListener implements ActionListener{//按按鈕改變方向
		public void actionPerformed(ActionEvent e){
			random =(int)(Math.random()*360);
			wx=(int)(Math.cos(random)*5)+1;
			wy=(int)(Math.sin(random)*5)+1;
			a++;
			jbtComputeLoan.setText(""+a);
		}
	}
}
