package ce1002.A6.s101502518;

import java.awt.Rectangle;
import java.awt.Shape;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.geom.Ellipse2D;
import java.io.File;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import java.io.IOException;


class frame extends JFrame implements Runnable{
	
	double x=250;
	double y=250;
	double x_size = 55;  
    double y_size = 55;
    double an = 7*(Math.random());
	double xMoveD = 5*(Math.cos(an));  
    double yMoveD = 5*(Math.sin(an));
    
    int count = 0;
    
	JButton but;
	
	frame(){
		super("A6");
		setLayout(null);
		this.setSize(500,500);
		this.setVisible(true);
		this.setDefaultCloseOperation(EXIT_ON_CLOSE);
		
		but = new JButton();
		but.setText(Integer.toString(count));
		but.setSize((int)x_size,(int)y_size);
		
		but.addActionListener(new ActionListener() {			
			@Override
			public void actionPerformed(ActionEvent e) {
				// TODO Auto-generated method stub
				//按鈕使隻任意變換方向
				double ann = 7*(Math.random());
				xMoveD = 5*(Math.cos(ann)); 
				yMoveD = 5*(Math.sin(ann));			
			}
		});
		
		this.add(but);
		new Thread(this).start();	
	}
	
		
	public void run() {
			
		// TODO Auto-generated method stub
		
		while(true)
		{
			x += xMoveD;
			y += yMoveD;
			int width = this.getContentPane().getWidth();
			int height = this.getContentPane().getHeight();
			//判斷邊界問題
			if(x<=0)	
			{
				 x = 0;  
		         xMoveD = -xMoveD;
		         count = count + 1;
		         but.setText(Integer.toString(count));
		         
			}
			else if ((x+x_size) >= width)
			{
				 x = width - x_size;  
		         xMoveD = -xMoveD;
		         count = count + 1;
		         but.setText(Integer.toString(count));
		         
			}

			if (y<=0) 
			{  
	            y = 0;  
	            yMoveD = -yMoveD;
	            count = count + 1;
	            but.setText(Integer.toString(count));
	           
	        } 
			else if ((y+y_size) >= height) 
	        {  
	            y = height - y_size;  
	            yMoveD = -yMoveD; 
	            count = count + 1;
	            but.setText(Integer.toString(count));
	        }
			
			but.setLocation((int)x,(int)y);
			
			try{
				Thread.sleep(50);  
			}	
	        catch (InterruptedException e) { 
	        	System.out.print("x");
	        }
		}
	}				
}

		


class A6{
	public static void main(String[] args) {  
		new frame();
	}
}

