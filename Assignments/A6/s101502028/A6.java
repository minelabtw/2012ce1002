package ce1002.A6.s101502028;

import java.awt.Color;
import java.awt.Font;
import java.awt.event.*;
import javax.swing.*;

public class A6 extends JFrame implements ActionListener {
	private static JButton btn = new JButton();
	private static Integer counter = 0;
	private static int x = 0; // position x
	private static int y = 0; // position y
	private static int dirX = 1; // direction x
	private static int dirY = 1; // direction y

	public static void main(String[] args) throws InterruptedException {
		A6 frame = new A6("A6"); // create a frame
		frame.setSize(800, 600);
		frame.setLocationRelativeTo(null);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setVisible(true);
		for (;;) { // for loop to make the button move
			Thread.sleep(5);
			x += dirX; // add the direction x to the position x
			y += dirY; // add the direction y to the position y
			if (x < 0 || x > frame.getWidth() - 110) { // rebound the button if touch the wall on x axis
				dirX *= -1;
				counter++;
			}

			if (y < 0 || y > frame.getHeight() - 130) { // rebound the button if touch the wall on y axis
				dirY *= -1;
				counter++;
			}

			btn.setText(counter.toString()); // set the counter to the button
			btn.setLocation(x, y); // set the new position after adding the
									// direction
		}
	}

	public A6(String pTitle) { // add the button on the frame
		super(pTitle);
		setLayout(null);
		Font font = new Font("TimesRoman", Font.BOLD, 50); // font of the letter in the button
		btn.setFont(font); // set font to make it look clear
		btn.setForeground(Color.BLACK);
		btn.setBackground(Color.WHITE);
		btn.setText(counter.toString()); // put the number inside the button
		btn.setBounds(x, y, 100, 100); // set the position and size of the button
		btn.addActionListener(this);
		add(btn);
	}

	public void actionPerformed(ActionEvent e) { // random direction when the button do the action
		double angle = Math.random() * (Math.toRadians(360));
		dirX = (int) (Math.cos(angle) * 2);
		dirY = (int) (Math.sin(angle) * 2);
	}
}
