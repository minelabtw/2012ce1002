package ce1002.A6.s101502507;

import java.awt.event.*;
import javax.swing.*;

public class A6 extends JFrame implements ActionListener 
{
	private static int x=160, y=160, m=0, delta=3; //x,y為按鈕起始位置,m為計算碰撞次數,delta為前進一次之距離
	private static double a=0, b=0, r, r1; //a為水平位移,b為鉛直位移,r為舊移動斜率,r1為新移動斜率
	private static JButton btn = new JButton(); //設定按鈕		
	
	public static void main(String args[])
	{
		A6 frame = new A6("A6"); //視窗標題
		frame.setSize(600, 600); //視窗大小
		frame.setLocationRelativeTo(null); //視窗置於螢幕中央
		frame.setVisible(true);	//視窗可見
		r = Math.random()*Math.PI; //行徑斜率隨機設定
		a = delta*Math.cos(r); //水平位移
		b = delta*Math.sin(r); //鉛直位移
			
		while(true)
		{
			x+=a; //水平座標
			y+=b; //鉛直座標
			btn.setLocation(x,y); //設定xy座標
			
			if( x<=0 || x>=511 ) //按鈕觸碰到視窗左右邊界
			{
				a=-a; //水平行進方向相反
				m++; //每碰撞一次 計數加一
			}			
			if( y<=0 || y>=484 ) //按鈕觸碰到視窗上下邊界
			{
				b=-b; //鉛直行進方向相反
				m++; //每碰撞一次 計數加一
			}				
			
			frame.getContentPane().repaint(); //重畫
			btn.setText(Integer.toString(m)); //將碰撞運算結果轉為字串並印在按鈕上
			
			try //延遲時間 才看得清楚行徑路徑
			{
				Thread.sleep(9); //延遲9毫秒
			}						
			catch(InterruptedException e){}					
		}		
	}
	
	public A6(String pTitle)
	{
		super(pTitle);
		setLayout(null); //不使用版面配置
		btn.setBounds(x,y,70,70); //按鈕位置大小
		add(btn); //裝上按鈕
		btn.addActionListener(this); //按下按鈕會執行其程式內之指令
	}
	
	public void actionPerformed(ActionEvent e) //按下按鈕時會隨機變換前進路徑
	{
		while(true)
		{
			r1 = Math.random()*Math.PI;		
			if(Math.abs(r1-r)>(Math.PI/4))	break; //當新方向與舊方向要差45度以上
		}		
		r = r1; //設定新的移動角度
		a = delta*Math.cos(r); //設定新的水平移動方向
		b = delta*Math.sin(r); //設定新的鉛直移動方向
	}
}