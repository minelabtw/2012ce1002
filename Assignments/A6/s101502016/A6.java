package ce1002.a6.s101502016;

import java.awt.event.ActionListener;
import javax.swing.JFrame;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Random;

import javax.swing.JButton;

import ce1002.a6.s101502016.A6;

public class A6 implements ActionListener {

	private static JButton turn;
	static Random A = new Random();// 隨機
	static int i = 1;
	static int j = 1;// 令一開始皆往右上移動
	static int count = 0;

	public A6() {
		turn = new JButton(" 0 ");

	}

	public static void main(String[] args) throws InterruptedException {
		JFrame frame = new JFrame("A6");
		frame.setLayout(null);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setSize(900, 600);

		A6 test = new A6();

		int x = A.nextInt(450) + 1;
		int y = A.nextInt(450) + 1;
		turn.setBounds(x, y, 50 , 50);// 設定按鈕初始的位置

		frame.add(turn);
		turn.addActionListener(test);

		frame.setLocationRelativeTo(null);// 將frame置中
		frame.setVisible(true);
		frame.setResizable(false);
		int width = frame.getWidth();
		int height = frame.getHeight();// 取得長寬之值
		while (true) {

			if (x <= 0 || x + 50 >= width)// 碰到左右兩邊
			{
				i *= -1;
				plus();
				if (y <= 0 || y + 50 >= height)// 碰到角
				{
					j *= -1;
					plus();
				}
			} else if (y <= 0 || y +50 >= 572) {
				j *= -1;
				plus();
				
			}//碰到上下兩邊
			 // JFRAME下面的邊界有問題

			x += i;
			y += j;

			turn.setBounds(x, y, 50, 50);
			Thread.sleep(5);
		}

	}

	public void actionPerformed(ActionEvent arg0) {
		// TODO Auto-generated method stub
		int R = A.nextInt(4);

		switch (R) {

		case 0:
			i = 1;
			j = 1;
			break;
		case 1:
			i = -1;
			j = 1;
			break;
		case 2:
			i = -1;
			j = -1;
			break;
		case 3:
			i = 1;
			j = -1;
			break;
		}
	}

	public static void plus() {
		count++;
		turn.setText(String.valueOf(count));
	}

}
