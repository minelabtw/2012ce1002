package ce1002.A6.s101502001;
import javax.swing.*;
import java.awt.*;
import java.awt.event.*;

public class A6 extends JFrame{
	private int a=0;
	private JButton jbt=new JButton();
	public A6(){
		JPanel panel=new JPanel();
		panel.add(jbt);
		
		add(panel);
	}
	public static void main(String[]args){
		A6 frame=new A6();
		frame.setTitle("A6");
		frame.setSize(400,400);
		frame.setLocationRelativeTo(null);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setVisible(true);
	}
	static class jbt extends JPanel{
		private String m="";
		private int xCoordinate=0;
		private int yCoordinate=20;
		
		public jbt(String m){
			this.m=m;
			
			Timer timer=new Timer(1000, new TimerListener());
			timer.start();
		}
		protected void paintComponent(Graphics g){
			super.paintComponent(g);
			if(xCoordinate>getWidth()){
				xCoordinate=-20;
			}
			xCoordinate+=5;
			g.drawString(m,xCoordinate, yCoordinate);
		}
		class TimerListener implements ActionListener{
			public void actionPerformed(ActionEvent e){
				repaint();
			}
		}
	}
	

}
