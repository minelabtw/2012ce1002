package ce1002.A6.s995002525;

import java.awt.event.*;
import java.awt.*;
import javax.swing.*;
import java.lang.Thread;

public class A6 extends JFrame implements ActionListener{
	private int numClicks = -1;
	private JButton btn = new JButton("0");
	private Run run=new Run();
	private Thread t=new Thread(run);
	int len = 50;
	int wei = 30;
	int x=1,y=2;
	public static void main(String[] args)throws Exception {
		A6 hw = new A6("A6");
		hw.setSize(500,500);
		hw.setLocation(50,50);
		hw.setVisible(true);
		
	}
	public A6(String pTitle) {
		super(pTitle);
		setLayout(null);// 不使用版面配置	
		btn.setBounds(1,1,len,wei);//x y length weight
		btn.addActionListener(this);		
		add(btn);
		t.start();
		
	}
	 public void actionPerformed(ActionEvent e) {
		//OAQ
	 }	
	 public static void movebtn (int x,int y,JButton b){
		 b.move(b.location().x+x,b.location().y+y); 	
	 }

	 private class Run implements Runnable{
		 public void run(){
			 while(true){
				 numClicks++;
				 btn.setText(""+numClicks);
				 
				 if(btn.location().x<=0){
					 btn.move(1,btn.location().y);
					 x = (int)(Math.random()*10);
					 y = (int)(Math.random()*10);
				 }
				 else if(btn.location().x+wei*2>=500){
					 btn.move(499-wei*2,btn.location().y);
					 x = (int)(Math.random()*10);
					 y = (int)(Math.random()*10);
					 x = -x;
				 }
				 else if(btn.location().y<=0){
					 btn.move(btn.location().x,1);
					 x = (int)(Math.random()*10);
					 y = (int)(Math.random()*10);
				 }
				 else if(btn.location().y+len>=500){
					 btn.move(btn.location().x,499-len);
					 x = (int)(Math.random()*10);
					 y = (int)(Math.random()*10);
					 y = -y;
				 }
				 while(true){
					 movebtn(x,y,btn);
					 if(btn.location().x<=0 || btn.location().x+wei*2>=500 || btn.location().y<=0 || btn.location().y+len>=500)
						 break;
					 try{
						 t.sleep(30);
					 }catch(Exception e){
						 e.printStackTrace();  
					 }
				 }
			 }
		 }
		 
	 }
	 
}




