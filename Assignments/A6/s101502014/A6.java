package ce1002.A6.s101502014;
import java.awt.event.ActionEvent; 
import java.awt.event.ActionListener;
import javax.swing.*; //引入GUI需要的功能
import java.util.Random; //引入隨機數字的功能
public class A6 extends JFrame implements Runnable , ActionListener{
	int count = 0 , x , y;  //count表示碰到牆壁的次數 , x代表現在按鈕的x位置 , y代表現在按鈕的y位置
	double dx , dy , angle; //dx代表按鈕前進的x分量 , dy代表按鈕前進的y分量
	private JButton btn = new JButton("0"); //建立顯示0的按鈕
	public static void main(String[] args) {
		A6 frame = new A6(); //建立視窗
		frame.setTitle("A6"); //命名視窗
		frame.setSize(500, 500); //設定視窗大小
		frame.setLayout(null); //不使用版面設定
		frame.setLocationRelativeTo(null); //固定視窗位置出現在螢幕的中間
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setVisible(true); //讓視窗出現
	}
	public A6(){
		setLayout(null); //不使用版面設定 
		Random ran = new Random(); //ran為隨機數字
		x = ran.nextInt(434); //x從0~434之間隨機數字
		y = ran.nextInt(413); //y從0~413之間隨機數字
		btn.setLocation(x , y); //設定btn按鈕的位置
		btn.setSize(50 , 50); //設定btn按鈕的大小
		angle = Math.random() * Math.PI; //angle從0~Pi之間隨機一個角度
		dx = 3 * Math.cos(angle); //決定dx的量
		dy = 3 * Math.sin(0.5 + angle); //決定dy的量
		btn.addActionListener(this); //把btn按鈕加入反應的功能
		add(btn); //加入btn按鈕到視窗中
		Thread t = new Thread(this);  
		t.start();  
	}
	public void run()
	{
		while(true)
		{
			x += dx; //改變按鈕的x位置
			y += dy; //改變按鈕的y位置
			set();
			try {
				Thread.sleep(10); //停止10毫秒在進行下個動作
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
	}
	public void set()
	{	
		//////////////////////// 若超出視窗範圍 , 則把按鈕的位置設定到邊界
		if(x < 0) 
			x = 0;
		else if(x > 434)
			x = 434;
		if(y < 0)
			y = 0;
		else if(y > 413)
			y = 413;
		////////////////////////
		if(x == 0 || x == 434 || y == 0 || y == 413) //若按鈕的位置到了邊界 , 則判定為反彈 , count值加一 , 並改變按鈕上顯示的值
		{
			count++;
			String num = Integer.toString(count);
			btn.setText(num);
		}
		btn.setLocation(x , y); //重新設定btn按鈕的位置
		if(x == 0 || x == 434) //若按鈕碰到的是左右的牆壁 , 則改變dx的值為-dx
			dx = -dx;
		if(y == 0 || y == 413) //若按鈕碰到的是上下的牆壁 , 則改變dy的值為-dy
			dy = -dy;
	}
	public void actionPerformed(ActionEvent e){ //若按鈕接收到滑鼠點擊的訊息 , 則隨機朝一個方向前進
		angle = Math.random() * Math.PI; //隨機角度
		dx = 3 * Math.cos(angle); //重新設定dx
		dy = 3 * Math.sin(0.5 + angle); //重新設定dy
	}
}
