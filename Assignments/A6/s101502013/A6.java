package ce1002.A6.s101502013;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.*;

import javax.swing.JButton;
import javax.swing.JFrame;




public class A6 extends TimerTask {
	static JButton b1 = new JButton("0"); 
	static double a = (Math.random() * 3.14);//a決定角度 不過此隨機只能讓按鈕往下  v是速度 可以改 
	static int number = 0,v=5,vx = (int)(v * Math.cos(a)),vy = (int)(v * Math.sin(a));
	static boolean sw = true; 
	
	public A6(){
		//frame
		String title = new String ("A6");
		JFrame haha = new JFrame(title);
		haha.setSize(400 , 400);
		haha.setLocation(300,300);
		haha.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		haha.setLayout(null);
		//		

		// Register listeners
		ButtonListenerClass listener1 = new ButtonListenerClass();
	    b1.addActionListener(listener1);
	    //
	    System.out.println(""+haha.getWidth()+ "   "+ haha.getHeight());
	    
		//加入frame
		haha.add(b1);
		
		//設定按鈕在中間位置
		b1.setBounds((int) (haha.getWidth() / 2.0) - 25, (int) (haha.getHeight() / 2.0) - 25, 50, 50);
		//顯示frame
		haha.setVisible(true); // Display the frame
		//
		
	}
	public void run() {
        
		if (b1.getX() <= 0 || b1.getX() >= 335)//檢查左右撞牆
		{
			vx = vx * -1;
			//System.out.println(b1.getX() + "  " + b1.getY());
		}
		if (b1.getY() <= 0 || b1.getY() >= 315)//檢查上下撞牆
		{
			vy = vy * -1;
			//System.out.println(b1.getX() + "  " + b1.getY());
		}
		if(sw==true)//sw為自己方便測試 可以停止按鈕移動
			b1.setBounds(b1.getX() + vx, b1.getY() + vy, 50, 50);
		
		//System.out.println("任務時間：" + new Date() + );
    }
    public static void main(String[] args) {
        Timer timer = new Timer();
        timer.schedule(new A6(), 0 ,20);
        System.out.println("現在時間：" + new Date());
        
        try {
            Thread.sleep(60000);//60秒後停止
        }
        catch(InterruptedException e) {
        }
        timer.cancel(); 
    }
    class ButtonListenerClass implements ActionListener {
		public void actionPerformed(ActionEvent e) {
			number++;//設定數字 並隨機改變方向
			b1.setText(new String("" + number));
			a = (Math.random() * 3.14);
			vx = (int)(v * Math.cos(a));
			vy = (int)(v * Math.sin(a));
			//sw = false;
			
			

			
		}
	}
}

