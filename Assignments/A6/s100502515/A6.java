package ce1002.a6.s100502515;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;

public class A6 implements ActionListener {
	JButton button = new JButton();
	JFrame frame = new JFrame("A6");
	int originX = (int) (Math.random() * 500) + 10;
	int originY = (int) (Math.random() * 500) + 10;
	int moveX = 0;
	int moveY = 0;
	int count = 0;

	public A6() {
		frame.setSize(600, 600);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setResizable(false);
		frame.setVisible(true);
	}

	public static void main(String[] args) throws InterruptedException {
		A6 a6 = new A6();
		a6.moveMent();
		a6.FrameAndButton();
	}

	private void moveMent() { // Random movement of the button
		double x = Math.random();
		double y = Math.random();
		if (x < 0.5) {
			moveX = -1;
		} else {
			moveX = 1;
		}

		if (y < 0.5) {
			moveY = -1;
		} else {
			moveY = 1;
		}

	}

	private void FrameAndButton() throws InterruptedException {
		button.addActionListener(this);
		while (true) {

			button.setSize(100, 100); // Set the size of the button as 100x100
			button.setText(Integer.toString(count));
			frame.add(button);

			button.setLocation(originX, originY);// Set the origin location of
													// the button
			Thread.sleep(20);
			if (originX > 495 || originX < 0) {
				moveX = moveX * -1;
				count++;
			}
			if (originY > 475 || originY < 0) {
				moveY = moveY * -1;
				count++;
			}// Set the boundary of the frame
			originX += moveX;
			originY += moveY;
		}

	}

	@Override
	public void actionPerformed(ActionEvent arg0) {
		moveMent();// Change the movement of the button when it is pressed.
	}
}
