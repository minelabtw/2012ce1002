package ce1002.A6.s100502201;

import javax.swing.JFrame;
import javax.swing.JButton;
import java.util.Timer;
import java.util.TimerTask;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class A6 extends JFrame {

	public JButton button;
	
	private double ran = Math.random(); // Initialize it to make it constant
	public double x = 0; // X-axis location of JB
	public double y = 0; // Y-axis location of JB
	public double speedX = Math.sin(ran) * 10; // Velocity of X-axis
	public double speedY = Math.cos(ran) * 10; // Velocity of Y-axis
	
	private static int times = 0;
	
	public static void main(String[] args) {

		new A6();

	}

	public A6() {

		setLayout(null);
		setTitle("A6");
		setSize(500, 500);
		button = new JButton(times + "");
		button.setBounds(0, 0, 50, 50);
		add(button);
		setVisible(true);

		Timer timer = new Timer();
		timer.schedule(new pinball(), 100, 50); // Can't set delay time to zero,
												// would end up occurring errors

		button.addActionListener(new ActionListener() {

			public void actionPerformed(ActionEvent e) {

				ran = Math.random();
				speedX = Math.sin(2 * Math.PI * ran) * 10; //Change direction
				speedY = Math.cos(2 * Math.PI * ran) * 10;

			}

		});

	}

	class pinball extends TimerTask {
		
		public void run() {
			
			boolean bound = false;
			// If bump into wall, bounce back (4 directions)
			if (x + speedX < 0) {
				speedX = -speedX;
				bound = true;
			}
			
			if (y + speedY < 0) {
				speedY = -speedY;
				bound = true;
			}
			
			if (x + speedX + 50 >= getWidth()) { // Must conclude the size of JB
				speedX = -speedX;
				bound = true;
			}

			if (y + speedY + 50 >= getHeight()) {
				speedY = -speedY;
				bound = true;
			}

			if (bound) {
				times++;
				button.setText("" + times);
			}

			x = speedX + x; //Set new location
			y = speedY + y;
			button.setBounds((int) x, (int) y, 50, 50);

		}
	}
}