package ce1002.A6.s101502501;
import java.awt.event.*;
import javax.swing.*;

public class A6 extends JFrame implements ActionListener
{
	private static int x=11, y=11, num=0;//設定起始座標,num用來計算撞到邊邊時增加的數字
	private static	double dx=0, dy=0;
	private static JButton button = new JButton();//宣告按鈕
	
	public A6(String pTitle){
		super(pTitle);
		setLayout(null);
		button.setBounds(x, y, 50, 50);//設定按鈕大小
		add(button);//增加按鈕
		button.addActionListener(this);//按一下執行下面的動作
	}
	

	public static void main(String[] args){
		A6 frame = new A6("A6");
		frame.setSize(515,540);//設定視窗大小
		frame.setLocationRelativeTo(null);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setVisible(true);
		
		double r=Math.random()*3.14159;
		dx=3*Math.cos(r);
		dy=3*Math.sin(r);

		while(true) {
			x+=dx;
			y+=dy;
			button.setLocation((int)x, (int)y);
			if(button.getLocation(). x>=450||button.getLocation().x<=0){//碰到邊邊時反彈
				dx*=-1;
				num++;
			}
			if(button.getLocation(). y>=450||button.getLocation().y<=0){//碰到邊邊時反彈
				dy*=-1;
				num++;
			}
			
			button.setText(Integer.toString(num));//num增加時要顯示按鈕
			frame.getContentPane().repaint();
			
			try{
				Thread.sleep(15);
			}
			catch(InterruptedException e){
				e.printStackTrace();
			}
		}
	}

	public void actionPerformed(ActionEvent e){//點一下按鈕隨機變換方向
		double r = Math.random()*3.14159;
		dx=(3*Math.cos(r));
		dy=(3*Math.sin(r));
		
	}
}
