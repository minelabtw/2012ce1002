package ce1002.A6.s101502023;

import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.*;
import javax.swing.JButton;
import javax.swing.JFrame;

public class A6 extends Thread implements ActionListener
{
	JFrame J = new JFrame();
	static int x,y; //x and y coordinate
	static int collision=0; //total number of hit the wall
	static int a=(int)(Math.random()*51)-25,b=(int)(Math.random()*51)-25;
	private JButton bt = new JButton();
	public static void main(String args[])
	{ 
	    A6 thread = new A6("move");
	    thread.start(); 
    }
		
    A6(String name)
    {
    	super(name); 
    	
    	J.setTitle("A6");
    	J.setSize(800,600);
    	J.setLayout(null);
    	J.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		J.setVisible(true);
		x=J.getWidth()/2;
		y=J.getHeight()/2;
    }
   
	public void run()
	{
	    try
	    {
	    	while(true){
				Thread.sleep(50); //delay of 50 ms
				bt.setBounds(x,y,100,100); //the button of the length and width of 100 
				
				//make the button change with time 
				x+=a;
				y+=b;
				
				if(x>=685||x<=0)
				{
					if(x>=685)
						x=685;
					else 
						x=0;
					a=-a; //go in the opposite direction
					collision++; //碰到牆壁的次數+1
				}//run into the left and right sides of the wall
				if(y>=465||y<=0)
				{
					if(y>=465)
						y=465;
					else
						y=0;
					b=-b; //往反方向走
					collision++; //碰到牆壁的次數+1
				}//hit the up and down both sides of the wall
				J.add(bt);
				bt.setText(""+collision); //the button's name is the number of hit the wall
				bt.addActionListener(this); //click on the button response
			}
	    }catch(InterruptedException e){}
	  }

	public void actionPerformed(ActionEvent e) 
	{
		//when the button is clicked,button to move randomly change
		if(e.getSource()==bt)
		{
			a=(int)(Math.random()*51)-25;
			b=(int)(Math.random()*51)-25;
		}
	}
}