package ce1002.A6.s101502506;
import java.awt.event.*;
import java.awt.*;
import javax.swing.*;

public class A6  extends JFrame implements ActionListener
{
	private static int x=11,y=11;
	private static int times=0;//設一個變數times為初始值0計算碰到牆的次數
	private static double dx=0,dy=0;//設變換的x,y方向初始值皆為0
	private static JButton btn = new JButton();//設一個新button
	
	public A6(String pTitle)
	{
		super(pTitle);//視窗名字為pTitle
		setLayout(null);//跳出視窗在正中央
		btn.setBounds(x,y,60,60);
		add(btn);//附加button到frame上
		btn.addActionListener(this);//監聽此button
	}

	public static void main(String args[])
	{
		A6 frame = new A6("A6");//視窗名稱為A6
		frame.setSize(530, 550);//設定視窗大小
		frame.setLocationRelativeTo(null);
		frame.setVisible(true);

		double r=Math.random()*3.14;//製造隨機的變換位置
		dx=3*Math.cos(r);
		dy=3*Math.sin(r);
			
		while(true)
		{
			x+=dx;
			y+=dy;
			btn.setLocation((int)x,(int)y);
			
			if( btn.getLocation().x>=450 || btn.getLocation().x<=0 )//當碰到一邊
			{
				dx*=-1;
				times++;
			}
			
			if( btn.getLocation().y>=450 || btn.getLocation().y<=0 )//當碰到一邊
			{
				dy*=-1;
				times++;
			}
			
			btn.setText(Integer.toString(times));//顯示數字在button上
			frame.getContentPane().repaint();
			
			try//捕捉例外
			{
				Thread.sleep(15);
			}
			catch (InterruptedException e)
			{
				e.printStackTrace();
			}
		}
		
	}
	
	public void actionPerformed(ActionEvent e)//隨機變換方向
	{
		double r=Math.random()*3.14;
		dx=3*Math.cos(r);
		dy=3*Math.sin(r);
	}

}
