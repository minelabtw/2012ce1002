package ce1002.A6.s995002022;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JFrame;

public class A6 extends JFrame implements ActionListener {
	static int num=0;
	static  JButton b = new JButton(""+num);
	static int dx=2,dy=2;
	static int x,y;
	static int count=1;
	
public static void main(String[] args) {
	A6 hw = new A6("A6");
	hw.setSize(400,400);
	hw.setLocation(700, 300);
	hw.setVisible(true);
	
	while(true)
	 {
		 if(count%2==1){x+=dx;}
		 if(count%2==0){y+=dy;}
		 b.setLocation((int)x, (int)y);
		 if(count%2==1){	
		 	if(b.getLocation().x>=300||b.getLocation().x<=0)
				 {
				 dx*=-1;
				 num++;
				 }
		 }
		 if(count%2==0){
		 	if(b.getLocation().y>=300||b.getLocation().y<=0)
				 {
				 dy*=-1;
				 num++;
				 }
		 }
		 b.setText(Integer.toString(num));		 
		 try 
		{
		 Thread.sleep(10);
		 } 
		catch (InterruptedException e) 
		{
		}
	 }
}	

public A6(String pTitle) {
	super(pTitle);
	setLayout(null);// 不使用版面配置
	b.setBounds(100, 300, 90, 60); //X Y 長 寬
	b.addActionListener(this);
	add(b);
}

	public void actionPerformed(ActionEvent e) {
		count++;
	}
}