package ce1002.A6.s101502003;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.*;
public class A6 extends JFrame  implements ActionListener 
{
	private int count =0;
	private JButton btn = new JButton(" "+count);
	public static void main(String[] args)
	{
		A6 hw = new A6("A6"); //標題名字
		hw.setSize(500, 500);
		hw.setLocation(250, 250);
		hw.setVisible(true);
	}
	public A6(String pTitle) {
		super(pTitle);
		setLayout(null);
		btn.addActionListener(this);
		add(btn);
		btn.setBounds(20, 30, 50, 50);
		Timer timer = new Timer(100,new TimerListener()); //設時間
		timer.start(); //時間開始
		
	}
	class TimerListener implements ActionListener
	{
		public void actionPerformed(ActionEvent e)
		{
			int xCoordinate =20; //設初值
			int yCoordinate =30; 
			btn.setBounds(xCoordinate, yCoordinate, 50, 50); //跑~
			xCoordinate +=5;
			yCoordinate +=5;
			if(xCoordinate>getWidth()) //撞到牆壁count+1
			{
				xCoordinate = -20;
				count ++;
				btn.setText(" " + count ); //button的數+1
			}
			if(yCoordinate>getHeight()) //撞到牆壁count+1
			{
				yCoordinate = 0;
				count ++;
				btn.setText(" " + count ); //button的數+1
			}
		}
	}
	public void actionPerformed(ActionEvent e) { //如果按到按鈕加一
		count++;
		btn.setText(" " + count );
	}
}