package ce1002.A6.s101502502;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Timer;
import java.util.TimerTask;

import javax.swing.JButton;
import javax.swing.JFrame;

public class A6 extends JFrame implements ActionListener
{
	private JButton BT = new JButton();
	private int a = 0;
	DateTask mTask = new DateTask();//讓button持續跑
	private int wBT = 60;
	private int hBT = 60;//button的大小
	private static int wWin = 500;
	private static int hWin = 500;//視窗的大小
	private double x = (int)(Math.random()*(wWin - wBT));
	private double y = (int)(Math.random()*(hWin - hBT));//隨機的初始位置
	private double dx = 5;
	private double dy = 5;//每次移動的的x, y值
	private int direction = 1;//初始方向位置

//我讓button預設為四個方向:
//0為右上, 1為右下 , 2為左下 , 3為左上
	
	public void ChangePosition()
	{//改變位置
		switch(direction)
    	{
    		case 0:
    			x+=dx;
    			y-=dy;
    			break;
    		
    		case 1:
    			x+=dx;
    			y+=dy;
    			break;
    			
    		case 2:
    			x-=dx;
    			y+=dy;
    			break;
    		
    		case 3:
    			x-=dx;
    			y-=dy;
    			break;
    	}
		
	}
	
	public void ChangeDirection()
	{//撞壁時改變方向
		int d = direction;
		double x0 = x;
		double y0 = y;
		
		switch(direction)
		{
			case 0:
				x0-=dx;
				y0+=dy;
				if(x0 > wWin - wBT)		
					direction = 3;	
				else if(y0 < 0)
					direction = 1;
				break;
				
			case 1:
				x0-=dx;
				y0-=dy;
				if(x0 > wWin - wBT)		
					direction = 2;	
				else if(y0 > hWin - hBT)
					direction = 0;
				break;
			
			case 2:
				x0+=dx;
				y0-=dy;
				if(x0 < 0)		
					direction = 1;	
				else if(y0 > hWin - hBT)
					direction = 3;
				break;
			
			case 3:
				x0+=dx;
				y0+=dy;
				if(x0 < 0)		
					direction = 0;	
				else if(y0 < 0)
					direction = 2;
				break;
		}
		
		if(d != direction)
		{//若確定有改變方向,則button上的數字+1,然後繼續改變位置
			x = x0;
			y = y0;
			a++;
			BT.setText(""+a);
			ChangePosition();
		}
		
	}
	
	public class DateTask extends TimerTask 
	{
	    public void run() 
	    {
	    	ChangePosition();
	    	ChangeDirection();
    		BT.setBounds((int)x, (int)y, wBT, hBT);//每改變位置一次就讓button移動
	    }
	}
	
	public A6()
	{
		setLayout(null);
		BT.setBounds((int)x, (int)y, wBT, hBT);
		BT.setText("" + a);
		add(BT);
		BT.addActionListener(this);
	}
	
	public static void main(String[] args)
	{
		A6 frame = new A6();
		frame.setTitle("A6");
		frame.setSize(wWin+16, hWin+38);
		frame.setLocationRelativeTo(null);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setVisible(true);
		
		Timer timer = new Timer();
        timer.schedule(frame.mTask, 1000, 50);//第一次隔1000毫秒才開始,接下來每50毫秒就執行一次
	}

	@Override
	public void actionPerformed(ActionEvent arg0) {
		//按button時要隨機改變方向
		direction=(int)(Math.random()*4);//改變方向
		
		double r = Math.random()*3.14159;
		dx = Math.abs(10 * Math.cos(r));
		dy = Math.abs(10 * Math.sin(r));//改變移動的x,y值
	}
}
