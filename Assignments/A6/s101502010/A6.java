package ce1002.A6.s101502010;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.Random; 
import javax.imageio.ImageIO;
import javax.swing.*;

import ce1002.A5.s101502010.A5;

public class A6 {
	static JButton btn ;
	static JFrame frame = new JFrame();
	static int tmpx = 0,tmpy = 0,directionx=1,directiony=1,i=0;
	Random rand=new Random(); 
	public static void main(String srgs[])throws IOException, InterruptedException{
		A6 a6 = new A6();
		
		while(true){
			btn.setLocation(tmpx,tmpy);
			btn.setText(""+i);
			tmpx+=directionx;
			tmpy+=directiony;
			if((tmpx+150)>=frame.getContentPane().getWidth() || tmpx<=0){
				directionx*=-1;
				i++;
			}
			if((tmpy+100)>=frame.getContentPane().getHeight() || tmpy<=0){
				directiony*=-1;
				i++;
			}
			Thread.sleep(5);   
		}
	}
	public A6() throws IOException, InterruptedException{
		
		frame.setTitle("A6-101502010");
		frame.setLayout(null);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);  
		frame.setSize(1000,1000);
		btn = new JButton(""+i);
		btn.setBounds(0,0,150,100);
		btn.addActionListener(new ActionListener(){
          	public void actionPerformed(ActionEvent arg0) {
          		directionx*=((rand.nextInt(10)%2)*2-1);
          		directiony*=((rand.nextInt(10)%2)*2-1);
          	}
        });
		frame.add(btn);
		frame.setVisible(true);
	}
}
