package ce1002.A6.s101502508;

import javax.swing.JButton;
import javax.swing.JFrame ;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class A6 extends JFrame implements ActionListener{
	
	private static int counter=0 ;//計算反彈次數
	private static int x=200 ;
	private static int y=200 ;
	private static double Va=0 ;//向量ab
	private static double Vb=0 ;
	private static double r ;//斜率
	private static int  k=10 ;//隨意值(發現數字不能太小  如0和1，不然會有bug)，
	static JButton btn = new JButton() ;

	public A6(){
		setLayout(null);
		setTitle("A6") ;
		btn.setBounds(x, y, 60, 60) ;//設定初始位置
		btn.addActionListener(this);
	}
	
	public static void main(String[] args){
		A6 frame = new A6() ;
		frame.add(btn) ;
		frame.setSize(500, 500) ;
		frame.setLocationRelativeTo(null) ;
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE) ;
		frame.setVisible(true) ;
		r =  (Math.random()*3.14159) ;
		
		Va =  (k*Math.cos(r)) ;
		Vb =  (k*Math.sin(r)) ;
		
		while (true){
			x+=Va ;
			y+=Vb ;
			btn.setLocation(x, y) ;//記錄每一次的xy位置
			
			if( x>422 || x<0 ){
				Va*=(-1) ;
				counter++ ;
			}
			
			if( y>402 || y<0){
				Vb*=(-1) ;
				counter++ ;
			}
			
			btn.setText(Integer.toString(counter)) ;//轉string
			frame.getContentPane().repaint() ;//在中心的數字每次都更新
		
			try{//延遲時間使用
				Thread.sleep(60) ;
			}
			catch(InterruptedException e){}
		}
	}
	
	public void actionPerformed(ActionEvent e) {//每次按按鈕就做隨機處理
		Va =  (k*Math.cos(r)) ;
		Vb =  (k*Math.sin(r)) ;
	}

}
