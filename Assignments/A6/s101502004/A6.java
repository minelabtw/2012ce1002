package ce1002.A6.s101502004;
import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class A6 extends JFrame{
	int i=0;//按鈕上的數字計算
	private JButton B = new JButton();//新增按鈕
	MoveTheButton move = new MoveTheButton(B);//新增物件
	public A6(){
		add(move);//將物件add進主程式中
	}
	
	public static void main(String[] args){//main function設置視窗
		A6 frame = new A6();
		frame.setTitle("A6");
		frame.setSize(600, 400);
		frame.setLocationRelativeTo(null);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setVisible(true);
	}
	
	class MoveTheButton extends JPanel implements ActionListener{//按鈕移動與action的class
		private int x = 40;private int y = 40;//初始x與y座標
		private int dx = 2;//每次移動多少
		private int dy = 2;//每次移動多少
		public MoveTheButton(JButton B){
			Timer timer = new Timer(50, new TimerListener());//設定Timer
			timer.start();
			setLayout(null);//設定框架
			B.setBounds(40, 40, 50, 50);//設定按鈕大小
			add(B);//將按鈕add進去
		}
		
		public void MovingButton(JButton B){
			if(x>getWidth()-50){//撞擊畫面右邊後的反應
				dx = -Math.abs(dx);//改變方向
				i++;//按鈕上的數字+1
			}
			if(y>getHeight()-50){//撞擊畫面下方
				dy = -Math.abs(dy);
				i++;
			}
			if(x<0){//左邊
				dx = Math.abs(dx);
				i++;
			}
			if(y<0){//上方
				dy = Math.abs(dy);
				i++;
			}
			x += dx;//移動x座標
			y += dy;//移動y座標
			B.setLocation(x, y);//按鈕位置設定
			B.setText(Integer.toString(i));//設定按鈕上面的數字
			B.addActionListener(this);//按鈕反應
		}
		
		class TimerListener implements ActionListener{//Timer反應
			public void actionPerformed(ActionEvent e){
				move.MovingButton(B);//將上面的function進Timer啟動
			}
		}
		public void actionPerformed(ActionEvent e){//按鈕反應，改變方向
			dx = -dx;
			dy = -dy;
		}
	}
}
