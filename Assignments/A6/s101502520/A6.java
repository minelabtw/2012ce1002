package ce1002.A6.s101502520;

import java.awt.Point;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.Timer;

public class A6 extends JFrame{
	private final int width = 300;
	private final int height = 300;
	private final int btnWidth = 60;
	private final int btnHeight = 60;
	
	private int counter = 0;
	private double x, y;
	
	private JButton btn = new JButton("0");
	private Timer timer = new Timer(20, new ActionListener() {
		
		@Override
		public void actionPerformed(ActionEvent arg0) {
			// TODO Auto-generated method stub
			movepersec();
		}
	});
	public A6()
	{
		changeDir();
		iniUI();
	}
	
	public void iniUI()
	{
		setLayout(null);
		btn.setSize(btnWidth, btnHeight);
		btn.setLocation(width / 2, height / 2);
		btn.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				// TODO Auto-generated method stub
				changeDir();
			}
		});
		add(btn);
		timer.start();
		setSize(width, height);
		
	}
	
	public void movepersec()
	{
		Point nowPoint = btn.getLocation();
		nowPoint.setLocation(nowPoint.getX() + x, nowPoint.getY() + y);
		btn.setLocation(nowPoint);
		//use getContentPane().getWidth() to get frame width without border
		if (nowPoint.getX() < 0 || nowPoint.getX() >= getContentPane().getWidth() - btnWidth) {
			x = -1 * x;
			counter++;
			btn.setText(Integer.toString(counter));
		}
		if (nowPoint.getY() < 0 || nowPoint.getY() >= getContentPane().getHeight() - btnHeight) {
			y = -1 * y;
			counter++;
			btn.setText(Integer.toString(counter));
		}
	}
	
	public void changeDir()
	{
		double degree = Math.random() * 360;// random for 360 degrees
		x = Math.cos(Math.toRadians(degree)) * 10;//enlarge the difference
		y = Math.sin(Math.toRadians(degree)) * 10;
	}

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
            	new A6().setVisible(true);
            }
        });
	}

}
