package ce1002.A6.s101502526;
import java.awt.*;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.*;
import java.util.Stack;
import java.util.Timer;
import java.util.TimerTask;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JTextField;


public class A6 extends JFrame implements ActionListener {

	static int i = 0 , x = 250 , y = 250 , j;
	int xway = 1, yway =1 , randomx =0, randomy=0 , pox= 1 , poy = 1  ;
	static JButton b = new JButton(""+i);
	double random ;
    final Timer timer = new Timer();
  public A6() {

	  	 Container c = getContentPane();                     //設定按鈕
	     c = this.getContentPane();
	     c.setLayout(null);
	     b.setBounds(200, 200, 50, 50);
	     add(b);
	     random= Math.random()*360;                         //隨機一個角度
	     b.addActionListener(new ActionListener(){          //按下去再隨機角度且數字+1
			public void actionPerformed(ActionEvent e) {
				random= Math.random()*360;
		    	i+=1;
		    	b.setText(""+i);                            //改變數字
		    	revalidate();
		    	repaint();
			}
		});	

	     timer.scheduleAtFixedRate(new TimerTask() {        //持續跑動做
	    	 public void run() {
	    		 randomx = (int)(Math.cos(random)*5*pox);   //X為+上的COS剛剛的變數*5
	    		 randomy = (int)(Math.sin(random)*5*poy);	//Y為+上的SIN剛剛的變數*5
	    		 x += randomx  ;
	    		 y += randomy ;
	    		 b.setLocation(x, y);						//改變位置
	    		 add(b);
	    		 revalidate();
	    		 repaint();
	    	  if (x>getWidth()-70||x<0)						//若碰壁反彈
	    	  {
	    		  pox*=-1;
	    		  i+=1;
	    		  b.setText(""+i);
	    		  
	    	  }
	    	  if (y>getHeight()-90||y<0)					//若碰壁反彈
	    	  {
	    		  poy*=-1;
	    		  i+=1;
	    		  b.setText(""+i);
	    	  }

	        }
	      }, 0, 20);
    
  }
  
  public static void main(String[] args) {               
    A6 frame = new A6();
    frame.setTitle("The Front View of a Microwave Oven");
    frame.setSize(500, 500);
    frame.setLocationRelativeTo(null); 
    frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    frame.setVisible(true);

  }

@Override
public void actionPerformed(ActionEvent arg0) {
	// TODO Auto-generated method stub
	
}

}
