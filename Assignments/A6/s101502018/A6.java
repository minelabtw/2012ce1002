package ce1002.A6.s101502018;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.lang.Thread;
import javax.swing.*;
public  class A6 extends JFrame implements ActionListener{
	JButton btn = new JButton();
	int x=200,y=200,dx=1,dy=1,m=0,z=0;
	public A6(String pTitle){
		super(pTitle);
		setLayout(null);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setSize(400,500);
		btn.setBounds(120, 20, 50, 50);
		btn.addActionListener(this);
		btn.setText(String.valueOf(m));
		add(btn);
		Thread thread1 = new Thread(new Runnable() {
			public void run(){
					try{
						while(z==0){
							x=x+dx;
							y=y+dy;
							btn.setBounds(x, y, 50, 50);
							if(x>=340 ||x<=0){
								dx=-dx;
								m++;
								btn.setText(String.valueOf(m));
							}
							if(y>=415 ||y<=0){
								dy=-dy;
								m++;
								btn.setText(String.valueOf(m));
							}
							Thread.sleep(10);
						}
					}
					catch(InterruptedException e) { 
						e.printStackTrace();
					}
			}
		});
		thread1.start();
		setVisible(true);
	}
	
	public static void main(String[] args){
		new A6("A6");
	}
	public void actionPerformed(ActionEvent e) {
		double r = Math.random()*(Math.PI * 2);
		dx = (int)(4*Math.cos(r));
		dy = (int)(4*Math.sin(r));
	}
}
