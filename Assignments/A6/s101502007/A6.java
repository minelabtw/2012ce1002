package ce1002.A6.s101502007;

import java.awt.Component;
import java.awt.Container;
import java.io.IOException;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JFrame;

public class A6 extends JFrame{
	static String g="0";
	static int arc=30; //初始角度
	static JButton b=new JButton(g);
	static double i=Math.cos(arc)*10; //初始的向量 乘以10是為了減少誤差
	static double j=Math.sin(arc)*10; //初始的向量 乘以10是為了減少誤差
	public A6() 
	{
		setLayout(null);
		b.setBounds(5, 5, 50, 50);
		setSize(400, 400);
		b.addActionListener(new ActionListener()
		{
			@Override
			public void actionPerformed(ActionEvent e)
			{
				arc=(int)(Math.random()*359)%360; //若按下按鈕,則隨意出現角度
				i=Math.cos(arc)*10; //新的向量
				j=Math.sin(arc)*10; //新的向量
			}
		}				
				);
		add(b);
	}
	
	public static void main(String[] args) throws InterruptedException {
		A6 frame=new A6();
		frame.setVisible(true);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	    int f=0;//計數器
		double x = 5;
		double y = 5;
		while(true)
		{
			x=x+i;
			y=y+j;
			if (x>=frame.getContentPane().getWidth()-50 ||x<=0) //若剛好或超過左右兩邊 
			{
				i=-i;
				f++;
				if (x<=0)
					x=0;
				else
					x=frame.getContentPane().getWidth()-50;
				g=Integer.toString(f);
				b.setText(g);  //重設數字
			}
			if (y>=frame.getContentPane().getHeight()-50 || y<=0) //若剛好或超過上下兩邊 
			{
				j=-j;
				f++;
				if (y<=0)
					y=0;
				else
					y=frame.getContentPane().getHeight()-50;
				g=Integer.toString(f);
				b.setText(g); //重設數字
			}
			int n=(int)x;
			int m=(int)y;
			b.setLocation(n, m);
			Thread.sleep(40);
		}
	}

}
