package ce1002.A6.s101502021;

import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.*;

import javax.swing.JButton;
import javax.swing.JFrame;

public class A6 extends Thread implements ActionListener{
	JFrame J = new JFrame();
	static int i,j; //x,y座標
	static int total=0; //撞到牆壁的總數
	static int w=(int)(Math.random()*51)-25,z=(int)(Math.random()*51)-25;
	private JButton bt = new JButton();
	
	//宣告A6執行緒物件
	public static void main(String args[]){ 
	    A6 thread = new A6("move");
	    thread.start(); //啟動執行緒
    }
		
    A6(String name){
    	super(name); //呼叫Thread類別的建構子
    	
    	J.setTitle("A6");
    	J.setSize(800,600);
    	J.setLayout(null);
    	J.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		J.setVisible(true);
		i=J.getWidth()/2;
		j=J.getHeight()/2;
		
    }
   
	public void run(){
	    try{
	    	//讓執行緒一直run
	    	while(true){
				Thread.sleep(50); //延遲50毫秒
				bt.setBounds(i,j,100,100); //設定位置在座標(i,j)，長寬各100的button
				
				//讓button位置隨著時間改變
				i+=w;
				j+=z;
				
				//碰到左右兩邊牆壁的話
				if(i>=685||i<=0){
					if(i>=685)
						i=685;
					else 
						i=0;
					w=-w; //往反方向走
					total++; //碰到牆壁的次數+1
				}
				
				//碰到上下兩邊牆壁的話
				if(j>=465||j<=0){
					if(j>=465)
						j=465;
					else
						j=0;
					z=-z; //往反方向走
					total++; //碰到牆壁的次數+1
				}
				J.add(bt);
				bt.setText(""+total); //button的名子就是碰到牆壁的次數
				bt.setFont(new Font("華康中楷體",Font.BOLD,40)); //設定字型、字體大小
				bt.setBorderPainted(true); //顯示button的邊框
				bt.addActionListener(this); //點擊button有反應
			}
	    }catch(InterruptedException e) {}
	  }

	public void actionPerformed(ActionEvent e) {
		//點擊button時，button移動的方式會隨機改變
		if(e.getSource()==bt){
			w=(int)(Math.random()*51)-25;
			z=(int)(Math.random()*51)-25;
		}
	}
}
