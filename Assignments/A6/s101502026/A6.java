package ce1002.A6.s101502026;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.util.Timer;
import java.util.TimerTask;

class move extends TimerTask
{
	public void run()
	{
		A6.x=A6.x+A6.dx;
		A6.y=A6.y+A6.dy;
		A6.button.setBounds(A6.x,A6.y,70,70);
		if (A6.y==690||A6.y==0)
		{
			A6.dy=-A6.dy;	
			A6.initial++;
			A6.button.setText(String.valueOf(A6.initial));
		}
		if (A6.x==1115||A6.x==0)
		{
			A6.dx=-A6.dx;
			A6.initial++;
			A6.button.setText(String.valueOf(A6.initial));
		}
	}
}

public class A6 implements ActionListener 
{
	public static JButton button=new JButton();
	public static int x=0,y=0,dx=1,dy=1,initial=0;
	public static void main (String[]args)
	{
		JFrame.setDefaultLookAndFeelDecorated(true); //make the frame look like decorated
		Timer timer=new Timer(); //build timer
		A6 frame1=new A6();
		JFrame frame2=new JFrame("A6");
		frame2.setSize(1200,800);
		frame2.setLocationRelativeTo(null);
		frame2.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame2.setVisible(true);
		frame2.setLayout(null);
		button.addActionListener(frame1);
		button.setText(String.valueOf(initial));
		frame2.add(button);
		x = (int)(Math.random()*(frame2.getWidth()-80)+1); //set the initial place for the button's x-coordinate
        y = (int)(Math.random()*(frame2.getHeight()-105)+1); //set the initial place for the button's y-coordinate
		timer.schedule(new move(), 0, 10); //move the button with Timer
	}
	public void actionPerformed(ActionEvent e) { //this is what happened after clicking mouse
		double r = Math.random()*(Math.PI * 2);
		dx = (int)(2*Math.cos(r)); //x-direction
		dy = (int)(2*Math.sin(r)); //y-direction 
	}
}