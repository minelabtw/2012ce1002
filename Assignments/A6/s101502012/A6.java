package ce1002.A6.s101502012;

import java.util.Timer;
import java.util.TimerTask;
import java.awt.event.*;

import javax.swing.*;

public class A6 extends TimerTask implements ActionListener {

	public JFrame frame;
	public JButton btnMove = new JButton();
	private int x, y, i = 0;
	double a = Math.random(); // 設定隨機數
	int vx = (int) (5 * Math.cos(a)), vy = (int) (5 * Math.sin(a));

	public A6() {
		frame = new JFrame();
		frame.getContentPane().setLayout(null);
		frame.setBounds(100, 100, 500, 500);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().add(btnMove);

		btnMove.setText("0");
		btnMove.addActionListener(this);

		x = frame.getWidth() / 2 - 50;
		y = frame.getHeight() / 2 - 50;
		btnMove.setBounds(x, y, 50, 50);

		Timer timer = new Timer();
		timer.schedule(this, 0, 20);
	}

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		try {
			A6 window = new A6();
			window.frame.setVisible(true);
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	@Override
	public void actionPerformed(ActionEvent e) {
		// TODO Auto-generated method stub

		a = Math.random(); // 改變隨機數a
		vx = (int) (5 * Math.cos(a)); // x座標移動方向改變
		vy = (int) (5 * Math.sin(a)); // y座標移動方向改變
		i++;
		btnMove.setText(new String("" + i));
		System.out.print(i);

	}

	@Override
	public void run() {
		if (x <= 0 || x >= 435) { //超出視窗的瞬間讓他改方向
			vx = -vx;
		}
		if (y <= 0 || y >= 415) { //超出視窗的瞬間讓他改方向
			vy = -vy;
		}

		x += vx;
		y += vy; 
		btnMove.setLocation(x, y);
	}

}
