package ce1002.A6.s100502006;

import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.Timer;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;

public class A6 extends JFrame{
	private JButton jbt=new JButton();
	private double xCoordinate;
	private double yCoordinate;
	private double displacement=0.5;
	private double dx;
	private double dy;
	private double angle; 
	private JPanel p1=new JPanel();
	private int count=0;
	public A6() {
		
		//initialize components
		p1.setLayout(null);
		p1.setPreferredSize(new Dimension(600, 600));
		jbt.setSize(100, 100);
		
		//JButton start at middle of the panel
		xCoordinate=(p1.getPreferredSize().width-jbt.getWidth())/2;
		yCoordinate=(p1.getPreferredSize().height-jbt.getHeight())/2;
		jbt.setLocation((int)Math.round(xCoordinate),(int)Math.round(yCoordinate));
		jbt.setText(String.valueOf(count));
		
		//count angle and displacements
		angle=Math.random()*(2*Math.PI);
		dx=displacement*Math.cos(angle);
		dy=displacement*Math.sin(angle);
		
		p1.add(jbt);
		add(p1);
		
		//TimerListener
		Timer timer=new Timer(1,new TimerListener());
		timer.start(); 
		
		//ButtonListener
		jbt.addActionListener(new ButtonListener());
		
	}
	class TimerListener implements ActionListener{

		@Override
		public void actionPerformed(ActionEvent e) {
			// TODO Auto-generated method stub
			
			//determine if the button is in the boundaries of the panel
			if((int)Math.round(xCoordinate)<=0){
				count++;
				jbt.setText(String.valueOf(count));
				dx=Math.abs(dx);
			}
			else if ((int)Math.round(xCoordinate)>=(p1.getPreferredSize().width-jbt.getWidth())) {
				count++;
				jbt.setText(String.valueOf(count));
				dx=-Math.abs(dx);
			}
			if ((int)Math.round(yCoordinate)<=0) {
				count++;
				jbt.setText(String.valueOf(count));
				dy=Math.abs(dy);
			}
			else if ((int)Math.round(yCoordinate)>=(p1.getPreferredSize().height-jbt.getHeight())) {
				count++;
				jbt.setText(String.valueOf(count));
				dy=-Math.abs(dy);
			}
			
			//move
			xCoordinate+=dx;
			yCoordinate+=dy;
			jbt.setLocation((int)Math.round(xCoordinate),(int)Math.round(yCoordinate));
			
		}		
	}
	class ButtonListener implements ActionListener{

		@Override
		public void actionPerformed(ActionEvent e) {
			// TODO Auto-generated method stub
			
			//change angle
			angle=Math.random()*(2*Math.PI);
			dx=displacement*Math.cos(angle);
			dy=displacement*Math.sin(angle);
			
		}
		
	}
	public static void main(String[] args) {
		A6 frame=new A6();
		frame.setTitle("A6");
		
		//set size of the frame
		frame.setResizable(false);
		frame.pack();

		frame.setLocationRelativeTo(null);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setVisible(true);
	}
}
