package ce1002.A6.s101502523;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;

public class A6 extends Canvas implements Runnable {
	private static JFrame frame = new JFrame("A6"); 
	private int number = 0;	//button上的數字
	private JButton jbt = new JButton("0");
	private int x = 0;  //x軸位置
	private int y = 0;	//y軸位置
	private int dx = 2;	//x軸方向行進距離
	private int dy = 2; //y軸方向行進距離

	public A6() {		
		frame.setLayout(null);
		frame.setSize(600,480);
		frame.setResizable(false);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setVisible(true);
		jbt.setBounds(x, y, 50, 50); //設定button大小與初始位置
		frame.add(jbt);		
		//按button會改變行進方向
		jbt.addActionListener(new ActionListener(){	
			public void actionPerformed(ActionEvent e) {
				int select =  (int)(Math.random()*10)%2;
				if(select==0)
					dx*=(-1);
				else
					dy*=(-1);
				number++;
			}
		});				
		run();
	}
	public static void main(String[] args) {
		new A6();
	}	
	public void run() {	
		while(true) {
			x+=dx;	//下一個x軸位置
			y+=dy;	//下一個y軸位置
			//碰到邊界反彈
			if(x+dx+50>=597) { 
				dx*=(-1);
				number++;
			}
			if(y+dy+50>=460) {
				dy*=(-1);
				number++;
			}
			if(x+dx<=0) {
				dx*=(-1);
				number++;
			}
			if(y+dy<=0) {
				dy*=(-1);
				number++;
			}
			jbt.setText(""+number);	//更新button上的數字
			jbt.setLocation(x, y);	//更新button位置
			frame.repaint();		
			try {
				Thread.sleep(10);
			} catch (InterruptedException k) {
				k.printStackTrace();
			}
		}
	}
}
