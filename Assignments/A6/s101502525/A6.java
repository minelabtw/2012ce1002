package ce1002.A6.s101502525;

import java.awt.event.*;

import javax.swing.*;

public class A6 extends JFrame{
	private JButton button;
	private int x,y,c,bx,by;//location,counter,border
	private double dx,dy,v;//vector,speed
	
	public A6() throws InterruptedException{
		setUI();
		for(;;Thread.sleep((long)(v*3.5)))//each move wait for v*3.5 milliseconds
			run();
	}
	private void setVar(){
		v=6;
		x=(int)(Math.random()*(getWidth()-(button.getWidth()+getInsets().left+getInsets().right)));
		y=(int)(Math.random()*(getHeight()-(button.getHeight()+getInsets().top+getInsets().bottom)));
		randomVector();
	}
	private void setUI(){
		//frame
		setSize(800,600);
		setVisible(true);
		setDefaultCloseOperation(EXIT_ON_CLOSE);
		//button
		button=new JButton(new Integer(c).toString());
		button.setBounds(x,y,50,50);
		button.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent arg0) {
				randomVector();
			}
		});
		//component
		setVar();
		setLayout(null);
		add(button);
	}
	private void run(){
		bx=getWidth()-(button.getWidth()+getInsets().left+getInsets().right);
		by=getHeight()-(button.getHeight()+getInsets().top+getInsets().bottom);//bound
		if(x>=bx&&dx>=0||x<=0&&dx<=0){//left and right bound
			dx*=-1;
			c++;
		}
		if(y>=by&&dy>=0||y<=0&&dy<=0){//top and bottom bound
			dy*=-1;
			c++;
		}
		button.setText(String.valueOf(c));//counter
		button.setLocation(x+=(int)(dx*v),y+=(int)(dy*v));//move
	}
	private void randomVector(){
		double angle=Math.random()*Math.toRadians(360), nx=Math.cos(angle), ny=Math.sin(angle);//new random direction
		if((int)(dx*v)==(int)(nx*v)&&(int)(dy*v)==(int)(ny*v)){//no change
			randomVector();//random again
			return;
		}
		dx=nx;
		dy=ny;
	}
	public static void main(String[] args) throws InterruptedException{
		new A6();
	}
}