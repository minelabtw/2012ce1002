package ce1002.A6.s101502527;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.*;

public class A6 extends JFrame implements ActionListener {
	
	int nowx=(int)(Math.random()*450+1); //按鈕位置
	int nowy=(int)(Math.random()*450+1);
	int num=0; //碰撞幾次和點擊幾次
	double ang=Math.random()*2*Math.PI; //角度
	double movex=(int) (10*(Math.cos(ang))); //x方向移動
	double movey=(int) (10*(Math.sin(ang)));//y方向移動
	JButton b = new JButton("0");
		
	public static void main(String[] args) throws InterruptedException {
		
		A6 frame =new A6();//main
		frame.setLayout(null);
		frame.setSize(516, 538);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setLocationRelativeTo(null);
		frame.setVisible(true);
		while(true){ //開始移動
			Thread.sleep(40);
			frame.move();
		}
	}
	
	public A6() throws InterruptedException{ //按鈕設定
		b.setBounds(nowx,nowy, 50, 50);
		b.addActionListener(this);
		add(b);
	}
	
	private void move(){
		
		b.setLocation(nowx+(int)movex,nowy+(int)movey);//移動
		nowx+=movex;//更新目前位置
		nowy+=movey;
		if(nowx<=0||nowx>=450){  //撞牆
			num++;
			movex*=-1;
			b.setText(Integer.toString(num));
		}
		if(nowy<=0||nowy>=450){
			num++;
			movey*=-1;
			b.setText(Integer.toString(num));
		}
	}	

	public void actionPerformed(ActionEvent e) { //案下去的時候 轉換角度
		ang=Math.random()*2*Math.PI;
		movex=(int) (10*(Math.cos(ang)));
		movey=(int) (10*(Math.sin(ang)));
		num++;
		b.setText(Integer.toString(num));
	}
}




