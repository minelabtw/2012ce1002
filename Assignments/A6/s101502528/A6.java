package ce1002.A6.s101502528;

import java.awt.event.*;
import java.io.IOException;
import java.util.Random;
import java.util.Scanner;
import java.math.*;
import javax.swing.*;

public class A6 extends JFrame implements ActionListener
{
	private double dx,dy;
	private int i=0,x=100,y=100;
	private int out=0;
	private JButton btn;
	private	int width;
	private int height;

	public A6()
	{
		setVisible(true);
		setSize(400,300);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setLayout(null);
		invalidate();

		btn=new JButton();
		btn.setSize(50,50);
		btn.setLocation(0,0);
		btn.addActionListener(this);
		add(btn);

		dic();
		move();
	}

	public static void main(String[] args)
	{
		new A6();
	}

	public void move()
	{
		//碰到四個牆即轉換方向
		while(out==0)
		{
			width=getWidth();
			height=getHeight();
			btn.setLocation(x,y);
			try {
				Thread.sleep(10);
			}
			catch (InterruptedException e)
			{
			e.printStackTrace();
			}
			if(x>=width-55&&dx>0)
			{
				dx*=-1;
				i++;
			}
			if(x<=0&&dx<0)
			{
				dx*=-1;
				i++;
			}
			if(y>=height-80&&dy>0)
			{
				dy*=-1;
				i++;
			}
			if(y<=0&&dy<0)
			{
				dy*=-1;
				i++;
			}
			x=x+(int)(dx*4);
			y=y+(int)(dy*4);
			btn.setText(new Integer(i).toString());
		}
	}

	public void actionPerformed(ActionEvent e)
	{
		dic();
	}

	public void dic()
	{
		Random r=new Random();
		double arc=r.nextDouble()*Math.toRadians(360);
		dx=Math.cos(arc);
		dy=Math.sin(arc);
	}
}
