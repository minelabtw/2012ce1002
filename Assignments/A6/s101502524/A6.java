package ce1002.A6.s101502524;

import java.awt.event.*;
import java.io.IOException;
import java.util.Random;
import java.util.Scanner;
import java.math.*;
import javax.swing.*;

public class A6 extends JFrame implements ActionListener
{
	private double dx,dy;
	private int i=0,x=100,y=100;
	private int out=0;
	private JButton btn;
	private	int w;
	private int h;
	public A6()
	{		
		setVisible(true);//設定使視窗可見
		setSize(400,300);//設定視窗大小
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setLayout(null);
		revalidate();
		
		btn=new JButton();
		btn.setSize(50,50);//按鈕大小
		btn.setLocation(0,0);//按鈕位置
		btn.addActionListener(this);
		add(btn);//新增按鈕
		
		ch();
		move();
	}
	public void ch()//隨機產生新的角度
	{
		Random r=new Random();
		double arc=r.nextDouble()*Math.toRadians(360);
		dx=Math.cos(arc);
		dy=Math.sin(arc);
	}
	public static void main(String[] args)
	{
		new A6();
	}
	public void move()
	{
		while(out==0)//連續移動，間隔時間10毫秒
		{
			w=getWidth();
			h=getHeight();
			btn.setLocation(x,y);
			try {//間格10毫秒
				Thread.sleep(10);
			} 
			catch (InterruptedException e) 
			{
			e.printStackTrace();
			}
			//碰掉邊緣反彈，並將計數器加一
			if(x>=w-55&&dx>0)
			{
				dx*=-1;
				i++;
			}
			if(x<=0&&dx<0)
			{
				dx*=-1;
				i++;
			}
			if(y>=h-80&&dy>0)
			{
				dy*=-1;
				i++;
			}
			if(y<=0&&dy<0)
			{
				dy*=-1;
				i++;
			}
			//移動
			x=x+(int)(dx*3);
			y=y+(int)(dy*3);
			
			btn.setText(new Integer(i).toString());
		}
	}
	public void actionPerformed(ActionEvent e)
	{
		ch();
	}
}
