package ce1002.A6.s101502513;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class A6 extends JFrame implements ActionListener {
	private static int mx = 150, dx = 1;
	private static int my = 120, dy = 1;
	private static int num = 0;
	private static JButton move = new JButton("0");
	
	public A6(String title) {
		 super(title);
		 setLayout(null);
		 move.addActionListener(this);
		 add(move);
	}
	
	public static void main(String[] args) throws InterruptedException {
		A6 frame = new A6("A6");
		frame.setSize(600, 450);
		frame.setLocation(120, 80);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setVisible(true);
		
		while(true) { //object will move constantly
			move.setBounds(mx, my, 60, 60);
			mx += dx; //move
			my += dy; //move
			
			//Object will rebound when it collides with frames
			if( mx <= 0 || mx >= frame.getWidth() - 70 ) {
				dx = -dx;
				num++;
				move.setText(String.valueOf(num)); //the number of button will increase
			}
			if( my <= 0 || my >= frame.getHeight() - 95 ) {
				dy = -dy;
				num++;
				move.setText(String.valueOf(num)); //the number of button will increase
			}
			
			Thread.sleep(10); //the program will loop every 10 ms
		}
	}
	
	public void actionPerformed(ActionEvent e) { //move randomly after click
		double r = Math.random()*(Math.PI * 2); //random angle
		dx = (int)(4*Math.cos(r));
		dy = (int)(4*Math.sin(r));
	}
}
