//A6 by J
//set location; sleep
package ce1002.A6.s101502020;

import java.awt.*;
import java.awt.event.*;
public class A6{
	//variables claimed here
	Frame frame;
	Button btn2;
	int x,y,c;
	double vx,vy,speed;
	
	public A6() throws InterruptedException{
		setUI();
		run();
	}
	private void setUI() {
		//variables
		speed=3;
		randomVector();
		x = 300;
		y = 100 ;
		c=0;
		
		//frame info
		frame= new Frame("A6");
		frame.setSize(480,480);
		frame.addWindowListener(new AdapterDemo());
		frame.setLayout(null);
		
		//button info
		btn2 = new Button("b2");
		btn2.setSize(100,100);
		btn2.setLocation(300,300);
		btn2.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent arg0) {
				randomVector();
			}
		});
		
		//components
		frame.add(btn2);
		frame.setVisible(true);
	}
	private void run() throws InterruptedException {
		while (true) {	
			btn2.setLocation(x,y);			
			//bounce if statements
			if ( y<=frame.getInsets().top-frame.getInsets().bottom &&vy<0|| y>=frame.getHeight()-100&&vy>0 ) {
				vy=-vy;
				c++;
			}
			if ( x<=0 &&vx<0|| x>=frame.getWidth()-100 &&vx>0) {
				vx=-vx;
				c++;
			}
			x+=(int)(vx*speed) ; 
			y+=(int)(vy*speed) ;		//time is 1
			
			btn2.setLabel(String.valueOf(c));
			Thread.sleep(10);
		}
	}
	private void randomVector(){
		double angle=Math.random()*360;
		vx=Math.cos(Math.toRadians(angle));
		vy=Math.sin(Math.toRadians(angle));
	}		//unit vector in x & y direction
	public static void main(String[] args) throws InterruptedException {		
		new A6();
	}
}

class AdapterDemo extends WindowAdapter {
	public void windowClosing(WindowEvent e) {
		System.exit(0);
	}
}
