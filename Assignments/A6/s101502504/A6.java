package ce1002.A6.s101502504;
import java.awt.event.*;
import java.awt.*;
import javax.swing.*;

public class A6 extends JFrame implements ActionListener 
{
	private static int x=11,y=11,count=0;//count is the number 
	private static double ix=0,iy=0;
	private static JButton button = new JButton();//set button
	
	public A6(String pTitle)
	{
		super(pTitle);
		setLayout(null);
		button.setBounds(x,y,60,60);//set the button on frame
		add(button);//add button b
		button.addActionListener(this);//do action when push button
	}

	public static void main(String args[])
	{
		A6 frame = new A6("A6");
		frame.setSize(575, 600);//set size
		frame.setLocationRelativeTo(null);
		frame.setDefaultCloseOperation(EXIT_ON_CLOSE);
		frame.setVisible(true);

		double r = Math.random()*3.14;//randomly
		ix=3*Math.cos(r);
		iy=3*Math.sin(r);
			
		while( ix!=0 && iy!=0 )
		{
			x+=ix;
			y+=iy;
			button.setLocation((int)x,(int)y);
			
			if( button.getLocation().x>=500 || button.getLocation().x<=0 )//touch the side
			{
				ix*=-1;
				count++;
			}
			
			if( button.getLocation().y>=500 || button.getLocation().y<=0 )//touch the side
			{
				iy*=-1;
				count++;
			}
			
			button.setText(Integer.toString(count));//change the number on button
			
			try
			{
				Thread.sleep(17);
			}
			catch (InterruptedException e)
			{
				e.printStackTrace();
			}
			
		}
	}			
	
	public void actionPerformed(ActionEvent e)//change the direction randomly
	{
		double r = Math.random()*3.14;
		ix = 3*Math.cos(r);
		iy = 3*Math.sin(r);
	}
	
}