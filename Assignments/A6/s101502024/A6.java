package ce1002.A6.s101502024;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Random;
import javax.swing.*;

public class A6 extends JFrame implements ActionListener {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private static int j, i, x, y, counts = -2;
	private static JButton btn = new JButton();

	public static void main(String[] args) {//the main function
		A6 huehuehue = new A6("huehuehue");
		huehuehue.setSize(503, 425);
		huehuehue.setLocation(100,100);
		huehuehue.setVisible(true);
		do {//the way the cube moves
			btn.setName(" " + counts +" ");

			x -= i;//i & j for directions ; x & y  for locations
			y -= j;

			if (x >= huehuehue.getWidth() - (btn.getWidth() + 10)) {
				counts++;//to count the times the cube touches the wall
				x = huehuehue.getWidth() - (btn.getWidth() + 10); 
				i = 1;
			}
			if (y >= huehuehue.getHeight() - (btn.getHeight() + 30)) { 
				counts++;
				y = huehuehue.getHeight() - (btn.getHeight() + 30); 
				j = 1;
			}
			if (x <= 0) {
				counts++;
				x = 0;
				i = -1;
			}
			if (y <= 0) {
				counts++;
				y = 0;
				j = -1;
			}

			btn.setText(" "+ counts + "");
			btn.setLocation(x, y);
			try {//from Hint
				Thread.sleep(10);

			} 
			catch (InterruptedException e1) {
				e1.printStackTrace();
			}
		} while (true);
	}

	public A6(String pTitle) {//set button details
		super(pTitle);
		setLayout(null);
		btn.setSize(77, 77);
		btn.addActionListener(this);
		add(btn);
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		// TODO Auto-generated method stub
		double r=Math.random()*3.14159;//謝謝助教!
		i = (int) (3*Math.cos(r));
		j = (int) (3*Math.sin(r));
	}
}
