package ce1002.A6.s101502505;
import javax.swing.*;
import java.util.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class A6 extends TimerTask{
	static JButton a6 = new JButton("0"); 
	double degree = (Math.random() * 3.14);
	int ve=3;
	int vex = (int)( ve * Math.cos(degree));//x向量為cos
	int vey = (int)( ve * Math.sin(degree));//y向量為sin
	
	
	public A6(){
		movemove moveya = new movemove();
	    a6.addActionListener(moveya);
	}

	public static void main(String[] args)
	{
		JFrame frame = new JFrame("A6");//製造視窗
		frame.setTitle("A6");//主題
		frame.setSize(300,300);//大小
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);//關閉視窗
		frame.setVisible(true);//顯示
		frame.setLayout(null);//版面配置
		frame.add(a6);
		a6.setBounds(0,0,50,50);
	}
	int num = 0;
	class movemove implements ActionListener {
		public void actionPerformed(ActionEvent e) {
			num++;
			a6.setText(new String("" + num));
			degree = (Math.random() * 3.14);
			vex = (int)(ve * Math.cos(degree));
			vey = (int)(ve * Math.sin(degree));
		}
	}
}
