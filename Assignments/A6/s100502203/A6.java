package ce1002.A6.s100502203;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.*;

public class A6 extends JFrame {
	static JButton move = new JButton("0");
	static double theta = Math.random() * 360;	// use theta to note degree
	static double dirX = 20 * Math.sin(Math.toRadians(theta));	// x, y = rcos, rsin
	static double dirY = 20 * Math.cos(Math.toRadians(theta));
	static final int width = 600, height = 600;
	static final int jbtSize = 50;

	public A6() {
		setLayout(null);	// create a null frame
		setSize(width, height);
		move.setBounds(width / 2, height / 2, jbtSize, jbtSize);
		setTitle("A6");
		add(move);
		setLocationRelativeTo(null);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setVisible(true);
		
		move.addActionListener(new ActionListener() {	// push button -> create another direction
			public void actionPerformed(ActionEvent e) {
				theta = Math.random() * 360;
				dirX = 20 * Math.sin(Math.toRadians(theta));
				dirY = 20 * Math.cos(Math.toRadians(theta));
			}
		});
	}

	public static void main(String[] args) {
		A6 frame = new A6();
		double x = move.getX();
		double y = move.getY();
		int count = 0;

		try {
			while (true) {
				x += dirX;
				y += dirY;
				if (x >= width - (int) (jbtSize * 1.5) || x <= 0) {	// margin x
					dirX = -dirX;
					move.setText(Integer.toString(++count));
				}
				if (y >= height - (int) (jbtSize * 1.75) || y <= 0) {	// margin y
					dirY = -dirY;
					move.setText(Integer.toString(++count));
				}
				move.setLocation((int)x, (int)y);
				Thread.sleep(100);	// sleep time 100ms
			}
		} catch (InterruptedException ex) {
		}
	}
}
