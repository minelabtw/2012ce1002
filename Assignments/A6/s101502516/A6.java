package ce1002.A6.s101502516;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.*;
import java.util.Random;

public class A6 extends JFrame implements ActionListener {
	static double x = 200, y = 200;
	static double r = Math.random() * 100;
	static double dx = Math.cos(r);
	static double dy = Math.sin(r);
	static int count = 0;
	static JButton move = new JButton( "0" );

	public static void main(String[] args){ // 設定視窗大小及跳出位子
		A6 hw = new A6( "A6" );
		hw.setSize( 400, 400 );
		hw.setLocationRelativeTo( null );
		hw.setVisible( true );
		
		int right = hw.getInsets().right; // 找出邊框大小
		int bottom = hw.getInsets().bottom;
		int top = hw.getInsets().top;
		int left = hw.getInsets().left;
		
		int count = 0;

		while( true ) // 改變按鈕位置，且做特殊處理
		{
			move.setLocation( (int)x, (int)y );
			try{
				Thread.sleep( 5 );
			}catch ( InterruptedException e )
			{
				e.printStackTrace();
			}

			if ( x <= 0 || x >= 400 - 60 - right - left )
			{
				dx *= -1;
				count++;
				String c = String.valueOf( count );
				move.setLabel( c );
			}
			if ( y <= 0 || y >= 400 - 60 - top - bottom )
			{
				dy *= -1;
				count++;
				String c = String.valueOf( count );
				move.setLabel( c );
			}
			x += dx;
			y += dy;
		}
	}
		
	public A6(String pTitle){ // 按鍵設定
		super(pTitle);
		setLayout(null);
		move.setSize( 60, 60 );
		move.addActionListener(this);
		add( move );
	}
	public void actionPerformed(ActionEvent e) { // 按鍵動作設定
		r = Math.random() * 100;
		dx = Math.cos(r);
		dy = Math.sin(r);
	}
}
