package ce1002.A6.s101502517;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.*;

public class A6 extends JFrame implements ActionListener 
{
	public static JButton btn = new JButton("0");//建立BUTTON
	public static double r=Math.random()*3.1415926;//亂數
	public static double dx=Math.cos(r);//取COS() X方向向量
	public static double dy=Math.sin(r);//取SIN() Y方向向量
 
	public static void main(String[] args) {

		A6 hw = new A6("A6");
		hw.setSize(500, 500);//設定大小
		hw.setLocation(0,0);
		hw.setVisible(true);	
		
		double x=250,y=250;
		int numClicks = 0;//記數
		String num;//記數字串
				num=Integer.toString(numClicks);//數字轉字串
		while(true){//無窮迴圈
			x=x+dx;
			y=y+dy;
			btn.setLocation((int)x, (int)y);
			//放緩程式速度
			try {
				Thread.sleep(8);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			if(x<=0||x>=hw.getWidth()-65){//越過左右兩邊邊界 X方向轉向
				dx=(-1)*dx;
				numClicks++;
				num=Integer.toString(numClicks);
				btn.setText(num);
			}
			if(y<=0||y>=hw.getHeight()-85){//越過上下邊界 Y方向轉向
				dy=(-1)*dy;
				numClicks++;
				num=Integer.toString(numClicks);
				btn.setText(num);
			}
			
		}	
	}
	public A6(String pTitle) {
		super(pTitle);
		setLayout(null);// 不使用版面配置
		btn.setBounds(0, 0, 50, 50);
		btn.addActionListener(this);
		add(btn);
	}
	public void actionPerformed(ActionEvent e) {//點擊按鈕換方向
		r=Math.random()*3.1415926;
		dx=Math.cos(r);
		dy=Math.sin(r);
	}

}