package ce1002.A6.s101502002;
import java.awt.event.*;
import java.util.Timer;
import java.util.TimerTask;
import javax.swing.*;
public class A6 extends JFrame implements ActionListener{
	public int num = 0; //按鈕上顯示的數字
	public JButton move = new JButton(""+num); //按鈕
	public int m=getWidth()/2,n=getHeight()/2; //原始位置
	public int size=60; //按鈕的大小
	public int x =2,y=3; //初始移動向量

	public A6(){
		move.setBounds( m,n,size,size); //設定按鈕的位置及大小
		move.addActionListener(this); //按下按鈕可以做事
		this.add (move); //加入按鈕
		Timer time = new Timer(); 
        time.schedule(new moving(), 1000, 20);
	}
	class moving extends TimerTask { //每個時間間隔要做的事
        public void run() {
				if(m+x<0||m+x+size>getWidth()){ //碰到左右邊界的話
					x=-x; //x方向往反方向
					num++; //按鈕上的數字加一
					move.setText(""+num);
				}
				else if(n+y<0||n+y+size>getHeight()){ //碰到上下邊界
					y=-y; //y方向往反方向
					num++; //按鈕上數字加一
					move.setText(""+num);
				}
				m=m+x; //每次移動一點
				n=n+y;
				move.setBounds(m, n, size, size); //設定移動完的位置
		}
		
	}
	public static void main(String[]args){ //主程式
		A6 frame = new A6();
		frame.setTitle("A6"); //設狀態列
		frame.setSize(650,650); //設框框大小
		frame.setLocationRelativeTo(null); //框框在螢幕正中間
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE); //按叉叉關閉
		frame.setVisible(true); //可以被看見
	}
	@Override
	public void actionPerformed(ActionEvent e) { //按下按鈕後
		x=(int)(Math.random()*4+1); //重新給定xy一個1~5任意的值
		y= (int)(Math.random()*4+1);// TODO Auto-generated method stub	
	}
}