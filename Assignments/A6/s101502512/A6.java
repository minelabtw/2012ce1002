package ce1002.A6.s101502512;

import java.awt.Color;
import java.awt.Font;
import java.awt.event.*;
import javax.swing.*;

public class A6 extends JFrame implements ActionListener {
	public static int times = 0;	// 撞牆幾次
	public static JButton btn = new JButton("" + times);
	public static int x = 300; 		// 按鈕的初始X座標
	public static int y = 300; 		// 按鈕的初始Y座標
	public static int i = 1; 		// X座標的變化量
	public static int j = 1; 		// Y座標的變化量

	public A6(String Title) {
		super(Title);
		setLayout(null);
		btn.setBounds(x, y, 100, 100);
		btn.addActionListener(this);
		Font font1 = new Font("SansSerif", Font.LAYOUT_RIGHT_TO_LEFT, 20);
		btn.setFont(font1);
		btn.setForeground(Color.white);
		btn.setBackground(Color.black);
		add(btn);

	}

	public static void main(String[] args) {
		A6 frame = new A6("A6");
		frame.setSize(600, 500);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setVisible(true);
		frame.setLocationRelativeTo(null);
		System.out.println(frame.getInsets()); // 找出邊框邊界

		while (true) {
			btn.setName("" + times);
			try {
				Thread.sleep(5);

			} catch (InterruptedException e1) {
				e1.printStackTrace();
			}

			x += i;
			y += j;

			if (x >= frame.getWidth() - (btn.getWidth() + 8)) { 	// 撞到右邊的牆
				times++;
				x = frame.getWidth() - (btn.getWidth() + 8); 		// 回歸邊線的值
				i = -1*i;
			}
			if (y >= frame.getHeight() - (btn.getHeight() + 30)) { 	// 撞到下面的牆
				times++;
				y = frame.getHeight() - (btn.getHeight() + 30); 	// 回歸邊線的值
				j = -1*j;
			}
			if (x <= 0) { 	// 撞到左邊的牆
				times++;
				x = 0; 		// 回歸邊線的值
				i = -1*i;
			}
			if (y <= 0) { 	// 撞到上面的牆
				times++;
				y = 0; 		// 回歸邊線的值
				j = -1*j;
			}

			btn.setText("" + times);
			btn.setLocation(x, y);
		}
	}

	public void actionPerformed(ActionEvent e) {// 隨機變換角度
		double r = Math.random() * (Math.PI * 2);
		j = (int) (3 * Math.sin(r));
		i = (int) (3 * Math.cos(r));
	}

}
