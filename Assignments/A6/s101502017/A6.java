package ce1002.A6.s101502017;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.*;

//教學http://openhome.cc/Gossip/ComputerGraphics/SkeletonOfAnimation.htm
public class A6 extends JFrame implements ActionListener, Runnable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	
	private JButton button;
	int count = -2;//起點設(0,0)所以-2;
	String s = String.valueOf(count);
	int dx, dy, i = 1, j = 1;;

	public A6() {
		setTitle("A6");
		button = new JButton();
		button.addActionListener(this);
		button.setText(s);
		button.setBounds(dx, dy, 50, 50);
		setLayout(null);
		add(button);
	}

	public static void main(String[] args) {
		A6 frame = new A6();
		frame.setSize(400, 400);
		frame.setLocale(null);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setVisible(true);
		frame.run();
	}

	public void run() {
		while (true) {
			if (dx == 350) {
				i = -1;
				count++;
			}
			if (dy == 330) {
				j = -1;
				count++;
			}
			if (dx == 0) {
				i = 1;
				count++;
			}
			if (dy == 0) {
				j = 1;
				count++;
			}
			dx += i;
			dy += j;
			button.setText(count+"");//int的變數 + "" 強迫其變字串
			button.setBounds(dx, dy, 50, 50);
			this.repaint(); // 重繪畫面

			// 執行緒暫停 10毫秒
			try {
				Thread.sleep(10); // 避免Busy loop
			} catch (InterruptedException e) {
				// 例外處理
			}
		}
	}

	public void actionPerformed(ActionEvent e) {
		double r = Math.random() * 3.14159;
		i =(int) ( Math.cos(r)+1);
		j =(int) ( Math.sin(r)+1);
	}
}
