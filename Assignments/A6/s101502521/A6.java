package ce1002.A6.s101502521;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;

public class A6 {
	int dx;
	int dy;
	int count;
	JFrame frame;
	JButton btn;
	
	public static void main(String[] args) throws InterruptedException
	{
		A6 ins = new A6();
		
		while(true)
		{
			int newX=ins.btn.getLocation().x+ins.dx;
			int newY=ins.btn.getLocation().y+ins.dy;
			ins.btn.setLocation(newX, newY);
			if(newX<0)
			{
				ins.count++;
				ins.btn.setText(Integer.toString(ins.count));
				ins.dx = -ins.dx;
			}
			if(newY<0)
			{
				ins.count++;
				ins.btn.setText(Integer.toString(ins.count));
				ins.dy = -ins.dy;
			}
			if(newX+ins.btn.getBounds().width>ins.frame.getContentPane().getWidth())
			{
				ins.count++;
				ins.btn.setText(Integer.toString(ins.count));
				ins.dx = -ins.dx;
			}
			if(newY+ins.btn.getHeight()>ins.frame.getContentPane().getHeight())
			{
				ins.count++;
				ins.btn.setText(Integer.toString(ins.count));
				ins.dy = -ins.dy;
			}
			Thread.sleep(30);
		}
		//end of main
	}
	
	A6()
	{
		count=0;
		frame = new JFrame("A6");
		frame.setSize(600,500);
		btn = new JButton("0");
		frame.setLayout(null);
		btn.setBounds(250, 250, 60, 60);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		btn.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent arg0) {
				// TODO Auto-generated method stub
				randDxDy();
			}
		});
		frame.add(btn);
		frame.setVisible(true);
		randDxDy();
	}
	/**
	 * 
	 */
	public void randDxDy()
	{
		do
		{
			double t = Math.random()*10;
			dx = (int)(Math.cos(t)*5);
			dy = (int)(Math.sin(t)*5);
		}while(dx==0||dy==0);
	}
}
