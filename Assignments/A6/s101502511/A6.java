package ce1002.A6.s101502511;

import javax.swing.*;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.*;
import java.util.Timer;

public class A6 extends TimerTask implements ActionListener {

	private double x = 350;// x座標
	private double y = 350;// y座標
	private double r = Math.random() * 3.1415926;// get random at pi
	private double i = Math.cos(r);// vector i
	private double j = Math.sin(r);// vector j
	private int c = 0;// 碰撞次數
	private static JButton btn = new JButton();//

	public A6() {
		btn.setSize(50, 50);
		btn.addActionListener(this);
		btn.setLocation(350, 350);
	}

	public static void main(String[] args) {

		Timer timer = new Timer();// new timer

		JFrame frame = new JFrame();
		frame.setLayout(null);
		frame.setTitle("A6");
		frame.setSize(700, 700);
		frame.setLocationRelativeTo(null);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setVisible(true);
		frame.add(btn);
		timer.schedule(new A6(), 1, 8);// timer schedule
	}

	public void actionPerformed(ActionEvent e) {// 點一次換方向
		r = Math.random() * 3.1415926;
		i = Math.cos(r);
		j = Math.sin(r);
	}

	public void run() {// run timer
		String s;
		if (x >= 634) {// 超過邊界
			i = -Math.abs(Math.cos(r));// 負值
			c++;
		}
		if (x <= 0) {
			i = Math.abs(Math.cos(r));
			c++;
		}
		if (y >= 610) {
			j = -Math.abs(Math.sin(r));
			c++;
		}
		if (y <= 0) {
			j = Math.abs(Math.sin(r));
			c++;
		}
		s = Integer.toString(c);// int轉換成string
		x = x + i;
		y = y + j;
		btn.setText(s);// button的名子
		btn.setLocation((int) x, (int) y);// 移動位置
	}
}
