package ce1002.A3.s101502513;

import java.util.*;

public class A3 {
	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);
		int num;
		int[][] matrixA = new int[3][3];
		int[][] matrixB = {
				{ 7, 5, 3 },
				{ 1, 4, 9 },
				{ 4, 2, 6 }
		};
		
		System.out.println("Input matrix A: ");
		for( int i = 0; i < 3; i++ ) //input matrixA
			for( int j = 0; j < 3; j++ ) {
				num = input.nextInt();
				matrixA[i][j] = num;
			}
		System.out.println("\nAfter: ");
		multiplication(matrixA, matrixB); //function that matrixA * matrixB
	}
	
	public static void multiplication(int[][] matrixA, int[][] matrixB) {
		int[][] mult = new int[3][3];
		
		for( int RowA = 0; RowA < 3; RowA++ ) //run Row of A third
			for( int ColB = 0; ColB < 3; ColB++ ) { //run Column of B second
				int Sum = 0; //initialize Sum
				
				for( int RowB = 0; RowB < 3; RowB++ ) { //run Row of B first
					int NumA = matrixA[RowA][RowB]; //RowB is same as Column of A
					int NumB = matrixB[RowB][ColB];
					Sum += NumA * NumB;
				}
					mult[RowA][ColB] = Sum; //save Sum into mult
			}
		
		for( int x = 0; x < 3; x++ ) { //output result of matrixA * matrixB
			for( int y = 0; y < 3; y++ )
				System.out.print(mult[x][y] + " ");
			System.out.println();
		}
	}
}
