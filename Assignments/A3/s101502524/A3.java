package ce1002.A3.s101502524;

import java.util.Scanner;

public class A3 {
	public static void main(String[] args)
	{
		Scanner input=new Scanner(System.in);
		int[] B={7,5,3,1,4,9,4,2,6};
		int[] a=new int[9];
		System.out.println("Input matrix A:");
		for(int i=0;i<9;i++)//輸入陣列a
		{
			a[i]=input.nextInt();
		}
		System.out.println();
		System.out.println();
		System.out.println("After:");
		multiplication(a,B);//呼叫函式
	}
	public static void multiplication(int[] x,int[] y)
	{
		int save=0;
		for(int s=0;s<3;s++)//計算第一行的三個數字，並輸出
		{
			for(int q=0,w=0;q<3;q++,w=w+3)
			{
				save=save+x[q]*y[w+s];
			}
			System.out.print(save+" ");
			save=0;
		}
		System.out.println();
		for(int s=0;s<3;s++)//計算第二行的三個數字，並輸出
		{
			for(int q=3,w=0;q<6;q++,w=w+3)
			{
				save=save+x[q]*y[w+s];
			}
			System.out.print(save+" ");
			save=0;
		}
		System.out.println();
		save=0;
		for(int s=0;s<3;s++)//計算第三行的三個數字，並輸出
		{
			for(int q=6,w=0;q<9;q++,w=w+3)
			{
				save=save+x[q]*y[w+s];
			}
			System.out.print(save+" ");
			save=0;
		}
	}
}
