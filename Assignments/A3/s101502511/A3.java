package ce1002.A3.s101502511;

import java.util.*;

public class A3 {

	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);
		int[][] a_matrix = new int[3][3];
		int[][] c_matrix = new int[3][3];

		System.out.println("Input matrix A:");

		for (int i = 0; i < 3; i++) {//input matrix 
			for (int j = 0; j < 3; j++)
				a_matrix[i][j] = scanner.nextInt();
		}
	
		c_matrix = multiplication(a_matrix);/*return C_matrix of multiplication function*/
		
		System.out.println();
		System.out.println("After: ");
		for (int i = 0; i < 3; i++) {//print the matrix
			for (int j = 0; j < 3; j++)
				System.out.print(c_matrix[i][j]+" ");
			System.out.println();
			}
	}
	public static int[][] multiplication(int[][] A_matrix) {
		int[][] B_matrix = { { 7, 5, 3 }, { 1, 4, 9 }, { 4, 2, 6 } };
		int[][] C_matrix = new int[3][3];

		for (int i = 0; i < 3; i++) {//換列
			for (int k = 0; k < 3; k++) {//換行
				for (int j = 0; j < 3; j++) {//計算每一個的值
					C_matrix[i][k] += A_matrix[i][j] * B_matrix[j][k];
				}
			}
		}
		return C_matrix;//return matrix
	}
}
