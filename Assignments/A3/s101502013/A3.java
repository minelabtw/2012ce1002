package ce1002.A3.s101502013;
import java.util.Scanner;
public class A3 {
	public static void main(String[] args){
		Scanner haha = new Scanner(System.in);
		int[][] input,answer,B =  {{7,5,3},{1,4,9},{4,2,6}};
		input = new int[3][3];
		System.out.println("Input matrix A:");
		for (int i=0;i<3;i++)//輸入矩陣A
			for (int j=0;j<3;j++)
				input[i][j] = haha.nextInt();
		answer = multiplication(input,B);
		System.out.println("After:");
		for (int i=0;i<3;i++)//print答案
			{
			for (int j=0;j<3;j++)
				System.out.print(answer[i][j] + " ");
			System.out.println();
			}
	}
	public static int[][] multiplication (int[][] input, int[][] B)
	{
		int[][] answer;
		answer = new int[3][3];
		for (int i=0;i<3;i++) 
				for (int j=0;j<3;j++)//3*3矩陣一格一格運算[0][0]->[0][1]->[0][2]->[1][0]……
					for (int k=0;k<3;k++)//將左邊矩陣的橫排與右邊矩陣的直排相乘壘加(矩陣乘法運算)
						answer[i][j] = answer[i][j] + (input[i][k] * B[k][j]);
		return answer;
	}
}