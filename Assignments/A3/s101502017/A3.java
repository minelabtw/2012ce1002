package ce1002A3.s101502017;

import java.util.*;

public class A3 {
	public static void main(String[] args){
		Scanner x = new Scanner(System.in);
		int[][] a;
		a = new int[3][3];
		System.out.println("Input matrix A:");
		for(int i = 0;i < 3; i++){
			for(int j = 0;j < 3; j++){
				a[i][j]=x.nextInt();
			}
		}
		multiplication(a);
		x.close();
	}
	public static void multiplication(int[][] a){
		int b[][] = { {7,5,3}, {1,4,9}, {4, 2,6}};
		
		int c[][] = new int[3][3];
		
		for (int m = 0; m < 3; m++){
			for (int p = 0; p < 3; p++){
			    for (int n = 0; n < 3; n++){
			     c[m][p] = c[m][p] + a[m][n] * b[n][p];
			    }
			}
		}
		System.out.println("After:");
		
		for (int i = 0; i < 3; i++) {
			for (int j = 0; j < 3; j++) {
				System.out.print(c[i][j] + ",");
			}
			System.out.println("");
		}
	}
}
