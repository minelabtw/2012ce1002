package ce1002.A3.s101502509;

import java.util.Scanner;

public class A3 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner input = new Scanner(System.in);
		int[][] matrixa = new int[3][3];		
		System.out.println("Input matrix A:");
		for(int i = 0; i < matrixa.length; i++)  {		//input matrixa
			for(int j = 0; j < matrixa[0].length; j++) 
				matrixa[i][j] = input.nextInt();
		}
		System.out.println("\nAfter:");		
		multiplication(matrixa);		//calculate matrixa * matrixb and print the result
	}
	private static void multiplication(int refa[][]) {
		int[][] matrixb = { {7, 5, 3},		//fix matrixb
							{1, 4, 9},
							{4, 2, 6} };
		int[][] answer = new int[3][3];		//set the arraysize of answer
		for(int row = 0; row < answer.length; row++)	
			for(int line = 0; line < answer[0].length; line++) {
				for(int interval = 0; interval < answer[0].length; interval++) {
					answer[row][line] += refa[row][interval] * matrixb[interval][line];   //calculate the value of answer[row][line]
				}
				System.out.print(answer[row][line] + " ");	//print answer[row][line]
				if(line == answer[0].length - 1) {	//change line 
					System.out.println();
				}
			}
	}
}