package ce1002.A3.s101502516;
import java.util.Scanner;
public class A3 {
	public static void main(String[] args){
		int [][] b = { {7,5,3}, {1,4,9}, {4,2,6} };
		int [][] a = { {0,0,0}, {0,0,0}, {0,0,0} };
		int x;
		Scanner input = new Scanner(System.in);
		System.out.println( "Input matrix A: " );
		
		for ( int i = 0; i < 3; i++ ) // 輸入A矩陣
		{
			for ( int j = 0; j < 3; j++ )
			{
				x = input.nextInt();
				a[i][j] = x;
			}
		}
		
		int [][] c = { {0,0,0}, {0,0,0}, {0,0,0} };
		for ( int i = 0; i < 3; i++ ) // 相乘
		{
			for ( int j = 0; j < 3; j++ )
			{
				for ( int k = 0; k < 3; k++ )
				{
					c[i][j] += a[i][k] * b[k][j]; 
				}
			}
		}
		
		System.out.println( " " );
		System.out.println( "After: " );
		for( int i = 0; i < 3; i++ ) // 輸出結果
		{
			for ( int j = 0; j < 3; j++ )
			{
				System.out.print( c[i][j] + " " );
			}
			System.out.println( " " );
		}
	}
}
