package ce1002.A3.s101502510;

import java.util.Scanner;

public class A3 {
	public static void multiplication(int[][] Array) {

		Scanner input = new Scanner(System.in);
		System.out.println("Input matrix A: ");
		int[][] B = { { 7, 5, 3 }, { 1, 4, 9 }, { 4, 2, 6 } };
		int[][] end = new int[3][3];
		int x = 0, y = 0, z = 0, m = 0;

		for (int i = 0; i < 3; i++)//輸入A陣列
			for (int j = 0; j < 3; j++) {
				Array[i][j] = input.nextInt();
			}

		for (int i = 0; i < 3; i++) {
			for (int j = 0; j < 3; j++) {
				x += Array[i][j] * B[j][m];
				y += Array[i][j] * B[j][m + 1];
				z += Array[i][j] * B[j][m + 2];
			}
			end[i][m] = x;//記錄陣列相乘後第i列第1個數
			end[i][m + 1] = y;//記錄陣列相乘後第i列第2個數
			end[i][m + 2] = z;//記錄陣列相乘後第i列第3個數
			x = y = z = 0;//重新計算故歸零
		}

		System.out.println("\nAfter: ");
		for (int i = 0; i < 3; i++) {
			for (int j = 0; j < 3; j++) {
				System.out.print(end[i][j] + " ");
			}
			System.out.println();
		}
	}

	public static void main(String args[]) {
		int[][] array = new int[3][3];
		multiplication(array);// 傳入一個空陣列到multiplication函式內
	}
}
