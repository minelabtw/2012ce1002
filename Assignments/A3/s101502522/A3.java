package ce1002.A3.s101502522;
import java.util.Scanner;
public class A3 {
	public static void main(String[] args)
	{
		Scanner input=new Scanner(System.in);
		int[][] a=new int[3][3];
		int[][] b={{7,5,3},{1,4,9},{4,2,6}};
		int[][] c=new int[3][3];
		
		System.out.println("Input matrix A:");
		for(int i=0; i<3 ; i++)
			for(int j=0 ; j<3 ;j++)
				a[i][j]=input.nextInt();
		System.out.println();
		
		System.out.println("After:");
		
		multiplication(a,b,c);//計算相乘後的陣列
		
		for(int i=0 ; i<3 ;i++)
		{
			for(int j=0 ;j<3 ; j++)
				System.out.print(c[i][j]+" ");
			System.out.println();		
		}
	}
	public static void multiplication(int[][] x, int[][] y, int[][] z) //矩陣相乘函式
	{
		for(int i=0 ; i<3 ;i++)
			for(int j=0 ; j<3 ;j++)
				for(int k=0 ; k<3 ; k++)
					z[i][j]=z[i][j]+x[i][k]*y[k][j];  
	}
}
