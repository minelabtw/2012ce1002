package ce1002.A3.s101502514;
import java.util.Scanner;
public class A3 {
	static int a[][]=new int[3][3];
	static int b[][]={{7,5,3},
			   		 {1,4,9},
			   		 {4,2,6},};
	static int c[][]=new int[3][3];
	public static void multiplication( ){//相乘
		int k1=0;
		for(int i=0;3>i;i++){
			int l=0;
			int ou=0;
			for(int j=0;3>j;j++){	
				ou=ou+a[i][j]*b[l][k1];
				l++;
			}
			c[i][k1]=ou;//將乘玩的值存進去另一個陣列
		}
		
		int k2=1;
		for(int i=0;3>i;i++){
			int l=0;
			int ou=0;
			for(int j=0;3>j;j++){	
				ou=ou+a[i][j]*b[l][k2];
				l++;
			}
			c[i][k2]=ou;//將乘玩的值存進去另一個陣列
		}
		
		int k3=2;
		for(int i=0;3>i;i++){
			int l=0;
			int ou=0;
			for(int j=0;3>j;j++){	
				ou=ou+a[i][j]*b[l][k3];
				l++;
			}
			c[i][k3]=ou;//將乘玩的值存進去另一個陣列
		}
	}
	
	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);
		int in;
		System.out.println("Input matrix A: ");
		for(int i=0;3>i;i++){//輸入陣列
			for(int j=0;3>j;j++){
				in=input.nextInt();
				a[i][j]=in;
			}
		}
		
		multiplication();//相乘
		
		System.out.println("After: ");
		for(int i=0;3>i;i++){
			for(int j=0;3>j;j++){
				System.out.print(c[i][j]+" ");//將處理完的鎮列印出
			}
			System.out.println();
		}
	}
}
