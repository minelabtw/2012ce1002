package ce1002.A3.s101502512;

import java.util.Scanner;

public class A3 {
	public static void multiplication(int[][] x) {
		int[][] B = { { 7, 5, 3 }, { 1, 4, 9 }, { 4, 2, 6 } };
		int sum;//輸出的數
		for (int k = 0; k < 3; k++) {//換排計算
			for (int i = 0; i < 3; i++) {//換列計算
				sum = 0;
				
				for (int j = 0; j < 3; j++) //計算數字
					sum += x[k][j] * B[j][i];
				
				System.out.print(sum + " ");
				if (i == 2)//換行
					System.out.print("\n");
			}
		}
	}

	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);
		int[][] A = new int[3][3];

		System.out.println("Input matrix A:");
		for (int row = 0; row < 3; row++) {//輸入陣列
			for (int line = 0; line < 3; line++)
				A[row][line] = input.nextInt();
		}

		System.out.println("After:");
		multiplication(A);

	}

}


