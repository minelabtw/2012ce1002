package ce1002.A3.s100502006;

import java.util.Scanner;

public class A3 {
	static Scanner input=new Scanner(System.in);
	public static void main(String[] args){
		int[][] A=new int[3][3];//declare a int array
		System.out.println("Input matrix A: ");
		for(int i=0;i<3;i++){//input the array
			for(int j=0;j<3;j++){
				A[i][j]=input.nextInt();
			}
		}
		System.out.println("After: ");
		multiplication(A);//multiply the array with array B
	}
	private static void multiplication(int[][] x){
		int[][] B={{7,5,3},{1,4,9},{4,2,6}};//declaration of array B
		for(int j=0;j<3;j++){
			for(int k=0;k<3;k++){
				System.out.print(x[j][0]*B[0][k]+x[j][1]*B[1][k]+x[j][2]*B[2][k]+" ");//count array members
				if(k==2){
					System.out.println();//change line
				}
			}
		}
	}
}
