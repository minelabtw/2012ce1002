package ce1002.A3.s101502012;

import java.util.Scanner;

public class A3 {
	public static void main(String[] args) {
		int[][] A = new int[3][3];	//宣告一個空矩陣A
		Scanner input = new Scanner(System.in);
		System.out.println("Input matrix A:");
		for (int i=0;i<3;i++)
			for (int j=0;j<3;j++) //由使用者輸入矩陣數字
			{
				A[i][j] = input.nextInt();
			}
		System.out.println ("\nAfter:");
		for(int k=0;k<3;k++)
		{
			for(int l=0;l<3;l++)
			{
				System.out.print( multiplication(A)[k][l] +" "); //傳入自定義函式 並得到答案			
			}
			System.out.print("\n");
		}
	}
	static int[][] multiplication(int[][] A) //自定義函示 功能為乘B後的結果
	{
		int[][] B = {{7,5,3},{1,4,9},{4,2,6}},result = new int[3][3]; //Result是最後的答案
		for(int i=0 ; i<3 ; i++)
			for(int j=0 ; j<3 ; j++)
			{
				result[i][j]=A[i][0]*B[0][j]+A[i][1]*B[1][j]+A[i][2]*B[2][j];
			}
		return result;
	}
}
