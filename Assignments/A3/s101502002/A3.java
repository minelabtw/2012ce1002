package ce1002.A3.s101502002;
import java.util.Scanner;
public class A3 {
	public static void main(String[]args){ //主程式
		Scanner input=new Scanner(System.in);
		int []num=new int[9]; //A矩陣
		System.out.println("Input matrix A: ");
		for (int i=0;i<9;i++) //輸入A矩陣
			num[i]=input.nextInt();
		multiplication(num); //值行函式
		System.out.println("\nAfter: ");
		for(int i=0;i<=6;i=i+3) //一行一行輸出答案
			System.out.println(num[i]+" "+num[i+1]+" "+num[i+2]);	
	}
	public static void multiplication(int []arr){ //函式
		int[]buffer=new int[9]; //令一個新陣列來記錄原本A矩陣的值
		for(int i=0;i<9;i++)
			buffer[i]=arr[i];
		for(int i=0;i<=6;i=i+3) //計算第一行
			arr[i]=buffer[i]*7+buffer[i+1]*1+buffer[i+2]*4;
		for(int i=1;i<=7;i=i+3) //計算第二行
			arr[i]=buffer[i-1]*5+buffer[i]*4+buffer[i+1]*2;
		for(int i=2;i<=8;i=i+3) //計算第三行
			arr[i]=buffer[i-2]*3+buffer[i-1]*9+buffer[i]*6;
	}
}
