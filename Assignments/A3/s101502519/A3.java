package ce1002.s101502519;

import java.util.Scanner;

public class A3 {
	public static void multiplication(int[][] y){   //傳入陣列
		int[][] x={{7,5,3},{1,4,9},{4,2,6}};
		int[][] z=new int[3][3];
		z[0][0]=y[0][0]*x[0][0]+y[0][1]*x[1][0]+y[0][2]*x[2][0];      //運算
		z[0][1]=y[0][0]*x[0][1]+y[0][1]*x[1][1]+y[0][2]*x[2][1];
		z[0][2]=y[0][0]*x[0][2]+y[0][1]*x[1][2]+y[0][2]*x[2][2];
	    z[1][0]=y[1][0]*x[0][0]+y[1][1]*x[1][0]+y[1][2]*x[2][0];
		z[1][1]=y[1][0]*x[0][1]+y[1][1]*x[1][1]+y[1][2]*x[2][1];
		z[1][2]=y[1][0]*x[0][2]+y[1][1]*x[1][2]+y[1][2]*x[2][2];
		z[2][0]=y[2][0]*x[0][0]+y[2][1]*x[1][0]+y[2][2]*x[2][0];
		z[2][1]=y[2][0]*x[0][1]+y[2][1]*x[1][1]+y[2][2]*x[2][1];
		z[2][2]=y[2][0]*x[0][2]+y[2][1]*x[1][2]+y[2][2]*x[2][2];
		for(int i=0;i<=2;i++)                //傳值
			for(int o=0;o<=2;o++){
				y[i][o]=z[i][o];
			}
	}
	
	public static void main(String[] args){
		Scanner in =new Scanner(System.in);
		
		int[][] y=new int[3][3];
		System.out.println("Input matrix A: ");
		for(int i=0;i<=2;i++)               //輸入陣列
			for(int o=0;o<=2;o++){
				y[i][o]=in.nextInt();
			}
		multiplication(y);	           //傳入函式
		System.out.println();
		System.out.println("After: ");
		for(int i=0;i<=2;i++){             //輸出陣列
			for(int o=0;o<=2;o++){
				System.out.print(y[i][o]+" ");
			}
			System.out.println();
		}
	}	
}
