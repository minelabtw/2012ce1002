package ce1002.A3.s101502026;

import java.util.Scanner;

public class A3
{
	public static void main(String[] args)
	{
		Scanner scanner = new Scanner(System.in);
		int[][] B_array = {{7,5,3},{1,4,9},{4,2,6}};
		while(true)
		{
			int[][] A_array = new int[3][3]; //use a two-dimensional array to store the array inserted
			int[][] C_array= new int [3][3]; //use a two-dimensional array to store the array figured out
			System.out.println("Input matrix A: ");
			for(int i=0;i<A_array.length;i++)
			{
				for(int j=0;j<A_array[0].length;j++)
					A_array[i][j]=scanner.nextInt();
			}
			for(int i=0;i<A_array.length;i++) //this is the loop to calculate arrayA*arrayB
			{
				for(int j=0;j<A_array[0].length;j++)
				{
					int sum=0;
					for(int k=0;k<A_array[0].length;k++)
						sum+=A_array[i][k]*B_array[k][j];
					C_array[i][j] = sum;
				}
			}
			System.out.println("\nAfter: ");
			for(int i=0;i<C_array.length;i++)
			{
				for(int j=0;j<C_array[0].length;j++)
					System.out.print(C_array[i][j]+" ");
				System.out.println();
			}
			System.out.println();
		}
	}
}