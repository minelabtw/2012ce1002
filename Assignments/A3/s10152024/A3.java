package ce1002.A3.s10152024;

import java.util.Scanner;

public class A3 {

	public static void multiplication(int x[][]) {

		int b[][] = new int[][] { { 7, 5, 3 }, { 1, 4, 9 }, { 4, 2, 6 } };// declare
		int c[][] = new int[3][3];

		for (int m = 0; m < 3; m++) {// to get the result
			for (int p = 0; p < 3; p++) {
				for (int n = 0; n < 3; n++) {
					c[m][p] = c[m][p] + x[m][n] * b[n][p];
				}
			}
		}
		for (int i = 0; i < 3; i++) {// replace the volume
			for (int j = 0; j < 3; j++) {
				x[i][j] = c[i][j];
			}

		}

	}

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner hue = new Scanner(System.in);

		int a[][] = new int[3][3];

		int a1;

		System.out.println("Input matrix A: ");// input
		for (int i = 0; i < 3; i++) {
			for (int j = 0; j < 3; j++) {
				a1 = hue.nextInt();
				a[i][j] = a1;

			}
		}
		System.out.println(" ");

		multiplication(a);// do the converting progress

		System.out.println("After:");// show result
		for (int i = 0; i < 3; i++) {
			for (int j = 0; j < 3; j++) {
				System.out.print(a[i][j] + " ");

			}

		}

	}

}
