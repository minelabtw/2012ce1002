package ce1002.A3.s100502203;

import java.util.Scanner;

public class A3 {
	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);
		int matrixA[][] = new int[3][3], matrixB[][] = { { 7, 5, 3 },
				{ 1, 4, 9 }, { 4, 2, 6 } }, matrixC[][] = new int[3][3];
		System.out.println("Input matrix A:");
		for (int i = 0; i < 3; i++)
			for (int j = 0; j < 3; j++)
				matrixA[i][j] = input.nextInt();
		for (int i = 0; i < 3; i++)
			for (int j = 0; j < 3; j++)
				for (int k = 0; k < 3; k++)
				// C(row, column) = A(row, k) * B(k, column)
					matrixC[i][j] += (matrixA[i][k] * matrixB[k][j]);
		System.out.println("\nAfter:");
		for (int i = 0; i < 3; i++){
			for (int j = 0; j < 3; j++)
				System.out.print(matrixC[i][j] + " ");
			System.out.println(" ");
		}
		input.close();
	}
}
