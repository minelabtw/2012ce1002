package ce1002.A3.s100502515;

import java.util.Scanner;

public class A3 {
	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);
		double[][] a = new double[3][3];
		System.out.println("Input Matrix A:");
		for (int i = 0; i < a.length; i++) {
			for (int j = 0; j < a.length; j++) {
				a[i][j] = scanner.nextDouble();//Matrix A set.
			}
		}
		A3 a3 = new A3();
		a3.multiplication(a);
		scanner.close();
	}

	private void multiplication(double[][] a) {
		double[][] b = { { 7, 5, 3 }, { 1, 4, 9 }, { 4, 2, 6 } };//Matrix B set.
		double[][] c = new double[3][3];
		for (int i = 0; i < 3; i++) {
			for (int j = 0; j < 3; j++) {
				for (int k = 0; k < 3; k++) {
					c[i][j] += a[i][k] * b[k][j];//Matrix multiplication.
				}
			}
		}
		System.out.println("After:");
		for (int i = 0; i < c.length; i++) {
			for (int j = 0; j < c.length; j++) {
				System.out.print(c[i][j] + "  ");//Result shown here.
			}
			System.out.println();
		}
	}

}
