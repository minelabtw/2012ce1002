package ce1002.A3.s101502520;

import java.util.Scanner;

public class A3 {

	public static void main(String[] args) {
		int[][] A = new int[3][3];
		int[][] C = new int[3][3];
		int[][] B = {
				{7, 5, 3},
				{1, 4, 9},
				{4, 2, 6}
			};
		Scanner inputScanner = new Scanner(System.in);
		System.out.println("Input matrix A: ");
		for(int i = 0; i < 3; i++)
			for(int j = 0; j < 3; j++)
				A[i][j] = inputScanner.nextInt();
		inputScanner.close();//finished the input
		
		for(int i = 0; i < 3; i++)
			for(int j = 0; j < 3; j++)
			{
				C[i][j] = A[i][0] * B[0][j] + A[i][1] * B[1][j] + A[i][2] * B[2][j];//do the 3x3 matrix multiplication
			}
		System.out.println();
		System.out.println("After: ");//print out the result	
		for(int i = 0; i < 3; i++, System.out.println())
			for(int j = 0; j < 3; j++)
				System.out.print(C[i][j] + " ");
		
	}

}
