package ce1002.A3.s101502014;
import java.util.Scanner;
public class A3 {
	static int[][] B = {{7 , 5 , 3} , {1 , 4 , 9} , {4 , 2 , 6}}; //B陣列為題目預設的
	public static int[][] multiplication(int[][] a) //multiplication函式用來運算矩陣相乘
	{
		int[][] ans = new int[3][3]; //宣告ans陣列記錄矩陣相乘的結果
		int sum; //sum紀錄矩陣相乘後每一格的結果
		for(int k = 0 ; k < 3 ; k++)
		{
			for(int l = 0 ; l < 3 ; l++)
			{
				sum = 0; //sum初始為0
				for(int m = 0 ; m < 3 ; m++)
				{
					ans[k][l] = sum += a[k][m] * B[m][l];
				}
			}
		}
		return ans; //回傳ans陣列
	}
	public static void main(String[] args) {
		Scanner cin = new Scanner(System.in); //cin為輸入的動作
		while(true)
		{
			int[][] A = new int[3][3] , C = new int[3][3]; //A陣列為使用者輸入的陣列 , C陣列為AB陣列矩陣相乘的結果
			for(int i = 0 ; i < 3 ; i++) //使用者輸入A陣列的內容
			{
				for(int j = 0 ; j < 3 ; j++)
					A[i][j] = cin.nextInt();
			}
			C = multiplication(A); //將A陣列代入multiplication函式 , C陣列紀錄函式回傳的陣列值
			for(int i = 0 ; i < 3 ; i++) //輸出C陣列的內容
			{
				for(int j = 0 ; j < 3 ; j++)
					System.out.print(C[i][j] + " ");
				System.out.println("");
			}
		}
	}
}

