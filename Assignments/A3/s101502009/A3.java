package ce1002.a3.s101502009;

import java.util.Scanner;

public class A3 {	
	
	public static void multiplication(int a[][]){
		
		int b[][]={{7,5,3},{1,4,9},{4,2,6}};
		int c[][]=new int[3][3];
		
		for(int i=0;i<3;i++){
			for(int j=0;j<3;j++){				
				c[i][j]=a[i][0]*b[0][j]+a[i][1]*b[1][j]+a[i][2]*b[2][j];
			}			
		}
		for(int i=0;i<3;i++){
			for(int j=0;j<3;j++){				
				a[i][j]=c[i][j];
			}			
		}		
	}
	public static void main(String[] args){
		System.out.println("Input matrix A:");
		Scanner input=new Scanner(System.in);		
		
		//announce the a rectangular
		int a[][]=new int[3][3];
		for(int i=0;i<3;i++){
			for(int j=0;j<3;j++){				
				a[i][j]=input.nextInt();
			}			
		}
		
		multiplication(a);
		
		System.out.println();
		System.out.println("After:");
		for(int i=0;i<3;i++){
			for(int j=0;j<3;j++){
				System.out.print(a[i][j]+" ");
			}
			System.out.println();
		}
		
		
		
	}//end of main

}
