//A3-100502201
//Multiplication of matrix
//Use function to calculate and practice the use of double arrays
package ce1002.A3.s100502201;

import java.util.Scanner;

public class A3 {
	public static int[][] b = { { 7, 5, 3 }, { 1, 4, 9 }, { 4, 2, 6 } }; //Declaring given Matrix B

	private static int[][] multiplication(int[][] a) { //A matrix is needed
		int[][] c = new int[3][3]; //All 3x3 matrix

		for (int m = 0; m < 3; m++)
			for (int p = 0; p < 3; p++)
				for (int n = 0; n < 3; n++)
					c[m][p] = c[m][p] + a[m][n] * b[n][p]; //Method to multiply
		return c;
	}

	public static void main(String[] args) {
		System.out.print("Input matrix A:\n");
		Scanner matrix = new Scanner(System.in);
		int[][] a = new int[3][3];
		for (int k = 0; k < 3; k++) //Input A matrix
			for (int l = 0; l < 3; l++)
				a[k][l] = matrix.nextInt();
		int c[][] = multiplication(a); //Use function and save value to C matrix
		System.out.print("\nAfter:\n");
		for (int i = 0; i < 3; i++) { //Print
			for (int j = 0; j < 3; j++)
				System.out.print(c[i][j] + " ");
			System.out.println("");
		}
	}
}
