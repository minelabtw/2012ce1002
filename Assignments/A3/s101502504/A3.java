package ce1002.A3.s101502504;
import java.util.Scanner;
public class A3
{
	public static void main(String[] args)
	{
		System.out.println("Input matrix A: ");//prompt user input
		multiplication();
	}
	
	public static void multiplication()
	{
		Scanner input = new Scanner(System.in);
		int[][] arrayA = new int[3][3];//initialize arrayA
		int[][] arrayB = {{7,5,3},{1,4,9},{4,2,6}};//initialize arrayB
		int[][] arrayC = new int[3][3];//initialize arrayC
		int a=0;
		
		for(int i=0; i<3; i++)
			for(int j=0; j<3; j++)
				arrayA[i][j]=input.nextInt();//user input arrayA

		for(int t=0; t<3; t++)//run for three times for straight direction
		{
			for(int i=0; i<3; i++)//calculate the horizontal direction
				for(int j=0; j<3; j++)
					arrayC[t][j] += arrayA[t][i] * arrayB[i][j];
		}
		
		System.out.print("\n");
		System.out.println("After:");
		
		int times=0;
		for(int i=0; i<3; i++)//print the answer
		{
			for(int j=0; j<3; j++)
				System.out.print(arrayC[i][j]+" ");
			System.out.print("\n");
		}
			
	}
}