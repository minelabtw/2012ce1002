package ce1002.A3.s101502523;
import java.util.*;
public class A3 {
	public static void main(String[] args){
		Scanner input = new Scanner(System.in);
		int[] sum= new int[9];
		int[] matrix = new int[9]; 
		System.out.println("Input matrix A: ");
		for(int i=0 ; i<9 ; i++){
			//input value into the matrix array
			matrix[i] = input.nextInt();
		}
		multiplication(matrix, sum); //call function
		System.out.println("\nAfter: ");
		for(int m=0 ; m<9 ; m++){
			//output the answer
			System.out.print(sum[m]+" ");
			if(m==2||m==5||m==8)
				System.out.println("");
		}
	}
	public static void multiplication(int[] A, int[] sum){
		//the function of multiplying two matrix
		int[] B = {7,1,4,5,4,2,3,9,6}; //B array(Read:first��,second��and��,and so on)
		int k=0, l=0; //k is the position of A array, l is the position of B array
		for(int j=0 ; j<9 ; j++){ 
			//j is the position of the sum array
				sum[j] = A[k]*B[l] + A[k+1]*B[l+1] + A[k+2]*B[l+2]; //after the multiplication
				l+=3;
			if(j==2||j==5||j==8&&k<6){
				//when the position of sum array is the last number of the row, change the position of the A and B array
				k+=3;
				l=0;
			}
		}
	}
}
