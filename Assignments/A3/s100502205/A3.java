package ce1002.A3.s100502205;

import java.util.Scanner;

public class A3 {
	public static int[][] B = { { 7, 5, 3 }, { 1, 4, 9 }, { 4, 2, 6 } };

	public static int[][] multiplication(int[][] A, int[][] B) {
		// A.row = A.length, A.column = A[0].length
		// B.row = B.length, B.column = B[0].length
		int[][] C = new int[A.length][B[0].length];
		for (int i = 0; i < A.length; i++) {
			for (int j = 0; j < B[0].length; j++) {
				C[i][j] = 0;
				for (int k = 0; k < B.length; k++)
					C[i][j] += A[i][k] * B[k][j];
			}
		}
		return C;
	}

	public static void main(String[] args) {
		Scanner cin = new Scanner(System.in);
		int[][] A = new int[3][3];
		System.out.println("Input matrix A:");
		for (int i = 0; i < A.length; i++)
			for (int j = 0; j < A[0].length; j++)
				A[i][j] = cin.nextInt();
		int[][] C = multiplication(A, B);
		System.out.println("\nAfter");
		for (int i = 0; i < C.length; i++) {
			for (int j = 0; j < C[0].length; j++)
				System.out.print(C[i][j] + " ");
			System.out.println();
		}
	}
}
