package ce1002.A3.s976002019;

import java.util.Scanner;

public class A3 {
	public static void main(String arg[]){
		int[][] A=new int[3][3];
		int[][] B={{7, 5, 3}, /*預設B矩陣*/
				   {1, 4, 9},
				   {4, 2, 6}};
		int[][] result=new int[3][3];
		System.out.println("Input matrix A:"); /*輸入A矩陣*/
		Scanner input1=new Scanner(System.in);
		for(int i=0; i<3; i++){
			A[0][i]=input1.nextInt();
		}
		Scanner input2=new Scanner(System.in);
		for(int i=0; i<3; i++){
			A[1][i]=input2.nextInt();
		}
		Scanner input3=new Scanner(System.in);
		for(int i=0; i<3; i++){
			A[2][i]=input3.nextInt();
		}
		
		
		result[0][0]=A[0][0]*B[0][0]+A[0][1]*B[1][0]+A[0][2]*B[2][0]; /*計算箱城結果*/
		result[0][1]=A[0][0]*B[0][1]+A[0][1]*B[1][1]+A[0][2]*B[2][1];
		result[0][2]=A[0][0]*B[0][2]+A[0][1]*B[1][2]+A[0][2]*B[2][2];
		result[1][0]=A[1][0]*B[0][0]+A[1][1]*B[1][0]+A[1][2]*B[2][0];
		result[1][1]=A[1][0]*B[0][1]+A[1][1]*B[1][1]+A[1][2]*B[2][1];
		result[1][2]=A[1][0]*B[0][2]+A[1][1]*B[1][2]+A[1][2]*B[2][2];
		result[2][0]=A[2][0]*B[0][0]+A[2][1]*B[1][0]+A[2][2]*B[2][0];
		result[2][1]=A[2][0]*B[0][1]+A[2][1]*B[1][1]+A[2][2]*B[2][1];
		result[2][2]=A[2][0]*B[0][2]+A[2][1]*B[1][2]+A[2][2]*B[2][2];
		
		for(int i=0;i<3;i++) /*印出結果*/
				System.out.println(result[i][0]+" "+result[i][1]+" "+result[i][2]+" "); 
		
		
		
		
	}

}
