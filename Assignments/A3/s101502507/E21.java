package ce1002.E2.s101502507;

import java.util.Scanner;

public class E21 
{
	public static void main(String[] args)
	{
		Scanner input = new Scanner(System.in);
		int abc[] = new int[100];		
		int m=0,a,b=0,c=0,i,h=0;
		
		while(m==0)//判斷迴圈是否結束
		{
			System.out.print("1: push\n2: pop\n3: show\n4: exit\n");
			a = input.nextInt();
			
			if(a==1)
			{
				System.out.print("push ");
				b = input.nextInt();
				h+=b;//h是陣列內的數量
				if(h-b>0)//若陣列中有剩餘則從那數字之後繼續堆疊
				{	
					for(i=h-b;i<h;i++)//紀錄使用者輸入之數字到陣列中				
						abc[i]=input.nextInt();	
				}
				else//若以彈跳光了則從第一個位置開始堆疊
				{
					for(i=0;i<h;i++)//紀錄使用者輸入之數字到陣列中				
						abc[i]=input.nextInt();	
				}
				System.out.println();				
			}
						
			if(a==2)//彈跳數字
			{
				System.out.print("pop ");
				c = input.nextInt();
				if(c<=h)//若使用者想彈出的數量小於等於陣列中的數量則印出
				{
					for(i=h-1;i>=h-c;i--)	
						System.out.print(abc[i]+" ");
					h-=c;
					System.out.println("\n");	
				}
				else//若彈出數量大於陣列中的數量則輸出剩下的數字並輸出Stack is empty!字樣
				{
					for(i=h-1;i>=0;i--)	
						System.out.print(abc[i]+" ");	
					System.out.println();
					h=0;//陣列空了
					System.out.println("Stack is empty!\n");
				}
			}		
						
			if(a==3)//輸出剩下數字
			{
				if(h>0)//若還有剩下的則輸出
				{
					for(i=0;i<h;i++)	
						System.out.print(abc[i]+" ");					
					System.out.println("\n");	
				}
				else//若沒有剩下的則輸出Stack is empty!字樣	
					System.out.println("Stack is empty!\n");				
			}			
			
			if(a==4)//離開迴圈
			{
				System.out.println("Good Bye");
				m=1;				
			}			
		}			
	}
}