package ce1002.A3.s101502028;

import java.util.*;

public class A3 {
	public static void main(String[] args) {
		Scanner array = new Scanner(System.in);
		int[][] ma = new int[3][3]; // array to input the matrix
		System.out.println("Input matrix A:");
		for (int row = 0; row < 3; row++) {
			for (int line = 0; line < 3; line++) {
				ma[row][line] = array.nextInt(); // input the matrix
			}
		}
		System.out.println("\nAfter:");
		multiplication(ma); // apply the function
	}

	private static void multiplication(int ma[][]) { // function of multiplication
		int[][] matrix = new int[3][3]; // array of the result of the matrix
		int[][] mb = { { 7, 5, 3 }, { 1, 4, 9 }, { 4, 2, 6 } }; // the given matrix
		for (int a = 0; a < 3; a++) {
			for (int b = 0; b < 3; b++) {
				for (int c = 0; c < 3; c++) {
					matrix[a][b] = matrix[a][b] + (ma[a][c] * mb[c][b]); // sum of all multiplied input matrix with given matrix
				} /* ex. 00 = [(00 x 00)+(01 x 10)+(02 x 20)]
						 ab = [(ac x cb)+(ac x ab)+(ac x cb)] */
				System.out.print(matrix[a][b] + " "); // output the sum
			}
			System.out.println(); // next row
		}
	} // end function
}
