package ce1002.A3.s101502508;

import java.util.Scanner ;

public class A3 {
	public static void main(String args[]){
		
		Scanner input = new Scanner(System.in) ;
		int [][] matrixA  = new int [3][3] ;
		
		////////////////////////////輸入想要的A矩証////////////////////////////
		System.out.println("Input matrix A:") ;
		for ( int row=0 ; row<matrixA.length ; row++ ){ 
			for ( int column=0 ; column<matrixA[0].length ; column++ ){
				matrixA[row][column] = input.nextInt() ;
			}
		}
		//////////////////////////////////////////////////////////////////
		
		System.out.println() ;
		System.out.println("After:") ;
		
		int [][] result = new int[3][3] ;
		result = multiplication(matrixA) ;//將function回傳出的結果放入result裡面
		
		//結果輸出
		for ( int row=0 ; row<matrixA.length ; row++ ){ 
			for ( int column=0 ; column<matrixA[0].length ; column++ ){
				System.out.print(result[row][column]+" ") ;
			}
			System.out.println() ;
		}//結果輸出結束	
	}
	
	public static int[][] multiplication(int[][] A ){
		
		int [][] result = new int [3][3] ;//宣告result儲存矩陣結果
		
		int [][] matrixB = {
				{7, 5, 3},
				{1, 4, 9},
				{4, 2, 6},
		} ;
		
		for ( int row=0 ; row<3 ; row++ )//矩陣計算
			for ( int column=0 ; column<3 ; column++ )
				for ( int x=0 ; x<3 ; x++ )
					result[row][column] = result[row][column] + A[row][x]*matrixB[x][column] ;
		
		return result ;
	}
}
