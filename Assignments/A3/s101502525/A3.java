package ce1002.A3.s101502525;

import java.util.Scanner;

public class A3 {
	public static void main(String[] args){
		//initialize
		Scanner input=new Scanner(System.in);
		int[][] A=new int[3][3];
		int[][] B={{7,5,3},{1,4,9},{4,2,6}};
		
		//input
		System.out.println("Input matrix A:");
		for(int i=0;i<3;i++)
			for(int j=0;j<3;j++)
				A[i][j]=input.nextInt();
		
		//compute & output
		System.out.println("\nAfter:");
		multiplication(A,B);
		printMatrix(C);
	}
	public static int[][] C=new int[3][3];
	private static void multiplication(int[][] A,int[][] B){
		for(int i=0;i<3;i++)//column
			for(int j=0;j<3;j++)//row
				for(int k=0;k<3;k++)
					C[i][j]+=A[i][k]*B[k][j];
	}
	private static void printMatrix(int[][] matrix){
		for(int i=0;i<3;i++){//column
			for(int j=0;j<3;j++)//row
				System.out.printf("%d ",matrix[i][j]);
			System.out.print("\n");
		}
	}
}
