package ce1002.A3.s995002022;

import java.util.Scanner;

public class A3 {

	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);
		System.out.println("Input matrix A: ");
		int[][] arr = {{0, 0, 0},
						{0, 0, 0},
						{0, 0, 0} }; 
		int[][] z = {{7, 5, 3},
					{1, 4, 9},
					{4, 2, 6} }; 

		
		
		for (int i = 0; i < 3; i++){
			for (int j = 0; j < 3; j++){
				
				int input = scanner.nextInt();

				arr[i][j] = input;
				
			}
		}
		
		 int c[][] = new int[arr.length][z[0].length];

		  for (int m = 0; m < arr.length; m++) {
		   for (int p = 0; p < z[0].length; p++) {
		    for (int n = 0; n < arr[0].length; n++) {
		     c[m][p] = c[m][p] + arr[m][n] * z[n][p];
		    }
		   }
		  }
		  			 
		  System.out.println("");
		  System.out.println("After:");
		  
		  for (int i = 0; i < c.length; i++) {
		   for (int j = 0; j < c[0].length; j++) {
		    System.out.print(c[i][j] + ",");

		   }
		   System.out.println("");
		  }
		 }
		}
