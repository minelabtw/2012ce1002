package ce1002.A3.s101502007;

import java.util.Scanner;

public class A3 {
	
	public static int[][] multiplication(int[][] input)
	{ 
		int [][]answer=new int[3][3];
		int B[][]={ {7,1,4},     //改成這樣較方便運算
					{5,4,2},
					{3,9,6},};
		for (int a=0;a<3;a++)
		{
			for (int b=0;b<3;b++)
			{
				int x,y,z;
				x=input[a][0]*B[b][0];
				y=input[a][1]*B[b][1];
				z=input[a][2]*B[b][2];
				answer[a][b]=x+y+z;
			}
		}
		return answer;
	}

	public static void main(String[] args)
	{
		Scanner input=new Scanner(System.in);
		int [][]A=new int[3][3];
		int [][]C=new int[3][3];
		System.out.println("Input matrix A: ");
		for (int i=0;i<A.length;i++)
		{
			for (int j=0;j<A[0].length;j++)
			A[i][j]=input.nextInt();
		}	
		System.out.println("After: ");
		C=multiplication(A);
		for (int e=0;e<A.length;e++)
		{
			for (int f=0;f<A[0].length;f++)
				{
				if(f==2)
					System.out.println(C[e][f]);
				else
					System.out.print(C[e][f]+" ");
				}
		}	
	} //end of main
}
