package ce1002.A3.s101502515;

import java.util.Scanner;

public class A3 {
	static void multiplication(int input[][]){//輸入二維
		int[][] arr=new int[][]{{7,5,3},{1,4,9},{4,2,6}};//題目陣列
		for(int k=0;k<3;k++){//inputer跳行
			for(int i=0;i<3;i++){
				int outer=0;
				for(int j=0;j<3;j++){
					outer+=input[k][j]*arr[j][i];//加起來
				}
				System.out.print(outer+" ");
			}
			System.out.println("");
		}
	}
	public static void main(String[] args){
		Scanner input=new Scanner(System.in);
		int[][] inputer=new int[3][3];
		for(int i=0;i<3;i++){//迴圈輸入陣列
			for(int j=0;j<3;j++){
				inputer[i][j]=input.nextInt();
			}	
		}
		multiplication(inputer);
	}
}
