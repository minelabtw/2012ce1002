package ce1002.A3.s101502502;

import java.util.Scanner;

public class A3 
{
	public static int[] multiplication(int[] numbers)//矩陣相乘
	{
		int[] aMatrix = new int[9];//在main function裡所輸入的A矩陣
		int[] bMatrix = {7,1,4,5,4,2,3,9,6};//固定的B矩陣
				
		for(int i = 0 ; i < 9 ; i++)//把輸入的矩陣放入aMatrix
		{
			aMatrix[i] = numbers[i];
			numbers[i] = 0;//清除原本的矩陣
		}
		
		for(int i = 0 ; i < 9 ; i+=3)
		{
			if(i==0)//aMatrix的第一列與bMatrix相乘
			{
				for(int j = 0 ; j < 3 ; j++)
				{
					numbers[i] = numbers[i] + aMatrix[j] * bMatrix[j];
					numbers[i+1] = numbers[i+1] + aMatrix[j] * bMatrix[j+3];
					numbers[i+2] = numbers[i+2] + aMatrix[j] * bMatrix[j+6];
				}
			}
			
			if(i==3)//aMatrix的第二列與bMatrix相乘
			{
				for(int j = 0 ; j < 3 ; j++)
				{
					numbers[i] = numbers[i] + aMatrix[j+3] * bMatrix[j];
					numbers[i+1] = numbers[i+1] + aMatrix[j+3] * bMatrix[j+3];
					numbers[i+2] = numbers[i+2] + aMatrix[j+3] * bMatrix[j+6];
				}
			}
			
			if(i==6)//aMatrix的第三列與bMatrix相乘
			{
				for(int j = 0 ; j < 3 ; j++)
				{
					numbers[i] = numbers[i] + aMatrix[j+6] * bMatrix[j];
					numbers[i+1] = numbers[i+1] + aMatrix[j+6] * bMatrix[j+3];
					numbers[i+2] = numbers[i+2] + aMatrix[j+6] * bMatrix[j+6];
				}
			}
			
		}
		
		return numbers;//回傳運算完的陣列
	}
	
	
	public static void main(String[] args)
	{
		Scanner cin = new Scanner(System.in);
		int[] aRow = new int[9];
		
		System.out.println("Input matrix A:");
		
		for(int i = 0 ; i < 9 ; i++)//輸入A陣列
		{
			aRow[i] = cin.nextInt();
		}
		System.out.println();
		cin.close();
		
		aRow = multiplication(aRow);//丟入函式multiplication運算
		
		System.out.println("After:");
		for(int i = 0 ; i < 9 ; i++)
		{
			System.out.print(aRow[i] + " ");//輸出結果
			
			if((i+1)%3==0)//排版
				System.out.println();
		}
		
	}
}
