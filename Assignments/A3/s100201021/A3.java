package ce1002.A3.s100201021;
import java.util.Scanner;
public class A3 {
	public static void main(String[] args){
		Scanner num=new Scanner(System.in);
		int[][] x=new int[3][3];
		System.out.println("Input matrix A:");
		//input matrix A;
		for(int i=0;i<3;i++)
			for(int j=0;j<3;j++)
				x[i][j]=num.nextInt();
		//call void function
		multiplication(x);
		num.close();
		//
	}
	public static void multiplication(int[][] x){
		int[][] ans=new int[3][3];				//for answer;
		int[][] B  ={{7,5,3},{1,4,9},{4,2,6}};	//define B
		//Calculate answer(A*B)
		for(int i=0;i<3;i++)
			for(int j=0;j<3;j++)
				for(int k=0;k<3;k++){
					ans[i][j]+= x[i][k]*B[k][j];
				}
		//output answer;
		System.out.println("\nAfter:");			
		for(int i=0;i<3;i++){
			for(int j=0;j<3;j++)
				System.out.print(ans[i][j]+" ");
			System.out.println("");
		}
		//
	}
}
//	 7 5 3
//B= 1 4 9
//	 4 2 6