package ce1002.A3.s100201510;

import java.util.Scanner;

public class A3 {
	// a function returns matrix.
	public static int[][] multiplication(int[][] matrixA , int[][] matrixB){
		int[][] matrixAB = new int[3][3];
		
		for(int i = 0 ; i < 3*3 ; i++){ // initialize array
			matrixAB[i%3][i/3] = 0;
		}
		
		// do the multiplication by definition of matrix product
		for(int i = 0 ; i < 3*3 ; i++){ 
			for(int k = 0 ; k < 3 ; k++){
				matrixAB[i/3][i%3] += matrixA[i/3][k] * matrixB[k][i%3] ;
			}
		}
		
		return matrixAB;
		
	}
	
	// main function.
	public static void main(String[] args) {
		int[][] A = new int[3][3];
		int[][] B = {// B is fixed matrix.
				{7 , 5 , 3},
				{1 , 4 , 9},
				{4 , 2 , 6}};
		
		System.out.println("Input matrix A:");
		Scanner input = new Scanner(System.in);
		for(int i = 0 ; i < 9 ; i++){// input each entry from .
			A[i/3][i%3] = input.nextInt();
		}
		input.close();
		System.out.println("After:");
		
		for(int i = 0 ; i < 3*3 ; i++){// show result.
			System.out.printf("%d " , multiplication(A , B)[i/3][i%3]);
			//multiplication(A,B) is the matrix AB.
			if( (i+1)%3 == 0 )
				System.out.print("\n");
		}
	}
}