package ce1002;
import java.util.Scanner;

public class A3 {public static void main(String[] args){
	Scanner input = new Scanner(System.in);
	System.out.println("Input matrix A:");
	int [][]a=new int[3][3];//宣告矩陣a
	int [][]b={{7,5,3},{1,4,9},{4,2,6}};//宣告矩陣b同時給數字
	for(int x=0;x<3;x++)
	{
		for(int y=0;y<3;y++)
		{
				a[x][y]=input.nextInt();//使用者輸入陣列a
		}
	}
	System.out.println(" ");
	System.out.println("After:");
	multiplication(a,b);//呼叫自定義函式
}

public static void multiplication(int [][]m,int [][]n){
	int [][]c = new int [3][3];//宣告矩陣c存放運算結果
	for(int i=0;i<3;i++)
	{
		for(int j=0;j<3;j++)
		{
			c[i][j]=m[i][0]*n[0][j]+m[i][1]*n[1][j]+m[i][2]*n[2][j];//計算矩陣相乘
			System.out.print(c[i][j]);//印出c
			System.out.print(" ");
		}
		System.out.println();
	}
	}
	
}
