package ce1002.A3.s101502022;
import java.util.*;

public class A3 {
	static int[][] arrayA= new int[3][3];
	static int[][] arrayB= { {7,5,3},
					  {1,4,9},
					  {4,2,6}		
	};
	static int[][] arrayC= new int[3][3];
	
	
	public static void multiplication(String args[]){
		for(int n=0;n<=2;n++){
			for(int i=0;i<=2;i++){
				for(int j=0;j<=2;j++){
					arrayC[n][i]=arrayC[n][i]+arrayA[n][j]*arrayB[j][i];//讓arrayC累加
					
				}
			}
		}
		
		System.out.println("\nAfter: ");
		for(int i=0;i<=2;i++){
			for(int j=0;j<=2;j++){
				System.out.print(arrayC[i][j]+" ");
				if(j==2)//每三個跳下一行
					System.out.print("\n");
			}
		}
	}
	
	
	public static void main(String args[]){
		
		Scanner input=new Scanner(System.in);
		System.out.println("Input matrix A: ");
		for(int i=0;i<=2;i++){
			for(int j=0;j<=2;j++){
				arrayA[i][j]=input.nextInt();
			}
		}
		 multiplication(args);
		
	}

}
