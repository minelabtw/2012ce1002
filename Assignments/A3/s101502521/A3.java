package ce1002.A3.s101502521;

import java.util.Scanner;

public class A3 {
	public static void main(String[] args)
	{
		int[][] A = new int[3][3];
		int[][] B = new int[3][3];
		int[][] C = new int[3][3];
		B[0][0]=7;B[0][1]=5;B[0][2]=3; // define matrix B
		B[1][0]=1;B[1][1]=4;B[1][2]=9;
		B[2][0]=4;B[2][1]=2;B[2][2]=6;
		System.out.println("Input matrix A: ");
		Scanner input = new Scanner(System.in);
		for(int i=0;i<3;i++)
			for(int j=0;j<3;j++)
				A[i][j] = input.nextInt(); // get matrix A
		multiplication(A, B, C);
		System.out.println("");
		System.out.println("After:");
		
		for(int i=0;i<3;i++)
			System.out.println(C[i][0]+" "+C[i][1]+" "+C[i][2]);
	}
	
	public static void multiplication(int[][] A, int[][] B, int[][] ans)
	{
		for(int i=0;i<3;i++)
			for(int j=0;j<3;j++)
				for(int k=0;k<3;k++)
					ans[i][j] += A[i][k]*B[k][j]; // matrix multiplication
	}
}
