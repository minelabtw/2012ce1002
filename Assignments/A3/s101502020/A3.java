//A3 by J
package ce1002.A3.s101502020;

import java.util.Scanner;

public class A3 {
	
	public static long[][] multiplication (long[][] ary) {
		
		long[][] matrixB={{7,5,3},{1,4,9},{4,2,6}} ;		//fixed multiplication target
		long[][] answer=new long[3][3] ;
				
		for (int i=0 ; i<3 ; i++ ) {
			for (int j=0 ; j<3 ; j++ ) {
				long sum=0 ;
				for (int k=0 ; k<3 ; k++) {
					sum+=ary[i][k]*matrixB[k][j];
				}		//multiplication
				answer[i][j]=sum ;		
			}		//column
		}		//row
		
		return answer;
	}

	public static void main(String[] args) {
				
		long[][] matrixA=new long[3][3] ;
		
		Scanner input=new Scanner(System.in) ;
		
		System.out.println("Input matrix A: ");
		
		for (int i=0 ; i<3 ; i++) {
			for (int j=0 ; j<3 ; j++) {
				matrixA[i][j]=input.nextLong();
			}
		}		//matrix A input here
		
		matrixA=multiplication(matrixA);
		
		System.out.println("\nAfter:");
		
		for (int i=0 ; i<3 ; i++) {
			
			for (int j=0 ; j<3 ; j++) {
				System.out.print( matrixA[i][j] + " " );
			}
			System.out.print("\n");
			
		}		//output result
		
	}

}
