package ce1002.A3.s101502004;
import java.util.Scanner;

public class A3 {
	public static void multiplication(int[][] A){//計算矩陣的function
		int [][] B = {{7,5,3},
						{1,4,9},
							{4,2,6}};//設定B矩陣
		int [][] C = new int[3][3];//結果矩陣
		
		for(int z=0;z<3;z++)//換列
			for(int y=0;y<3;y++)
				for(int x=0;x<3;x++)//A,B元素加起做運算
					C[y][z]+=A[z][x]*B[x][y];
		System.out.println("After: ");
		for(int y=0;y<3;y++){//換列
			for(int x=0;x<3;x++)//換行
				System.out.print(C[x][y]+" ");
			System.out.println("");//印出來的換列
		}
	}
	public static void main(String[] args){
		System.out.println("Input matrix A:");
		Scanner inp = new Scanner(System.in);
		int [][] matrix = new int[3][3];
		int [][] after = new int[3][3];
		for(int i=0;i<3;i++)
			for(int j=0;j<3;j++)
				matrix[i][j]=inp.nextInt();//輸入矩陣
		System.out.println(" ");//空白一行
		multiplication(matrix);//把整個矩陣傳進function
	}
}
