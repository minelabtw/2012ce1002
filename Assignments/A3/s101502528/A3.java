package ce1002.A3.s101502528;

import java.util.Scanner;

public class A3
{
	public static void main(String[] args)
	{
		Scanner input=new Scanner(System.in);
		int[][] a=new int[3][3];
		int[] ans=new int[9];
		
		System.out.println("Input matrix A:");
		//輸入矩陣A
		for (int i=0 ; i<3 ; i++)
			for (int j=0 ; j<3 ; j++)
				a[i][j]=input.nextInt();
		
		//呼叫函式計算
		multiplication(a,ans);
		
		System.out.println();
		System.out.println("After:");
		//輸出計算後的矩陣
		for (int i=0 ; i<9 ; i++)
		{
			System.out.print(ans[i]+" ");
			if (i%3==2)
				System.out.println();
		}
	}
	
	public static void multiplication(int[][] matrixa,int[] count)
	{
		int[] save=new int[27];
		int[][] matrixb={{7,5,3},{1,4,9},{4,2,6}};
		int x=0,y=0;
		
		//計算
		for (int i=0 ; i<3 ; i++)
			for (int j=0 ; j<3 ;j++)
				for (int k=0 ; k<3 ; k++)
				{
					save[x]=matrixa[i][k]*matrixb[k][j];
					x++;
				}
		
		//將計算出的值分配進3*3的矩陣
		for (int i=0 ; i<27 ; i=i+3)
		{
			count[y]=save[i]+save[i+1]+save[i+2];
			y++;
		}
	}
}
