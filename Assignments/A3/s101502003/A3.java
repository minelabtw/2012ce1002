package ce1002.A3.s101502003;
import java.util.Scanner;
public class A3 {
	public static void multiplication(int array[][]) // 運算的function
	{
		int[][]arrayB=// 設陣列B的數
		{
			{7,5,3},
			{1,4,9},
			{4,2,6}
		};
		System.out.print("\nAfter:\n");
		for(int i=0;i<3;i++) // 第一排的A*B
		{
			int sum=0;
			for(int j=0;j<3;j++)
			{
				sum = sum+array[0][j]*arrayB[j][i];
			}
			System.out.print(sum + " ");
		}
		System.out.print("\n");
		for(int i=0;i<3;i++)// 第二排的A*B
		{
			int sum=0;
			for(int j=0;j<3;j++)
			{
				sum = sum+array[1][j]*arrayB[j][i];
			}
			System.out.print(sum + " ");
		}
		System.out.print("\n");
		for(int i=0;i<3;i++) // 第三排的A*B
		{
			int sum=0;
			for(int j=0;j<3;j++)
			{
				sum = sum+array[2][j]*arrayB[j][i];
			}
			System.out.print(sum + " ");
		}
		System.out.print("\n");
	}
	public static void main(String[] args)
	{
		int [][] arrayA=new int[3][3];
		Scanner input = new Scanner(System.in);
		System.out.println("Input matrix A: ");
		for(int i=0;i<3;i++)// 輸入陣列A
		{
			for(int j=0;j<3;j++)
			{
				int x = input.nextInt();
				arrayA[i][j]=x;
			}
		}
		multiplication(arrayA); // 帶入函式
	}
}