package ce1002.A3.s101502010;

import java.util.Scanner;

public class A3 {
	public static void multiplication(int[][] input){
		int[][] b=new int[][]{{7, 5, 3},{ 1, 4, 9},{ 4, 2, 6}};
		int[][] output=new int[3][3];
		for(int i=0;i< 3;i++)
		for(int j=0;j< 3;j++)
		for(int k=0;k< 3;k++){
			output[i][j]+=input[i][k]*b[k][j];
		}
			for(int i=0;i< 3;i++)
				for(int j=0;j< 3;j++)
					input[i][j]=output[i][j];
		
	}//end of multiplication
	public static void main(String[] args){
		System.out.println("Input matrix A:");
		int[][] input=new int[3][3];
		Scanner element = new Scanner(System.in); 
		for(int i=0;i< 3;i++)
			for(int j=0;j< 3;j++)
				input[i][j]=element.nextInt();
		
		multiplication(input);
		System.out.println("\nAfter: ");
		for(int i=0;i< 3;i++)
			for(int j=0;j< 3;j++){
				System.out.print(input[i][j]+" ");
			if(j==2)
				System.out.print("\n");
			}
		
	}//end of main
}