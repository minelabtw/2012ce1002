package CE1002.s101502518;

import java.util.Scanner;

public class A3 {
	static public void main(String[] args)
	{
		int[][]  matrixA = new int[3][3];
		int[][]  matrixB ={
		{7,5,3},
		{1,4,9},
		{4,2,6}
		};
		
		Scanner input = new Scanner(System.in);
		
		System.out.println("Input matrix A:");
		for(int i=0;i<3;i++)
			for(int j=0;j<3;j++)
				matrixA[i][j] = input.nextInt();
		
		System.out.println("\nAfter:");
		for(int l=0;l<3;l++)
		{
			for(int i=0;i<3;i++)
			{
				int add=0;
				for(int j=0;j<3;j++)
					add=add+matrixA[l][j]*matrixB[j][i];			
				System.out.print(add+" ");	
			}
		System.out.println();
		}
	}
}
