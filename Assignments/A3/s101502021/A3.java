package ce1002.A3.s101502021;
import java.util.Scanner;

import javax.naming.spi.DirStateFactory.Result;

public class A3 {
	
	static int arr[][] = {{7,5,3},{1,4,9},{4,2,6}};//矩陣B為固定的值，以後想改的話可以直接從這裡改
	static int result[][] = {{0,0,0},{0,0,0},{0,0,0}};//初始化乘出來的值一開始為0矩陣
	
	public static void main(String[] args){
		
		int[][] Inputarr = new int[3][3];//矩陣A為使用者輸入的值
		Scanner input = new Scanner(System.in);
		
		System.out.println("Input matrix A: ");
		
		//輸入矩陣A
		for(int i=0;i<3;i++)
			for(int j=0;j<3;j++)
				Inputarr[i][j]=input.nextInt();
		
		//計算矩陣AxB
		multiplication(Inputarr);
		
		System.out.println("\nAfter: ");
		
		//輸出乘積
		for(int i=0;i<3;i++){
			for(int j=0;j<3;j++)
				System.out.print(result[i][j]+" ");
			System.out.println(" ");
		}
	
	}
	
	//兩矩陣相乘的函式
	public static void multiplication(int inputarr[][]){
		
		for(int i=0;i<3;i++)
			for(int j=0;j<3;j++){
				for(int w=0;w<3;w++)
					result[i][j]+=(inputarr[i][w]*arr[w][j]);
			}
	}
}
