package ce1002.A3.s101502015;

import java.util.Scanner;

public class A3 {
	public static void main(String[] args)
	{
		System.out.println("Input matrix A:");
		int [][] A = new int [3][3] ;
		Scanner t = new Scanner(System.in);
		for(int i=0;i<3;i++)
		{
			for(int j=0;j<3;j++)
			{
				A[i][j] = t.nextInt();
			}
		}
		System.out.println();
		multiplication(A);

	}
	public static void multiplication(int[][] A)
	{
		int [][] B = {{7,1,4},{5,4,2},{3,9,6}} ;
		int [][] C = {{0,0,0},{0,0,0},{0,0,0}} ;
		for(int i=0;i<3;i++)
		{
			for(int j=0;j<3;j++)
			{
				for(int k=0;k<3;k++)
					C[i][j] = A[i][k]*B[j][k] + C[i][j];
			}
		}
		System.out.println("After: ");
		for(int i=0;i<3;i++)
		{
			for(int j=0;j<3;j++)
			{
				System.out.print(C[i][j] + " ");
			}
			System.out.println();
		}
	}

}
