package ce1002.A3.s101502526;
import java.util.Scanner;
public class A3 {


	public static void main(String[] args) {
		Scanner input= new Scanner(System.in);
		System.out.println("Input matrix A: ");
        int a[][] = new int[3][3];
        int number;
        for (int i = 0 ; i <3 ; i ++)
        {
        	for (int j = 0 ; j <3 ; j++)
        	{
        		number = input.nextInt();                    //先輸入矩陣
        		a[i][j] = number ;
        	}
        }
		System.out.println();
		System.out.println("After: ");		
		multiplication(a);	                                //丟入函式後輸出
        for (int i = 0 ; i <3 ; i ++)
        {
        	for (int j = 0 ; j <3 ; j++)
        	{
        		System.out.print(a[i][j]+" ");	
        	}
        		System.out.println();
        }
	}

	public static void multiplication(int a[][]) {
		int b2[] = {7,5,3,1,4,9,4,2,6};
		int sum = 0 , time = 0 ;
		int c[][] = new int[3][3];
        int b[][] = new int[3][3];
        for (int i = 0 ; i <3 ; i ++)
        {
        	for (int j = 0 ; j <3 ; j++)                     //先將B矩陣設好
        	{
        		b[i][j] = b2[time] ;
        		time +=1;
        	}
        }
		for (int i = 0 ; i < 3; i ++)
		{
			for (int j = 0; j <3 ; j ++)
			{
				for (int k = 0 ; k<3 ; k++)
				{
					sum = sum + a[i][k] * b[k][j];           //矩陣相乘
				}
				c[i][j] = sum ;                              //值放到C矩陣
				sum = 0;
			}
		}
        for (int i = 0 ; i <3 ; i ++)
        {
        	for (int j = 0 ; j <3 ; j++)
        	{
        		a[i][j] = c[i][j] ;                          //C矩陣再放回A矩陣
        	}
        }
	}

}
