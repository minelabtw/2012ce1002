package ce1002.s101502016;

import java.util.*;

public class A3 {
	
	public static void multiplication(int[][] X)
	{
	int result=0;
		
		int[][] B = {
				{7,5,3},
				{1,4,9},
				{4,2,6},
		};
		
		
		for(int i=0 ; i<3 ; i++)
		{
			for(int j=0,a=0 ; j<3 ; j++,a++)//a用來換行 i,j來控制位置,k用來計算
			{	
				for(int k=0 ; k<3 ; k++)
				{
					result += (X[i][k]*B[k][j]);//此為矩陣的計算公式
				}
				
				if(a%3==0)
					System.out.println(" ");//換行
				
				System.out.print(result+" ");
				result=0;
				
			}
		}
		
	}
	////////////////////////////////////////////////////
	public static void main(String[] args){
	
		Scanner input = new Scanner(System.in);
		
		System.out.println("Input matrix A: ");
		
		int[][] A = new int[3][3];
		for(int i=0 ; i<3 ; i++)//輸入A之矩陣
		{
			for(int j=0 ; j<3 ; j++)
			{
				A[i][j] = input.nextInt();	
			}
		}
		
		System.out.print("\n");
		System.out.print("After: ");
		
		multiplication(A);
		
	}
}
