package ce1002.A3.s101502029;

import java.util.*;

public class A3 {
	public static int[][] multiplication(int a[][]) { // 將A陣列輸入函示裡
		int b[][] = { { 7, 5, 3 }, { 1, 4, 9 }, { 4, 2, 6 } };// B陣列
		int ab[][] = new int[3][3];// A陣列和B陣列的乘積
		// 計算過程
		for (int i = 0; i < 3; i++) {
			for (int j = 0; j < 3; j++) {
				for (int k = 0; k < 3; k++) {
					ab[i][j] += (a[i][k] * b[k][j]);
				}
			}
		}
		return ab;
	}

	public static void main(String[] args) {
		Scanner place = new Scanner(System.in);
		int[][] array = new int[3][3];
		System.out.println("Input matrix A: ");
		int AB[][];// AB為計算結果
		// 將A陣列輸入到array
		for (int i = 0; i < 3; i++) {
			for (int j = 0; j < 3; j++) {
				array[i][j] = place.nextInt();
			}
		}
		System.out.println("\nAfter:");
		AB = multiplication(array); // 將array回傳multiplication函示裡
		// 將結果AB列印出來
		for (int i = 0; i < 3; i++) {
			for (int j = 0; j < 3; j++) {
				System.out.print(AB[i][j] + " ");
			}
			System.out.println("");
		}
	}
}
