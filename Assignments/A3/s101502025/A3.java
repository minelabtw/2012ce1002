package ce1002.A3.s101502025;

import java.util.Scanner;

public class A3 {
	public static void main(String[] args) {
		Scanner t=new Scanner(System.in);
		int [][] ass=new int[3][3]; // the array be input
		System.out.println("Input matrix A:");
		for(int a=0;a<ass.length;a++) // input and save the array
		{
			for(int j=0;j<ass[a].length;j++)
			{
				ass[a][j]=t.nextInt();
			}
		}
		System.out.println(" ");
		System.out.println("After:  ");
		res(ass); // calculate the resault
	}
	
	public static void res(int [][] abc){
		int [][] B=new int[3][3]; // the array "B"
		B[0][0]=7;
		B[0][1]=5;
		B[0][2]=3;
		B[1][0]=1;
		B[1][1]=4;
		B[1][2]=9;
		B[2][0]=4;
		B[2][1]=2;
		B[2][2]=6;
		for(int g=0;g<3;g++) // the number of the rows
		{
			int sum1=0,sum2=0,sum3=0;
			int h;
			for(h=0;h<3;h++) // the number of the columns
			{
				sum1=sum1+(abc[g][h]*B[h][0]);
				sum2=sum2+(abc[g][h]*B[h][1]);
				sum3=sum3+(abc[g][h]*B[h][2]);
			}
			System.out.print(sum1+" "+sum2+" "+sum3); // print the resaults
			System.out.println(" ");
		}
	}
}