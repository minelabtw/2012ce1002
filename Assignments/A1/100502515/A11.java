package a1.s100502515;

import java.util.Scanner;

public class A11 {

	public static void main(String[] args) {
		A11 triangle = new A11();
		@SuppressWarnings("resource")
		Scanner scanner = new Scanner(System.in);
		System.out.print("Enter x = ");
		double x = scanner.nextDouble();
		System.out.print("Enter y = ");
		double y = scanner.nextDouble();
		System.out.print("Enter z = ");
		double z = scanner.nextDouble();//Enter the length if three sides.
		if (triangle.checkIfValid(x, y, z)) {
			System.out.println("The triangle xyz is valid.");
		}
		else {
			System.out.println("The triangle xyz is invalid.");
		}//Ouput the result of the check

	}

	private boolean checkIfValid(double x, double y, double z) {
		if ((x + y) > z && (x + z) > y && (y + z) > x) {
			return true;
		} else {
			return false;
		}
	}//Check if the triangle is valid.
}
