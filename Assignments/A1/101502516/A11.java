//得自行將移標移動到後方再進行輸入
package ce1002.s101502516;
import java.util.Scanner;

public class A11 {
	 public static void main(String[] args){
		 double x,y,z;
		
		 System.out.print("x="); //輸出訊息、輸入一邊長
		 Scanner input1 = new Scanner(System.in);
		 x = input1.nextDouble();

		 System.out.print("y="); //輸出訊息、輸入一邊長
		 Scanner input2 = new Scanner(System.in);
		 y = input2.nextDouble();

		 System.out.print("z="); //輸出訊息、輸入一邊長
		 Scanner input3 = new Scanner(System.in);
		 z = input3.nextDouble();
		 
		 if ( x + y <= z || x + z <= y || y + z <= x ) //判斷三邊長能否構成三角形
			 System.out.println("triangle xyz is invalid.");
		 else
			 System.out.println("triangle xyz is valid.");
	 }
}