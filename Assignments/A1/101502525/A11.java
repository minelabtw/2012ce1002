package ce1002.s101502525;

import java.util.Scanner;

public class A11 {
	public static void main(String[] args){
		int[] side=new int[3];
		char name='x';//side name
		for(int i=0;i<3;i++){
			System.out.printf("%c=",name++);
			side[i]=new Scanner(System.in).nextInt();//input
		}
		System.out.println("triangle xyz is "+
							(side[0]+side[1]<=side[2]||
							side[0]+side[2]<=side[1]||
							side[1]+side[2]<=side[0]?"in":"")+"valid.");//judge & output
	}
}
