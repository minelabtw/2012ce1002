package ce1002.s100502203;

import java.util.Scanner;

public class A11 {
	public static void main(String[] args) {
		int x, y, z;
		Scanner input = new Scanner(System.in);
		System.out.print("x=");
		x = input.nextInt();
		System.out.print("y=");
		y = input.nextInt();
		System.out.print("z=");
		z = input.nextInt();

		int max = x;	// to find the maximum
		if (y > max)
			max = y;
		if (z > max)
			max = z;
		
		int total = x+ y+ z;
		boolean check = false;
		if((total-max) > max)	// a + b > c (c is the maximum) to check
			check = true;

		if (check == true)
			System.out.printf("triangle xyz is valid.");
		else
			System.out.print("triangle xyz is invalid.");
	}
}
