package ce1002.s101502020;
//A11 by J
import java.util.Scanner ;

public class A11 {

	public static void main(String[] args) {
		Scanner input=new Scanner(System.in) ;
		
		System.out.print( "x=" ) ;
		int x=input.nextInt() ;
		
		System.out.print( "y=" ) ;
		int y=input.nextInt() ;
		
		System.out.print( "z=" ) ;
		int z=input.nextInt() ;
		
		if ( x+y>z && z>x && z>y ){
			System.out.print( "triangle xyz is valid." ) ;
		}
		else if ( x+z>y && y>x && y>z ){
			System.out.print( "triangle xyz is valid." ) ;
		}
		else if ( y+z>x && x>y && x>z ){
			System.out.print( "triangle xyz is valid." ) ;
		}
		else {
			System.out.print( "triangle xyz is invalid." ) ;
		}
		
		/*
		 if (x+y<=z || x+z<=y || y+z<=x)
		 {print invalid}
		 else
		 {print valid}
		 
		 brilliant, genius Chen!
		 */

	}

}
