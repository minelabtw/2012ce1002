package ce1002.s101502501;

import java.util.Scanner;

public class A11 {
	public static void main( String[] args)
	{
		Scanner cin = new Scanner( System.in );
		
		System.out.print("x=");
		int x = cin.nextInt();//輸入x
		System.out.print("y=");
		int y = cin.nextInt();//輸入y
		System.out.print("z=");
		int z = cin.nextInt();//輸入z
		
		int buffer=0;
		
		if( x > y)//把xyz從小排到大
		{
			buffer = x;
			x = y;
			y = buffer;
		}
		if( x > z )
		{
			buffer = x;
			x = z;
			z = buffer;
		}
		if( y > z)
		{
			buffer = y;
			y = z;
			z = buffer;
		}
		
		if (x+y>z)//三邊可成為三角形
		{
			System.out.println("triangle xyz is valid.");
		}
		else
		{
			System.out.println("triangle xyz is invalid.");
		}
	}
}
