package ce1002.s101502023;

import java.util.Scanner;

public class A11
{
	public static void main(String[] args)
	{
		
		int x;
		int y;
		int z;
		Scanner input = new Scanner(System.in);
	    
		System.out.print("x=");
		x=input.nextInt();
		
		System.out.print("y=");
	    y=input.nextInt();
	    
	    System.out.print("z=");
	    z=input.nextInt();
	    
	    if(x+y>z && x+z>y && y+z>x)//to test whether three numbers could form the triangle or not.
	    	System.out.println("trangle xyz is valid.");	    
	    else
	    	System.out.println("trangle xyz is invalid.");
	}

}
