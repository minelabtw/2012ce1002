import java.util.Scanner;
public class A11 {
	public static void main (String[] args)
	{
		while(true)
		{
			double x,y,z; // 設x>y>z的三角形三個邊
			Scanner input = new Scanner( System.in );
			System.out.print("x=");
			x = input.nextDouble();
			System.out.print("y=");
			y = input.nextDouble();
			System.out.print("z=");
			z = input.nextDouble();
			double buffer;
			if ( z > y ) // 如果 z>y 則調換兩者
			{
				buffer = z;
				z = y;
				y = buffer;
			}
			if ( x < y ) // 如果 y>x 則調換兩者
			{
				buffer = y;
				y = x;
				x = buffer;
			}
			if ( x < y+z ) // 兩邊之和大於第三邊 則三角形成立
				System.out.println("trangle xyz is valid.");
			else // 兩編之和小於第三邊 則三角形不成立
				System.out.println("trangle xyz is invalid.");
		}
	}
}
