package ce1002.s101502021;
import java.util.Scanner;

public class A11 {
	public static void main(String[] args){
		
		int A,B,C;
		Scanner input = new Scanner(System.in);
	    
		System.out.print("A=");
		A=input.nextInt();
		System.out.print("B=");
	    B=input.nextInt();
	    System.out.print("C=");
	    C=input.nextInt();
	    
	    //用三邊長的關係判斷是否為三角形
	    if(A+B>C && A+C>B && B+C>A)
	    	System.out.println("trangle ABC is valid.");
	    
	    else
	    	System.out.println("trangle ABC is invalid.");
	}

}
