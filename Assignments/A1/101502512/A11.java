package ce1002.s101502512;

import java.util.Scanner;

public class A11 {
	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);
		int x, y, z;// 三個邊
		System.out.print("x=");
		x = input.nextInt();
		System.out.print("y=");
		y = input.nextInt();
		System.out.print("z=");
		z = input.nextInt();

		if (x + z > y && x + y > z && y + z > x)// 符合三角形的條件
			System.out.println("triangle xyz is valid.");
		else
			// 不符合
			System.out.println("triangle xyz is invalid.");
	}
}