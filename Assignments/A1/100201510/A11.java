package ce1002.s100201510;

import java.util.Scanner ;

public class A11 {
	public static void main(String[] args) {
		Scanner number = new Scanner(System.in);
		System.out.print("x=");
		int s1 = number.nextInt();
		System.out.print("y=");
		int s2 = number.nextInt();
		System.out.print("z=");
		int s3 = number.nextInt();
		
		if(s1 + s2 > s3 && s2 + s3 > s1 && s3 + s1 > s2)
			System.out.printf("%s\n", "trangle xyz is valid.");
		else
			System.out.printf("%s\n", "trangle xyz is invalid.");
	}
}