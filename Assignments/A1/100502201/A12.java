//A12
//Sum of three dices
package ce1002.s100502201;

public class A12 {
	public static void main(String[] args){
		int x = (int)(Math.random() * 16) + 3; //Since it only shows result, we should know that the result range from 3 to 18. Use int before Math.random() is to make sure that the outcome is an integer.
		System.out.print("shibara = " + x);
	}
}
