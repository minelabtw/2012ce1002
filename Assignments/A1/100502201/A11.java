//A11
//Judge whether sides of triangle are valid or not
package ce1002.s100502201;
import java.util.Scanner;

public class A11 {
	public static void main(String[] args){
		double x,y,z;
		Scanner a=new Scanner(System.in); //Construct Scanner
		System.out.println("x=");
		x= a.nextDouble(); //x's input
		Scanner b=new Scanner(System.in);
		System.out.println("y=");
		y= b.nextDouble();//y's input
		Scanner c=new Scanner(System.in);
		System.out.println("z=");
		z= c.nextDouble();//z's input
		if (x+y>z&&y+z>x&&x+z>y){ //Two-side sum must be larger than 3rd side
			System.out.println("triangle xyz is valid.");
		}
		else {
			System.out.println("triangle xyz is invalid.");
		}
	}
}
