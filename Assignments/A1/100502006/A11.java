package ce1002.s100502006;

import java.util.Scanner;

public class A11 {
	static Scanner input = new Scanner(System.in);
	public static void main(String[] args){
		double x=0,y=0,z=0,temp=0;//variable to store sides
		System.out.print("x=");
		x = input.nextDouble();
		System.out.print("y=");
		y = input.nextDouble();
		System.out.print("z=");
		z = input.nextDouble();
		if(x>y){//sort the sides
			temp=x;
			x=y;
			y=temp;
		}
		if(y>z){//sort the sides
			if(x>z){
				temp=z;
				z=y;
				y=x;
				x=temp;
			}
			else{
				temp=z;
				z=y;
				y=temp;
			}
		}
		if(x+y>z&&z-x<y){//determine if the sides can form a triangle
			System.out.println("triangle xyz is valid.");
		}
		else {
			System.out.println("triangle xyz is invalid");
		}
	}
}
