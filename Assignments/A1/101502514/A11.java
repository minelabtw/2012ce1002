/*有時候執行時需把游標移到後面*/
package ce1002.s101502514;
import java.util.Scanner;
public class A11 {
	public static void main(String[] args){
		Scanner input=new Scanner(System.in);
		System.out.print("x=");
		int a=input.nextInt();//輸入
		System.out.print("y=");
		int b=input.nextInt();//輸入
		System.out.print("z=");
		int c=input.nextInt();//輸入
		
		if(a+b<=c||a+c<=b||b+c<=a)
			System.out.println("triangle xyz is invalid.");//不是三角形
		else
			System.out.println("triangle xyz is valid.");
	}
}
