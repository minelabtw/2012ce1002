package ce1002.s101502502;

import java.util.Scanner;

public class A11 
{
	public static void main(String[] args)
	{		
		Scanner cin = new Scanner(System.in);//自訂輸入
		
		int buffer;
		
		System.out.print("x=");
		int num1 = cin.nextInt();
		System.out.print("y=");
		int num2 = cin.nextInt();
		System.out.print("z=");
		int num3 = cin.nextInt();//輸入三邊長
		
//////////////////////排序////////////////////
		
		if (num1 > num2)
		{
			buffer = num1;
			num1 = num2;
			num2 = buffer;
		}
		
		if (num1 > num3)
		{
			buffer = num1;
			num1 = num3;
			num3 = buffer;
		}
		
		if (num2 > num3)
		{
			buffer = num2;
			num2 = num3;
			num3 = buffer;
		}
		
		
		if(num1 + num2 <= num3)//不成形成三角形的情況
			System.out.println("triangle xyz is invalid.");
		else//三角形
			System.out.println("triangle xyz is valid.");
		
	}

}
