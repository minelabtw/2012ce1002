package ce1002.s101522071;

import java.util.Scanner; // import Scanner class

public class A1 {
	public static double toFahrenheit(double input){ // define method toFahrenheit to change input from Celcius to Fahrenheit
		double answer = (input*9)/5 + 32;
		return answer;
	}
	
	public static double toCelcius(double input){ // define method toCelcius to change input from Fahrenheit to Celcius
		double answer = ((input - 32)*5)/9;
		return answer;
	}
	
	public static void main(String[] args){
		Scanner input = new Scanner(System.in); // new a Scanner
		
		int userChoice;
		double temperature;
		double answer;
		boolean exitFlag = false;
		
		while(!exitFlag){
			System.out.println("Please choose the method you want to use:\n1.toFahrenheit\n2.toCelcius\n3.Exit ");
			userChoice = input.nextInt();
			
			switch(userChoice){
				case 1: // call method toFahrenheit
					System.out.println("Please input the temperature: ");
					temperature = input.nextDouble();
					answer = toFahrenheit(temperature);
					
					System.out.println(temperature + " in Celcius is equal to " + answer + " in Fahrenheit.");
					System.out.println("");
					break;
				case 2: // call method toCelcius
					System.out.println("Please input the temperature: ");
					temperature = input.nextDouble();
					answer = toCelcius(temperature);
					
					System.out.println(temperature + " in Fahrenheit is equal to " + answer + " in Celcius.");
					System.out.println("");
					break;
				case 3: // exit
					exitFlag= true;
					
					System.out.println("Good Bye");
					break;
				default:
					break;
			}
		}
	}
}