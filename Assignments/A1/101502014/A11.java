package ce1002.s101502014;
import java.util.Scanner;
public class Q11 {
	public static void main(String[] args) {
		Scanner cin = new Scanner(System.in); //定義cin為輸入的功能
		while(true)
		{
			int x , y , z;
			System.out.print("x="); //輸入x
			x = cin.nextInt();
			System.out.print("y="); //輸入y
			y = cin.nextInt();
			System.out.print("z="); //輸入z
			z = cin.nextInt();
			System.out.println();
			if(x + y > z && y + z > x && x + z > y) //判斷是否為三角形
				System.out.println("trangle xyz is valid.");
			else
				System.out.println("trangle xyz is invalid.");
		}
	}
}