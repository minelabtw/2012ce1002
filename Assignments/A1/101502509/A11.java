package ce1002.s101502509;

import java.util.Scanner;

public class A11 {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		int x,y,z;
		Scanner input=new Scanner(System.in);
		
		System.out.print("x=");
		x=input.nextInt();        //Get x
		
		System.out.print("y=");
		y=input.nextInt();        //Get y
		
		System.out.print("z=");
		z=input.nextInt();        //Get z
		
		/*according to the fact of triangle,choose any two sides of triangle,
		  the length sum is bigger than the other third side length.
		  If the triangle can't coincide the fact,it is invalid triangle.  
		*/
		if(x+y>z && y+z>x && x+z>y)       
	     	System.out.print("triangle xyz is valid.");
	    else
	    	System.out.print("triangle xyz is invalid.");

	}

}
