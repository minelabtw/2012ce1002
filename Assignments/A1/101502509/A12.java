/*output result:
    Do you throw three dies ? ( Win => (sum > 10) )
	1. Yes
	2. NO
	1
	shibara = 12
	Congratulation! You Win.
	
	Do you throw three dies ? ( Win => (sum > 10) )
	1. Yes
	2. NO
	1
	shibara = 7
	You Lose.
	
	Do you throw three dies ? ( Win => (sum > 10) )
	1. Yes
	2. NO
	2
	See you next time!
 */

package ce1002.s101502509;

import java.util.Scanner;

public class A12 {
	public static int randomDie(){     //generate a die number at random 
		return (int)(Math.random()*18)%6+1;
	}
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		int choice;
		do{
			Scanner input = new Scanner(System.in);
			System.out.println("Do you throw three dies ? ( Win => (sum > 10) )");
			System.out.println("1. Yes");
			System.out.println("2. NO");
			choice = input.nextInt();    //Get your choice
			switch(choice){
				case 1:  //choose 1,and play the game
					int ranDie1 = randomDie();    //random die1
					int ranDie2 = randomDie();    //random die2
					int ranDie3 = randomDie();    //random die3
					int sum = ranDie1+ranDie2+ranDie3;
					System.out.println("shibara = "+sum);
					
					if(sum > 10){                //sum > 10,the user wins the game
						System.out.println("Congratulation! You Win.");
					} 
					else if(sum <= 10) {         // sum <= 10,the user loses the game
						System.out.println("You Lose.");
					}
					break;
			
				case 2: //exit the game
					System.out.println("See you next time!");
					break;
			}
			System.out.println();
		}while(choice != 2);	
	}
}
