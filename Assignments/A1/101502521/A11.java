package ce1002.s101502521;

import java.util.Scanner;

public class A11 {
	public static void main(String[] args)
	{
		System.out.print("x=");
		Scanner input = new Scanner(System.in);
		int x = input.nextInt();
		System.out.print("y=");
		int y = input.nextInt();
		System.out.print("z=");
		int z = input.nextInt();
		int tmp;
		if(x<=0||y<=0||z<=0) // can't be negative
		{
			System.out.println("trangle xyz is invalid.");
			return;
		}
		if(x>y)
		{
			tmp = x;
			x = y;
			y = tmp;
		}
		if(y>z)
		{
			tmp = y;
			y = z;
			z = tmp;
		}
		if(x>y)
		{
			tmp = x;
			x = y;
			y = tmp;
		}
		// sort three number
		if(x+y<=z) // the longest edge should longer than the sum of others
			System.out.println("trangle xyz is invalid.");
		else
			System.out.println("trangle xyz is valid.");
	}
}
