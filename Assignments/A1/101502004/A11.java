package ce1002.s101502004;

import java.util.Scanner;
public class A11 {
	public static void main(String[] args){
		Scanner inp = new Scanner(System.in);//輸入
		System.out.println("x=");//顯示"x="
		int x = inp.nextInt();//輸入x
		System.out.println("y=");//顯示"y="
		int y = inp.nextInt();//輸入y
		System.out.println("z=");//顯示"z="
		int z = inp.nextInt();//輸入z
		
		if(x+y>z && y+z>x && x+z>y){//判別是否為三角形
			System.out.println("trangle xyz is valid.");//輸出
		}
		else{//若非三角形
			System.out.println("trangle xyz is invalid.");//輸出
		}
	}
}
