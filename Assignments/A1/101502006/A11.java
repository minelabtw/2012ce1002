package ce1002.s101502006;
import java.util.Scanner; // program uses class Scanner 

public class A11 {
	public static void main(String[] args)
	{
		// create Scanner to obtain input from command window
		Scanner input = new Scanner( System.in ); 
		
		int x,y,z;
		
		System.out.print( "x=" ); 
		x=input.nextInt();

		System.out.print( "y=" ); 
		y=input.nextInt();
		
		System.out.print( "z=" ); 
		z=input.nextInt();

		if(x+y<=z || y+z<=x || x+z<=y) //wrong condition
			System.out.printf( "triangle xyz is invalid." ); 
		else
			System.out.printf( "triangle xyz is valid." ); 
		
	}	
}
