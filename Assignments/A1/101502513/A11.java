package ce1002.s101502513;

import java.util.Scanner; //allow program to perform input

public class A11 {
	public static void main(String[] args) {
		
		int x, y, z;
		Scanner input = new Scanner(System.in);
		
		System.out.print("x=");
		x = input.nextInt(); //input x
		System.out.print("y=");
		y = input.nextInt(); //input y
		System.out.print("z=");
		z = input.nextInt(); //input z
		
		//determine if triangle xyz is valid or not
		if( x + y > z && x + z > y && y + z > x )
			System.out.println("triangle xyz is valid.");
		else
			System.out.println("triangle xyz is invalid.");
	}
}
