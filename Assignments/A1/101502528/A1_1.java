package ce1002.s101502528;

import java.util.Scanner;

public class A1_1 {
	public static void main (String[] args){
		int x,y,z,buf;
		System.out.print("x=");
		Scanner input=new Scanner(System.in);
		x=input.nextInt();
		
		System.out.print("y=");
		y=input.nextInt();
		
		System.out.print("z=");
		z=input.nextInt();
		
		//排序↓
		if(x<y){
			buf=x;
			x=y;
			y=buf;
		}
		
		if(x<z){
			buf=x;
			x=z;
			z=buf;
		}
		//排序↑
		
		//三角形判斷與輸出
		if(x<y+z)
			System.out.print("triangle xyz is valid.");
		else
			System.out.print("triangle xyz is invalid.");
		//三角形判斷與輸出
	}
}
