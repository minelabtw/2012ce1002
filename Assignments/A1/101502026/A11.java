package ce1002.s101502026;

import java.util.Scanner;
import java.util.Arrays;

public class A11
{
	public static void main(String[] args)
	{
		while(true)
		{
			double length[]=new double[3]; //declare an array sized 3
			Scanner x=new Scanner(System.in);
			System.out.print("x=");
			length[0]=x.nextDouble();
			System.out.print("y=");
			length[1]=x.nextDouble();
			System.out.print("z=");
			length[2]=x.nextDouble();
			Arrays.sort(length); //sort the array from smallest to largest numbers
			if(length[0]+length[1]>length[2])
				System.out.println("triangle xyz is valid.");
			else
				System.out.println("triangle xyz is invalid.");
		}
	}
}
