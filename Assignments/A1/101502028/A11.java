package ce1002.s101502028;
import java.util.*; // import to use scanner
public class A11 {
	public static void main (String[] args){ // main function
		Scanner side = new Scanner (System.in); // scanner object named "side"
		int x, y, z; // three different integrals
		System.out.print("x="); // message
		x = side.nextInt(); // input the x integral
		System.out.print("y="); // message
		y = side.nextInt(); // input the y integral
		System.out.print("z="); // message
		z = side.nextInt(); // input the z integral
		if (x + y > z && x + z > y && y + z > x) // find if three sides can be a triangle
			System.out.print("triangle xyz is valid."); // message
		else // if not a triangle
			System.out.print("triangle xyz is invalid."); // message
	} // end program

}
