package ce1002.s100502205;

import java.util.Scanner;

public class A11 {
	public static void main(String[] args) {
		Scanner cin = new Scanner(System.in);
		double x, y, z;
		boolean valid;
		System.out.print("x=");
		x = cin.nextDouble();
		System.out.print("y=");
		y = cin.nextDouble();
		System.out.print("z=");
		z = cin.nextDouble();
		valid = (x + y > z) && (y + z > x) && (x + z > y);
		System.out.println("triangle xyz is " + (valid ? "" : "in") + "valid.");
	}
}
