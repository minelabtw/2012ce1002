package ce1002.s101502510;

import java.util.Scanner;

public class A11 {
	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);
		int x, y, z;

		System.out.print("x=");
		x = scanner.nextInt();// 輸入x
		System.out.print("y=");
		y = scanner.nextInt();// 輸入y
		System.out.print("z=");
		z = scanner.nextInt();// 輸入z

		if (x + y > z && x + z > y && y + z > x) {// 檢測三邊是否能成三角形
			System.out.println("trangle xyz is valid.");
		} else {// 若三邊不能成三角形
			System.out.println("trangle xyz is invalid.");
		}
	}
}