package ce1002.s101502504;
import java.util.Scanner;

public class A11
{
	public static void main(String[] args)
	{
		int x,y,z;
		int buffer=0;
		Scanner  input = new Scanner(System.in);
		
		System.out.print("x=");//user input one value
		x = input.nextInt();//store in x
		
		System.out.print("y=");//user input one value
		y = input.nextInt();//store in y
		
		System.out.print("z=");//user input one value
		z = input.nextInt();//store in z
		
		for(int i=0; i<3; i++)//use loop to let x < y < z
		{
			if(x>y)
			{
				buffer=x;
				x=y;
				y=buffer;
			}
			buffer=0;
			if(y>z)
			{
				buffer=y;
				y=z;
				z=buffer;
			}
		}
		
		if( (x+y) > z )//can be a triangle
			System.out.println("triangle xyz is valid.");
		else//can't be a triangle
			System.out.println("triangle xyz is invalid.");
	}
}