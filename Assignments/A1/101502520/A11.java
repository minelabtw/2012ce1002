package ce1002.s101502520;

import java.util.Scanner;
import java.util.Arrays;


public class A11 {

	/**
	 * @param args
	 */
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner input = new Scanner(System.in);
		
		int[] side;
		side = new int[3];
		System.out.print("x=");
		side[0] = input.nextInt();
		
		System.out.print("y=");
		side[1] = input.nextInt();
		
		System.out.print("z=");
		side[2] = input.nextInt();
		
		input.close();
		
		for(int i = 0; i < 3; i++)
			if(side[i] <= 0)
			{
				System.out.println("triangle xyz is invalid.");
				return;
			}
		
		Arrays.sort(side);//sort the sides
		
		if(side[0] + side[1] > side[2])//if small + small > big then it can be triangle
			System.out.println("triangle xyz is valid.");
		else {
			System.out.println("triangle xyz is invalid.");
		}
		
		return;

	}

}
