package ce1002.s101502523;
import java.io.*;
public class A11 {
	public static void main(String[] args) throws IOException{
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		String str;
		int x, y, z;
		//input x
		System.out.print("x=");
		str = br.readLine();
		x = Integer.parseInt(str);	
		//input y
		System.out.print("y=");
		str = br.readLine();
		y = Integer.parseInt(str);
		//input z
		System.out.print("z=");
		str = br.readLine();
		z = Integer.parseInt(str);
		//triangle is valid or not
		if(x+y>z&&x+z>y&&y+z>x)
			System.out.println("triangle xyz is valid.");
		else
			System.out.println("triangle xyz is invalid.");
	}
}
