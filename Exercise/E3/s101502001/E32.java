package ce1002.E3.s101502001;
import java.util.Scanner;
import ce1002.E3.s101502001.queue;

public class E32 {
	public static void main(String[]args){
		boolean exit=false;
		queue q=new queue();
		while(!exit){
			Scanner input=new Scanner(System.in);
			System.out.println("1: push");
			System.out.println("2: pop");
			System.out.println("3: show");
			System.out.println("4: exit");
			int a=input.nextInt();
			if(a==1){
				System.out.print("push ");
				int b=input.nextInt();
				for(int i=0;i<b;i++){
					q.push(input.nextInt());
				}
				System.out.print("\n");
			}
			if(a==2){
				System.out.print("pop ");
				int c=input.nextInt();
				for(int k=0;k<c;k++){
					q.pop();
				}
				System.out.print("\n");
			}
			if(a==3){
				System.out.print("show ");
				q.show();
				System.out.print("\n");
			}
			if(a==4){
				System.out.println("Good Bye");
				exit=true;
				break;
			}
		}
	}
}
