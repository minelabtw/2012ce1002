package ce1002.E3.s101502010;
import ce1002.E3.s101502010.stack;
import java.util.Scanner;

public class E31 {
	public static void main(String[] args){
		stack _stack=new stack();
		Scanner input=new Scanner(System.in);
		System.out.print("1: push\n2: pop\n3: show\n4: exit\n");
		while(true){
			int method=input.nextInt();
			if(method==1){//接續陣列
				System.out.print("push ");
				int num=input.nextInt();
				_stack.push(num);
			}
			else if(method==2){//從嘿面刪除陣列
				System.out.print("pop ");
				int num=input.nextInt();
				_stack.pop(num);
			}
			else if(method==3){
				_stack.show();
			}
			else if(method==4)
				break;
			else
				System.out.println("Please enter a method again!\n");
			System.out.print("1: push\n2: pop\n3: show\n4: exit\n");
		}
		System.out.println("Good Bye");
	}//end of main
}
