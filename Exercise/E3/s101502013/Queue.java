package ce1002.E3.s101502013;

public class Queue {
	int[][] queue;
	//第一維是輸入數字 第二維則是表示此數字是否有效  若為0則無效 為1則表示有效(模擬queue)
	
	Queue()
    {
		;//something initialize
    }
    Queue(int input)
    {
    	queue = new int[input][2];
    }
	void push(int input)
	{
		int pointer=-1;//pointer表示陣列Queue第一維的第幾個開始為有效資料
		for (int i=(queue.length-1);i>=0;i--)//queue.length=100  抓出pointer
		{
			if (queue[i][1]==1)
			{
				pointer=i;
				break;
			}
		}
	
		if (pointer<queue.length)
		{
			queue[pointer+1][0]=input;//將資料存入
			queue[pointer+1][1]=1;//標示為有效資料
		}
		else
		{
			System.out.println("The Queue is overflow.");
		}
	}
	int pop()
	{
		int pointer = -1;
		for (int i=(queue.length-1);i>=0;i--)//抓出pointer
		{
			if (queue[i][1]==1)
			{
				pointer=i;
				break;
			}
		}
		if (pointer == -1)
		{	
			System.out.print("Queue is empty! The integer returned is invalid!");
			return 0;
		}
		else
		{
			int buffer=queue[0][0];
			queue[pointer][1]=0;
			for (int j=0;j<pointer;j++)//將資料向前移
			{
				queue[j][0]=queue[j+1][0];
			}
			return buffer;	
		}
	}
		
	void show()
	{
		int pointer=-1;
		for (int i=(queue.length-1);i>=0;i--)//抓出pointer
		{
			if (queue[i][1]==1)
			{
				pointer=i;
				break;
			}
		}
		for (int i=0;i<=pointer;i++)
			System.out.print(queue[i][0] + " ");		
	}
	

}