package ce1002.E3.s982001011;

public class Queue {
	private int _size;   		//現有陣列長度
	private int head, tail;		//陣列頭尾
	private int[] data;			

	public Queue(int size, int val) {
		data = new int[10];
		head = 0;
		tail = size - 1;
		_size = size;
		for (int i = 0; i < size; ++i) {
			data[i] = val;
		}
	}

	public Queue() {
		_size = 0;
		head = 0;
		tail = 99;
		data = new int[100];
	}

	public void push(int n) {  //push
		++_size;
		tail = (tail + 1) % 100;
		data[tail] = n;
	}

	public int pop() {		//pop
		if (!empty()) {
			--_size;
			int n = data[head];
			head = (head + 1) % 100;
			return n;
		} else {
			return -1;
		}
	}

	public boolean empty() {		//判斷是否為空
		return tail == (head - 1) % 100;
	}

	public int[] show() {			//function 3
		int[] rtn = new int[_size];
		for (int i = 0; i < _size; ++i) {
			rtn[i] = data[(head + i) % 100];
		}
		return rtn;
	}

	public int size() {       
		return _size;
	}
}
