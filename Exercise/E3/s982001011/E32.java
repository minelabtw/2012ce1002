package ce1002.E3.s982001011;

import java.util.Scanner;
import ce1002.E3.s982001011.Queue;

public class E32 {
	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);
		int flag;

		Queue myQueue = new Queue();
		do {
			System.out.print("1: push\n2: pop\n3: show\n4: exit\n");
			System.out.print("�п�J�ﶵ\n");
			flag = input.nextInt();
			int n;
			switch (flag) {
			case 1:
				System.out.print("push ");
				n = input.nextInt();
				for (int i = 0; i < n; ++i) {
					myQueue.push(input.nextInt());
				}
				System.out.println();
				break;
			case 2:
				System.out.print("pop ");
				n = input.nextInt();
				for (int i = 0; i < n; ++i) {
					if (!myQueue.empty())
						System.out.print(myQueue.pop() + " ");
					else {
						System.out.print("\nQueue is empty!");
						break;
					}
				}
				System.out.println();
				break;
			case 3:
				int[] nums = myQueue.show();
				if (myQueue.empty()) {
					System.out.print("Queue is empty!");
				} else {
					for (int i = 0; i < myQueue.size(); ++i) {
						System.out.print(nums[i] + " ");
					}
				}
				System.out.println();
				break;
			case 4:
				System.out.println("Good bye!");
				break;
			default:
				break;
			}
			System.out.println();

		} while (flag != 4);
	}

}
