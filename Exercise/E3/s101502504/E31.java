package ce1002.E3.s101502504;
import java.util.Scanner;
public class E31
{
	public static void main(String[] args)
	{
		stack s = new stack();//call stack
		Scanner input = new Scanner(System.in);
		int exit=0;
		while(exit==0)
		{
			System.out.println("1: push");
			System.out.println("2: pop");
			System.out.println("3: show");
			System.out.println("4: exit");
			int number = input.nextInt();//decide 1 2 3 4 which to choose	
			
			if(number==1)//user push
				s.push();

			if(number==2)//user pop
				s.pop();

			if(number==3)//show the array
				s.show();

			if(number==4)//exit
			{
				s.exit();
				exit=1;
			}
		}
	}
}
