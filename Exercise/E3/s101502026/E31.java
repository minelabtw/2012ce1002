package ce1002.E3.s101502026;

import java.util.Scanner;
import ce1002.E3.s101502026.stack; //import the file "stack" from package ce1002.E3.s101502026

public class E31
{
	public static void main(String[] args)
	{
		stack stack = new stack(); //declare an object
		int input,quantity,pop_number;
		while(true)
		{
			Scanner scanner = new Scanner(System.in);
			System.out.print("1: push\n2: pop\n3: show\n4: exit\n");
			input = scanner.nextInt();
			if(input==1)
			{
				System.out.print("push ");
				quantity=scanner.nextInt();
				stack.push(quantity);
				stack.arrayNumberCount(quantity); //call the method arrayNumberCount
				System.out.println();
			}
			else if(input==2)
			{
				System.out.print("pop ");
				pop_number=scanner.nextInt();
				boolean right = stack.pop(pop_number);
				if(right==false)
				{
					System.out.print("\nStack is empty!\n\n");
					stack.array_number=0;
					continue;
				}
				else
				{
					stack.arrayNumberSubtract(pop_number);
					System.out.println("\n");
				}
			}
			else if(input==3)
			{
				stack.show();
			}
			else
			{
				System.out.println("Good Bye");
				break;
			}
		}
	}
}
