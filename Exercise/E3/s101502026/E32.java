package ce1002.E3.s101502026;

import java.util.Scanner;
import ce1002.E3.s101502026.queue; //import the file "queue" from package ce1002.E3.s101502026

public class E32
{
	public static void main(String[] args)
	{
		queue queue = new queue(); //declare an object "queue"
		int input,quantity,pop_number;
		while(true)
		{
			Scanner scanner = new Scanner(System.in);
			System.out.print("1: push\n2: pop\n3: show\n4: exit\n");
			input = scanner.nextInt();
			if(input==1)
			{
				System.out.print("push ");
				quantity=scanner.nextInt();
				queue.push(quantity); //call the method "push"
				queue.arrayNumberCount(quantity);
				System.out.println();
			}
			else if(input==2)
			{
				System.out.print("pop ");
				pop_number=scanner.nextInt();
				boolean right = queue.pop(pop_number);
				if(right==false)
				{
					System.out.print("\nQueue is empty!\n\n");
					continue;
				}
				else
				{
					for(int i=0;i<queue.array_number-pop_number;i++)
					{
						queue.queueData[i]=queue.queueData[i+pop_number];
					}
					queue.arrayNumberSubtract(pop_number);
					System.out.println("\n");
				}
			}
			else if(input==3)
			{
				if(queue.queueData[0]==0)
				{
					System.out.println("Queue is empty!\n");
					continue;
				}	
				else
				{
					queue.show();
					System.out.println("\n");
				}
			}
			else
			{
				System.out.println("Good Bye");
				break;
			}
		}
	}
}
