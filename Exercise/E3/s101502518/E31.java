package CE1002.E3.s101502518;

import java.util.Scanner;
import CE1002.E3.s101502518.stack;

public class E31 {
	public static void main(String[] args)
	{
		int pushlength = 0, poplength = 0,x = 0;
		stack sta = new stack();
		while(true)
		{
			System.out.println("1: push");
			System.out.println("2: pop");
			System.out.println("3: show");
			System.out.println("4: exit");
			Scanner input = new Scanner(System.in);
			int n = input.nextInt();
		
			if(n==1)
			{
				System.out.print("push ");
				pushlength = input.nextInt();
				for(int i=0;i<pushlength;i++)
					sta.push(input.nextInt());
				System.out.println();
			}
			
			if(n==2)
			{
				System.out.print("pop ");	
				poplength = input.nextInt();
				
				if(poplength>x)
					poplength=x+1;
				
				for(int i=0;i<poplength;i++)
					sta.pop();
				System.out.println();
			}
			
			if(n==3)
			{
				sta.show();
			}
			
			if(n==4)
			{
				System.out.println("Good Bye");
				break;
			}
		}
	}

	
}
