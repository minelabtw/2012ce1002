package ce1002.E3.s101502009;

import java.util.Scanner;

public class E31 {
	public static void main(String[] args){
		stack stack=new stack();
		Scanner input=new Scanner(System.in);
		while(true){
			System.out.println("1: push");
			System.out.println("2: pop");
			System.out.println("3: show");
			System.out.println("4: exit");
			int cin=input.nextInt();
			if(cin==1){
				stack.push();
			}
			
			else if(cin==2){
				stack.pop();				
			}
			else if(cin==3){
				stack.show();
			}
			else if(cin==4){
				System.out.println("Good Bye");
				break;				
			}
			}//end of while
	}//end of main

}
