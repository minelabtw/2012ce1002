package ce1002.E3.s100502203;

import java.util.Scanner;

public class E32 {
	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);
		Queue queue = new Queue();
		// circular queue, use tag to note full or empty, tag = true ->
		// full
		int opt;
		System.out.println("1: push\n" + "2: pop\n" + "3: show\n" + "4: exit");
		opt = input.nextInt();
		while (opt != 4) {
			int number;
			switch (opt) {
			case 1:
				System.out.print("push ");
				number = input.nextInt();
				for (int i = 0; i < number; i++) {
					if (!queue.isFull()) // queue not full
						queue.push(input.nextInt());
					else {
						System.out.println("\nQueue is full!");
						break;
					}
				}
				break;

			case 2:
				System.out.print("pop ");
				number = input.nextInt();
				for (int i = 0; i < number; i++) {
					if (queue.isEmpty()) { // queue empty
						System.out.print("\nQueue is empty!");
						break;
					} else
						System.out.print(queue.pop() + " ");
				}
				System.out.println(" ");
				break;

			case 3:
				queue.show();
				break;
			}
			System.out.println("\n1: push\n" + "2: pop\n" + "3: show\n"
					+ "4: exit");
			opt = input.nextInt();
		}
		System.out.print("Good Bye");
		input.close();
	}

	public static class Queue {
		int queueData[], rear, front, Max_size = 100;
		boolean tag;

		Queue() {
			queueData = new int[Max_size];
			rear = front = Max_size - 1;
			tag = false;
		}

		boolean isFull() {
			if (tag == true)
				return true;
			else
				return false;
		}

		boolean isEmpty() {
			if (rear == front && tag != true)
				return true;
			else
				return false;
		}

		void push(int n) {
			queueData[rear] = n;
			rear = (rear + 1) % Max_size; // circle
			if (rear == front) // when push note "full"
				tag = true;
		}

		int pop() {
			int cur = front;
			front = (front + 1) % Max_size; // circle
			tag = false; // every time pop means not full
			return queueData[cur];
		}

		void show() {
			if (this.isEmpty()) { // queue empty
				System.out.println("Queue is empty!");
				return;
			}
			for (int i = front; i != rear; i = (i + 1) % Max_size)
				System.out.print(queueData[i] + " ");
			System.out.println(" ");
		}
	}
}
