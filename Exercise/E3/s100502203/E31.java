package ce1002.E3.s100502203;

import java.util.Scanner;

public class E31 {	
	

	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);
		Stack stack = new Stack();

		int opt;
		System.out.println("1: push\n" + "2: pop\n" + "3: show\n" + "4: exit");
		opt = input.nextInt();
		while (opt != 4) { // 4 = exit
			int number;
			switch (opt) {
			case 1:
				System.out.print("push ");
				number = input.nextInt();
				for (int i = 0; i < number; i++)
					if (!stack.isFull()) // stack not full
						stack.push(input.nextInt());
					else
						System.out.println("Stack is full!");
				break;

			case 2:
				System.out.print("pop ");
				number = input.nextInt();
				for (int i = 0; i < number; i++)
					if (!stack.isEmpty()) // stack not empty
						System.out.print(stack.pop() + " ");
					else {
						System.out.print("\nStack is empty!");
						break;
					}
				System.out.println(" ");
				break;

			case 3:
				stack.show();
				break;
			}
			System.out.println("\n1: push\n" + "2: pop\n" + "3: show\n"
					+ "4: exit");
			opt = input.nextInt();
		}
		System.out.print("Good Bye");
		input.close();
	}
	
	public static class Stack {
		int stackData[], top, Max_size = 100;

		Stack() {
			stackData = new int[Max_size];
			top = 0;
		}
		
		boolean isFull(){
			if(top == Max_size - 1)
				return true;
			else 
				return false;
		}
		
		boolean isEmpty(){
			if(top == 0)
				return true;
			else
				return false;
		}

		void push(int n) {
			stackData[top++] = n;
		}

		int pop() {
			return stackData[--top];
		}

		void show() {
			if (this.isEmpty()) {
				System.out.println("Stack is empty!");
				return;
			}
			for (int i = 0; i < top; i++)
				System.out.print(stackData[i] + " ");
			System.out.println(" ");
		}
	}
}
