package ce1002.E3.s101502523;
import ce1002.E3.s101502523.stack;
import java.util.*;
public class E31 {
	public static void main(String[] args){
		Scanner input = new Scanner(System.in);
		stack s = new stack();
		while(true){
			int selection, size;
			System.out.println("1: push\n"+"2: pop\n"+"3: show\n"+"4: exit");
			selection = input.nextInt();
			//choose 4, good bye~
			if(selection==4){
				System.out.print("Good Bye");
				break;
			}
			//for case 1 to 3
			switch(selection){
				case 1:
					System.out.print("push ");
					size = input.nextInt();  //determine the size of the input
					s.push(size);
					break;
				case 2:
					System.out.print("pop ");
					size = input.nextInt();  //determine the size of the input
					s.pop(size);
					System.out.println("");	
					break;
				case 3:
					s.show();
					System.out.println("");	
					break;
			}
			System.out.println("");
		}
	}
}