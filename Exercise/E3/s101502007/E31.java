package ce1002.E3.s101502007;
import java.util.Scanner;
import ce1002.E3.s101502007.stack;
public class E31 {
	public static void main(String args[])
	{
		stack answer=new stack();
		while(true)
		{
			System.out.println("1: push");
			System.out.println("2: pop");
			System.out.println("3: show");
			System.out.println("4: exit");
			Scanner input=new Scanner(System.in);
			int n=input.nextInt();
			if (n==1)
			{
				System.out.print("push");
				int a=input.nextInt();
				answer.push(a);
			} //end of 1
			else if (n==2)
			{
				System.out.print("pop");
				int b=input.nextInt();
				answer.pop(b);
			} //end of 2
			else if (n==3)
			{
				answer.show();
			} //end of 3
			else if (n==4)
			{
				System.out.println("Good bye");
				break;
			} //end of 4
		}
	} //end of main
}