package ce1002.E3.s101502525;

public class stack {
    int[] stackData;
    int top;
    stack(){
    	stackData=new int[100];
    	top=0;
    }
    public boolean empty() {
		return top>0?false:true;
	}
	public void push(int n){
		stackData[top++]=n;//input with top+1
	}
	public void pop(){
		--top;
	}
	public void show(){
		for(int i=0;i<top;i++)
			System.out.printf("%d ",stackData[i]);
		System.out.print("\n");
	}
}
