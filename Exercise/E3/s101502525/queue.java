package ce1002.E3.s101502525;

public class queue {
	//initialize
	int[] queueData;
	int front;
	int rear;
    queue(){
    	queueData=new int[100];
    	front=0;
    	rear=0;
    }
	public boolean empty() {
		return front==rear?true:false;
	}
	public void push(int n){
		queueData[rear++]=n;//input with rear+1
		if(rear==99){//move when the rear becomes the max
			for(int i=0;i<front-rear+1;i++)
				queueData[i]=queueData[front];
			rear-=front;
			front=0;
		}
	}
	public void pop(){
		front++;//output with front+1
	}
	public void show(){
		for(int i=front;i<rear;i++)
			System.out.printf("%d ",queueData[i]);
		System.out.print("\n");
	}
}
