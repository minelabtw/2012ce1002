package ce1002.E3.s101502525;
import java.util.Scanner;
import ce1002.E3.s101502525.stack;

public class E31 {
	public static void main(String[] Args){
		//initialize
		Scanner input=new Scanner(System.in);
		stack stackData=new stack();
		boolean exit=false;
		
		while(!exit){
			//output menu
			System.out.println("1: push");
			System.out.println("2: pop");
			System.out.println("3: show");
			System.out.println("4: exit");
			
			//choose case
			switch(input.next()){
			case "1":{//push
				System.out.print("push ");
				int num=input.nextInt();
				for(int i=0;i<num;i++)
					stackData.push(input.nextInt());
				System.out.print("\n");
				break;
			}
			case "2":{//pop
				System.out.print("pop ");
				int num=input.nextInt();
				for(int i=0;i<num;i++)
					if(!stackData.empty()){//only pop when not empty
						System.out.printf("%d ",stackData.stackData[stackData.top-1]);
						stackData.pop();
					}
					else{
						System.out.print("\nStack is empty!");
						break;
					}
				System.out.print("\n\n");
				break;
			}
			case "3":{//show
				if(!stackData.empty())//only show when not empty
					stackData.show();
				else
					System.out.print("Stack is empty!\n");
				System.out.print("\n");
				break;
			}
			default://case 4 or wrong input->exit
				System.out.println("Good Bye");
				exit=true;
			}
		}
	}
}
