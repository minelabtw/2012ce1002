package ce1002.E3.s101502509;

public class Queue 
{
    int[] queueData;
    int queuefront;
    int queuerear;
    
    Queue() {		//constructor
    	queueData = new int[100]; 	//create queue[100]
    	queuefront = 0;				//queuepointer initialize
    	queuerear = 0;
    }
    
    void push(int num) {
    	queueData[queuerear] = num;	//push num into queue
    	queuerear++;
    }
    
    void pop() {
    	System.out.print(queueData[queuefront] + " ");	//pop number out queue
    	for(int j = 0; j < queuerear; j++) {	//move whole queue
    		queueData[j] = queueData[j + 1];
    	}
    	queuerear--;
    }
    
    void show() {
    	for(int i = 0; i < queuerear; i++) {		//print queue numbers
    		System.out.print(queueData[i] + " ");
    	}
    }
}
