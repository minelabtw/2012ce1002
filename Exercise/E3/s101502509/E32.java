package ce1002.E3.s101502509;

import java.util.Scanner;
import ce1002.E3.s101502509.Queue;

public class E32 {
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Queue myqueue = new Queue(); 	//create queue object
		Scanner input = new Scanner(System.in);
		int choice;
		
		do{
			System.out.println("1: push");
			System.out.println("2: pop");
			System.out.println("3: show");
			System.out.println("4: exit");
			choice = input.nextInt();   //input your choice
			
			switch(choice){
				case 1:		//queue push
				{
					int pushsize;
					System.out.print("push ");
					pushsize = input.nextInt();		//get pushsize
					
					for(int i = 0; i < pushsize; i++) {		//input push numbers
						 myqueue.push(input.nextInt());
					}
					break;
				}
				case 2:		//queue pop
				{
					if(myqueue.queuerear == 0) {
						System.out.println("Queue is empty!");
					} else {
						int popsize;
						System.out.print("pop ");
						popsize = input.nextInt();		//get popsize
						
						for(int i = 0; i < popsize; i++) {		//print pop numbers
							if(myqueue.queuerear == 0) {
								System.out.print("\nQueue is empty!");
								break;
							} else {
								myqueue.pop();
							}
						}
						System.out.println();
					}
					break;
				}
				case 3:		//queue show
				{
					if(myqueue.queuerear == 0) {
						System.out.print("Queue is empty!");
					} else {
						myqueue.show();		//print queue numbers
					}
					System.out.println();
					break;
				}
				case 4:		//exit
				{
					System.out.println("Good Bye");
					break;	
				}
			}
			System.out.println();
		}while(choice != 4);
	}
}