package ce1002.E3.s101502509;

public class Stack 
{
    int[] stackData;
    int stacklength;
    
    Stack() {		//constructor
        stackData = new int[100]; 	//create stack[100]
        stacklength = 0;	//stacksize initialize
    }
    
    void push(int num) {
    	stackData[stacklength] = num;	//push num into stack
    	stacklength++;
    }
    
    void pop() {
    	System.out.print(stackData[stacklength-1] + " ");	//pop number out stack
    	stacklength--;
    }
    
    void show() {
    	for(int i = 0; i < stacklength; i++) {		//print stack numbers
    		System.out.print(stackData[i] + " ");
    	}
    }
}