package ce1002.E3.s101502509;

import java.util.Scanner;
import ce1002.E3.s101502509.Stack;

public class E31 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Stack mystack = new Stack();		//create stack object
		Scanner input = new Scanner(System.in);
		int choice;
		
		do{
			System.out.println("1: push");
			System.out.println("2: pop");
			System.out.println("3: show");
			System.out.println("4: exit");
			choice = input.nextInt();   //input your choice
			
			switch(choice){
				case 1:		//stack push
				{
					int pushsize;
					System.out.print("push ");
					pushsize = input.nextInt();		//get pushsize
					
					for(int i = 0; i < pushsize; i++) {		//input push numbers
						mystack.push(input.nextInt());
					}
					break;
				}
				case 2:		//stack pop
				{
					if(mystack.stacklength == 0) {
						System.out.print("Stack is empty!");
					} else {
						int popsize;
						System.out.print("pop ");
						popsize = input.nextInt();		//get popsize
	
						for(int i = 0; i < popsize; i++) {	//print pop numbers
							if(mystack.stacklength <= 0) {
								System.out.print("\nStack is empty!");
								break;
							} else {
								mystack.pop();
							}
						}
					}
					System.out.println();
					break;
				}
				case 3:		//stack show
				{
			    	if(mystack.stacklength == 0) {
						System.out.print("Stack is empty!");
					} else {
						mystack.show();		//print stack numbers
					}
					System.out.println();
					break;
				}
				case 4:		//exit
				{
					System.out.println("Good Bye");
					break;	
				}
			}
			System.out.println();
		}while(choice != 4);
	}
}
