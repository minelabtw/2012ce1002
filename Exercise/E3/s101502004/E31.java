package ce1002.E3.s101502004;
import java.util.Scanner;
import ce1002.E3.s101502004.stack;

public class E31 {
	public static void main(String[] args){
		System.out.println("1: push\n2: pop\n3: show\n4: exit");
		Scanner inp = new Scanner(System.in);
		int choose = inp.nextInt();
		stack stack = new stack();//建立物件
		while(choose!=4){
			if(choose==1){
				System.out.println("push");
				int howmanypush = inp.nextInt();//push多少數字
				stack.push(howmanypush);//傳入class
			}
			if(choose==2){
				System.out.println("pop");
				int howmanypop = inp.nextInt();//pop多少數字
				if(stack.pop(howmanypop)==false)//回傳值若為false
					System.out.println("\nStack is empty!");
			}
			if(choose==3)
				stack.show();//顯示show的結果
			if(choose==4)
				break;
			System.out.println("\n1: push\n2: pop\n3: show\n4: exit");//重複輸出
			choose=inp.nextInt();
		}
		if(choose==4)
			System.out.println("Good Bye");
	}
}
