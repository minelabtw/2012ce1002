package ce1002.E3.s101502524;

import java.util.Scanner;
import ce1002.E3.s101502524.stack;

public class E31
{
	public static void main(String[] args)
	{
		stack stack=new stack();
		boolean exit=false;
		while(!exit)
		{
			Scanner input=new Scanner(System.in);
			
			System.out.println( "1: push" );
			System.out.println( "2: pop" );
			System.out.println( "3: show" );
			System.out.println( "4: exit" );
			
			int choose=input.nextInt();
			
			switch(choose)
			{
				case 1:
					System.out.print("push ");
					int round=input.nextInt();
					stack.push(round);
					System.out.println("");
					break;
					
				case 2:
					System.out.print("pop ");
					int round2 = input.nextInt();
					stack.pop(round2);
					System.out.println("");
					break;
				
				case 3:
					stack.show();
					System.out.println("");
					break;
					
				case 4:
					exit=true;
					break;
			}
		}
		System.out.println("Good Bye");
	}
}
