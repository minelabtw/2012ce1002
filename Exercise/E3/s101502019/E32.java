package ce1002.E3.s101502019;
import java.util.*;
import ce1002.E3.s101502019.queue;
public class E32 {
	public static void main(String[] args){
		Scanner input = new Scanner(System.in);
		int choice;
		queue x = new queue();
		while (true){
			boolean c = true;
			System.out.println("1: push\n2: pop\n3: show\n4: exit");
			choice = input.nextInt();
			switch(choice)
			{
				case 1:
					x.push();
					break;
				case 2:
					x.pop();
					break;
				case 3:
					x.show();
					break;
				case 4:
					System.out.println("Good Bye");
					c = false;
					break;
			}
			if(c == true)
				continue;
			else
				break;
		}
	}
}
