package ce1002.E3.s101502516;
import java.util.Scanner;
public class E32 {
	public static void  main(String[] args){
		queue  queue1 = new queue(); // 引用queue
		while( true )
		{
			System.out.println( "1: push" ); // 輸出資訊
			System.out.println( "2: pop" );
			System.out.println( "3: show" );
			System.out.println( "4: exit" );
			
			int a,b,c;
			Scanner input = new Scanner(System.in);
			a = input.nextInt();
			
			if ( a == 1 ) // push
			{
				System.out.print( "push " );
				b = input.nextInt();
				for ( int x = 0; x < b; x++ )
				{
					c = input.nextInt();
					queue1.push( c );
				}
			}
			else if ( a == 2 ) // pop
			{
				System.out.print( "pop " );
				b = input.nextInt();
				queue1.pop( b );
			}
			else if ( a == 3 ) // show
			{
				queue1.show();
			}
			else if ( a == 4 ) // exit
			{
				System.out.println( "Good Bye" );
				break;
			}
		}
	}
}
