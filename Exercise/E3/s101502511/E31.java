package ce1002.E3.s101502511;

import java.util.Scanner;

public class E31 {
	public static void main(String[] args) {

		int input;
		stack stack_ = new stack();

		do {
			Scanner scanner = new Scanner(System.in);//new scanner

			System.out.println("1: push");
			System.out.println("2: pop");
			System.out.println("3: show");
			System.out.println("4: exit");

			input = scanner.nextInt();

			switch (input) {

				case 1:
					System.out.print("push ");
					int size;
					size = scanner.nextInt();//輸入push的數量
					
					stack_.push(size);//帶進去stack.push執行
					break;
				
				case 2:
			    	System.out.print("pop ");
			    	int pop;
					pop = scanner.nextInt();//輸入要pop的數量
					
					stack_.pop(pop);//帶進去stack_.pop執行
					break;
					
				case 3:
					stack_.show();//直接show出stack_.show()內容(show當前的陣列)
					
					break;
				
				case 4://結束
					System.out.println("Good Bye");
					System.exit(0);
					
					break;

			}

		} while (true);
	}
}
