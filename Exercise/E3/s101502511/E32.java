package ce1002.E3.s101502511;

import java.util.Scanner;

public class E32 {

	public static void main(String[] args) {
		int input;
		queue queue_ = new queue();

		do {
			Scanner scanner = new Scanner(System.in);//new scanner

			System.out.println("1: push");
			System.out.println("2: pop");
			System.out.println("3: show");
			System.out.println("4: exit");

			input = scanner.nextInt();

			switch (input) {

				case 1:
					System.out.print("push ");
					int size;
					size = scanner.nextInt();//輸入push的數量
					
					queue_.push(size);//帶進去queue.push,秀出結果
					break;
				
				case 2:
			    	System.out.print("pop ");
			    	int pop;
					pop = scanner.nextInt();//輸入pop的數量
					
					queue_.pop(pop);//帶進去,並秀出pop出的數
					break;
					
				case 3:
					queue_.show();//show出當前陣列
					
					break;
				
				case 4://結束
					System.out.println("Good Bye");
					System.exit(0);
					
					break;

			}

		} while (true);
	}

}


