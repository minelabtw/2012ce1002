package ce1002.E3.s101502527;
import ce1002.E3.s101502527.stack;
public class E31 {
	public static void main(String[] args){
		java.util.Scanner scan = new java.util.Scanner(System.in);
		stack sta = new stack();
		while(true){
			System.out.println("1: push");
			System.out.println("2: pop");
			System.out.println("3: show");
			System.out.println("4: exit");
			int n=scan.nextInt();
			boolean exit=false;
			switch (n){//call function
			case 1:
				System.out.print("push ");
				int pn=scan.nextInt();
				sta.push(pn);
				break;
			case 2:
				System.out.print("pop ");
				int popn=scan.nextInt();
				sta.pop(popn);
				break;
			case 3:
				sta.show();
				break;
			case 4:
				exit= true;
				break;
			
			}
			if(exit){//in exit==true break;
				System.out.print("Good Bye");
				break;
			}
			
		}
	}

}
