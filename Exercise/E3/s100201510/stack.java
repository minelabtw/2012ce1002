package ce1002.E3.s100201510;

import java.util.Scanner;

public class stack
{
    private int[] stackData;
	// length is the number of elements in stack.
	// it will be 0 while stack is empty.
	private int length;
	// number is the number want to push or pop.
	//private int number;
	private Scanner input;
	 
	stack()
    {
        //something initialize
		stackData = new int[100];
		for(int i = 0 ; i < 100 ; i++)
			stackData[i] = 0 ;
		
		length = 0;
		
		input = new Scanner(System.in);
    }
	public void push(int n)
    {
        for(int i = length ; i < (length + n) ; i++){
        	stackData[i] = input.nextInt();
        }
        System.out.print("\n");
        length += n;
    }
    public void pop(int n)
    {
		if(length - n <= 0){
			length = 0 ;
			System.out.print("Stack is empty!\n");
			for(int i = length ; i < length - n ; i--){
				stackData[i] = 0;
			}
		}
		else{
			length -= n;
			for(int i = length ; i < length - n ; i--){
				stackData[i] = 0;
			}
			for(int i = length ; i < length - n ; i--){
				System.out.printf("%d " , stackData[i]);
			}
		}
		System.out.print("\n\n");
    }
    public void show()
    {
    	if(length == 0)
        	System.out.print("Stack is empty!\n");
        for(int i = 0 ; i < length ; i++)
        	System.out.printf("%d " , stackData[i]);
        
        System.out.print("\n\n");
    }
}

