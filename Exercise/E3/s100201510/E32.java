package ce1002.E3.s100201510;

import java.util.Scanner;
import ce1002.E3.s100201510.queue;

public class E32 {
	
	private static Scanner input;

	public static void main(String[] args) {
		
		int option = 0;
		int swich = 0;
		queue myqueue = new queue(); //  is a stack object.
		input = new Scanner(System.in);
		while(true){
			//print selections.
			System.out.print("1: push\n2: pop\n3: show\n4: exit\n");
			option = input.nextInt();
			//input.close();
			if(option > 4 || option <1){ // check wrong option.
				System.out.print("Illigle option.");
				System.exit(1);
			}
			
			if(option == 1){//push
				System.out.print("push ");
				swich = input.nextInt();
				myqueue.push(swich);
			}
			else if(option == 2){//pop
				System.out.print("pop ");
				swich = input.nextInt();
				myqueue.pop(swich);
			}
			else if(option == 3){//show
				myqueue.show();
			}
			else if(option == 4){//exit
				System.out.print("Good Bye");
				System.exit(1);
			}
		}
	}

}
