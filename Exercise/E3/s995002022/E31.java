package ce1002.E3.s995002022;

import java.util.Scanner;
import ce1002.E3.s995002022.stack;

public class E31 {
static stack s = new stack();



	public static void main(String[] args) 
	{
		
		boolean exit = false;
		while( !exit )
		{	
			Scanner scanner = new Scanner( System.in );
		
			System.out.println( "" );
			System.out.println( "1: push" );
			System.out.println( "2: pop" );
			System.out.println( "3: show" );
			System.out.println( "4: exit" );
			
			int choise = scanner.nextInt();
			
			switch( choise )
			{
				//push case
				case 1:
					System.out.print( "push " );
					int round = scanner.nextInt();	//how many value have to push
					
					for(int i = 0; i < round; i++)
						s.push( scanner.nextInt() );
					
					break;
					
				//pop case
				case 2:
					System.out.print( "pop " );
					int round2 = scanner.nextInt();	// how many value have to pop
					
					
					for(int i = 0; i < round2; i++)
						if(s.pop()==false)
							break;
					
					System.out.println( "" );
					break;					
					
				//show all the elements in the stack
				case 3:
					s.show();
					
					System.out.println( "" );
					break;
				
				//exit
				case 4:
					exit = true; 
					break;
			}
		}
		System.out.println("Good Bye");
	}
	
	
	
	
	
}
