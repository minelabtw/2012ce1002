package ce1002.E3.s995002022;

import java.util.Scanner;
import ce1002.E3.s995002022.queue;

public class E32 {
static queue q = new queue();
	
public static void main(String[] args) 
{
	boolean exit = false;
	while( !exit )
	{	
		Scanner scanner = new Scanner( System.in );
	
		System.out.println( "" );
		System.out.println( "1: push" );
		System.out.println( "2: pop" );
		System.out.println( "3: show" );
		System.out.println( "4: exit" );
		
		int choise = scanner.nextInt();
		
		switch( choise )
		{
			//push case
			case 1:
				System.out.print( "push " );
				int round = scanner.nextInt();	//how many value have to push
				
				for(int i = 0; i < round; i++)
					q.push( scanner.nextInt() );
				
				break;
				
			//pop case
			case 2:
				System.out.print( "pop " );
				int round2 = scanner.nextInt();	// how many value have to pop
				
				int size = q.size;
				
				if(round2 > size)	//檢查pop次數是否超過佇列裡的資料量
					round2 = size + 1;	//若超過, pop所有資料, 並僅提示1次堆疊已空
				
				for(int i = 0; i < round2; i++)
					q.pop();
				
				System.out.println( "" );
				break;					
				
			//show all the elements in the queue
			case 3:
				q.show();
				
				System.out.println( "" );
				break;
			
			//exit
			case 4:
				exit = true; 
				break;
		}
	}
	System.out.println("Good Bye");
}


	
	
	
	
	
	
	
	
	
	
	
}
