package ce1002.E3.s995002022;

public class queue {

	 int[] queue = new int[100];	//my queue
	 public int size = 0;	//queue's current size
	    
	    void push(int num)
	    {
	    	if( size < 100 )
			{
				size++;
				queue[size-1] = num;
			}
			
			else	//queue is full
				System.out.println( "queue is full!" );
	    }
	    
	    
	    void pop()
	    {
	    	if( size > 0 )
			{
				System.out.print( queue[0] + " ");
				
				for(int i = 1; i < size; i++)
					queue[i-1] = queue[i];
				
				queue[size-1] = 0;	//reset
				
				size--;
			}
			
			else
			{
				System.out.println("");
				System.out.print( "queue is empty!" );
			}
	    }
	    
	    void show()
	    {
	    	if( size <= 0 )
				System.out.print( "queue is empty!" );
			
			else
				for(int i = 0; i < size; i++)
					System.out.print( queue[i] + " " );
	    }
	
	
	
}
