package ce1002.E3.s995002022;

public class stack {

	 int[] stack = new int[100];	//my stack
	 int size = 0;	//stack's current size
	
	void push( int num )	//push value to stack
	{
		if( size < 100 )
		{
			size++;
			stack[size-1] = num;
		}
		
		else	//stack is full
			System.out.println( "Stack is full!" );
	}
	
	boolean pop()	//pop value from stack
	{
		if( size > 0 )
		{
			System.out.print( stack[size-1] + " ");
			stack[size-1] = 0;	//reset
			size--;
			return true;
		}
		
		else
		{
			System.out.println("");
			System.out.print( "Stack is empty!" );
			return false;
		}
	}
	
	void show()
	{
		if( size <= 0 )
			System.out.print( "Stack is empty!" );
		
		else
			for(int i = 0; i < size; i++)
				System.out.print( stack[i] + " " );
	}
	//--------------------------
	
	
}
