package ce1002.E3.s100502006;

import java.util.Scanner;

public class E32 {
	static Scanner input=new Scanner(System.in);
	public static void main(String[] args){
		int choice=0;//store choice
		queue q= new queue();//declare a queue
		do{
			System.out.println("1: push\n"+"2: pop\n"+"3: show\n"+"4: exit");
			choice=input.nextInt();//make a choice
			switch(choice){
			case 1://push
				System.out.print("push ");
				q.push(input.nextInt());
				break;
			case 2://pop(FIFO)
				System.out.print("pop ");
				int numberToPop=input.nextInt();
				for(int i=0;i<numberToPop;i++){
					if(!q.pop()){//if the queue is empty
						break;
					}
				}
				System.out.println("\n");
				break;
			case 3://show
				q.show();
				break;
			case 4:
				System.out.println("Good Bye");
				break;
			}
		}while(choice!=4);
	}
}
