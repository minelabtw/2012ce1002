package ce1002.E3.s101502014;
import java.util.Scanner;
public class E32 {
	public static void main(String[] args) {
		Scanner cin = new Scanner(System.in); //cin為輸入的動作
		queue Queue = new queue(); //以queue宣告一個名為Queue的物件
		int x , num; //x為使用者輸入的功能代碼 , num為輸入或輸出的個數
		while(true)
		{
			System.out.println("1: push");
			System.out.println("2: pop");
			System.out.println("3: show");
			System.out.println("4: exit");
			x = cin.nextInt();
			if(x == 1)
			{
				System.out.print("push ");
				num = cin.nextInt();
				Queue.push(num);
			}
			else if(x == 2)
			{
				System.out.print("pop ");
				num = cin.nextInt();
				if(Queue.pop(num) == false)
					System.out.println("\nQueue is empty!\n");
			}
			else if(x == 3)
			{
				Queue.show();
			}
			else if(x == 4) //若x為4 , 則輸出離開訊息 , 並跳出迴圈
			{
				System.out.println("Good Bye");
				break;
			}
		}
	}
}
