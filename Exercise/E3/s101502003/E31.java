package ce1002.E3.s101502003;
import java.util.*;

public class E31 {
	public static void main(String[] args)
	{
		stack t=new stack(); // 把stack那個class引進來
		Scanner input = new Scanner(System.in);
		int a=1; // 方便讓迴圈進行所設的數
		while(a==1)
		{
			System.out.println("1: push");
			System.out.println("2: pop");
			System.out.println("3: show");
			System.out.println("4: exit");
			int x = input.nextInt();
			if(x==1)
				t.push(); // stack的push function
			else if (x==2)
				t.pop();// stack的pop function
			else if (x==3)
				t.show();// stack的show function
			else // x=4時結束,離開迴圈
			{
				System.out.println("Good Bye");
				break;
			}		
		}
	}
}
