package ce1002.E3.s101502501;

import java.util.Scanner;

public class stack {
	int[] cinArray;
	int numberOfArray;

	Scanner cin = new Scanner (System.in);
	stack(){
		cinArray = new int[100]; //定一個陣列用來存堆疊 (Stack)
		numberOfArray = 0; //用來計算陣列中有幾項
	}
	void push(){
		
		int pushNum = cin.nextInt();
		for(int i=numberOfArray; i<numberOfArray+pushNum; i++){
			cinArray[i] = cin.nextInt(); //把輸入存進陣列中
		}
		for(int i=0; i<pushNum; i++){
			numberOfArray++; //計算有幾項
		}
		System.out.print("\n"); //空行
	}
	
	void pop(){
		int popNum = cin.nextInt();
		if(numberOfArray-popNum>=0){//陣列裡還沒全pop掉時
			for(int i=numberOfArray-1; i>numberOfArray-1-popNum; i--){ //從最後一項開始輸出
				System.out.print(cinArray[i]+" ");
			}
			for(int i=0; i<popNum;i++){
				numberOfArray--; //項數要減掉
			}					
		}
		else{//陣列裡全pop掉時
			for(int i=numberOfArray-1; i>=0; i--){
				System.out.print(cinArray[i]+" ");
			}
			for(int i=0; i<popNum;i++){
				numberOfArray--;
			}
			System.out.print("\n");
			System.out.print("Stack is empty!");
			
		}
		System.out.println("\n");
		
	}
	void show(){
		if(numberOfArray>0){
			for(int i=0; i<numberOfArray; i++){
				System.out.print(cinArray[i]+" ");
			}
		}
		else{//陣列裡全空
			System.out.print("Stack is empty!");
		}
		System.out.println("\n");
	}
	
		
	

}
