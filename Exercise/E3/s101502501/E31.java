package ce1002.E3.s101502501;
import java.util.Scanner;
import ce1002.E3.s101502501.stack;
public class E31 {
	public static void main (String[] args){
		
		stack stack = new stack();

		Scanner cin = new Scanner (System.in);
		int chooseNum=0;
				
		while (chooseNum!=4){ //輸入不為4時進入迴圈
			
			System.out.println("1: push");
			System.out.println("2: pop");
			System.out.println("3: show");
			System.out.println("4: exit");
			chooseNum = cin.nextInt();
			
			if(chooseNum == 1){ //輸入為1
				System.out.print("push ");
				stack.push();
			}
			if(chooseNum == 2){
				System.out.print("pop ");
				stack.pop();

			}
			if(chooseNum == 3){
				stack.show();
			
			}
			if(chooseNum == 4){
				System.out.println("Good Bye");					
			}
		}
	}

}
