package ce1002.E3.s101502002;
import java.util.Scanner;
public class E31 {
	public static void main(String[]args){
		Scanner input=new Scanner(System.in);
		stack stack=new stack();
		while(true){ //重複輸入
			System.out.println("\n1: push\n2: pop\n3: show\n4: exit");
			int num=input.nextInt(); //輸入要執行哪一個選項
			if(num==1){
				System.out.print("push ");
				int a=input.nextInt();
				stack.push(a);
			}
			else if(num==2){
				System.out.print("pop ");
				int b=input.nextInt();
				stack.pop(b);
			}
			else if(num==3){
				stack.show();
			}
			else { //離開
				System.out.println("Good bye");
				break;
			}	
		}
	}
}
