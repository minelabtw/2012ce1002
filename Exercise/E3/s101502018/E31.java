package ce1002.E3.s101502018;
import ce1002.E3.s101502018.stack;
import java.util.*;
public class E31 {
	public static void main(String[] args) 
	{
		stack stack=new stack();
		boolean exit = false;
		while( !exit )
		{	
			Scanner scanner = new Scanner( System.in );
		
			System.out.println( "" );
			System.out.println( "1: push" );
			System.out.println( "2: pop" );
			System.out.println( "3: show" );
			System.out.println( "4: exit" );
				
			int choise = scanner.nextInt();
			switch( choise )
			{
				//push case
				case 1:
					System.out.print( "push " );
					int round = scanner.nextInt();	//how many value have to push
						
					for(int i = 0; i < round; i++)
						stack.push( scanner.nextInt() );
						
					break;
						
				//pop case
				case 2:
					System.out.print( "pop " );
					int round2 = scanner.nextInt();	// how many value have to pop
					
					if(round2 > stack.size)	//檢查pop次數是否超過堆疊裡的資料量
						round2 = stack.size + 1;	//若超過, pop所有資料, 並僅提示1次堆疊已空
						
					for(int i = 0; i < round2; i++)
						stack.pop();
						
					System.out.println( "" );
					break;					
						
				//show all the elements in the stack
				case 3:
					stack.show();
						
					System.out.println( "" );
					break;
					
				//exit
				case 4:
					exit = true; 
					break;
			}
		}
		System.out.println("Good Bye");
	}
}

