package ce1002.E3.s101502521;

import java.util.Scanner;
import ce1002.E3.s101502521.queue; // import my class

public class E32 {
	
	public static void main(String[] args)
	{
		queue myqueue = new queue();
		Scanner input = new Scanner(System.in);
		while(true)
		{
			System.out.println("1: push");
			System.out.println("2: pop");
			System.out.println("3: show");
			System.out.println("4: exit");
			int choose = input.nextInt();
			if(choose==4)
			{
				System.out.println("Good Bye");
				break;
			}
			switch(choose) // show message
			{
			case 1:
				System.out.print("push ");
				break;
			case 2:
				System.out.print("pop ");
				break;
			}
			if(choose==3) // show stack
			{
				myqueue.show();
				
				continue;
			}
			if(choose==1)
			{
				int arg = input.nextInt(); // the number of input
				for(int i=0;i<arg;i++)
					myqueue.push(input.nextInt());
			}
			if(choose==2)
			{
				int arg=input.nextInt(); // pop stack
				int i;
				for(i=1;i<=arg;i++)
					if(!myqueue.pop())
					{
						System.out.println("");
						System.out.print("Queue is empty!");
						break;
					}
				System.out.println("");
			}
		}
	}
}
