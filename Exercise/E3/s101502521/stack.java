package ce1002.E3.s101502521;

public class stack
{
    int[] stackData;
    int idx; // index of array
    stack()
    {
        stackData = new int[100]; // init the array
        idx=-1; // first index to -1 means no element in stack
    }
    void push(int n)
    {
    	idx++;
        stackData[idx] = n;
    }
    boolean pop()
    {
    	if(idx==-1)
    		return false;
        System.out.print(stackData[idx]+" ");
        idx--;
        return true;
    }
    void show()
    {
    	if(idx==-1) // if no element in stack
    		System.out.print("Stack is empty!");
        for(int i=0;i<=idx;i++)
        	System.out.print(stackData[i]+" ");
        System.out.println("");
    }
 
}