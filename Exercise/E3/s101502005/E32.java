package ce1002.E3.s101502005;

import java.util.Scanner;

public class E32 {
	public static void main(String[] args){
		Scanner input = new Scanner(System.in);
		queue i = new queue();
		boolean over = true;
		while (over==true) {
			System.out.println("1: push");
			System.out.println("2: pop");
			System.out.println("3: show");
			System.out.println("4: exit");
			
			int count = input.nextInt();
			if (count == 1)
				i.push();
			if(count == 2)
				i.pop();
			if(count == 3)
				i.show();
			if(count == 4)
			{
				System.out.print("Good Bye");
				over = false;
			}
		}
			
	}
}
