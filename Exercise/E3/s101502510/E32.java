package ce1002.E3.s101502510;

import ce1002.E3.s101502510.queue;
import java.util.Scanner;

public class E32 {

	public static void main(String args[]) {

		Scanner input = new Scanner(System.in);
		queue q = new queue();

		boolean exit = false;
		while (exit == false) {
			System.out.println("\n1: push");
			System.out.println("2: pop");
			System.out.println("3: show");
			System.out.println("4: exit");
			switch (input.nextInt()) {
			case 1: {// 輸入1
				System.out.print("push ");
				q.push(input.nextInt());//引用queue class內的 push function
				break;
			}
			case 2: {// 輸入2
				System.out.print("pop ");
				q.pop(input.nextInt());//引用queue class內的 pop function
				break;
			}
			case 3: {// 輸入3
				q.show();//引用queue class內的 show function
				break;
			}
			case 4: {// 輸入4
				System.out.println("Good Bye");
				exit = true;
				break;
			}
			}
		}
	}
}