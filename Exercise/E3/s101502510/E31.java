package ce1002.E3.s101502510;

import ce1002.E3.s101502510.stack;
import java.util.Scanner;

public class E31 {
	public static void main(String args[]) {

		Scanner input = new Scanner(System.in);
		stack s = new stack();

		boolean exit = false;
		while (exit == false) {
			System.out.println("\n1: push");
			System.out.println("2: pop");
			System.out.println("3: show");
			System.out.println("4: exit");
			switch (input.nextInt()) {
			case 1: {// 輸入1
				System.out.print("push ");
				s.push(input.nextInt());//引用stack class內的 push function
				break;
			}
			case 2: {// 輸入2
				System.out.print("pop ");
				s.pop(input.nextInt());//引用stack class內的 pop function
				break;
			}
			case 3: {// 輸入3
				s.show();//引用stack class內的 show function
				break;
			}
			case 4: {// 輸入4
				System.out.println("Good Bye");
				exit = true;
				break;
			}
			}
		}
	}
}
