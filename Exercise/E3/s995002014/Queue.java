package ce1002.E32.s995002014;

public class Queue {
	
    private int[] queueData;
	public int size;
	
    Queue() { //constructor 
		queueData= new int[100]; //set 100
		size=0; //ini size 0
    }
    void push(int n) {
		if(size<100) {
			size++;
			queueData[size-1]=n;
		}
		else
			System.out.println("queue is full");
    }
    void pop(){
		if(size>0){
			System.out.print(queueData[0]);
			for(int i=1;i<size;i++)
				queueData[i-1]=queueData[i];
			queueData[size-1]=0; //reset 
			size--;
		}
		else{
			System.out.println(" ");
			System.out.print( "queue is empty!" );
		}
    }
    void show(){
		if(size<=0)
			System.out.print("queue is empty!");
		else
			for(int i=0;i<size;i++)
				System.out.print(queueData[i]+" ");
    }
}

