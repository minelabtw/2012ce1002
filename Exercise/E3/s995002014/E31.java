package ce1002.E3.s995002014;

import ce1002.E3.s995002014.Stack;
import java.util.Scanner;

public class E31 {
	public static void main(String[] args) {
		Stack stack=new Stack(); // create a stack
		boolean exit = false;
		while(!exit){
			
			Scanner scanner = new Scanner( System.in );
		
			System.out.println( "" );
			System.out.println( "1: push" );
			System.out.println( "2: pop" );
			System.out.println( "3: show" );
			System.out.println( "4: exit" );
			
			int choise = scanner.nextInt();
			
			switch( choise )
			{
				case 1: //psuh
					System.out.print( "push " );
					int round = scanner.nextInt(); //push how many number	
					for(int i = 0; i < round; i++)//push process
						stack.push( scanner.nextInt() );
					break;
					
				case 2://pop
					System.out.print( "pop " );
					int round2 = scanner.nextInt();	//pop how many number
					if(round2 > stack.size)	
						round2 = stack.size + 1;	
					for(int i = 0; i < round2; i++)// pop process
						stack.pop();
					System.out.println( "" );
					break;
					
				case 3://show
					stack.show();
					System.out.println( "" );
					break;
					
				case 4://exit
					exit = true; 
					break;
			}
		}
		System.out.println("Good Bye");
	}
}
