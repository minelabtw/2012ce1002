package ce1002.E3.s995002014;

public class Stack {
	
    private int[] stackdata;
	public int size;
	
    Stack() { //constructor 
		stackdata= new int[100]; //set 100
		size=0; //ini size 0
    }
    void push(int n) {
		if(size<100) {
			size++;
			stackdata[size-1]=n;
		}
		else
			System.out.println("stack is full");
    }
    void pop(){
		if(size>0){
			System.out.print(stackdata[size-1]);
			stackdata[size-1]=0;
			size--;
		}
    }
    void show(){
		if(size<=0)
			System.out.print("stack is empty!");
		else
			for(int i=0;i<size;i++)
				System.out.print(stackdata[i]+" ");
    }
}
