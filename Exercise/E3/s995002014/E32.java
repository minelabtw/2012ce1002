package ce1002.E32.s995002014;

import java.util.Scanner;

public class E32 {
	public static void main(String[] args){
		Queue queue=new Queue(); //create a queue
		boolean exit = false;
		while( !exit )
		{	
			Scanner scanner = new Scanner( System.in );
		
			System.out.println( "" );
			System.out.println( "1: push" );
			System.out.println( "2: pop" );
			System.out.println( "3: show" );
			System.out.println( "4: exit" );
			
			int choise = scanner.nextInt();
			
			switch( choise )
			{
				case 1: //push
					System.out.print( "push " );
					int round = scanner.nextInt();	//push how many number
					for(int i = 0; i < round; i++)
						queue.push( scanner.nextInt() );		
					break;
				case 2: //pop
					System.out.print( "pop " );
					int round2 = scanner.nextInt();	//pop how many number
					if(round2 > queue.size)	// check if it's more than size of queue
						round2 = queue.size + 1;	//if so pop all and tell user
					for(int i = 0; i < round2; i++)
						queue.pop();
					System.out.println( "" );
					break;					
				case 3: //show
					queue.show();
					System.out.println( "" );
					break;
				case 4: //exit
					exit = true; 
					break;
			}
		}
		System.out.println("Good Bye");
	}
}
