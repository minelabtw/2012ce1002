package ce1002.E3.s101502520;

import java.util.Scanner;
import ce1002.E3.s101502520.queue;

public class E32 {

	public static void main(String[] args) {
		int input;
		queue dataStack = new queue(100);
		Scanner inpScanner = new Scanner(System.in);
		
		while(true){
			System.out.println("1: push");
			System.out.println("2: pop");
			System.out.println("3: show");
			System.out.println("4: exit");
			input = inpScanner.nextInt();
			
			switch (input) {
			case 1:
				System.out.print("push ");
				input = inpScanner.nextInt();
				for(int i = 0; i < input; i++)
				{
					dataStack.push(inpScanner.nextInt());
				}
				break;
			case 2:
				System.out.print("pop ");
				input = inpScanner.nextInt();
				for(int i = 0; i < input; i++)
				{
					if(!dataStack.pop())
						break;
				}
				System.out.println();
				break;
			case 3:
				dataStack.show();
				System.out.println();
				break;
			case 4:
				System.out.print("Good Bye");
				inpScanner.close();
				return;

			default:
				break;
			}
			System.out.println();
		}

	}

}
