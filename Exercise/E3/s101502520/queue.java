package ce1002.E3.s101502520;

public class queue 
{
    public int[] queueData;
    private int nextout, nextin, total;
    boolean over;//using this bool to decide which one is bigger(nextin or nextout)
    queue(int n)
    {
        total = n;
        nextout = 0;
        nextin = 0;
        over = false;
        queueData = new int[total];
    }
    void push(int n)
    {
        queueData[nextin]= n;
        nextin++;
        if(nextin == total)
        {
        	nextin = 0;
        	over = true;
        }
    }
    boolean pop()
    {
    	if((nextout < nextin && over == false) || (nextout > nextin && over == true))
    	{
    		System.out.print(queueData[nextout] + " ");
    		nextout++;
    		if(nextout == total)
    		{
    			nextout = 0;
    			over = false;
    		}
    		return true;
    	}
    	else
    	{
    		nextout = nextin;
    		System.out.println();
			System.out.print("Stack is empty!");
			return false;
    	}
    	
    }
    void show()
    {
    	if(nextout == nextin){
    		System.out.print("Stack is empty!");
    		return;
    	}
        if(over)
        {
    	    for(int i = nextout; i < total; i++)
    		    System.out.print(queueData[i] + " ");
    	    for(int i = 0; i < nextin; i++)
    		    System.out.print(queueData[i] + " ");
        }
        else
        {
    	    for(int i = nextout; i < nextin; i++)
    		    System.out.print(queueData[i] + " ");
        }
    }
}
