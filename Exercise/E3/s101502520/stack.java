package ce1002.E3.s101502520;

public class stack 
{
    public int[] stackData;
    private int pointer;
    stack(int n)
    {
        stackData = new int[n];
        pointer = 0;
    }
    void push(int n)
    {
        stackData[pointer] = n;
        pointer++;
    }
    boolean pop()
    {
        pointer--;
        if(pointer >= 0){
        	System.out.print(stackData[pointer] + " ");
        	return true;
        }
        else {
			pointer = 0;
			System.out.println();
			System.out.print("Stack is empty!");
			return false;
		}
    }
    void show()
    {
    	if(pointer == 0)
    	{
    		System.out.print("Stack is empty!");
    		return;
    	}
        for(int i = 0; i < pointer; i++)
        	System.out.print(stackData[i] + " ");
    }
 
}