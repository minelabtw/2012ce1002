package ce1002.E3.s101502502;

import ce1002.E3.s101502502.queue;
import java.util.Scanner;

public class E32 
{
	public static void main(String[] args)
	{
		Scanner cin = new Scanner(System.in);
		int quit = 0;
		queue QQ = new queue();//給定物件讓我們可以呼叫stack裡面的class
		

		while (quit == 0) {
			System.out.println("1: push");
			System.out.println("2: pop");
			System.out.println("3: show");
			System.out.println("4: exit");

			int operate = cin.nextInt();// 操作

			if (operate == 1)// push
			{
				QQ.push();
			}

			if (operate == 2)// pop
			{
				QQ.pop();
			}

			if (operate == 3)// show
			{
				QQ.show();
			}

			if (operate == 4)// exit
			{
				System.out.print("Good Bye");
				break;
			}

		}
	}
}