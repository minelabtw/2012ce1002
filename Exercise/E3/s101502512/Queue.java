package ce1002.E3.s101502512;

public class Queue {

	int size = 0;
	int[] queue = new int[100];

	Queue() {
		size = 0;
	}

	void push(int n) {
		if (size < 100) {
			size++;
			queue[size - 1] = n;
		}

		else
			System.out.println("queue is full!");
	}

	public void pop() {
		if (size > 0) {
			System.out.print(queue[0] + " ");

			for (int i = 1; i < size; i++)
				queue[i - 1] = queue[i];

			queue[size - 1] = 0; // reset

			size--;
		}

		else {
			System.out.println("");// 數字被提完了
			System.out.print("queue is empty!");
		}
	}

	public void show() {
		if (size <= 0)
			System.out.print("queue is empty!");

		else
			for (int i = 0; i < size; i++)
				System.out.print(queue[i] + " ");
	}

}
