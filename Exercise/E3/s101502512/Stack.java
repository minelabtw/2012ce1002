package ce1002.E3.s101502512;

public class Stack {

	int size = 0;
	int[] stack = new int[100];

	Stack() {
		size = 0;
	}

	void push(int n) {
		if (size < 100) {
			size++;
			stack[size - 1] = n;
		}

		else
			System.out.println("Stack is full!");
	}

	void pop() {
		if (size > 0) {
			System.out.print(stack[size - 1] + " ");
			stack[size - 1] = 0; // reset
			size--;

		}

		else {
			System.out.println("");// 數字被提完了
			System.out.print("Stack is empty!");

		}

	}

	void show() {
		if (size <= 0)
			System.out.print("Stack is empty!");

		else
			for (int i = 0; i < size; i++)
				System.out.print(stack[i] + " ");
	}

}
