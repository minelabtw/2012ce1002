package ce1002.E3.s101502512;

import java.util.Scanner;

public class E32 {
	public static void main(String[] args) {
		int size = 0;// 已push的長度
		Queue q = new Queue();
		boolean exit = false;
		while (!exit) {
			Scanner input = new Scanner(System.in);

			System.out.println("");
			System.out.println("1: push");
			System.out.println("2: pop");
			System.out.println("3: show");
			System.out.println("4: exit");

			int choise = input.nextInt();

			switch (choise) {

			case 1:// push
				System.out.print("push ");
				int round = input.nextInt(); // how many value have to push

				for (int i = 0; i < round; i++) {
					int a = input.nextInt();
					q.push(a);
					size++;
				}
				break;

			case 2:// pop
				System.out.print("pop ");
				int round2 = input.nextInt();

				if (round2 > size) // 是否超過資料量
					round2 = size + 1; // 若超過, pop所有資料, 並僅提示1次堆疊已空

				for (int i = 0; i < round2; i++)
					q.pop();

				System.out.println("");
				break;

			case 3:// show
				q.show();

				System.out.println("");
				break;

			case 4:// exit
				exit = true;
				break;
			}
		}
		System.out.println("Good Bye");
	}
}
