package ce1002.E3.s101502021;
import java.util.Scanner;
import ce1002.E3.s101502021.queue;

public class E32 {
	public static void main(String[] args){
		Scanner input = new Scanner(System.in);
		int Howmany,choose;
		//將queue class宣告為queue變成物件拿來使用
		queue queue = new queue();
		
		while(true){
			System.out.println("1: push");
			System.out.println("2: pop");
			System.out.println("3: show");
			System.out.println("4: exit");
			
			choose=input.nextInt();
			
			//堆疊
			if(choose==1){
				System.out.print("push ");
				
				//輸入要疊幾個數字
				Howmany=input.nextInt();
				
				queue.push(Howmany);
			}
			//取走(從頭取)
			else if(choose==2){
				System.out.print("pop ");
				
				//輸入要取走幾個數字
				Howmany=input.nextInt();
				
				queue.pop(Howmany);
			}
			//秀出剩下的數字
			else if(choose==3){
				
				queue.show();
			}
			//結束程式
			else if(choose==4){
				System.out.print("Good Bye");
				return;
			}
		}
	
	}

}
