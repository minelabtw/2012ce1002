package ce1002.E3.s101502021;
import java.util.Scanner;
import ce1002.E3.s101502021.stack;

public class E31 {
	public static void main(String[] args){
		Scanner input = new Scanner(System.in);
		int Howmany,choose;
		//將stack class宣告為stack變成物件拿來使用
		stack stack = new stack();
		
		while(true){
			System.out.println("1: push");
			System.out.println("2: pop");
			System.out.println("3: show");
			System.out.println("4: exit");
			
			choose=input.nextInt();
			
			//堆疊
			if(choose==1){
				System.out.print("push ");
				
				//輸入要疊幾個數字
				Howmany=input.nextInt();
				
				stack.push(Howmany);
			}
			//取走(從尾巴取)
			else if(choose==2){
				System.out.print("pop ");
				
				//輸入要取走幾個數字
				Howmany=input.nextInt();
				
				stack.pop(Howmany);
			}
			//秀出剩下的數字
			else if(choose==3){
				
				stack.show();
			}
			//結束程式
			else if(choose==4){
				System.out.print("Good Bye");
				return;
			}
		}
	}

}
