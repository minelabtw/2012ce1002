package ce1002.E3.s101502017;

import java.util.Scanner;
import ce1002.E3.s101502017.stack;

public class E31 
{
	public static void main(String[] args)
	{
		stack stack=new stack();
		Scanner a =new Scanner(System .in );
		int num,p ,q;

		do{
			System.out.println( "" );
			System.out.println("1: push");
			System.out.println("2: pop");
			System.out.println("3: show");
			System.out.println("4: exit");
			num =a.nextInt();
			
			switch(num){
			//push case
				case 1:
					System.out.print("push ");
					p = a.nextInt();
					for(int i = 0; i < p ; i++)
					stack.push(a.nextInt());
					break;
			//pop case
				case 2:
					System.out.print("pop ");
					q = a.nextInt();
					for(int i = 0; i < q ; i++){
					if(stack.pop() == false)
						break;
					}
					break;
			//show all the elements in the stack
				case 3:
					stack.show();
					break;
			//exit
				case 4: System.out.println("Good Bye");
					break;
				
			}
		}while(num < 4);
		
	}
	
}
