package ce1002.E3.s101502028;

public class queue {
	int[] queue = new int[100]; // array number
	int size = 0; // size of inside number

	queue() {

	}

	void push(int n) { // push numbers
		if (size < 100) {
			size++;
			queue[size - 1] = n;
		}

		else // arrays are full
			System.out.println("queue is full!");
	}

	void pop() { // pop numbers
		if (size > 0) {
			System.out.print(queue[0] + " ");

			for (int i = 1; i < size; i++)
				queue[i - 1] = queue[i];

			queue[size - 1] = 0;

			size--;
		}

		else { // no more numbers to pop
			System.out.println("");
			System.out.print("queue is empty!");
		}
	}

	void show() { // show numbers
		if (size <= 0)
			System.out.print("queue is empty!");

		else
			for (int i = 0; i < size; i++)
				System.out.print(queue[i] + " ");
	}
}
