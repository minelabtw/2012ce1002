package ce1002.E3.s101502028;

import java.util.*;

public class E32 {
	public static void main(String[] args) {
		int size = 0;
		queue queue = new queue();
		boolean exit = false;
		while (!exit) {
			Scanner scanner = new Scanner(System.in);

			System.out.println("");
			System.out.println("1: push");
			System.out.println("2: pop");
			System.out.println("3: show");
			System.out.println("4: exit");

			int choise = scanner.nextInt(); // input the number of choice

			switch (choise) { // to choose the case
			case 1:
				System.out.print("push ");
				int round = scanner.nextInt();

				for (int i = 0; i < round; i++){
					queue.push(scanner.nextInt()); // use the queue.java function push
					size++; // increase the number of inside number
				}
				break;

			case 2:
				System.out.print("pop ");
				int round2 = scanner.nextInt();

				if (round2 > size) // check the size of the inside number
					round2 = size + 1;

				for (int i = 0; i < round2; i++)
					queue.pop(); // use the queue.java function pop

				System.out.println("");
				break;

			case 3:
				queue.show(); // use the queue.java function show

				System.out.println("");
				break;

			case 4: // case to exit the program
				exit = true;
				break;
			}
		}
		System.out.println("Good Bye");
	}
}
