package ce1002.E3.s101502028;

import java.util.*;

public class E31 {
	public static void main(String[] args) {
		int size = 0;
		stack stack = new stack();
		boolean exit = false;
		while (!exit) {
			Scanner scanner = new Scanner(System.in);

			System.out.println("");
			System.out.println("1: push");
			System.out.println("2: pop");
			System.out.println("3: show");
			System.out.println("4: exit");

			int choise = scanner.nextInt(); // input the number of choice

			switch (choise) { // to choose which case
			case 1:
				System.out.print("push ");
				int round = scanner.nextInt();

				for (int i = 0; i < round; i++) {
					stack.push(scanner.nextInt()); // input number using stack.java function push
					size++; // count the size of the inside number
				}
				break;

			case 2:
				System.out.print("pop ");
				int round2 = scanner.nextInt();

				if (round2 > size) // check the size inside the size
					round2 = size + 1;

				for (int i = 0; i < round2; i++)
					stack.pop(); // use the stack.java function pop

				System.out.println("");
				break;

			case 3:
				stack.show(); // use the stack.java function show

				System.out.println("");
				break;

			case 4: // exit case
				exit = true;
				break;
			}
		}
		System.out.println("Good Bye");
	}
}
