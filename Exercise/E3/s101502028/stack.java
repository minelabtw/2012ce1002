package ce1002.E3.s101502028;

public class stack {
	int[] stack = new int[100]; // number of arrays
	int size = 0; // size of the inside number

	stack() {

	}

	void push(int n) { // push numbers
		if (size < 100) {
			size++;
			stack[size - 1] = n;
		}

		else // more than the array
			System.out.println("Stack is full!");
	}

	void pop() { // pop numbers
		if (size > 0) {
			System.out.print(stack[size - 1] + " ");
			stack[size - 1] = 0;
			size--;
		}

		else { // no more numbers
			System.out.println("");
			System.out.print("Stack is empty!");
		}
	}

	void show() { // show the numbers inside
		if (size <= 0)
			System.out.print("Stack is empty!");

		else
			for (int i = 0; i < size; i++)
				System.out.print(stack[i] + " ");
	}

}