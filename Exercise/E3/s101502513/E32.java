package ce1002.E3.s101502513;

import java.util.*;
import ce1002.E3.s101502513.queue;

public class E32 {
	public static void main(String[] args) {
		int x, y;
		Scanner input = new Scanner(System.in);
		queue UseQueue = new queue();
		
		do {
			System.out.print("1: push\n" + "2: pop\n" + "3: show\n" + "4: exit\n");
			x = input.nextInt();
			
			if( x == 1 ) {
				System.out.print("push ");
				y = input.nextInt(); //decide how much numbers you input
				UseQueue.push(y);
			}
			else if( x == 2 ) {
				System.out.print("pop ");
				
				if( UseQueue.pop() == false )
					System.out.println("Stack is empty!\n");
				else
					System.out.println();
			}
			else if( x == 3 ) {
				UseQueue.show();
			}
			else if( x == 4 ) {
				System.out.println("Good Bye");
				break;
			}
		} while( x >= 1 && x <= 4 );
	}
}