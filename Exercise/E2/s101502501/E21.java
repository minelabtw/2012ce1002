package ce1002.E2.s101502501;
import java.util.Scanner;
public class E21 {
	public static void main (String[] args){
		Scanner cin = new Scanner (System.in);
		
		int chooseNum=0;
		int[] cinArray = new int[100]; //定一個陣列用來存堆疊 (Stack)
		int numberOfArray = 0; //用來計算陣列中有幾項
		
		while (chooseNum!=4){ //輸入不為4時進入迴圈
			System.out.println("1: push");
			System.out.println("2: pop");
			System.out.println("3: show");
			System.out.println("4: exit");
			chooseNum = cin.nextInt();
			
			if(chooseNum == 1){ //輸入為1
				System.out.print("push ");
				int pushNum = cin.nextInt();
				for(int i=numberOfArray; i<numberOfArray+pushNum; i++){
					cinArray[i] = cin.nextInt(); //把輸入存進陣列中
				}
				for(int i=0; i<numberOfArray+pushNum; i++){
					System.out.print(cinArray[i]+" "); //輸出陣列
				}
				for(int i=0; i<pushNum; i++){
					numberOfArray++; //計算有幾項
				}
				System.out.println("\n"); //空行
			}
			if(chooseNum == 2){
				System.out.print("pop ");
				int popNum = cin.nextInt();
				if(numberOfArray-popNum>=0){//陣列裡還沒全pop掉時
					for(int i=numberOfArray-1; i>numberOfArray-1-popNum; i--){ //從最後一項開始輸出
						System.out.print(cinArray[i]+" ");
					}
					for(int i=0; i<popNum;i++){
						numberOfArray--; //項數要減掉
					}					
				}
				else{//陣列裡全pop掉時
					for(int i=numberOfArray-1; i>=0; i--){
						System.out.print(cinArray[i]+" ");
					}
					for(int i=0; i<popNum;i++){
						numberOfArray--;
					}
					System.out.print("\n");
					System.out.print("Stack is empty!");
					
				}
				System.out.println("\n");

			}
			if(chooseNum == 3){
				if(numberOfArray>0){
					for(int i=0; i<numberOfArray; i++){
						System.out.print(cinArray[i]+" ");
					}
				}
				else{//陣列裡全空
					System.out.print("Stack is empty!");
				}
				System.out.println("\n");
			}
			
		}
		if(chooseNum == 4){
			System.out.println("Good Bye");					
		}
	}

}
