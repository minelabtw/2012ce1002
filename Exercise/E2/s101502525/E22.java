package ce1002.E2.s101502525;

import java.util.Scanner;

public class E22 {
	//initialize
	static Scanner input=new Scanner(System.in);
	static int[] queue=new int[100];
	static int front=0;
	static int rear=0;
	
	public static void main(String[] Args){
		while(menu());//repeat
	}
	
	private static boolean menu(){
		//output menu
		System.out.println("1: push");
		System.out.println("2: pop");
		System.out.println("3: show");
		System.out.println("4: exit");
		
		//choose case
		switch(input.next()){
		case "1":{//push
			System.out.print("push ");
			int num=input.nextInt();
			for(int i=0;i<num;i++)
				push(queue);
			System.out.print("\n");
			System.out.printf("\n%d\t%d\n",front,rear);
			return true;
		}
		case "2":{//pop
			System.out.print("pop ");
			int num=input.nextInt();
			for(int i=0;i<num;i++)
				if(!empty(queue))//only pop when not empty
					pop(queue);
				else{
					System.out.print("\nQueue is empty!");
					break;
				}

			System.out.print("\n\n");
			return true;
		}
		case "3":{//show
			if(!empty(queue))//only show when not empty
				show(queue);
			else
				System.out.print("Queue is empty!\n");
			System.out.print("\n");
			return true;
		}
		default://case 4 or wrong input->exit
			System.out.println("Good Bye");
			return false;
		}
	}
	
	private static boolean empty(int[] queue) {
		return front==rear?true:false;
	}
	private static void push(int[] queue){
		queue[rear++]=input.nextInt();//input with rear+1
		if(rear==99){//move when the rear becomes the max
			for(int i=0;i<front-rear+1;i++)
				queue[i]=queue[front];
			rear-=front;
			front=0;
		}
	}
	private static void pop(int[] queue){
		System.out.printf("%d ",queue[front++]);//output with front+1
	}
	private static void show(int[] queue){
		for(int i=front;i<rear;i++)
			System.out.printf("%d ",queue[i]);
		System.out.print("\n");
	}
}
