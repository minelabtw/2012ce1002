package ce1002.E2.s101502525;

import java.util.Scanner;

public class E21 {
	//initialize
	static Scanner input=new Scanner(System.in);
	static int[] stack=new int[100];
	static int top=0;
	
	public static void main(String[] Args){
		while(menu());//repeat
	}
	
	private static boolean menu(){
		//output menu
		System.out.println("1: push");
		System.out.println("2: pop");
		System.out.println("3: show");
		System.out.println("4: exit");
		
		//choose case
		switch(input.next()){
		case "1":{//push
			System.out.print("push ");
			int num=input.nextInt();
			for(int i=0;i<num;i++)
				push(stack);
			System.out.print("\n");
			return true;
		}
		case "2":{//pop
			System.out.print("pop ");
			int num=input.nextInt();
			for(int i=0;i<num;i++)
				if(!empty(stack))//only pop when not empty
					pop(stack);
				else{
					System.out.print("\nStack is empty!");
					break;
				}
			System.out.print("\n\n");
			return true;
		}
		case "3":{//show
			if(!empty(stack))//only show when not empty
				show(stack);
			else
				System.out.print("Stack is empty!\n");
			System.out.print("\n");
			return true;
		}
		default://case 4 or wrong input->exit
			System.out.println("Good Bye");
			return false;
		}
	}
	
	private static boolean empty(int[] stack) {
		return top>0?false:true;
	}
	private static void push(int[] stack){
		stack[top++]=input.nextInt();//input with top+1
	}
	private static void pop(int[] stack){
		System.out.printf("%d ",stack[--top]);//print with top-1
	}
	private static void show(int[] stack){
		for(int i=0;i<top;i++)
			System.out.printf("%d ",stack[i]);
		System.out.print("\n");
	}
}
