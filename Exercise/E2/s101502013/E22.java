package ce1002.E2.s101502013;
import java.util.Scanner;
public class E22 {
	public static void main(String[] args){
		Scanner haha=new Scanner(System.in);
		int input;//選擇push pop show exit
		boolean sw=true;//迴圈開關
		int[][] Queue = new int[100][2];//第一維是輸入數字 第二維則是表示此數字是否有效  若為0則無效 為1則表示有效(模擬Queue)
		while (sw==true)
		{
			System.out.println("1: push\r\n2: pop\r\n3: show\r\n4: exit");
			input = haha.nextInt();
			switch (input)
			{
			case 1:
				push(Queue);
				System.out.println("");//排版換行
				break;
			case 2:
				pop(Queue);
				System.out.println("");		
				break;
			case 3:
				show(Queue);
				System.out.println("\r\n");		
				break;
			case 4:
				sw=false;
				System.out.println("Good Bye!");
				break;
			}
				
		}
	}
	public static void push(int[][] Queue)
	{
		int pointer=-1, input;//pointer表示陣列Queue第一維的第幾個開始為有效資料
		Scanner haha=new Scanner(System.in);
		System.out.print("push ");
		input = haha.nextInt();
		for (int i=(Queue.length-1);i>=0;i--)//Queue.length=100  抓出pointer
		{
			if (Queue[i][1]==1)
			{
				pointer=i;
				break;
			}
		}
		for(int i=0;i<input;i++)
		{
			if (pointer<100)
			{
				Queue[pointer+1][0]=haha.nextInt();//將資料存入
				Queue[pointer+1][1]=1;//標示為有效資料
				pointer++;				
			}
			else
			{
				System.out.println("The Queue is overflow.");
				break;
			}
		}
	}
	public static void pop(int[][] Queue)
	{
		int pointer = -1, input;
		Scanner haha=new Scanner(System.in);
		System.out.print("pop ");
		input = haha.nextInt();
		for (int i=(Queue.length-1);i>=0;i--)//抓出pointer
		{
			if (Queue[i][1]==1)
			{
				pointer=i;
				break;
			}
		}
		for (int i=0;i<input;i++)
		{
			if (pointer == -1)
			{	
				System.out.println("Queue is empty!");
				break;
			}
			else
			{
				System.out.print(Queue[0][0] + " ");
				Queue[pointer][1]=0;
				for (int j=0;j<pointer;j++)//將資料向前移
				{
					Queue[j][0]=Queue[j+1][0];
					
				}
				pointer--;
			}
		}
		
	}
	public static void show(int[][] Queue)
	{
		int pointer=-1;
		for (int i=(Queue.length-1);i>=0;i--)//抓出pointer
		{
			if (Queue[i][1]==1)
			{
				pointer=i;
				break;
			}
		}
		for (int i=0;i<=pointer;i++)
			System.out.print(Queue[i][0] + " ");		
	}
	

}
