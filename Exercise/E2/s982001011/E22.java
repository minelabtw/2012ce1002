package ce1002.E2.s982001011;

import java.util.Scanner;

class Queue {					//環狀Queue
	private int _size;   		//現有陣列長度
	private int head, tail;		//陣列頭尾
	private int[] data;			

	public Queue(int size, int val) {
		data = new int[10];
		head = 0;
		tail = size - 1;
		_size = size;
		for (int i = 0; i < size; ++i) {
			data[i] = val;
		}
	}

	public Queue() {
		_size = 0;
		head = 0;
		tail = 99;
		data = new int[100];
	}

	public void push(int n) {  //push
		++_size;
		tail = (tail + 1) % 100;
		data[tail] = n;
	}

	public int pop() {		//pop
		if (!empty()) {
			--_size;
			int n = data[head];
			head = (head + 1) % 100;
			return n;
		} else {
			return -1;
		}
	}

	public boolean empty() {		//判斷是否為空
		return tail == (head - 1) % 100;
	}

	public int[] show() {			//function 3
		int[] rtn = new int[_size];
		for (int i = 0; i < _size; ++i) {
			rtn[i] = data[(head + i) % 100];
		}
		return rtn;
	}

	public int size() {       
		return _size;
	}

}

public class E22 {
	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);
		int flag;

		Queue myQueue = new Queue();
		do {
			System.out.print("1: push\n2: pop\n3: show\n4: exit\n");
			System.out.print("請輸入選項\n");
			flag = input.nextInt();
			int n;
			switch (flag) {
			case 1:
				System.out.print("push ");
				n = input.nextInt();
				for (int i = 0; i < n; ++i) {
					myQueue.push(input.nextInt());
				}
				System.out.println();
				break;
			case 2:
				System.out.print("pop ");
				n = input.nextInt();
				for (int i = 0; i < n; ++i) {
					if (!myQueue.empty())
						System.out.print(myQueue.pop() + " ");
					else {
						System.out.print("\nQueue is empty!");
						break;
					}
				}
				System.out.println();
				break;
			case 3:
				int[] nums = myQueue.show();
				if (myQueue.empty()) {
					System.out.print("Queue is empty!");
				} else {
					for (int i = 0; i < myQueue.size(); ++i) {
						System.out.print(nums[i] + " ");
					}
				}
				System.out.println();
				break;
			case 4:
				System.out.println("Good bye!");
				break;
			default:
				break;
			}
			System.out.println();

		} while (flag != 4);
	}
}
