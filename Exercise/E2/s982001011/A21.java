package ce1002.s982001011;

import java.util.Scanner;

public class A21 {
	public static void main(String[] args){
		int lo , up;	//上下屆
		boolean check;	//有沒整除
		Scanner in = new Scanner(System.in);
		System.out.print( "lower bound=" );
		lo = in.nextInt();
		System.out.print( "upper bound=" );
		up = in.nextInt();
		int[]primes = new int[up]; //是質數的存進來
		int n=0;
		for( int i = 2; i < up; i++ ) {
			check=true;
			for( int j = 0; j < n; j++ ) {
				if(i%primes[j]==0) {
					check=false;
					break;
				}
			}
			if( check ) primes[n++]=i;
		}
		for( int i = 0; i < n; i++) {
			if( primes[i] >= lo ) System.out.printf("%1$d ",primes[i]);
			}
	 }

}
