package ce1002.E2.s100502201;

import java.util.Scanner;

public class E21 {
	public static void main(String[] args)
	{
	    int [] stack= new int[100];
	    int top;
	    int input, select;

	    top = creates(stack);

	    while(true)
	    {
	        System.out.println("1: push");
	        System.out.println("2: pop");
	        System.out.println("3: show");
	        System.out.println("4: exit");
	        Scanner com = new Scanner(System.in);
	        select = com.nextInt();

	        if(select == 4)
	        {
	        	System.out.print("Good Bye");
	            break;
	        }
	        switch(select)
	        {
	            case 1:
	                System.out.print("push ");
	                Scanner time = new Scanner(System.in);
	                Scanner push = new Scanner(System.in);
	                int pu=time.nextInt();
	                for (int j=1;j<=pu;j++)
	                {
	                	input = push.nextInt();
	                	top = push(stack, top, input);
	                }
	                break;
	            case 2:
	            	if(top == -1)
	            	{
	            		System.out.print("Stack is empty!\n");
	            		break;
	            	}
	            	Scanner ptime = new Scanner(System.in);
	            	System.out.print("pop ");
	            	int po = ptime.nextInt();
	            	for (int l=1;l<=po;l++)
	            	{
	            		System.out.printf("%d ",stack[top]);
	            		top = pop(stack, top);
	            		if (top==-1)
	            		{
	            			System.out.print("\nStack is empty!\n");
	            			break;
	            		}
	            	}
	            	System.out.print("\n");
	                break;
	            case 3:
	            	if(top == -1)
	            	{
	            		System.out.print("Stack is empty!\n");
	            		break;
	            	}
	                show(stack, top);
	                break;
	            default:
	                //System.out.print("Invalid input.\n");
	            	break;
	        }
	    }

	    return;
	}

	public static int creates(int [] stack)
	{
	    int i;

	    for(i = 0; i < 99; i++)
	        stack[i] = 0;

	    return -1;
	}

	public static int stacktop(int [] stack, int top)
	{
	    return stack[top];
	}

	public static int push(int [] stack, int top, int item)
	{
	    int t = top;

	    if(t >= 99)
	    {
	        System.out.print("Stack is full!\n");
	        return t;
	    }

	    stack[++t] = item;

	    return t;
	}

	public static int pop(int [] stack, int top)
	{
	    int t = top;

	    if(t == -1)
	    {
	        System.out.print("Stack is empty!\n");
	        return t;
	    }

	    return --t;
	}

	public static void show(int [] stack, int top)
	{
	    int t = top;
	    	int j=0;
	    while(t>-1)
	    {
	        System.out.printf("%d ", stack[j]);
	        j++;
	        t--;
	    }
	    System.out.print("\n");
	}

}
