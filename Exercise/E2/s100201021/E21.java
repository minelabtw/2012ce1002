package ce1002.E2.s100201021;
import java.util.Scanner;
public class E21 {
	public static void main(String[] args){
		int[] st = new int[100];	//st=stack;
		int ch=0,ns,per=-1;			//ch = input,per= now top;  per=-1 > stack is empty;
		Scanner num = new Scanner(System.in);
		while( ch !=4){	//if ch = 4, break and output "good bye"
			System.out.println("\n1: push\n2: pop\n3: show\n4: exit");
			ch =num.nextInt();
			switch(ch){
			case 1:	//push
				System.out.print("push ");
				ns =num.nextInt();
				for(int i=1;i<=ns;i++){
					st[per+i]=num.nextInt();
				}
				per+=ns;	//record now top
				break;
			case 2:	//pop
				System.out.print("pop ");
				ns =num.nextInt();
				for(int i=0;i<ns;i++){
					if(per-i<0){
						per=-1;	//if per<0 (stack is empty,reset per=-1;
						System.out.print("\nStack is empty!");
						break;
					}
					else 
						System.out.print(st[per-i]+" ");
				}
				System.out.println();
				per-=ns;	//record now top
				if(per<0)	//if per<0 (stack is empty,reset per=-1;
					per=-1;
				break;
			case 3:	//show
				for(int i=0;i<=per;i++)
					System.out.print(st[i]+" ");				
				if(per<0){
					System.out.print("Stack is empty!");
					per =-1;	//if per<0 (stack is empty,reset per=-1; (for debug)
				}
				System.out.println();
				break;
			default://input != 1|2|3|4 ,reset;
				break;
			}
		}
		System.out.println("Good Bye");
	}
}