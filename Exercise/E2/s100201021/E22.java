package ce1002.E2.s100201021;
import java.util.Scanner;
public class E22 {
	public static void main(String[] args){
		Scanner num=new Scanner(System.in);
		int[] qu=new int[100];
		int ch=0,ns,per=-1;	//per = tail
		while( ch !=4){	//if ch = 4, break and output "good bye"
			System.out.println("\n1: push\n2: pop\n3: show\n4: exit");
			ch =num.nextInt();
			switch(ch){
			case 1:	//push
				System.out.print("push ");
				ns =num.nextInt();
				for(int i=1;i<=ns;i++){
					qu[per+i]=num.nextInt();
				}
				per+=ns;	//record now tail
				break;
			case 2:	//pop
				System.out.print("pop ");
				ns =num.nextInt();
				for(int i=0;i<ns;i++){
					if(i>per){
						System.out.println("\nQueue is empty!");
						break;
					}
					else
						System.out.print(qu[i]+" ");
				}
				for(int i=ns;i<=per;i++){
					qu[i-ns]=qu[i];
				}
				per-=ns;		//record now tail
				if(per<0)
					per=-1;		//if per<0 (Queue is empty,reset per=-1; (for debug)
				break;
			case 3:	//show
				for(int i=0;i<=per;i++)
					System.out.print(qu[i]+" ");				
				if(per<0){
					System.out.print("\nQueue is empty!");
					per =-1;	//if per<0 (Queue is empty,reset per=-1; (for debug)
				}
				System.out.println();
				break;
			default://input != 1|2|3|4 ,reset;
				break;
			}
		}
		System.out.println("Good Bye");
	}

}
