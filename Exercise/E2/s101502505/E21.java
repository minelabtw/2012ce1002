package ce1002.E2.s101502505;
import java.util.Scanner;
public class E21 {
	public static void main(String[] args){
		Scanner input = new Scanner(System.in);
		System.out.println("1: push\r\n2: pop\r\n3: show\r\n4: exit");
		int [] x = null;
		int a;
		do
		{
			a = input.nextInt();
			if(a == 1)//push
			{
				System.out.print("push ");
				int b = input.nextInt();
				x = new int[b];
				for(int i = 0 ; i < b; i++)//input number into array
				{
					int c = input.nextInt();
					x[i] = c;
				}
			}
			else if(a == 2)//pop
			{
				System.out.print("pop ");
				int d = input.nextInt();
				
				for(int j = 0 ; j < d ; j++)
				{
					if(x.length-j-1 < 0)
						break;
					if(x[x.length-j-1] != 0)
					{
						System.out.print(x[x.length-j-1] + " ");
						x[x.length-j-1] = '\0';//turn the pop out number into 0
					}
				}
				System.out.println(" ");
				if(x.length - d < 0)
				{
					System.out.print("Stack is empty!");
					System.out.print("\r\n");
				}
			}
			else if(a == 3)//show
			{
				for(int k = 0 ; k < x.length ; k++)
				{
					if(x[k] != 0)
						System.out.print(x[k] + " ");
					int o = 0;
					if(x[0] == 0 && x[0] == x[k+1])
						o++;
					if(o == x.length - 1)
						System.out.print("Stack is empty!");
				}
				System.out.print("\r\n");
			}
		}while(a != 4);
		
		if(a == 4)//exit
			System.out.print("Good Bye");
	}
}
