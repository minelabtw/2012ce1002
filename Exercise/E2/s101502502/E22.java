package ce1002.E2.s101502502;

import java.util.Scanner;

public class E22 
{
	public static void main(String[] args)
	{
		Scanner cin = new Scanner(System.in);
		int[] inputArray = new int[100];//用陣列儲存輸入的值
		int a=0;
		int count=0;
		int quit=0;
		
		while(quit==0)
		{
			System.out.println("1: push");
			System.out.println("2: pop");
			System.out.println("3: show");
			System.out.println("4: exit");
			
			int operate = cin.nextInt();//操作
			
			if(operate==1)//push
			{
				System.out.print("push ");
				count = cin.nextInt();
				a=count;
				
				for(int i = 0 ; i < count ; i++)//把輸入的值存入陣列裡
				{
					inputArray[i] = cin.nextInt();
				}
				System.out.print("\n");
			}
			else if(operate==2)//pop
			{
				System.out.print("pop ");
				count = cin.nextInt();
				
				if(a>count)//沒有超出陣列的情況
				{
					for(int i = 0 ; i < count; i++ )
					{
						System.out.print(inputArray[i]+ " ");
					}
					System.out.print("\n\n");
				}
				
				
				if(a<=count)//有超出陣列的情況
				{
					for(int i = 0 ; i < a; i++ )
					{
						System.out.print(inputArray[i]+ " ");
					}
					System.out.println();
					System.out.print("Queue is empty!\n\n");
				}
				
				a=a-count;
				
				for(int i = 0 ; i < a ; i++)//把pop出去的替換掉
				{
					inputArray[i] = inputArray[i+count];
				}
					
			}
			else if(operate==3)//show
			{
				if(a>0)//當陣列中還有數字的情況，則輸出
				{
					for(int i = 0 ; i < a ; i++)
					{
						System.out.print(inputArray[i]+ " ");
					}
					System.out.print("\n\n");
				}
				
				
				if(a<=0)//陣列空了
				{
					System.out.print("Queue is empty!\n\n");
				}
			}
			else//exit
			{
				System.out.print("Good Bye");
				quit=1;
			}
		}
		
	}
}
