package ce1002.E2.s101502513;

import java.util.*;

public class E22 {
	public static void main(String args[]) {
		Scanner input = new Scanner(System.in);
		int x = 0, y = 0, num, i = 0; //i is the length of the array
		int[] queue = new int[100];

		while( x >= 0 && x < 4 ) {
			System.out.println("1: push\n" + "2: pop\n" + "3: show\n" + "4: exit");
			x = input.nextInt();

			if (x == 1) { //input numbers into array
				System.out.print("push ");
				y = input.nextInt(); //decide how much numbers you input
				int j = i;

				for ( int a = i; a < y + j; a++, i++ ) {
					num = input.nextInt();
					queue[a] = num;
				}
				System.out.println();
			}
			else if (x == 2) { //remove numbers
				System.out.print("pop ");
				y = input.nextInt();
				int j = y;
				
				for( int a = 0; a < y ; a++, i-- ) {
					System.out.print(queue[a] + " ");
					queue[a] = 0;
					
					if( i == 1 ) {
						System.out.print("\nQueue is empty!");
						break;
					}
				}
				for( int b = 0; b < i; b++, j++ ) //arrange
					queue[b] = queue[j];
				System.out.println("\n");
			}
			else if( x == 3 ) { //show array
				if( i != 0 && i != 1 ) {
					for( int a = 0; a < i; a++ )
						System.out.print(queue[a] + " ");
				}
				else if( i == 0 || i == 1 ) {
					System.out.print("Queue is empty!");
				}
				System.out.println("\n");
			}
			else if (x == 4) //end program
				System.out.println("Good Bye");
		}
	}
}
