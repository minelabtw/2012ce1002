package ce1002.E2.s100502006;

import java.util.Scanner;

public class E22 {
	static Scanner input=new Scanner(System.in);
	public static void main(String[] args){
		int choice=0,numberInQueue=0;//store choice and the number in queue
		String[] q=new String[100];//declare a queue
		do{
			System.out.println("\n1: push\n"+"2: pop\n"+"3: show\n"+"4: exit");
			choice=input.nextInt();//make a choice
			switch(choice){
			case 1://push
				System.out.print("push ");
				int numberToPush=input.nextInt();
				for(int i=0;i<numberToPush;i++){
					q[numberInQueue]=input.next();
					numberInQueue++;
				}
				break;
			case 2://pop(FIFO)
				System.out.print("pop ");
				int numberToPop=input.nextInt();
				for(int i=0;i<numberToPop;i++){
					if(numberInQueue<=0){
						System.out.print("\nQueue is empty!");
						break;
					}
					else{
						System.out.print(q[0]+" ");
						numberInQueue--;
						for(int j=0;j<numberInQueue;j++){//shift the queue
							q[j]=q[j+1];
						}						
					}
				}
				System.out.println();
				break;
			case 3://show
				if(numberInQueue<=0){
					System.out.print("Queue is empty!");
				}
				else{
					for(int i=0;i<numberInQueue;i++){
						System.out.print(q[i]+" ");
					}
				}
				System.out.println();
				break;
			case 4:
				System.out.println("Good Bye");
				break;
			}
		}while(choice!=4);
	}
}
