package ce1002.E2.s100502006;

import java.util.Scanner;

public class E21 {
	static Scanner input=new Scanner(System.in);
	public static void main(String[] args){
		int choice=0,numberInStack=0;//store choice and the number in stack
		String[] s=new String[100];//declare a stack
		do{
			System.out.println("\n1: push\n"+"2: pop\n"+"3: show\n"+"4: exit");
			choice=input.nextInt();//make a choice
			switch(choice){
			case 1://push
				System.out.print("push ");
				int numberToPush=input.nextInt();
				for(int i=0;i<numberToPush;i++){
					s[numberInStack]=input.next();
					numberInStack++;
				}
				break;
			case 2://pop(LIFO)
				System.out.print("pop ");
				int numberToPop=input.nextInt();
				for(int i=0;i<numberToPop;i++){
					if(numberInStack<=0){
						System.out.print("\nStack is empty!");
						break;
					}
					else{
						System.out.print(s[numberInStack-1]+" ");
						numberInStack--;
					}
				}
				System.out.println();
				break;
			case 3://show
				if(numberInStack<=0){
					System.out.print("Stack is empty!");
				}
				else{
					for(int i=0;i<numberInStack;i++){
						System.out.print(s[i]+" ");
					}
				}
				System.out.println();
				break;
			case 4:
				System.out.println("Good Bye");
				break;
			}
		}while(choice!=4);
	}
}
