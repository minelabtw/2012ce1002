package ce1002.E2.s100502203;

import java.util.Scanner;

public class E22 {
	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);
		int Max_size = 100;
		boolean tag = false;
		// circular queue, use tag to note full or empty, tag = true implies full
		int queue[] = new int[Max_size], front = Max_size - 1, rear = front, opt;
		System.out.println("1: push\n" + "2: pop\n" + "3: show\n" + "4: exit");
		opt = input.nextInt();
		while (opt != 4) {
			int number;
			switch (opt) {
			case 1:
				System.out.print("push ");
				number = input.nextInt();
				for (int i = 0; i < number; i++) {
					if (tag != true) { // queue not full
						queue[rear] = input.nextInt();
						rear = (rear + 1) % Max_size; // circle
						if (rear == front) // rear = front -> when push note "full"
							tag = true;
					} else {
						System.out.println("\nQueue is full!");
						break;
					}
				}
				break;

			case 2:
				System.out.print("pop ");
				number = input.nextInt();
				for (int i = 0; i < number; i++) {
					if (tag != true && rear == front) { // queue empty
						System.out.print("\nQueue is empty!");
						break;
					}
					else{
						if (front == rear)
							break;
						System.out.print(queue[front] + " ");
						front = (front + 1) % Max_size; // circle
						tag = false; // every time pop means not full
					}
				}
				System.out.println(" ");
				break;

			case 3:
				if (tag != true && rear == front) // queue empty
					System.out.print("Queue is empty!");
				for (int i = front; i != rear; i = (i + 1) % Max_size)
					System.out.print(queue[i] + " ");
				System.out.println(" ");
				break;
			}
			System.out.println("\n1: push\n" + "2: pop\n" + "3: show\n"
					+ "4: exit");
			opt = input.nextInt();
		}
		System.out.print("Good Bye");
		input.close();
	}
}
