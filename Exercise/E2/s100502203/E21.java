package ce1002.E2.s100502203;

import java.util.Scanner;

public class E21 {
	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);
		int Max_size = 100; // size = 100

		int stack[] = new int[Max_size], top = -1, opt;
		System.out
				.println("1: push\n" + "2: pop\n" + "3: show\n" + "4: exit");
		opt = input.nextInt();
		while (opt != 4) { // 4 = exit
			int number;
			switch (opt) {
			case 1:
				System.out.print("push ");
				number = input.nextInt();
				for (int i = 0; i < number; i++)
					if (top < Max_size - 1) // stack not full
						stack[++top] = input.nextInt();
					else
						System.out.println("Stack is full!");
				break;

			case 2:
				System.out.print("pop ");
				number = input.nextInt();
				for (int i = 0; i < number; i++)
					if (top > 0) // stack not empty
						System.out.print(stack[top--] + " ");
					else{
						System.out.print("\nStack is empty!");
						break;
					}	
				System.out.println(" ");
				break;

			case 3:
				if(top <= 0){ // stack empty
					System.out.print("\nStack is empty!");
					break;
				}
				for (int i = 0; i <= top; i++)
					System.out.print(stack[i] + " ");
				System.out.println(" ");
				break;
			}
			System.out.println("\n1: push\n" + "2: pop\n" + "3: show\n"
					+ "4: exit");
			opt = input.nextInt();
		}
		System.out.print("Good Bye");
		input.close();
	}
}
