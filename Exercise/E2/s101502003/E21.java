package ce1002.E2.s101502003;
import java.util.Scanner;
public class E21 {
	public static void main(String[] args)
	{
		Scanner input = new Scanner(System.in);
		int[] array = new int[100];
		int a=1,b=0,c=0,d=0; 
		while(a==1)
		{
			System.out.println("1: push");
			System.out.println("2: pop");
			System.out.println("3: show");
			System.out.println("4: exit");
			int x = input.nextInt();
			if(x==1)
			{
				System.out.print("push ");
				int want = input.nextInt();
				for(int i=0;i<want;i++)
				{
					array[i]=input.nextInt(); // 輸入陣列
				}
				System.out.print("\n\n");
				b=want; // 一開始的
				d=want; // 用來算陣列裡剩下的數
			}
			
			if(x==2)
			{
				System.out.print("pop ");
				int pop = input.nextInt();
				if(pop>(b-c)) // 如果輸入的數字超過陣列裡剩的數字
				{
					for(int k=(b-c-1);k>=0;k--) // 先印出全部陣列裡的數字
						System.out.print(array[k]+" ");
					System.out.print("\nStack is empty!\n\n");
				}
				else
				{
					for(int k=(b-c-1);k>=(b-c-pop);k--) // 輸出倒數pop個的數字
						System.out.print(array[k]+" ");
					System.out.print("\n\n");
				}
				c=pop; 
				d=d-c; // d是算最後一個數的位子在哪
			}
			else if (x==3)
			{
				if(c>b) // 如果陣列裡的數字都pop了,輸出文字
					System.out.print("Stack is empty!\n\n");
				else // 把剩下的數字show出來
				{
					for(int j=0;j<d;j++)
						System.out.print(array[j]+" ");
					System.out.print("\n\n");
				}
			}
			else if(x==4) // 離開迴圈
			{
				System.out.print("Good bye");
				break;
			}	
		}
	}
}
