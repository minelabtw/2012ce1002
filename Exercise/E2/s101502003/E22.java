package ce1002.E2.s101502003;
import java.util.Scanner;
public class E22 {
	public static void main(String[] args)
	{
		Scanner input = new Scanner(System.in);
		int[] array = new int[100];
		int a=1,b=0,c=0,d=0,e=0;  // 用來取代want,pop等方便用在各種地方所設的數
		while(a==1)
		{
			System.out.println("1: push");
			System.out.println("2: pop");
			System.out.println("3: show");
			System.out.println("4: exit");
			int x = input.nextInt();
			if(x==1)
			{
				System.out.print("push ");
				int want = input.nextInt();
				for(int i=0;i<want;i++) // 輸入want個數的陣列
				{
					array[i]=input.nextInt();
				}
				System.out.print("\n");
				b=want;
			}
			
			if(x==2)
			{
				e=b-c; // 用來計算陣列裡剩幾個數
				System.out.print("pop ");
				int pop = input.nextInt();
				if(pop>e) // 如果要跳出的數筆陣列裡的數還多,先列出陣列裡剩下的數,在輸出文字
				{
					for(int k=d;k<b;k++)
						System.out.print(array[k]+" ");
					System.out.print("\nQueue is empty!\n\n");
				}
				else
				{
					for(int k=d;k<(pop+d);k++)
						System.out.print(array[k]+" "); // 列出pop出來的數字
					System.out.print("\n\n");
				}
				c=pop;
				d=d+c; // 計算陣列裡剩下的第一個數的位置在哪
			}
			else if (x==3)
			{
				e=b-c;
				if(c>e) // 如果陣列裡沒數字,輸出文字
					System.out.print("Queue is empty!\n\n");
				else
				{
					for(int j=d;j<b;j++) // 列出剩下的數字
						System.out.print(array[j]+" ");
					System.out.print("\n");
				}
			}
			else if(x==4)// 如果x=4,離開迴圈
			{
				System.out.println("Good bye");
				break;
			}	
		}	
	}
}

