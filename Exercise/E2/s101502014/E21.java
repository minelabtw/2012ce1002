package ce1002.E2.s101502014;
import java.util.Scanner;
public class E21 {
	public static void main(String[] args) {
		Scanner cin = new Scanner(System.in); //cin為輸入的動作
		int n = 0 , num , index = 0;  //n為使用者輸入的功能代碼  , num為輸入或輸出的個數 , index為游標在陣列中的位置，預設為0
		int[] stack = new int[100]; 
		while(true)
		{
			System.out.println("1: push");
			System.out.println("2: pop");
			System.out.println("3: show");
			System.out.println("4: exit");
		    n = cin.nextInt(); 
		    if(n == 1)
		    {
		    	System.out.print("push ");
		    	num = cin.nextInt();
		    	for(int i = index ; i < index + num ; i++) //從陣列的最後面丟進數字
		    		stack[i] = cin.nextInt();
		    	index += num; //index移到陣列中最後一個數字的下一個位置
		    }
		    else if(n == 2)
		    {
		    	System.out.print("pop ");
		    	num = cin.nextInt(); 
		    	if(num > index) //判斷丟出的個數是否會使陣列變成空陣列
		    	{
		    		for(int j = index - 1 ; j >= 0 ; j--) //從陣列的最後面丟出數字
			    		System.out.print(stack[j] + " ");
		    		System.out.print("\nStack is empty!"); //輸出空陣列的訊息
		    		index = 0; //index移到陣列中的第一個位置
		    	}
		    	else
		    	{
			    	for(int j = index - 1 ; j >= index - num ; j--) //從陣列的最後面丟出數字
			    		System.out.print(stack[j] + " ");
			    	index -= num; //index移到陣列中最後一個數字的下一個位置
		    	}
		    	System.out.println("");
		    }
		    else if(n == 3)
		    {
		    	if(index == 0) //若index為0 , 代表沒有數字 , 則輸出空陣列的訊息
		    		System.out.println("Stack is empty!");
		    	else
		    	{
		    		for(int k = 0 ; k < index ; k++) //輸出目前陣列的狀況
		    			System.out.print(stack[k] + " ");
		    		System.out.println("");
		    	}
		    }
		    else
		    {
		    	System.out.println("Good Bye"); //輸出離開訊息
		    	break;
		    }
		    System.out.println("");
		}
	}
}