package ce1002.E2.s101502028;

import java.util.*;

public class E21 {
	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);
		int array[] = new int[100]; // 100 array
		int n = 0; // number if input numbers
		do {
			System.out.print("1: push\n2: pop\n3: show\n4: exit\n");
			int x = input.nextInt(); // select case
			if (x == 1) { // if case 1
				System.out.print("push ");
				int y = input.nextInt();
				for (int a = 0; a < y; a++) {
					array[n] = input.nextInt(); // input the array
					n++; // number of input number
				}
			} else if (x == 2) { // case 2
				System.out.print("pop ");
				int i = input.nextInt();
				for (int w = 0; w < i; w++) {
					System.out.print(array[n - 1] + " "); // output from the
															// last array
					n--; // substract the number of input numbers
					if (n == 0) { // if there's no more array
						System.out.print("\nStack is empty!"); // message
						break;
					}
				}
				System.out.print("\n");
			} else if (x == 3) {
				if (n == 0)
					System.out.print("Stack is empty!"); // output the rest
															// input numbers
				for (int a = 0; a < n; a++) {
					System.out.print(array[a] + " ");
				}
				System.out.print("\n");
			} else if (x == 4) { // end
				System.out.print("Good Bye");
				break;
			}
			System.out.print("\n");
		} while (true);
	}
}
