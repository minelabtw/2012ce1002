package ce1002.E2.s101502028;

import java.util.Scanner;

public class E22 {
	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);
		int array[] = new int[100];
		int n = 0, z = 0;
		do {
			System.out.print("1: push\n2: pop\n3: show\n4: exit\n");
			int x = input.nextInt(); // input case
			if (x == 1) { // case 1
				System.out.print("push ");
				int y = input.nextInt();
				for (int a = 0; a < y; a++) {
					array[n] = input.nextInt(); // input array numbers
					n++; // number if input numbers
				}
			} else if (x == 2) { // case 2
				System.out.print("pop ");
				int i = input.nextInt();
				for (int w = 0; w < i; w++) {
					System.out.print(array[w] + " "); // output from the first
														// array
					n--; // substract the number of arrays
					if (n == 0) { // if no more array
						System.out.print("\nQueue is empty!"); // message
						break;
					}
				}
				for (int o = 0; o <= n; o++) { // move the array to array[0]
					array[o] = array[i];
					i++;
				}
				System.out.print("\n");
			} else if (x == 3) { // case 3
				if (n == 0)
					System.out.print("Queue is empty!");
				for (int a = 0; a < n; a++) { // output the rest arrays
					System.out.print(array[a] + " ");
				}
				System.out.print("\n");
			} else if (x == 4) { // case 4
				System.out.print("Good Bye"); // good bye
				break;
			}
			System.out.print("\n");
		} while (true);
	}
}
