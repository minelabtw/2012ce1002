package ce1002.E2.s100201510;

import java.util.Scanner;

public class E21 {
	public static void main(String[] args) {
		// an array of capacity 100.
		int[] stack = new int[100] ;
		// length is the number of elements in stack.
		// it will be 0 while stack is empty.
		int length = 0 ;
		// number is the number want to push or pop.
		int number = 0 ;
		// selection is use to switch functions.  
		int selection = 0 ;
		
		Scanner inputstack = new Scanner(System.in);
		Scanner input = new Scanner(System.in);
		while(true){
			//print selections.
			System.out.print("1: push\n2: pop\n3: show\n4: exit\n");
			selection = input.nextInt();
			
			if(selection > 4 || selection <1){ // check wrong option.
				System.out.print("Illigle option.");
				System.exit(1);
			}
			
			if(selection == 1){//push
				System.out.print("push ");
				number = new Scanner(System.in).nextInt();
				length += number ;
				for(int i = 0 ; i < number ; i++){
					stack[i] = inputstack.nextInt();
				}
				System.out.print("\n");
			}
			else if(selection == 2){//pop
				System.out.print("pop ");
				number = new Scanner(System.in).nextInt();
				length -= number ;
				
				if(length <= 0){
					length = 0 ;
					System.out.print("Stack is empty!\n");
					for(int i = length ; i < length - number ; i--){
						stack[i] = 0;
					}
				}
				else{
					for(int i = length ; i < length - number ; i--){
						stack[i] = 0;
					}
					for(int i = length ; i < length - number ; i--){
						System.out.printf("%d " , stack[i]);
					}
				}
				System.out.print("\n");
			}
			else if(selection == 3){//show
				for(int i = 0 ; i < length ; i++)
					System.out.printf("%d " , stack[i]);
				System.out.print("\n");
			}
			else if(selection == 4){//exit
				System.out.print("Good Bye");
				inputstack.close();
				input.close();
				System.exit(1);
			}
		}
	}
}