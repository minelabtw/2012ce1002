package ce1002.E2.s101502520;

import java.util.Scanner;

public class E22 {
	
	
	public static void push(int[] stack, int push_num, int pointer, Scanner inp){
		for(int i = pointer; i < pointer + push_num; i++){
			if(i >= 100){
				i -= 100;
				pointer -= 100;
			}
			stack[i] = inp.nextInt();
		}
	}

	public static boolean pop(int[] stack, int pop_num, int start, int pointer, boolean over){
		for(int i = start; i < start + pop_num; i++){
			if(i >= 100){
				i -= 100;
				start -= 100;
				over = false;
			}
			if(i >= pointer && over == false){
				System.out.println();
				System.out.print("Queue is empty!");
				break;
			}
			else
				System.out.print(stack[i] + " ");
		}
		return over;
	}
	
	public static void show(int[] stack, int start, int pointer, boolean over){
		if(over){
			if(pointer == start)
				System.out.print("Queue is empty!");
			else{
				for(int i = start; i < 100; i++)
					System.out.print(stack[i] + " ");
				for(int i = 0; i < pointer; i++)
					System.out.print(stack[i] + " ");		
			}
		}
		else{
			if(pointer == start)
				System.out.print("Queue is empty!");
			else
				for(int i = start; i < pointer; i++)
					System.out.print(stack[i] + " ");
		}
		
	}
	
	public static void main(String[] args) {
		boolean over = false;
		int option, input, start = 0, pointer = 0;//using this "pointer" to record the next index it store.
		int[] stack;							  //using "start" to record which one pop next.
		stack = new int[100];
		
		Scanner inpScanner = new Scanner(System.in);
		
		while(true){
			System.out.println("1: push");
			System.out.println("2: pop");
			System.out.println("3: show");
			System.out.println("4: exit");
			option = inpScanner.nextInt();
			
			switch (option) {
			case 1:
				System.out.print("push ");
				input = inpScanner.nextInt();
				push(stack, input, pointer, inpScanner);
				pointer += input;
				if(pointer >= 100){
					pointer -= 100;
					over = true;
				}
				break;
			case 2:
				System.out.print("pop ");
				input = inpScanner.nextInt();
				over = pop(stack, input, start, pointer, over);
				System.out.println();
				start += input;
				if(start >= 100)
					start -= 100;
				if(start > pointer && over == false)
					start = pointer;
				break;
			case 3:
				show(stack, start, pointer, over);
				System.out.println();
				break;
			case 4:
				System.out.print("Good Bye");
				inpScanner.close();
				return;

			default:
				break;
			}
			System.out.println();
		}
	}
}
