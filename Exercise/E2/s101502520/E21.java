package ce1002.E2.s101502520;

import java.util.Scanner;

public class E21 {
	
	
	public static void push(int[] stack, int push_num, int pointer, Scanner inp){
		for(int i = pointer; i < pointer + push_num; i++){
			stack[i] = inp.nextInt();
		}
	}

	public static void pop(int[] stack, int pop_num, int pointer){
		for(int i = pointer - 1; i > pointer - pop_num - 1; i--){
			if(i < 0){
				System.out.println();
				System.out.print("Stack is empty!");
				break;
			}
			else
				System.out.print(stack[i] + " ");
		}		
	}
	
	public static void show(int[] stack, int pointer){
		if(pointer <= 0)
			System.out.print("Stack is empty!");
		else
			for(int i = 0; i < pointer; i++)
				System.out.print(stack[i] + " ");
	}
	
	public static void main(String[] args) {
		int option, input,  pointer = 0;//using this "pointer" to record the next index it store.
		int[] stack;
		stack = new int[100];
		
		Scanner inpScanner = new Scanner(System.in);
		
		while(true){
			System.out.println("1: push");
			System.out.println("2: pop");
			System.out.println("3: show");
			System.out.println("4: exit");
			option = inpScanner.nextInt();
			
			switch (option) {
			case 1:
				System.out.print("push ");
				input = inpScanner.nextInt();
				push(stack, input, pointer, inpScanner);
				pointer += input;
				break;
			case 2:
				System.out.print("pop ");
				input = inpScanner.nextInt();
				pop(stack, input, pointer);
				System.out.println();
				pointer -= input;
				if(pointer < 0)
					pointer = 0;
				break;
			case 3:
				show(stack, pointer);
				System.out.println();
				break;
			case 4:
				System.out.print("Good Bye");
				inpScanner.close();
				return;

			default:
				break;
			}
			System.out.println();
		}
	}
}
