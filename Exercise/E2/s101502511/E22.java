package ce1002.E2.s101502511;

import java.util.*;

public class E22 {

	public static void main(String[] args) {
		int input,size,pop=0,n=0;//n is arraysize now
		
		int [] stack = new int[100];
		do{
			Scanner scanner = new Scanner(System.in);//new scanner
			
			System.out.println("1: push");
			System.out.println("2: pop");
			System.out.println("3: show");
			System.out.println("4: exit");
			
			input=scanner.nextInt();
			
			if(input==1){
				System.out.print("push ");
				size=scanner.nextInt();
				int s=size;//size
				for(int i=n;i<n+s;i++)//input array
					stack[i]=scanner.nextInt();
				n=n+s;//now arraysize
			}
			else if(input==2){
				System.out.print("pop ");
				pop=scanner.nextInt();
				int k;
				if(n<pop)//防止印出0
					k=n;
				else
					k=pop;
				for(int i=0;i<k;i++){//清空pop的數
					System.out.print(stack[i]+" ");
					stack[i]='\0';
				}
				for(int i =0;i<=n-pop;i++){//整理陣列,把被清空的補回後面的數
					stack[i]=stack[i+pop];
				}
				System.out.println();
				if(pop>=n){//如果pop大於n,n=0;
					System.out.println("Stack is empty!");
					n=0;
				}
				else
					n=n-pop;
			}
			else if(input==3){
				if(n==0){
					System.out.println("Stack is empty");
				}
				for(int i=0;i<n;i++)//show array
					System.out.print(stack[i]+" ");
				System.out.println();
			}
			else if(input==4){
				System.out.println("Good Bye");
				System.exit(0);
			}
		
		}while(n>-1);
		
	}
}

