package ce1002.E2.s101502511;

import java.util.*;

public class E21 {

	public static void main(String[] args) {
		int input, size, pop, n = 0;// n is arraysize now

		int[] stack = new int[100];
		do {
			Scanner scanner = new Scanner(System.in);// new scanner

			System.out.println("1: push");
			System.out.println("2: pop");
			System.out.println("3: show");
			System.out.println("4: exit");

			input = scanner.nextInt();

			if (input == 1) {// push
				System.out.print("push ");
				size = scanner.nextInt();
				int j = size;
				for (int i = n; i < n + j; i++)
					stack[i] = scanner.nextInt();// input number
				n = n + j;// arraysize now
			} else if (input == 2) {
				System.out.print("pop ");
				pop = scanner.nextInt();
				int k = n - pop;
				if (k < 0)// 為了讓K不小於0
					k = 0;
				for (int i = n - 1; i >= k; i--) {// 清空pop的數
					System.out.print(stack[i] + " ");
					stack[i] = '\0';
				}
				System.out.println();
				if (pop >= n) {// 如果pop大於n,n=0;
					System.out.println("Statck is empty!");
					n = 0;
				} else
					n = n - pop;// arraysize now
			} else if (input == 3) {
				if (n == 0)
					System.out.println("Statck is empty");
				for (int i = 0; i < n; i++)
					// show array
					System.out.print(stack[i] + " ");
				System.out.println();
			} else if (input == 4) {
				System.out.println("Good Bye");
				System.exit(0);
			}

		} while (n > -1);

	}
}
