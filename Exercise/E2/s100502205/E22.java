package ce1002.E2.s100502205;

import java.util.Scanner;

public class E22 {
	public static void main(String[] args) {
		Queue<Integer> q = new Queue<Integer>();
		Scanner cin = new Scanner(System.in);
		while (true) {
			System.out.println("1: push");
			System.out.println("2: pop");
			System.out.println("3: show");
			System.out.println("4: exit");
			int cmd = cin.nextInt(), argv;
			switch (cmd) {
			case 1:
				System.out.print("push ");
				argv = cin.nextInt();
				for (int i = 0; i < argv; i++) {
					int element = cin.nextInt();
					q.push(Integer.valueOf(element));
				}
				break;
			case 2:
				System.out.print("pop ");
				argv = cin.nextInt();
				while (!q.empty() && argv-- != 0) {
					System.out.print(q.front() + " ");
					q.pop();
				}
				System.out.println();
				if (q.empty())
					System.out.println("Queue is empty!");
				break;
			case 3:
				if (q.empty())
					System.out.println("Queue is empty!");
				for (int i = 0, j = q.head; i < q.size(); i++, j = (j + 1) % 100)
					System.out.print(q.buf[j] + " ");
				System.out.println();
				break;
			}
			if (cmd == 4)
				break;
			System.out.println();
		}
		System.out.println("Good Bye");
	}
}

class Queue<T> {
	public Object[] buf = new Object[100];
	public int SIZE = 0, head = 0, rear = 0;

	public T front() {
		return (T) buf[head];
	}

	public void push(T x) {
		buf[rear++] = x;
		SIZE++;
		if (rear == 100)
			rear = 0;
	}

	public void pop() {
		head++;
		SIZE--;
		if (head == 100)
			head = 0;
	}

	public boolean empty() {
		return SIZE == 0;
	}

	public int size() {
		return SIZE;
	}
}
