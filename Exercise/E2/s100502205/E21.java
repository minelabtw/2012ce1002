package ce1002.E2.s100502205;

import java.util.Scanner;

public class E21 {
	public static void main(String[] args) {
		Stack<Integer> stk = new Stack<Integer>();
		Scanner cin = new Scanner(System.in);
		while (true) {
			System.out.println("1: push");
			System.out.println("2: pop");
			System.out.println("3: show");
			System.out.println("4: exit");
			int cmd = cin.nextInt(), argv;
			switch (cmd) {
			case 1:
				System.out.print("push ");
				argv = cin.nextInt();
				for (int i = 0; i < argv; i++) {
					int element = cin.nextInt();
					stk.push(Integer.valueOf(element));
				}
				break;
			case 2:
				System.out.print("pop ");
				argv = cin.nextInt();
				while (!stk.empty() && argv-- != 0) {
					System.out.print(stk.top() + " ");
					stk.pop();
				}
				System.out.println();
				if (stk.empty())
					System.out.println("Stack is empty!");
				break;
			case 3:
				if (stk.empty())
					System.out.println("Stack is empty!");
				for (int i = 0; i < stk.size(); i++)
					System.out.print(stk.buf[i] + " ");
				System.out.println();
				break;
			}
			if (cmd == 4)
				break;
			System.out.println();
		}
		System.out.println("Good Bye");
	}
}

class Stack<T> {
	public Object[] buf = new Object[100];
	private int SIZE = 0;

	public T top() {
		return (T) buf[SIZE - 1];
	}

	public void push(T x) {
		buf[SIZE++] = x;
	}

	public void pop() {
		SIZE--;
	}

	public boolean empty() {
		return SIZE == 0;
	}

	public int size() {
		return SIZE;
	}
}
