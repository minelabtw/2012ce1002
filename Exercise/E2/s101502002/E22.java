package E2.s101502002;

import java.util.Scanner;

public class E22 {
	public static void main(String[]args){
		Scanner input=new Scanner(System.in);
		int a=0,b=0,in=0;  //a是最初有幾個數字，b是要pop出幾個數字，in是還剩幾個數字
		int queue[]=new int[100]; //創造一大小為100的 Queue
		while(true){ //重複輸入
			System.out.println("\n1: push\n2: pop\n3: show\n4: exit");
			int num=input.nextInt(); //輸入要執行哪一個選項
			if(num==1){
				System.out.print("push ");
				a=input.nextInt();
				for(int i=0;i<a;i++){ //輸入陣列的每一項
					queue[i]=input.nextInt();
				}
				in=a;
			} 
			else if(num==2){
				System.out.print("pop ");
				b=input.nextInt();
				if(b>in){ //若要pop的數大於內容物數量
					for(int i=a-in;i<a;i++){ //從頭輸出剩下的每一項
						System.out.print(queue[i]+" ");
					}
					System.out.println();
					in=0;
				}
				else{
					for(int i=a-in;i<a-in+b;i++){  //若沒有超過內容數量，輸出由前方開始的b項
						System.out.print(queue[i]+" ");
					}
					System.out.println();
					in=in-b;
				}				
			}
			else if(num==3){ //輸出內容物東西
				if(in==0){ //空了
					System.out.println("Queue is empty!");
				}
				else{
					for(int i=a-in;i<a;i++){
						System.out.print(queue[i]+" ");
					}
					System.out.println();
				}								
			}
			else if(num==4){
				System.out.println("Good bye");
				break;
			}			
		}			
	}
}
