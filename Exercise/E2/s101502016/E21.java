package ce1002.E2.s101502016;

import java.util.*;

public class E21 {
	public static void main(String[] args){
		
		Scanner input = new Scanner(System.in);
		int select;
		int push,pop,show;
		int level=0;//標記現在的起始點
		
		int[] array = new int[100];
		while(true){
			System.out.println("1: push\n2: pop\n3: show\n4: exit");
			select = input.nextInt();
			
			///////////////////////////////////
			if(select==1)
			{
				System.out.print("push ");
				push = input.nextInt();//record the length
				for(int i=0 ; i<push ; i++)
				{
					array[level+i] = input.nextInt();
				}
				level += push;
				System.out.print("\n");
			}
			///////////////////////////////////
			if(select==2)
			{
				System.out.print("pop ");
				pop = input.nextInt();
				
				for(int i=level-1; i>=level-pop ; i--)
				{
					System.out.print(array[i]+" ");
					if( i == 0)//偵錯
					{
						System.out.println(" ");
						System.out.println("Stack is empty!\n");
						break;
					}
				}
				System.out.println("\n");
				level -= pop;
				if(level<0)//偵錯
					level = 0;
				
			}
			///////////////////////////////////
			if(select==3)
			{
				for(int i=0 ; i<level ; i++)
				{
					System.out.print(array[i]+" ");
					if(i==level)
					{
						System.out.println(" ");
						System.out.println("Stack is empty!");
						break;
					}	
				}
				System.out.println("\n");
			}
			///////////////////////////////////
			if(select==4)
			{
				System.out.println("Good Bye");
				break;//exit
			}
				
			
		}
		
	}
}
