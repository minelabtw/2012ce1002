package ce1002.E2.s101502029;

import java.util.*;

public class E22 {
	public static void main(String[] args) {
		Scanner place = new Scanner(System.in);
		int[] Queue = new int[100];
		int Queuelength = 0;
		while (true) {
			System.out.println("1: push ");
			System.out.println("2: pop ");
			System.out.println("3: show ");
			System.out.println("4: exit");
			int chooseNum = 0;
			chooseNum = place.nextInt();
			if (chooseNum == 1) {//若輸入1，則選擇1.Push
				int pushNum;
				System.out.print("push ");
				pushNum = place.nextInt();
				for (int i = 0; i < pushNum; i++) {//加長數字
					Queue[Queuelength] = place.nextInt();
					Queuelength++;//加長總數
				}
				System.out.println();
			} else if (chooseNum == 2) {//若輸入2，則選擇2.pop
				System.out.print("pop ");
				int popNum;
				popNum = place.nextInt();
				if (popNum > Queuelength) {//若輸入數字大於長數，則列出剩下所有數，
					popNum = Queuelength;
					for (int i = 0; i < popNum; i++) {
						System.out.print(Queue[i] + " ");
					}
					for (int j = 0; j < Queuelength; j++) {
						Queue[j] = Queue[j + popNum];
					}
					Queuelength -= popNum;
					System.out.println();
					System.out.print("Queue is empty!");
				} else {
					for (int i = 0; i < popNum; i++) {
						System.out.print(Queue[i] + " ");
					}
					for (int j = 0; j < Queuelength; j++) {
						Queue[j] = Queue[j + popNum];
					}
					Queuelength -= popNum;
				}
				System.out.println("\n");
			} else if (chooseNum == 3) {//若輸入3,則選擇3.show
				for (int i = 0; i < Queuelength; i++)
					System.out.print(Queue[i] + " ");
				if (Queuelength == 0) {
					System.out.print("Queue is empty!");
				}
				System.out.println("\n");
			} else if (chooseNum == 4) {//若輸入4，則選擇4.exit
				System.out.println("Good Bye");
				break;
			}
		}
	}
}
