package ce1002.E2.s101502029;

import java.util.*;

public class E21 {
	public static void main(String[] args) {
		Scanner place = new Scanner(System.in);
		int[] Stack = new int[100];
		int stracklength = 0;
		while (true) {
			System.out.println("1: push ");
			System.out.println("2: pop ");
			System.out.println("3: show ");
			System.out.println("4: exit");
			int chooseNum = 0;
			chooseNum = place.nextInt();
			if (chooseNum == 1) { //若輸入1，則選擇1.Push
				int pushNum;
				System.out.print("push ");
				pushNum = place.nextInt();
				for (int i = 0; i < pushNum; i++) {//疊加數字
					Stack[stracklength] = place.nextInt();
					stracklength++;//疊加總數
				}
				System.out.println();
			} else if (chooseNum == 2) { //若輸入2，則選擇2.pop
				System.out.print("pop ");
				int popNum;
				popNum = place.nextInt();
				if (popNum > stracklength) {//若輸入數字大於疊加數，則列出剩下所有數，
					popNum = stracklength;  //並顯示empty
					for (int i = 0; i < popNum; i++) {
						stracklength--;
						System.out.print(Stack[stracklength] + " ");
					}
					System.out.println();
					System.out.print("Stack is empty!");
				} else {
					for (int i = 0; i < popNum; i++) {
						stracklength--;
						System.out.print(Stack[stracklength] + " ");
					}
					System.out.println("\n");
				}
			} else if (chooseNum == 3) {//若輸入3,則選擇3.show
				for (int i = 0; i < stracklength; i++)
					System.out.print(Stack[i] + " ");
				if (stracklength == 0)
					System.out.print("Stack is empty!");
				System.out.println("\n");
			} else if (chooseNum == 4) {//若輸入4，則選擇4.exit
				System.out.println("Good Bye");
				break;
			}
		}
	}
}
