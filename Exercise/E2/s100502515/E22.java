package ce1002.E2.s100502515;

import java.util.Scanner;

public class E22 {
	int[] a = new int[100];
	int lastIndex = -1;

	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);
		E22 e22 = new E22();
		System.out.print("1: push\n2: pop\n3: show\n4: exit\n");
		boolean go = true;
		while (go) {
			System.out.print("Choose: ");
			int choice = scanner.nextInt();
			switch (choice) {
			case 1:
				System.out.print("push ");
				int num = scanner.nextInt();
				e22.push(num);
				break;
			case 2:
				System.out.print("pop ");
				int num2 = scanner.nextInt();
				e22.pop(num2);
				break;
			case 3:
				e22.show();
				break;

			default:
				System.out.println("Good Bye!!");
				go = false;
				break;
			}

		}
		scanner.close();

	}

	private void push(int c) {
		Scanner scanner = new Scanner(System.in);
		if ((lastIndex + c) > 99) {
			System.out.println("The Stack doesn't have enough space!!");
		} else {
			for (int i = 1; i <= c; i++) {
				a[lastIndex + i] = scanner.nextInt();
			}
			lastIndex += c;
		}
	}

	private void pop(int c) {
		Scanner scanner = new Scanner(System.in);
		if ((lastIndex - c) < -1) {
			System.out.println("The Stack doesn't have enough elements!!");
		} else {
			for (int i = 0; i < c; i++) {
				System.out.print(a[i] + " ");//Pop the numbers from the top.
			}
			System.out.println();
			for (int i = c; i <= lastIndex; i++) {
				a[i-c] = a[i];//Move the elements the star from index zero to keep the size 100.
			}
			lastIndex -= c;
		}
	}

	private void show() {
		if (lastIndex == -1) {
			System.out.println("The stack is empty!!");
		} else {
			for (int i = 0; i <= lastIndex; i++) {
				System.out.print(a[i] + " ");
			}
			System.out.println();
		}

	}
}
