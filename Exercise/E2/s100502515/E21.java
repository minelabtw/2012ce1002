package ce1002.E2.s100502515;

import java.util.Scanner;

public class E21 {
	int[] a = new int[100];
	int lastIndex = -1;

	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);
		E21 e21 = new E21();
		System.out.print("1: push\n2: pop\n3: show\n4: exit\n");
		boolean go = true;
		while (go) {
			System.out.print("Choose: ");
			int choice = scanner.nextInt();
			switch (choice) {
			case 1:
				System.out.print("push ");
				int num = scanner.nextInt();
				e21.push(num);
				break;
			case 2:
				System.out.print("pop ");
				int num2 = scanner.nextInt();
				e21.pop(num2);
				break;
			case 3:
				e21.show();
				break;

			default:
				System.out.println("Good Bye!!");
				go = false;
				break;
			}

		}
		scanner.close();

	}

	private void push(int c) {
		Scanner scanner = new Scanner(System.in);
		if ((lastIndex + c) > 99) {
			System.out.println("The Stack doesn't have enough space!!");
		} else {
			for (int i = 1; i <= c; i++) {
				a[lastIndex + i] = scanner.nextInt();
			}
			lastIndex += c;
		}
	}

	private void pop(int c) {
		Scanner scanner = new Scanner(System.in);
		if ((lastIndex - c) < -1) {
			System.out.println("The Stack doesn't have enough elements!!");
		} else {
			for (int i = lastIndex; i > (lastIndex-c); i--) {
				System.out.print(a[i] +" ");
			}
			System.out.println();
			lastIndex -= c;
		}
	}

	private void show() {
		if (lastIndex == -1) {
			System.out.println("The stack is empty!!");
		} else {
			for (int i = 0; i <= lastIndex; i++) {
				System.out.print(a[i] + " ");
			}
			System.out.println();
		}
		
	}
}
