package ce1002.E2.s101502009;

import java.util.Scanner;

public class E21 {
	public static void main(String[] arts){
		Scanner input=new Scanner(System.in);
		int t=0;
		int lenth=0;
		int[] array= new int[100];
		while(true){
			System.out.println("1: push");
			System.out.println("2: pop");
			System.out.println("3: show");
			System.out.println("4: exit");
			int cin=input.nextInt();
			
			if(cin==1){
				System.out.println("push ");
				t=input.nextInt();
				
				for(int i=lenth;i<lenth+t;i++){
					array[i]=input.nextInt();
				}
				lenth=lenth+t;
				System.out.println();
				System.out.println();
			}//end of 1
			
			else if(cin==2){
				System.out.println("pop ");
				int p=input.nextInt();
				
				if(p<=lenth){
					for(int i=lenth-1;i>lenth-1-p;i--){
						System.out.print(array[i]+" ");					
					}
				}//ordinary
				
				if(p>lenth){
					for(int i=lenth-1;i>=0;i--){
						System.out.print(array[i]+" ");					
					}
					System.out.println();
					System.out.println("Stack is empty!");
				}//over
				
				System.out.println();
				System.out.println();
				lenth=lenth-p;
				if(lenth<0){
					lenth=0;
				}
			}//end of 2
			
			else if(cin==3){
				if(lenth==0){
					System.out.println("Stack is empty!");
				}
				else{
					for(int i=0;i<lenth;i++){
						System.out.print(array[i]+" ");
					}
					
				}
				System.out.println();
				System.out.println();
				
				
			}//end of 3
			else if(cin==4){
				System.out.println("Good Bye");
				break;				
			}//end of 4
			
		}//end of while
		
		
	}//end of main

}
