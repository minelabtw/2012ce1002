package ce1002.E2.s976002019;

import java.util.Scanner;

public class E22 {
	public static void main(String arg[]){
		int[] stack = new int[100];
		int x,y,z;
		int head=-1,tail=-1;
		while(true){
			System.out.println("1:push ");
			System.out.println("2:pop ");
			System.out.println("3:show ");
			System.out.println("4:exit ");
			Scanner input1=new Scanner(System.in);
			x=input1.nextInt();
			switch (x) {
			
				case 1:
					System.out.print("push ");
					Scanner input2=new Scanner(System.in);
					y=input2.nextInt(); /*輸入push數量*/
					tail=push(stack,tail,y);
					System.out.println("");
					break;
				case 2:
					System.out.print("pop ");
					Scanner input3=new Scanner(System.in);
					y=input3.nextInt(); /*輸入pop數量*/
					head=pop(stack,head,tail,y);
					System.out.println("");
					break;
				case 3:
					//System.out.print("show ");
					show(stack,head, tail);
					System.out.println("");
					break;
				case 4:
					exit();
				}
		
		
			}
		}
		static int push(int array[],int a,int b){ /*push元素*/
			Scanner input=new Scanner(System.in);
			for(int i=0;i<b;i++){
				array[++a]=input.nextInt();
				
			}
			return a;

		}
		static int pop(int array[],int a,int b,int c){ /*pop元素*/
			if (b-a<c){ /*若pop元素較queue元素多，則全部輸出*/
				int tmp=a;
				for(int i=0;i<b-a;i++){
					System.out.print(array[++tmp]+" ");
					
				}
				a=b;
				System.out.println("");
				System.out.println("queue is empty!");
			}
			else{ /*輸出queue前端元素*/
				for(int i=0;i<c;i++){
					System.out.print(array[a+1]+" ");
					a++;
				}
			}
			return a;
		}
		static void show(int array[],int a, int b){ /*列出queue全部元素*/
			for(int i=a+1;i<=b;i++){
				System.out.print(array[i]+" ");
			}
		}
		static void exit(){ /*離開程式*/
			System.out.println("Good Bye");
			System.exit(1);
		}


}

