package ce1002.E2.s101502010;

import java.util.Scanner;

public class E21 {
	public static void main(String[] args){
		int[] stack=new int[100];
		int current_l=0;
		Scanner input=new Scanner(System.in);
		System.out.print("1: push\n2: pop\n3: show\n4: exit\n");
		while(true){
			int method=input.nextInt();
			if(method==1){//接續陣列
				System.out.print("push ");
				int num=input.nextInt();
				for(int i=current_l;i<current_l+num;i++){
					stack[i]=input.nextInt();
				}
				current_l+=num;
				System.out.print("\n");
			}
			else if(method==2){//從嘿面刪除陣列
				System.out.print("pop ");
				int num=input.nextInt();
				for(int i=1;i<=num&&current_l!=0;i++){
					System.out.print(stack[current_l-1]+" ");
					stack[current_l-1]=0;
					current_l--;
				}
				System.out.print("\n");
				if(current_l==0)//若以刪除完則輸出陣列已空
					System.out.println("Stack is empty!");
				System.out.print("\n");
			}
			else if(method==3){
				if(current_l==0)//若陣列為空白則輸出空白訊息
					System.out.print("Stack is empty!");
				else//輸出陣列
					for(int i=0;i<current_l;i++)
						System.out.print(stack[i]+" ");
				System.out.println("\n");
			}
			else if(method==4)
				break;
			else
				System.out.println("Please enter a method again!\n");
			System.out.print("1: push\n2: pop\n3: show\n4: exit\n");
		}
		System.out.println("Good Bye");
	}
}
