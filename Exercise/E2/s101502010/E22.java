package ce1002.E2.s101502010;

import java.util.Scanner;

public class E22 {
	public static void main(String[] args){
		int[] queue=new int[100];
		int current_l=0;
		Scanner input=new Scanner(System.in);
		System.out.print("1: push\n2: pop\n3: show\n4: exit\n");
		while(true){
			int method=input.nextInt();
			if(method==1){//輸入陣列
				System.out.print("push ");
				int num=input.nextInt();
				for(int i=current_l;i<current_l+num;i++){
					queue[i]=input.nextInt();
				}
				current_l+=num;
				System.out.print("\n");
			}
			else if(method==2){
				System.out.print("pop ");
				int num=input.nextInt();
				for(int i=0;i<num&&current_l!=0;i++){//從前排開始刪除陣列
					System.out.print(queue[i]+" ");
					queue[i]=0;
					current_l--;
				}
				for(int i=0;i<current_l+num;i++){//將陣列向前移動
					queue[i]=queue[i+num];
				}
				System.out.print("\n");
				if(current_l==0)
					System.out.println("Queue is empty!");//若陣列已空則輸出訊息
				System.out.print("\n");
			}
			else if(method==3){
				if(current_l==0)
					System.out.print("Queue is empty!");//輸出陣列已空之訊息
				else
					for(int i=0;i<current_l;i++)//輸出陣列
						System.out.print(queue[i]+" ");
				System.out.println("\n");
			}
			else if(method==4)
				break;
			else
				System.out.println("Please enter a method again!\n");
			System.out.print("1: push\n2: pop\n3: show\n4: exit\n");
		}
		System.out.println("Good Bye");
	}
}
