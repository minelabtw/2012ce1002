package ce1002.E2.s101502521;

import java.util.Scanner;

public class E21 {
	public static void main(String[] args)
	{
		Scanner input = new Scanner(System.in);
		int[] stack = new int[100];
		int p=0;
		while(true)
		{
			System.out.println("1: push");
			System.out.println("2: pop");
			System.out.println("3: show");
			System.out.println("4: exit");
			int choose = input.nextInt();
			if(choose==4)
			{
				System.out.println("Good Bye");
				break;
			}
			switch(choose) // show message
			{
			case 1:
				System.out.print("push ");
				break;
			case 2:
				System.out.print("pop ");
				break;
			}
			if(choose==3) // show stack
			{
				for(int i=0;i<p;i++)
					System.out.print(stack[i]+" ");
				System.out.println();
				if(p==0) // if is empty
					System.out.println("Stack is empty!");
				continue;
			}
			if(choose==1)
			{
				int arg = input.nextInt(); // the number of input
				for(int i=0;i<arg;i++)
					stack[i+p] = input.nextInt();
				p += arg;
			}
			if(choose==2)
			{
				int arg=input.nextInt(); // pop stack
				int i;
				for(i=1;i<=arg&&p-i>=0;i++)
					System.out.print(stack[p-i]+" ");
				System.out.println("");
				p -= (i-1); // move pointer to pop stack
				if((i-1)!=arg)
					System.out.println("Stack is empty!");
			}
		}
	}
}
