package ce1002.E2.s101502521;

import java.util.Scanner;

public class E22 {
	public static void main(String[] args)
	{
		Scanner input = new Scanner(System.in);
		int[] queue = new int[100];
		int pf=0; // pointer of front
		int pb=0; // pointer of back
		while(true)
		{
			System.out.println("1: push");
			System.out.println("2: pop");
			System.out.println("3: show");
			System.out.println("4: exit");
			int choose = input.nextInt();
			if(choose==4)
			{
				System.out.println("Good Bye");
				break;
			}
			switch(choose)
			{
			case 1:
				System.out.print("push ");
				break;
			case 2:
				System.out.print("pop ");
				break;
			}
			if(choose==3) // show queue
			{
				for(int i=pb;i<pf;i++)
					System.out.print(queue[i]+" ");
				System.out.println();
				if(pb==pf) // if is empty
					System.out.println("Queue is empty!");
				continue;
			}
			if(choose==1)
			{
				int arg = input.nextInt(); // get number of input
				for(int i=0;i<arg;i++)
					queue[i+pf] = input.nextInt();
				pf += arg;
			}
			if(choose==2)
			{
				int arg=input.nextInt();
				int i;
				for(i=0;i<arg&&pf>(pb+i);i++)
					System.out.print(queue[pb+i]+" "); // pop from front
				System.out.println("");
				pb += (i);
				if(pf==pb)
					System.out.println("Queue is empty!");
			}
		}
	}
}
