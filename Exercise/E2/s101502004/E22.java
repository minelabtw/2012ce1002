package ce1002.E2.s101502004;
import java.util.Scanner;

public class E22 {
	public static void main(String[] args){
		System.out.println("1: push\n2: pop\n3: show\n4: exit");
		Scanner inp = new Scanner(System.in);
		int choose=inp.nextInt();//輸入動作
		int[] stack = new int[100];//設定陣列
		int n=0;//陣列元素數量
		while(choose!=4){//迴圈停止於選擇為4
			if(choose==1){
				System.out.println("push");
				int howmanypush = inp.nextInt();//push多少數字
				for(int i=0;i<howmanypush;i++){//將數字存進陣列
					int number = inp.nextInt();
					stack[i]=number;
					n++;
				}
			}
			if(choose==2){
				System.out.println("pop");
				int howmanypop = inp.nextInt();//pop多少數字
				if(n-howmanypop<=0){//若pop過多
					for(int i=0;i<n;i++)//輸出pop哪些數字
						System.out.print(stack[i]+" ");
					n=0;//陣列元素變為0
					System.out.println("\nQueue is empty!");
				}
				else{
					for(int i=0;i<howmanypop;i++)//輸出pop哪些數字
						System.out.print(stack[i]+" ");
					System.out.println(" ");
					for(int i=howmanypop;i<n;i++)//更新陣列
						stack[i-howmanypop]=stack[i];
					n-=howmanypop;//陣列元素剩餘數量
				}
				
			}
			if(choose==3){
				if(n==0)//若陣列元素量為0
					System.out.println("Queue is empty!");
				else{
					for(int i=0;i<n;i++)//輸出陣列
						System.out.print(stack[i]+" ");
					System.out.println(" ");
				}
			}
			if(choose==4)
				break;
			System.out.println("\n1: push\n2: pop\n3: show\n4: exit");//重複輸出
			choose=inp.nextInt();
		}
		if(choose==4)
			System.out.println("Good Bye");
	}
}