//E2-2 by J
package ce1002.E2.s101502020;

import java.util.Scanner;

public class E22 {

	public static void main(String[] args) {
		
		Scanner input=new Scanner(System.in);
		
		int ist=0 ;		//ist means instruction; the way user choose
		int DataQuantity ;
		long[] ary1 = new long[100] ;
		long[] ary2 = new long[100] ;
		
		while ( ist!=4 ){
			
			System.out.print("1: push\n2: pop\n3: show\n4: exit\n");
			
			ist=input.nextInt();
			
			if (ist==1){
				System.out.print("push " );
				DataQuantity=input.nextInt();
				for (int i=0 ; i<(DataQuantity+ary1.length) ; i++ )
					ary1[i+ary1.length]=input.nextLong() ;
			}
			else if (ist==2){
				System.out.print("pop ");
				DataQuantity=input.nextInt();
				if(DataQuantity<=ary1.length) {
					for (int i=0 ; i<DataQuantity ; i++) {
						ary1[i]=ary1[DataQuantity+i] ;
						ary1[DataQuantity+i] = 0 ;
					}
				}
				else {
					for (int i=0 ; i<ary1.length ; i++)
						ary1[i]=0 ;
					
					System.out.println("Queue is empty!") ;
				}
			}
			else if (ist==3){
				if (ary1[0]==0)
					System.out.println("Queue is empty!");
				else
					for (int i=0 ; i<ary1.length ; i++ )
						System.out.print(ary1[i] + " ");
				
			}		//print information, completed!
			else if (ist==4){
				System.out.print("Good Bye") ;
			}		//ending condition
			else {
				System.out.print("U just enter a wrong input!");
			}		//wrong
		}

	}

}
