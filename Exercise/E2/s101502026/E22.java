package ce1002.E2.s101502026;

import java.util.Scanner;

public class E22
{
	public static void main(String[] args)
	{
		int[] array = new int[100];
		int input,quantity,array_number=0,pop_number;

		while(true)
		{
			boolean check=true;
			Scanner scanner = new Scanner(System.in);
			System.out.println("1: push\n2: pop\n3: show\n4: exit");
			input = scanner.nextInt();
			if(input==1)
			{
				System.out.print("push ");
				quantity=scanner.nextInt();
				for(int i=0;i<quantity;i++)
				{
					array[array_number+i]=scanner.nextInt();
				}
				array_number=array_number+quantity;
				System.out.println();
			}
			else if(input==2)
			{
				System.out.print("pop ");
				pop_number=scanner.nextInt();
				for(int i=0;i<pop_number;i++)
				{
					if(array_number==0&&array[array_number]==0)
					{
						System.out.print("Queue is empty!\n\n");
						check=false;
						break;
					}
					if(array[array_number-1]==0)
					{
						System.out.print("\nQueue is empty!\n\n");
						array_number=0;
						check=false;
						break;
					}
					System.out.print(array[i]+" ");
					array[i]=0;
				}
				if(check==false)
					continue;
				else
				{
					for(int i=0;i<array_number-pop_number;i++)
					{
						array[i]=array[i+pop_number];
					}
					array_number=array_number-pop_number;
					System.out.println("\n");
				}
			}
			else if(input==3)
			{
				if(array[0]==0)
				{
					System.out.println("Queue is empty!\n");
					continue;
				}	
				else
				{
					for(int i=0;i<array_number;i++)
					{
						System.out.print(array[i]+" ");
					}
					System.out.println("\n");;
				}
			}
			else
			{
				System.out.println("Good Bye");
				break;
			}
		}
	}
}
