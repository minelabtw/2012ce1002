package ce1002.E2.s101502508;

import java.util.Scanner ;

public class E21 {
	public static void main (String[] args){
		
		Scanner input = new Scanner(System.in) ;
		
		int choose, num = 0, num1 = 0 ;//choose-->做選擇   num-->告訴電腦要存入多少數字  num1-->取出多少數字
		                                        
		int[] stack = new int [100] ;
		
		while (true){
			
			System.out.println("1: push \n2: pop \n3: show \n4: exit") ;
			choose = input.nextInt() ;
			
			if (choose==1){
				
				System.out.print("push ") ;
				num = input.nextInt() ;
				stack = new int [num] ;
				
				for( int k=0 ; k<num ; k++ ){
					stack[k] = input.nextInt() ;
					System.out.println();
				}
			}
			
			else if (choose==2){
				
				System.out.print("pop ") ;
				num1 = input.nextInt();
				
				if( num>0 ){
					for( int k=num-1 ; k>=num-num1 ; k-- ){//利用輸入的數字和原本的個數相減做輸出
						System.out.print(stack[k]+" ") ;
						
						if( k==0 )
							break ;
					}
				}
				
				if( num-num1<0 ){
					System.out.println() ;
					System.out.print("Stack is empty!") ;
				}
				
				num = num-num1 ;
				System.out.println() ;
			}
			
			else if (choose==3){
				
				for( int k=0 ; k<num ; k++ ){
					System.out.print(stack[k]+" ") ;
				}
				
				System.out.println() ;
			}
			
			else if (choose==4){
				System.out.print("Good Bye") ;
				break ;
			}
		}
	}

}
