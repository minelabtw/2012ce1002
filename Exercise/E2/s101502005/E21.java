package E2.s101502005;

import java.util.Scanner;

public class E21 {
	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);

		int sum = 0;// 計算已經壓了幾個變數進去
		int a = 0, b = 0;
		int stack[];
		stack = new int[100];
		boolean over = true;

		while (over==true) {
			System.out.println("1: push");
			System.out.println("2: pop");
			System.out.println("3: show");
			System.out.println("4: exit");

			int count = input.nextInt();
			if (count == 1) {
				System.out.print("push ");
				a = input.nextInt();
				for (int i = 0; i < a; i++) {
					stack[i] = input.nextInt();
					sum++;
				}

			}

			else if (count == 2) {
				System.out.print("pop ");
				b = input.nextInt();
				for (int i = 0; i < b; i++) {
					System.out.print(stack[sum - 1] + " ");
					stack[sum - 1] = 0;
					sum--;
					if (sum == 0) {
						System.out.println("");
						System.out.print("Stack is empty!");
						break;
					}
				}
				System.out.println("");
			}

			else if (count == 3) {
				for (int i = 0; i < sum; i++) {
					System.out.print(stack[i] + " ");
				}

				if (sum == 0)
					System.out.println("Stack is empty!");
				System.out.println("");
			}

			else if(count == 4){
				System.out.print("Good Bye");
				over = false;
			}
		}
	}
}
