package CE1002.s101502518;

import java.util.Scanner;

public class E21 {
	public static void main(String[] args)
	{
		int[] stack;
		int pushlength = 0, poplength = 0,x = 0;
		stack = new int[101];
	
		while(true)
		{
			System.out.println("1: push");
			System.out.println("2: pop");
			System.out.println("3: show");
			System.out.println("4: exit");
			Scanner input = new Scanner(System.in);
			int n = input.nextInt();
		
			if(n==1)
			{
				System.out.print("push ");
				pushlength = input.nextInt();
				for(int i=x;i<x+pushlength;i++)
					stack[i] = input.nextInt();
				System.out.println();
				x=x+pushlength;
			}
			
			if(n==2)
			{
				System.out.print("pop ");	
				poplength = input.nextInt();
				
				if(poplength>x)//排出超過輸入長度
				{
					for(int i=x-1;i>=0;i--)
						System.out.print(stack[i]+" ");
					System.out.println();
					System.out.println("Stack is empty!");
					System.out.println();
					x=0;
				}
				else 
				{
					for(int i=x-1;i>x-poplength-1;i--)
						System.out.print(stack[i]+" ");
					System.out.println();
					x=x-poplength;
				}	
			}
			
			if(n==3)
			{
				if(x==0)
				{
					System.out.println("Stack is empty!");
					System.out.println();
				}
				else
					for(int i=0;i<x;i++)
						System.out.print(stack[i]+" ");
					System.out.println();
			}
			
			if(n==4)
			{
				System.out.println("Good Bye");
				break;
			}
		}
	}
}

