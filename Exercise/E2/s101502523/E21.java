package ce1002.E2.s101502523;
import java.util.*;
public class E21 {
	public static void main(String[] args){
		Scanner input = new Scanner(System.in);
		int[] stack = new int[100];
		int place=0; //the last position of array
		while(true){
			int selection, size;
			System.out.println("1: push\n"+"2: pop\n"+"3: show\n"+"4: exit");
			selection = input.nextInt();
			//choose 4, good bye~
			if(selection==4){
				System.out.print("Good Bye");
				break;
			}
			//for case 1 to 3
			switch(selection){
				case 1:
					System.out.print("push ");
					size = input.nextInt();  //determine the size of the input
					for(int i=1 ; i<=size ; i++){
						//input the value
						stack[place]=input.nextInt();
						place++;
					}
					break;
				case 2:
					System.out.print("pop ");
					size = input.nextInt();  //determine the size of the input
					for(int k=1 ; k<=size ; k++){
					//output the value from the end of array to the first position in "size" times
					//and move the position forward in "size" times
						System.out.print(stack[place-1]+" ");
						place--;
						if(place<=0){
							//the array is empty when the position is 0(first position)
							System.out.println("\nStack is empty!");
							place = 0;
							break;
						}
					}
					break;
				case 3:
					if(place<=0){
						//the array is empty when the position is 0(first position)
						System.out.println("Stack is empty!");
						break;
					}
					for(int j=0 ; j<place ; j++){
						//show the value from the first of array to the end of the array
						System.out.print(stack[j]+" ");						
					}
					System.out.println("");	
					break;
			}
			System.out.println("");
		}
	}
}




