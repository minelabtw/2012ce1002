package ce1002.E2.s101502523;
import java.util.*;
public class E22 {
	public static void main(String[] args){
		Scanner input = new Scanner(System.in);
		int[] queue = new int[100];
		int[] array = new int[100];
					 //						  queue[0]		last one
		int place=0; //the last position of array   |---------|  
					 //                             ��         ��
					 //							POPplace	place
		while(true){ 
			int selection, size;
			int POPplace=0; //the position of the place where pop to 
			System.out.println("1: push\n"+"2: pop\n"+"3: show\n"+"4: exit");
			selection = input.nextInt();
			//choose 4, good bye~
			if(selection==4){
				System.out.print("Good Bye");
				break;
			}
			//for case 1 to 3
			switch(selection){
				case 1:
					System.out.print("push ");
					size = input.nextInt();  //determine the size of the input
					for(int i=1 ; i<=size ; i++){
						//input the value
						queue[place]=input.nextInt();
						place++;
					}
					break;
				case 2:
					System.out.print("pop ");
					size = input.nextInt();  //determine the size of the input
					for(int k=0 ; k<size ; k++){
						System.out.print(queue[k]+" ");
						POPplace++;//move the forward position
						if(POPplace>=place){
							//the array is empty when the forward position and the last position is the same 
							System.out.print("\nQueue is empty!");
							break;
						}
					}
					for(int p=0 ; p<=place-POPplace ; p++){
						//let the value in queue to the array[0] to the last
						array[p] = queue[POPplace+p];
					}
					for(int l=0 ; l<place-POPplace; l++){
						//copy value from the first position
						queue[l] = array[l];
					}
					place = place-POPplace; //make a new last position 
					System.out.println("");
					break;
				case 3:
					if(place<=0){
						//the array is empty when the position is 0(first position)
						System.out.println("Queue is empty!");
						break;
					}
					for(int j=0 ; j<place ; j++){
						//show the value from the first of array to the end of the array
						System.out.print(queue[j]+" ");						
					}
					System.out.println("");	
					break;
			}
			System.out.println("");
		}
	}
}




