package ce1002.E2.s101502527;

import java.util.Scanner;

public class E22 {
	public static void main(String[] args){
		
		Scanner scan = new Scanner(System.in);
		int[] queue = new int[100];//初始化
		boolean exit=false;
		
		while(true){
			System.out.println("1: push");
			System.out.println("2: pop");
			System.out.println("3: show");
			System.out.println("4: exit");
			int in = scan.nextInt();
			switch (in){//呼叫個函式
			case 1:
				push(queue);
				System.out.println("");
				break;
			case 2:
				pop(queue);
				System.out.println("");
				break;
			case 3:
				show(queue);
				System.out.println("");
				break;
			case 4:
				System.out.println("Good Bye");
				exit=true;
				break;
			}
			if(exit)
				break;
		}
		
	}

	
	public static void push(int[] queue){
		Scanner scan = new Scanner(System.in);
		System.out.print("push ");
		int x= scan.nextInt();
		if(num==100){//佇列已滿則退出
			System.out.println("Queue is full");
			return ;
		}
		for(int i=0;i<x;i++,num++){
			if(num==100){//柱列中途滿
				System.out.println("\nQueue is full!");
				return ;
			}
			queue[num]=scan.nextInt();//pushing
			
		}
		return;
	}
	
	
	public static void pop(int[] queue){
		Scanner scan = new Scanner(System.in);
		System.out.print("pop ");
		int x=scan.nextInt();
		if(x>=num){//佇列全空 直接歸零
			for(int i=0;i<num;i++){
				System.out.print(queue[i]+" ");
			}
			System.out.println("\nQueue is empty!");
			num=0;
		}
		else{//佇列還有剩
			for(int i=0;i<x;i++){
				System.out.print(queue[i]+" ");//輸出刪除的
			}
			for(int i=0;i<num-x;i++){//更新佇列
				queue[i]=queue[i+x];
			}
			num-=x;
		}
		System.out.println("");
	}
	
	public static void show(int[] queue){
		
		if(num==0)
			System.out.print("Queue is empty!");//如果佇列空
		for(int i=0;i<num;i++){//輸出全佇列
			System.out.print(queue[i]+" ");
		}
		System.out.println("");
		return;
	}
	
	
	public static int num=0;//記錄目前佇列位置
}
