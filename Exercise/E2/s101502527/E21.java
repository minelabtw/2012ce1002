package ce1002.E2.s101502527;

import java.util.Scanner;

public class E21 {
	public static void main(String[] args){
		Scanner scan = new Scanner(System.in);//初始化
		int[] stack = new int[100];
		boolean exit=false;
		
		while(true){
			System.out.println("1: push");
			System.out.println("2: pop");
			System.out.println("3: show");
			System.out.println("4: exit");
			int in = scan.nextInt();
			switch (in){//呼叫每個函數
			case 1:
				push(stack);
				System.out.println("");
				break;
			case 2:
				pop(stack);
				System.out.println("");
				break;
			case 3:
				show(stack);
				System.out.println("");
				break;
			case 4:
				System.out.println("Good Bye");
				exit=true;
				break;
			}
			if(exit)
				break;
		}
		
		
		
	}

	
	public static void push(int[] stack){
		Scanner scan = new Scanner(System.in);
		System.out.print("push ");
		int x= scan.nextInt();
		if(num==100){//超過就退出
			System.out.println("Stack is full");
			return ;
		}
		for(int i=0;i<x;i++,num++){
			if(num==100){//堆疊中途滿則中途跳出
				System.out.println("\nStack is full!");
				return ;
			}
			stack[num]=scan.nextInt();//輸入
			
		}
		return;
	}
	
	
	public static void pop(int[] stack){
		Scanner scan = new Scanner(System.in);
		System.out.print("pop ");
		int x= scan.nextInt();
		for(int i=x;i>0;i--,num--){
			if(num==0){//pop中途空
				System.out.println("\nStack is empty!");
				return ;
			}
			System.out.print(stack[num-1]+" ");//輸出pop中的數
		}
		System.out.println("");
		return;
	}
	
	public static void show(int[] stack){
		if(num==0)
			System.out.print("Stack is empty!");
		for(int i=0;i<num;i++){//輸出目前堆疊
			System.out.print(stack[i]+" ");
		}
		System.out.println("");
		return;
	}
	
	public static int num=0;//紀錄目前堆疊位置
	
}
