package ce1002.E2.s101502012;

import java.util.Scanner;

public class E11 {
	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);
		int respond=0;
		int length=0,push,pop,min;  //length是array長度 push,pop分開方便整理 min是從提出提到的最後一個
		int[] array = new int[100];
		while(respond!=4)
		{
			System.out.println("1: push\n2: pop\n3: show\n4: exit");
			respond = input.nextInt();
			if(respond==1)
			{
				System.out.print("push ");
				push = input.nextInt();
				for (int i=length;i<(length+push);i++) //填入push個數字
				{
					array[i] = input.nextInt();
				}
				System.out.print("\n");
				length+=push;			
			}
			else if (respond==2)
			{
				System.out.print("pop ");
				pop = input.nextInt();
				min = length-pop;
				for (int j=(length-1);j>=min;j--) //顯示出被刪掉的數字 變成0是因為做程式時 方便想
				{
					if(j<0)
						break;
					System.out.print(array[j] +" ");
					array[j]=0;
				}
				if(min<=0)	//當數列空的時候顯示
					System.out.print("\nStack is empty!");
				
				length-=pop;
				
				if(length<0)
					length=0;
				System.out.print("\n\n");

			}
			else if (respond==3)
			{
				for(int l=0;l<length;l++) //顯示全部的數列
				{
					System.out.print(array[l] +" ");
				}
				if(length==0)	//當數列空的時候顯示
					System.out.println("Stack is empty!");
				System.out.print("\n\n");
			}
			else
			{
				System.out.println("Good Bye");
				break;
			}
		}
	}
}
