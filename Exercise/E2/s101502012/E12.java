package ce1002.E2.s101502012;

import java.util.Scanner;

public class E12 {
	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);
		int respond=0;
		int length=0,push,pop;
		int[] array = new int[100];
		while(respond!=4)
		{
			System.out.println("1: push\n2: pop\n3: show\n4: exit");
			respond = input.nextInt();
			if(respond==1)
			{
				System.out.print("push ");
				push = input.nextInt();
				for (int i=length;i<(length+push);i++)
				{
					array[i] = input.nextInt();
				}
				System.out.print("\n");
				length+=push;			
			}
			else if (respond==2)
			{
				System.out.print("pop ");
				pop = input.nextInt();
				length-=pop;
				if(length<=0)
					pop+=length; //以免顯示後面的空數列
				for (int j=0;j<pop;j++)
				{
					System.out.print(array[j] +" ");
					array[j]=0;
				}
				if (length<0) //修正長度小於0的問題
					length=0;
				for (int k=0;k<length;k++) //把整個數列向左移動
				{
					array[k]=array[k+pop];
				}
				if(length==0)
					System.out.print("\nStack is empty!");
				System.out.print("\n\n");
			}
			else if (respond==3)
			{
				for(int l=0;l<length;l++)
				{
					System.out.print(array[l] +" ");
				}
				if(length==0)
					System.out.println("Stack is empty!");
				System.out.print("\n\n");
			}
			else
			{
				System.out.println("Good Bye");
				break;
			}
		}
	}
}
