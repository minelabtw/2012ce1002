package ce1002.E2.s101502516;
import java.util.Scanner;
import java.util.Stack;
public class E22 {
	static int [] d = new int [100];
	static int i = 0;
	static int m = 0;
	public static void  push( int y ){
		d[i] = y;
		i++;
		m = i;
	}
	
	public static void  main(String[] args){
		while( true )
		{
			System.out.println( "1: push" );
			System.out.println( "2: pop" );
			System.out.println( "3: show" );
			System.out.println( "4: exit" );
			
			Scanner input = new Scanner(System.in);
			int a,b,c;
			a = input.nextInt();

			if ( a == 1 )
			{
				System.out.print( "push " );
				b = input.nextInt();
				for ( int x = 0; x < b; x++ )
				{
					c = input.nextInt();
					push( c );
				}
			}
			else if ( a == 2 )
			{
				int j = 0;
				int k = 0;
				System.out.print( "pop " );
				b = input.nextInt();
				for ( int x = 0; x < 100; x++ )
				{
					if ( d[x] != 0 )
					{
						j++;
					}
				}
				k = j;
				if ( k >= b )
				{
					m = i + b;
					for ( int x = 0; x < b; x++ )
					{
						System.out.print( d[x] + " " );
					}
					System.out.println( " " );
					for ( int x = 0 ; x < b; x++ )
					{
						d[x] = 0;
					}
				}	
				else
				{
					m = i + b;
					for ( int x = 0; x < k; x++ )
					{
						System.out.print( d[x] + " " );
					}
					System.out.println( " " );
					System.out.println( "Queue is empty!" );
					for ( int x = 0 ; x < k; x++ )
					{
						d[x] = 0;
					}
				}
			}
			else if ( a == 3 )
			{
				int j = 0;
				for ( int x = 0; x < i + 1; x++ )
				{
					if ( d[x] != 0)
						System.out.print( d[x] + " " );
				}
				System.out.println( " " );
			}
			else if ( a == 4 )
			{
				System.out.println( "Good Bye" );
				break;
			}
		}
	}
}
