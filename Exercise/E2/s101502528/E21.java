package ce1002.E2.s101502528;

import java.util.Scanner;

public class E21 {
	public static void main(String[] args)
	{
		int a=100,push=0,pop=0,check=0;
		int[] stack=new int[a];
		int choose;
		
		while(true)
		{	
			Scanner input=new Scanner(System.in);
			System.out.println("1:push ");
			System.out.println("2:pop ");
			System.out.println("3:show ");
			System.out.println("4:exit ");
			
			choose=input.nextInt();
			
			if (choose==1)
			{
				System.out.print("push ");
				push=input.nextInt();
				for (int i=0 ; i<push ; i++)
				{
					stack[i]=input.nextInt();
				}
				System.out.println();
			}
			
			else if (choose==2)
			{
				System.out.print("pop ");
				pop=input.nextInt();
				
				if (push-pop<0)//pop超過原有的數
				{
					pop=push;
					check=1;
				}
				
				for(int i=push-1 ; i>=push-pop ; i--)
				{
					System.out.print(stack[i]+" ");
				}
				
				push=push-pop;
				
				if (check==1)//pop超過原有的數時先輸出
				{
					System.out.println();
					System.out.print("Stack is empty!");
				}
				
				System.out.println();
				System.out.println();
			}
			
			else if (choose==3)
			{
				if (check==0)//正常的show
				{
					for (int i=0 ; i<push ; i++)
					{
						System.out.print(stack[i]+" ");
					}
				}
				
				if (check==1)//空掉的show
				{
					System.out.print("Stack is empty!");
				}
				System.out.println();
				System.out.println();
			}
			
			else if (choose==4)
			{
				System.out.print("Good Bye");
				break;
			}
		}
	}
}
