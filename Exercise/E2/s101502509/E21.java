package ce1002.E2.s101502509;

import java.util.Scanner;

public class E21 {

	private static int[] stack = new int[100];
	private static int stacklength;

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner input = new Scanner(System.in);
		int choice;
		
		do{
			System.out.println("1: push");
			System.out.println("2: pop");
			System.out.println("3: show");
			System.out.println("4: exit");
			choice = input.nextInt();   //input your choice
			
			switch(choice){
				case 1:
				{
					stackPush();
					break;
				}
				case 2:
				{
					stackPop();
					break;
				}
				case 3:
				{
					stackShow();
					break;
				}
				case 4:
				{
					exit();
					break;	
				}
			}
			System.out.println();
		}while(choice != 4);
	}

	private static void stackPush() {	//push function
		Scanner input = new Scanner(System.in);
		int pushsize;
		System.out.print("push ");
		pushsize = input.nextInt();
		
		for(int i = 0; i < pushsize; i++) {		//input push numbers
			stack[stacklength + i] = input.nextInt();
		}
		stacklength += pushsize;		//change stacklength
	}
	
	private static void stackPop() {	//pop function
		if(stacklength == 0) {
			System.out.print("Stack is empty!");
			return;
		}
		Scanner input = new Scanner(System.in);
		int popsize;
		System.out.print("pop ");
		popsize = input.nextInt();
		
		for(int i = stacklength-1; i >= stacklength - popsize; i--) {		//print pop numbers
			if(i < 0) {
				System.out.println();
				System.out.print("Stack is empty!");
				break;
			}
			System.out.print(stack[i] + " ");
		}
		stacklength -= popsize;		//change stacklength
		if(stacklength < 0) {
			stacklength = 0;
		}
		System.out.println();
	}
	
	private static void stackShow() {  //show function
		if(stacklength == 0) {
			System.out.print("Stack is empty!");
		}
		for(int i = 0; i < stacklength; i++) {		//print stack numbers
			System.out.print(stack[i] + " "); 
		}
		System.out.println();
	}
	
	private static void exit() {	//exit function
		System.out.println("Good Bye");
	}
}
