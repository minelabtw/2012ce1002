package ce1002.E2.s101502509;

import java.util.Scanner;

public class E22 {

	private static int[] queue = new int[100];
	private static int queuefront = 0;
	private static int queuerear = 0;

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner input = new Scanner(System.in);
		int choice;
		
		do{
			System.out.println("1: push");
			System.out.println("2: pop");
			System.out.println("3: show");
			System.out.println("4: exit");
			choice = input.nextInt();		//input your choice
			
			switch(choice){
				case 1:
				{
					queuePush();
					break;
				}
				case 2:
				{
					queuePop();
					break;
				}
				case 3:
				{
					queueShow();
					break;
				}
				case 4:
				{
					exit();
					break;	
				}
			}
			System.out.println();
		}while(choice != 4);
	}

	private static void queuePush() {	//push function
		Scanner input = new Scanner(System.in);
		int pushsize;
		System.out.print("push ");
		pushsize = input.nextInt();
		
		for(int i = 0; i < pushsize; i++) {		//input push numbers
			queue[queuefront + i] = input.nextInt();
		}
		queuerear += pushsize;
		queuefront = queuerear;
	}
	
	private static void queuePop() {	//pop function
		if(queuerear == 0) {
			System.out.print("Stack is empty!");
			return;
		}
		Scanner input = new Scanner(System.in);
		int popsize;
		System.out.print("pop ");
		popsize = input.nextInt();
		
		for(int i = 0; i < popsize; i++) {		//print pop numbers
			if(i >= queuerear) {
				System.out.println();
				System.out.print("Queue is empty!");
				break;
			}
			System.out.print(queue[i] + " ");
		}
		for(int j = 0; j < queuerear; j++) {	//move whole queue
			queue[j] = queue[j + popsize];
		}
		
		queuerear -= popsize;	
		queuefront -= popsize;
		if(queuerear < 0) {
			queuerear = 0;
		}
		System.out.println();
	}
	
	private static void queueShow() {	//show function
		if(queuerear == 0) {
			System.out.print("Queue is empty!");
		}
		for(int i = 0; i < queuerear; i++) {		//print stack numbers
			System.out.print(queue[i] + " "); 
		}
		System.out.println();
	}
	
	private static void exit() {	//exit function
		System.out.println("Good Bye");
	}

}



