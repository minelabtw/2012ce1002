package ce1002.s101502519;

import java.util.Scanner;

public class E22 {
	public static void main(String[] args) {
		Scanner in=new Scanner(System.in);
		int n=1,i,x=1;
		int[] s = new int[101];
		for(i=1;i<=100;i++){
			s[i]=i;
		}
		while(true){     //重複執行
			System.out.println("1: push");
			System.out.println("2: pop");
			System.out.println("3: show");
			System.out.println("4: exit");
			int a=in.nextInt();
			
			if(a==1){    //輸入1的情況
				System.out.print("push ");
				int push=in.nextInt();
				for(i=n;i<=push+n-1;i++){      //從起始項輸出到最後項
					System.out.print(s[i]+" ");
				}
				n=i-1;        //將最後項存起來
				System.out.println();
				System.out.println();
			}
			
			if(a==2){      //輸入2的情況
				System.out.print("pop ");
				int pop=in.nextInt();
				if(n-x<pop){   //pop超過陣列長度情況
					for(i=x;i<=n;i++){              //輸出原陣列
						System.out.print(s[i]+" ");
					}
					System.out.println();
					System.out.println("Queue is empty!");
					System.out.println();
					n=-1;   //記n為-1
				}
				
				else{
					for(i=x;i<=pop+x-1;i++){
						System.out.print(s[i]+" ");  //輸出要刪除的陣列
					}
					x=i;   ////將起始項存起來
					System.out.println();
					System.out.println();
				}
			}
			
			if(a==3){    //輸入3的情況
				if(n>0){     //非空的情況
					for(i=x;i<=n;i++){
						System.out.print(s[i]+" ");  //從x開始輸出陣列
					}
					System.out.println();
					System.out.println();
				}
				else{    //空的情況
					System.out.println("Queue is empty!");
					System.out.println();
				}
			}
			
			if(a==4){    //輸入4的情況
				System.out.println("Good Bye");
				break;   //跳出迴圈
			}
		}	
	}	
}
