package ce1002.E2.s101502524;

import java.util.Scanner;

public class E22 {
	public static void main(String[] args)
	{
		int m = 100,p,n=0,pop=0,c=0,l=0,pops=0;
		int[] a = new int[m];
		Scanner input = new Scanner(System.in);
		while(true)
		{
			System.out.println("1:push");
			System.out.println("2:pop");
			System.out.println("3:show");
			System.out.println("4:exit");
			p=input.nextInt();
			if(p==1)//輸入
			{
				System.out.print("push ");
				n=input.nextInt();
				for(int i =0;i<n;i++)
				{
					a[i]=input.nextInt();
				}
				System.out.println();
			}
			if(p==2)
			{
				System.out.print("pop ");
				pop=input.nextInt();
				if(n-pops-pop<0)//如果要跳出的數字大於所剩數字，則跳出所剩數字並顯示"Queue is empty!"
				{
					for(;l<n;l++)
					{
						System.out.print(a[l] + " ");
					}
					c=1;
					System.out.println();
					System.out.print("Queue is empty!");
					System.out.println();
				}
				else//正常的跳出數字
				{
					pops=pop+pops;
					for(;l<pops;l++)
					{
						System.out.print(a[l] + " ");
					}
					System.out.println();
					System.out.println();
				}
			}
			if(p==3)
			{
				if(c==0)//顯示數列中剩下的數字
				{
					for(int i=pops;i<n;i++)
					{
						System.out.print(a[i]+" ");
					}
				}
				else if(c==1)//如果數列已為空，則顯示"Queue is empty!"
				{
					System.out.print("Queue is empty!");
				}
				System.out.println();
				System.out.println();
			}
			if(p==4)//結束程式
			{
				System.out.print("Good bye");
				break;
			}
		}
	}
}