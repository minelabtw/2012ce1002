package ce1002.E2.s101502524;

import java.util.Scanner;

public class E21 {
	public static void main(String[] args)
	{
		int m = 100,p,n=0,pop=0,c=0;
		int[] a = new int[m];
		Scanner input = new Scanner(System.in);
		while(true)
		{
			System.out.println("1:push");
			System.out.println("2:pop");
			System.out.println("3:show");
			System.out.println("4:exit");
			p=input.nextInt();
			if(p==1)//輸入
			{
				System.out.print("push ");
				n=input.nextInt();
				for(int i =0;i<n;i++)
				{
					a[i]=input.nextInt();
				}
				System.out.println();
			}
			if(p==2)
			{
				System.out.print("pop ");
				pop=input.nextInt();
				if(n-pop<0)//如果要跳出的數字大於所剩數字，轉換pop並將c存為1
				{
					pop=n;
					c=1;
				}
				for(int i=n-1;i>=n-pop;i--)//正堂跳出數字
				{
					System.out.print(a[i] + " ");
				}
				if(c==1)//如果要跳出的數字大於所剩數字，則跳出所剩數字並顯示"Stack is empty!"
				{
					System.out.println();
					System.out.print("Stack is empty!");
				}
				n=n-pop;
				System.out.println();
				System.out.println();
			}
			if(p==3)
			{
				if(c==0)//正常顯示剩下的數字
				{
					for(int i=0;i<n;i++)
					{
						System.out.print(a[i]+" ");
					}
				}
				else if(c==1)//如果數列是空的，則輸出"Stack is empty!"
				{
					System.out.print("Stack is empty!");
				}
				System.out.println();
				System.out.println();
			}
			if(p==4)//結束程式
			{
				System.out.print("Good bye");
				break;
			}
		}
	}
}
