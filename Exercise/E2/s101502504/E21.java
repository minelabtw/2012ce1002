package ce1002.E2.s101502504;
import java.util.Scanner;
public class E21 
{
	public static void main(String[] args)
	{
		Scanner input = new Scanner(System.in);

		int pop=0,exit=0;
		int length = 0;
		int[] array = new int[100];//initialize
		
		while(exit==0)
		{
			System.out.println("1: push");
			System.out.println("2: pop");
			System.out.println("3: show");
			System.out.println("4: exit");
			int number = input.nextInt();//decide 1 2 3 4 which to choose

			if(number==1)//user wants to push
			{
				System.out.print("push ");
				length = input.nextInt();
				for(int i=0; i<length; i++)//put input to array	
					array[i]=input.nextInt();

				System.out.print("\n");
			}
			
			if(number==2)//user pops
			{
				System.out.print("pop ");
				pop = input.nextInt();

				if(length-pop<=0)//there are not  numbers in array
				{
					for(int i=length-1; i>=0;i--)
						System.out.print(array[i]+" ");
					System.out.print("\n");
					System.out.println("Stack is empty!");
					length=0;
				}
				else if(length-pop>0)//there are numbers in array
				{
					for(int i=length-1; i>=length-pop;i--)
						System.out.print(array[i]+" ");
					length = length-pop;//let length change
				}
				System.out.print("\n");
			}

			if(number==3)//show the array
			{
				if(length<=0)//if length<=0
					System.out.println("Stack is empty!");
				
				else if(length>0)
					for(int i=0; i<length; i++)
						System.out.print(array[i]+" ");
					
				System.out.print("\n");
				
				System.out.print("\n");
			}
			
			if(number==4)//user wants to exit
			{
				System.out.println("Good Bye");
				exit=1;
			}
			
		}
	}
}
