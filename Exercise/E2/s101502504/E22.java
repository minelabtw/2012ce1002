package ce1002.E2.s101502504;
import java.util.Scanner;
public class E22
{
	public static void main(String[] args)
	{
		Scanner input = new Scanner(System.in);

		int pop=0,exit=0,cut=0;
		int length = 0;
		int[] array = new int[100];//initialize
		
		while(exit==0)
		{
			System.out.println("1: push");
			System.out.println("2: pop");
			System.out.println("3: show");
			System.out.println("4: exit");
			int number = input.nextInt();//decide 1 2 3 4 which to choose

			if(number==1)//user wants to push
			{
				System.out.print("push ");
				length = input.nextInt();
				for(int i=0; i<length; i++)//put input to array	
					array[i]=input.nextInt();

				System.out.print("\n");
			}
			
			if(number==2)//user pops
			{
				System.out.print("pop ");
				pop = input.nextInt();
				if(length-pop<=0)//there are not  numbers in array
				{
					for(int i=cut; i<length;i++)
						System.out.print(array[i]+" ");
					System.out.print("\n");
					System.out.println("Queue is empty!");
					length=0;
				}
				else if(length-pop>0)//there are numbers in array
				{
					for(int i=0; i<pop;i++)
						System.out.print(array[i]+" ");
					cut=pop+cut;
				}

				System.out.print("\n");
			}

			if(number==3)//show the array
			{
				if(length<=0)//if length<=0
					System.out.println("Queue is empty!");
				
				else if(length>0)
					for(int i=pop; i<length; i++)
						System.out.print(array[i]+" ");
					
				System.out.print("\n");

			}
			
			if(number==4)//user wants to exit
			{
				System.out.println("Good Bye");
				exit=1;
			}
			
		}
	}
}
