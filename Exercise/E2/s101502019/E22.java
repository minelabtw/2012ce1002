package ce1002.E2.s101502019;
import java.util.*;
public class E22 {
	public static void main(String[] args){
		int choice = 0,numpush = 0,numpop = 0,count = 0;//count is to record how many number do you input
		int[] queue  = new int[100];
		Scanner input = new Scanner(System.in);
		while (true){
			System.out.println("1: push\n2: pop\n3: show\n4: exit");
			choice = input.nextInt();
			//////////////////////////////////////
			if (choice == 1){
				System.out.print("push ");
				numpush = input.nextInt();
				for(int i = 0;i < numpush;i++){
					queue[count] = input.nextInt();
					count++;
				}
				System.out.print("\n");
			}
			/////////////////////////////////////
			else if (choice == 2){
				System.out.print("pop ");
				numpop = input.nextInt();
				for(int i = 0;i < numpop;i++){
					System.out.print(queue[0]+" ");
					for(int n = 0;n < 99;n++){
						queue[n] = queue[n+1];//to adjust the position of the array
					}
					count--;
					if (count <= 0){
						System.out.println("\nQueue is empty!");
						break;
					}
				}
				System.out.println("\n");
			}
			/////////////////////////////////////
			else if (choice == 3){
				for(int i = 0;i < count;i++){
					System.out.print(queue[i]+" ");
				}
				if (count <= 0)
					System.out.println("\nQueue is empty!");
				System.out.println("\n");
			}
			/////////////////////////////////////
			else if(choice == 4){
				System.out.println("Good Bye");
				break;
			}
			/////////////////////////////////////
		}
	}
}
