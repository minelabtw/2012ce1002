package ce1002.E2.s101502023;

import java.util.Scanner;

public class E21 
{
    public static void main(String[] args)
    {
    	Scanner input = new Scanner(System.in);
    	int way;
    	int store=0;
    	int array[] = new int[100];
	    while(true)//repeat the loop
	    {	
    	    System.out.println("1: push");
	    	System.out.println("2: pop");
	    	System.out.println("3: show");
	    	System.out.println("4: exit");
	    	way=input.nextInt();//choose the way above four selections
	    	
	    	if(way==1)
	    	{
	            System.out.print("push ");
	            int number=input.nextInt();
	            for(int i=store;i<number+store;i++)
	            	array[i]=input.nextInt();//you can enter the number of the digitals
	            store=number+store;
	            System.out.println("");
	        }
///////////////////////////////////////////////////////////////////////////////////////////////////////	    	
	    	else if(way==2)
	    	{	    
	    		System.out.print("pop ");
		    	int number=input.nextInt();
		    	if(store-number-1<0)//check whether popping over the digitals or not
		    	{	
		    	    for(int i=store-1;i>=0;i--)
		    	    	System.out.print(array[i]+" ");
		    	    System.out.println("");
		    	    System.out.println("Stack is empty!");
		    	    System.out.println("");
		    	    store=0;
		    	}    
		    	else
		    	{
		    	    for(int i=store-1;i>store-number-1;i--)
		    	    	System.out.print(array[i]+" ");
		    	    System.out.println("");
		    	    System.out.println("");
		    	    store=store-number;//display the popping digitals
		    	}
	    	}
///////////////////////////////////////////////////////////////////////////////////////////////////////
	    	else if(way==3)
	    	{
		    	if(store-1<0)
		    	{
	    		    System.out.println("Stack is empty!");
	    		    System.out.println("");
	    		}
	    		else
	    		{
	    		    for(int i=0;i<=store-1;i++)
		    			System.out.print(array[i]+" ");
		    		System.out.println("");
		    		System.out.println("");//show the rest of the digitals
	    		}
	    	}
////////////////////////////////////////////////////////////////////////////////////////////////////////	    	
	    	else if(way==4)
	    	{
	    		System.out.print("Good Bye");
	    		return;//close the program
	    	}	
	    } 	
    }
}
