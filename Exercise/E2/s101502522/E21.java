package ce1002.E2.s101502522;
import java.util.Scanner;

public class E21 
{
	public static void main(String[] args)
	{
		int[] stack = new int[10];
		int size=0;
		while(true)
		{
			System.out.print("1: push\n2: pop\n3: show\n4: exit\n");
			Scanner input=new Scanner(System.in);
			

			int choose=input.nextInt();

			switch(choose)
			{
				case 1:
					System.out.print("push ");
					int push=input.nextInt();
					
					for(int i=0; i<push ; i++)
						stack[i]=input.nextInt();
					System.out.println();
					size=push;
					break;
					
				case 2:
					System.out.print("pop ");
					int pop=input.nextInt();
					
					for(int j=0; j<pop; j++)
					{
						System.out.print(stack[size-1-j]+" ");
						
						if(size-j-1==0)
						{
							System.out.println();
							System.out.print("Stack is empty!");
							size=0;
							break;
						}
					}
					if(size-pop<0)
						size=0;
					else
						size=size-pop;
					System.out.println();
					System.out.println();
					break;
					
				case 3:
					for(int k=0; k<size ;k++)
						System.out.print(stack[k]+" ");
					if(size==0)
						System.out.print("Stack is empty!");
					System.out.println();
					System.out.println();

					break;
					
				case 4:
					System.out.print("Good Bye");
					return;
					
			}
		}
			
	}

}
