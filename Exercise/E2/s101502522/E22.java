package ce1002.E2.s101502522;
import java.util.Scanner;

public class E22 
{
	public static void main(String[] args)
	{
		int[] Queue = new int[10];
		int size=0;
		int push=0;
		int pop=0;
		while(true)
		{
			System.out.print("1: push\n2: pop\n3: show\n4: exit\n");
			Scanner input=new Scanner(System.in);
			

			int choose=input.nextInt();

			switch(choose)
			{
				case 1:
					System.out.print("push ");
					push=input.nextInt();
					int i;
					for(i=0; i<push ; i++)
						Queue[i]=input.nextInt();
					System.out.println();
					size=push;
					break;
					
				case 2:
					System.out.print("pop ");
					pop=input.nextInt();
					int j;
					for(j=0; j<pop; j++)
					{
						System.out.print(Queue[j]+" ");
						
						if(size-pop<=0)
						{
							System.out.println();
							System.out.print("Queue is empty!");
							break;
						}
					}
					if(size-pop<0)
						size=0;
					else
						size=size-pop;
					System.out.println();
					System.out.println();
					break;
					
				case 3:
					for(int k=pop; k<push ;k++)
						System.out.print(Queue[k]+" ");
					if(size==0)
						System.out.print("Queue is empty!");
					System.out.println();
					System.out.println();

					break;
					
				case 4:
					System.out.print("Good Bye");
					return;
					
			}
		}
			
	}

}
