package ce1002.E2.s101502512;

import java.util.Scanner;

public class E21 {
	public static void main(String[] args) {
		int len = 0;// 輸入的長度
		int[] stack = new int[100];
		int a = 0;//陣列的長度
		int number2 = 0;//削去的長度
		for (;;) {
			Scanner input = new Scanner(System.in);
			System.out.println("1: push\n2: pop\n3: show\n4: exit");
			int x = input.nextInt();
			if (x == 1) {
				System.out.print("push ");
				len = input.nextInt();

				if (len > 100) {
					System.out.println("input again");// 超過陣列
				}
				for (int i = 0; i < len; i++) {
					stack[a] = input.nextInt();
					a++;
				}
			} else if (x == 2) {
				System.out.print("pop ");
				number2 = input.nextInt();
				for (int i = 0; i < number2; i++) {
					if (i == a) {//超過陣列重新輸入
						System.out.print("\nStack is empty!");
						len = 0;
						stack = new int[100];
						a = 0;
						number2 = 0;
						break;
					}
					System.out.print(stack[a - 1 - i] + " ");
				}
				a = a - number2;
			} else if (x == 3) {
				for (int i = 0; i < a; i++)
					System.out.print(stack[i] + " ");
				if (a == 0) {//超過陣列重新輸入
					System.out.println("Stack is empty!");
					len = 0;
					stack = new int[100];
					a = 0;
					number2 = 0;
				}

			} else if (x == 4) {
				System.out.println("Good bye");
				break;
			} else
				System.out.println("input again");
			System.out.println("\n");
		}
	}
}