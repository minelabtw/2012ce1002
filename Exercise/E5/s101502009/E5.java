package ce1002.E5.s101502009;

import java.awt.Color;
import java.awt.Graphics;

import javax.swing.JFrame;
import javax.swing.JPanel;
class paint extends JPanel{
	protected void paintComponent(Graphics g){
		super.paintComponent(g);
		
		int xCenter = getWidth()/2;
		int yCenter = getHeight()/2;
		int radius = (int)(Math.min(getWidth(), getHeight())*0.4);
		int x = xCenter - radius;
		int y = yCenter - radius;
		g.setColor(Color.black);
		g.fillArc(x, y, 2*radius, 2*radius, 0 ,45 );
		g.setColor(Color.pink);
		g.fillArc(x, y, 2*radius, 2*radius, 45 ,45 );
		g.setColor(Color.blue);
		g.fillArc(x, y, 2*radius, 2*radius, 90 ,45 );
		g.setColor(Color.green);
		g.fillArc(x, y, 2*radius, 2*radius, 135 ,45 );
		g.setColor(Color.red);
		g.fillArc(x, y, 2*radius, 2*radius, 180 ,45 );
		g.setColor(Color.white);
		g.fillArc(x, y, 2*radius, 2*radius, 225 ,45 );
		g.setColor(Color.yellow);
		g.fillArc(x, y, 2*radius, 2*radius, 270 ,45 );
		g.setColor(Color.gray);
		g.fillArc(x, y, 2*radius, 2*radius, 315 ,45 );
	}
	}
public class E5 {
	JFrame jf;
	E5(){
		jf=new JFrame("E5");		
		jf.setSize(1000,1000);
		jf.setVisible(true);
		jf.setDefaultCloseOperation(jf.EXIT_ON_CLOSE);
		jf.add(new paint());
		}
		
	
	public static void main(String[] args){
		E5 e5=new E5();
		
	}

}
