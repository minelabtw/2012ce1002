package ce1002.E5.s101502512;
import javax.swing.*;

import java.awt.Color;
import java.awt.Graphics;

public class E5 extends JFrame{
public E5(){
	setTitle("DrawArcs");
	add(new roundPanel());
}
public static void main(String[] args){
	E5 frame = new E5();
	frame.setSize(500,600);
	frame.setLocationRelativeTo(null);
	frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	frame.setVisible(true);
}
}
class roundPanel extends JPanel{
	protected void paintComponent(Graphics g){
		super.paintComponents(g);
		
		int xCenter=getWidth()/2;//find center
		int yCenter=getHeight()/2;//find center
		int radius=(int)(Math.min(getWidth(), getHeight())*0.5);//find radius
		int x=xCenter-radius;
		int y=yCenter-radius;
		
		Color color =new Color(155,0,155);//set color
		g.setColor(color);
		g.fillArc(x,y,2*radius,2*radius,0,45);
		
		g.setColor(Color.yellow);
		g.fillArc(x,y,2*radius,2*radius,45,90);
		
		g.setColor(Color.red);
		g.fillArc(x,y,2*radius,2*radius,90,45);
		
		g.setColor(Color.cyan);
		g.fillArc(x,y,2*radius,2*radius,135,45);
		
		g.setColor(Color.orange);
		g.fillArc(x,y,2*radius,2*radius,180,45);
		
		g.setColor(Color.green);
		g.fillArc(x,y,2*radius,2*radius,225,45);
		
		g.setColor(Color.pink);
		g.fillArc(x,y,2*radius,2*radius,270,45);
		
		g.setColor(Color.MAGENTA);
		g.fillArc(x,y,2*radius,2*radius,315,45);
		
	}

	private Color Color(int i, int j, int k) {
		// TODO Auto-generated method stub
		return null;
	}

}