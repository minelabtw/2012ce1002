package ce1002.E5.s101502507;

import java.awt.*; 
import java.awt.event.*; 
import javax.swing.*;

public class E5 extends JFrame 
{
	public E5()
	{
		setTitle("E5"); //設標題
		add(new ArcPanel());
	}
	
	public static void main(String[] args)
	{
		E5 frame = new E5();
		frame.setSize(300, 300); //視窗大小
		frame.setLocationRelativeTo(null);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setVisible(true);
	}
}
	class ArcPanel extends JPanel
	{
		protected void paintComponent(Graphics g)
		{
			super.paintComponent(g);
			int xCenter = getWidth()/2; //定圓心x位置
			int yCenter = getWidth()/2; //定圓心y位置
			int radius = (int)(Math.min(getWidth(), getHeight())*0.4);
			
			int x = xCenter-radius; //圓邊緣與視窗之左右距離
			int y = yCenter-radius; //圓邊緣與視窗之上下距離
			
			g.setColor(Color.blue); //藍色
			g.fillArc(x, y, 2*radius, 2*radius, 0, 45); //從0度開始轉45度			
			g.setColor(Color.red); //紅色
			g.fillArc(x, y, 2*radius, 2*radius, 45, 45); //從45度開始轉45度			
			g.setColor(Color.pink); //粉紅色
			g.fillArc(x, y, 2*radius, 2*radius, 90, 45); //從90度開始轉45度			
			g.setColor(Color.orange); //橘色
			g.fillArc(x, y, 2*radius, 2*radius, 135, 45); //從135度開始轉45度			
			g.setColor(Color.green); //綠色
			g.fillArc(x, y, 2*radius, 2*radius, 180, 45); //從180度開始轉45度			
			g.setColor(Color.gray); //灰色
			g.fillArc(x, y, 2*radius, 2*radius, 225, 45); //從225度開始轉45度			
			g.setColor(Color.black); //黑色
			g.fillArc(x, y, 2*radius, 2*radius, 270, 45); //從270度開始轉45度			
			g.setColor(Color.white); //白色
			g.fillArc(x, y, 2*radius, 2*radius, 315, 45); //從315度開始轉45度		
		}
	}