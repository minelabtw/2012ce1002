package ce1002.E5.s101502504;
import java.awt.*;
import javax.swing.*;

public class E5 extends JFrame
{
	public E5()
	{
		setTitle("E5");//set title
		add(new ArcsPanel());//add panel with a circle
	}
	
	public static void main(String args[])
	{
		E5 frame = new E5();
		frame.setSize(500,400);
		frame.setLocationRelativeTo(null);
		frame.setDefaultCloseOperation(EXIT_ON_CLOSE);
		frame.setVisible(true);
	}
}

class ArcsPanel extends JPanel
{
	protected void paintComponent(Graphics g)
	{
		super.paintComponent(g);
		
		int xCenter = getWidth()/2;
		int yCenter = getHeight()/2;
		int radius = (int)(Math.min(getWidth(),getHeight())*0.4);
		int x = xCenter-radius;//the initial x 
		int y = yCenter-radius;//the initial y 
		
		g.setColor(Color.blue);//0~45 angle
		g.fillArc(x,y,2*radius,2*radius,0,45);
		
		g.setColor(Color.cyan);//45~90angle
		g.fillArc(x,y,2*radius,2*radius,45,45);
		
		g.setColor(Color.gray);//90~135 angle
		g.fillArc(x,y,2*radius,2*radius,90,45);
		
		g.setColor(Color.green);//135~180 angle
		g.fillArc(x,y,2*radius,2*radius,135,45);
		
		g.setColor(Color.magenta);//180~225 angle
		g.fillArc(x,y,2*radius,2*radius,180,45);
		
		g.setColor(Color.red);//225~270 angle
		g.fillArc(x,y,2*radius,2*radius,225,45);
		
		g.setColor(Color.yellow);//270~315 angle
		g.fillArc(x,y,2*radius,2*radius,270,45);
		
		g.setColor(Color.pink);//315~360 angle
		g.fillArc(x,y,2*radius,2*radius,315,45);
	}
	
}
