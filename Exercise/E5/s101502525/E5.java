package ce1002.E5.s101502525;

import java.awt.Color;
import java.awt.Graphics;

import javax.swing.*;

public class E5 extends JFrame{
	//colors
	Color[] c={Color.RED,Color.ORANGE,Color.YELLOW,Color.GREEN,Color.BLUE,Color.MAGENTA,Color.BLACK,Color.WHITE};
	
	public E5(){
		setUI();
	}

	private void setUI() {
		add(new JPanel(){
			public void paintComponent(Graphics g){
				for(int angle=0,i=0;i<c.length;angle+=360/c.length,i++){
					g.setColor(c[i]);//ith color
					g.fillArc(0, 0,450,450,angle,360/c.length);	
				}
			}
		});
		setSize(500, 500);
		setVisible(true);
	}
	
	public static void main(String[] args){
		new E5();
	}
}
