package ce1002.E5.s100201510;
import java.awt.*;
import javax.swing.*;
public class E5{
	public static void main(String[] args) {
		JFrame frame = new JFrame("E5");
		frame.setSize(300, 300);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		// add the panel.
		frame.add(new PNL());
		frame.setVisible(true);
	}
}
class PNL extends JPanel {
	/**
	 * panel with 8 different color.
	 */
	private static final long serialVersionUID = 1L;

	protected void paintComponent(Graphics g) {
		super.paintComponents(g);
		// paint eight arcs with different color.
		for(int i = 0 ; i < 8 ; i++){
			g.setColor(new Color((int)(Math.random()*256),(int)(Math.random()*256),(int)(Math.random()*256)));
			g.fillArc(0,0,(int)(Math.min(getHeight(), getWidth())) , (int)(Math.min(getHeight(), getWidth())) , 45*i , 45);
		}
	}
}