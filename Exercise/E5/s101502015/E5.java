package ce1002.E5.s101502015;

import javax.swing.JFrame;
import javax.swing.JPanel;

import java.awt.Color;
import java.awt.Graphics;

public class E5 extends JFrame{
	
	public E5(){
		setTitle("E5");
		add(new ArcsPanel());
	}
	
	public static void main(String[] args){
		E5 frame = new E5();//設計版面
		frame.setSize(300,300);
		frame.setLocationRelativeTo(null);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setVisible(true);
	}

	class ArcsPanel extends JPanel{
		protected void paintComponent(Graphics g){
			super.paintComponent(g);
			
			int xCenter = getWidth()/2;//設計園的大小
			int yCenter = getHeight()/2;
			int radius = (int)(Math.min(getWidth(),getHeight())*0.4);
			int x = xCenter - radius;
			int y = yCenter - radius;
			
			g.setColor(Color.cyan);//設計圓的分割
			g.fillArc(x,y,2*radius,2*radius,0,45);
			g.setColor(Color.gray);
			g.fillArc(x,y,2*radius,2*radius,45,45);
			g.setColor(Color.red);
			g.fillArc(x,y,2*radius,2*radius,90,45);
			g.setColor(Color.yellow);
			g.fillArc(x,y,2*radius,2*radius,135,45);
			g.setColor(Color.green);
			g.fillArc(x,y,2*radius,2*radius,180,45);
			g.setColor(Color.pink);
			g.fillArc(x,y,2*radius,2*radius,225,45);
			g.setColor(Color.green);
			g.fillArc(x,y,2*radius,2*radius,270,45);
			g.setColor(Color.magenta);
			g.fillArc(x,y,2*radius,2*radius,315,45);
				
		}
	}
}
