package ce1002.E5.s976002019;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;

import javax.swing.JFrame;
import javax.swing.JPanel;

public class E5 extends JFrame {
	
	public E5(){
		JFrame frame =new JFrame();
		frame.setSize(300, 300);
		frame.setVisible(true);
		//JPanel panel=new JPanel();
		//frame.add(panel);
		frame.add(new ArcsPanel());
		
	}
	
	public static void main(String arg[]){
		
		
		E5 e5=new E5();
	
		
		
	}

}
class ArcsPanel extends JPanel {
	  // Draw four blazes of a fan
	  	protected void paintComponent(Graphics g) {
	    super.paintComponent(g);

	    int xCenter = getWidth() / 2;
	    int yCenter = getHeight() / 2;
	    int radius = (int)(Math.min(getWidth(), getHeight()) * 0.4);

	    int x = xCenter - radius;
	    int y = yCenter - radius;
	    
	    Color color1=new Color(0, 0, 0);
	    g.setColor(color1);
	    g.fillArc(x, y, 2 * radius, 2 * radius, 0, 45);
	    
	    Color color2=new Color(255, 255, 255);
	    g.setColor(color2);
	    g.fillArc(x, y, 2 * radius, 2 * radius, 45, 45);
	    
	    Color color3=new Color(255, 0, 0);
	    g.setColor(color3);
	    g.fillArc(x, y, 2 * radius, 2 * radius, 90, 45); 
	    
	    Color color4=new Color(0, 255, 0);
	    g.setColor(color4);
	    g.fillArc(x, y, 2 * radius, 2 * radius, 135, 45);
	    
	    Color color5=new Color(0, 0, 255);
	    g.setColor(color5);
	    g.fillArc(x, y, 2 * radius, 2 * radius, 180, 45);
	    
	    Color color6=new Color(255, 255, 0);
	    g.setColor(color6);
	    g.fillArc(x, y, 2 * radius, 2 * radius, 225, 45);
	    
	    Color color7=new Color(0, 255, 255);
	    g.setColor(color7);
	    g.fillArc(x, y, 2 * radius, 2 * radius, 270, 45);
	    
	    Color color8=new Color(255, 0, 255);
	    g.setColor(color8);
	    g.fillArc(x, y, 2 * radius, 2 * radius, 315, 45);
	    
	  }
	}
