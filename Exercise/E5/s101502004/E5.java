package ce1002.E5.s101502004;
import javax.swing.*;
import java.awt.*;

public class E5 extends JFrame{
	public E5(){
		add(new ArcsPanel());//將class add進去
	}
	
	public static void main(String[] args){//main function設定視窗
		E5 frame = new E5();
		frame.setTitle("E5");
		frame.setSize(500, 500);
		frame.setLocationRelativeTo(null);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setVisible(true);
	}
	
	class ArcsPanel extends JPanel{//new panel class
		protected void paintComponent(Graphics g){//override paintComponent
			super.paintComponent(g);//draw things in the superclass
			
			g.setColor(Color.blue);
			g.fillArc(0, 0, 300, 300, 0, 45);//45degree from 0
			g.setColor(Color.magenta);
			g.fillArc(0, 0, 300, 300, 45, 45);//45degree from 45
			g.setColor(Color.red);
			g.fillArc(0, 0, 300, 300, 90, 45);//45degree from 90
			g.setColor(Color.orange);
			g.fillArc(0, 0, 300, 300, 135, 45);//45degree from 135
			g.setColor(Color.green);
			g.fillArc(0, 0, 300, 300, 180, 45);//45degree from 180
			g.setColor(Color.white);
			g.fillArc(0, 0, 300, 300, 225, 45);//45degree from 225
			g.setColor(Color.yellow);
			g.fillArc(0, 0, 300, 300, 270, 45);//45degree from 270
			g.setColor(Color.gray);
			g.fillArc(0, 0, 300, 300, 315, 45);//45degree from 315
		}
	}

}
