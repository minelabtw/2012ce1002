package ce1002.E5.s100201021;
import javax.swing.*;
import java.awt.*;
public class E5 extends JFrame{
	public E5() {
		//set frame
		setVisible(true);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setSize(500,500);	
		add(new AP());
	}
	public static void main (String[] args){
		E5 fr=new E5();
	}
	class AP extends JPanel {
		protected void paintComponent(Graphics g) {
			super.paintComponent(g);
			int xc=getWidth()/2,yc=getHeight()/2;
			int r=(int)(Math.min(xc, yc)*0.8); 
			int x=xc-r,y=yc-r;
			for(int i=0;i<8;i++){
				//draw
				g.setColor(new Color((255/8)*i,(255/8)*i,225-25*i));
				g.fillArc(x, y, 2*r, 2*r, 45*i, 45);
			}
		}
	}
}


//   Graphics g;
// 	 super.paintComponent(g);
