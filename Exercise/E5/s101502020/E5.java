//E5 by J
package ce1002.E5.s101502020;

import javax.swing.*;
import java.awt.*;

public class E5 extends JFrame{

	public E5() {
		setTitle("DrawArc");
		add(new ArcsPanel());
	}		//new data type for needing GUI
	
	public static void main(String[] args) {
		
		E5 frame = new E5();
		frame.setLocationRelativeTo(null);
		frame.setDefaultCloseOperation(EXIT_ON_CLOSE);
		frame.setSize(250,300);
		frame.setVisible(true);

	}
	
	class ArcsPanel extends JPanel {
		
		protected void paintComponent(Graphics g) {
			super.paintComponent(g);		//i don't know what is this 
			
			
			int xCenter = getWidth()/2;
			int yCenter = getHeight()/2;
			int radius =(int)(Math.min(getWidth(),getHeight() )*0.4);
			int x= xCenter-radius;
			int y= yCenter-radius;
			
			//color instructions below
			g.setColor(Color.blue);
			g.fillArc(x, y, 2*radius, 2*radius, 0, 45);
			g.setColor(Color.cyan);
			g.fillArc(x, y, 2*radius, 2*radius, 45, 45);
			g.setColor(Color.gray);
			g.fillArc(x, y, 2*radius, 2*radius, 90, 45);
			g.setColor(Color.green);
			g.fillArc(x, y, 2*radius, 2*radius, 135, 45);
			g.setColor(Color.magenta);
			g.fillArc(x, y, 2*radius, 2*radius, 180, 45);
			g.setColor(Color.red);
			g.fillArc(x, y, 2*radius, 2*radius, 225, 45);
			g.setColor(Color.yellow);
			g.fillArc(x, y, 2*radius, 2*radius, 270, 45);
			g.setColor(Color.pink);
			g.fillArc(x, y, 2*radius, 2*radius, 315, 45);
			
			
		}
	}

}
