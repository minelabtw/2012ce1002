package ce1002.E5.s101502509;

import java.awt.*;
import javax.swing.*;

public class E5 extends JFrame{

	public E5() {
		setUI();
	}
	
	public void setUI() {
		setTitle("E5");
		add(new ArcPanel());
		setSize(400, 300);
		setLocationRelativeTo(null);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setVisible(true);
	}
	
	public class ArcPanel extends JPanel {
		Color[] ColorArray = {Color.RED, Color.YELLOW, Color.GREEN, Color.BLUE, 	//color array
							  Color.PINK, Color.GRAY, Color.CYAN, Color.MAGENTA};
		
		protected void paintComponent(Graphics g) {
			super.paintComponents(g);
			//get the center of panel 
			int XCenter = getWidth()/2;		
			int YCenter = getHeight()/2;
			
			int radius = (int)(Math.min(getWidth(), getHeight()) * 0.5);	//get the radius
			
			//get the upper left corner 
			int x = XCenter - radius;
			int y = YCenter - radius;
			
			//draw the region
			for(int angle = 0, index = 0; index < ColorArray.length; angle += 45, index++) {
				g.setColor(ColorArray[index]);	//set the color
				g.fillArc(x, y, 2 * radius, 2* radius, angle, 45);
			}
		}
	}
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		new E5();
	}

}
