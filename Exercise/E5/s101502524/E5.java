package ce1002.E5.s101502524;

import javax.swing.*;
import java.awt.*;

public class E5 extends JFrame{
	public E5()
	{
		setTitle("E5");
		add(new ArcsPanel());
	}

	public static void main(String[] args)
	{		
		E5 d = new E5();
		d.setVisible(true);//設定使視窗可被看見
		d.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	    d.setLocationRelativeTo(null);
	    d.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	    d.setSize(250, 300);
	    d.setVisible(true);
	}
}
class ArcsPanel extends JPanel {
	  protected void paintComponent(Graphics g) {
	    super.paintComponent(g);
	    //中心點
	    int xCenter = getWidth() / 2;
	    int yCenter = getHeight() / 2;
	    int radius = (int)(Math.min(getWidth(), getHeight()) * 0.4);

	    int x = xCenter - radius;
	    int y = yCenter - radius;
	    //分八個角度上色
	    g.setColor(Color.blue);
	    g.fillArc(x, y, 2 * radius, 2 * radius, 0, 45);
	    g.setColor(Color.black);
	    g.fillArc(x, y, 2 * radius, 2 * radius, 45, 45);
	    g.setColor(Color.CYAN);
	    g.fillArc(x, y, 2 * radius, 2 * radius, 90, 45);
	    g.setColor(Color.green);
	    g.fillArc(x, y, 2 * radius, 2 * radius, 135, 45);
	    g.setColor(Color.MAGENTA);
	    g.fillArc(x, y, 2 * radius, 2 * radius, 180, 45);
	    g.setColor(Color.red);
	    g.fillArc(x, y, 2 * radius, 2 * radius, 225, 45);
	    g.setColor(Color.yellow);
	    g.fillArc(x, y, 2 * radius, 2 * radius, 270, 45);
	    g.setColor(Color.pink);
	    g.fillArc(x, y, 2 * radius, 2 * radius, 315, 45);
	  }
	}
