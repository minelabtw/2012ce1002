package ce1002.E5.s101502520;

import java.awt.Color;
import java.awt.Graphics;

import javax.swing.JPanel;
import javax.swing.JFrame;

public class E5 extends JFrame{
	public E5()
	{
		iniUI();
	}
	
	public void iniUI()
	{
		add(new ArcsPanel());
		setSize(300, 300);
		setDefaultCloseOperation(EXIT_ON_CLOSE);
	}
	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
            	new E5().setVisible(true);
            }
        });
	}
}

class ArcsPanel extends JPanel{
	
	protected void paintComponent(Graphics g)
	{
		super.paintComponent(g);
		
		int xCenter = getWidth() / 2;
		int yCenter = getHeight() / 2;
		int radius = (int)(Math.min(getWidth(), getWidth() * 0.4));
		
		int x = xCenter - radius;
		int y = yCenter - radius;
		
		// setting color and fill the arcs
		g.setColor(Color.blue);
		g.fillArc(x, y, 2 * radius, 2 * radius, 0, 45);
		g.setColor(Color.cyan);
	    g.fillArc(x, y, 2 * radius, 2 * radius, 45, 45);
	    g.setColor(Color.lightGray);
	    g.fillArc(x, y, 2 * radius, 2 * radius, 90, 45);
	    g.setColor(Color.green);
	    g.fillArc(x, y, 2 * radius, 2 * radius, 135, 45);
	    g.setColor(Color.magenta);
	    g.fillArc(x, y, 2 * radius, 2 * radius, 180, 45);
	    g.setColor(Color.red);
	    g.fillArc(x, y, 2 * radius, 2 * radius, 225, 45);
	    g.setColor(Color.yellow);
	    g.fillArc(x, y, 2 * radius, 2 * radius, 270, 45);
	    g.setColor(Color.pink);
	    g.fillArc(x, y, 2 * radius, 2 * radius, 315, 45);
	}
}
