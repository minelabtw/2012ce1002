package ce1002.E5.s101502527;
import javax.swing.*;

import java.awt.Color;
import java.awt.Graphics;

public class E5 extends JFrame {
	
		public E5() { //呼叫circle
		setTitle("circle");
		add(new circle());
	}
	
	public static void main(String[] args){ //frame
			E5 frame  = new E5();
			frame.setSize(500,500);
			frame.setLocationRelativeTo(null);
			frame.setDefaultCloseOperation(EXIT_ON_CLOSE);
			frame.setVisible(true);
		}
	

	class circle extends JPanel{
		protected void paintComponent(Graphics g){
			super.paintComponent(g);
			int x=getWidth()/2; //設定中點
			int y=getHeight()/2;
			int r= 200; //設定半徑
			g.setColor(Color.BLACK);
			g.fillArc(x-r, y-r, r*2,r*2,0,45);
			g.setColor(Color.blue);
			g.fillArc(x-r, y-r, r*2,r*2,45,45);
			g.setColor(Color.red);
			g.fillArc(x-r, y-r, r*2,r*2,90,45);
			g.setColor(Color.green);
			g.fillArc(x-r, y-r, r*2,r*2,135,45);
			g.setColor(Color.yellow);
			g.fillArc(x-r, y-r, r*2,r*2,180,45);
			g.setColor(Color.pink);
			g.fillArc(x-r, y-r, r*2,r*2,225,45);
			g.setColor(Color.cyan);
			g.fillArc(x-r, y-r, r*2,r*2,270,45);
			g.setColor(Color.magenta);
			g.fillArc(x-r, y-r, r*2,r*2,315,45);
			
			
		}
	}

}
