package ce1002.E5.s101502515;
//每次執行隨機顏色
import java.awt.Color;
import java.awt.Graphics;
import javax.swing.JFrame;

public class E5 extends JFrame{
	static int state=0;//runstate
	static int co_r;//rgb setcolor
	static int co_g;
	static int co_b;
	private static Graphics g=null;
	public E5(){//set default
		this.setSize(600, 600);
		this.setVisible(true);
		g=getGraphics();
	}
	public void paint(Graphics g)//paint arc
	 {
		g.setColor(new Color(co_r,co_g,co_b));
		g.fillArc(100, 100, 250, 250, state, 45);
	 }
	public static void main(String args[]) {
		E5 cc=new E5();
		for(int i =0;i<8;i++){// run for all
			cc.paint(g);
			chcolor();
			state+=45;
		}
	}
	public static void chcolor(){//random for rgb
		co_r=(int)((Math.random()*255)+1);
		co_g=(int)((Math.random()*255)+1);
		co_b=(int)((Math.random()*255)+1);
	}
}
