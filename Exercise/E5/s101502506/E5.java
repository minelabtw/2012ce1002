package ce1002.E5.s101502506;

import java.awt.Color;
import java.awt.Component;
import java.awt.Graphics;
import javax.swing.JPanel;
import javax.swing.JFrame;

public class E5 extends JFrame
{
	public E5(){
		setTitle("E5");
		add(new ArcsJPanel());
	}


	public static void main(String[] args)
	{
		E5 frame= new E5();
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		ArcsJPanel arcsJPanel = new ArcsJPanel();
		frame.add(arcsJPanel);
		frame.setSize(300,300);
		frame.setVisible(true);
		
	}
}

class ArcsJPanel extends JPanel {
	public void paintComponent( Graphics g )
	{
		super.paintComponent(g); //�I�sø��method
		
		this.setBackground( Color.WHITE);//�I�����զ�
		
		g.setColor( Color.RED);//��
		g.fillArc(50, 50, 150, 150, 0, 45);
		g.setColor(Color.BLACK);//��
		g.fillArc(50, 50, 150, 150, 45, 45);
		
		g.setColor(Color.BLUE);//��
		g.fillArc(50, 50, 150, 150, 90, 45);
		g.setColor(Color.YELLOW);//��
		g.fillArc(50, 50, 150, 150, 135, 45);
		
		g.setColor(Color.GREEN);//��
		g.fillArc(50, 50, 150, 150, 180, 45);
		g.setColor(Color.ORANGE);//��
		g.fillArc(50, 50, 150, 150, 225, 45);
		
		g.setColor(Color.PINK);//����
		g.fillArc(50, 50, 150, 150, 270, 45);
		g.setColor(Color.CYAN);//����
		g.fillArc(50, 50, 150, 150, 315, 45);
	}
}
