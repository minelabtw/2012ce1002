package ce1002.E5.s101502027;
import javax.swing.*;
import java.awt.*;
public class E5 extends JFrame{
	
	public E5(){
		setTitle("E5");	
		add(new paint());
	}
	public static void main (String[]args){
		E5 fr=new E5();
		fr.setSize(400,300);
		fr.setLocationRelativeTo(null);
		fr.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		fr.setVisible(true);
	}
	class paint extends JPanel{
		protected void paintComponent(Graphics g){
			super.paintComponent(g);
			int xC = getWidth()/2;
			int yC = getHeight()/2;
			int radius = (int)(Math.min(getWidth(),getHeight())*0.4);
			int x = xC-radius;
			int y = yC-radius;
			g.setColor(Color.yellow);
			g.fillArc(x,y ,2*radius ,2*radius ,0 ,45   );
			g.setColor(Color.blue);
			g.fillArc(x,y ,2*radius ,2*radius ,45 ,45  );
			g.setColor(Color.green);
			g.fillArc(x,y ,2*radius ,2*radius ,90 ,45  );
			g.setColor(Color.red);
			g.fillArc(x,y ,2*radius ,2*radius , 135, 45);
			g.setColor(Color.darkGray);
			g.fillArc(x,y ,2*radius ,2*radius , 180,45 );
			g.setColor(Color.pink);
			g.fillArc(x,y ,2*radius ,2*radius , 225,45 );
			g.setColor(Color.orange);
			g.fillArc(x,y ,2*radius ,2*radius , 270,45 );
			g.setColor(Color.black);
			g.fillArc(x,y ,2*radius ,2*radius , 315,45 );
		}//���
	}

}
