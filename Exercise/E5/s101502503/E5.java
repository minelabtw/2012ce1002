package ce1002.E5.s101502503;

import java.awt.Color;
import java.awt.Graphics;

import javax.swing.JFrame;
import javax.swing.JPanel;

public class E5 extends JFrame{
	public E5() {
		setTitle("E5");
		add(new ArcsPanel());
	}
	
	public static void main(String[] args)
	{
		E5 frame = new E5();
		frame.setSize( 400 , 400 );//size of panel
		frame.setLocationRelativeTo(null); // center the frame
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setVisible(true);
	}
}
// the class for drawing arcs on a panel 
class ArcsPanel extends JPanel{
	//draw 8 blades
	protected void paintComponent(Graphics g){
		super.paintComponent(g);
		
		int xCenter = getWidth()/2;
		int yCenter = getHeight()/2;
		int radius = (int)(Math.min(getWidth(), getHeight()) * 0.4 ) ;//取半徑
		
		int x = xCenter - radius;
		int y = yCenter - radius;
		
		g.setColor(Color.RED);//這行以下為此顏色
		g.fillArc( x, y, 2*radius, 2*radius, 0, 45);
		//( 起始座標(x,y), 寬度 , 長度, 起始角度, 轉多少角度)
		g.setColor(Color.ORANGE);//這行以下為此顏色
		g.fillArc( x, y, 2*radius, 2*radius, 45, 45);
		g.setColor(Color.YELLOW);//這行以下為此顏色
		g.fillArc( x, y, 2*radius, 2*radius, 90, 45);
		g.setColor(Color.GREEN);//這行以下為此顏色
		g.fillArc( x, y, 2*radius, 2*radius, 135, 45);
		g.setColor(Color.cyan);//這行以下為此顏色
		g.fillArc( x, y, 2*radius, 2*radius, 180, 45);
		g.setColor(Color.BLUE);//這行以下為此顏色
		g.fillArc( x, y, 2*radius, 2*radius, 225, 45);
		g.setColor(Color.MAGENTA);//這行以下為此顏色
		g.fillArc( x, y, 2*radius, 2*radius, 270, 45);
		g.setColor(Color.pink);//這行以下為此顏色
		g.fillArc( x, y, 2*radius, 2*radius, 315, 45);		
	}
}
