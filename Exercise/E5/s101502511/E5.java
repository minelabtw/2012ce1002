package CE1002.E5.s101502511;

import javax.swing.*;
import java.awt.*;

public class E5 extends JFrame {
	
	public E5(){
			setTitle("E5");
			add(new panel());
		}

	public static void main(String[] args) {
		
		E5 frame = new E5();
		
		frame.setSize(500,500);
		frame.setLocationRelativeTo(null);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setVisible(true);
		
	}

}

//the class for drawing arcs on a panel
class panel extends JPanel {
	
	private static final Color Color = null;
/////Draw eight blades of fan
	protected void paintComponent(Graphics g){
		
		super.paintComponent(g);

		int xcenter = getWidth()/2;
		int ycenter = getHeight()/2;
		int radius = (int)(Math.min(getWidth(), getHeight())*0.4);
		
		int x= xcenter-radius;
		int y= ycenter-radius;
		
		//以下為填滿區塊
		g.fillArc(x, y, 2*radius, 2*radius, 0,45);
		g.setColor(Color.blue);//顏色
		g.fillArc(x, y, 2*radius, 2*radius, 45,45);
		g.setColor(Color.orange);
		g.fillArc(x, y, 2*radius, 2*radius, 90,45);
		g.setColor(Color.yellow);
		g.fillArc(x, y, 2*radius, 2*radius, 135,45);
		g.setColor(Color.white);
		g.fillArc(x, y, 2*radius, 2*radius, 180,45);
		g.setColor(Color.red);
		g.fillArc(x, y, 2*radius, 2*radius, 225,45);
		g.setColor(Color.green);
		g.fillArc(x, y, 2*radius, 2*radius, 270,45);
		g.setColor(Color.gray);
		g.fillArc(x, y, 2*radius, 2*radius, 315,45);
					 
	}
}
