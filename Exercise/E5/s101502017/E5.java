package ce1002.E5.s101502017;

import java.awt.Color;
import java.awt.Graphics;

import javax.swing.*;

public class E5 extends JFrame{
	public E5(){
		setTitle("E5");
		add(new ArcsPanel());
	}
	
	public static void main(String[] args){
		E5 frame = new E5();
		frame.setSize(250,300);
		frame.setLocale(null);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setVisible(true);
	}
	
	class ArcsPanel extends JPanel{
		protected void paintComponent(Graphics g){
			super.paintComponents(g);
			
			int xCenter =getWidth() / 2;
			int yCenter =getHeight() / 2;
			int R = (int)(Math.min(getWidth(), getHeight())*0.4);
			int x = xCenter - R;
			int y = yCenter - R;
			
			g.setColor(Color.blue);
			g.fillArc(x, y, 2*R,2*R, 0, 45);
			g.setColor(Color.cyan);
			g.fillArc(x, y, 2*R, 2*R, 45, 45);
			g.setColor(Color.gray);
			g.fillArc(x, y, 2*R, 2*R, 90, 45);
			g.setColor(Color.green);
			g.fillArc(x, y, 2*R, 2*R, 135, 45);
			g.setColor(Color.magenta);
			g.fillArc(x, y, 2*R, 2*R, 180, 45);
			g.setColor(Color.red);
			g.fillArc(x, y, 2*R, 2*R, 225, 45);
			g.setColor(Color.yellow);
			g.fillArc(x, y, 2*R, 2*R, 270, 45);
			g.setColor(Color.pink);
			g.fillArc(x, y, 2*R, 2*R, 315, 45);
			
		}
	}
}
