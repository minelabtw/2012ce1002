package ce1002.E5.s101502018;
import javax.swing.*;
import java.awt.*;
public class E5 extends JFrame{
	public E5(){
		setTitle("E5");
		add(new ArcsPanel());
	}
	public static void main(String[] args){
		E5 frame=new E5();
		frame.setSize(350,350);
		frame.setLocationRelativeTo(null);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setVisible(true);
	}
	class ArcsPanel extends JPanel{
		protected void paintComponent(Graphics g){
			super.paintComponent(g);
			g.setColor(Color.blue);
			g.fillArc(0, 0, 300, 300, 0, 45);
			g.setColor(Color.cyan);
			g.fillArc(0, 0, 300, 300, 45, 45);
			g.setColor(Color.gray);
			g.fillArc(0, 0, 300, 300, 90, 45);
			g.setColor(Color.green);
			g.fillArc(0, 0, 300, 300, 135, 45);
			g.setColor(Color.magenta);
			g.fillArc(0, 0, 300, 300, 180, 45);
			g.setColor(Color.red);
			g.fillArc(0, 0, 300, 300, 225, 45);
			g.setColor(Color.yellow);
			g.fillArc(0, 0, 300, 300, 270, 45);
			g.setColor(Color.pink);
			g.fillArc(0, 0, 300, 300, 315, 45);
		}
	}
}
