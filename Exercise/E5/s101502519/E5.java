package ce1002.E5.s101502519;

import java.awt.Color;
import java.awt.Graphics;

import javax.swing.JFrame;

public class E5 extends JFrame{
	public static void main(String[] args) {
		new E5();
	}
	
	public E5(){
		super("E5");      //新增視窗
		this.setSize(500, 500);
		this.setVisible(true);
	}
	
	public void paint(Graphics g){
		g.setColor(Color.blue);                //畫八個扇形
		g.fillArc(50, 50, 400, 400, 0, 45);
		g.setColor(Color.cyan);
		g.fillArc(50, 50, 400, 400, 45, 45);
		g.setColor(Color.gray);
		g.fillArc(50, 50, 400, 400, 90, 45);
		g.setColor(Color.green);
		g.fillArc(50, 50, 400, 400, 135, 45);
		g.setColor(Color.magenta);
		g.fillArc(50, 50, 400, 400, 180, 45);
		g.setColor(Color.red);
		g.fillArc(50, 50, 400, 400, 225, 45);
		g.setColor(Color.yellow);
		g.fillArc(50, 50, 400, 400, 270, 45);
		g.setColor(Color.pink);
		g.fillArc(50, 50, 400, 400, 315, 45);
	}
}
