package ce1002.E5.s101502501;
import javax.swing.JPanel;
import javax.swing.JFrame;

import java.awt.Color;
import java.awt.Graphics;

public class E5 extends JFrame{
	public E5(){
		setTitle("DrawArcs");
		add(new ArcsPanel());
	}
	
	/** Main method */
	public static void main(String[] args){
		E5 frame = new E5();
		frame.setSize(250, 300);
		frame.setLocationRelativeTo(null);//Center the frame
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setVisible(true);
	}

}
//The class for drawing arcs on a panel
class ArcsPanel extends JPanel {
	//Draw
	protected void paintComponent(Graphics g){
		super.paintComponent(g);
		
		int xCenter = getWidth()/2;//x軸的中心是視窗寬度的一半
		int yCenter = getHeight()/2;//y軸的中心是視窗寬度的一半
		int radius = (int)(Math.min(getWidth(), getHeight())*0.4);
		//半徑是視窗長寬較短的那邊乘上0.4
		int x = xCenter - radius;
		int y = yCenter - radius;
		
		g.setColor(Color.BLUE);//設定顏色，下面那行就會是這個顏色
		g.fillArc(x, y, 2*radius, 2*radius, 0, 45);
		//(起始座標, 起始座標, 寬度, 高度, 起始角度, 走多少角度)
		g.setColor(Color.CYAN);
		g.fillArc(x, y, 2*radius, 2*radius, 45, 45);
		g.setColor(Color.GREEN);
		g.fillArc(x, y, 2*radius, 2*radius, 90, 45);
		g.setColor(Color.RED);
		g.fillArc(x, y, 2*radius, 2*radius, 135, 45);
		g.setColor(Color.GRAY);
		g.fillArc(x, y, 2*radius, 2*radius, 180, 45);
		g.setColor(Color.MAGENTA);
		g.fillArc(x, y, 2*radius, 2*radius, 225, 45);
		g.setColor(Color.YELLOW);
		g.fillArc(x, y, 2*radius, 2*radius, 270, 45);
		g.setColor(Color.PINK);
		g.fillArc(x, y, 2*radius, 2*radius, 315, 45);
		
	}
}