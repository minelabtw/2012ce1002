package ce1002.E5.s101502523;
import java.awt.*;
import javax.swing.*;
public class E5 extends JFrame {
	public E5() {
		setTitle("E5");		
		//加入class
		add(new ArcsPanel());
	}
	public static void main(String[] args) {
		//設定介面
		E5 frame = new E5();
		frame.setSize(600,480);
		frame.setResizable(false);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setVisible(true);
	}	
}
class ArcsPanel extends JPanel {
	protected void paintComponent(Graphics g) {
		super.paintComponent(g);
		//設定中心點
		int xCenter = getWidth()/2;
		int yCenter = getHeight()/2;
		//設定半徑
		int radius = (int)(Math.min(getWidth(), getHeight())*0.4);
		//畫圖的原點
		int x = xCenter - radius;
		int y = yCenter - radius;
		//畫圖上色,每45度為一區塊
		g.fillArc(x, y, 2*radius, 2*radius, 0, 45);
		g.setColor(Color.RED);
		g.fillArc(x, y, 2*radius, 2*radius, 45, 45);
		g.setColor(Color.ORANGE);
		g.fillArc(x, y, 2*radius, 2*radius, 90, 45);
		g.setColor(Color.YELLOW);
		g.fillArc(x, y, 2*radius, 2*radius, 135, 45);
		g.setColor(Color.GREEN);
		g.fillArc(x, y, 2*radius, 2*radius, 180, 45);
		g.setColor(Color.CYAN);
		g.fillArc(x, y, 2*radius, 2*radius, 225, 45);
		g.setColor(Color.BLUE);
		g.fillArc(x, y, 2*radius, 2*radius, 270, 45);
		g.setColor(Color.MAGENTA);
		g.fillArc(x, y, 2*radius, 2*radius, 315, 45);	
	}
}
