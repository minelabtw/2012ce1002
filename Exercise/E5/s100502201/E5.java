//E5
//Draw rotating pallete
//Use graphics to draw
package ce1002.E5.s100502201;

import javax.swing.JPanel;
import javax.swing.JFrame;
import java.awt.Color;
import java.util.Timer;
import java.util.TimerTask;
import java.awt.Graphics;

public class E5 extends JFrame {

	

	public E5() { // Frame constructor

		setTitle("E5");
		setSize(250, 250);
		setLocation(250, 250);
		add(new pallete());
		setVisible(true);

	}

	public static void main(String[] args) {

		new E5();

	}
	
	class pallete extends JPanel {

		Color[] color = { Color.PINK, Color.BLUE, Color.CYAN, Color.GRAY,
				Color.GREEN, Color.MAGENTA, Color.RED, Color.YELLOW };
		int deg = 0;

		class rotate extends TimerTask { //Rotate during time

			public void run() {

				deg++;
				if (deg == 360)
					deg = 0;
				repaint();

			}

		}

		public pallete() { // Set timer

			Timer timer = new Timer();
			timer.schedule(new rotate(), 0, 10);

		}

		protected void paintComponent(Graphics g) { //Paint

			super.paintComponent(g);

			for (Color array : color) {
				g.setColor(array);
				g.fillArc(25, 25, 125, 125, deg, 45); //Set location, radius and degree
				deg += 45;
			}

		}

	}

}
