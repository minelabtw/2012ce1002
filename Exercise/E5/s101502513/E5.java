package ce1002.E5.s101502513;

import javax.swing.*;
import java.awt.*;

public class E5 extends JFrame {
	public E5() {
		setTitle("E5");
		add(new circle());
	}
	public static void main(String[] args) {
		E5 frame = new E5();
		frame.setSize(250, 250);
		frame.setLocationRelativeTo(null);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setVisible(true);
	}
	class circle extends JPanel {
		protected void paintComponent(Graphics g) {
			super.paintComponent(g);
			int x = 0;
			int y = 0;
			int radius = (int)(Math.min(getWidth(), getHeight()) * 0.4);
			g.setColor(Color.BLUE);
			g.fillArc(x, y, 2 * radius, 2 * radius, 0, 45);
			g.setColor(Color.CYAN);
			g.fillArc(x, y, 2 * radius, 2 * radius, 45, 45);
			g.setColor(Color.GRAY);
			g.fillArc(x, y, 2 * radius, 2 * radius, 90, 45);
			g.setColor(Color.GREEN);
			g.fillArc(x, y, 2 * radius, 2 * radius, 135, 45);
			g.setColor(Color.MAGENTA);
			g.fillArc(x, y, 2 * radius, 2 * radius, 180, 45);
			g.setColor(Color.RED);
			g.fillArc(x, y, 2 * radius, 2 * radius, 225, 45);
			g.setColor(Color.YELLOW);
			g.fillArc(x, y, 2 * radius, 2 * radius, 270, 45);
			g.setColor(Color.PINK);
			g.fillArc(x, y, 2 * radius, 2 * radius, 315, 45);
		}
	}
}
