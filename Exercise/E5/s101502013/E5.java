package ce1002.E5.s101502013;

import java.awt.Color;
import java.awt.FlowLayout;
import java.awt.Graphics;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

public class E5 extends JFrame {
	public E5(String title){
		//以下設定frame版面
		setTitle(title);
		//
		
		//加入panel
		add(new ArcsPanel());
		//
		
		
		
	}
	public static void main(String[] args){
		E5 lol = new E5("E5");
		lol.setSize(350 , 350);
		lol.setLocation(500,300);
		lol.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		lol.setVisible(true); // Display the frame
		
	}
}

class ArcsPanel extends JPanel {
	  // Draw four blazes of a fan
	  protected void paintComponent(Graphics g) {
	    super.paintComponent(g);

	    int xCenter = 350 / 2;
	    int yCenter = 350 / 2;
	    int radius = (int)(350 * 0.4);
	    int x = xCenter - radius;
	    int y = yCenter - radius;

	    g.setColor(Color.pink);
	    g.fillArc(x, y, 2 * radius, 2 * radius, 0, 45);
	    g.setColor(Color.yellow);
	    g.fillArc(x, y, 2 * radius, 2 * radius, 45, 45);
	    g.setColor(Color.green);
	    g.fillArc(x, y, 2 * radius, 2 * radius, 90, 45);
	    g.setColor(Color.white);
	    g.fillArc(x, y, 2 * radius, 2 * radius, 135, 45);
	    g.setColor(Color.blue);
	    g.fillArc(x, y, 2 * radius, 2 * radius, 180, 45);
	    g.setColor(Color.RED);
	    g.fillArc(x, y, 2 * radius, 2 * radius, 225, 45);
	    g.setColor(Color.MAGENTA);
	    g.fillArc(x, y, 2 * radius, 2 * radius, 270, 45);
	    g.setColor(Color.orange);
	    g.fillArc(x, y, 2 * radius, 2 * radius, 315, 45);
	    
	    
	  }
	}
