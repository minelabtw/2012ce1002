package ce1002.E5.s101502014;
import javax.swing.*; //引入GUI需要的功能
import java.awt.*; //引入繪圖的功能
public class E5 extends JFrame {
	public E5() {
		setTitle("E5"); //設定視窗名稱
		add(new circle()); //把繪圖結果加入到視窗中
	}
	
	public static void main(String[] args) {
		E5 frame = new E5(); //建立視窗
		frame.setSize(300 , 300); //設定視窗大小
		frame.setLocationRelativeTo(null); //固定視窗位置出現在螢幕的中間
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setVisible(true); //讓視窗出現
	}
	
	class circle extends JPanel{
		protected void paintComponent(Graphics g) {
			super.paintComponents(g);
			g.setColor(Color.red); //圓上的0度到45度塗上紅色
			g.fillArc(0 , 0 , 250 , 250 , 0 , 45);
			g.setColor(Color.orange); //圓上的45度到90度塗上橘色
			g.fillArc(0 , 0 , 250 , 250 , 45 , 45);
			g.setColor(Color.yellow); //圓上的90度到135度塗上黃色
			g.fillArc(0 , 0 , 250 , 250 , 90 , 45);
			g.setColor(Color.green); //圓上的135度到180度塗上綠色
			g.fillArc(0 , 0 , 250 , 250 , 135 , 45);
			g.setColor(Color.blue); //圓上的180度到225度塗上藍色
			g.fillArc(0 , 0 , 250 , 250 , 180 , 45);
			g.setColor(new Color(102 , 0 , 255)); //圓上的225度到270度塗上靛色
			g.fillArc(0 , 0 , 250 , 250 , 225 , 45);
			g.setColor(Color.magenta); //圓上的270度到315度塗上紫色
			g.fillArc(0 , 0 , 250 , 250 , 270 , 45);
			g.setColor(Color.black); //圓上的315度到360度塗上黑色
			g.fillArc(0 , 0 , 250 , 250 , 315 , 45);
		}
	}
}
