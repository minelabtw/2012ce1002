package ce1002.E5.s101502521;

import java.awt.Color;
import java.awt.Container;
import java.awt.Graphics;
import java.awt.Polygon;

import javax.swing.JFrame;
import javax.swing.JPanel;

public class E5 extends JPanel {
	public void paintComponent(Graphics g) {
	    super.paintComponent(g);
	    int radius = 300;
	    g.setColor(Color.blue);//select color first
		g.fillArc(0, 0, 2 * radius, 2 * radius, 0, 45);//fill the arc
		g.setColor(Color.cyan);
	    g.fillArc(0, 0, 2 * radius, 2 * radius, 45, 45);
	    g.setColor(Color.lightGray);
	    g.fillArc(0, 0, 2 * radius, 2 * radius, 90, 45);
	    g.setColor(Color.green);
	    g.fillArc(0, 0, 2 * radius, 2 * radius, 135, 45);
	    g.setColor(Color.magenta);
	    g.fillArc(0, 0, 2 * radius, 2 * radius, 180, 45);
	    g.setColor(Color.red);
	    g.fillArc(0, 0, 2 * radius, 2 * radius, 225, 45);
	    g.setColor(Color.yellow);
	    g.fillArc(0, 0, 2 * radius, 2 * radius, 270, 45);
	    g.setColor(Color.pink);
	    g.fillArc(0, 0, 2 * radius, 2 * radius, 315, 45);

	  }
	
	public static void main(String[] args)
	{
		JFrame frame = new JFrame("E5");
	    frame.setSize(800, 800);
	    frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	    Container contentPane = frame.getContentPane();
	    contentPane.add(new E5());//use my class as panel
	    frame.setVisible(true);
	}
}
