package ce1002.E5.s100502205;

import javax.swing.JFrame;
import javax.swing.JPanel;

import java.awt.Color;
import java.awt.Graphics;

public class E5 extends JFrame {
	public E5() {
		this.setTitle("E5");
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		this.setSize(450, 450);
		this.add(new ArcsPanel());
		this.setVisible(true);
	}

	public static void main(String[] args) {
		new E5();
	}
}

class ArcsPanel extends JPanel {
	Color[] color = { Color.BLUE, Color.CYAN, Color.GRAY, Color.GREEN,
			Color.MAGENTA, Color.RED, Color.YELLOW, Color.PINK };

	protected void paintComponent(Graphics g) {
		super.paintComponent(g);

		int xCenter = getWidth() / 2;
		int yCenter = getHeight() / 2;
		int radius = (int) (Math.min(getWidth(), getHeight()) * 0.4);

		int x = xCenter - radius;
		int y = yCenter - radius;
		int theta = 0;

		for (Color vv : color) {
			g.setColor(vv);
			g.fillArc(x, y, 2 * radius, 2 * radius, theta, 45);
			theta += 45;
		}
	}
}
