package ce1002.E5.s995002022;

import java.awt.*;
import javax.swing.*;
 class E5 extends JFrame{
	public static void main(String[] args)throws Exception {
		E5 hw = new E5("E5");
		hw.setSize(600,600);
		hw.setLocation(50,50);
		hw.setVisible(true);
	}
	private Graphics g = null;
	public E5(String pTitle) {
		super(pTitle);
		setLayout(null);// 不使用版面配置		
		g = getGraphics();
	}
	public void paint(Graphics g){
		g.setColor(Color.blue);
		g.fillArc(100,100,400,400,0,45);
		g.setColor(Color.cyan);
		g.fillArc(100,100,400,400,45,45);
		g.setColor(Color.gray);
		g.fillArc(100,100,400,400,90,45);
		g.setColor(Color.green);
		g.fillArc(100,100,400,400,135,45);
		g.setColor(Color.magenta);
		g.fillArc(100,100,400,400,180,45);
		g.setColor(Color.red);
		g.fillArc(100,100,400,400,225,45);
		g.setColor(Color.yellow);
		g.fillArc(100,100,400,400,270,45);
		g.setColor(Color.pink);
		g.fillArc(100,100,400,400,315,45);
		
	}

}
