package ce1002.E5.s101502522;
import java.awt.*;
import javax.swing.*;
public class E5 extends JFrame {
	public E5() //設定E5 GUI的設置
	{
		setTitle("E5");
		add(new ArcsPanel());
	}
	public static void main(String[] args){ //建立E5 GUI
		E5 frame = new E5();
		frame.setSize(300,300);
		frame.setLocationRelativeTo(null);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setVisible(true);
	}

}
class ArcsPanel extends JPanel //畫圖
{
	protected void paintComponent(Graphics g)
	{
		super.paintComponent(g);
		int xCenter = getWidth()/2;//將中心設定在視窗的中心
		int yCenter = getHeight()/2;
		int radius = (int)(Math.min(getWidth(),getHeight())*0.4); //將半徑設為視窗中的長或寬(取小的)的0.4倍
		int x = xCenter-radius;
		int y = yCenter-radius;
		g.fillArc(x,y,2*radius,2*radius,0,45);    //畫圖並增加顏色
		g.setColor(Color.BLUE);
		g.fillArc(x,y,2*radius,2*radius,45,45);
		g.setColor(Color.RED);
		g.fillArc(x,y,2*radius,2*radius,90,45);
		g.setColor(Color.YELLOW);
		g.fillArc(x,y,2*radius,2*radius,135,45);
		g.setColor(Color.GREEN);
		g.fillArc(x,y,2*radius,2*radius,180,45);
		g.setColor(Color.PINK);
		g.fillArc(x,y,2*radius,2*radius,225,45);
		g.setColor(Color.MAGENTA);
		g.fillArc(x,y,2*radius,2*radius,270,45);
		g.setColor(Color.CYAN);
		g.fillArc(x,y,2*radius,2*radius,315,45);
		
		
	}
	
}