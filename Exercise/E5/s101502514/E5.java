package ce1002.E5.s101502514;
import javax.swing.JFrame;
import java.awt.*;
import javax.swing.JPanel;
import java.awt.event.*;
public class E5 extends JFrame{
	
	public E5() {
		setTitle("E5");
		add(new ArcsPanel());
	}
	
	public static void main(String[] args) {
		E5 frame = new E5();
		frame.setSize(300, 300);
		frame.setVisible(true);//可見
		frame.setLocationRelativeTo(null); 
	}
}

class ArcsPanel extends JPanel{
	protected void paintComponent(Graphics g) {
		super.paintComponent(g);  	
		g.fillArc(0,0,150,150,0,45);//畫弧
		g.setColor (Color.blue);//設定顏色
		g.fillArc(0,0,150,150,45,45);
		g.setColor (Color.WHITE);
		g.fillArc(0,0,150,150,90,45);
		g.setColor(Color.red);
		g. fillArc(0,0,150,150,135,45);
		g.setColor(Color.ORANGE);
		g. fillArc(0,0,150,150,180,45);
		g.setColor(Color.YELLOW);
		g. fillArc(0,0,150,150,225,45);
		g.setColor(Color.cyan);
		g. fillArc(0,0,150,150,270,45);
		g.setColor(Color.magenta);
		g. fillArc(0,0,150,150,315,45);
		g.setColor(Color.PINK);
	} 
}
	


