package ce1002.E5.s101502012;

import javax.swing.JFrame;
import javax.swing.JPanel;

import java.awt.Color;
import java.awt.Graphics;

public class E5 extends JFrame {
	public E5() {
		setTitle("DrawArcs");
		add(new ArcsPanel());
	}
	public static void main(String[] args) {
		E5 frame = new E5();
		frame.setSize(300,300);
		frame.setLocationRelativeTo(null);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setVisible(true);
	}
}
class ArcsPanel extends JPanel { //創建一個Class 裡面是畫好的圓形
	protected void paintComponent(Graphics g){
		super.paintComponent(g);
		
		int xCenter = getWidth()/2;
		int yCenter = getHeight()/2;
		
		int radius = (int)(Math.min(getWidth(),getHeight())*0.4);
		
		int x = xCenter - radius;
		int y = yCenter - radius;
		
		g.setColor(Color.BLACK);
		g.fillArc(x, y, 2*radius, 2*radius, 0, 45);
		g.setColor(Color.BLUE);
		g.fillArc(x, y, 2*radius, 2*radius, 45, 45);
		g.setColor(Color.CYAN);
		g.fillArc(x, y, 2*radius, 2*radius, 90, 45);
		g.setColor(Color.DARK_GRAY);
		g.fillArc(x, y, 2*radius, 2*radius, 135, 45);
		g.setColor(Color.GRAY);
		g.fillArc(x, y, 2*radius, 2*radius, 180, 45);
		g.setColor(Color.GREEN);
		g.fillArc(x, y, 2*radius, 2*radius, 225, 45);
		g.setColor(Color.MAGENTA);
		g.fillArc(x, y, 2*radius, 2*radius, 270, 45);
		g.setColor(Color.YELLOW);
		g.fillArc(x, y, 2*radius, 2*radius, 315, 45);
	}
}