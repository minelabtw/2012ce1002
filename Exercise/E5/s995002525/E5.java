package ce1002.E5.s995002525;

import java.awt.*;
import javax.swing.*;
 class E5 extends JFrame{
	public static void main(String[] args)throws Exception {
		E5 hw = new E5("E5");
		hw.setSize(400,400);
		hw.setLocation(50,50);
		hw.setVisible(true);
	}
	private Graphics g = null;
	public E5(String pTitle) {
		super(pTitle);
		setLayout(null);// 不使用版面配置		
		g = getGraphics();
	}
	public void paint(Graphics g){
		g.setColor(Color.blue);
		g.fillArc(75,75,250,250,0,45);
		g.setColor(Color.cyan);
		g.fillArc(75,75,250,250,45,45);
		g.setColor(Color.gray);
		g.fillArc(75,75,250,250,90,45);
		g.setColor(Color.green);
		g.fillArc(75,75,250,250,135,45);
		g.setColor(Color.magenta);
		g.fillArc(75,75,250,250,180,45);
		g.setColor(Color.red);
		g.fillArc(75,75,250,250,225,45);
		g.setColor(Color.yellow);
		g.fillArc(75,75,250,250,270,45);
		g.setColor(Color.pink);
		g.fillArc(75,75,250,250,315,45);
		
	}

}
