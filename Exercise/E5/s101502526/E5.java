package ce1002.E5.s101502526;

import javax.swing.JFrame;
import javax.swing.JPanel;

import java.awt.Color;
import java.awt.Graphics;

public class E5 extends JFrame {                    //增加有圖的PANEL
  public E5() {
    add(new Panel());
  }

  /** Main method */
  public static void main(String[] args) {          //設定視窗
    E5 frame = new E5();
    frame.setLocationRelativeTo(null); // Center the frame
    frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    frame.setSize(250, 300);
    frame.setVisible(true);
  }
}

// The class for drawing arcs on a panel
class Panel extends JPanel {
  // Draw four blazes of a fan
  protected void paintComponent(Graphics g) {
    super.paintComponent(g);

    int x = getWidth() / 2;                                               //中心點
    int y = getHeight() / 2;
    int radius = (int)(Math.min(getWidth(), getHeight()) * 0.4);
    int x2 = x - radius;
    int y2 = y - radius;
    
    g.setColor(Color.blue);                                              //逐步畫扇形
    g.fillArc(x2, y2, 2 * radius, 2 * radius, 0, 45);
    g.setColor(Color.cyan);
    g.fillArc(x2, y2, 2 * radius, 2 * radius, 45, 45);
    g.setColor(Color.gray);
    g.fillArc(x2, y2, 2 * radius, 2 * radius, 90, 45);
    g.setColor(Color.green);
    g.fillArc(x2, y2, 2 * radius, 2 * radius, 135, 45);
    g.setColor(Color.magenta);
    g.fillArc(x2, y2, 2 * radius, 2 * radius, 180, 45);
    g.setColor(Color.red);
    g.fillArc(x2, y2, 2 * radius, 2 * radius, 225, 45);
    g.setColor(Color.yellow);
    g.fillArc(x2, y2, 2 * radius, 2 * radius, 270, 45);
    g.setColor(Color.pink);
    g.fillArc(x2, y2, 2 * radius, 2 * radius, 315, 45);
  }
}