package ce1002.E5.s101502502;

import javax.swing.*;
import java.awt.Color;
import java.awt.Graphics;

public class E5 extends JFrame
{
	public E5()
	{
		setTitle("E5");//視窗名稱
		add(new ArcsPanel());
	}
	
	public static void main(String[] args)
	{
		E5 frame = new E5();
		frame.setSize(500, 500);
		frame.setLocationRelativeTo(null);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setVisible(true);
	}
	
	class ArcsPanel extends JPanel
	{
		protected void paintComponent(Graphics g)
		{
			super.paintComponent(g);
		
			int xCenter = getWidth() / 2;//圓心的x座標為視窗的一半
			int yCenter = getHeight() / 2;//圓心的y座標為視窗的一半
			int radius = (int)(Math.min(getWidth(),getHeight())*0.5);//設定半徑
			int x = xCenter - radius;
			int y = yCenter - radius;
			
			g.setColor (Color.orange);//橘色
			g.fillArc(x, y, 2*radius, 2*radius, 0, 45);//從0開始畫45度
			g.setColor (Color.blue);//藍色
			g.fillArc(x, y, 2*radius, 2*radius, 45, 45);//從45開始畫45度
			g.setColor (Color.green);//綠色
			g.fillArc(x, y, 2*radius, 2*radius, 90, 45);//從90開始畫45度
			g.setColor (Color.white);//白色
			g.fillArc(x, y, 2*radius, 2*radius, 135, 45);//從135開始畫45度
			
			g.setColor (Color.red);//紅色
			g.fillArc(x, y, 2*radius, 2*radius, 180, 45);//從180開始畫45度
			g.setColor (Color.black);//黑色
			g.fillArc(x, y, 2*radius, 2*radius, 225, 45);//從225開始畫45度
			g.setColor (Color.pink);//粉紅色
			g.fillArc(x, y, 2*radius, 2*radius, 270, 45);//從270開始畫45度
			g.setColor (Color.gray);//灰色
			g.fillArc(x, y, 2*radius, 2*radius, 315, 45);//從315開始畫45度
			
		}
	}
}
