package ce1002.E5.s100502006;

import java.awt.Color;
import java.awt.Graphics;

import javax.swing.JFrame;
import javax.swing.JPanel;

public class E5 extends JFrame{
	public E5(){
		setTitle("E5");
		add(new ArcsPanel());
	}
	
	public static void main(String[] args){
		E5 frame=new E5();
		frame.setSize(300,300);
		frame.setLocationRelativeTo(null);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setVisible(true);
	}
}

class ArcsPanel extends JPanel{
	protected void paintComponent(Graphics g) {
		super.paintComponent(g);//override the paintComponent method
		
		//the coordinate of center
		int xCenter=getWidth()/2;
		int yCenter=getHeight()/2;
		
		//set radius and starting coordinate
		int radius= (int)(Math.min(getWidth(),getHeight())*0.4);
		int x= xCenter - radius;
		int y = yCenter-radius;
		
		//draw arcs
		g.setColor(Color.blue);
		g.fillArc(x, y, 2*radius, 2*radius, 0,45);
		g.setColor(Color.cyan);
		g.fillArc(x, y, 2*radius, 2*radius, 45,45);
		g.setColor(Color.darkGray);
		g.fillArc(x, y, 2*radius, 2*radius, 90,45);
		g.setColor(Color.green);
		g.fillArc(x, y, 2*radius, 2*radius, 135,45);
		g.setColor(Color.magenta);
		g.fillArc(x, y, 2*radius, 2*radius, 180,45);
		g.setColor(Color.orange);
		g.fillArc(x, y, 2*radius, 2*radius, 225,45);
		g.setColor(Color.pink);
		g.fillArc(x, y, 2*radius, 2*radius, 270,45);
		g.setColor(Color.RED);
		g.fillArc(x, y, 2*radius, 2*radius, 315,45);
	}
}
