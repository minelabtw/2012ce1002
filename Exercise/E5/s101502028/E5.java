package ce1002.E5.s101502028;

import javax.swing.*;
import java.awt.*;

public class E5 extends JFrame { // set the frame of the windows
	public E5() {
		setTitle("E5");
		add(new ArcsPanel());
	}

	public static void main(String[] args) { // create the frame
		E5 frame = new E5();
		frame.setSize(600, 500);
		frame.setLocationRelativeTo(null);
		frame.setVisible(true);
	}
}

class ArcsPanel extends JPanel {
	protected void paintComponent(Graphics g) { // create a graphic
		super.paintComponents(g);
		int xCenter = getWidth() / 2; // get the x center
		int yCenter = getHeight() / 2; // get the y center
		int radius = (int) (Math.min(getWidth(), getHeight()) * 0.5); // radius
		int x = xCenter - radius; // center
		int y = yCenter - radius; // center
		
		g.setColor(Color.BLUE);
		g.fillArc(x, y, 2 * radius, 2 * radius, 0, 45); // each part of the pie
		
		g.setColor(Color.ORANGE);
		g.fillArc(x, y, 2 * radius, 2 * radius, 45, 45);
		
		g.setColor(Color.PINK);
		g.fillArc(x, y, 2 * radius, 2 * radius, 90, 45);
		
		g.setColor(Color.YELLOW);
		g.fillArc(x, y, 2 * radius, 2 * radius, 135, 45);
		
		g.setColor(Color.CYAN);
		g.fillArc(x, y, 2 * radius, 2 * radius, 180, 45);
		
		g.setColor(Color.GREEN);
		g.fillArc(x, y, 2 * radius, 2 * radius, 225, 45);
		
		g.setColor(Color.RED);
		g.fillArc(x, y, 2 * radius, 2 * radius, 270, 45);
		
		g.setColor(Color.MAGENTA);
		g.fillArc(x, y, 2 * radius, 2 * radius, 315, 45);
	}
}
