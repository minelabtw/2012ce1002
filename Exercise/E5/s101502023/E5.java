package ce1002.A5.s101502023;

import javax.swing.*;
import java.awt.*;

public class E5 extends JFrame
{
   public  static void main (String args[])
   {
	   new E5();
   }
   public E5()
   {
	   setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	   setSize(600,600);
	   setVisible(true);
	   setTitle("E5");
	   setBackground(Color.white);
   }
   public void paint(Graphics g)
   {
	    int xCenter = getWidth() /2;
		int yCenter = getHeight()/2;
		int radius = (int)(Math.min(getWidth(),getHeight())*0.4);
		
		int x = xCenter-radius;
		int y = yCenter-radius;
		
		g.setColor(Color.yellow);
		g.fillArc(0,0,2*radius,2*radius,0 ,45  );
		
		g.setColor(Color.darkGray);
		g.fillArc(x,y,2*radius,2*radius,45 ,45 );
		
		g.setColor(Color.green);
		g.fillArc(x,y,2*radius,2*radius,90 ,45  );
		
		g.setColor(Color.gray);
		g.fillArc(x,y,2*radius,2*radius,135,45 );
		
		g.setColor(Color.magenta);
		g.fillArc(x,y,2*radius,2*radius,180,45 );
		
		g.setColor(Color.pink);
		g.fillArc(x,y,2*radius,2*radius,225,45 );
		
		g.setColor(Color.lightGray);
		g.fillArc(x,y,2*radius,2*radius,270,45 );
		
		g.setColor(Color.blue);
		g.fillArc(x,y,2*radius,2*radius,315,45 );
   }
  
}
