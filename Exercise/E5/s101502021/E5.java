package ce1002.E5.s101502021;

import javax.swing.*;
import java.awt.*;

public class E5 extends JFrame {
	public static void main (String[] args){
		new E5();
	}
	
	public E5(){
		
		Container c=getContentPane();
	    c.setLayout(null); 

		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setSize(800,600);
		setTitle("E5");
		setVisible(true);
	
	}
	
	public void paint(Graphics g){
		int xCenter = getWidth()/2;//中心點x座標
		int yCenter = getHeight()/2;//中心點y座標
		int radius = (int)(Math.min(getWidth(),getHeight())*0.4);
		
		int x = xCenter - radius;
		int y = yCenter - radius;
		
		//設定顏色為藍色，從0度開始畫到45度
		g.setColor(Color.blue);
		g.fillArc( x, y, 2*radius, 2*radius, 0, 45);
		
		//設定顏色為淺藍，從45度開始畫到90度
		g.setColor(Color.cyan);
		g.fillArc( x, y, 2*radius, 2*radius, 45, 45);
		
		//設定顏色為灰色，從90度開始畫到135度
		g.setColor(Color.gray);
		g.fillArc( x, y, 2*radius, 2*radius, 90, 45);
		
		//設定顏色為綠色，從135度開始畫到180度
		g.setColor(Color.green);
		g.fillArc( x, y, 2*radius, 2*radius, 135, 45);
		
		//設定顏色為紫色，從180度開始畫到225度
		g.setColor(Color.magenta);
		g.fillArc( x, y, 2*radius, 2*radius, 180, 45);
		
		//設定顏色為紅色，從235度開始畫到270度
		g.setColor(Color.red);
		g.fillArc( x, y, 2*radius, 2*radius, 225, 45);
		
		//設定顏色為黃色，從270度開始畫到315度
		g.setColor(Color.yellow);
		g.fillArc( x, y, 2*radius, 2*radius, 270, 45);
		
		//設定顏色為粉紅，從315度開始畫到360度
		g.setColor(Color.pink);
		g.fillArc( x, y, 2*radius, 2*radius, 315, 45);
	}

}
