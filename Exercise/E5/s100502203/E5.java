package ce1002.E5.s100502203;

import java.awt.*;
import javax.swing.*;

public class E5 extends JFrame{
	public E5(){
		setSize(600, 600);
		setTitle("E5");
		add(new ArcsPanel());
		setLocationRelativeTo(null);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setVisible(true);
	}
	
	public static void main(String[] args) {
		E5 frame = new E5();
	}

}

class ArcsPanel extends JPanel{
	protected void paintComponent(Graphics g){
		super.paintComponents(g);
		
		int xCenter = getWidth() / 2;
		int yCenter = getHeight() / 2;
		int radius = (int) (Math.min(getWidth(), getHeight()) * 0.4);
		int x = xCenter - radius;
		int y = yCenter - radius;
		
		g.setColor(Color.RED);
		g.fillArc(x, y, 2 * radius, 2 * radius, 0, 45);
		g.setColor(Color.ORANGE);
		g.fillArc(x, y, 2 * radius, 2 * radius, 45, 45);
		g.setColor(Color.YELLOW);
		g.fillArc(x, y, 2 * radius, 2 * radius, 90, 45);
		g.setColor(Color.GREEN);
		g.fillArc(x, y, 2 * radius, 2 * radius, 135, 45);
		g.setColor(Color.BLUE);
		g.fillArc(x, y, 2 * radius, 2 * radius, 180, 45);
		g.setColor(Color.MAGENTA);
		g.fillArc(x, y, 2 * radius, 2 * radius, 225, 45);
		g.setColor(Color.PINK);
		g.fillArc(x, y, 2 * radius, 2 * radius, 270, 45);
		g.setColor(Color.GRAY);
		g.fillArc(x, y, 2 * radius, 2 * radius, 315, 45);
	}
}
