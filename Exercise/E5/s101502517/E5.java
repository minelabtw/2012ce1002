package ce1002.E5.s101502517;
import javax.swing.JFrame;
import javax.swing.JPanel;

import java.awt.Color;
import java.awt.Graphics;
public class E5 extends JFrame{
	
	public E5(){
		setTitle("E5");//
		add(new ArcsPanel());
	}
		
	
	public static void main(String[] args){
		E5 frame=new E5();//視窗名
		frame.setSize(250,300);//視窗大小
		frame.setLocationRelativeTo(null);//CENTER
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);//加入關閉紐
		frame.setVisible(true);//設定可見
		
		
		
	}
	
	class  ArcsPanel extends JPanel{
		protected void paintComponent(Graphics g){
			super.paintComponent(g);
			
			int xCenter=getWidth()/2;//X中心點
			int yCenter=getHeight()/2;//Y中心點
			int radius=(int)(Math.min(getWidth(),getHeight())*0.4);
			
			int x=xCenter-radius;//圓心
			int y=yCenter-radius;//圓心
			
			////////↓↓↓↓↓↓↓↓↓↓↓↓設定起始點 寬高  半徑 角度及顏色↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓//////////
			
			
			
			g.setColor(Color.blue);
			g.fillArc(x, y, 2*radius, 2*radius,   0, 45);
			
			g.setColor(Color.red);
			g.fillArc(x, y, 2*radius, 2*radius,  45, 45);
			
			g.setColor(Color.yellow);
			g.fillArc(x, y, 2*radius, 2*radius, 90, 45);
			
			g.setColor(Color.green);
			g.fillArc(x, y, 2*radius, 2*radius,   135, 45);
		
			g.setColor(Color.cyan);
			g.fillArc(x, y, 2*radius, 2*radius,   180, 45);
			
			g.setColor(Color.black);
			g.fillArc(x, y, 2*radius, 2*radius,  225, 45);
			
			g.setColor(Color.pink);
			g.fillArc(x, y, 2*radius, 2*radius,   270, 45);
			
			g.setColor(Color.magenta);
			g.fillArc(x, y, 2*radius, 2*radius,   315, 45);
			
			/////////↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑設定中心點 半徑 角度及顏色↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑//////////////
		
		}
		
		
		
		
	}
	


}

