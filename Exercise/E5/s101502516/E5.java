package ce1002.E5.s101502516;
import javax.swing.JPanel;
import javax.swing.JFrame;
import java.awt.Color;
import java.awt.Graphics;
public class E5 extends JFrame{
	public E5(){
		setTitle( "E5" );
		add(new ArcsPanel());
	}
	
	public static void main(String[] args) {
		E5 frame = new E5();  // 設定視窗
		frame.setSize( 500, 500 );
		frame.setLocationRelativeTo( null );
		frame.setDefaultCloseOperation( JFrame.EXIT_ON_CLOSE );
		frame.setVisible( true );
	 }
}

class ArcsPanel extends JPanel{
	
	protected void paintComponent( Graphics g ){ //畫圖上色
		super.paintComponent( g );
		
		g.fillArc( 70, 90, 300, 300, 0, 45 );
		g.setColor(Color.black);
		
		g.fillArc( 70, 90, 300, 300, 45, 45 );
		g.setColor(Color.YELLOW);
		
		g.fillArc( 70, 90, 300, 300, 90, 45 );
		g.setColor(Color.red);
		
		g.fillArc( 70, 90, 300, 300, 135, 45 );
		g.setColor(Color.blue);
		
		g.fillArc( 70, 90, 300, 300, 180, 45 );
		g.setColor(Color.green);
		
		g.fillArc( 70, 90, 300, 300, 225, 45 );
		g.setColor(Color.cyan);
		
		g.fillArc( 70, 90, 300, 300, 270, 45 );
		g.setColor(Color.GRAY);
		
		g.fillArc( 70, 90, 300, 300, 315, 45 );
		g.setColor(Color.magenta);
		
		g.fillArc( 70, 90, 300, 300, 360, 45 );
		g.setColor(Color.orange);
	}
}

