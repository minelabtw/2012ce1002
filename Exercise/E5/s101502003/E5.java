package ce1002.E5.s101502003;
import javax.swing.JFrame;
import javax.swing.JPanel;
import java.awt.Graphics;
import java.awt.*;
public class E5 extends JFrame{
	public E5()
	{
		setTitle("E5"); //設標題
		add(new ArcsPanel());
	}
	public static void main(String[] args)
	{
		E5 frame = new E5();
		frame.setSize(500,500); //設視窗大小
		frame.setLocationRelativeTo(null);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE); //關掉結束程式
		frame.setVisible(true);
	}
	class ArcsPanel extends JPanel
	{
		protected void paintComponent(Graphics g)
		{
			super.paintComponent(g); 
			int xCenter = getWidth()/2; //設x,y座標
			int yCenter = getHeight()/2;
			int radius = (int)(Math.min(getWidth(),getHeight())*0.4); //設半徑
			int x = xCenter - radius;
			int y = yCenter - radius;
			g.setColor(Color.BLUE); //設顏色
			g.fillArc(x, y, 2*radius, 2*radius, 0, 45); //設藍色的範圍,下面以此類推~~~
			g.setColor(Color.CYAN);
			g.fillArc(x, y, 2*radius, 2*radius, 45, 45);
			g.setColor(Color.DARK_GRAY);
			g.fillArc(x, y, 2*radius, 2*radius, 90, 45);
			g.setColor(Color.GREEN);
			g.fillArc(x, y, 2*radius, 2*radius, 135, 45);
			g.setColor(Color.magenta);
			g.fillArc(x, y, 2*radius, 2*radius, 180, 45);
			g.setColor(Color.RED);
			g.fillArc(x, y, 2*radius, 2*radius, 225, 45);
			g.setColor(Color.YELLOW);
			g.fillArc(x, y, 2*radius, 2*radius, 270, 45);
			g.setColor(Color.PINK);
			g.fillArc(x, y, 2*radius, 2*radius, 315, 45);
		}
	}
}
