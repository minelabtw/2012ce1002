package ce1002.E5.s101502002;
import javax.swing.*;
import java.awt.*;
public class E5 extends JFrame{	
		public E5(){
			setTitle("E5"); //狀態列標題
			add(new ArcsPanel()); //新增一個panel
		}	
	public static void main(String[] args){ //主程式
		E5 frame = new E5();  //一個新的frame
		frame.setSize(250,300); //長寬
		frame.setLocationRelativeTo(null); //出現在中間
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE); //按叉叉關閉
		frame.setVisible(true);		//顯示出來唷
	}
}
class ArcsPanel extends JPanel{ 
	protected void paintComponent(Graphics g){ 
		super.paintComponent(g);
		
		int xcen = getWidth()/2; //寬
		int ycen = getHeight()/2; //高
		int radius = (int)(Math.min(getWidth(),getHeight()*0.4)); //半徑
		int x = xcen-radius; //初始點x座標
		int y = ycen-radius; //初始點y座標
		
		g.setColor(Color.red); //設定顏色
		g.fillArc(x,y,2*radius,2*radius,0,45); //畫最初的1/8
		g.setColor(Color.blue);
		g.fillArc(x,y,2*radius,2*radius,45,45);
		g.setColor(Color.cyan);
		g.fillArc(x,y,2*radius,2*radius,90,45);
		g.setColor(Color.lightGray);
		g.fillArc(x,y,2*radius,2*radius,135,45);
		g.setColor(Color.orange);
		g.fillArc(x,y,2*radius,2*radius,180,45);
		g.setColor(Color.pink);
		g.fillArc(x,y,2*radius,2*radius,225,45);
		g.setColor(Color.yellow);
		g.fillArc(x,y,2*radius,2*radius,270,45);
		g.setColor(Color.magenta);
		g.fillArc(x,y,2*radius,2*radius,315,45);
	}
}
