package ce1002.E5.s101502007;
import javax.swing.JFrame;
import javax.swing.JPanel;
import java.awt.Graphics;
import java.awt.Color;
public class E5 extends JFrame {
	 public E5() {
		    setTitle("DrawArcs");
		    add(new ArcsPanel());
		  }

		  /** Main method */
		  public static void main(String[] args) {
		    E5 frame = new E5();
		    frame.setLocationRelativeTo(null); // Center the frame
		    frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		    frame.setSize(250, 300);
		    frame.setVisible(true);
		  }
		}

		// The class for drawing arcs on a panel
		class ArcsPanel extends JPanel {
		  // Draw four blazes of a fan
		  protected void paintComponent(Graphics g) {
		    super.paintComponent(g);

		    int xCenter = getWidth() / 2;
		    int yCenter = getHeight() / 2;
		    int radius = (int)(Math.min(getWidth(), getHeight()) * 0.4);

		    int x = xCenter - radius;
		    int y = yCenter - radius;
		    g.setColor(Color.black);
		    g.drawArc(x, y, 2 * radius, 2 * radius, 0, 360); 
		    g.setColor(Color.yellow);
		    g.fillArc(x, y, 2 * radius, 2 * radius, 0, 45); 
		    g.setColor(Color.white);
		    g.fillArc(x, y, 2 * radius, 2 * radius, 45, 45);
		    g.setColor(Color.black);
		    g.fillArc(x, y, 2 * radius, 2 * radius, 90, 45);
		    g.setColor(Color.RED);
		    g.fillArc(x, y, 2 * radius, 2 * radius, 135, 45);
		    g.setColor(Color.BLUE);
		    g.fillArc(x, y, 2 * radius, 2 * radius, 180, 45);
		    g.setColor(Color.GRAY);
		    g.fillArc(x, y, 2 * radius, 2 * radius, 225, 45);
		    g.setColor(Color.ORANGE);
		    g.fillArc(x, y, 2 * radius, 2 * radius, 270, 45);
		    g.setColor(Color.GREEN);
		    g.fillArc(x, y, 2 * radius, 2 * radius, 315, 45);
		  }
		}
	


