package ce1002.E1.s101502003;
import java.util.Scanner;
public class E12 {
	public static void main(String[] args)
	{
		Scanner input = new Scanner(System.in);
		System.out.print("a=");//輸出文字
		int a=input.nextInt();//輸入數字
		System.out.print("b=");
		int b = input.nextInt();
		int buffer;
		int c=a;//c=a,用來存原本a的數值
		int d=b;//b=d,用來存原本b的數值
		if(a>b)//先排順序由小排到大
		{
			buffer=a;
			a=b;
			b=buffer;
		}
		if(b%a==0)//如果餘數=0,最大公因數=比較小的數
			System.out.println("gcd("+c+","+d+")="+a);
		while(b%a!=0)//如果餘數不=0,用輾轉相除法
		{
			int x=b%a;
			buffer=x;
			x=a;
			a=buffer;
			if(b%a==0)
			{
				System.out.print("gcd("+c+","+d+")="+a);
				break;
			}
		}
	}
}
