package ce1002.E1.s101502018;
import java.util.*;
public class E11 {
	public static void main(String[] args){
		Scanner a=new Scanner(System.in);
		double c,p,x,y,z=0;//x最後答案,y可換到的糖果,z多出來的紙
		System.out.print("How many candy per warping paper = ");
		p=a.nextDouble();
		System.out.print("Number of candy = ");
		c=a.nextInt();
		x=c;
		while((c+z)*p>=1){//若糖果數加上多出來的紙超過1,則可再換
			y=(int)((c+z)*p);
			z=c+z-y/p;
			c=c*p;
			x=x+c;
		}
		System.out.print("Number of candy which will be eaten = "+(int)x);
	}
}
