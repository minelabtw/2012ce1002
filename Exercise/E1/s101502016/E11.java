package ce1002.E1.s101502016;

import java.util.*;

public class E11 {
		public static void main(String[] args){
	
		Scanner imput = new Scanner(System.in);
		double candy,paper,a,x;//x用來儲存一開始candy的值
		double output=0;

		System.out.print("Number of candy = ");
		candy = imput.nextDouble();
		System.out.print("How many candy per warping paper = ");
		paper = imput.nextDouble();
		paper = (int)(1/paper);//轉換型態
		
		x = candy;
		do{
			output += (int)(candy/paper);
			a = (int)(candy/paper)+(int)(candy%paper);//餘數加上轉換的次數
			candy = a;
		}while(candy/paper>=1);
		output += x;
		
		System.out.print("Number of candy which will be eaten = "+(int)output);
		
	}
}
