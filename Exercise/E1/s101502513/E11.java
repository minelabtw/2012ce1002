package ce1002.E1.s101502513;

import java.util.Scanner;

public class E11 {
	public static void main(String[] args) {
		float Candy, CandyPaper = 0, BonusSum = 0;
		Scanner input = new Scanner(System.in);
		
		System.out.print("Number of candy = ");
		float candy = input.nextFloat();
		System.out.print("How many candy per warping paper = ");
		float bonus = input.nextFloat();
		
		Candy = candy;
		CandyPaper = (float)(candy * bonus);
		
		while( CandyPaper >= 1 ) {
			CandyPaper = (float)(candy * bonus);
			BonusSum = BonusSum + CandyPaper;
			candy = CandyPaper;
		}
		
		System.out.println("Number of candy which will be eaten = " + (int)(Candy + BonusSum) );
	}
}
