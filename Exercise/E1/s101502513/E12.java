package ce1002.E1.s101502513;

import java.util.Scanner;

public class E12 {
	public static void main(String[] args) {
		int buffer;
		Scanner input = new Scanner(System.in);
		
		System.out.print("a=");
		int a = input.nextInt();
		System.out.print("b=");
		int b = input.nextInt();
		
		int num1 = a ,num2 = b;
		
		while( num2 != 0 ) {
			buffer = num1 % num2;
			num1 = num2;
			num2 = buffer;
		}
		
		System.out.println("gcd(" + a + "," + b + ")=" + num1 );
	}
}
