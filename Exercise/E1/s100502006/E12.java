package ce1002.E1.s100502006;

import java.util.Scanner;

public class E12 {
	static Scanner input= new Scanner(System.in);
	public static void main(String[] args){
		int a=0,b=0,temp=0,M=0,m=0;
		System.out.print("a=");
		a=input.nextInt();
		System.out.print("b=");
		b=input.nextInt();
		M=a;
		m=b;
		while(m!=0){//calculate gcd
			temp=M;
			M=m;
			m=temp%m;
		}
		System.out.print("gcd("+a+","+b+")="+M);
	}
}
