package ce1002.s101502520;

import java.util.Scanner;

public class E11 {

	public static void main(String[] args) {		// TODO Auto-generated method stub
		
		System.out.print("Number of candy = ");
		Scanner inpuScanner = new Scanner(System.in);
		int c = inpuScanner.nextInt();
		System.out.print("How many candy per warping paper = ");
		float p = inpuScanner.nextFloat();
		inpuScanner.close();
		float last = c;
		c = c + (int)(last * p);
		while(last * p >=1)
		{
			last = last + (int)(last * p) * (1 - 1 / p);
			c = c + (int)(last * p);//p*c is the amount of free candy
		}
		
		System.out.print("Number of candy which will be eaten = " + c);
	}

}
