package ce1002.s101502520;

import java.util.Scanner;

public class E12 {
	public static int gcd(int a, int b) {//����۰��k
		int r;
		while(b != 0)
		{
			r = a % b;
			a = b;
			b = r;
		}
		return a;
	}

	public static void main(String[] args) {
		int a, b;
		Scanner inpuScanner = new Scanner(System.in);
		System.out.print("a=");
		a = inpuScanner.nextInt();
		System.out.print("b=");
		b = inpuScanner.nextInt();
		int ans = gcd(a, b);
		inpuScanner.close();
		System.out.println("gcd(" + a + "," + b + ")=" + ans);
	}

}
