package ce1002.E1.s101502526;
import java.util.Scanner;
public class E12 {

	public static void main(String[] args) {
		int a , b , c = 0 ,mod=0 ;
		Scanner input = new Scanner(System.in);
		System.out.print("a=");
		a = input.nextInt();
		System.out.print("b=");
		b = input.nextInt();
		System.out.print("gcd("+a+","+b+")=");
		mod = a %b ;
		for (;mod> 1;)
		{
			if (a < b)               //先將大小置換位子
			{
				c = a;
				a = b;
				b = c;
			}
			mod = a %b ;            //餘數
			c = a/b;              
			a = mod ;               //a的值換為餘數
		}
		System.out.print(b);
	}

}
