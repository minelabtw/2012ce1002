package ce1002.E1.s101502028;
import java.util.*; // necessary to use scanner
public class E12 {
	public static void main (String[] args){
		Scanner space = new Scanner (System.in); // create a new scanner object
		int a, b;
		System.out.print("a = "); // message
		a = space.nextInt(); // input a
		System.out.print("b = "); // message
		b = space.nextInt(); // input b
		System.out.print("gcd(" + a + "," + b + ")=" + getGDC(a, b)); // show the result
	} // end main
	public static int getGDC(int no1, int no2){ // gdc function
		int buff;
		if (no2 > no1) // find the biggest number
		{
			buff = no2;
			no2 = no1;
			no1 = buff;
		}
		if (no2 == 0) // if the smaller number is 0
		{
			return no1; // gdc is the other number
		}
		else // else
			return getGDC(no2, no1 % no2); // return the gdc
	}
}
