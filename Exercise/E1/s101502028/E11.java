package ce1002.E1.s101502028;
import java.util.*; // necessary to use scanner
public class E11 {
	public static void main (String[] args){
		Scanner no = new Scanner (System.in); // new scanner object
		int candy, n;
		double paper;
		System.out.print("Number of candy = "); // message
		candy = no.nextInt(); // input number of candies
		System.out.print("How many candy per warping paper = "); // message
		paper = no.nextDouble(); // input number of papers
		paper = 1 / paper; // convert the double paper to int paper
		n = (int) paper; // force the double paper to int paper
		for (int i = 1; i <= candy; i++) // for loop to count the number candies
		{
			if (i == n) // if the number of candies is equal to the number of papers
			{
				candy++; // candies plus one
				n = n + (int) paper; // number of paper plus number of paper to get the next candy
			} // end if
		} // end for loop
		System.out.print("Number of candy which will be eaten = " + candy); // message
	} // end main
}
