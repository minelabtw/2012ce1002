package ce1002.E1.s101502510;

import java.util.Scanner;

public class E11 {
	public static void main(String args[]){
		Scanner input=new Scanner(System.in);
		
		System.out.print("Number of candy = ");
		double candy=input.nextDouble();
		System.out.print("How many candy per warping paper = ");
		float bonus=input.nextFloat();
		
		double a=0,b=0,c=0,d;
		d=candy;//記錄一開始的糖果數
		do{
			a=(double)(candy*bonus);//計算換幾顆糖果
			b=b+a;//紀錄換幾顆糖果
			candy=a;//將candy轉換成a
		}while(a>=1);
		
		System.out.print("Number of candy which will be eaten = "+(int)(d+b));
		
	}
}
