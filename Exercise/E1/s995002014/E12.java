//995002014 張力 E12
import java.util.Scanner;
public class E12 {
    
	static int gcd(int m, int n) {//輾轉相除法
        int r;
        while(n!= 0) { 
            r = m % n; 
            m = n; 
            n = r; 
        }
        return m;
    }
	
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
		System.out.println("a=");
		int a = in.nextInt();
		System.out.println("b=");
		int b = in.nextInt();
		int m = gcd(a, b);
		System.out.println("gcd("+a+","+b+")="+m);     
    }
}