package ce1002.E1.s101502022;
import java.util.*;

public class E12 {
	
	public static void GCD(String[] args){
		int a,b,c,d,e;
		
		Scanner x= new Scanner(System.in);
		
		System.out.print("a=");
		a=x.nextInt();
		System.out.print("b=");
		b=x.nextInt();
		
		d=a;
		e=b;
		while(b!=0){
			c=a%b;
			a=b;
			b=c;
		}
		
		System.out.print("gcd("+d+","+e+")="+a);	
		
	}
	
	public static void main(String[] args){
		GCD(args);
	}

}
