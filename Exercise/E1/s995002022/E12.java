package ce1002.E1.s995002022;

import java.util.Scanner;

public class E12 {

	static int gcd(int m, int n) {
        int r;
        while(n != 0) { 
            r = m % n; 
            m = n; 
            n = r; 
        }
        return m;
    }
	
	public static void main(String[] args) {

		Scanner input = new Scanner(System.in);

		System.out.print("a=");
		
		int a = input.nextInt();
		
		System.out.print("b=");
		
		int b = input.nextInt();
		int ans;
		int m = gcd(a, b);
		
		ans=m;
		
		
		System.out.println("gcd("+a+","+b+")="+ans);
	}
}