package ce1002.s101502519;

import java.util.Scanner;

public class E11 {
	public static void main(String[] args){
	
		int c1,c2,a;
		double p;
		
		Scanner input = new Scanner(System.in);
		
		System.out.print("Number of candy = ");
		c1=input.nextInt();           //輸入糖果數
		System.out.print("How many candy per warping paper = ");
		p=input.nextDouble();         //數入一張包裝紙可以換幾顆糖果
		
		System.out.print("Number of candy which will be eaten = ");
		
		a=(int)(1/p);
		c2=c1;
		
		while(c1>=a){        //算糖果數
			c2= c2+c1/a;
			c1=	c1/a+c1%a;		
		}

		System.out.print(c2);
	}
}