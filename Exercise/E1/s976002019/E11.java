package ce1002.E1.s976002019;

import java.util.Scanner;

public class E11 {
	public static void main(String arg[]){
		int c, temp, temp2,result; /*宣告變數 糖果, 暫存, 結果*/
		double p, remind; /*一張包裝紙可換多少顆糖果*/
		System.out.print("Number of candy=");
		Scanner input1 = new Scanner(System.in);
		c=input1.nextInt(); /*輸入糖果樹*/
		System.out.print("How many candy per warping paper=");
		Scanner input2 = new Scanner(System.in); /*輸入p*/
		p=input2.nextDouble();
		temp= (int) (c*p); /*初始包裝紙可換糖果樹*/
		remind= c*p-temp;/*剩餘初始包裝紙數量*/
		result=c; /*初始結果*/
		
		while(temp>0){ /*若包裝紙數量夠則繼續換糖果*/
			result+=temp;/*結果加上包裝紙所換得糖果*/
			temp2=temp;
			temp=(int)(temp*p); /*所換得糖果的包裝紙數量*/
			remind+=temp2*p-temp;
			if(remind>1){
				temp=(int)(temp+remind);
				remind=remind-Math.ceil(remind);
			}
		}		
		
		System.out.print("Number of candy which will be eaten ="+result);
		
		
	}

}