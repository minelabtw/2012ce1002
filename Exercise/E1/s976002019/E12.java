package ce1002.E1.s976002019;

import java.util.Scanner;

public class E12 {
	public static void main(String arg[]){
		int a, b;
		
		System.out.print("a=");
		Scanner input1 = new Scanner(System.in);/*輸入a*/
		a=input1.nextInt();
		System.out.print("b=");
		Scanner input2 = new Scanner(System.in);/*輸入b*/
		b=input2.nextInt();
		System.out.print("gcd("+a+","+b+")="+gcd(a,b));/*印出結果*/
		
		
	}
	
	static int gcd(int a, int b){
		if(a-b==0){ /*回傳最大公因數*/
			return a;
		}
		else if (a-b<0){ /*左右交換計算*/
			return gcd(b, a);
		}
		else{				
		
		return gcd(a-b, b); 
		}
	}

}
