package ce1002.E1.s101502010;

import java.util.Scanner;

public class E12 {
	public static void main(String[] args){
		System.out.print("a=");
		Scanner input = new Scanner(System.in);
		int a=input.nextInt();
		System.out.print("b=");
		int b=input.nextInt();
		System.out.print("gcd("+a+","+b+")=");
		while(a!=0){
			if(a>b){
				int tmp=a;
				a=b;
				b=tmp;
			}
			b-=a;
		}//end of while
		System.out.println(b);
	}//end of main
}
