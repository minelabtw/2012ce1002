package ce1002.E1.s101502010;

import java.util.Scanner;

public class E11 {
	public static void main(String[] args){
		System.out.print("Number of candy = ");
		Scanner input = new Scanner(System.in);
		int num=input.nextInt();
		System.out.print("How many candy per warping paper = ");
		double rate=input.nextDouble();
		int tmp=num,times=(int)(1/rate);
		while(tmp>=times){
			num+=(int)(tmp*rate);
			tmp=(tmp/times)+tmp%times;
		}//end of while
		System.out.print("Number of candy which will be eaten = "+num);
	}//end of main
}
