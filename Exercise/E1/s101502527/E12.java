package ce1002.E1.s101502527;

public class E12 {
	public static void main(String[] args){
		java.util.Scanner scan = new java.util.Scanner(System.in);
		
		System.out.print("a=");
		int a=scan.nextInt();
		System.out.print("b=");
		int b=scan.nextInt();
		
		System.out.print("gcd("+a+","+b+")=");
		
		if(b>a){
			int swi=a;
			a=b;
			b=swi;
		}
		
		while(!(a==0||b==0)){
			a%=b;
			int swi=a;
			a=b;
			b=swi;
		}
		
		System.out.println(a);
		
	}

}
