package ce1002.E11.s101502023;

import java.util.*;

public class E12 
{
    public static void main(String[] args)
    {
    	int c;
    	int x;//x is the remainder of a divided by b.
    	Scanner input=new Scanner(System.in);
    	
    	System.out.print("a=");
    	int a=input.nextInt();
    	System.out.print("b=");
    	int b=input.nextInt();
    	System.out.print("gcd("+ a +","+ b +")=");
    	
    	if(a<b){
    		x=a;
    		a=b;
    		b=x;}//let a is bigger than b forever.
    	c=a%b;
    	while(c!=0)
    	{
    		a=b;
    	    b=c;
    	    c=a%b;
    	}//to calculate the GCD.    
        System.out.print(b);
    }
}
