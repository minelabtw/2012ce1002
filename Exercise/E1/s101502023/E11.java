package ce1002.E11.s101502023;

import java.util.Scanner;

public class E11 
{
    public static void main(String[] args)
    {
    	int c;
    	int change;//How many candies can be changed?
    	Scanner input= new Scanner(System.in);  	
    	System.out.print("Number of candy = ");
    	c=input.nextInt();    	
    	
    	System.out.print("How many candy per warping paper = ");
     	double p=input.nextDouble(); 
     	double output;
     	
     	if(c%2==0){
            c=c;
         	change=(int)(c*p);
         	output=c;}//if you have the even candies, you will do the loop.
     	else{
     		c=c-1;
         	change=(int)(c*p)+1;
         	output=c;}//if you have the odd candies, you will do the loop.
     	double d=(change*p);
 		while(d>=1)
     	{
	     	change=(int)(d+change);
	     	d=(d*p);
     	}
        output=output+change;//the last candies can be got.
     	System.out.print("Number of candy which will be eaten = " +(int)output );      	 	
    }
}
