package ce1002.E1.s101502522;

import java.util.Scanner;
public class E12 {
	public static void main(String[] args){
		Scanner input=new Scanner(System.in);
		System.out.print("a=");
		int a=input.nextInt();
		System.out.print("b=");
		int b=input.nextInt();
		System.out.print("gcd("+a+","+b+")"+"="+gcd(a,b));
	}
	
	private static int gcd(int m, int n){  //最大公因數函式
		int r = 0; 
		while(n != 0){
			r = m % n;
			m = n;
			n = r;
		}
			return m;
	}
}
