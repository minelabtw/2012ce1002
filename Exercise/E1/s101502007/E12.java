package ce1002.s101502007;

import java.util.Scanner;

public class E12 {
	public static void main(String args[])
	{
		Scanner input=new Scanner(System.in);
		System.out.print("a=");
		int a=input.nextInt();
		System.out.print("b=");
		int b=input.nextInt();
		int d=a;
		int e=b;
		if (a>b)
		{
			int c=a;
			a=b;
			b=c;
		}
		int max=0;
		for (int i=1;i<=a;i++)
		{
			if (a%i==0 && b%i==0)
			{
				if (i>max)
					max=i;
			}
		}
		System.out.print("gcd("+d+","+e+")="+max);
	}
}
