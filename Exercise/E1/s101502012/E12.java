package ce1002.E1.s101502012;

import java.util.Scanner;

public class E12 {
	public static void main ( String[] args ){
		Scanner input = new Scanner(System.in);
		int a,b;
		while (true)
		{
			System.out.print("a=");
			a = input.nextInt();
			System.out.print("b=");
			b = input.nextInt();
			int ans=1,c=1;
			while (true)
			{
				if ((0==a%c)&&(0==b%c))
					ans=c;
				if ((c==a)||(c==b)) //當除數到a或b時 break迴圈
					break;
				c++;
			}
			System.out.println("gcd(" +a +"," +b +")=" +ans +"\n");
		}
	}
}