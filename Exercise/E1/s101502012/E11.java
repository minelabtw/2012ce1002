package ce1002.E1.s101502012;

import java.util.Scanner;

public class E11 {
	public static void main (String[] args){
		while (true)
		{
			double n,p;
			Scanner input = new Scanner(System.in);
			System.out.print("Number of candy = ");
			n = input.nextDouble();
			System.out.print("How many candy per warping paper = ");
			p = input.nextDouble();
			int ans=(int)n,bonus=2;
			int maxn=0,remain=0,b=ans;
			while (bonus!=0)
			{
				bonus=(int)(b*p);
				for (int i=1;i<=b;i++) //找出最有效益的給糖果數
				{
					if(((int)(i*p))>((int)((i-1)*p)))
						maxn=i;
				}
				remain=b-maxn; //剩餘糖果數
				b=remain+bonus;
				ans+=bonus;
			}

			System.out.print("Number of candy which will be eaten = " +ans +"\n\n");
		}
	}
}
