package ce1002.E1.s101502512;

import java.util.Scanner;

public class E11 {
	public static void main(String[] args) {

		Scanner input = new Scanner(System.in);
		System.out.print("Number of candy = " );
		int c = input.nextInt();// 最先吃掉的糖果
		
		System.out.print("How many candy per warping paper = " );
		float p = input.nextFloat();// 抽到的獎勵
		
		if (p >= 1 || p <= 0) {
			System.out.println("Error");
		}

		int sum = 0;// 可換得幾個糖果
		float candy = c;
		float x;//沒用到的糖果紙
		while (candy * p >=1) {
			sum += candy * p;
			x=((candy * p)%1)/p;//剩幾張糖果紙
			candy = (int) (candy * p)+x;//換來的跟沒有用的糖果紙總和			
		}


		System.out.println("Number of candy which will be eaten = "
				+ (int) (c + sum));
	}
}
