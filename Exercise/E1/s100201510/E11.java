package ce1002.E1.s100201510;

import java.util.Scanner;

public class E11 {
	public static void main(String[] args) {
		System.out.printf("Number of candy = ");
		int c = new Scanner(System.in).nextInt(); // c is initial number of candy
		
		System.out.printf("How many candy per warping paper = ");
		double p = new Scanner(System.in).nextDouble(); // p is the exchange rate
		
		//terminate if user can get infinite number of candy.
		if( 1.0/p <= 1 || p == 0)
			System.exit(1);
		
		System.out.printf("Number of candy which will be eaten = %1.0f" , bouns(c , p) );
	}
	
	//method to calculate total candy number.
	public static int bouns( double candynumber , double rate ){
		double candypaper = candynumber ;
		double get = 0;
		
		while(candypaper * rate >= 1){ // see if we can get more candy
			get = Math.floor(candypaper * rate); // candy we get in an exchange.
			candynumber += get;
			candypaper = candypaper - get/rate + get;
		}
		
		return (int)Math.floor(candynumber);
	}
}
