package ce1002.E1.s101502508;

import java.util.Scanner ;

public class E12 {
	public static void main(String args[]){
		Scanner input = new Scanner(System.in) ;
		int a = input.nextInt();
		int b = input.nextInt();
		
		int c ;
		
		if(a>b)
			c=b ;
		else
			c=a ;
		
		while( c>0 ){
			if( a%c==0 && b%c==0 ){
				break ;
			}
			
			c-- ;	
		}
		
		System.out.print("gcd(" + a + "," + b + ")=" + c) ;
	}

}
