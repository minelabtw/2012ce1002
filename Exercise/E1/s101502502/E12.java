package ce1002.E1.s101502502;

import java.util.Scanner;

public class E12 
{
	public static void main(String[] args)
	{
		Scanner cin = new Scanner(System.in);
		int c = 0;
		
		System.out.print("a=");
		int num1 = cin.nextInt();
		System.out.print("b=");
		int num2 = cin.nextInt();
		
		int a = num1;
		int b = num2;
		
		while(b != 0)
		{
			c = a % b;
			a = b;
			b = c;
		}
			
		
		System.out.print("gcd(" + num1 + "," + num2 + ")=" + a);
		
	}
}
