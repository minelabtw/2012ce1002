package ce1002.E1.s101502514;

import java.util.Scanner;

public class E12 {
	
	public static int GCD(int c,int d){
		int min=0;
		int buffer;
		if(c<d){//讓c比d大
			buffer=c;
			c=d;
			d=buffer;
		}
		for(int i=1;i<=c;i++){
			if(c%i==0&&d%i==0){//找最大公因數\
				min=i;
			}
		}
		return min;
	}
	public static void main(String[] args){
	int a,b;
	System.out.print("a=");
	Scanner input=new Scanner(System.in);
	a=input.nextInt();
	System.out.print("b=");
	b=input.nextInt();
	
	System.out.println("gcd("+a+","+b+")="+GCD(a,b));
	
	
	
	}
}
