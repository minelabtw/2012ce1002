package ce1002.E1.s101502515;

import java.util.Scanner;

public class E12 {
	static int gcd=0;//GCD的答案
	public static int GCD(int a,int b){//堆疊
		if(a%b==0){
			gcd=b;
		}
		else{
			GCD(b,a%b);}//b>a%b 再代入
		return gcd;//符合a%b==0 答案
	}
	public static void main(String args[]){
		Scanner input = new Scanner(System.in);
		System.out.print("a=");
		int a = input.nextInt();
		System.out.print("b=");
		int b = input.nextInt();
		System.out.print("gcd("+a+","+b+")="+GCD(a,b));
	}

}
