package ce1002.E1.s101502515;

import java.util.Scanner;

public class E11 {
	static int set(double a,double b){//a=candy b=rate
		int sum=(int)a;
		double c;
		for(;;){
			if((a*b)<1)//換出來<1 跳出
				break;
			sum+=(int)(a*b);//增加糖果數
			int get=(int)(a*b);//建立包裝紙數量
			c=(a*b);
			c-=(int)(a*b);
			a=c*(1/b);//剩餘張數
			a+=get;//加上包裝紙
		}
		return (int)sum;
	}
	public static void main(String args[]) {
		Scanner input = new Scanner(System.in);
		System.out.print("Number of candy = ");
		int buy = input.nextInt();
		System.out.print("How many candy per warping paper = ");
		double change = input.nextDouble();
		System.out.print("Number of candy which will be eaten = "+set(buy,change));
		}
}
