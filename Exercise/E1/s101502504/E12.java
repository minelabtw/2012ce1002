package ce1002.E1.s101502504;
import java.util.Scanner;
public class E12
{
	public static void main(String[] args)
	{
		Scanner input = new Scanner(System.in);
		
		System.out.print("a = ");
		int a = input.nextInt();//user input the value of a
		
		System.out.print("b = ");
		int b = input.nextInt();//user input the value of b

		int x=a,y=b;//avoid the value of a and b change
		if(x<y)//let x > y
		{
			int buffer = x;
			x = y;
			y= buffer;
		}
		
		int re=x%y;//the remainder of a/b == x/y
		
		while(re!=0)//loop to find GCD of them
		{
			x=y;
			y=re;
			re=x%y;
		}
		
		System.out.print("gcd("+a+","+b+")="+y);//show tje answer
	}
}