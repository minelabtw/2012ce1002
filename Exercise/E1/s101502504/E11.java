package ce1002.E1.s101502504;
import java.util.Scanner;
public class E11 
{
	public static void main(String[] args)
	{
		Scanner input = new Scanner(System.in);
		
		System.out.print("Number of candy = ");
		int candy = input.nextInt();//user input candy amount 
		
		System.out.print("How many candy per wraping paper = ");
		float p = input.nextFloat();//user input the probability
	
		float change=1 , n = 1/p;
		int amount =candy , rest=1;
		
		change = candy/n;//
		while( (candy/n) >= 1 )//loop until (candy/n) < 1
		{
			rest = candy - (int)(n*(int)change);//everytime the rest candy that don't change
			candy = (int)change + rest;//candy = rest + change
			amount += (int)change;//amount is the total candy that eat
			change = candy/n;
		}
		System.out.print("Number of candy which will be eaten = "+amount);
	}
}