package ce1002.E1.s101502026;

import java.util.Scanner;

public class E11 
{
	public static void main(String[] args)
	{
		while(true)
		{
			Scanner scanner = new Scanner(System.in);
			float rate;
			int candy_sum=0,candy_initial,candy_exchange,candy_left;
			System.out.print("Number of candy = ");
			candy_initial = scanner.nextInt();
			int candy_initial_store=candy_initial;
			System.out.print("How many candy per warping paper = ");
			rate = scanner.nextFloat();
			while(candy_initial*rate>=1) //this is the loop to calculate how much candy the customers can get
			{
				candy_exchange=((int)(candy_initial*rate));
				candy_sum=candy_sum+candy_exchange;
				candy_left=candy_initial-((int)((candy_exchange*10)/(rate*10)))+candy_exchange;
				candy_initial=candy_left;
			}
			System.out.print("Number of candy which will be eaten = "+(candy_sum+candy_initial_store)+"\n\n");
		}
	}
}
