package ce1002.E1.s101502505;
import java.util.Scanner;

public class E11 {
	public static void main(String[] args){
		Scanner input = new Scanner(System.in);
		int total = 0;
		double per,candy,gain = 0,left = 0;
		System.out.print("Number of candy = ");
		candy = input.nextInt();
		System.out.print("How many candy per warping paper = ");
		per = input.nextDouble();
		total = (int) candy;
		do
		{
			total = (int)(gain + total);
			gain = candy * per;
			left = candy % (1 / per);
			candy = (int)(gain + left);
			
		}while(candy >= (1 / per));
		
		System.out.print("Number of candy which will be eaten = " + (int)(total+gain));
	}
}
