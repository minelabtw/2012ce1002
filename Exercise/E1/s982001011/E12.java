package ce1002.s982001011;

import java.util.Scanner;

class MyMath{
	public static int gcd( int a, int b ){

		if( a%b == 0 ){         //a被b整除
			return b;
		}else{
			a = a % b;          //取餘數
			return gcd( b, a );
		}

	}
}

public class E12 {
	public static void main(String[] args){

		Scanner input = new Scanner(System.in);
		System.out.print("¿é¤JA = ");
		int A = input.nextInt();            //讀入A B

		System.out.print("¿é¤JB = ");
		int B = input.nextInt();

		System.out.print("GCD(A,B) = " + MyMath.gcd(A, B) );
	}


}
