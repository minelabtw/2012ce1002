package ce1002.s982001011;

import java.util.Scanner;

class CandyCustomer{
	public float changeCoef;   //交換比率
	public int eatenCandy;      //貪吃鬼
	public int candyWrap;       //包裝紙
	public CandyCustomer( int firstCandy, float coef ){     //讀入
		eatenCandy = firstCandy;
		candyWrap = firstCandy;
		changeCoef = coef;
	}
	public boolean tradable(){
		return candyWrap * changeCoef >= 1;  //boolean
	}
	public void tradeAndEat(){
		int tradeNum = (int) (candyWrap * changeCoef);  //應得糖果
		candyWrap -= (int)(tradeNum / changeCoef);      //剩下的包裝紙
		eatenCandy += tradeNum;                         //以吃下
		candyWrap += tradeNum;
	}
	public int getEaten(){
		return eatenCandy;
	}
}

public class E11 {


	public static void main(String[] args){

		Scanner input = new Scanner(System.in);

		System.out.print("Number of candy = ");    //input  數據
		int candy = input.nextInt();

		System.out.print("How many candy per warping paper = ");
		float extra = input.nextFloat();


		CandyCustomer cc = new CandyCustomer( candy, extra );
		while( cc.tradable() ){
			cc.tradeAndEat();
		}

		System.out.print("Number of candy which will be eaten = " + cc.getEaten() );
	}

}
