package ce1002.E1.s101502005;

import java.math.BigInteger;//使用資料庫裡的求最大公因數
import java.util.Scanner;
public class E12 {
	public static void main(String[]args){
		
		Scanner input = new Scanner(System.in);
		BigInteger a, b, c;
		
		System.out.print("a=");
		a = input.nextBigInteger();
		
		System.out.print("b=");
		b = input.nextBigInteger();
		
		c = a.gcd(b);
		
		String str = "gcd("+a+","+b+")="+c;
		System.out.print(str);	
	}
}
		


