package e1.s100502515;

import java.util.Scanner;

public class E11 {
	public static void main(String[] args) {
		E11 e11 = new E11();
		Scanner scanner = new Scanner(System.in);
		System.out.print("Number of candies = ");
		int c = scanner.nextInt();
		System.out.print("How many candies per wrapping paper = ");
		double p = scanner.nextDouble();
		e11.bonusCandy(c, p);
		scanner.close();

	}

	private void bonusCandy(int c, double p) {
		double count = 0;
		double temp = c;
		while (temp >= 1) {
			count += temp;
			temp = temp * p;// if there are wrapping paper left, keep counting.
		}
		System.out.println("Number of candies which'll be eaten = "
				+ (int) count);
	}
}
