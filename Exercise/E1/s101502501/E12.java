package ce1002.E1.s101502501;
import java.util.Scanner;
public class E12 {
	public static void main (String[] args){
		Scanner cin = new Scanner (System.in);
		System.out.print ("a=");
		int a = cin.nextInt();//輸入a
		System.out.print ("b=");
		int b = cin.nextInt();//輸入b
		System.out.print ("gcd("+a+","+b+")=");
		int c;
		while( b != 0 )//計算a.b的最大公因數
	    {
	        c = a%b;
	        a = b;
	        b = c;
	    }
		
		System.out.print (a);//輸出最大公因數
	}

}
