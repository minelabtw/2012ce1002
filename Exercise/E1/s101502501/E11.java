package ce1002.E1.s101502501;

import java.util.Scanner;
public class E11 {
	public static void main (String[] args){
		Scanner cin = new Scanner (System.in);
		System.out.print ("Number of candy = ");
		int c = cin.nextInt();//輸入顧客買幾顆糖果c
		System.out.print ("How many candy per warping paper = ");
		float p = cin.nextFloat();//輸入抽到包裝紙的浮點數p
		
		int allcandy=c;//最後有幾顆糖果(一開始是買幾顆就有幾顆)
		int n=0;//用來計算剩幾顆糖果
		int newcandy;//換來的糖果
		newcandy = (int)(c*p);
		while(newcandy>=1)
		{
			n=c-newcandy*(int)(1/p);//計算換完後還剩幾顆糖果
			if(n>0)//有剩的時候
			{
				allcandy = allcandy + newcandy;
				c=newcandy+n;//換到的糖果+原本剩下的
				newcandy=(int)(c*p);//再去換糖果			
			}
			else//糖果換完了
			{
				allcandy = allcandy + newcandy;
				c=newcandy;//換來的糖果
				newcandy=(int)(c*p);//再換的糖果
			}
		}
		System.out.print ("Number of candy which will be eaten = ");
		System.out.print (allcandy);//輸出顧客實際能獲得幾顆糖果
		
	}

}
