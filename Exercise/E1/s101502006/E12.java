package ce1002.E1.s101502006;

import java.util.Scanner;

public class E12 {
	public static void main(String[] args)
	{
		// create Scanner to obtain input from command window
		Scanner input = new Scanner( System.in ); 
		
		int a,b;
		
		System.out.print("a=");
		a=input.nextInt();
		
		System.out.print("b=");
		b=input.nextInt();
		
		System.out.print("gcd(" + a + "," + b + ")=" + GCD(a,b));
	}
	
	public static int GCD (int a,int b)
	{
		int c;
		
		//we dont assume that a is greater than b, because they will exchange each other automatically.
		while( b != 0 )
		{
			c = a%b;
			a = b;
			b = c;
		}
		return a;
	}
}