package ce1002.E1.s101502006;

import java.util.Scanner;

public class E11 {
	public static void main(String[] args)
	{
		// create Scanner to obtain input from command window
		Scanner input = new Scanner( System.in ); 
		float p;
		int c;
		
		System.out.print("Number of candy = ");
		c=input.nextInt();
		
		System.out.print("How many candy per warping paper = ");
		p=input.nextFloat();
		
		int temp=1,candy=c,left,record=0;
		
		while(temp>=1)
		{
			temp=(int)(candy*p);
			left=(int)(candy- (temp/p));
			candy=temp+left;
			record+=temp;
		}
		
		System.out.print("Number of candy which will be eaten = " + (c+record));
	}
}
