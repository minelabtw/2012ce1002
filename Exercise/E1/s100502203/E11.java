package ce1002.E1.s100502203;

import java.util.Scanner;

public class E11 {
	public static int Candy(int c, double p) {
		int total = 0;
		double tmp = c;
		while (tmp >= 1) {			// loop to calculate the total candy use tmp*p to
			total += (int)tmp;		// measure the number of exchanged candy each time
			tmp *= p;				// when tmp less than 1 , exit the loop
		}							// (in other words) not take "number<1" into account
		return total;
	}

	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);
		int c;
		double p;
		System.out.print("Number of candy = ");
		c = input.nextInt();
		System.out.print("How many candy per warping paper = ");
		p = input.nextDouble();
		System.out.printf("Number of candy which will be eaten = %d", E11.Candy(c, p));
	}
}
