package ce1002.E1.s101502528;

import java.util.Scanner;

public class E12 {
	public static void main(String[] args){
		int a,b,c;
		
		System.out.print("a=");
		Scanner input=new Scanner(System.in);
		a=input.nextInt();
		System.out.print("b=");
		b=input.nextInt();
		
		//避免數值變動提前輸出
		System.out.print("gcd("+a+","+b+")=");
		//避免數值變動提前輸出
		
		//計算最大公因數
		while( b != 0 ){
	        c = a%b;
	        a = b;
	        b = c;
	    }
		//計算最大公因數
		
		System.out.print(a);
	}
}
