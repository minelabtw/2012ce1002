package ce1002.E1.s101502021;
import java.util.Scanner;

public class E12 {
	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);
		int Input_A,Input_B;
		
		System.out.print("a=");
		Input_A=input.nextInt();
		System.out.print("b=");
		Input_B=input.nextInt();
		
		System.out.print("gcd("+Input_A+","+Input_B+")="+GCD(Input_A, Input_B));
	}
	
	//尋找最大公因數
	public static int GCD(int A,int B){
		int C;
		
		while(B!=0){
			C=A%B;
			A=B;
			B=C;
		}
		return A;
	}

}
