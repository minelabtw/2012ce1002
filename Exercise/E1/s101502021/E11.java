package ce1002.E1.s101502021;
import java.util.Scanner;

public class E11 {
	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);
		int Input_Buy,Bonus1=0,n;
		double Input_Multiple;
		
		System.out.print("Number of candy = ");
		Input_Buy=input.nextInt();
		System.out.print("How many candy per warping paper = ");
		Input_Multiple=input.nextDouble();
		
		if(Input_Multiple>0 && Input_Multiple <1){
			Input_Multiple=1/Input_Multiple;//將其倒數
			
			//如果包裝紙的張數夠換至少一顆糖果
			if(Input_Buy/Input_Multiple>=1){
				Bonus1+=Input_Buy/Input_Multiple;
				do{
					n=(int)(Bonus1%Input_Multiple);//將Bonus1%Input_Multiple整數化
					Input_Buy+=Bonus1;
					Bonus1+=n;
					Bonus1/=Input_Multiple;
					
				}while(Bonus1/Input_Multiple>=1);//當包裝紙換過的糖果所拿的包裝紙數又能夠換至少一顆糖果
				
				Input_Buy+=Bonus1;
			}
			
			System.out.print(Input_Buy);
		}
		
	}

}
