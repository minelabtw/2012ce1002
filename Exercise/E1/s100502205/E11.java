package ce1002.E1.s100502205;

import java.util.Scanner;

public class E11 {
	public static void main(String[] args) {
		Scanner cin = new Scanner(System.in);
		int c, n, r = 0;
		double p;
		System.out.print("Number of candy = ");
		c = cin.nextInt();
		System.out.print("How many candy per warping paper = ");
		p = cin.nextDouble();
		/* n = (int) Math.floor(1 / p); */
		r = c;
		while (c * p > 0) {
			int i, j = 0, mx = 0;
			for (i = c; i >= 0; i--) {
				if ((int) (i * p) >= mx) {
					mx = (int) (i * p);
					j = i;
				} else
					break;
			}
			if (mx == 0)
				break;
			r += mx;
			c = c - j + mx;
		}
		/*
		 * while (c >= n) { r += c / n; c = c / n + c % n; }
		 */
		System.out.println("Number of candy which will be eaten = " + r);
	}
}