package ce1002.E1.s100502205;

import java.util.Scanner;

public class E12 {
	public static int gcd(int x, int y) {
		int t;
		while (x % y != 0) {
			t = x;
			x = y;
			y = t % y;
		}
		return y;
	}

	public static void main(String[] args) {
		Scanner cin = new Scanner(System.in);
		int a, b;
		System.out.print("a=");
		a = cin.nextInt();
		System.out.print("b=");
		b = cin.nextInt();
		System.out.printf("gcd(%d,%d)=%d\n", a, b, gcd(a, b));
		cin.close();
	}
}
