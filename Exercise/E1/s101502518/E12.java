package CE1002.s101502518;

import java.util.Scanner;

public class E12 {
	public static void main(String[] args)
	{		
		Scanner input = new Scanner(System.in);
		System.out.print("a=");
		int a = input.nextInt();
		System.out.print("b=");
		int b = input.nextInt();
		
		int x=a,y=b,z;
		while(x!=0)
		{			
			if(x<y)
			{
				z=x;
				x=y;
				y=z;
			}
			x = x-y*(x/y);
		}
		System.out.print("gcd("+a+","+b+")="+y);	
	}
}
