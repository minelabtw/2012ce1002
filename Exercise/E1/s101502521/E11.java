package ce1002.s101502521;

import java.util.Scanner;

public class E11 {
	public static void main(String[] args)
	{
		double n;
		int m;
		Scanner input = new Scanner(System.in);
		System.out.print("Number of candy = ");
		m = input.nextInt();
		System.out.print("How many candy per warping paper = ");
		n = input.nextDouble();
		int ans = m; // 答案
		int need = (int)(1/n); // 算出真正需要的糖果紙數量
		while(((m%need)+(m/need))>=need)
		{
			ans+=(m/need);
            m=((m%need)+(m/need));//m 持續保存目前所剩的糖果紙數量
		}
		ans+=(m/need);
		System.out.println("Number of candy which will be eaten = "+ans);
	}
}
