package ce1002.E1.s101502014;
import java.util.Scanner;
public class E12 {
	public static int GCD(int x ,int y) //GCD函式為找出x跟y的最大公因數
	{
		int z = 1;
		while(z > 0)
		{
			z = x % y;
			x = y;
			y = z;
		}
		return x;
	}
	public static void main(String[] args) {
		Scanner cin = new Scanner(System.in);
		int a , b;
		while(true)
		{
			System.out.print("a=");
			a = cin.nextInt();
			System.out.print("b=");
			b = cin.nextInt();
			System.out.println("gcd(" + a + "," + b + ")=" + GCD(a , b));
		}
	}
}
