package ce1002.E1.s101502014;
import java.util.Scanner;
public class E11 {
	public static void main(String[] args) {
		Scanner cin = new Scanner(System.in); //cin定義為輸入的功能
		double p , c; //p為一張包裝紙可以換幾顆糖果 , c為顧客買幾顆糖果
		int ans; //ans為顧客吃掉的糖果數
		while(true)
		{
			System.out.print("How many candy per warping paper = ");
			p = cin.nextDouble();
			System.out.print("Number of candy = ");
			c = cin.nextDouble();
			ans = (int)c;
			while(c * p >= 1) //如果剩下的糖果紙換不到一顆糖果，則結束迴圈
			{
				double plus = ((c * p) - Math.floor(c * p)) / p; //plus為換不到糖果的包裝紙數，返回成糖果數
				ans -= (int)plus; //將顧客吃掉的糖果數扣掉plus
				c = Math.floor(c * p) + plus; //c為顧客後來增加的糖果數
				ans += (int)c; //顧客吃掉後來換到的糖果
			}
			System.out.println("Number of candy which will be eaten = " + ans);
		}
	}
}