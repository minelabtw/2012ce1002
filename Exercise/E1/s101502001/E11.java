package ce1002.E1.s101502001;
import java.util.Scanner;

public class E11 {
	public static void main(String[]args){
		Scanner input = new Scanner(System.in);
		System.out.print("Number of candy = ");
		int a = input.nextInt();
		System.out.print("How many candy per warping paper = ");
		double b = input.nextDouble();
		System.out.print("Number of candy which will be eaten = ");
		int c,d,e;
		int total=a;
		do{
			c=(int)(a*b);
			d=(int)(c/b);
			e=(int)(a-d);
			a=c+e;
			total=c+total;
		}while(c>1);
		System.out.println(total);
	}
}