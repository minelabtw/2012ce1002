package ce1002.E1.s101502523;
import java.util.Scanner;
public class E12 {
	public static void main(String[] args){
		Scanner input = new Scanner(System.in);
		int a, b, n, m, c=0, buffer=0;
		System.out.print("a=");
		a = input.nextInt();
		System.out.print("b=");
		b = input.nextInt();
		//reserve the value of a and b 
		n = a;
		m = b;
		//exchange the value 
		if(n<m){
			buffer = m;
			m = n;
			n = buffer;
		}
		//GCD calculation
		while(m!=0){
			c = n%m;
	        n = m;
	        m = c;
		}
		//output the solution
		System.out.print("gcd("+a+","+b+")="+n);
	}

}
