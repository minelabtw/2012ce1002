package ce1002.E1.s101502523;
import java.util.Scanner;
public class E11 {
	public static void main(String[] args){
		Scanner input = new Scanner(System.in);
		int c;
		double p, sum;
		//input number of the candy and the exchange rate the of the candy
		System.out.print("Number of candy = ");
		c = input.nextInt();
		System.out.print("How many candy per warping paper = ");
		p = input.nextDouble();
		//calculate the total number of candy that will be eaten
		sum = c;
		for(int i = 1 ; c*Math.pow(p,i) > 0 ; i++){
			sum = sum + (c*Math.pow(p,i));
		}			
		//output the solution
		System.out.println("Number of candy which will be eaten = " + (int)sum);
	}
}
