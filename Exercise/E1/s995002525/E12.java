package ce1002.E1.s995002525;
import java.util.Scanner;
public class E12 {
	public static void main(String[] args){
		Scanner number = new Scanner(System.in);
		int a,b;
		int gcd=0;		//initialize
		System.out.print("a=");
		a = number.nextInt();
		System.out.print("b=");
		b = number.nextInt();
		int x,y;
		x = a;
		y = b;
		while(a%b!=0 && b%a!=0){
			if(a>=b){
				a = a%b;
				gcd = a;
			}
			else if(a<b){
				b = b%a;
				gcd = b;
			}
		}
		System.out.println("gcd("+x+","+y+")="+gcd);
	}

}
