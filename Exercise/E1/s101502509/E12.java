package ce1002.E1.s101502509;

import java.util.Scanner;

public class E12 {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		int a,b;
		Scanner input = new Scanner(System.in);
		System.out.print("a=");
		a = input.nextInt();	//Get number a
		
		System.out.print("b=");
		b = input.nextInt();	//Get number b
		
		System.out.print("gcd(" + a + "," + b +")="+ gcd(a, b));	//print the greatest common divisor between a and b
	}
	public static int gcd(int num1, int num2) {	//find the greatest common divisor between num1 and num2
		int r = 1;
		while(r != 0) {
			r = num1 % num2;
			num1 = num2;
			num2 = r;		
		}	
		return num1;
	}
}
