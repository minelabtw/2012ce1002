package ce1002.E1.s101502509;

import java.util.Scanner;

public class E11 {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		int candy, paperint;
		double paperrate, paperdouble = 0;
		Scanner input = new Scanner(System.in);
		
		System.out.print("Number of candy = ");
		candy = input.nextInt();            //Get the candy number
	
		System.out.print("How many candy per warping paper = ");
		paperrate = input.nextDouble();     //Get the paper rate
		
		paperint = candy;       //initialize the warping paper
		while(paperint * paperrate > 0) {
			paperdouble += (paperint * paperrate) - (int)(paperint * paperrate);  //calculate the paper Double
			paperint = (int)(paperint * paperrate);    //calculate the paper Integral
			candy += paperint;       //calculate the candy number total
			
			if(paperdouble >= 1) {  //if your paper Double enough to change a candy,subtract one and change
				candy++;			//so your candy and warping paper will add one,
				paperint++;          
				paperdouble--; 
			}
		}
		System.out.println("Number of candy which will be eaten = "+ candy); //print actual candy you get
	}
}







