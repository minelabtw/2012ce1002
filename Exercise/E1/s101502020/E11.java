package ce1002.E1.s101502020;

import java.util.Scanner ;

public class E11 {

	public static int candy (int c, double p , int s) {
		
		if ( (c%(1/p)+c*p) >= (1/p) )
			return candy( (int)(c%(1/p)+c*p) , p , (int)(s+(int)c*p) ) ;		//(left , ratio , total)
	
		return s+(int)(c*p) ;		//last time should be added in
	}

	
	public static void main(String[] args) {
		Scanner input = new Scanner(System.in ) ;
		
		System.out.print("Number of candy = ") ;
		int c=input.nextInt();
		System.out.print("How many candy per warping paper = ") ;
		double p=input.nextDouble() ;
		
		int sum=c ;
		
		System.out.print("Number of candy which will be eaten = " + candy(c,p,sum) ) ;
	}

}
