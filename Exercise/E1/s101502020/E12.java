package ce1002.E1.s101502020;

import java.util.Scanner; 

public class E12 {

	public static long GCD (long a , long b ) {
		long buff ;
		
		if (a<b){
			buff=a ;
			a=b ;
			b=buff ;
		}
		
		if (b==0)
			return a ;
				
		return GCD(b,a-b); 
	}
	
	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);
		
		System.out.print("a=");
		long a=input.nextLong() ;
		System.out.print("b=") ;
		long b=input.nextLong() ;
		
		System.out.print("gcd(" + a + "," + b + ")=" + GCD(a,b) ) ;
		
	}

}
