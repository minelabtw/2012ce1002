//E1-1
//An interesting loop question
//n=1/p
package ce1002.E1.s100502201;
import java.util.Scanner;

public class E11 {
	public static void main(String[] args)
	{
		Scanner cin=new Scanner(System.in);
		System.out.print("Number of candy = ");
		int c=cin.nextInt();
		Scanner cin1=new Scanner(System.in);
		System.out.print("How many candy per warping paper = ");	
		double p = cin1.nextDouble(); 
		int r=c;
		int b=c;
		int res=0;
		while (r>0)
		{
			b=r;
			int rew=(int)(b*p);
			if(rew==0)
				break;
			r=0;
			while(rew<=b*p) //Saved candies and rewarded candies
			{
				b--;
				if(rew>b*p)
				{
					b++;
					break;
				}
				r++;
			}
			if(b+r==c) //Plus first time remaining candies
				c=b;
			r+=rew;
			res+=r;
		}
		System.out.print("Number of candy which will be eaten = " + (res+c));
	}
}
