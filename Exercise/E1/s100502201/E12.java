//E1-2
//gcd
package ce1002.E1.s100502201;
import java.util.Scanner;

public class E12 
{
	public static void main(String[] args)
	{
		Scanner input = new Scanner(System.in);
		Scanner input1= new Scanner(System.in);
		System.out.print("a=");
		int a=input.nextInt();
		System.out.print("b=");
		int b=input.nextInt();
		int d=a;
		int e=b;
		int c;
		while( e != 0 ) //Algorithm
	    {
	        c = d%e;
	        d = e;
	        e = c;
	    }
		System.out.print("gcd("+a+","+b+")="+d);
	}
}
