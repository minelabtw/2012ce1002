package ce1002.E1.s101502002;

import java.util.Scanner;
public class E11 {
	public static void main(String[]args){
		
		System.out.print("Number of candy = ");
		Scanner input= new Scanner(System.in);//輸入原始糖果個數
		int a=input.nextInt();
		System.out.print("How many candy per warping paper = ");
		double b= input.nextDouble();//輸入一張紙可以換幾顆
		double c=a*b;//直接乘的結果
		int d=(int)(a*b);//實際可以換的個數

		while(d>=1){
			//還可以換糖果的情況下的迴圈

			int e=(int)((c-d)*(1/b));//原本多出來沒有拿去換的
			a=d+a;//所有可以吃的糖果
			c=(d+e)*b;
			d=(int)((d+e)*b);
		}
		
		System.out.println("Number of candy which will be eaten = "+a);//輸出結果
	}

}
