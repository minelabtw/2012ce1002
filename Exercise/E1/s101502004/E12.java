package ce1002.E1.s101502004;
import java.util.Scanner;

public class E12 {
	public static void main(String[] args){
		Scanner inp=new Scanner(System.in);
		System.out.println("a=");
		int a=inp.nextInt();
		System.out.println("b=");
		int b=inp.nextInt();
		
		int x=0,y=0,gcd=0;//x是被除數,y是除數
		if(a>b){
			x=a;
			y=b;
		}
		else{
			x=b;
			y=a;
		}
		while(x%y!=0){//迴圈停止於餘數為0
				gcd=x%y;
				x=y;
				y=gcd;
		}
		System.out.println("gcd("+a+","+b+")="+y);
	}
}
