package ce1002.E1.s101502516;
import java.util.Scanner;
public class E12 {
	public static void main (String[] ards){
	
		int a,b,c,button;
		int max = 0;
		
		System.out.print("a=");
		Scanner input1 = new Scanner(System.in);
		a = input1.nextInt();
		
		System.out.print("b=");
		Scanner input2 = new Scanner(System.in);
		b = input2.nextInt();
		
		System.out.print("gcd(" + a + "," + b +")=");
		if ( a > b ) //交換位置
		{
			button = a;
			a = b;
			b = button;
		}

		for ( int i = 1; i < b; i++ ) //找出最大公因數
		{
			if ( a % i == 0 && b % i == 0 )
			{
				c = i;
				if ( c > max )
				{
					max = c;
				}
			}
		}
		System.out.print(max);
	}
}
