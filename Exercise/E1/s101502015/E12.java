package ce1002.E1.s101502015;

import java.util.Scanner;

public class E12 
{
	public static void main(String[] args)
	{
		int a = 0 , b = 0,x,y,gcd = 0;
		Scanner n = new Scanner(System.in);
		System.out.println("a= ");
		a = n.nextInt();
		System.out.println("b= ");
		b = n.nextInt();
		x=a;
		y=b;
		while(a!=1 && b!=1 && a!=0 && b!=0)
		{
			if(a>b)//讓b變為較大的數
			{
				int buffer = a;
				a = b;
				b = buffer;
			}
			b=b%a;//輾轉相除法
			gcd = a;
		}
		
		System.out.println("gcd=(" + x + "," + y +")=" + gcd);
	}
}
