package ce1002.E4.s101502018;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.*;
import javax.swing.*;

public class E4 extends JFrame implements ActionListener 
{
	private JButton btn1 = new JButton("push");
	private JButton btn2 = new JButton("pop");
	private JTextField text1 = new JTextField(20);
	private JTextField text2 = new JTextField(20);
	private JLabel input =new JLabel("input");
	private JLabel output =new JLabel("result");
	private int numClicks = 0;
	static String[] stack = new String[100];
	static int length = 0;
	
	public static void main(String[] args) {

		E4 hw = new E4("Hello World App");

		hw.setSize(400, 300);

		hw.setLocation(250, 250);

		hw.setVisible(true);

	}

	public E4(String pTitle) {

		super(pTitle);

		setLayout(null);// 不使用版面配置
		input.setBounds(70, 20, 100, 20);
		output.setBounds(70, 40, 100, 20);
		btn1.setBounds(220, 60, 100, 20);
		btn2.setBounds(120, 60, 100, 20);
		text1.setBounds(120, 20, 200, 20);
		text2.setBounds(120, 40, 200, 20);
		
		btn2.setActionCommand("pop");
	    btn1.setActionCommand("push");

	    btn1.addActionListener(this);
	    btn2.addActionListener(this);
		add(btn1);	
		add(input);
		add(output);
		add(btn2);
		add(text1);
		add(text2);

	}

	public void actionPerformed(ActionEvent e) {
		String total="";
		String cmd = e.getActionCommand();
		if(cmd=="push")
		{
	    	stack[length] = text1.getText();
	    	stack[length+1]=",";
	    	length+=2;
		}
		else if(cmd=="pop")
		{
			stack[length-2]="";
			length-=2;
		}
		for(int i=0;i<length-1;i++)
		{
			total+=stack[i];
		}
		text2.setText(total);
	}	

}

