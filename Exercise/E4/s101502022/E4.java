package ce1002.E4.s101502022;
import java.util.*;
import javax.swing.*;

import java.awt.event.*;
import java.awt.Button;
import java.awt.FlowLayout;


public class E4 extends JFrame{
	private JTextField input = new JTextField(30);
	private JTextField result = new JTextField(29);
	static int a1=0;
	int y=0;
	String w,z;
	String[] array=new String[100];
	
	public E4(){
		setLayout(new FlowLayout(FlowLayout.CENTER,10,20));
		add(new JLabel("input"));
		input.setBounds(20, 40, 140, 20);
		add(input);
		add(new JLabel("result"));
		result.setBounds(20, 60, 160, 20);
		add(result);
		
		
		JButton jbtpush=new JButton("push");
		JButton jbtpop=new JButton("pop");
		JPanel panel=new JPanel();
		panel.add(jbtpush);
		panel.add(jbtpop);
		add(panel);
		
		pushListener listener1= new pushListener();
		popListener listener2= new popListener();
		jbtpush.addActionListener(listener1);
		jbtpop.addActionListener(listener2);
		
	}
	
	
	class pushListener implements ActionListener{
		
		public void actionPerformed(ActionEvent e) {
			 w=input.getText();
			array[a1]=w;
			a1=a1+1;
			z="";
			z=array[0];
			for(int i=1;i<a1;i++){
				
				z=z+','+array[i];
				
				
			}
			result.setText(z);
			
		}
		
	}
	
	class popListener implements ActionListener{

		public void actionPerformed(ActionEvent e) {
			if(a1!=0){
				a1--;
				
				z="";
				
				z=array[0];
				
				if(a1>0){
					for(int i=1;i<a1;i++){
						
						z=z+','+array[i];
						
						
					}
				}
				
				
				result.setText(z);
				
			}
			else
				result.setText("");
			
		}
		
	}
	
	
	
	public static void main(String[] args){
		E4 frame= new E4();
		frame.setTitle("E4");
		frame.setSize(400,300);
		frame.setLocationRelativeTo(null);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setVisible(true);
		
		
	}

}
