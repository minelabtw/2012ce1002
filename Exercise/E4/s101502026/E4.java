package ce1002.E4.s101502026;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;

public class E4 extends JFrame
{
	private JTextField field1 = new JTextField(40);
	private JTextField field2 = new JTextField(40);
	JButton button1 = new JButton("push");
	JButton button2 = new JButton("pop");
	
	private int numClicks=0;
	private int[] array = new int[10000]; //use an array sized 100 to build a stack
	private int array_number=0;
	
	public E4() //build three panels,two buttons,two labels,two textfields
	{
		JLabel label1 = new JLabel("input:");
		JLabel label2 = new JLabel("result:");
		
		JPanel panel1 = new JPanel();
		panel1.add(label1,BorderLayout.WEST);
		panel1.add(field1,BorderLayout.CENTER);
		
		JPanel panel2 = new JPanel();
		panel2.add(label2,BorderLayout.WEST);
		panel2.add(field2,BorderLayout.CENTER);
		
		add(panel1,BorderLayout.NORTH);
		add(panel2,BorderLayout.CENTER);
		
		JPanel panel3 = new JPanel();
		panel3.add(button1);
		panel3.add(button2);
		add(panel3,BorderLayout.SOUTH);
		
		button1.addActionListener(new ButtonListener());
		button2.addActionListener(new ButtonListener());
	}

	private class ButtonListener implements ActionListener //build ActionListener
	{
		public void actionPerformed(ActionEvent e)
		{
			if(e.getSource()==button1)
			{
				array[array_number]=Integer.parseInt(field1.getText());
				array_number++;
				String store="";
				for(int i=0;i<array_number;i++)
				{
					if(i!=0)
						store=store+",";
					store = store + Integer.toString(array[i]);
				}
				field2.setText(store);
			}
			else
			{
				field1.setText(""+array[array_number-1]);
				array_number--;
				String store="";
				for(int i=0;i<array_number;i++)
				{
					if(i!=0)
						store=store+",";
					store = store + Integer.toString(array[i]);
				}
				field2.setText(store);
			}
		}
	}
	
	public static void main(String[] args) //build a JFrame
	{
		JFrame frame = new E4();
		frame.setTitle("M1");
		frame.setSize(600,500);
		frame.setLocationRelativeTo(null);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setVisible(true);
	}
}