package ce1002.E4.s101502504;
import java.awt.TextField;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.*;
import java.util.Scanner;

public class E4 extends JFrame implements ActionListener 
{
	private static int[] array = new int[100];
	private int count =0;
	private JLabel l1 = new JLabel("input");
	private JLabel l2 = new JLabel("result");
	private TextField t1 = new TextField(100);//user input
	private TextField t2 = new TextField(100);//shoe the answer
	private JButton push = new JButton("push");
	private JButton pop = new JButton("pop");
	
	public static void main(String[] args)
	{
		E4 hw = new E4("M1");//setTitle
		hw.setSize(400, 300);
		hw.setLocation(250, 250);
		hw.setVisible(true);

	}

	public E4(String pTitle)//set things in JFrame
	{
		super(pTitle);
		setLayout(null);
		l1.setBounds(10, 40, 50, 20);
		l2.setBounds(10, 60, 50, 20);
		t1.setBounds(60, 40, 300, 20);
		t2.setBounds(60, 60, 300, 20);
		push.setBounds(100, 100, 100, 20);
		pop.setBounds(220, 100, 100, 20);
		
		push.addActionListener(this);
		pop.addActionListener(this);
		
		add(l1);
		add(l2);
		add(t1);
		add(t2);
		add(push);
		add(pop);
	}

	public void actionPerformed(ActionEvent e)//do operation and action on JTextField
	{
		if(e.getSource()==push)//click push
		{
			int a = Integer.valueOf(t1.getText());
			array[count] = a; 
			
			if( count==0 )//the first number of array
				t2.setText(""+array[count]);
			else//the rest of array
				t2.setText(t2.getText()+","+array[count]);
			
			count++;
		}
		
		else if(e.getSource()==pop)//click pop
		{
			count--;
			
			if(count==0)//stack is empty
				t2.setText("Stack is empty! Please pop some numbers!");
			
			for(int i=0; i<count; i++)
			{
				if( i==0 )//the first number of array
					t2.setText(""+array[i]);
				else//the rest of array
					t2.setText(t2.getText()+","+array[i]);
			}
		}
		
	}
	
}
