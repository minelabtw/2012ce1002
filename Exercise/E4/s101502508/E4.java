package ce1002.E4.s101502508;

import java.util.Scanner ;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JLabel ;
import javax.swing.JFrame ;


import javax.swing.*;

public class E4 extends JFrame implements ActionListener 
{
	String StackNumber="" ;
	String number ;
	String stack[] = new String [100] ;
	int n=0 ;
	
	private JLabel label1 ;
	private JLabel label2 ;
	private JButton btn = new JButton("Push");
	private JButton btn2 = new JButton("Pop");

	private JTextField text = new JTextField(20);
	private JTextField text2 = new JTextField(20);
	
	private int numClicks = 0;

	
	public static void main(String[] args) {

		E4 hw = new E4("M1");

		hw.setSize(400, 300);

		hw.setLocation(250, 250);

		hw.setVisible(true);

	}
	
	public void actionPerformed(ActionEvent e) {

		number = text.getText();
		
		if(e.getSource()==btn){
			StackNumber = StackNumber +" "+ number ;
			stack[n] = number ;
			n++ ;
		}
			
		
		if(e.getSource()==btn2){
			StackNumber="" ;
			System.out.println("45254");
			for ( int m=0 ; m<n-1 ; m++ ){
				StackNumber = StackNumber+ " " + stack[m] ;
			}
			n-- ;
		}
			
		
			
		//StackNumber = StackNumber + number ;
		
		text2.setText(StackNumber) ;

	}
	
	public E4(String pTitle) {

		super(pTitle);

		label1 = new JLabel ( "Input:"  ) ;
		label1.setBounds(20,40,40,20) ;
		label2 = new JLabel ( "Result:" ) ;
		label2.setBounds(20,70,40,20) ;
		
		setLayout(null);// 不使用版面配置
		btn.setBounds(120, 100, 65, 20);//左邊起始位置,高度位置,長,寬
		btn2.setBounds(210, 100, 65, 20);
        text.setBounds(65, 40, 300, 20);
        text2.setBounds(65, 70, 300, 20);
        
		btn.addActionListener(this);
		btn2.addActionListener(this);
		
		add(label1);
		add(label2);
        add(text);
        add(text2);
		add(btn);
		add(btn2);
		
	}

}
