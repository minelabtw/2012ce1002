package ce1002.A5.s101502005;
import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;

public class A5 extends JFrame implements ActionListener
{
	ImageIcon pic = new ImageIcon("src/ce1002/A5/s101502005/test-1.jpg");//將圖片加到按鈕
	ImageIcon pic2 = new ImageIcon("src/ce1002/A5/s101502005/test-2.jpg");
	ImageIcon pic3 = new ImageIcon("src/ce1002/A5/s101502005/test-3.jpg");
	ImageIcon pic4 = new ImageIcon("src/ce1002/A5/s101502005/test-4.jpg");
	
	JButton Jb = new JButton(pic);//設計一個button
	
	int count = 0;//count是用來計算按的次數
	public A5(){
		setLayout(new BorderLayout(80,80));
		add(Jb, BorderLayout.CENTER);//將圖片放在正中央
		Jb.addActionListener(this);
		
	}
	public static void main(String[] args)
	{
		A5 frame = new A5();
		frame.setTitle("A5");
		frame.setSize(400,400);
		frame.setLocationRelativeTo(null);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setVisible(true);
	}
	
	@Override
	public void actionPerformed(ActionEvent arg0) 
	{
		count++;//每次都+1次
		
		if(count%4==1)//如果除以4的餘數是1顯示pic2
		{
			Jb.setIcon(pic2);
		}
			
		if(count%4==2)//如果除以4的餘數是2顯示pic3
		{
			Jb.setIcon(pic3);
		}
			
		if(count%4==3)//如果除以4的餘數是3顯示pic4
		{
			Jb.setIcon(pic4);
		}
			
		if(count%4==0)////如果除以4的餘數是0顯示pic1
		{
			Jb.setIcon(pic);
		}
	}
	
}
