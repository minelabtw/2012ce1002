package ce1002.E4.s101502012;

import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JTextField;

public class E4 implements ActionListener{
	
    static JTextField inputText = new JTextField("", 29); //輸入的格子
    static JTextField resultText = new JTextField("", 29); //顯示結果的格子
	static String[] stack = new String[100];
	static int length = 0;
	
	public static void main(String[] args) {
		frame();
	}
	public static void frame(){ //整個視窗的函數
        JFrame frame = new JFrame();
        frame.setSize(400, 300);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.getContentPane().setLayout(new FlowLayout());
         
        JButton push = new JButton("push");
        JButton pop = new JButton("pop");
        JLabel input = new JLabel("input: ");
        JLabel result = new JLabel("result: ");
        
        pop.setActionCommand("pop"); //給予按鈕指令"pop"
        push.setActionCommand("push"); //給予按鈕指令 "push"
        E4 a = new E4();
		pop.addActionListener(a);
        push.addActionListener(a);
        
        frame.add(input);        
        frame.add(inputText);
        frame.add(result);
        frame.add(resultText);
        frame.add(push);
        frame.add(pop);
        frame.setVisible(true);
	}
	public void actionPerformed(ActionEvent e) {
		if (length<0) //偵錯 length<0時 陣列並沒有小於零的陣列 會錯誤
		{
			length=0;
		}
		String total="";
		String cmd = e.getActionCommand();
		if(cmd=="push")
		{
	    	stack[length] = inputText.getText();
	    	stack[length+1]=",";
	    	length+=2;
		}
		else if(cmd=="pop") 
		{
			if (length>=2) //偵錯 length<0時 陣列並沒有小於零的陣列 會錯誤
			{
				stack[length-2]="";
				length-=2;
			}
		}
		for(int i=0;i<length-1;i++)
		{
			total+=stack[i];
		}
		resultText.setText(total);
	}	
}
