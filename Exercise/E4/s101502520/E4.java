package ce1002.E4.s101502520;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

@SuppressWarnings("serial")
public class E4 extends JFrame{
	
	private String[] vol = new String[100];//using array and a counter to implement stack
	
	private int counter = 0;
	
	private JPanel main = new JPanel();
	private JLabel inputLabel = new JLabel("input: ");
	private JLabel resultLabel = new JLabel("result: ");
	private JTextField input = new JTextField();
	private JTextField result = new JTextField();
	private JButton pushButton = new JButton("push");
	private JButton popButton = new JButton("pop");
	
	public  E4()
	{
		iniUI();
	}
	public void iniUI()
	{
		main.setLayout(null);
		
		inputLabel.setSize(60, 30);
		resultLabel.setSize(60, 30);
		input.setSize(500, 30);
		result.setSize(500,30);
		pushButton.setSize(80, 30);
		popButton.setSize(80, 30);
		
		main.add(input);
		main.add(result);
		main.add(inputLabel);
		main.add(resultLabel);
		main.add(pushButton);
		main.add(popButton);
		
		inputLabel.setLocation(0, 0);
		input.setLocation(60, 0);
		resultLabel.setLocation(0, 40);
		result.setLocation(60, 40);
		pushButton.setLocation(0, 90);
		popButton.setLocation(100, 90);
		
		setDefaultCloseOperation(EXIT_ON_CLOSE);
		
		pushButton.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent arg0) {
				// TODO Auto-generated method stub
				push();
				reflash();
			}
		});
		
		popButton.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				// TODO Auto-generated method stub
				pop();
				reflash();
			}
		});
		
		setSize(600, 200);
		getContentPane().add(main);
		
		setVisible(true);
		
	}
	
	public void push()
	{
		if (counter == 99)
			return;
		String inputText = input.getText();
		if (inputText.equals(""))
			vol[counter] = "0";
		else
			vol[counter] = inputText;
		counter ++;
	}
	
	public void pop()
	{
		if (counter == 0)
			return;
		vol[counter - 1] = "";
		counter--;
	}
	
	public void reflash()
	{
		String resultString = vol[0];
		for (int i = 1; i < counter; i++)
			resultString += "," + vol[i];
		result.setText(resultString);
	}
	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		@SuppressWarnings("unused")
		E4 stackUI = new E4();
	}

}
