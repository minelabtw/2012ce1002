package ce1002.E4.s101502009;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.event.AncestorEvent;
import javax.swing.event.AncestorListener;
//extend繼承   JButton按鈕     JLabel標籤 JTextField輸入框 
public class E4 extends JFrame {
	private JButton btn_push;
	private JLabel label1;
	private JLabel label2;
	private JTextField txf_input;
	private JTextField txf_result;
	private JButton btn_pop ;
	private JTextField text = new JTextField(20);
	stack s;
	
public static void main(String[] args)
{
	E4 hw = new E4("E4");

	hw.setSize(600, 600);
	//大小

	hw.setLocation(250, 250);
	//位置

	hw.setVisible(true);
	//顯示視窗
}
//建構子
E4(String pTitle) {

	super(pTitle);
	s = new stack();
	setDefaultCloseOperation(EXIT_ON_CLOSE);
	btn_push = new JButton("push");//""內要顯示的字
	btn_pop = new JButton("pop");
	txf_input = new JTextField();
	txf_result = new JTextField();
	setLayout(null);// 不使用版面配置
	label1 = new JLabel("input:");
	label2 = new JLabel("result:");
	label1.setBounds(0, 0, 60, 20);
	label2.setBounds(0, 20, 60, 20);
	btn_push.setBounds(50, 40, 100, 20);
	btn_pop.setBounds(150,40,100,20);
	txf_input.setBounds(50, 0, 250, 20);
	txf_result.setBounds(50, 20, 250, 20);
	//btn.addActionListener(this);
	btn_push.addActionListener(new ActionListener() {
		
		@Override
		public void actionPerformed(ActionEvent e) {
			// TODO Auto-generated method stub
			try
			{
				int n = Integer.parseInt(txf_input.getText());
				s.push(n);
				txf_result.setText(s.show());
			}
			catch(Exception err)
			{
				System.out.println("invalid input");
				//err.printStackTrace();
			}
		}
	});
	

	
	btn_pop.addActionListener(new ActionListener() {
		
		@Override
		public void actionPerformed(ActionEvent e) {
			// TODO Auto-generated method stub
			s.pop();
			txf_result.setText(s.show());
			}
		});
		add(btn_pop);
		add(btn_push);
		add(label1);
		add(txf_input);
		add(txf_result);
		add(label2);

	}
	
}