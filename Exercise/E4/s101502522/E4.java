package ce1002.E4.s101502522;
import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Stack;

public class E4 extends JFrame implements ActionListener { 
	private JLabel Input = new JLabel("input:"); //宣告所有物件
	private JLabel Result = new JLabel("Result:");
	private JTextField jftInput = new JTextField(25);
	private JTextField jftResult = new JTextField(25);
	private JButton jbtPush = new JButton("push");
	private JButton jbtPop = new JButton("pop");
	private String stackData = new String();
	private Stack<String> stack = new Stack<String>();
	
	public E4()
	{
		setLayout(null); //不使用版面配置
		Input.setBounds(5,5,40,20); //調整物件位置
		jftInput.setBounds(50,5,250,20);
		Result.setBounds(5,30,40,20);
		jftResult.setBounds(50,30,250,20);
		jbtPush.setBounds(95,55,70,25);
		jbtPop.setBounds(175,55,70,25);
		
		add(Input); //增加物件到版面上
		add(jftInput);
		add(Result);
		add(jftResult);
		add(jbtPush);
		add(jbtPop);
		
		jbtPush.addActionListener(this); //激發按鈕功能
		jbtPop.addActionListener(this);
		
	}
	public static void main(String[] args)
	{
		E4 frame = new E4(); //設定版面
		frame.setTitle("M1");
		frame.setSize(350,300);
		frame.setLocationRelativeTo(null);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setVisible(true);
	}
	public void actionPerformed(ActionEvent click) //按鈕功能
	{
		int l=0; //儲存Stack位置
		if(click.getSource()==jbtPush) //push功能
		{
			stack.push(jftInput.getText());
			jftInput.setText(""); //清空"input:"列
		}
		
		if(click.getSource()==jbtPop)//pop功能
			if(!stack.empty()) //若stack內不是空的才push
				stack.pop();
		
		while(l<stack.size()) //將stack內的字串儲存相加
		{
			if(l==0)
				stackData=stackData+stack.elementAt(l);
				if(stackData.equals("")) //若stack是空的則無反應
					break;
			else
				stackData=stackData+","+stack.elementAt(l);
			l++;
		}
		l=0;
		
		jftResult.setText(stackData); 
		
		stackData="";
	}

}
