//字串陣列為同學教的
package ce1002.E4.s101502019;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.*;
import javax.swing.*;

		///////////////////////////////////////////////////////


public class E4 extends JFrame implements ActionListener {
	String numpush,numpop,add = "";
	private JButton btn = new JButton("push");
	private JButton btn2 = new JButton("pop");
	private JTextField text = new JTextField(20);
	private JTextField text2 = new JTextField(20);
	private JLabel lab = new JLabel("input:");
	private JLabel lab2 = new JLabel("result:");
	int count = 0;
	String[] array = new String[100];
	
	public static void main(String[] args) {

		E4 hw = new E4("E4");

		hw.setSize(500, 300);

		hw.setLocationRelativeTo(null);

		hw.setVisible(true);

	}

	public E4(String pTitle) {

		super(pTitle);

		setLayout(null);// 不使用版面配置

		lab.setBounds(20, 10, 60, 20);
		
		lab2.setBounds(20, 30, 60, 20);
		
		btn.setBounds(200,55, 70, 20);

		text.setBounds(70, 10, 400, 20);

		btn.addActionListener(this);
		
		btn2.setBounds(275, 55, 70, 20);

		text2.setBounds(70, 30, 400, 20);

		btn2.addActionListener(this);

		add(btn);

		add(text);
		
		add(btn2);

		add(text2);
		
		add(lab);
		
		add(lab2);
	}

	public void actionPerformed(ActionEvent e) {
		if(e.getSource()==btn){
			array[count] = text.getText();
			add = add + array[count] +",";
			text2.setText(add);
			count++;
		}
		else if(e.getSource()==btn2){
			array[count] = text.getText();
			count--;
			add = "";
			for( int i = 0;i < count;i++ ) {
					add = add + array[i] + ",";
			}
			text2.setText(add); 
		}
	}
}
