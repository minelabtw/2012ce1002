package ce1002.E4.s995002014;
import java.awt.*;
import java.awt.event.*;
import javax.swing.*;

public class E4 extends JFrame implements ActionListener {
	
	private JButton push_btn = new JButton("Push");
	private JButton pop_btn = new JButton("Pop");
	private TextField text1 = new TextField(50);
	public TextField text2 = new TextField(50);
	private JLabel lab1 =new JLabel("input");
	private JLabel lab2 =new JLabel("result"); 
	stack  stack = new stack();
	
	public static void main(String[] args){
		
		E4 hw = new E4("Hello World App");
		hw.setSize(400, 400);
		hw.setLocation(250, 250);
		hw.setVisible(true);
		hw.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	}
	
	public E4(String pTitle){
		
		super(pTitle);
		
        setLayout(new FlowLayout());
        
        push_btn.setActionCommand("push");
        push_btn.addActionListener(this);
        pop_btn.setActionCommand("pop");
        pop_btn.addActionListener(this);
        text1.addActionListener(this);
        
        add(lab1);
        add(text1);
        add(lab2);
        add(text2);
        add(push_btn);
        add(pop_btn);
	}
	
	public void actionPerformed(ActionEvent e) {
		String cmd = e.getActionCommand();
		String input = text1.getText();
		StringBuffer out = new StringBuffer();
		if (cmd == "push"){
			
			int round = Integer.parseInt(input);
			stack.push(round);
			if(stack.size<=0)
				text2.setText("stack is empty!");
			else {
				for(int i=0;i<stack.size;++i) 
					out.append(stack.stackdata[i]+" ");//加" "變成string ， 因為setText不能append 只好用stringbuffer
					text2.setText(out.toString()); 
			}
		}
		if (cmd=="pop"){
			stack.pop();
			if(stack.size<=0)
				text2.setText("stack is empty!");
			else{
				for(int i=0;i<stack.size;++i) 
					out.append(stack.stackdata[i]+" ");//加" "變成string ， 因為setText不能append 只好用stringbuffer
					text2.setText(out.toString());
			}
		}
	}
}


class stack {
	
    protected int[] stackdata;
	public int size;
	
    stack() { 
		stackdata= new int[100]; 
		size=0;
    }
    void push(int n) {
		if(size<100) {
			size++;
			stackdata[size-1]=n;
		}
		else
			System.out.println("stack is full");
    }
    void pop(){
		if(size>0){
			//System.out.print(stackdata[size-1]);
			stackdata[size-1]=0;
			size--;
		}
    }
}
