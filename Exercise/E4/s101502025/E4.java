package ce1002.E4.s101502025;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Scanner;
import javax.swing.*;

public class E4 extends JFrame implements ActionListener 
{
	// all the objects
	private JLabel a = new JLabel("input:");;
	private JLabel  b= new JLabel("result:");;
	private JButton push = new JButton("push");;
	private JButton pop = new JButton("pop");;
	private JTextField input = new JTextField(20);
	private JTextField result = new JTextField(20);
	private String ss,bb="",dd="";
	private int n=0;
	private String[] cc=new String[100];

	public static void main(String[] args) {
		
		// the window
		E6 hw = new E6("M1");
		hw.setSize(250, 200);
		hw.setLocation(250, 250);
		hw.setVisible(true);
	}

	public E6(String pTitle) {
		
		// the place of the objects
		super(pTitle);
		setLayout(null);
		push.setBounds(40, 40, 65, 20);
		pop.setBounds(120, 40, 65, 20);
		a.setBounds(0, 0, 70, 20);
		b.setBounds(0, 20, 70, 20);
		input.setBounds(40, 0, 150, 20);
		result.setBounds(40,20, 150, 20);
		pop.addActionListener(this);
		push.addActionListener(this);
		add(pop);
		add(push);
		add(a);
		add(b);
		add(input);
		add(result);
	}

	public void actionPerformed(ActionEvent e) {
		
		// calculate
		if(e.getSource()==push)
		{
			ss=input.getText();
			cc[n]=ss;
			if(n==0)
			{
				bb=ss;
			}
			else
			{
				bb=bb + "," + ss;
			}
			n++;
			result.setText(bb);
		}
				
		if(e.getSource()==pop)
		{
			if(n==1)
				dd="";
			else
			{
				for(int i=0;i<n-1;i++)
				{
					if(i==0)
					dd=cc[i];
					else
					dd=dd + "," + cc[i];
				}
			}
			n--;
			result.setText(dd);
		}
	}
}
