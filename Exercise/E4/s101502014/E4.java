package ce1002.E4.s101502014;
import javax.swing.*; //引入GUI需要的功能
import java.awt.event.ActionEvent; //引入按鈕的事件功能
import java.awt.event.ActionListener; //引入按鈕的反應功能
public class E4 extends JFrame implements ActionListener{
	private JButton psh = new JButton("push"); //建立push的按鈕
	private JButton pop = new JButton("pop"); //建立pop的按鈕
	private JTextField ipt_content = new JTextField(20); //建立input的文字框
	private JTextField rst_content = new JTextField(20); //建立result的文字框
	private JLabel ipt = new JLabel("input:"); //建立"input:"的文字
	private	JLabel rst = new JLabel("result:"); //建立"result:"的文字
	String[] stack = new String[100]; //stack陣列儲存每一次push的結果
	int index = 0; //index紀錄stack的高度
	public static void main(String[] args) {
		E4 frame = new E4("E4"); //建立視窗
		frame.setSize(400 , 300); //設定視窗大小
		frame.setLayout(null); //不使用版面設定
		frame.setLocationRelativeTo(null); //固定視窗位置出現在螢幕的中間
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setVisible(true); //讓視窗出現
	}
	public E4(String pTitle){
		super(pTitle);
		setLayout(null); //不使用版面設定 
		psh.setBounds(100 , 80 , 80 , 30); //設定push按鈕的位置
		pop.setBounds(190 , 80 , 80 , 30); //設定pop按鈕的位置
		ipt_content.setBounds(50 , 10 , 300 , 20); //設定input文字框的位置
		rst_content.setBounds(50 , 50 , 300 , 20); //設定result文字框的位置
		ipt.setBounds(10 , 10 , 100 , 20); //設定input文字的位置
		rst.setBounds(10 , 50 , 100 , 20); //設定result文字的位置
		psh.addActionListener(this); //把push按鈕加入反應的功能
		pop.addActionListener(this); //把pop按鈕加入反應的功能
		add(psh); //加入push按鈕到視窗中
		add(pop); //加入pop按鈕到視窗中
		add(ipt_content); //加入input文字框到視窗中
		add(rst_content); //加入result文字框到視窗中
		add(ipt); //加入input文字到視窗中
		add(rst); //加入result文字到視窗中
	}
	public void actionPerformed(ActionEvent e) {
		if (e.getSource() == psh){ //若push按鈕有被按下的反應
			if(index == 0) //若原本為空stack , 則不用輸出","和讀取之前的stack內容
				stack[index] = ipt_content.getText();
			else //若不是空stack , 則讀取之前的stack內容和input文字框的內容
				stack[index] = stack[index - 1] + "," + ipt_content.getText();
			rst_content.setText(stack[index]); //輸出結果到result文字框內
			index++; //stack高度加一
		}
		else if(e.getSource() == pop){ //若pop按鈕有被按下的反應
			if(index == 0 || index == 1) //若stack高度為0或1 , 則result文字框的內容變為空的 , stack高度變為0
			{
				rst_content.setText("");
				index = 0;
			}
			else //若stack高度非0或1 , 則stack高度減一 , result文字框的內容取代維之前的stack內容
			{
				index--;
				rst_content.setText(stack[index - 1]);
			}
		}
	}
}