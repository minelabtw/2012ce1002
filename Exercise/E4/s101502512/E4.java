package ce1002.E4.s101502512;

import javax.swing.*;
import java.util.Scanner;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class E4 extends JFrame implements ActionListener {
	private JButton btn = new JButton("push");
	private JButton btn2 = new JButton("pop");
	private JTextField text = new JTextField(100);
	private JTextField text2 = new JTextField(100);
	private JLabel la = new JLabel("input");
	private JLabel la2 = new JLabel("result");
	private int size;// 有幾個數字
	private int[] stack = new int[100];
	private String a;// 存輸入的數字

	public static void main(String[] args) {

		E4 hw = new E4("E4");
		hw.setSize(500, 150);
		hw.setLocation(250, 250);
		hw.setVisible(true);
		hw.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		;

	}

	public E4(String Title) {

		super(Title);

		setLayout(null);// 不使用版面配置

		btn.setBounds(150, 60, 100, 20);
		btn2.setBounds(256, 60, 100, 20);
		text.setBounds(100, 20, 380, 20);
		text2.setBounds(100, 40, 380, 20);
		la.setBounds(20, 20, 40, 20);
		la2.setBounds(20, 40, 40, 20);

		btn.addActionListener(this);// 判斷按鈕有沒有被按
		btn2.addActionListener(this);// 判斷按鈕有沒有被按
		add(la);
		add(text);
		add(btn);
		add(la2);
		add(text2);
		add(btn2);
	}

	public void actionPerformed(ActionEvent e) {
		stack[size] = Integer.parseInt(text.getText());// 轉成字串
		if (e.getSource() == btn) {
			size++;

			a = "";
			for (int i = 0; i < size; i++) {// 將輸入的數字存入一個字串
				a += stack[i];
				if (i + 1 < size)
					a += ",";
			}

			text2.setText(a);
		} else {
			size--;
			a = "";
			if (size < 0) {
				size = 0;

			}

			for (int i = 0; i < size; i++) {// 將輸入的數字存入一個字串
				a += stack[i];
				if (i + 1 < size)// 超過一個數字，加入逗號區分
					a += ",";
			}

			text2.setText(a);
		}

	}

}
