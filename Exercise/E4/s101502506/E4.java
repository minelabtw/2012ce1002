package ce1002.E4.s101502506;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import javax.swing.JLabel;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class E4 extends JFrame 
{
	private JButton btn1 = new JButton("push");
	private JButton btn2 = new JButton("pop");
	private JTextField text1 = new JTextField(20);
	private JTextField text2 = new JTextField(20);
	private JLabel label1 = new JLabel();
	private JLabel label2 = new JLabel();
	private int bagnumber=0;


	public static void main(String[] args) 
	{

		E4 hw = new E4("M1");

		hw.setSize(300, 200);

		hw.setLocation(250, 250);

		hw.setVisible(true);

	}

	public E4(String pTitle) 
	{

		super(pTitle);

		setLayout(null);// 不使用版面配置
		
		label1.setText("input:");
		label1.setBounds(10,10,50,20);
		label2.setText("result:");
		label2.setBounds(10,40,60,20);
		

		btn1.setBounds(60, 80, 80, 20);
		btn2.setBounds(150, 80, 80, 20);

		text1.setBounds(50, 10, 200, 20);
		text2.setBounds(55, 40, 200, 20);
        
		ButtonHandler handler = new ButtonHandler();
		btn1.addActionListener(handler);
		btn2.addActionListener(handler);
		
		add(label1);
		add(label2);

		add(btn1);
		add(btn2);

		add(text1);
		add(text2);

	}
private class ButtonHandler implements ActionListener
{
	public void actionPerformed(ActionEvent event) 
	{
		String string,str=" ";
		String a[]=new String[100];//宣告一個陣列儲存輸入的數
		
		if(event.getSource()== btn1)//當使用者按下btn1
		{	
			string = text1.getText();
			bagnumber++;
			if(bagnumber==1)
			{	
				a[bagnumber-1]= string;
				text2.setText(a[0]);
			}
			else
			{
				a[bagnumber-1]=string;
				for(int i=0;i<=bagnumber-1;i++)
				{
					str+= a[i];
				}
				text2.setText(str);
			}
		}	
		else if(event.getSource()== btn2)//當使用者按下btn2
		{	
			string = String.format("%s",event.getActionCommand());
		}	

	}
	
		 
}

}


