package ce1002.E4.s101502515;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JTextField;

public class E4 extends JFrame{
	static int point = -1;//目前指向(全域)
	static String strbuf = "";//轉向傳給textfield使用
	static int[] stacker = new int[100];
	public E4(){
		this.setDefaultCloseOperation(EXIT_ON_CLOSE);//正常關閉
		this.setSize(300, 300);
		this.setVisible(true);
		this.getContentPane().setLayout(null);//自定範圍
		JLabel inp = new JLabel("input:");
		inp.setVisible(true);
		inp.setBounds(0, 0, 50, 25);
		this.add(inp);
		JLabel res = new JLabel("result:");
		res.setVisible(true);
		this.add(res);
		res.setBounds(0, 25, 50, 25);
		final JTextField intex= new JTextField();
		intex.setVisible(true);
		this.add(intex);
		intex.setBounds(50, 0, 200, 25);
		final JTextField retet= new JTextField();
		retet.setVisible(true);
		this.add(retet);
		retet.setBounds(50, 25, 200, 25);
		JButton pus = new JButton("push");
		pus.setVisible(true);
		this.add(pus);
		pus.setBounds(50, 60, 100, 25);
		JButton pop = new JButton("pop");
		pop.setVisible(true);
		this.add(pop);
		pop.setBounds(160, 60, 100, 25);
		ActionListener pusa = new ActionListener() {//push按鍵的動作
			@Override
			public void actionPerformed(ActionEvent e) {
				int i = Integer.parseInt(intex.getText());
				push(stacker,i);
				replaforstrbuf();
				retet.setText(strbuf);
			}
		};
		ActionListener popa = new ActionListener() {//pop按鍵的動作
			@Override
			public void actionPerformed(ActionEvent e) {
				poper(stacker);
				replaforstrbuf();
				retet.setText(strbuf);
			}
		};
		pus.addActionListener(pusa);
		pop.addActionListener(popa);
		
	}
	public static void main(String args[]) {
		E4 aa=new E4();//創立此物件
	}
	static void push(int[] stacker, int a){
			point+=1;
			stacker[point] = a;
	}
	static void poper(int[] stacker){//讀取目前指向並抹去&減少指向&輸出//a=次數
			if(point==-1){//沒有內容物
				System.out.print("\n");
				System.out.print("Stack is empty!");
			}
			else{
			stacker[point]=0;//抹去
			point-=1;
			}
	}
	static void replaforstrbuf(){//刷新目前有的數
		strbuf="";
		for(int i =0;i<point+1;i++){
			strbuf+=(stacker[i]+",");
		}
	}
}
