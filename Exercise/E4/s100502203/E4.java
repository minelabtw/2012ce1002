package ce1002.E4.s100502203;

import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.*;

@SuppressWarnings("serial")
public class E4 extends JFrame{
	JTextField intext = new JTextField(29);
	JTextField outtext = new JTextField(29);
	static JButton push = new JButton("push");
	JButton pop = new JButton("pop");	
	Stack stack = new Stack();
	
	public E4(){
		setLayout(new FlowLayout(FlowLayout.CENTER, 10, 20));	
		add(new JLabel("input"));
		add(intext);
		add(new JLabel("output"));
		add(outtext);
		add(push);
		add(pop);
		
		push.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent e) {
				if(!stack.isFull()){
					stack.push(Integer.parseInt(intext.getText()));
					outtext.setText(stack.show());
				}
			} 
		});
		
		pop.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent e) {
				if(!stack.isEmpty()){
					stack.pop();
					outtext.setText(stack.show());
				}
			} 
		});
	}
	
	public static void main(String[] args) {
		E4 frame = new E4();
		frame.setTitle("E4");
		frame.setSize(410, 300);
		frame.setLocationRelativeTo(null);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setVisible(true);
	}
	
	public static class Stack {
		int stackData[], top, Max_size = 100;

		Stack() {
			stackData = new int[Max_size];
			top = 0;
		}

		boolean isFull() {
			if (top == Max_size - 1)
				return true;
			else
				return false;
		}

		boolean isEmpty() {
			if (top == 0)
				return true;
			else
				return false;
		}

		void push(int n) {
			stackData[top++] = n;
		}

		int pop() {
			return stackData[--top];
		}

		String show() {
			if (this.isEmpty()) {
				System.out.println("Stack is empty!");
				return null;
			}
			String tmp = "";
			
			for (int i = 0; i < top; i++){
				tmp += stackData[i];
				if(i < top - 1)
					tmp += ",";
			}
			return tmp;
		}
	}
}