package ce1002.E4.s100502006;

import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JTextField;

public class E4 extends JFrame{
	
	private JTextField jtfInput = new JTextField();
	private JTextField jtfResult = new JTextField();
	
	private JButton jbtPush = new JButton("push");
	private JButton jbtPop =new JButton("pop");
	
	private int[] s=new int[100];//the stack
	private int numberInStack=0;
	private String stringInResult="";//to show in result
	
	public E4() {
		//set layout
		setLayout(new GridLayout(3,2));
		add(new JLabel("input:"));
		add(jtfInput);
		add(new JLabel("result:"));
		add(jtfResult);
		add(jbtPush);
		add(jbtPop);
		//action listener
		jbtPush.addActionListener(new PushButtonListener());
		jbtPop.addActionListener(new PopButtonListener());

	}
	private class PushButtonListener implements ActionListener {
		@Override
		public void actionPerformed(ActionEvent e) {
			// TODO Auto-generated method stub
			
			int intToPush=Integer.parseInt(jtfInput.getText());//read in the input integer
			s[numberInStack]=intToPush;
			numberInStack++;
			//set the string to print in result
			if(numberInStack>1){
					stringInResult+=",";
			}
			stringInResult+=String.valueOf(intToPush);
			//print out the result and clear the input text field
			jtfResult.setText(stringInResult);
			jtfInput.setText("");
		}
	}
	private class PopButtonListener implements ActionListener{
		@Override
		public void actionPerformed(ActionEvent e) {
			// TODO Auto-generated method stub
			if(numberInStack>0){
				numberInStack--;
				
				jtfInput.setText(String.valueOf(s[numberInStack]));//get the first element of the stack
				//reset the result
				stringInResult="";
				for(int i=0;i<numberInStack;i++){
					if(i!=0){
						stringInResult+=",";
					}
					stringInResult+=String.valueOf(s[i]);
				}
				//print out result
				jtfResult.setText(stringInResult);
				
			}			
		}
	}
	
	public static void main(String[] args) {
		E4 frame= new E4();
		frame.pack();
		frame.setTitle("E4");
		frame.setLocationRelativeTo(null);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setVisible(true);
	}
}
