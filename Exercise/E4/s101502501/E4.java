package ce1002.E4.s101502501;
import java.util.Scanner;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.*;

public class E4 extends JFrame implements ActionListener 
{
	private JLabel input = new JLabel("input");
	private JLabel output = new JLabel("output");
	private JButton pushBtn = new JButton("push");
	private JButton popBtn = new JButton("pop");
	private JTextField text1 = new JTextField(20);
	private JTextField text2 = new JTextField(20);
	private int[] array = new int[100];
	private int pushNum = 0;

	
	public static void main(String[] args) {

		E4 hw = new E4("M1");//視窗最上面那條顯示的字
		hw.setSize(400, 300);//視窗大小
		hw.setLocation(250, 250);
		hw.setVisible(true);//顯示視窗(如果是false就不會顯示)
		

	}

	public E4(String pTitle) {

		super(pTitle);
		setLayout(null);
		text1.setBounds(40, 20, 330, 20);//那一條白色的
		text2.setBounds(40, 40, 330, 20);
		input.setBounds(5, 20, 50, 20);
		output.setBounds(2, 40, 50, 20);		
		pushBtn.setBounds(120, 60, 70, 25);//push按鈕
		popBtn.setBounds(200, 60, 70, 25);//pop按鈕

		
		pushBtn.addActionListener(this);//按按鈕後執行下面那些actionPerformed
		popBtn.addActionListener(this);

		add(text1);//新增在視窗裡
		add(text2);
		add(pushBtn);
		add(popBtn);
		add(input);
		add(output);

	}

	public void actionPerformed(ActionEvent e) {
		
		if(e.getSource()==pushBtn){//按push按鈕
			
			int a = Integer.valueOf(text1.getText());//把字串存成陣列
			
			array[pushNum] = a;
			
			if(pushNum==0){//只有輸入一個字的時候
				text2.setText(" "+array[pushNum]);
			}
			else{//輸入一個以上的字時
				text2.setText(text2.getText()+","+array[pushNum]);
			}
			pushNum++;//計算陣列裡有幾個數字
			for(int i=0; i<pushNum; i++){
				System.out.println(array[i]);
			}
		}
		
		else if(e.getSource()==popBtn){//按pop按鈕
			
			String str = new String();//用一個字串來存陣列裡的數字
			pushNum--;//陣列裡的數字減少一個(因為最後那的會pop掉)
			for(int i=0; i<pushNum; i++){
				str += array[i]+",";
			}
						
			text2.setText(" "+str);//輸出
			
			System.out.println(array[pushNum]);
			
			
		}
	}
}