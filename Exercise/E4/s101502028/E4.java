package ce1002.E4.s101502028;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.*;

public class E4 extends JFrame implements ActionListener {
	private JButton btn1 = new JButton("push"); // button of push
	private JButton btn2 = new JButton("pop"); // button of pop
	private JTextField text1 = new JTextField(100); // blank space
	private JTextField text2 = new JTextField(100); // blank space
	private JLabel lb1 = new JLabel("input:"); // label of input
	private JLabel lb2 = new JLabel("output:"); // label of output
	private int i = 0; // numbers counter
	private int[] numbers = new int[100]; // array to input numbers
	private String number; // show all the numbers

	public static void main(String[] args) { // windows form
		E4 hw = new E4("E4");
		hw.setSize(500, 150);
		hw.setLocation(250, 250);
		hw.setVisible(true);
	}

	public E4(String pTitle) {
		super(pTitle);
		setLayout(null); // set our own window
		btn1.setBounds(150, 60, 100, 20); // button push position
		btn2.setBounds(256, 60, 100, 20); // button pop position
		text1.setBounds(60, 20, 390, 20); // blank space position
		text2.setBounds(70, 40, 380, 20); // blank space position
		lb1.setBounds(20, 20, 40, 20); // label input position
		lb2.setBounds(20, 40, 40, 20); // label output position
		btn1.addActionListener(this); // apply the button
		btn2.addActionListener(this); // apply the button
		add(btn1); // add all this to the window
		add(btn2);
		add(lb1);
		add(lb2);
		add(text1);
		add(text2);
	}

	public void actionPerformed(ActionEvent e) {
		if (e.getSource() == btn1) { // if apply the button of push
			numbers[i] = Integer.parseInt(text1.getText()); // input a number
			i++; // counter ++
			number = ""; // empty the space
			for (int a = 0; a < i; a++){
				number += numbers[a]; // put the array number in the space to show it
				if (a + 1 < i)
					number += ", "; // if there's next number, show a coma
			}
			text2.setText(number); // show all the numbers
		}
		else if (e.getSource() == btn2){ // if apply the button of pop
			i--; // counter --
			number = ""; // empty the space
			for (int a = 0; a < i; a++){
				number += numbers[a]; // put the array number in the space to show it 
				if (a + 1 < i)
					number += ", "; // if there's next number, show a coma
			}
			text2.setText(number); // show all the numbers
		}
	}
}