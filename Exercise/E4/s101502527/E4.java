package ce1002.E4.s101502527;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.*;

public class E4 extends JFrame implements ActionListener 
{
	private JButton pushb = new JButton("push");//宣告元件
	private JButton popb = new JButton("pop");
	private JTextField input = new JTextField(20);
	private JTextField output = new JTextField(20);
	private JLabel in=new JLabel("input");
	private JLabel out=new JLabel("output");
	private static int[] stack=new int[100];
	static int now=0;
	
	public static void main(String[] args) {//開始

		E4 hw = new E4("Hello World App");

		hw.setSize(500, 500);

		hw.setLocation(250, 250);

		hw.setVisible(true);

	}

	public E4(String pTitle) {//設定 元件

		super(pTitle);

		setLayout(null);// 不使用版面配置

		pushb.setBounds(20, 40, 140, 20);
		popb.setBounds(300, 40, 140, 20);
		in.setBounds(0, 60, 50, 50);
		out.setBounds(0, 100, 50, 50);
		input.setBounds(100, 80, 300, 20);
		output.setBounds(100, 120, 300, 20);
		pushb.addActionListener(this);
		pushb.setActionCommand("push");
		popb.addActionListener(this);
		popb.setActionCommand("pop");
		add(in);
		add(pushb);
		add(popb);
		add(out);
		add(input);
		add(output);
		

	}

	public void actionPerformed(ActionEvent e) {
		
		String cmd = e.getActionCommand();
		
		if(cmd=="push"){//按鈕push
			if(!input.getText().equals("")){//不等於空字串 以下為push
				stack[now]=Integer.valueOf(input.getText());
				now++;
				String str="";
				for(int x=0;x<now-1;x++){
					str+=String.valueOf(stack[x])+",";
				}
				str+=String.valueOf(stack[now-1]);
				output.setText(str);
			}
			else
				output.setText("Input is empty");
			
		}

		if(cmd=="pop"){ 
			if(now!=0){//如果stack沒空   以下為pop
				now--;
				String str="";
				for(int x=0;x<now;x++){
					str+=String.valueOf(stack[x])+",";
				}
				if(now!=0)
					str = str.substring(0, str.length() - 1);//減掉逗號
				output.setText(str);
				
			}
			
			else
				output.setText("Stack is empty");
		}
			
	}

}
	
	

