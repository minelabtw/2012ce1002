package ce1002.E4.s101502524;


import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Scanner;

import javax.swing.*;


public class E4 extends JFrame implements ActionListener{
		
		private JButton btn = new JButton("push");;
		private JButton btn2 = new JButton("pop");;
		private JTextField text = new JTextField(20);
		private JTextField text2 = new JTextField(20);
		private JLabel input =new JLabel("input  :");
		private JLabel result =new JLabel("result :");
		private String str=new String();
		private int[] a= new int[100];
		private int count=0;

		
		public static void main(String[] args) {
			
			Scanner input=new Scanner(System.in);
			
			E4 hw = new E4("Stack");//設定視窗名稱

			hw.setSize(200, 150);//設定視窗原始大小

			hw.setLocation(250, 250);//設定視窗原始位置

			hw.setVisible(true);//使視窗可被看到

		}

		public E4(String pTitle) {

			super(pTitle);

			setLayout(null);// 不使用版面配置

			//設定各東西的位置
			btn.setBounds(120, 80, 70, 20);

			btn2.setBounds(200,80,70, 20);
			
			text.setBounds(70, 20, 400, 20);
			
			text2.setBounds(70,45,400,20);
			
			input.setBounds(15,20,100,20);
			
			result.setBounds(15, 45, 100, 20);

			btn.addActionListener(this);
			
			btn2.addActionListener(this);
			
			//新增東西至視窗內
			add(btn);
			
			add (btn2);
			
			add(text);
			
			add(text2);
			
			add(input);
			
			add(result);
		}

		public void actionPerformed(ActionEvent e) 
		{
			if(e.getSource()==btn)//當點下push時，則將input內的字加入result
			{
				if(!text.getText().equals(""))//當輸入不為空時才執行
				{
					str="";
					a[count]=Integer.valueOf(text.getText());//將輸入存入陣列
					count++;
					//將陣列內容存入字串中
					for(int i=0;i<count;i++)
					{
						str=str+String.valueOf(a[i]+",");
					}
					if(count!=0)
					{
						str = str.substring(0, str.length() - 1);
					}
					//將字串輸出至result
					text2.setText(str);
				}
			}
			if(e.getSource()==btn2)//當點下pop時，則將result內最後一個字去除
			{
				if(count!=0)//當數量不為0時才執行
				{
					str="";
					count--;
					if(count==0)
					{
						str=str+String.valueOf("");
					}
					else
					{
						//將陣列存入字串
						for(int i=0;i<count;i++)
						{
							str=str+String.valueOf(a[i]+",");
						}
						str = str.substring(0, str.length() - 1);
					}
					//將字串輸出至result
					text2.setText(str);
				}
			}
		}
	}

