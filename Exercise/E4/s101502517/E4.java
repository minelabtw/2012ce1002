package ce1002.E4.s101502517;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.*;


public class E4 extends JFrame implements ActionListener{
	private JButton btn1 = new JButton("push");
	private JButton btn2 = new JButton("pop");
	private JLabel in = new JLabel("Input");
	private JLabel out = new JLabel("Output");
	private JTextField inp = new JTextField(20);
	private JTextField outp = new JTextField(20);
	String [] stack=new String[101];
	int l=1;
	String o="";
	
	
	public static void main(String[] args) {

		E4 hw = new E4("STACK(GUI)");
		hw.setSize(450, 250);
		hw.setLocation(250, 250);
		hw.setVisible(true);

	}

	public E4(String pTitle) {

		super(pTitle);

		setLayout(null);// 不使用版面配置

		btn1.setBounds(20, 60, 180, 20);//BUTTON
		btn2.setBounds(170, 60, 180, 20);
		
		inp.setBounds(80, 0, 250, 20);//文字爛
		outp.setBounds(80, 20, 250, 20);
		
		in.setBounds(20, 0, 60, 20);//標籤
		out.setBounds(20, 20, 60, 20);
		
		
		btn1.addActionListener(this);//監聽
		btn2.addActionListener(this);
		add(btn1);
		add(btn2);
		add(inp);
		add(outp);
		add(in);
		add(out);
	}

	public void actionPerformed(ActionEvent e) {
		if(e.getSource()==btn1){//push
			stack[l]=inp.getText();//擷取輸入值
			inp.setText("");//清空INPUT欄位
			l++;//內容量+1
			o="";//清空O字串
			for(int i=1;i<l;i++){//建立O字串
				o=o+stack[i];		
				if(i+1<l){//後面還有東西+逗號
					o=o+",";
				}	
			}
			outp.setText(o);//輸出o字串		
		}
		else{//pop
			inp.setText("");//清空input欄
			l--;//內容量-1
			if(l<=0){
				l=1;
			}
			o="";//清空o字串
			for(int i=1;i<l;i++){//建立o字串
				o=o+stack[i];		
				if(i+1<l){//後面還有東西+逗號
					o=o+",";
				}			
			}
			outp.setText(o);//輸出o字串
		}
	}

}
