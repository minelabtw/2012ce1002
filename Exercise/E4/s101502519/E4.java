package ce1002.E4.s101502519;

import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JTextField;

public class E4 extends JFrame {
	private JLabel label1 = new JLabel("input:");
	private JLabel label2 = new JLabel("output:");
	private JTextField textField1 = new JTextField();
	private JTextField textField2 = new JTextField();
	private JButton button1 = new JButton("push");
	private JButton button2 = new JButton("pop");
	private int[] a=new int[50]; 
	private int i =0;
	private String output1;
	public static void main(String[] args) {
		new E4(); 
	}
	
	public E4() {
		super("E4");
		this.setSize(500,400);      //視窗大小
				
		setLayout(null);//不使用版面配置
		label1.setBounds(5,5,50,30);     //設置原件
		label2.setBounds(5,30,50,30);
		textField1.setBounds(60,10,400,20);
		textField2.setBounds(60,35,400,20);
		button1.setBounds(130,60,100,30);
		button2.setBounds(240,60,100,30);
		add(label1);             //添加原件
		add(textField1);
		add(label2);
		add(textField2);
		add(button1);
		add(button2);
		
		button1.addActionListener(new ActionListener(){   //push
			public void actionPerformed(ActionEvent arg0) {
				
				a[i] = Integer.parseInt(textField1.getText());   //將輸入的數字加入字串
				textField1.setText("");

				if(i==0){
					output1=a[i]+"";
				}
				else{
					output1=output1+","+a[i];
				}
				textField2.setText(output1);
				
				i=i+1;
			}
		});
		button2.addActionListener(new ActionListener(){    //pop
			public void actionPerformed(ActionEvent arg0) {    //將輸入的數字移出字串
				
				textField1.setText("");
				System.out.println(i);
				if(i-1==0){
					textField2.setText("");
				}
				else{
					output1=a[0]+"";
					for (int j = 1; j <= i-2; j++) {
						output1=output1+","+a[j];
					}
					
					textField2.setText(output1);
				}
				i=i-1;
			}
		});

		this.setVisible(true);           
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);	
	}
}
