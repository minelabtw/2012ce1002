package ce1002.E4.s101502020;

import java.awt.*;
import javax.swing.*;

public class E4 extends JFrame{

	public E4()  {
		setLayout(new GridLayout(3,2));
		
		add(new JLabel("input:"));
		add(new JTextField());
		add(new JLabel("result:"));
		add(new JTextField());
		add(new JButton("push"));
		add(new JButton("pop"));
		
	}
	
	public static void main(String[] args) {
		
		E4 frame=new E4();
		frame.setTitle("M1");
		frame.setSize(500,500);
		frame.setLocationRelativeTo(null);
		frame.setDefaultCloseOperation(EXIT_ON_CLOSE);
		frame.setVisible(true);
	}

}
