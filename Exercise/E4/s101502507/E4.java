package ce1002.E4.s101502507;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.*;
import java.util.Scanner;


public class E4 extends JFrame implements ActionListener 
{
	private JLabel lb1 = new JLabel("input:");
	private JLabel lb2 = new JLabel("result:");
	private JButton btn1 = new JButton("push");
	private JButton btn2 = new JButton("pop");
	private JTextField text1 = new JTextField(20);
	private JTextField text2 = new JTextField(20);
	
	Scanner input = new Scanner(System.in);
	int abc[] = new int[100];		
	int i=0,j=0;
	String a[] = new String[100];
	String d = "";
	String e = "";	
	
	public static void main(String[] args)
	{
		E4 hw = new E4("M1");
		hw.setSize(600, 400);
		hw.setLocation(250, 250);
		hw.setVisible(true);
	}

	public E4(String pTitle) 
	{
		super(pTitle);
		setLayout(null);// 不使用版面配置
		
		lb1.setBounds(20, 20, 60, 20);
		lb2.setBounds(20, 60, 60, 20);

		btn1.setBounds(120, 100, 80, 20);
		btn2.setBounds(240, 100, 80, 20);

		text1.setBounds(60, 20, 400, 20);
		text2.setBounds(60, 60, 400, 20);

		btn1.addActionListener(this);
		btn2.addActionListener(this);
		
		add(lb1);
		add(lb2);

		add(btn1);
		add(btn2);

		add(text1);
		add(text2);		
	}

	public void actionPerformed(ActionEvent arg0) 
	{
		if(arg0.getSource()==btn1)
		{			
			d = text1.getText();
			a[i]=d;
			i++;
			e = e + d + ",";			
		}
		else if(arg0.getSource()==btn2)
		{
			e = "";
			for(j=0;j<i-1;j++)			
				e = e + a[j] + ",";
			i--;
		}		
		text2.setText(e);
	}
}