package ce1002.E4.s101502523;
import java.util.*;
import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class E4 extends JFrame implements ActionListener {
	private TextField input = new TextField(70);
	private TextField result = new TextField(70);
	private JButton btPush = new JButton("push");
	private JButton btPop = new JButton("pop");
	private Stack<String> stack = new Stack<String>();
	private String str = new String();
	
	public E4() {
		JPanel p0 = new JPanel(new GridLayout(2,1,1,1));
		JPanel p1 = new JPanel(new FlowLayout(FlowLayout.CENTER));
		JPanel p2 = new JPanel(new FlowLayout(FlowLayout.CENTER));
		JPanel p3 = new JPanel(new FlowLayout(FlowLayout.CENTER));		
		//加入物件
		p1.add(new JLabel("input:"));
		p1.add(input);
		p2.add(new JLabel("result:"));
		p2.add(result);
		p3.add(btPush);
		p3.add(btPop);
		p0.add(p1);
		p0.add(p2);
		//排版
		add(p0, BorderLayout.NORTH);
		add(p3, BorderLayout.CENTER);
		//ActionListener
		btPush.addActionListener(this);
		btPop.addActionListener(this);		
	}
	
	public static void main(String[] args) {
		//設定frame
		E4 frame = new E4();
		frame.setTitle("E4");		
		frame.setSize(600,150);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setVisible(true);	
	}
	
	public void actionPerformed(ActionEvent e) {
		int i=0;//stack元素位置
		//push
		if(e.getSource() == btPush)	{
			if(!input.getText().equals("")) {
				stack.push(input.getText());
				input.setText("");	
			}
		}
		//pop
		else if(e.getSource() == btPop)
			//避免stack為空時仍pop
			if(!stack.empty())
				stack.pop();	
		//儲存在一個string內(為符合輸出格式)
		while(i<stack.size()) {
			if(i==0) 
				str = str + stack.elementAt(i);	
			else 
				str = str + "," + stack.elementAt(i);				
			i++;		
		}
		//初始化stack位置
		i=0;
		//在result欄輸出stack所存的所有值
		result.setText(str);
		//初始化str
		str = "";
	}
}
