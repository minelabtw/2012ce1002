package ce1002.E4.s101502528;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.*;

public class E4 extends JFrame implements ActionListener
{
	private JButton pop = new JButton("pop");
	private JButton push = new JButton("push");
	private JTextField in = new JTextField(20);
	private JTextField out = new JTextField(20);
	private JLabel lab1 = new JLabel("input");
	private JLabel lab2 = new JLabel("result");
	private String str = new String();
	private int[] stack = new int[100];
	private int count = 0;
	
	public static void main(String[] args)
	{
		E4 hw = new E4("E4");
		hw.setSize(400,300);
		hw.setLocation(250, 250);
		hw.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		hw.setVisible(true);
	}
	
	public E4(String pTitle)
	{
		super(pTitle);
		setLayout(null);

		pop.setBounds(80, 90, 75, 20);
		push.setBounds(160, 90, 75, 20);
		in.setBounds(60, 30, 240, 20);
		out.setBounds(60, 60, 240, 20);
		lab1.setBounds(20, 30, 40, 20);
		lab2.setBounds(20, 60, 40, 20);
		pop.addActionListener(this);
		push.addActionListener(this);

		add(pop);
		add(push);
		add(in);
		add(out);
		add(lab1);
		add(lab2);

	}
	
	
	
	public void actionPerformed(ActionEvent e)
	{
		if(e.getSource()==push)//若按下push鍵則將input內的數push進result
		{
			if(!in.getText().equals(""))
			{
				str="";
				stack[count]=Integer.valueOf(in.getText());
				count++;
				
				//將陣列存入字串中
				for(int i=0;i<count;i++)
				{
					str=str+String.valueOf(stack[i]+",");
				}
				
				if(count!=0)
				{
					str = str.substring(0, str.length() - 1);
				}
				out.setText(str);
			}
		}
		
		if(e.getSource()==pop)//若按下pop鍵則將result內的數pop出一個
		{
			if(count!=0)
			{
				str="";
				count--;
				
				if(count==0)
				{
					str=str+String.valueOf("");
				}
				
				else
				{
					for(int i=0;i<count;i++)
					{
						str=str+String.valueOf(stack[i]+",");
					}
					str = str.substring(0, str.length() - 1);
				}
				out.setText(str);
			}
		}
	}
	
}
