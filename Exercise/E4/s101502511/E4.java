package ce1002.E4.s101502511;

import java.util.*;
import java.awt.*;
import javax.swing.*;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class E4 extends JFrame implements ActionListener {

	private JButton btn = new JButton("push");;
	private JButton btn1 = new JButton("pop");;
	private TextField text = new TextField(20);
	private TextField text1 = new TextField(20);
	private JLabel label = new JLabel("input:");
	private JLabel label1 = new JLabel("result:");
	private String numClicks;
	private String stack;
	private String[] stack1 = new String[100];
	private int i = 0, j = 0;
	private int[] n = new int[100];

	public static void main(String[] args) {
		
		E4 hw = new E4("E4");
		hw.setSize(500, 400);
		hw.setLocation(250, 250);
		hw.setVisible(true);
	}

	public E4(String pTitle) {

		super(pTitle);

		setLayout(null);// 不使用版面配置

		btn.setBounds(150, 150, 100, 20);//push按鈕
		btn1.setBounds(250, 150, 100, 20);//pop按鈕

		text.setBounds(150, 60, 180, 20);//文字方塊
		text1.setBounds(150, 100, 180, 20);

		label.setBounds(100, 60, 100, 20);//input位置
		label1.setBounds(100, 100, 100, 20);//result位置

		btn.addActionListener(this);
		btn1.addActionListener(this);

		add(btn);
		add(btn1);

		add(text);
		add(text1);

		add(label);
		add(label1);

	}

	public void actionPerformed(ActionEvent e) {
		numClicks = text.getText();
		if (e.getSource() == btn) {//push

			if (i == 0) {//避免出現null
				stack = numClicks;
			} else {
				stack += ",";
				stack += numClicks;
			}
			n[j] = numClicks.length() + 1;//push的大小
			i++;//第幾個push
			j++;//j是計算第幾個push的大小
			text1.setText(stack);
		} else if (e.getSource() == btn1) {//pop
			i--;
			if (stack.length() <= n[j]) {//如果數字太小無法pop,就無作用

			} else {
				stack = stack.substring(0, stack.length() - n[j]);
				j--;
			}
			text1.setText(stack);
		}
	}
}
