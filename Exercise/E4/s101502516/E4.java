package ce1002.E4.s101502516;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.*;

public class E4 extends JFrame implements ActionListener 
{
	JButton push = new JButton("push");
	JButton pop = new JButton("pop");
	JTextField text = new JTextField(20);
	JTextField text2 = new JTextField(20);
	JLabel word1 = new JLabel("input");
	JLabel word2 = new JLabel("result");
	
	public static void main(String[] args) {

		E4 hw = new E4("Hello World App");

		hw.setSize(400, 300);

		hw.setLocation(500, 500);

		hw.setVisible(true);

	}

	public E4(String pTitle) {

		super(pTitle);

		setLayout(null);// 不使用版面配置

		push.setBounds(100, 40, 70, 35);
		pop.setBounds(170, 40, 70, 35);

		text.setBounds(40, 0, 300, 20);
		text2.setBounds(40, 20, 300, 20);
		
		word1.setBounds(0, 0, 40, 20);
		word2.setBounds(0, 20, 40, 20);

		push.addActionListener(this);
		pop.addActionListener(this);
		
		

		add(word1);
		add(word2);
		
		add(push);
		add(text);
		
		add(pop);
		add(text2);

	}

	int x = 0;
	String [] stack = new String [100];
	int [] stack2 = new int [100];
	String str = "";
	public void actionPerformed(ActionEvent e) {
		if ( e.getSource()== push )
		{
			str = "";
			stack[x] = text.getText();
			x++;
			for ( int i = 0; i < x; i++ )
			{
				if ( i < x - 1 )
				{
					str += stack[i] + ",";
				}
				else
				{
					str += stack[i]; 
				}

			}
			text2.setText( str );
		}
		else if ( e.getSource() == pop )
		{
			str = "";
			x--;
			if ( x < 0 )
			{
				str = "";
			}
			for ( int i = 0; i < x; i++ )
			{
				if ( i < x - 1 )
				{
					str += stack[i] + ",";
				}
				else
				{
					str += stack[i]; 
				}

			}
			text2.setText( str );
		}
	}
}
