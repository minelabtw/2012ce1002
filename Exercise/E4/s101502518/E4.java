package ce1002.E4.s101502518;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;


import javax.swing.*;

public class E4 extends JFrame{
	
	private JLabel label1 = new JLabel("input: ");
	private JLabel label2 = new JLabel("result: ");
	private JTextField text1 = new JTextField(50);
	private JTextField text2 = new JTextField(50);
	private JButton but1 = new JButton("push");
	private JButton but2 = new JButton("pop");
	private stack sta = new stack();
	private String n;
	
	public static void main(String[] args)
	{
		E4 hw = new E4("E4");

		hw.setSize(500, 400);
		hw.setLocation(250, 250);
		hw.setVisible(true);
	}
	
	public E4(String pTitle) {

		super(pTitle);
		setLayout(null);
		label1.setBounds(5,5,50,20);
		text1. setBounds(45,5,420, 20);
		label2.setBounds(5,30,50,20);
		text2. setBounds(45,30,420, 20);
		but1.setBounds(167, 55, 80, 30);
		but2.setBounds(253, 55, 80, 30);
		but1.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				// TODO Auto-generated method stub
				sta.push(text1.getText());
				text2.setText(sta.show());
			}
		});
		but2.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				// TODO Auto-generated method stub
				sta.pop();
				text2.setText(sta.show());
			}
		});
		
		add(label1);
		add(text1);
		add(label2);
		add(text2);
		add(but1);
		add(but2);
	}
	
	

	
}
