package ce1002.E4.s101502526;
import java.awt.*;
import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Stack;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JTextField;


public class E4 extends JFrame implements ActionListener {
	JButton push = new JButton("push");               //公用變數
    JButton pop = new JButton("pop");
    JTextField result = new JTextField();
    JTextField input = new JTextField();
	String array [] = new String[ 10000 ];
	int i = 0;
  public E4() {

    JPanel p1 = new JPanel();
    JPanel p2 = new JPanel(new BorderLayout(10,3));         //設定樣式  用了4個PANEL
    JPanel p3 = new JPanel(new BorderLayout(10,20));        //先分上下兩塊，上面再分上下兩塊
    JPanel p4 = new JPanel(new BorderLayout(10,20));
    

    p1.add(push);
    p1.add(pop);

    p2.add(p3,BorderLayout.NORTH);
    p2.add(p4,BorderLayout.SOUTH);
    p3.add(new JLabel("input:"),BorderLayout.WEST);
    p3.add(input);
    p4.add(new JLabel("result:"),BorderLayout.WEST);
    p4.add(result);
    add(p2, BorderLayout.NORTH);
    add(p1, BorderLayout.CENTER);
    push.addActionListener(this);
    pop.addActionListener(this);

  }


  public static void main(String[] args) {                   //設定視窗
    E4 frame = new E4();
    frame.setTitle("The Front View of a Microwave Oven");
    frame.setSize(400, 250);
    frame.setLocationRelativeTo(null); 
    frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    frame.setVisible(true);

  }
  public void actionPerformed(ActionEvent e) { 
	String array2 = "";
	String array3 = "";	 
	if (e.getSource()==push)                          //PUSH
	{
		array[i] = input.getText();

		array2 = array[i];                            //先放到陣列中再用字串加起
		i += 1 ;
		if ( i !=1) 
		{
			for (int k = 0 ; k < i ; k ++)
			{
				array3 = array3 + array[k];
				result.setText(array3);
				array3 = array3 + ',';
			}
		}
		else 
			result.setText(array2);
	}
	if (e.getSource()==pop)                           //POP
	{
		if ( i > 0) {
			i -= 1 ;
			if ( i !=0) 
			{
				for (int k = 0 ; k < i ; k ++)        //數量減少
				{
					array3 = array3 + array[k];
					result.setText(array3);
					array3 = array3 + ',';
				}
			}
			else 
				result.setText("");
		}
	}
  }
}
