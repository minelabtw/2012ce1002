package ce1002.E4.s101502004;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.*;

public class E4 extends JFrame implements ActionListener{
	private JTextField input = new JTextField(400);//設置輸入框與按鈕
	private JTextField result = new JTextField(400);
	private JButton push = new JButton("push");
	private JButton pop = new JButton("pop");
	
	private String[] stack=new String[100];//儲存數字的陣列
	private int stacknumber=0;//陣列元素總數
	
	public E4(){//GUI設置
		JPanel P1 = new JPanel();//上方輸入區設置
		P1.setLayout(new GridLayout(2, 2));
		P1.add(new JLabel("input"));
		P1.add(input);
		P1.add(new JLabel("result"));
		P1.add(result);
		
		JPanel P2 = new JPanel();//下方按鈕區設置
		P2.setLayout(new FlowLayout(FlowLayout.LEFT));
		P2.add(push);
		P2.add(pop);
		
		add(P1,BorderLayout.NORTH);
		add(P2,BorderLayout.CENTER);
		
		push.addActionListener(this);
		pop.addActionListener(this);
	}
	public static void main(String[] args){
		E4 frame = new E4();
		frame.setTitle("E4");
		frame.setSize(400, 200);
		frame.setLocationRelativeTo(null);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setVisible(true);
	}
	public void actionPerformed(ActionEvent e) {
		String answer = null;//顯示出來的字串
		if(e.getSource()==push){//判斷按鈕
			stack[stacknumber]=input.getText();//先將數字讀進去
			stacknumber++;//總數+1
			answer = stack[0];//將第一個數字放進去
			for(int i=1;i<stacknumber;i++){//再把stack的內容一一放進去
				answer+=','+stack[i];
			}
			result.setText(answer);
		}
		else if(e.getSource()==pop){
			stacknumber--;//總數-1
			answer = stack[0];//不要讓answer為null
			for(int i=1;i<stacknumber;i++){//再開始把其他元素放進去
				answer+=','+stack[i];
			}
			if(stacknumber<=0){
				result.setText(" ");//若總數小魚等於0，輸出空白
			}
			else{
				result.setText(answer);//否則輸出答案
			}
		}
	}
}
