package ce1002.e4.s101502016;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Scanner;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JTextField;


public class E4 extends JFrame implements ActionListener {
	public static int[] stack = new int[100]; // my stack
	public static int size = 0; // stack's current size
	public static JButton btn = new JButton("push");
	public static JButton btn2 = new JButton("pop");
	public static JTextField text = new JTextField(20);
	public static JTextField text2 = new JTextField(20);
	public static JLabel lb = new JLabel("input:");
	public static JLabel lb2 = new JLabel("result:");
	public static String tmp="";
	public static String number;
	public static String result;
	public static int informationOtherWantsToUse;
	
	

	//////main
	public static void main(String[] args) {

		//設定GUI大小
		Scanner input = new Scanner(System.in);

		E4 hw = new E4("E4");

		hw.setSize(900, 600);

		hw.setLocation(250, 250);

		hw.setVisible(true);
		

	}

	public E4(String pTitle) {

		super(pTitle);// 呼叫父類別的建構式

		setLayout(null);// 不使用版面配置

		lb.setBounds(20, 20, 140, 20);
		lb2.setBounds(20, 40, 140, 20);

		text.setBounds(60, 20, 800, 20);
		text2.setBounds(60, 40, 800, 20);

		btn.setBounds(100, 100, 100, 20);// (x軸,y軸,長,寬)
		btn2.setBounds(200, 100, 100, 20);

		btn.addActionListener(this);
		btn2.addActionListener(new t());

		add(lb);
		add(lb2);
		add(btn);
		add(text);
		add(btn2);
		add(text2);

	}

	public void actionPerformed(ActionEvent e) {

		size++;
		
		number = text.getText();
		tmp="";
		int num = Integer.valueOf(number);
		stack[size-1] = num;
		
		for(int i=0 ; i<size ;i++)
			tmp += stack[i]+",";
		
		text2.setText( tmp );
		
		

	}

}
