package ce1002.E4.s101502013;

import java.awt.*;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.util.*;
import javax.swing.*;
public class E4 implements ActionListener{	
	//stack大概
	static String[] printString = new String[100];
	int pointer = 0;
	
	//以下設定frame版面
	
	//label
	JLabel l1 = new JLabel("input: "); 
	JLabel l2 = new JLabel("result: "); 
	//
	
	//field
	JTextField f1 = new JTextField(20); 
	JTextField f2 = new JTextField(20); 
	//
	
	//button 
	JButton b1 = new JButton("push"); 
	JButton b2 = new JButton("pop");	
	
	public E4(){
		//frame
		String title = new String ("E4");
		JFrame haha = new JFrame(title);
		FlowLayout FL = new FlowLayout(0);
		haha.setSize(350 , 190);
		haha.setLocation(500,300);
		haha.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		haha.setLayout(FL);
		//		

		// Register listeners
		PushListenerClass listener1 = new PushListenerClass();
	    PopListenerClass listener2 = new PopListenerClass();
	    b1.addActionListener(listener1);
	    b2.addActionListener(listener2);
		//
		
		//panel排版
		JPanel p1 = new JPanel(FL);
		JPanel p2 = new JPanel(FL);
		JPanel p3 = new JPanel(FL);
		
		//
		
		//加入panel
		p1.add(l1);
		p1.add(f1);
		p2.add(l2);
		p2.add(f2);
		p3.add(b1);
		p3.add(b2);
		//
		
		//加入frame
		haha.add(p1);
		haha.add(p2);
		haha.add(p3);
		//
		
		//顯示frame
		haha.setVisible(true); // Display the frame
		//
		
	}
	public static void main(String[] args){
		E4 lol = new E4();
		printString[0] = "";
		
		
	}
	//如果按下PUSH發生的事
	class PushListenerClass implements ActionListener {
		public void actionPerformed(ActionEvent e) {
			if (pointer <= 0)
				printString[pointer] = f1.getText();
			if (pointer > 0)
				printString[pointer] = printString[pointer-1] + "," + f1.getText();
			f2.setText(printString[pointer]);
			pointer++;
			
		}
	}
	//如果按下POP發生的事
	class PopListenerClass implements ActionListener {
		public void actionPerformed(ActionEvent e) {
			if (pointer > 1)
				f2.setText(printString[pointer-2]);
			if (pointer <= 1)
			{
				f2.setText("");
				pointer = 1;
			}
			pointer--;
			
		}
	}

}
	