package ce1002.E4.s101502521;

public class stack
{
    int[] stackData;
    int idx; // index of array
    stack()
    {
        stackData = new int[100]; // init the array
        idx=-1; // first index to -1 means no element in stack
    }
    void push(int n)
    {
    	idx++;
        stackData[idx] = n;
    }
    boolean pop()
    {
    	if(idx==-1)
    		return false;
        //System.out.print(stackData[idx]+" ");
        idx--;
        return true;
    }
    String show()
    {
    	String rtn = "";
    	if(idx==-1) // if no element in stack
    		return rtn;
    	rtn = Integer.toString(stackData[0]);
        for(int i=1;i<=idx;i++)
        	rtn+=(","+stackData[i]);
        //System.out.println("");
        return rtn;
    }
 
}