package ce1002.E4.s101502502;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.*;

public class E4 extends JFrame implements ActionListener
{
	private JButton btn1 = new JButton("push");
	private JButton btn2 = new JButton("pop");
	private JLabel no1 = new JLabel("input: ");
	static JTextField text1 = new JTextField(20);
	private JLabel no2 = new JLabel("result: ");
	static JTextField text2 = new JTextField(20);
	
	static String numPush = "";
	static int count = 0;
	static String[] array = new String[100];
	static String str = "";
	
	public E4()
	{
		setLayout(null);// 不使用版面配置

		no1.setBounds(100, 40, 40, 20);
		no2.setBounds(100, 80, 40, 20);
		text1.setBounds(140,40,200,20);
		text2.setBounds(140,80,200,20);
		btn1.setBounds(130, 120, 100, 20);
		btn2.setBounds(230, 120, 100, 20);
		
		btn1.addActionListener(this);
		btn2.addActionListener(new testListener());
		
		add(no1);
		add(no2);
		add(text1);
		add(text2);
		add(btn1);
		add(btn2);
	}

	
	public static void main(String[] args)
	{
		E4 frame = new E4();
		frame.setTitle("E4");
		frame.setSize(500, 300);
		frame.setLocationRelativeTo(null);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setVisible(true);
	}

	public void actionPerformed(ActionEvent e) 
	{
		numPush = text1.getText();
		array[count] = numPush;//陣列儲存push進來的數字
		
		str = str + array[count] + " ";
		text2.setText(str);//在text2輸出
		
		count++;
	}
}
