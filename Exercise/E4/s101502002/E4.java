package ce1002.E4.s101502002;
import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
public class E4 extends JFrame implements ActionListener{
	private String [] a =new String[200]; //來存stack裡面的東西
	private int count =0; //計算stack內有幾項東西(從1開始)
	private  JTextField text1 = new JTextField(200); //第一個輸入框框
	private  JTextField text2 = new JTextField(200); //第二個輸入框框
	private JButton btn1 = new JButton("push"); //push按鈕
	private JButton btn2 = new JButton("pop"); //pop按鈕
	private String all=""; //輸出的時候用的String
	public E4(){
		setLayout(new GridLayout(3,2,5,5)); //排版，3行2列
		add(new JLabel("input:")); //加上各個東西
		add(text1); //擺上第一個框框
		add(new JLabel("output:"));
		add(text2); //第二個框框
		btn1.addActionListener(this); //讓按鈕做事情
		add(btn1); //擺上按鈕
		btn2.addActionListener(this);
		add(btn2);
	}
	public static void main(String[]args){ //主程式
		E4 frame = new E4(); //一個frame
		frame.setTitle("E4"); //狀態列顯示E4
		frame.setSize(400,300); //長400寬300
		frame.setLocationRelativeTo(null); //擺在中間
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE); //按叉叉關閉
		frame.setVisible(true); //可以看見框框
	}
	@Override
	public void actionPerformed(ActionEvent e) {
		if(e.getSource()==btn1){ //按下push
			count++; //(從1開始，count的初始值是0)
			a[count]=text1.getText(); //取得第一個框框內的文字
			all=all + a[count]+","; //整個東西加上取得的東西
			text2.setText(all); //在第二個框框顯示全部的東西
		}
		if(e.getSource()==btn2){ //按下pop
			count=count-1; //去除最後一項
			all =""; //全部東西重新放進去
			for(int i=1;i<=count;i++) //一項一項加到all裡面
				all=all + a[i]+",";
			text2.setText(all); //輸出all
		}// TODO Auto-generated method stub
	}
}