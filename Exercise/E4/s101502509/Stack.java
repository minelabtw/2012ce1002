package ce1002.E4.s101502509;

public class Stack {
	private int[] stackData = new int[100];
	private int stackLength = 0;
	
	public void push(Integer num) {	//push data into the rear of stackData[] 
		stackData[stackLength++]= num;
	}
	
	public int pop() {
		return stackData[--stackLength];	//pop the last data of stackData[]
	}
	public String show(){	//show the stackData[]
		String tmp="";
		for(int i = 0; i < stackLength; i++) {
			tmp += (String)(stackData[i] + " ");
		}
		return tmp;
	}
}
