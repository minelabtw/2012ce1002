package ce1002.E4.s101502509;

import ce1002.E4.s101502509.Stack;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class E4 extends JFrame{
	Stack stack=new Stack();	// create stack[100];
	
	JLabel jlInput = new JLabel("input: ");
	JLabel jlOutput = new JLabel("result: ");
	JTextField jtInput = new JTextField(10);
	JTextField jtOutput = new JTextField(10);
	JButton jbtPush = new JButton("Push");
	JButton jbtPop = new JButton("Pop");
	
	public E4() {
		JPanel P1 = new JPanel();
		P1.setLayout(new GridLayout(3, 3));
		
		P1.add(jlInput);
		P1.add(jtInput);
		P1.add(jlOutput);
		P1.add(jtOutput);
		
		jbtPush.addActionListener(new pushActionListener());
		jbtPop.addActionListener(new popActionListener());
		P1.add(jbtPush);
		P1.add(jbtPop);
		add(P1);
		
		setTitle("E4");
		setSize(400, 250);
		
		setLocationRelativeTo(null);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setVisible(true);
		
		
	}
	public class pushActionListener implements ActionListener {	//the action of class pushActionListener 
		public void actionPerformed(ActionEvent e) {
			pushButtonAction();
		}
	};
	
	public class popActionListener implements ActionListener {	//the action of class popActionListener
		public void actionPerformed(ActionEvent e) {
			popButtonAction();
		}
	};
	
	public void pushButtonAction(){
//		String t=jtInput.getText();
//		ArrayList<Integer> t2=new ArrayList<Integer>();
		stack.push(new Integer(jtInput.getText()));	//push the text of jtInput into stack
		jtOutput.setText(stack.show());	//show each data of stack at jtOutput
		jtInput.setText("");	//clear  the text of jtInput
	}
	
	public void popButtonAction(){
		jtInput.setText(new Integer(stack.pop()).toString());	//pop the last data of stack into jtInput
		jtOutput.setText(stack.show());	//show each data of stack at jtOutput after pop
	}
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		new E4(); //create a frame
	}
}