package ce1002.E4.s101502510;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.*;

public class E4 extends JFrame implements ActionListener {

	JLabel lbl = new JLabel("input: ", JLabel.LEFT);
	JLabel lbl_1 = new JLabel("result: ", JLabel.LEFT);
	JButton btn = new JButton("push");
	JButton btn_1 = new JButton("pop");
	JTextField text = new JTextField(20);
	JTextField text_1 = new JTextField(20);
	private int num = 0;// 計算陣列大小
	private int[] stack = new int[100];

	public E4(String pTitle) {

		super(pTitle);

		setLayout(null);// 不使用版面配置

		lbl.setBounds(20, 40, 50, 50);
		lbl_1.setBounds(20, 60, 50, 50);
		btn.setBounds(100, 150, 100, 100);
		btn_1.setBounds(250, 150, 100, 100);
		text.setBounds(100, 55, 300, 20);
		text_1.setBounds(100, 75, 300, 20);

		btn.addActionListener(this);
		btn_1.addActionListener(this);

		add(lbl);
		add(text);
		add(lbl_1);
		add(text_1);
		add(btn);
		add(btn_1);

	}

	public void actionPerformed(ActionEvent e) {

		String p = new String();
		String l = new String();
		if (e.getSource() == btn) {
			stack[num] = Integer.parseInt(text.getText());
			if (num == 0) {
				p = String.valueOf(stack[num]);
			} else {
				for (int i = 0; i <= num; i++) {
					if (i == 0)
						p += String.valueOf(stack[i]);
					else
						p += String.valueOf("," + stack[i]);

				}
			}
			text_1.setText(p);
			num++;

		} else if (e.getSource() == btn_1) {
			num--;
			for (int i = num - 1; i >= 0; i--) {
				if (i == num - 1)
					l += String.valueOf(stack[i]);
				else
					l += String.valueOf("," + stack[i]);
			}
			text_1.setText(l);
		}
	}

	public static void main(String[] args) {

		E4 hw = new E4("E4");
		hw.setSize(500, 500);
		hw.setLocation(250, 250);
		hw.setVisible(true);
	}
}
