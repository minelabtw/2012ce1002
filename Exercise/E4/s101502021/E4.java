package ce1002.E4.s101502021;
import java.awt.Label;

import javax.naming.spi.DirStateFactory.Result;
import javax.swing.AbstractButton;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JTextField;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class E4 extends JFrame implements ActionListener{
	
	private JButton bt_push = new JButton("push");//按鈕push
	private JButton bt_pop = new JButton("pop");//按鈕pop
	private JTextField text_input = new JTextField();//輸入框
	private JTextField text_result = new JTextField();//輸入框
	private JLabel lb_input = new JLabel("input:");
	private JLabel lb_result = new JLabel("result:");
	private String string="";//空的字串
	private String[] store = new String[1000];
	static int i=0;

	public static void main(String[] args) {
		
		E4 e4 = new E4("M1");
		
		e4.setSize(800, 600);//視窗大小
		e4.setLocation(250, 250);
		e4.setTitle("M1");//視窗名稱
		e4.setLayout(null);
		e4.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);//關閉視窗
		e4.setVisible(true);//顯示視窗
	}
	public E4(String pTitle){
		
		super(pTitle);
		setLayout(null);
		//設定標籤、輸入框的位置、大小
		lb_input.setBounds(10, 10, 50, 30);
		lb_result.setBounds(10, 40, 50,30);
		text_input.setBounds(50, 15, 700,20 );	
		text_result.setBounds(50, 45, 700,20 );
		bt_push.setBounds(270, 70, 100,50 );
		bt_pop.setBounds(380, 70, 100,50 );
		
		//使按鈕有反應
		bt_push.addActionListener(this);
		bt_pop.addActionListener(this);
		
		//將標籤、輸入框放進視窗中
		add(lb_input);
		add(lb_result);
		add(text_input);
		add(text_result);
		add(bt_push);
		add(bt_pop);
	}

	public void actionPerformed(ActionEvent e) {
		//按下push的按鈕
		if ( e.getSource() == bt_push){
			string="";
			store[i]=text_input.getText();
			
			for(int j=0;j<=i;j++){
				if(j==i)
					string=string+store[j];
				else
					string=string+store[j]+',';
			}
			//在result的輸入框中顯示string
			text_result.setText(string);
			i++;
		}
		
		//點下pop的按鈕
		if ( e.getSource() == bt_pop){
			string="";
			i--;
	
			for(int j=0;j<i;j++){
				if(j==i-1)
					string=string+store[j];
				else 
					string=string+store[j]+',';
				
			}
			//在result的輸入框中顯示string
			text_result.setText(string);
		}
		
	}
}
	


