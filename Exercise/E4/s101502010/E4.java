package ce1002.E4.s101502010;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.*;
 
public class E4 { 
    private static JButton push_btn = new JButton("push");
    private static JButton pop_btn = new JButton("pop");
    private static TextField input_txt = new TextField(20);
    private static TextField result_txt = new TextField(20);
    private static JLabel input_lab = new JLabel("input:");
    private static JLabel result_lab = new JLabel("result:");
    stack box = new stack();
    public static void main(String[] args) {  
    	E4 frame = new E4();
    }
    public E4(){
    	JFrame demo = new JFrame();
        demo.setSize(400, 300);
        demo.setTitle("E4-101502010");
        demo.setLayout(null);
        demo.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);  
        input_lab.setBounds(0, 0, 80, 40);
        result_lab.setBounds(0, 40, 80, 40);
        push_btn.setBounds(80, 80, 100, 40);
        pop_btn.setBounds(180,80,100,40);
        input_txt.setBounds(80, 0, 250, 35);
        result_txt.setBounds(80, 40, 250, 35);
        input_lab.setFont(new Font("", Font.BOLD, 20));
        result_lab.setFont(new Font("", Font.BOLD, 20));
        push_btn.setFont(new Font("", Font.BOLD, 20));
        pop_btn.setFont(new Font("", Font.BOLD, 20));
        input_txt.setFont(new Font("", Font.BOLD, 20));
        result_txt.setFont(new Font("", Font.BOLD, 20));
    	push_btn.addActionListener(new ActionListener(){
          	public void actionPerformed(ActionEvent arg0) {
          		int n = Integer.parseInt(input_txt.getText());
          		box.push(n);
          		result_txt.setText(box.show());
          	}
        });
    	pop_btn.addActionListener(new ActionListener(){
            public void actionPerformed(ActionEvent arg0) {
            	box.pop();
            	result_txt.setText(box.show());
            }     
        });
    	
    	demo.add(input_lab);
    	demo.add(input_txt);
    	demo.add(result_lab);
    	demo.add(result_txt);
    	demo.add(push_btn);
    	demo.add(pop_btn);
    	
    	demo.setVisible(true);
    }
}