package ce1002.E4.s995002525;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Stack;
import javax.swing.*;

public class E4  extends JFrame implements ActionListener {
	private JButton btn1 = new JButton("push");
	private JButton btn2 = new JButton("pop");
	private JTextField text1 = new JTextField(20);
	private JTextField text2 = new JTextField(20);
	private JLabel label1 = new JLabel("input");
	private JLabel label2 = new JLabel("result");
	private static Stack stack = new Stack();
	
	public static void main(String[] args)throws Exception {

		E4 hw = new E4("Hello World App");

		hw.setSize(300, 300);

		hw.setLocation(250, 250);

		hw.setVisible(true);

	}

	public E4(String pTitle) {

		super(pTitle);

		setLayout(null);// 不使用版面配置
		
		label1.setBounds(5,5,50,20);//x y length weight
		label2.setBounds(5,30,50,20);
		
		text1.setBounds(50, 5, 200, 20);
		text2.setBounds(50, 30, 200, 20);

		btn1.setBounds(60, 60, 70, 20);
		btn2.setBounds(150, 60, 70, 20);
		
		btn1.addActionListener(this);//this class
		btn2.addActionListener(new Button2());//class Button2

		add(btn1);
		add(text1);
		add(label1);
		add(btn2);
		add(text2);
		add(label2);

	}

	public void actionPerformed(ActionEvent e) {
		stack.push(text1.getText());
		text2.setText(text2.getText()+"  "+text1.getText());
		text1.setText("");
	}
	class Button2 implements ActionListener {
	    public void actionPerformed(ActionEvent e) {
	    	text1.setText(stack.peek().toString());
	    	stack.pop();
	    	
	    	
	    }
	}
	
}

