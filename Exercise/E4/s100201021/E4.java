package ce1002.E4.s100201021;
import java.awt.*;
import java.awt.event.*;
import javax.swing.*;

public class E4 extends JFrame{
		public JButton push=new JButton("push");
		public JButton pop=new JButton("pop");
		public JLabel input=new JLabel("input");
		public JLabel result=new JLabel("result");
		public JTextField inpField=new JTextField(25);
		public JTextField resField=new JTextField(25);
		int stack=0;		//length of stack
		String[] st=new String[100]; 	//stack
		String OS,t;
	public E4(){
		//set frame
		setTitle("E4");
		setVisible(true);
		setSize(400,300);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setLayout(new GridLayout(4,1,5,5));
		
		//set panel
		JPanel P1=new JPanel(new FlowLayout());
		JPanel P2=new JPanel(new FlowLayout());
		JPanel P3=new JPanel(new GridLayout(1,2,2,2));
		P1.add(input);
		P1.add(inpField);
		P2.add(result);
		P2.add(resField);
		P3.add(push);
		P3.add(pop);
		
		//set button
		inpush p=new inpush();
		inpop i=new inpop();		
		push.addActionListener(p);
		pop.addActionListener(i);		
		
		//add panel
		add(P1);
		add(P2);
		add(P3);

	}
	public static void main(String args[]){
		E4 frame=new E4();
	}
	
	//push
	class inpush implements ActionListener{
		public void actionPerformed(ActionEvent e) {
			t=inpField.getText();
			if(t!=null && t.length()>0){
				stack++;
				st[stack]=t;
				for(int i=1;i<=stack;i++){
					if(i==1)
						OS=st[i];
					else{
						OS+=","+st[i];
					}
				}					
			}
			resField.setText(OS);	//output result
		}
	}
	//pop
	class inpop implements ActionListener{
		public void actionPerformed(ActionEvent e) {
			stack--;
			if(stack<0)
				stack=0;
			if(stack>0){
				for(int i=1;i<=stack;i++){
					if(i==1)
						OS=st[i];
					else{
						OS+=","+st[i];
					}
				}
			}
			else {
				OS="";
			}
			resField.setText(OS);	//output result
		}
	}	
}
