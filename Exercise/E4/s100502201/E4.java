//E4
//Stack with GUI
package ce1002.E4.s100502201;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.*;

public class E4 extends JFrame implements ActionListener {
	int[] stack = new int[100];
	private JButton push = new JButton("push");
	private JButton pop = new JButton("pop");
	private JTextField input = new JTextField(20);
	private JTextArea result = new JTextArea();
	private JLabel labelI = new JLabel("input:");
	private JLabel labelR = new JLabel("result:");
	private int ptr = -1;

	public static void main(String[] args) { //Constructor

		E4 E4 = new E4("E4");

		E4.setSize(350, 200);

		E4.setLocation(250, 250);

		E4.setVisible(true);

	}

	public E4(String pTitle) {

		super(pTitle);

		setLayout(null);// 不使用版面配置

		push.setBounds(30, 80, 100, 20);
		pop.setBounds(180, 80, 100, 20);

		input.setBounds(80, 10, 170, 20);
		result.setBounds(80, 40, 170, 20);

		labelI.setBounds(0, 10, 80, 20);
		labelR.setBounds(0, 40, 80, 20);

		push.setActionCommand("push"); //Make push and pop distinct to launch actionlistener
		push.addActionListener(this);
		pop.setActionCommand("pop");
		pop.addActionListener(this);

		result.setText("");

		add(push);
		add(pop);

		add(labelI);
		add(labelR);

		add(input);
		add(result); //Put all elements into frame

	}

	public void actionPerformed(ActionEvent e) {

		String cmd = e.getActionCommand();
		if (cmd == "push") {
			if (input.getText() == null) { //Work in progress, no use
				JOptionPane.showMessageDialog(null, "Invalid input!", "Error",
						JOptionPane.INFORMATION_MESSAGE);
			} else {
				result.setText("");
				int num = Integer.parseInt(input.getText());
				stack[++ptr] = num;
				for (int i = 0; i <= ptr; i++)
					result.append(String.valueOf(stack[i]) + ","); //A function to make strings reveal in recursive fashion
				input.setText("");
			}
		} else if (cmd == "pop") {
			result.setText("");
			ptr--;
			for (int i = 0; i <= ptr; i++)
				result.append(String.valueOf(stack[i]) + ",");
		}
	}

}
