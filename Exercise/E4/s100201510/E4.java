package ce1002.E4.s100201510;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.*;

public class E4 extends JFrame{
	//set the buttons
	public static JTextField RESULTFIELD = new JTextField(30);
	public static JTextField INPUTFIELD = new JTextField(30);
	public E4(){//set layout
		setTitle("E4");
		setLayout(new FlowLayout());
		add(new Label("input:"));
		add(INPUTFIELD);
		add(new Label("result:"));
		add(RESULTFIELD);
		JButton PUSH = new JButton("push");
		add(PUSH);
		JButton POP = new JButton("POP");
		add(POP);
		// set the function of buttons
		POP.addActionListener(new POPlisener());
		PUSH.addActionListener(new PUSHlisener());
	}
	
	public static void main(String[] args) {
		E4 frame = new E4();
		frame.setSize(450, 200);
		frame.setLocationRelativeTo(null);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setVisible(true);
	}
}

class PUSHlisener implements ActionListener{
	// the function of PUSH
	public void actionPerformed(ActionEvent arg0) {
		MYstack.push(E4.INPUTFIELD.getText());
		// print result on RESULTFIELD
		String buffer = "";
		for(int i = 0 ; i < MYstack.length ; i++){
			buffer = buffer + "," + MYstack.stack[i];
		}
		(E4.RESULTFIELD).setText(buffer);
		//clear INPUTFIELD
		(E4.INPUTFIELD).setText("");
	}
}

class POPlisener implements ActionListener{
	// the function of PUSH
	public void actionPerformed(ActionEvent arg0) {
		MYstack.pop(Integer.parseInt(E4.INPUTFIELD.getText()));
		// print result on RESULTFIELD
		String buffer = "";
		for(int i = 0 ; i < MYstack.length ; i++){
			buffer = buffer + "," + MYstack.stack[i];
		}
		(E4.RESULTFIELD).setText(buffer);
		//clear INPUTFIELD
		(E4.INPUTFIELD).setText("");
	}
}

class MYstack {
	//record the number of the elements in stack
	public static int length = 0;
	//a stack of size 100
	public static String[] stack = new String[100];
	
	public static void push(String CH){
		stack[length] = CH ;
		length++;
	}
	
	public static void pop(int NUM){
		length -= NUM;
		
		if(length < 0)
			length = 0;
	}
}