package ce1002.E4.s100502205;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;

public class E4 extends JFrame {
	private JLabel input, output;
	private JTextField inText, outText;
	private JButton pushBtn, popBtn;
	private JPanel upperPanel, lowerPanel, chPanel;
	private int[] STACK = new int[105];
	private int stkIdx = 0;

	private void showRes() {
		String tot = "";
		for (int i = 0; i < stkIdx; i++) {
			if (i != 0)
				tot += ",";
			tot += STACK[i];
		}
		if (stkIdx == 0)
			tot = "Empty !";
		outText.setText(tot);
		inText.setText("");
	}

	private void pop() {
		if (stkIdx > 0)
			stkIdx--;
		showRes();
	}

	private void push() {
		String in = inText.getText();
		try {
			int number = Integer.parseInt(in);
			STACK[stkIdx++] = number;
		} catch (Exception e) {
			// input not a number, ignore this.
		}
		showRes();
	}

	public E4() {
		super("E4");// this.setTitle("E4");
		this.setSize(450, 150);
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		this.setLayout(new BorderLayout());
		this.setResizable(false);

		input = new JLabel("input:");
		output = new JLabel("result:");
		inText = new JTextField(30);
		outText = new JTextField(30);
		pushBtn = new JButton("push");
		popBtn = new JButton("pop");
		upperPanel = new JPanel();
		lowerPanel = new JPanel();
		chPanel = new JPanel();

		upperPanel.setLayout(new FlowLayout());
		upperPanel.add(input);
		upperPanel.add(inText);
		lowerPanel.setLayout(new FlowLayout());
		lowerPanel.add(output);
		lowerPanel.add(outText);
		chPanel.setLayout(new FlowLayout());
		chPanel.add(pushBtn);
		chPanel.add(popBtn);

		this.add(upperPanel, BorderLayout.NORTH);
		this.add(lowerPanel, BorderLayout.CENTER);
		this.add(chPanel, BorderLayout.SOUTH);

		pushBtn.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				push();
			}
		});
		popBtn.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				pop();
			}
		});
		this.setVisible(true);
	}

	public static void main(String[] args) {
		new E4();
	}
}