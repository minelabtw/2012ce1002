package ce1002.E4.s101502503;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.*;

public class E4 extends JFrame implements ActionListener 
{
	private JLabel label = new JLabel("   input:");//input標籤
	private JLabel label2 = new JLabel("   result:");//result標籤
	private JTextField input = new JTextField(20);//input文字方塊
	private JTextField result = new JTextField(20);//result文字方塊
	private JButton pushbtn = new JButton("push");//push按鈕
	private JButton popbtn = new JButton("pop");//pop按鈕
	private int[] stack = new int[50];//存所輸入的數
	private int count = 0;//計算有幾個數
	
	public static void main(String[] args) {

		E4 hw = new E4("M1");

		hw.setSize(400, 300);//視窗大小

		hw.setLocation(250, 250);

		hw.setVisible(true);//開啟視窗

	}

	public E4(String pTitle) {

		super(pTitle);

		setLayout(null);// 不使用版面配置
		label.setBounds(0, 10, 50, 20);//(距左,距上,長度,距右)
		label2.setBounds(0, 35, 50, 20);
		input.setBounds(50, 10, 320, 20);
		result.setBounds(50, 35, 320, 20);
		pushbtn.setBounds(130, 60, 70, 20);
		popbtn.setBounds(210, 60, 70, 20);
		
		pushbtn.addActionListener(this);//點push按鈕
		popbtn.addActionListener(this);//點pop按鈕
		
		//////新增//////
		add(label);
		add(label2);
		add(input);
		add(result);
		add(pushbtn);
		add(popbtn);
		

	}

	public void actionPerformed(ActionEvent e) {
		
		if(e.getSource()==pushbtn)//若點擊push按鈕
		{
			int a = Integer.valueOf(input.getText());//取所輸入的數
			stack[count]=a;//存到stack陣列
			if(count==0)
				result.setText(stack[0]+"");
			else
				result.setText(result.getText() +"," + stack[count]);
			count++;
		}
		else if(e.getSource()==popbtn)//若點擊pop按鈕
		{
			String str = new String();//字串存取pop一個數之後,stack裡面的數
			count--;
			for(int i=0;i<count;i++)
			{
				if(i==0)
				{
					str+=stack[0];
				}
				else
					str+=","+stack[i];
			}
			result.setText(str);//顯示在result	
		}
	}
}
