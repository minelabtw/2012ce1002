package ce1002.E4.s101502003;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.*;
public class E4 extends JFrame implements ActionListener 
{
	private JButton push = new JButton("push"); //設按鈕 文字 標籤
	private JButton pop = new JButton("pop");
	private JTextField text = new JTextField(20);
	private JLabel input = new JLabel("input");
	private JLabel result = new JLabel("result");
	private JTextField enter = new JTextField(20);
	int []Array = new int[100];
	private int i=0;
	public static void main(String[] args) { //設視窗大小
		E4 hw = new E4("M1");
		hw.setSize(300, 250);
		hw.setLocation(250, 250);
		hw.setVisible(true);
	}

	public E4(String pTitle) {
		super(pTitle);
		setLayout(null);// 不使用版面配置
		input.setBounds(10,40,40,20); //設定按鈕 文字 標籤位置
		result.setBounds(10,70,60,20);
		enter.setBounds(60,70,200,20);
		push.setBounds(40, 100, 80, 20);
		pop.setBounds(150 , 100 , 80 ,20);
		text.setBounds(60, 40, 200, 20);
		push.addActionListener(this);
		pop.addActionListener(this);
		add(input);
		add(result);
		add(push);
		add(pop);
		add(text);
		add(enter);
	}
	public void actionPerformed(ActionEvent e) 
	{
		
		if(e.getSource()==push) //如果按下push鍵
		{
			int a = Integer.valueOf(text.getText()); //輸入的數列入陣列
			Array[i]=a;
			if(i==0)
			{
				enter.setText(""+Array[i]); 
			}
			else		
				enter.setText(enter.getText()+","+Array[i]); //在第二行輸出push的數字
			i++;
		}
			
		else if (e.getSource()==pop) //如果按下pop鍵
		{
			i--;
			if(i==0)
				enter.setText("Stack is empty!"); //如果裡面是空的,輸出文字
			for(int b=0;b<i;b++) //輸出剩下的文字
			{
				if(b==0) 
				{
					enter.setText(""+Array[b]);
				}
				else		
					enter.setText(enter.getText()+","+Array[b]);
			}	
		}
	}
}