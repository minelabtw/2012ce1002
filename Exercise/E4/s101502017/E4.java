package ce1002.E4.s101502017;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JTextField;

public class E4 extends JFrame implements ActionListener {
	private JButton Push = new JButton("Push");
	private JButton Pop = new JButton("Pop");
	private JTextField text1 = new JTextField(20);
	private JTextField text2 = new JTextField(20);
	JLabel c = new JLabel("input:");
	JLabel d = new JLabel("result:");
	int i = 0;
	String [] stack = new String [100];

	public static void main(String[] args) {

		E4 hw = new E4("Hello World App");

		hw.setSize(400, 300);

		hw.setLocation(250, 250);

		hw.setVisible(true);

	}

	public E4(String pTitle) {

		super(pTitle);

		setLayout(null);// 不使用版面配置

		Pop.setBounds(200, 80, 100, 40);

		Push.setBounds(60, 80, 100, 40);

		text1.setBounds(60, 20, 280, 20);

		text2.setBounds(60, 50, 280, 20);

		c.setBounds(10, 20, 50, 20);

		d.setBounds(10, 50, 50, 20);

		Pop.addActionListener(this);

		Push.addActionListener(this);

		add(Pop);

		add(Push);

		add(text1);

		add(text2);

		add(c);

		add(d);

	}

	public void actionPerformed(ActionEvent e) {
		String total="";
		String cmd = e.getActionCommand();
		if(cmd=="Push")
		{
	    	stack[i] = text1.getText();
	    	stack[i+1]=",";
	    	i+=2;
		}
		else if(cmd=="Pop")
		{
			stack[i-2]="";
			i-=2;
		}
		for(int j=0;j<i-1;j++)
		{
			total+=stack[j];
		}
		text2.setText(total);
	}
}