package ce1002.E4.s101502029;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Scanner;

import javax.swing.*;

public class E4 extends JFrame implements ActionListener {
	private JButton push = new JButton("Push");
	private JButton pop = new JButton("Pop");
	private JLabel input = new JLabel("input");
	private JLabel result = new JLabel("result");
	private JTextField textinput = new JTextField(20);
	private JTextField textresult = new JTextField(20);
	private int numClicks = 0;
	private String Num;
	private int[] a = new int[100];

	public static void main(String[] args) {
		E4 hw = new E4("M1");

		hw.setSize(670, 500);

		hw.setLocation(250, 250);

		hw.setVisible(true);

	}

	public E4(String pTitle) {

		super(pTitle);

		setLayout(null);// 不使用版面配置
		// 左到右的空格,上到下的空格,按鈕的長度,按鈕的寬度
		input.setBounds(0, 0, 40, 20);
		textinput.setBounds(40, 0, 600, 20);
		result.setBounds(0, 20, 40, 20);
		textresult.setBounds(40, 20, 600, 20);
		push.setBounds(200, 40, 70, 20);
		pop.setBounds(280, 40, 70, 20);
		push.addActionListener(this);
		pop.addActionListener(this);
		add(input);
		add(textinput);
		textinput.getText();
		add(result);
		add(textresult);
		add(push);
		add(pop);
	}

	public void actionPerformed(ActionEvent e) {

		if (e.getSource() == push) {
			a[numClicks] = Integer.parseInt(textinput.getText());
			numClicks++;
			Num = "";

			for (int i = 0; i < numClicks; i++) {
				Num = Num + a[i]+",";
			}
				textresult.setText(Num);
				textinput.setText("");
			}
			if (e.getSource() == pop) {
				a[numClicks] = Integer.parseInt(textinput.getText());
				numClicks--;
				Num = "";

				for (int i = 0; i < numClicks; i++) {
					Num = Num + a[i];
					if (i < 1) {
						Num += ",";
					}
				}

				textresult.setText(Num);
				textinput.setText("");
			}

		}

	}

