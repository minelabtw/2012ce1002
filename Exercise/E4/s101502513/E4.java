package ce1002.E4.s101502513;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class E4 extends JFrame implements ActionListener {
	private JLabel first = new JLabel("input:");
	private JTextField input = new JTextField(20);
	private JLabel second = new JLabel("result:");
	private JTextField result = new JTextField(20);
	private JButton push = new JButton("push");
	private JButton pop = new JButton("pop");
	private int i = 0;
	private int[] stack = new int[100];
	private String m;
	
	public E4() {
		setLayout(null); //have no layout
		
		//coordinate
		first.setBounds(10, 20, 40, 20); //input
		input.setBounds(50, 20, 200, 20);
		second.setBounds(10, 50, 40, 20); //output
		result.setBounds(50, 50, 200, 20);
		push.setBounds(40, 80, 70, 20); //button
		pop.setBounds(120, 80, 70, 20);
		push.addActionListener(this); //trigger
		pop.addActionListener(this);
		
		add(first);
		add(input);
		add(second);
		add(result);
		add(push);
		add(pop);
	}
	
	public static void main(String[] args) {
		E4 frame = new E4();
		frame.setTitle("E4");
		frame.setSize(300, 300);
		frame.setLocationRelativeTo(null);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setVisible(true);
	}
	
	public void actionPerformed(ActionEvent e) {
		stack[i] = Integer.parseInt(input.getText()); //receive the value of inputting
		
		if( e.getSource() == push ) { //if you press button "push"
			if( i == 0 ) //First value.
				m = String.valueOf(stack[i]);
			else if( i > 0 ) //add "," after first value and new value
				m += String.valueOf("," + stack[i]);
			
			result.setText(m); //return m
			i++;
		}
		else if( e.getSource() == pop ) {
			i--;
			if( i < 0 ) //At least 0 value
				i = 0;
			m = ""; //empty
			
			for( int a = 0; a < i; a++ ) {
				if( a == 0 )
					m = String.valueOf(stack[a]);
				else if( a > 0 )
					m += String.valueOf("," + stack[a]);
			}
			result.setText(m);
		}
	}
}
